;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Sonam Gupta MTech IT Banasthali 13-3-2014
;An equation obtained by equating a physical quantity with its dimensional formula is called the dimensional equation of the 
;physical quantity. [ncert]
;किसी भौतिक राशि को उसके विमीय सूत्र के बराबर लिखने पर प्राप्त समीकरण को उस राशि का विमीय समीकरण कहते हैं.
(defrule obtain1
(declare (salience 4900))
(id-root ?id obtain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obtain.clp 	obtain1   "  ?id "  prApwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-11-2014)
;A measure of temperature is obtained using a thermometer. [NCERT CORPUS]
;wApamApI (WarmAmItara) kA upayoga karake wApa kI eka mApa prApwa howI hE. [NCERT CORPUS]
(defrule obtain4
(declare (salience 4900))
(id-root ?id obtain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 measure)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obtain.clp 	obtain4   "  ?id "  prApwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-11-2014)
;However, dimensionless constants can not be obtained by this method. [NCERT CORPUS]
;waWApi vimAhIna sWirAfkoM ke mAna isa viXi xvArA jFAwa nahIM kie jA sakawe. [NCERT CORPUS]
;Note that value of constant k can not be obtained by the method of dimensions. [NCERT CORPUS]
;XyAna xIjie, yahAz sWirAfka @k kA mAna vimIya viXi se jFAwa nahIM kiyA jA sakawA hE. [NCERT CORPUS]

(defrule obtain5
(declare (salience 4900))
(id-root ?id obtain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 method)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jFAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obtain.clp 	obtain5   "  ?id "  jFAwa_kara )" crlf))
)

;-------------------------- Default rules -----------------------

;@@@ Added by Sonam Gupta MTech IT Banasthali 13-3-2014
;To obtain advice. [cambridge]
;सलाह प्राप्त करना .
(defrule obtain2
(id-root ?id obtain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obtain.clp 	obtain2   "  ?id "  prApwa_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 13-3-2014
(defrule obtain3
(id-root ?id obtain)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obtain.clp 	obtain3   "  ?id "  prApwa_kara )" crlf))
)
