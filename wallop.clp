
;@@@ Added by 14anu-ban-11 on (17-04-2015)
;We walloped them 6–0.(oald)
;हमने उनको 6–0 से बुरी तरह से हरा दिया . (self)
(defrule wallop1
(declare (salience 10))
(id-root ?id wallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burI_waraha_se_harA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallop.clp 	wallop1   "  ?id "  burI_waraha_se_harA_xe )" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-04-2015)
;This story packs an emotional wallop.(coca)
;यह कहानी भावनात्मक प्रभाव से भरी है . (self)
(defrule wallop3
(declare (salience 20))
(id-root ?id wallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 emotional)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallop.clp 	wallop3   "  ?id "  praBAva)" crlf))
)

;---------------------------------- Default Rules -----------------------------------------------

;@@@ Added by 14anu-ban-11 on (17-04-2015)
;My mother gave me such a wallop when she eventually found me.(cald)
;मेरी माँ ने मुझे घूंसा मारा जब उसको मै अन्त में मिला . (self)
(defrule wallop0
(declare (salience 00))
(id-root ?id wallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUMsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallop.clp 	wallop0   "  ?id "  GUMsA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-04-2015)
;My father used to wallop me if I told lies.(oald)
;मेरे पिता जी मुझे जोर से मारा करते थे  यदि मैं झूठ बोलता था . (self)
(defrule wallop2
(declare (salience 00))
(id-root ?id wallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallop.clp 	wallop2   "  ?id "  jora_se_mAra)" crlf))
)


