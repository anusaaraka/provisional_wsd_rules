
(defrule counter0
(declare (salience 5000))
(id-root ?id counter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virUxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  counter.clp 	counter0   "  ?id "  virUxXa )" crlf))
)

(defrule counter1
(declare (salience 4900))
(id-root ?id counter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAuMtara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  counter.clp 	counter1   "  ?id "  kAuMtara )" crlf))
)

;"counter","N","1.kAuMtara"
;Go to the teller counter.
;--"2.ginane_kA_yaMwra"
;Japanese always use special counter for counting. 
;
;

;$$$ Modified  by 14anu-ban-03 (13-03-2015)
;Indeed, many eminent child development researchers have countered Harris's thesis. [report set 4]
;वास्तव में,कई उत्कृष्ट बाल विकास शोधकर्ताओं ने हैरिस के शोध-प्रबन्ध का विरोध किया है । [manual]
;@@@ Added by 14anu01 on 27-06-2014
;The second argument is more difficult to counter. 
;दूसरी बहस को विरोध करना अधिक मुश्किल है . 
;दूसरे तर्क का विरोध करना अधिक मुश्किल है . [manual] ;added by 14anu-ban-03 (13-03-2015)
(defrule counter2
(declare (salience 4900))
(id-root ?id counter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))  ;added by 14anu-ban-03 (13-03-2015)
(assert (kriyA_id-subject_viBakwi ?id kA)) ;added by 14anu-ban-03 (13-03-2015)
(assert (id-wsd_root_mng ?id viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  counter.clp 	counter2   "  ?id " kA )" crlf)  ;added by 14anu-ban-03 (13-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  counter.clp  counter2   "  ?id " kA )" crlf)  ;added by 14anu-ban-03 (13-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  counter.clp 	counter2   "  ?id "  viroXa_kara)" crlf))
)

;@@@ Added by14anu-ban-03 (10-10-2014)
;To counter this force, the child has to apply an external force on the car in the direction of motion.[ncert]
;इस बल को निष्फल करने के लिए बालिका को कार पर गति की दिशा में बाह्य बल लगाना पडता है. [ncert]
(defrule counter4
(declare (salience 4900))
(id-root ?id counter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRPala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  counter.clp 	counter4   "  ?id "  niRPala_kara)" crlf))
)


;@@@ Added by 14anu01 on 27-06-2014
;His writing ran counter to the dominant trends of the decade.
;उसका लेखन  दशक के प्रबल चलन के विपरीत चल रहा हे. 
(defrule counter3
(declare (salience 4900))
(id-root ?id counter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  counter.clp 	counter3   "  ?id "  viparIwa)" crlf))
)
