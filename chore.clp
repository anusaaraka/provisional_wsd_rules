;@@@ Added by 14anu-ban-03 (16-02-2015)
;Doing the domestic chores. [oald]
;घरेलू काम करना . [manual]
(defrule chore0
(declare (salience 00))
(id-root ?id chore)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chore.clp 	chore0   "  ?id "  kAma )" crlf))
)


;@@@ Added by 14anu-ban-03 (16-02-2015)
;Shopping's a real chore for me. [oald]
;खरीददारी मेरे लिए एक सत्य उबाऊ काम है . [manual]
(defrule chore1
(declare (salience 100))
(id-root ?id chore)
?mng <-(meaning_to_be_decided ?id)
;(subject-subject_samAnAXikaraNa  ?id1 ?id)  ;commented by 14anu-ban-03 (19-02-2015)
(viSeRya-viSeRaNa ?id ?id1)   ;added by 14anu-ban-03 (19-02-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ubAU_kAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chore.clp 	chore1   "  ?id "  ubAU_kAma )" crlf))
)
