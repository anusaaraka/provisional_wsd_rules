;@@@Added by 14anu-ban-02(04-04-2015)
;Sentence: The hat identified him as a bourgeois.[oald]
;Translation: टोपी द्वारा  उसको मध्यवर्गीय के रूप में पहचाना गया . [self]
(defrule bourgeois1 
(declare (salience 100)) 
(id-word ?id bourgeois) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-as_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?)
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id maXyavargIya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bourgeois.clp  bourgeois1  "  ?id "  maXyavargIya )" crlf)) 
) 



;@@@Added by 14anu-ban-02(04-04-2015)
;Sentence: A traditional bourgeois family.[oald]
;Translation:परम्परागत मध्यवर्गीय परिवार . [self]
(defrule bourgeois2 
(declare (salience 100)) 
(id-word ?id bourgeois) 
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 family) 
(viSeRya-viSeRaNa ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id maXyavargIya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bourgeois.clp  bourgeois2  "  ?id "  maXyavargIya )" crlf)) 
) 



;---------------------default_rules----------------------------------------------------
;@@@Added by 14anu-ban-02(04-04-2015)
;Sentence: I'd forgotten what a bourgeois you are.[oald]
;Translation: मैं भूल गया था कि आप कितने रूढ़ीवादी हैं. [self]
(defrule bourgeois0 
(declare (salience 0)) 
(id-word ?id bourgeois) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id rUDZIvAxI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bourgeois.clp  bourgeois0  "  ?id "  rUDZIvAxI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(04-04-2015)
;Sentence: Bourgeois ideology.[oald]
;Translation:रूढ़ीवादी विचारधारा .  [self]
(defrule bourgeois3 
(declare (salience 0)) 
(id-word ?id bourgeois) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id xakiyAnUsI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bourgeois.clp  bourgeois3  "  ?id "  xakiyAnUsI )" crlf)) 
) 
