(defrule ravel0
(declare (salience 5000))
(id-root ?id ravel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KolanA_KulajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ravel.clp 	ravel0   "  ?id "  KolanA_KulajA )" crlf))
)

;"ravel","VI","1.KolanA_KulajAnA"
;The rug has started to ravel at the edges.


;$$$ Modified by 14anu02 on 26.06.14
;Before modification this rule could not get fired since it was same as 'ravel0' with a less salience
;The mystery needs to be raveled out.
;इस रहस्य को सुलझाने की जरूरत है.
(defrule ravel1
(declare (salience 5001))	;salience changed by 14anu02
(id-root ?id ravel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)	;added by 14anu02
(id-word ?id1 out)	;added by 14anu02
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sulaJA))	;modified by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " ravel.clp	ravel1  "  ?id "  " ?id1 "  sulaJA  )" crlf))
)

;"ravel","VT","1.sulaJAnA"

