;@@@ Added by 14anu26  [28-06-14]
;she was fed by the Dove.
;वह पवित्र आत्मा  के द्वारा पाली गयी थी . 
(defrule feed2
(declare (salience 5000))
(id-root ?id feed)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feed.clp 	feed2   "  ?id "   pAla )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-12-2014)
;While the larvae feed on the petioles in the crown and on sap from the base of the fronds.[agriculture]
;जब कि डिंभक डंठल के  ऊपरी सिरे से और लम्बे बडे पत्तें के निचले भाग के रस से  भोजन  ग्रहण करते हैं.[manual]
(defrule feed3
(declare (salience 5000))
(id-root ?id feed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 petiole|pollen|frond|root|bud)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bojana_grahaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feed.clp 	feed3   "  ?id "  Bojana_grahaNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-02-2015)
;We are fed up with this facade of democracy.	[cald]
;हम संचालन के इस मुखौटे से तंग आ चुके हैं.			[manual]

(defrule feed4
(declare (salience 5001))
(id-word ?id fed)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (+ ?id 1) waMga_A_cukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feed.clp  feed4  "  ?id "  " (+ ?id 1) "  waMga_A_cukA  )" crlf))
)

;---------------- Default rules --------------------
(defrule feed0
(declare (salience 5000))
(id-root ?id feed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feed.clp 	feed0   "  ?id "  KAnA )" crlf))
)

;"feed","N","1.KAnA"
;The baby had the last feed two hours ago.
;--"2.mAla{maSIna_meM_iswemAla_kiye_jAnevAlA}"
;--"3.pAIpa{jisase_maSIna_ko_wela_Axi_xewe_hEM}"
;Petrol feed of this car is chocked up.
;
(defrule feed1
(declare (salience 4900))
(id-root ?id feed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAnA_KilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feed.clp 	feed1   "  ?id "  KAnA_KilA )" crlf))
)

;"feed","V","1.KAnA_KilAnA"
;She has a large family to feed.
;Feed the wood to the fire.
;--"2.xenA"
;Mohan feeds the information to me.
;--"3.dAlanA"
;To use a public phone, you have to feed coins into it.
;--"4.panapanA"
;Hatred feeds on envy.
;
