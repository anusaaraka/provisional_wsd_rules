;@@@ Added by 14anu-ban-04 (14-03-2015)
;They are very good assistants, very discreet - they wouldn't go talking to the press.             [cald]
;वे  बहुत अच्छे सहायक हैं , अत्यन्त समझदार - वे प्रैस से बातचीत करने नहीं जाएँगे .                          [self]
(defrule discreet2
(declare (salience 30))
(id-root ?id discreet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRaNa-viSeRaka ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discreet.clp    discreet2   "  ?id "   samaJaxAra )" crlf))
)

;@@@ Added by 14anu-ban-04 (14-03-2015)
;The family made discreet enquiries about his background.                  [cald]
;परिवार ने  उसकी पृष्ठभूमि के बारे में  पृथक पूछताछ की।                                                                                      [self]
(defrule discreet1
(declare (salience 20))
(id-root ?id discreet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 enquiry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pqWaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discreet.clp 	discreet1   "  ?id " pqWaka )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (14-03-2015)
;A discreet glance at the clock told me the interview had lasted an hour.                     [oald]
;घड़ी  पर पड़ी एक सतर्क नजर ने मुझे बताया कि साक्षात्कार  एक घण्टा  चला था .                                         [self]
(defrule discreet0
(declare (salience 10))
(id-root ?id discreet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sawarka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discreet.clp    discreet0   "  ?id "  sawarka )" crlf))
)


