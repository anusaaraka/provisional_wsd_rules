;@@@ Added by 14anu-ban-06  (09-10-2014)
;British writers and statesmen also used their criti - cism of Indian culture and society to justify British political and economic domination over India .(parallel corpus)
;ब्रितानी लेखकों और कूटनीतिज्ञों ने भी भारतीय समाज और संस्कृति की आलोचना भारत पर अपने राजनीतिक और आर्थिक शासन का औचित्य उचित सिद्ध करने के लिए की .
;(parallel corpus)
(defrule justify0
(declare (salience 0))
(id-root ?id justify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa_sixXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  justify.clp 	justify0   "  ?id "  uciwa_sixXa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06  (09-10-2014)
;But who is going to listen to those who justify Gujarat's violence on the grounds that it was " natural and spontaneous " ?(parallel corpus)
;लेकिन ऐसे लगों को कौन सुनने को तैयार होगा जो गुजरात की हिंसा को ' स्वाभाविक और स्वतःस्फूर्त ' बताकर जायज ठहरा रहे हैं .(parallel corpus)
;While taking care not to hurt the religious sentiments of the people he began to justify the revolution on the ground of equality preached by Islam .(parallel corpus)
;इस बात का घ़्यान रखते हुए कि लोगो की धर्मिक भवनाओं को ठेस न पहुंचे उऩ्होनें क्रांति को इस आधार पर जायज ठहराना शुरू किया कि इसलाम में भी बराबरी की बात कही गई है .(manual)
;He made a reasoned and forthright statement and said that although he could not justify the assault , he knew that students had acted under great provocation .(parallel corpus)
;तर्कपूर्ण और स़्पष़्टवादी वक़्तव़्य देते हुए उऩ्होंने कहा कि वे प्रोफेसर से हाथापाई को जायज  नहीं ठहरा सकते , लेकिन उऩ्हें मालूम है कि छात्र बेहद भड़के हुए
;अंग्रेज न्यायाधीशों , काजी और मुफ्ती के सर्किट न्यायालय स्थापित किए गए .(parallel corpus)
;While that can by no means justify the bloodshed that the state witnessed during the riots , the Government has finally decided to conduct a survey of the wide network of madarsas in the state .(parallel corpus)
;दंगों के दौरान इतने बड़ै पैमाने पर हे खून - खराबे को इससे जायज नहीं ठहराया जा सकता लेकिन सरकार ने इसी से चेतकर राज्य में मदरसों के नेटवर्क का सर्वेक्षण कराने का फैसल किया.
;(parallel corpus)
(defrule justify1
(declare (salience 2000))
(id-root ?id justify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 revolution|assault|emergency|bloodshed|violence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAyaja_TaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  justify.clp 	justify1   "  ?id "  jAyaja_TaharA )" crlf))
)

;@@@ Added by 14anu-ban-06  (09-10-2014)
;We shall not justify this fact, but we shall accept it.(NCERT)
;hama isa waWya kA samarWana nahIM karegeM, basa yaha mAna kara caleMge.(manual)
(defrule justify2
(declare (salience 2100))
(id-root ?id justify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 fact)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  justify.clp 	justify2   "  ?id "  samarWana_kara )" crlf))
)
