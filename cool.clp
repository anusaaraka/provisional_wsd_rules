;@@@ Added by 14anu20 on 13/06/2014
;He is very cool.
;वह अत्यन्त बढिया है .  .
(defrule cool6
(declare (salience 5500))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " cool.clp   cool6   "   ?id " baDiyA )" crlf))
)


;$$$ Modified by 14anu-ban-03 (04-03-2015)
;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 28/6/2014*****
;He was a cool guy.
;वह एक अच्छा लड़का था.
(defrule cool7
(declare (salience 5000))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
;(id-root ?id1 person|guy|man|woman|gal|girl|boy)   ;commented by 14anu-ban-03 (04-03-2015)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))  ;added by 14anu-ban-03 (04-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cool.clp 	cool7   "  ?id "  acCA )" crlf))
)


;@@@ Added by 14anu-ban-03 (04-03-2015)
;The little iron magnets inside it align themselves parallel to the magnetic field at that place as the basalt cools and solidifies. [ncert]
;jaba yaha TaNdA hokara Tosa meM baxalawA hE wo isake anxara ke Cote-Cote lOha-cumbaka cumbakIya kRewra kI xiSA ke samAnwara samareKiwa ho jAwe hEM. [ncert]
;jaba asiwASma TaNdA hokara Tosa meM baxalawA hE wo isake anxara ke Cote-Cote lOha-cumbaka cumbakIya kRewra kI xiSA ke samAnwara samareKiwa ho jAwe hEM. [self]
(defrule cool8
(declare (salience 4600))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 basalt)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaNdA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cool.clp 	cool8   "  ?id "  TaNdA_ho )" crlf))
)

;$$$ Modified by 14anu-ban-03 (04-03-2015)
;She was very angry but she's cooled down now. [same clp]
;vaha bahuwa gusse meM WI lekina aba vaha SAnwa hE. [same clp]
(defrule cool0
(declare (salience 5000))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)   	;commented by 14anu-ban-03 (04-03-2015)
(kriyA-subject ?id ?id2)  	;added by 14anu-ban-03 (04-03-2015)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  ;added by 14anu-ban-03 (04-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  SAnwa_ho))  ;meaning changed from 'TaNdA_kara' to 'SAnwa' by 14anu-ban-03 (04-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cool.clp	cool0  "  ?id "  " ?id1 "  SAnwa_ho  )" crlf))
)

;Added by human
(defrule cool1
(declare (salience 4900))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TaNdA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cool.clp	cool1  "  ?id "  " ?id1 "  TaNdA_kara  )" crlf))
)

(defrule cool2
(declare (salience 4800))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TaNdA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cool.clp	cool2  "  ?id "  " ?id1 "  TaNdA_ho  )" crlf))
)

(defrule cool3
(declare (salience 4700))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaMdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cool.clp 	cool3   "  ?id "  TaMdA )" crlf))
)

(defrule cool4
(declare (salience 4600))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIwala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cool.clp 	cool4   "  ?id "  SIwala )" crlf))
)

(defrule cool5
(declare (salience 4500))
(id-root ?id cool)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaNdA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cool.clp 	cool5   "  ?id "  TaNdA_kara )" crlf))
)

;"cool","VT","1.TaNdA_karanA"
;A swim in the pool should cool you(down) 
;--"2.kroXa_TaNdA_karanA"
;Cool down, do not be so angry.
;
;
;LEVEL 
;Headword : cool
;
;Examples --
;
;1.I am enjoying a cool autumn day.
;mEM SIwala Saraxa qwu ke xina kA Ananxa uTA rahA hUz.
;2.Cool greens && blues && violets.
;SIwala hare Ora nIle Ora bEMganI [raMga].
;3.Cool down, do not be so angry.
;SAMwa ho jAo, iwanA gussA mawa ho.
;4.She has a cool head.
;usakA ximAga SAMwa hE.
;5.He was given a cool reception.
;use acCe se svAgawa xiyA gayA WA.
;6.The bike cost me a cool fifty thousand.
;yaha sAikala muJe acCe se pacAsa hajZAra kI padZa gaI.
;
;vAkya 1,2 meM "cool" kA arWa "SIwala-TaMdA" EsA A rahA hE.
;vAkya 3,4 meM "cool" kA arWa "SAMwa" EsA A rahA hE.
;vAkya 5,6 meM "cool" kA arWa "acCA" EsA A rahA hE.
;
;uparaliKiwa vAkyoM meM "cool" ke jo Binna lagawe arWa A rahe hEM, unameM vAswava meM
;saMbaMXa hE.
;
;vAkya 1 Ora 2 meM A rahe arWa "SIwala-TaMdZA" ko viswqwa karane para vAkya 3 Ora 4 meM A 
;rahe arWa "SAMwa" meM usakI arWa CAyA prakata howI hE.
;wo aba hama vAkya 1-4 meM "cool" ke lie "SIwala-TaMdZA" kA arWa prayoga kara sakawe hEM.
;
;vAkya 5-6 meM "cool" kA arWa "acCe se" EsA A rahA hE.
;
;wo aba hama "cool" ke lie sUwra isa prakAra xe sakawe hE.
;
;anwarnihiwa sUwra ;
;
;TaMdA -- SIwala -jo garama nahIM yAni uwwejanA rahiwa --SAMwa/acCe_se 
;
;sUwra :TaMdA^SIwala/acCe_se
;
;
