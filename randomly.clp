;@@@ Added bu 14anu-ban-10 on (19-8-14)
;In a turbulent flow the velocity of the fluids at any point in space varies rapidly and randomly with time. 
;vikRubXa pravAha meM kisI biMxu para warala kA vega xruwa waWA yAxqcCika rUpa se samaya meM baxalawA rahawA hE. [ncert]
;vikRubXa pravAha meM kisI biMxu para warala kA vega xruwa waWA atakala puccU se samaya meM baxalawA rahawA hE.[self]
(defrule randomly0
(declare (salience 0000))
(id-root ?id randomly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id atakala_puccU_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  randomly.clp  randomly0  "  ?id "  atakala_puccU_ se)"crlf))
)

;@@@ Added by 14anu-ban-10 on (19-8-14)
;So what I'm going to do is randomly assign you
;अब मैं आप सब को यादृच्छिक तरीके से
(defrule randomly1
(declare (salience 200))
(id-root ?id randomly)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaka ?id1 ?id)(kriyA-kriyA_viSeRaNa ?id2 ?id))
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxqcCika_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  randomly.clp   randomly1   "  ?id "  yAxqcCika_rUpa_se )" crlf))
)

;(parser problem) category verb given by parser,reported parser problem by 14anu-ban-10 on (05-11-2014).
;removed 'kriyA-kriyA_viSeRaNa' relation
;@@@ Added bu 14anu-ban-10 on (25-8-14)
;Again on becoming normal stop it randomly .
;पुन: सामान्य होने पर जहाँ का तहाँ रोकें ।
(defrule randomly2
(declare (salience 500))
(id-root ?id randomly)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAz_kA_wahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  randomly.clp   randomly2   "  ?id "  jahAz_kA_wahAz )" crlf))
)
