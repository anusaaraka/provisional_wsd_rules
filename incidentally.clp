;@@@ Added by 14anu-ban-06 (18-02-2015)
;Incidentally, have you heard the news about Sue?(OALD)
;वैसे, आपने सू के बारे में समाचार सुना ? (manual)
;Incidentally, I wanted to have a word with you about your travel expenses.(cambridge)
;वैसे, मैं आपके साथ आपके यात्रा व्यय के बारे में बात करना चाहता हूँ .(manual)
(defrule incidentally0
(declare (salience 0))
(id-root ?id incidentally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vEse))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incidentally.clp 	incidentally0   "  ?id "  vEse )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-02-2015)
;The information was only discovered incidentally.(OALD)
;सूचना केवल संयोग से ढूँढ ली गयी थी . (manual)
(defrule incidentally1
(declare (salience 2000))
(id-root ?id incidentally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id2 ?id)
(kriyA-subject ?id2 ?id1)
(id-root ?id1 information|detail)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyoga_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incidentally.clp 	incidentally1   "  ?id "  saMyoga_se )" crlf))
)

