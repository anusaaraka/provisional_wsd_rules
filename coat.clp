;@@@ Added by 14anu-ban-03 (07-03-2015)
;My pet cat has a smooth coat. [hinkhoj]
;मेरी पालतू बिल्ली के एक कोमल बाल है . [manual]
(defrule coat3
(declare (salience 4900))
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 smooth)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp 	coat3   "  ?id "  bAla )" crlf))
)

;@@@ Added by 14anu-ban-03 (07-03-2015)
;He was wearing a leather coat. [hinkhoj]
;उसने एक चमडे की जाकेट पहनी हुई थी . [manual]
(defrule coat4
(declare (salience 4900))
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 leather)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAketa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp 	coat4   "  ?id "  jAketa )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-03-2015)
;I ate biscuits coated with chocolate. [same clp]
;मैंने चाकलेट से भरे हुए बिस्कुट खाए . [manual]
(defrule coat5
(declare (salience 5000))  
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bare_ho))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp 	coat5   "  ?id "  Bare_ho )" crlf))
)


;@@@ Added by Shruti Singh M.Tech(CS)Banasthali (26-08-2016)
;this white coat also forms on tongue.[health corpus]
;yaha saPexa parawa BT jIBa para banawI hE
(defrule coat7
(declare (salience 5000))
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 white)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp     coat7   "  ?id "  parawa )" crlf))
)


;$$$ Modified by 14anu-ban-03 (07-03-2015)
;Walls will need a double coat of paint. [hinkhoj]
;दीवारों के रङ्ग को एक दोहरी परत की आवश्यकता हैं. [manual]
(defrule coat0
(declare (salience 5000))
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id coating )       ;commented by 14anu-ban-03 (07-03-2015)
(viSeRya-of_saMbanXI ?id ?id1) ;added by 14anu-ban-03 (07-03-2015)
(id-root ?id1 paint)  ;added by 14anu-ban-03 (07-03-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id parawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  coat.clp  	coat0   "  ?id "  parawa )" crlf))
)


;-----------------Default Rules--------------------
(defrule coat1
(declare (salience 00))            ;salience reduced by 14anu-ban-03 (07-03-2015)
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kota))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp 	coat1   "  ?id "  kota )" crlf))
)

;"coat","V","1.lepanA"
;I ate biscuits coated with chocolate.
(defrule coat2
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (07-03-2015)
(id-root ?id coat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lepa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coat.clp 	coat2   "  ?id "  lepa )" crlf))
)

