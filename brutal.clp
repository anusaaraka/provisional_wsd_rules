;@@@Added by 14anu-ban-02(08-04-2015)
;Sailors sometimes faced brutal punishments like whipping.[mw]
;जहाजी ने कभी कभी चाबुक मारने जैसे कठोर दण्डों का सामना किया . [self]
(defrule brutal1 
(declare (salience 100)) 
(id-root ?id brutal) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 punishment|struggle)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kaTora)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brutal.clp  brutal1  "  ?id "  kaTora )" crlf)) 
) 

;@@@Added by 14anu-ban-02(08-04-2015)
;The writer describes the dangers of drugs with brutal honesty.[mw]
;लेखक कड़ुवी सच्चाई के साथ ड्रग के खतरे के बारे में बताता है . [self]
(defrule brutal2 
(declare (salience 100)) 
(id-root ?id brutal) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 honesty)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kadZavI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brutal.clp  brutal2  "  ?id "  kadZavI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(08-04-2015)
;The traffic was brutal on the way to work.[mw]	;run the sentence on parser no. 3
;काम पर जाने के मार्ग पर आवागमन कठिन था . [self]
(defrule brutal3
(declare (salience 100)) 
(id-root ?id brutal) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 traffic)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kaTina)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brutal.clp  brutal3  "  ?id "  kaTina )" crlf)) 
) 

;@@@Added by 14anu-ban-02(09-04-2015)
;I had a brutal headache this morning.[mw]
;मुझे आज सुबह तेज सिर दर्द  था . [self]
(defrule brutal4
(declare (salience 100)) 
(id-root ?id brutal) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 headache)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id wejZa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brutal.clp  brutal4  "  ?id "  wejZa )" crlf)) 
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(08-04-2015)
;Sentence: A brutal killing.[oald]
;Translation: निर्मम हत्या . [self]
(defrule brutal0 
(declare (salience 0)) 
(id-root ?id brutal) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nirmama)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brutal.clp  brutal0  "  ?id "  nirmama )" crlf)) 
) 


