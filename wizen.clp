
;@@@ Added by 14anu-ban-11 on (24-04-2015)
;Note:- working properly on parser no.6
;Wizened apples. (oald)
;सूखा हुअा सेब . (self)
(defrule wizen1
(declare (salience 10))
(id-root ?id wizen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 apple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUKA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wizen.clp 	wizen1   "  ?id "  sUKA_ho)" crlf))
)

;----------------------------------- Default Rules -------------------------------

;@@@ Added by 14anu-ban-11 on (24-04-2015)
;Note:- working properly on parser no. 15
;Her old wizened face.(oald)
;उसका झुर्रीदार चेहरा .(self)
(defrule wizen0
(declare (salience 00))
(id-root ?id wizen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JurrIxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wizen.clp    wizen0   "  ?id "  JurrIxAra)" crlf))
)


