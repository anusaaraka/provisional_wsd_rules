;@@@ Added by 14anu-ban-11 on (05-12-2014)
;Pick up one of our free information sheets at reception.(oald)
;Reception से  हमारे स्वतन्त्र सूचना पत्रों में से एक उठा लीजिए . (manual)
(defrule sheet1
(declare (salience 10))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet1   "  ?id "  pawra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (05-12-2014)
;A sheet of A4 size.(oald)
;A4 आकार का कागज . (manual)
(defrule sheet2
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 A4)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAgaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet2   "  ?id "  kAgaja)" crlf))
)

;@@@ Added by 14anu-ban-11 on (05-12-2014)
;The road was covered with a sheet of ice. (oald)
;सडक बरफ की परत से ढक  गयी थी .(manual)
(defrule sheet3
(declare (salience 40))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 ice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet3   "  ?id "  parawa)" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-02-2015)
;The application form was a single sheet of paper.[cald]
;आवेदन पत्र कागज का एक पन्ना था . [self]
(defrule sheet4
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 paper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pannA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet4   "  ?id "  pannA)" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-02-2015)
;The rain was coming down in sheets.[cald]
; बहुत अधिक मात्रा में वर्षा/बारिश हो रही थी .  [self]
(defrule sheet5
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id2 rain)
(kriyA-subject  ?id1 ?id2)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_aXika_mAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet5   "  ?id "  bahuwa_aXika_mAwrA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-02-2015)
;Start each answer on a fresh sheet.[oald]
;हर उत्तर एक  नए पृष्ठ से शुरू करें .  [self]
(defrule sheet6
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 fresh|new|different|clean|blank)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pqRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet6   "  ?id "  pqRTa)" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-02-2015)
;Place the dough on a baking sheet.[oald]
;साने हुए आटे को अवन की ट्रे पर  रखिए.  [self]
(defrule sheet7
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id1 baking)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 avana_kI_tre))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sheet.clp 	sheet7  "  ?id "    " ?id1 " avana_kI_tre)" crlf))

)


;@@@ Added by 14anu-ban-01 on (19-02-2015)
;A sheet of flame shot up into the air immediately after the explosion.[cald]
;विस्फोट होने के ठीक बाद  दहकती हुई ज्वाला की लपट तेज़ी से हवा में बढी .  [self]
(defrule sheet8
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 flame|fire|blaze)
(viSeRya-of_saMbanXI  ?id ?id1)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xahakawI_huI_jvAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet8   "  ?id "  xahakawI_huI_jvAlA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-02-2015)
;No government operates with a completely clean sheet.[oald]
;कोई भी सरकार पूरी तरह से दोषरहित/निष्कलंक काम नहीं करती . [self]
(defrule sheet9
(declare (salience 30))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id2 operate|work|act|behave)
(kriyA-with_saMbanXI  ?id2 ?id)
(id-root ?id1 clean|blank)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id3 with)
(pada_info (group_head_id ?id)(preposition  ?id3))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id3 xoRarahiwa/niRkalaMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sheet.clp 	sheet9  "  ?id "    " ?id1 " " ?id3 "  xoRarahiwa/niRkalaMka)" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-02-2015)
;They kept a clean sheet in the match.[oald]
;उन्होनें मैच में [अपने] प्रतिद्वंद्वियों का स्कोर निल/शून्य रखा.  [self] 
(defrule sheet10
(declare (salience 40))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 match|game)
(kriyA-in_saMbanXI  ?id1 ?id2)
(kriyA-object  ?id1 ?id)
(id-root ?id3 clean)
(viSeRya-viSeRaNa  ?id ?id3)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id3 prawixvaMxviyoM_kA_skora_SUnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sheet.clp 	sheet10  "  ?id "   " ?id3 "  prawixvaMxviyoM_kA_skora_SUnya)" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-02-2015)
;We can't go out yet, it's sheeting down outside.[cald]
;हम अभी भी बाहर नहीं जा सकते,बाहर मूसलाधार वर्षा हो रही है . [self]
(defrule sheet11
(declare (salience 40))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) down)
(id-root ?id1 outside)
(kriyA-aXikaraNavAcI  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) mUsalAXAra_varRA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sheet.clp 	sheet11  "  ?id "   " =(+ ?id 1) "  mUsalAXAra_varRA_ho)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (05-12-2014)
;Have you changed the sheets?(oald)
;क्या आप चादरें बदल चुके हैं?(manual)
(defrule sheet0
(declare (salience 0))
(id-root ?id sheet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAxara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheet.clp 	sheet0   "  ?id "  cAxara)" crlf))
)

