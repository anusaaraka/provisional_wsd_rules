
;@@@ Added by 14anu-ban-09 on (25-03-2015)  ;NOTE:- Parser problem. Run on parser no. 2.
;He doused himself in kerosene oil and lighted fire to commit suicide.	[Hinkhoj]
;आत्महत्या करने के लिए उसने स्वयं पर मिट्टी के तेल डाला और  आग लगा ली   .        [Manual]         
(defrule oil2
(declare (salience 6000))
(id-root ?id oil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-word ?id1 coconut|kerosene|Coconut|Kerosene|soya|Soya|Groundnut|groundnut|sunflower|Sunflower|Almond|almond|Sesame|sesame)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  oil.clp 	oil2   "  ?id "  - )" crlf))
)

(defrule oil0
(declare (salience 5000))
(id-root ?id oil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  oil.clp 	oil0   "  ?id "  wela )" crlf))
)

(defrule oil1
(declare (salience 4900))
(id-root ?id oil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wela_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  oil.clp 	oil1   "  ?id "  wela_lagA )" crlf))
)

;"oil","VT","1.wela lagAnA"
;Oil the wooden surface.
;
;
