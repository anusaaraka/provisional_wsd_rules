;$$$ Modified by 14anu-ban-06 (28-04-2015)
;@@@ Added by 14anu-ban-10 on (23-02-2015)
;Politicians involved in sex romps with call girls.[oald]
;राजनीतिज्ञों का संबद्ध यौन रतिक्रिया वेश्यिौ के साथ .[manual]
;वेश्याओ के साथ यौन रतिक्रिया में संबद्ध राजनीतिज्ञ.(manual);added by 14anu-ban-06 (28-04-2015)
(defrule romp2
(declare (salience 5100))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1);added 'id1' by 14anu-ban-06 (28-04-2015)
(id-root ?id1 sex);added by 14anu-ban-06 (28-04-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rawikriyA));meaning changed from 'rawikirayA' 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romp.clp 	romp2  "  ?id "  rawikriyA)" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-02-2015)
;His latest film is an enjoyable romp.[oald]
; उसकी  नयी  फ़िल्म  का  आनन्ददायक दिलचस्प अनुभव  है . [manual]
(defrule romp3
(declare (salience 5200))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 film)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilacaspa_anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romp.clp 	romp3  "  ?id "  xilacaspa_anuBava)" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-02-2015)
; They won in a 5–1 romp.[oald]
; वे 5-1 से आसानी से जीते .[manual]
(defrule romp4
(declare (salience 5300))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsAnI_se_jI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romp.clp 	romp4  "  ?id " AsAnI_se_jI )" crlf))
)

;$$$ Modified by 14anu-ban-06 (28-04-2015)
;@@@ Added by 14anu-ban-10 on (23-02-2015)
;Their horse romped home in the 2 o'clock race.[oald]
;उनके घोडे ने 2 बजे कि दौड में  आसानी से विजय पायी .[manual]
;उनका घोडा 2 बजे की दौड में आसानी से विजय हुआ .(manual) ;added by 14anu-ban-06 (28-04-2015)
(defrule romp5
(declare (salience 5300))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1);added by 14anu-ban-06 (28-04-2015)
(id-root ?id1 race);added by 14anu-ban-06 (28-04-2015)
(id-root =(+ ?id 1) home)
;(kriyA-object  ?id ?id1 );commented by 14anu-ban-06 (28-04-2015)
;(id-root ?id1 home);commented by 14anu-ban-06 (28-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id AsAnI_se_vijayI_ho));commented and meaning changed from 'AsAnI_se_vijaya_pAyI' to 'AsAnI_se_vijayI_ho' by 14anu-ban-06 (28-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  AsAnI_se_vijayI_ho));added by 14anu-ban-06 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " romp.clp	 romp5  "  ?id "  " (+ ?id 1)  " AsAnI_se_vijayI_ho  )" crlf));added by 14anu-ban-06 (28-04-2015)
)

;$$$ Modified by 14anu-ban-06 (28-04-2015)
;@@@ Added by 14anu-ban-10 on (23-02-2015)
;She romped through the exam questions.[oald] [parser no.- 5]
;वह परीक्षा प्रश्नों के द्वारा आसानी से सफल हुई . [manual]
;वह परीक्षा प्रश्नों में आसानी से सफल हुई .(manual);added by 14anu-ban-06 (28-04-2015)
(defrule romp6
(declare (salience 5400))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-through_saMbanXI  ?id ?id1 );commented by 14anu-ban-06 (28-04-2015)
;(id-root ?id1  question);commented by 14anu-ban-06 (28-04-2015)
(id-word ?id1 through);added by 14anu-ban-06 (28-04-2015)
(kriyA-upasarga ?id ?id1);added by 14anu-ban-06 (28-04-2015)
(kriyA-object ?id ?);added by 14anu-ban-06 (28-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id AsAnI_se_saPala_ho));commneted and meaning changed from 'AsAnI_se_saPala_huI' to 'AsAnI_se_saPala_ho' by 14anu-ban-06 (28-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AsAnI_se_saPala_ho));added by 14anu-ban-06 (28-04-2015)
(assert (kriyA_id-object_viBakwi ?id meM));added by 14anu-ban-06 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " romp.clp	romp6  "  ?id "  " ?id1 "  AsAnI_se_saPala_ho  )" crlf);added by 14anu-ban-06 (28-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  romp.clp      romp6   "  ?id " meM )" crlf));added by 14anu-ban-06 (28-04-2015)
)

;@@@ Added by 14anu-ban-06 (28-04-2015)
;Davis romped away to win the championship.(OALD)
;डॆवस ने प्रतियोगिता जीतने के लिए दक्षता बढायी . (manual)
(defrule romp7
(declare (salience 5500))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xakRawA_baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " romp.clp	romp7  "  ?id "  " ?id1 "  xakRawA_baDA  )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-04-2015)
;Share prices continue to romp ahead. (OALD) [parser no.- 4]
;शेयर मूल्यों में वृद्धि होना जारी रहता है . (manual)
(defrule romp8
(declare (salience 5500))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 ahead)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vqxXi_ho))
(assert (kriyA_id-subject_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " romp.clp	romp8  "  ?id "  " ?id1 "  vqxXi_ho  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  romp.clp      romp8   "  ?id " meM )" crlf))
)

;------------------------ Default Rules ----------------------

;"romp","N","1.uCala_kUxa"
;Children are having a romp in the party.
(defrule romp0
(declare (salience 5000))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uCala_kUxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romp.clp 	romp0   "  ?id "  uCala_kUxa )" crlf))
)

;"romp","VI","1.uCala_kUxa_karanA"
;All the puppies are romping around in the garden.
(defrule romp1
(declare (salience 4900))
(id-root ?id romp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uCala_kUxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romp.clp 	romp1   "  ?id "  uCala_kUxa_kara )" crlf))
)

;"romp","N","1.uCala_kUxa"
;Children are having a romp in the party.
;--"2.majexAra_Pilma[kaWA/puswaka]"
;The latest film of Basu Chatterji is an enjoyable romp.
;
;"romp","VI","1.uCala_kUxa_karanA"
;All the puppies are romping around in the garden.
