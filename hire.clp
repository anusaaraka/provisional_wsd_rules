;FILE MODIFIED ON 26.8.09 (MEENA)




;Modified by Meena(3.3.10)
;Added by Meena(26.8.09)
;I told Margaret that I thought she would probably be hired . 
;He is being hired by another company .
(defrule hire1
(declare (salience 5000))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?)
(kriyA-subject ?id ?id1)
(or(kriyA-by_saMbanXI ?id ?id2)(kriyA-kriyA_viSeRaNa  ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_xe))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hire.clp    hire1   "  ?id "  kAma_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hire.clp    hire1   "  ?id " ko )" crlf))
)



;@@@Added by 14anu18 (23-06-14)
;We hired him as our plumber.
;हमने हमारे नलसाज की तरह उसको नौकरी पर रखा . 
(defrule hire3
(declare (salience 4900))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (gdbm_lookup_p "animate.gdbm" ?str) TRUE))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI_para_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hire.clp 	hire3   "  ?id "  nOkarI_para_raKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-01-2015)
;Hire a private taxi from there till Auli .(parallel corpus)
;वहाँ  से  औली  के  लिए  प्राइवेट  टैक्सी  किराये पर लीजिए .(parallel corpus) 
;They hire a room on the second floor .(COCA)
;उन्होनें दूसरी मंज़िल पर कमरा किराये पर लिया.(manual) 
(defrule hire4
(declare (salience 5100))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 taxi|cab|room)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kirAye_para_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hire.clp 	hire4   "  ?id "  kirAye_para_le )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-02-2015)
;How much do you charge for hiring out a bicycle for a week? (cambridge)
;आप एक सप्ताह  के लिए साइकिल किराये पर लेने के लिए कितनी कीमत लेते हैं?  (manual)
(defrule hire5
(declare (salience 5100))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 out)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kirAye_para_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  hire.clp 	hire5  "  ?id "  " ?id1 " kirAye_para_xe )" crlf))
)
;------------------------- Default rules ------------------------------

;$$$ Modified by 14anu-ban-06 (14-11-2014)
;In 1931, Ford hired chemists Robert Boyer and Frank Calvert to produce artificial silk.(agriculture) ;added by 14anu-ban-06 (14-11-2014)
;1931 में,कृत्रिम रेशम का उत्पादन करने के लिए फोर्ड  ने रसायनज्ञ रॉबर्ट बोयर  और फ्रैंक कैल्वर्ट को  काम पर रखा  था.(manual)
;Salience reduced by Meena(3.3.10)
(defrule hire2
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_para_raKa));meaning changed from 'majaxUrI_para_raKa' to 'kAma_para_raKa' by 14anu-ban-06 (14-11-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hire.clp 	hire2   "  ?id "  kAma_para_raKa )" crlf))
)

;"hire","V","1.majaxUrI_para_raKanA/kirAye_para_lenA"
;usane vaha rikSA 'hire'(kirAye para) liyA huA hE.
;

(defrule hire0
(declare (salience 5000))
(id-root ?id hire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kirAyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hire.clp 	hire0   "  ?id "  kirAyA )" crlf))
)

;"hire","N","1.kirAyA/BAdZA"
;aprela mAha se naye rela mAlaBAdZA 'hire'(kirAyA/BAdZA ) lAgU ho jAyagA.


