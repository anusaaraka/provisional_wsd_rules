
(defrule plan0
(declare (salience 5000))
(id-root ?id plan)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id planning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id yojanA2))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  plan.clp  	plan0   "  ?id "  yojanA2 )" crlf))
)

;"planning","N","1.yojanA2"
(defrule plan1
(declare (salience 4900))
(id-root ?id plan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA_banA))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plan.clp 	plan1   "  ?id "  yojanA_banA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  plan.clp      plan1   "  ?id " kI )" crlf)
)
)

;$$$ Modified by 14anu-ban-09 on (27-01-2015)
;Changed meaning from 'yojanA_banAne_meM' to 'yojanA'.
;@@@ Added by 14anu20 dated 16/06/2014
;They can help her plan for study.
;वे उनको अध्ययन के लिए योजना बनाने में सहायता कर सकते हैं 
;वे उनको अध्ययन की योजना में सहायता कर सकते हैं. 	[Self]	;Translation added by 14anu-ban-09 on (27-01-2015) 
(defrule plan02
(declare (salience 6000))
(id-root ?id plan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)	;Added by 14anu-ban-09 on (27-01-2015)
;(kriyA-vAkyakarma  ?id1 ?id)	;Commented by 14anu-ban-09 on (27-01-2015)
(id-root ?id1 help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA))	;Modified 'yojanA_banAne_meM' to 'yojanA' by 14anu-ban-09 on (27-01-2015) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plan.clp 	plan02   "  ?id "  yojanA )" crlf)
)							;Modified 'yojanA_banAne_meM' to 'yojanA' by 14anu-ban-09 on (27-01-2015)
)

(defrule plan2
(declare (salience 4800))
(id-root ?id plan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plan.clp 	plan2   "  ?id "  yojanA )" crlf))
)

;"plan","N","1.yojanA"
;The Government should make a plan to bring up the poor.
;--"2.mApaciwra"
;A building plan was approved by the authority.
;
;
