;@@@ Added by 14anu-ban-10 on (25-02-2015)
;Free radicals in the body are thought to be one of the causes of diseases such as cancer.[oald]]
;शरीर में  मुक्त मूलक को सोचा जाता हैं  कई  कारणों में से एक  बीमारियों का कारण  जैसे की कैंसर . [manual]
(defrule radical2
(declare (salience 5100))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 free)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical2   "  ?id "  mUlaka )" crlf))
)
;@@@ Added by 14anu-ban-10 on (25-02-2015)
;Political radicals.[oald]
;राजनैतिक उग्र सुधारवादी.[manual]
(defrule radical3
(declare (salience 5200))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 political)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ugra_suXAravAxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical3   "  ?id "  ugra_suXAravAxI)" crlf))
)
;@@@ Added by 14anu-ban-10 on (25-02-2015)
;Radical ideas.[oald]
;आरम्भिक  योजना.[manual]
(defrule radical4
(declare (salience 5300))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 idea)
(id-cat_coarse ?id number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AramBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical4  "  ?id "  AramBika)" crlf))
)
;@@@ Added by 14anu-ban-10 on (25-02-2015)
;Our government should bring a radical change in the foreign policy.[hinkhoj]
;हमारी सरकार को विदेशी नीति में एक सुधारवादी  बदलाव लाना चाहिए .[manual] 
(defrule radical5
(declare (salience 5400))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 change)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXAravAxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical5  "  ?id "  suXAravAxI)" crlf))
)
;@@@ Added by 14anu-ban-10 on (25-02-2015)
;He is a person with radical opinions.[hinkhoj]
;वह व्यक्ति मूल शब्द   मान्यता वाला है .[manual] 
(defrule radical6
(declare (salience 5400))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 opinion)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla_Sabxa ))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical6  "  ?id "  mUla_Sabxa)" crlf))
)

;------------------------ Default Rules ----------------------

;"radical","Adj","1.mUla_[BUwa]_viRayaka"
(defrule radical0
(declare (salience 5000))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla_viRayaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical0   "  ?id "  mUla_viRayaka )" crlf))
)

;"radical","N","1.mUla"
(defrule radical1
(declare (salience 4900))
(id-root ?id radical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  radical.clp 	radical1   "  ?id "  mUla )" crlf))
)



;"radical","Adj","1.mUla_[BUwa]_viRayaka"
;--"2.mOlika"
;We need a thorough radical changes in our society.
;--"3.suXAravAxI"
;Our government should bring a radical change in the foreign policy. 
;--"4.awivAxI"
;He is a person with radical opinions.
