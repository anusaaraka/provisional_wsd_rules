;@@@ Added by 14anu-ban-03 (17-04-2015)
;You shouldn't have done it without consulting me. [oald]
;आपको मुझसे विचार विमर्श किए बिना इसको नहीं करना चाहिये था . [manual]
(defrule consult1
(declare (salience 10))
(id-root ?id consult)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-without_saMbanXI ?id2 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra_vimarSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consult.clp  consult1  "  ?id "  vicAra_vimarSa_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (17-04-2015)
;He consulted the manual. [oald]
;उसने नियम पुस्तिका की सहायता ली . [manual]
(defrule consult2
(declare (salience 10))
(id-root ?id consult)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 manual)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id sahAyawA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  consult.clp  consult2  "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consult.clp  consult2  "  ?id "  sahAyawA_le )" crlf))
)

;----------------------------------------Default rule-------------------------------------------------

;@@@ Added by 14anu-ban-03 (17-04-2015)
;If the pain continues, consult your doctor. [oald]
;यदि दर्द जारी रहता है, तो आपने डाक्टर की राय लीजिए . [manual]
(defrule consult0
(declare (salience 00))
(id-root ?id consult)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id rAya_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  consult.clp  consult0  "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consult.clp  consult0   "  ?id "  rAya_le )" crlf))
)


