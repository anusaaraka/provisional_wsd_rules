
(defrule near0
(declare (salience 5000))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near0   "  ?id "  nikata )" crlf))
)

;"near","Adj","1.nikata"
;His school is very near.
;They are his nearest relatives.
;The drought has brought near satrvation in the village.
;--"2.milawA_julawA"
;This is the nearest colour that you can get.
;
(defrule near1
(declare (salience 4900))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near1   "  ?id "  ke_pAsa )" crlf))
)

;"near","Adv","1.ke_pAsa"
;Don't go any nearer.
;--"2.lagaBaga"
;She gave a near perfect performance.
;
(defrule near2
(declare (salience 4800))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_nikata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near2   "  ?id "  ke_nikata )" crlf))
)

;modified nikata as ke_nikata by manju
;"near","Prep","1.nikata"
;She was standing near the door.
;
(defrule near3
(declare (salience 4700))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikata_pahuzca))
(assert (kriyA_id-object_viBakwi ?id ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near3   "  ?id "  nikata_pahuzca )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  near.clp      near3   "  ?id "  ke )"  crlf)
)
)

;"near","VI","1.nikata_pahuzcanA"
;He is nearing his death.
;

;@@@Added by 14anu-ban-08 (01-04-2015)
;The conflict is unlikely to be resolved in the near future .  [oald]
;यह लड़ाई अविश्वसनीय है कि आनेवाले  भविष्य में सुलझ जाए.  [self]
(defrule near4
(declare (salience 5001))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 future|disaster)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AnevAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near4   "  ?id "  AnevAlA )" crlf))
)

;@@@Added by 14anu-ban-08 (01-04-2015)
;He was the nearest thing to a father.  [oald]
;वह पापा के सबसे करीब था.  [self]
(defrule near5
(declare (salience 5002))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 thing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near5   "  ?id "  karIba )" crlf))
)

;@@@Added by 14anu-ban-08 (01-04-2015)
;She spoke in a near whisper.  [oald]
;वह पास आके धीरे से बोली.  [self]
(defrule near6
(declare (salience 5002))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 whisper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near6   "  ?id "  pAsa )" crlf))
)

;@@@Added by 14anu-ban-08 (02-04-2015)
;The project is nearing completion.  [oald]
;परियोजना समाप्ति के करीब पहुँचने वाली हैं.  [self]
(defrule near7
(declare (salience 5001))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 project)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karIba_pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near7   "  ?id "  karIba_pahuzca)" crlf))
)


;@@@Added by 14anu-ban-08 (02-04-2015)
;As Christmas neared, the children became more and more excited.  [oald]
;जैसे-जैसे क्रिसमस पास आता है बच्चे अत्यधिक उत्तेजित हो जाते है.  [self]
(defrule near8
(declare (salience 5005))
(id-root ?id near)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 Christmas|diwali|holi|festival|birthday)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  near.clp 	near8   "  ?id "  pAsa_A)" crlf))
)

