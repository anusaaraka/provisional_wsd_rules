;Added by sheetal(09-09-09)
(defrule sh-fly0
(declare (salience 5000))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flying)
(id-cat_coarse ?id verbal_noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id udAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fly.clp       sh-fly0   "  ?id "  udAna )" crlf))
)
;I have always been afraid of flying .
;mEM #hameSA #udAna se #BayaBIwa ho cukA hUz

;@@@ Added by 14anu01 on 24-06-2014
; Flies are harmful.
;मक्खियाँ हानिकारक हैं
(defrule fly1
(declare (salience 5000))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flies)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id makKI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fly.clp       fly1   "  ?id "  makKI)" crlf))
)

;@@@Added by 14anu19 (04-07-2014)
;I'm flying to Bombay tomorrow.
;मैं कल बॉम्बे रवाना हो रहा हूँ
(defrule fly2
(declare (salience 5001))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) to)
(kriyA-to_saMbanXI  ?id ?id1)
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id ravAnA_ho))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) ravAnA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fly.clp  fly2  "  ?id "  " =(+ ?id 1) "  ravAnA_ho  )" crlf))
)

;@@@Added by 14anu-ban-05 on (20-03-2015)
;Marching soldiers , group playing bands of brass , tanks and armored cars , colorful groups riding on camels , folk dancers , colorful jhankis and the fly past of the Air Force together make this the most amazing program .[TOURISM]
;mArca karawe sEnika , pIwala ke bEMda bajAwe samUha , tEMka Ora baKwarabaMxa gAdZiyAz , UztoM para savAra raMgabireMge samUha , loka narwaka , skUlI bacce , raMgIna JAzkiyAz Ora vAyusenA kA PlAI-pAsta (udZAna) eka sAWa milakara ise varRa kA sarvAXika ASZcaryajanaka kAryakrama banA xewe hEM .[MANUAL]

(defrule fly3
(declare (salience 5002))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 past)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 udZAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fly.clp  fly3  "  ?id "  " ?id1 "  udZAna  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-03-2015)
;Cathy flew past me in the corridor.[CALD]
;Cathy flew by me in the corridor.[CALD]
;कैथी गलियारे में मेरे बगल से तेज़ रफ्तार से निकली.	[manual]

(defrule fly4
(declare (salience 5003))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-past_saMbanXI  ?id ?)(kriyA-by_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZa_raPwAra_se_nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fly.clp       fly4   "  ?id "  wejZa_raPwAra_se_nikala)" crlf))
)


;@@@ Added by 14anu-ban-05 on (20-03-2015)
;He flew at me without warning.	[OALD]
;उसने बिना चेतावनी के मुझपर  अचानक हमला कर दिया. [manual]

(defrule fly5
(declare (salience 5004))
(id-root ?id fly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acAnaka_hamalA_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fly.clp       fly5   "  ?id "  acAnaka_hamalA_kara_xe)" crlf))
)
