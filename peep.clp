
(defrule peep0
(declare (salience 5000))
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-in_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAzka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " peep.clp peep0 " ?id "  JAzka )" crlf)) 
)

(defrule peep1
(declare (salience 4900))
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 JAzka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " peep.clp	peep1  "  ?id "  " ?id1 "  JAzka  )" crlf))
)

(defrule peep2
(declare (salience 4800))
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cIM-cIM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peep.clp 	peep2   "  ?id "  cIM-cIM )" crlf))
)

;"peep","N","1.cIM-cIM"
;I heard a peep from the floor which I late discovered were mice.
;--"1.JAzkI"
;The girl took a peep through the bedroom door to make sure that baby is asleep in her cot.
;

;$$$ Modified by 14anu-ban-06 (30-04-2015)
;The class girls had been silent for the entire period without peeping.(peep.clp)[parser np.-3]
;कक्षा की लडकियाँ बिना चीं चीं किये सम्पूर्ण अवधि के लिए चुप रही थीं . (manual);added by 14anu-ban-06 (30-04-2015)
(defrule peep3
(declare (salience 4800));salience increased from '4700' by 14anu-ban-06 (30-04-2015)
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-without_saMbanXI ? ?id);added by 14anu-ban-06 (30-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cIM-cIM_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peep.clp 	peep3   "  ?id "  cIM-cIM_kara )" crlf))
)

;"peep","V","1.cIM-cIM_karanA"
;The class girls had been silent for the entire period with out peeping.
;--"1.JAzkanA"
;The man was peeping through the hole.
;

;$$$ Modified by 14anu-ban-06 (30-04-2015)
;@@@ Added by 14anu-ban-09 on (31-01-2014)
;The man was peeping through the hole.		[w3dictionary]
;आदमी छेद से झाँख रहा था.				[Self]
;आदमी छेद से झाँक रहा था.(manual);added by 14anu-ban-06 (30-04-2015)
(defrule peep4
(declare (salience 4700))
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-through_saMbanXI ?id ?);commented by 14anu-ban-06 (30-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAzka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peep.clp 	peep4   "  ?id "  JAzka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-01-2015)
;On the way , the train passing through the dense forest and sky-touching hills , taking u-turn and mountains peeping through the clouds present a hypnotising scene .	[Total_tourism]
;रास्ते में घने जंगल एवं गगनचुम्बी पहाड़ियों के बीच से गुजरती ट्रैन , यू-टर्न लेती ट्रैन एवं बादलों के बीच से झाँकते पहाड़ मनमोहक दृश्य उपस्थित करते हैं .									[Total_tourism]

(defrule peep5
(declare (salience 4900))
(id-root ?id peep)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAzka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peep.clp 	peep5   "  ?id "  JAzka )" crlf))
)


