;@@@ Added by 14anu03 on 26-june-2014
;He killed everyone but me.
;उसने हर किसीको मारा परंतु मुझे नहीं. 
(defrule but100
(declare (salience 5500))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 me)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 paraMwu_muJe_nahIM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  but.clp     but100   "  ?id "  " ?id1 "  paraMwu_muJe_nahIM )" crlf))
)

;@@@ Added by 14anu-ban-02(20-02-2015)
;Thus, a dimensionally correct equation need not be actually an exact (correct) equation, but a dimensionally wrong (incorrect) or inconsistent equation must be wrong.          [ncert 11_02]
;इस प्रकार कोई विमीय रूप से सही समीकरण आवश्यक रूप से यथार्थ (सही) समीकरण नहीं होती, जबकि विमीय रूप से गलत या असङ्गत समीकरण गलत होनी चाहिए.      [ncert]
(defrule but5
(declare (salience 5000))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id ?id1 ?id2)
(subject-subject_samAnAXikaraNa  ? ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jabaki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but5   "  ?id "  jabaki )" crlf))
)
;@@@Added by 14anu-ban-02(03-03-2015)
;Inside the sealed plastic, his bread and cheese was squashed but fresh.[coca]
;बंद प्लास्टिक के अन्दर,उसके ब्रेड और चीज मसल जाने के बावजूद  ताजे थे.[self] 
(defrule but6
(declare (salience 5000))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id ?id1 ?id2)
(subject-subject_samAnAXikaraNa  ?id3 ?id1)
(id-root ?id3 bread|cheese)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAvajUxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but6   "  ?id "  ke_bAvajUxa )" crlf))
)


(defrule but0
(declare (salience 5000))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kevala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but0   "  ?id "  kevala )" crlf))
)

;"but","Adv","1.kevala"
;He is but a boy.
;
(defrule but1
(declare (salience 4900))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paranwu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but1   "  ?id "  paranwu )" crlf))
)

;"but","Conj","1.paranwu"
;Sonu eats sweets but Sweety doesn't.
;
(defrule but2
(declare (salience 4800))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_awirikwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but2   "  ?id "  ke_awirikwa )" crlf))
)

;"but","Prep","1.ke_awirikwa"
;Nobody but only you could do that.
;
(defrule but3
(declare (salience 4700))
(id-root ?id but)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paraMwu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp 	but3   "  ?id "  paraMwu )" crlf))
)

;Added by sheetal(13-12-2009).
;(defrule but4
;(declare (salience 4850))
;(id-root ?id but)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id preposition)
;(link_name-lnode-rnode MVp ?id1 ?id)
;(id-cat_coarse ?id1 verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id parnwu))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  but.clp       but4   "  ?id "  parnwu )" crlf))
;)
;They speak neither French nor German but a curious mixture of the two.(see in 3rd linkage.)

