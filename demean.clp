;@@@ Added by 14anu-ban-04 (26-03-2015)
;The entire family was demeaned by his behaviour.        [cald]
;उसके बर्ताव से सम्पूर्ण परिवार  अपमानित हो गया था .                   [self]
(defrule demean2
(declare (salience 20))
(id-root ?id demean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apamAniwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  demean.clp 	demean2   "  ?id "  apamAniwa_ho )" crlf))
)

;"demeaning","Adj","1.gOrava_kama_honA"
;She found it very demeaning to work under their contract.
(defrule demean0
(declare (salience 5000))
(id-root ?id demean)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id demeaning )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gOrava_kama_honA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  demean.clp  	demean0   "  ?id "  gOrava_kama_honA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (26-03-2015)
;Man always tries to demean women.                          [same clp file]
;puruRa hameSA mahilAoM ko nIcA xiKAne kI koSiSa karawe hE.                  [same clp file]
(defrule demean1
(declare (salience 100))                   ;salience reduced from '4900' to '100' by 14anu-ban-04 (26-03-2015)
(id-root ?id demean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive ? ?id)                      ;added by 14anu-ban-04 (26-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIcA_xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  demean.clp 	demean1   "  ?id "  nIcA_xiKA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (26-03-2015)
;Such images demean women.                 [olad]                     
;ऐसी प्रतिमाएँ स्त्रियों को अपमानित करती हैं .            [self]
(defrule demean3
(declare (salience 10))
(id-root ?id demean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apamAniwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  demean.clp 	demean3   "  ?id "  apamAniwa_kara )" crlf))
)

;"demean","VT","1.nIcA_xiKAnA"
;Man always tries to demean women.
;puruRa hameSA mahilAoM ko nIcA xiKAne kI koSiSa karawe hE.
;
;
