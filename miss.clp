
(defrule miss0
(declare (salience 5000))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id missing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id KoyA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  miss.clp  	miss0   "  ?id "  KoyA_huA )" crlf))
)

;"missing","Adj","1.KoyA huA"
;The missing tape was found intact.
;
;
(defrule miss1
(declare (salience 4900))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cUka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " miss.clp	miss1  "  ?id "  " ?id1 "  cUka  )" crlf))
)

;You've missed out the last chance in this game.
;
(defrule miss2
(declare (salience 4800))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAWa_se_jAne_xe));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " miss.clp miss2 " ?id "  hAWa_se_jAne_xe )" crlf)) 
)

(defrule miss3
(declare (salience 4700))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hAWa_se_jAne_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " miss.clp	miss3  "  ?id "  " ?id1 "  hAWa_se_jAne_xe  )" crlf))
)

(defrule miss4
(declare (salience 4600))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kumArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  miss.clp 	miss4   "  ?id "  kumArI )" crlf))
)

;"miss","N","1.kumArI"
;She is a gorgeous miss.
;
(defrule miss5
(declare (salience 4500))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 money)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ko_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  miss.clp 	miss5   "  ?id "  Ko_jA )" crlf))
)

;He thought that she may have missed the train.
(defrule miss6
(declare (salience 4400))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cUka_jA))
(assert (kriyA_id-subject_viBakwi ?id  0))  ;He thought that she may have missed the train.(Suggested by Sukhada 11-03-11)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  miss.clp 	miss6   "  ?id "  cUka_jA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  miss.clp    miss6   "  ?id " 0 )" crlf)
)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 02.06.2014 email-id:sahni.gourav0123@gmail.com
;sentence:I will miss my sister.
;hindi translation:मैं मेरी बहन की कमी महसूस करूँगा . 
(defrule miss7
(declare (salience 5000))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(or (id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str))))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI_mahasUsa_kara))
(assert (kriyA_id-object_viBakwi ?id  kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  miss.clp 	miss7   "  ?id "  kamI_mahasUsa_karawA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  miss.clp    miss7   "  ?id " kI )" crlf)
)

;"miss","V","1.cUka_jAnA"
;She would never miss the target.
;--"2.CUta_jAnA"
;She missed the train.
;He missed the appointment.
;--"3.kamI_mahasUsa_karanA"
;I miss my sister.
;--"4.asaPala_honA"
;He was sure he would not miss this time.
;

;@@@Added by 14anu19 (28-06-2014)
;Vishakha never missed a sermon given by the Master in Shravasti .
;विशाखा ने दी हुई श्रावस्ती में मास्टर के द्वारा धर्मोपदेश कभी नहीं छोडा .
(defrule miss8
(declare (salience 4500))
(id-root ?id miss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-word ?id1 sermon|class)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  miss.clp     miss8   "  ?id "  Coda )" crlf))
)

