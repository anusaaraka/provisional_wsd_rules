;$$$ Modified by 14anu-ban-04 (09-02-2015)
;The sky darkened as thick smoke billowed from the blazing oil well.               [cald]
;आसमान काला हो गया जब  घना धुआँ धधकता हुए तेल से पूरी तरह से बाहर निकल गया.                        [self]
(defrule darken0
(declare (salience 4900))                              ;salience decreased from '5000' to '4900' by 14anu-ban-04 (09-02-2015)
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-object ?id ?)                       ;commented by 14anu-ban-04 (09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken0   "  ?id "  kAlA_ho_jA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (09-02-2015)
;A scandal that darkened the family's good name.[same clp file]
;एक अनैतिक आचरण जिसने परिवार का अच्छा नाम काला कर दिया. [manual]
(defrule darken1
(declare (salience 5000))                            ;salience increased from '4900' to '5000' by 14anu-ban-04 (09-02-2015)
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?)                            ;added by 14anu-ban-04 on (09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken1   "  ?id "  kAlA_kara_xe )" crlf))
)

;"darken","V","1.kAlA_ho_jAnA[kara_xenA]"
;The sky darkened
;The screen darkened
;A scandal that darkened the family's good name
;--"2.azXerA_honA[karanA]"
;Darken a room
;

;@@@ Added by 14anu-ban-04 (06-10-2014)
;Jean left her car in the doctors' parking area and walked across the darkened expanse.    [bnc_gold]
;जीन ने  उसकी गाडी डाक्टरों के वाहन खडे करने के क्षेत्र में छोड दी और  अन्धकारमय फैलाव के आरपार चला गया.              [manual]      
(defrule darken2       
(declare (salience 40))
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMXakAramaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken2   "  ?id "  aMXakAramaya)" crlf))
)


;@@@ Added by 14anu-ban-04 (18-11-2014)
;The darkened ends of the needle represent north poles.                          [NCERT-CORPUS]
;सुइयों के काले सिरे उत्तरी ध्रुव प्रदर्शित करते हैं.                                          [NCERT-CORPUS]
      
(defrule darken3      
(declare (salience 50))
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 end)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken3   "  ?id "  kAlA)" crlf))
)


;@@@ Added by 14anu-ban-04 (18-11-2014)
;Evening had begun to darken .                    [tourism-corpus]
;शाम गहराने लगी थी ।                  [tourism-corpus]
(defrule darken4       
(declare (salience 40))
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 evening)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaharAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken4   "  ?id "  gaharAnA)" crlf))
)


;@@@ Added by 14anu-ban-04 (09-02-2015) 
;It was a tragedy that darkened his later life.                      [oald]
;यह एक ऐसी दुःखद घटना थी जिसने उसका  पिछला जीवन अन्धकारमय कर दिया .            [self]
(defrule darken5
(declare (salience 5010))
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMXakAramaya_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken5   "  ?id " aMXakAramaya_kara_xe)" crlf))
)

;@@@ Added by 14anu-ban-04 (09-02-2015)
;Her mood darkened at the news.                            [oald]
;उसका मन समाचार सुन कर उदास हो गया .                             [self]
(defrule darken6
(declare (salience 5010))
(id-root ?id darken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 mood)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAsa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  darken.clp 	darken6  "  ?id " uxAsa_ho_jA)" crlf))
)


