;@@@ Added by 14anu-ban-06  (04-09-14)
;Henna is helpful in removing dead skin from head .(parallel corpus)
;सिर की मृत त्वचा हटाने में मेहँदी सहायक होती है ।
(defrule helpful0
(declare (salience 0))
(id-root ?id helpful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  helpful.clp 	helpful0   "  ?id "  sahAyaka )" crlf))
)

;@@@ Added by 14anu-ban-06  (04-09-14)
;The librarians will be able to give helpful advice to adults who want to choose books for their children .(parallel corpus)
;पुस्तकाध्यक्ष उन वयस्कों को उपयोगी सलाह दे पायेंगे जो अपने बच्चों के लिए किताबें चुनना चाहते हैं .
(defrule helpful1
(declare (salience 2000))
(id-root ?id helpful)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 information|advice|suggestion)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayogI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  helpful.clp 	helpful1   "  ?id "  upayogI )" crlf))
)
