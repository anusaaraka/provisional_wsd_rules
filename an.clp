;$$$Modified by 14anu-ban-02(19-03-2015)
;A discreet glance at the clock told me the interview had lasted an hour.[oald]
;घड़ी  पर पड़ी एक सतर्क नजर ने मुझे बताया कि साक्षात्कार  एक घण्टा  चला था . [self]  
;@@@ Added by 14anu03 on 20-june-14
;The chicken is laying an egg.
;मुर्गी अण्डा दे रही है .
;मुर्गी एक अण्डा दे रही है .[self] by 14anu-ban-02(19-03-2015)
(defrule an100
(declare (salience 4000))	;salience reduced ti 4000 from 5500 by 14anu-ban-02(19-03-2015)
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) ?word)
(viSeRya-det_viSeRaNa ?id1 ?id)	;added ?id1 by 14anu-ban-02(19-03-2015)
(kriyA-object  ?id2 ?id1)	;added by 14anu-ban-02(19-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))	;meaning changed from '-' to 'eka' by 14anu-ban-02(19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp        an100   "  ?id "  eka )" crlf))
)

;$$$Modified by 14anu-ban-02(04-03-2015)
;###[COUNTER STATEMENT]###An amicable settlement was reached.[oald]
;###[COUNTER STATEMENT]###एक सौहार्दपूर्ण समझौता हो गया. [self]	;suggested by chaitanaya sir.
;Added by Meena(27.4.10)
;He felt an utter fool . 
(defrule an0
(declare (salience 5000))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) ?word) 		;commented by 14anu-ban-02(04-03-2015)
;(viSeRya-viSeRaNa ?id1 =(+ ?id 1))	;commented by 14anu-ban-02(04-03-2015)
(viSeRya-det_viSeRaNa  ?id1 ?id)     	;added by 14anu-ban-02(04-03-2015)
(kriyA-object  ?id2 ?id1)    	;added by 14anu-ban-02(04-03-2015)
(id-root ?id2 feel)            	;added by 14anu-ban-02(04-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp        an0   "  ?id "  - )" crlf))
)






(defrule an1
(declare (salience 5000))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id Art)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp 	an1   "  ?id "  eka )" crlf))
)




;Modified by Meena(28.4.10)
;Added by Meena(4.12.09)
;An income tax increase may be necessary .
(defrule an2
(declare (salience 4900))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) ?word)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 =(+ ?id 1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp        an2   "  ?id "  - )" crlf))
)


;Salience reduced by Meena(4.12.09)
(defrule an3
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp 	an3   "  ?id "  eka )" crlf))
)

;"an","Art","2.eka"
;She bought an umbrella.
;
;@@@ Added by 14anu26        [01-07-14]
;The comic villainy of an Ajit is not unknown in popular Indian tradition . 
;अजीत की यह खलनायकी लकप्रिय भारतीय परंपरा में नई नहीं है .
(defrule an4
(declare (salience 4900))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id1 ?id)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp 	an4   "  ?id "  - )" crlf))
)

;@@@Added by 14anu-ban-02(05-03-2015)
;She has taken an avid interest in the project.[oald]
;उसने परियोजना में अधिक रूचि ली .[self] 
(defrule an5
(declare (salience 4900))
(id-root ?id an)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id1 ?id)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  an.clp 	an5   "  ?id "  - )" crlf))
)


