;commented by 14anu-ban-03 (19-02-2015)
;@@@ Added by 14anu-ban-03 (16-02-2015)
;Scientists are on the scent of a cure.(hinkhoj)
;वैज्ञानिक इलाज की खोज पर हैं . [manual]
;(defrule cure2
;(declare (salience 5000))
;(id-root ?id cure)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-of_saMbanXI  ?id1 ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ilAja))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cure.clp 	cure2   "  ?id "  ilAja )" crlf))
;)

;------------------------- Default Rules -------------------


;$$$ Modified by 14anu-ban-03 (19-02-2015)
;Scientists are on the scent of a cure.(hinkhoj)
;वैज्ञानिक इलाज की खोज पर हैं . [manual]
;"cure","N","1.cikiwsA"
;Her cure took three months
(defrule cure0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (16-02-2015)
(id-root ?id cure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja)) ;changed meaning from 'cikiwsA' to 'ilAja' by 14anu-ban-03 (19-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cure.clp 	cure0   "  ?id "  ilAja )" crlf))
)

;svasWa karanA is better than niroga karanA
;default_sense && category=verb	niroga_kara	0
;"cure","VT","1.niroga_karanA"
;The doctors cured him of Blood pleasure

(defrule cure1
(declare (salience 4900))
(id-root ?id cure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svasWa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cure.clp 	cure1   "  ?id "  svasWa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  cure.clp      cure1   "  ?id " ko )" crlf)
)
)

