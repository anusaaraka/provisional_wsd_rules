;@@@ Added by 14anu-ban-10 on (12-02-2015)
;In 1995 under the leadership of Martin Moran British team had performed a successful mission through a new route .[tourism corpus]
;1995  में  मार्टिन  मोरान  के  नेतृत्व  में  ब्रिटिश  टीम  ने  एक  नये  रूट  से  सफल  अभियान  किया  था  ।[tourism corpus]
(defrule route0
(declare (salience 100))
(id-root ?id route)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  route.clp 	route0  "  ?id "  rUta )" crlf))
)
;@@@ Added by 14anu-ban-10 on (12-02-2015)
;Which is the best route to take?[oald]
;कौन सा सबसे अच्छा रास्ता  है लेने के लिए   ?[self]
(defrule route1
(declare (salience 200))
(id-root ?id route)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id ? )         
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAswA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  route.clp 	route1  "  ?id "  rAswA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-02-2015)
;the route to success.[oald]
;सफलता के लिए मार्ग ।[self]
(defrule route2
(declare (salience 300))
(id-root ?id route)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  route.clp 	route2  "  ?id "  mArga)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-02-2015)
;The flight to Sydney is routed via Tokyo.[oald]
;सिडनी के लिए उड़ान टोक्यो के माध्यम से भेजा गया .[self]
(defrule route3
(declare (salience 400))
(id-root ?id route)
?mng <-(meaning_to_be_decided ?id)
(kriyA-via_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BejA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  route.clp 	route3  "  ?id "  BejA_gayA )" crlf))
)


