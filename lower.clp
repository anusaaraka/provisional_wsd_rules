
(defrule lower0
(declare (salience 5000))
(id-root ?id lower)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 rent)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lower.clp 	lower0   "  ?id "  kama )" crlf))
)

;@@@ Added by 14anu-ban-08 (13-11-2014)
;Many people have claimed soybeans in Asia were historically only used after a fermentation process, which lowers the high phytoestrogens content found in the raw plant.    [Agriculture]
;एशिया में बहुत से लोग दावा कर चुके हैं कि ऐतिहासिक रूप से सोयाबीनों को किण्वन प्रक्रिया के बाद ही उपयोग किया जाता है ,जो कच्चे पौधे में पाये जाने वाले phytoestrogens की भारी मात्रा को कम कर देता है |     [Self]
(defrule lower2
(declare (salience 4901))
(id-root ?id lower)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 content)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lower.clp 	lower2   "  ?id " kama_kara )" crlf))
)

;---------------------- Default rules ------------------

(defrule lower1
(declare (salience 4900))
(id-root ?id lower)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIcA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lower.clp 	lower1   "  ?id "  nIcA_kara )" crlf))
)

;"lower","VTI","1.nIcA_karanA"
;Lower a rating
;--"2.GatAnA"
;The prices have been lowered as an election stunt.
;--"3.uwAranA"
;He lowered the chair onto the lawns. 
;
;
