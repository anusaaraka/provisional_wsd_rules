;@@@ Added by 14anu-ban-06 (14-01-2015)
; The fumes are then inhaled.(fume.clp)
; भाप तब नास से ली जातीं हैं . (manual)
(defrule inhale1
(declare (salience 2000))
(id-root ?id inhale)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 fume)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAsa_se_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  inhale.clp  	inhale1   "  ?id "  nAsa_se_le )" crlf))
)

;@@@ Added by 14anu-ban-06 (14-01-2015)
;informal
;Tony inhaled his burger.(cambridge)
;टोनी अपना बर्गर  गटक गया .  (manual)
(defrule inhale2
(declare (salience 2500))
(id-root ?id inhale)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 burger|pizza)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gataka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  inhale.clp  	inhale2   "  ?id "  gataka_jA )" crlf))
)

;------------------------- Default rule ----------------------
;@@@ Added by 14anu-ban-06 (14-01-2015)
;These days , we sit for hours in Bangalore's heavy traffic and inhale a miasma of toxic gases ; I think this is worse than smoking a cigarette .(parallel corpus)
;इन दिनों हम बंगलूरु के भारी यातायात में घंटों बैठे रहते है और विषैली गैसों से दूषित वायु में सांस लेते हैं ; मुझे लगता है कि यह सिगरेट पीने से भी बदतर है ।(manual)
(defrule inhale0
(declare (salience 0))
(id-root ?id inhale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAzsa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  inhale.clp    inhale0   "  ?id "  sAzsa_le )" crlf))
)

