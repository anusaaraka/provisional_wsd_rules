;@@@ Added by 14anu-ban-06 (22-04-2015)
;The statistics are a clear illustration of the point I am trying to make.(OALD)
;आँकडे बिंदु का स्पष्ट उदाहरण हैं जो मैं बनाने का प्रयास कर रहा हूँ . (manual)
(defrule illustration1
(declare (salience 2000))
(id-word ?id illustration)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAharaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illustration.clp 	illustration1   "  ?id "  uxAharaNa )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (22-04-2015)
;A full-page illustration. (cambridge)
;पूरे पृष्ठ की व्याख्या . (manual)
(defrule illustration0
(declare (salience 0))
(id-word ?id illustration)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illustration.clp 	illustration0   "  ?id "  vyAKyA )" crlf))
)
