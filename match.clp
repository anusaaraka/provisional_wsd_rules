
;(defrule match0
;(declare (salience 5000))
;(id-root ?id match)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) hockey|football|cricket)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mEca))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp 	match0   "  ?id "  mEca )" crlf))
;)





;Added by Meena(7.9.09);salience in match1 should be higher than match3
;He would play a football match against his formar champions.
;They are playing an important match against Liverpool on Saturday . 
(defrule match1
(declare (salience 5000))
;(declare (salience 4900))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 play|win|lose|have)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mEca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match1   "  ?id "  mEca )" crlf))
)

;$$$ Modified by Nandini (16-12-13) (In meaning spelling correction)
;Added by Meena(7.9.09);to be discussed for the meaning "usake samaswara_kA_KilAdI"
;I was no match for him at tennis .
;I was his match at tennis . 
(defrule match2
(declare (salience 4900))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-RaRTI_viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa  ?id ?id1))
(subject-subject_samAnAXikaraNa  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnaswara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match2   "  ?id "  samAnaswara )" crlf)
)
)

;$$$Modified by 14anu-ban-08 (11-03-2015)  ;commented relation, added relation, commented viBakwi
;###[COUNTER EXAMPLE]### Do you think these two colours match?  [same clp file]
;###[COUNTER EXAMPLE]### क्या तुमको लगता है यह दो रंग मेल खाते हैं.  [self]
;Added by Meena(7.9.09);
;Do you think these two colours match?
;He bought a sofa to match with curtains .
(defrule match3
(declare (salience 4900))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa ?id ?id1)      ;commented by 14anu-ban-08 (11-03-2015)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)  ;added by 14anu-ban-08 (11-03-2015)
(id-root ?id1 colour)                   ;added by 14anu-ban-08 (11-03-2015)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id mela))
;(assert (kriyA_id-subject_viBakwi ?id kA))   ;commented by 14anu-ban-08 (11-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match3   "  ?id "  mela )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  match.clp     match3   "  ?id "  kA )" crlf)   ;commented by 14anu-ban-08 (11-03-2015)
)

;Added by Meena (7.9.09)
;Does this shirt match these trousers?
(defrule match4
(declare (salience 4900))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
;(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_KA))
(assert (kriyA_id-object_viBakwi ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match4   "  ?id "  mela_KA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  match.clp     match4   "  ?id "  ke_sAWa )" crlf)
)
)




;Added by Meena(3.9.09)
;Do not strike a match if you suspect a gas leak . 
;He took all her letters into the yard and put a match to them . 
(defrule match5
(declare (salience 4900))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 strike|put)
(kriyA-object ?id1 ?id)
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAcisa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match5   "  ?id "  mAcisa )" crlf))
)


;India faced difficulties winning matches in Australia.  [Set7-76-77pdf]  ;Run on parser 4 ;added by 14anu-ban-08 (12-03-2015)
;भारत ने ऑस्ट्रेलिया में मैच जीतने का सामना कठिनाई से किया.  [self]  ;added by 14anu-ban-08 (12-03-2015)
(defrule match6
(declare (salience 2000))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mEca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match6   "  ?id "  mEca )" crlf))
)

;@@@ Added by Nandini (16-12-13)
;But we have no camphor and no matches, Grandpa.
(defrule match7
(declare (salience 2500))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 camphor)
(conjunction-components  ? ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAcisa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match7   "  ?id "  mAcisa )" crlf))
)

;@@@ Added by Nandini (16-12-13)
;He was determined that his daughter should make a good match. [oxford advanced learner's dictionary]
;usane nirXAriwa kiyA ki usakI betI ko eka acCA jIvavansAWI cAhie.
(defrule match8
(declare (salience 4950))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 daughter)
(and(kriyA-subject  ? ?id1)(kriyA-object  ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIvanasAWI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match8   "  ?id "  jIvanasAWI )" crlf))
)


;$$$ Modified by 14anu-ban-08 (06-12-2014)        ;Added vibhakti by 14anu-ban-08 (06-12-2014)
;Does this shirt match these trousers?       ;example from this same clp file
;क्या यह कमीज इस पतलून से मेल खाती है?      [Self]
;@@@ Added by Nandini (16-12-13)
;New information is matched against existing data in the computer.
;The suspects' stories just don't match up.
;sanxigXa kI kahAniyAz jarA ke Upara mela_nahIM kA hEM.
(defrule match9
(declare (salience 4970))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_KA))
(assert (kriyA_id-object_viBakwi ?id se))              ;Add vibhakti by 14anu-ban-08  (06-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match9   "  ?id "  mela_KA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  match.clp     match9   "  ?id "  se )" crlf))      ;Add vibhakti by 14anu-ban-08  (06-12-2014)
)

;@@@Added by 14anu-ban-08 (11-03-2015)
;He bought a sofa to match with curtains .  [same clp file]
;उसने पर्दों से मेल खाता हुआ सोफा खरीदा. [self]
(defrule match10
(declare (salience 4971))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 curtain)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match10   "  ?id "  mela_KA )" crlf))
)


;@@@Added by 14anu-ban-08 (11-03-2015)
;I was his match at tennis.  [oald]
;टेनिस में मैं उसकी बराबरी पर था.  [self]
(defrule match11
(declare (salience 4971))
(id-root ?id match)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI ?id ?id1)
(id-root ?id1 tennis)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id barAbarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  match.clp     match11   "  ?id "  barAbarI )" crlf))
)

;"match","N","1.mEca"
;--"2.mukAbalA"
;He would play a foodball match against his formar champions.
;--"3.jodZIxAra"
;I've found a vase that's an exact match of one we already have.
;--"4.vivAha"
;She made a good match when she married him.
;


;LEVEL 
;
;
;Headword : match
;
;Examples --
;
;--jodZIxAra
;She will be the perfect match if she marries him.
;agara usane usase SAxI kI wo usake lie vaha sahI jodZIxAra hogI. <--mela
;
;--mela (_milawA julawA)
;This colour matches with my car's colour.
;yaha raMga mere kAra ke raMga se mela KAwA hEM <--mela
;
;--lAyaka
;He is no match to her.
;vaha usake lAyaka nahIM hEM.<-- barAbara nahIM hE <--usase mela nahIM KAwA
;
;--mEca
;Yesterday I enjoyed watching a football match between India&China.
;kala mEneM BArawa Ora cIna ke bIca Kele gae PUta- bAla mEca kA AnaMxa uTAyA.
;
;sUwra : mela`/mEca
;
;
;
