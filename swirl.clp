;@@@ Added by 14anu-ban-11 on (26-02-2015)
;The material is yellow with blue swirls on it. (oald)
;इसपर नीले घुमाव के साथ पीला द्रव्य  है . (self)
(defrule swirl2
(declare (salience 5001))
(id-root ?id swirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 blue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swirl.clp 	swirl2   "  ?id "  GumAva)" crlf))
)

;@@@ Added by 14anu-ban-11 on (26-02-2015)
;He took a mouthful of water and swirled it around his mouth.(oald)
;उसने पानी से मुंह भर लिया और अपने मुँह मे चारों ओर इसको घुमाया . (self)
(defrule swirl3
(declare (salience 4901))
(id-root ?id swirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
(id-root ?id1 mouth)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swirl.clp 	swirl3   "  ?id "  GumA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (26-02-2015)
;The water swirled down the drain. (oald)
;पानी नाली मे नीचे की ओर घूमते हुए बहा . (self)
(defrule swirl4
(declare (salience 4902))
(id-root ?id swirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 drain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmawe_hue_bahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swirl.clp 	swirl4   "  ?id " GUmawe_hue_bahA )" crlf))
)

;------------------------ Default Rules ----------------------
;"swirl","N","1.cakkara"
;Dancers spinning round in a swirl of bright colours.
(defrule swirl0
(declare (salience 5000))
(id-root ?id swirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swirl.clp 	swirl0   "  ?id "  cakkara )" crlf))
)

;"swirl","V","1.cakkara_KAwe_hue_udZanA"
;The dust is swirling in the streets.
(defrule swirl1
(declare (salience 4900))
(id-root ?id swirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara_KAwe_hue_udZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swirl.clp 	swirl1   "  ?id "  cakkara_KAwe_hue_udZa )" crlf))
)

