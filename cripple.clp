;@@@ Added by 14anu-ban-03 (02-04-2015)
;Two top athletes have been crippled from the championship after positive drug tests. [cald]
;दो शीर्ष एथलीटों को सकारात्मक दवा परीक्षण के बाद चैम्पियनशिप से अयोगय कर दिया गया है । [manual]
(defrule cripple2
(declare (salience 5000))
(id-root ?id cripple)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ayogya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cripple.clp 	cripple2   "  ?id "  ayogya_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (02-04-2015)
;You could see the poor dog crippled daily as the disease spread through its body. [cald]
;जैसे-जैसे बीमारी उसके शरीर में  फैलती जा रही है आप  प्रति दिन उस कमज़ोर कुत्ते को निर्बल होते हुए देख सकते हैं.  [manual]
(defrule cripple3
(declare (salience 5000))
(id-root ?id cripple)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirbala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cripple.clp 	cripple3   "  ?id "  nirbala_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-04-2015)
;He's an emotional cripple. [oald]
;वह भावुक रूप से अपाहिज है . [manual]
(defrule cripple4
(declare (salience 5001))
(id-root ?id cripple)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apAhija))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cripple.clp 	cripple4   "  ?id "  apAhija )" crlf))
)

;--------------------------------------------------Default rules-----------------------------------------------
;"cripple","N","1.laMgadZA_manuRya"
;He sits idle at home like cripples.
(defrule cripple0
(declare (salience 5000))
(id-root ?id cripple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laMgadZA_manuRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cripple.clp 	cripple0   "  ?id "  laMgadZA_manuRya )" crlf))
)

;"cripple","VT","1.apAhija_karanA"
;Her child got crippled by polio attack.
;This measure crippled our efforts.
;The accident has crippled her for life.
(defrule cripple1
(declare (salience 4900))
(id-root ?id cripple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apAhija_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cripple.clp 	cripple1   "  ?id "  apAhija_kara )" crlf))
)

