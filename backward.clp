;@@@ Added by 14anu-ban-02 (21-11-2014)
;We get thrown backward with a jerk.[ncert]
;हम झटके के साथ पीछे की ओर गिर पडते हैं.[ncert]
(defrule backward0
(declare (salience 0))
(id-root ?id  backward)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pICe_ki_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   backward.clp    backward0   "  ?id "  pICe_ki_ora)" crlf))
)

;@@@ Added by 14anu-ban-02 (21-11-2014)
;It observed that these areas were instances of socially and educationally backward - class citizens .[karan singla]
;उसने विचार व्यक्त किया कि ये सामाजिक तथा शैक्षिक दृष्टि से पिछड़े वर्ग के नागरिकों के उदाहरण हैं .[karan singla]
(defrule backward1
(declare (salience 100))
(id-root ?id  backward)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   backward.clp 	 backward1   "  ?id "  piCadA)" crlf))
)
