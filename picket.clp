

;@@@ Added by 14anu-ban-09 on (19-03-2015)
;200 workers were picketing the factory. 	[oald]
;200 कार्यकर्ता फैक्टरी में धरना दे रहे थे . 			[Anusaaraka]

(defrule picket3
(declare (salience 5001))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 worker|labour)
(kriyA-object  ?id ?id2)
(id-root ?id2 factory|gate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XaranA_xe))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picket.clp 	picket3   "  ?id "  XaranA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  picket.clp    picket3   "  ?id " meM )" crlf)
)
)


;@@@ Added by 14anu-ban-09 on (19-03-2015)
;Striking workers picketed outside the gates. 	[oald]
;हड़ताल करने वाले कार्यकर्ताओं ने द्वारों के बाहर धरना दिया . 	[Manual]

(defrule picket4
(declare (salience 5001))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 worker|labour)
(kriyA-outside_saMbanXI  ?id ?id2)
(id-root ?id2 factory|gate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XaranA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picket.clp 	picket4   "  ?id "  XaranA_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-03-2015) 
;The other jumped up and picket it. 		[Report set 8]
;दूसरा कूदा और उसने रूकावट डाली.			[Manual]
(defrule picket5
(declare (salience 5001))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 it)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUkAvata_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picket.clp 	picket5   "  ?id "  rUkAvata_dAla )" crlf)
)
)

(defrule picket0
(declare (salience 5000))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id picketing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XaranA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  picket.clp  	picket0   "  ?id "  XaranA )" crlf))
)

;"picketing","N","1.XaranA/GerA"
;Picketing should be made illegal.
;
(defrule picket1
(declare (salience 4900))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XaranEwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picket.clp 	picket1   "  ?id "  XaranEwa )" crlf))
)

;"picket","N","1.XaranEwa"
;The pickets became violent when their demands were refused.
;--"2.KUztA"
;The cow was tied to the picket.
;
(defrule picket2
(declare (salience 4800))
(id-root ?id picket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GerA_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picket.clp 	picket2   "  ?id "  GerA_dAla )" crlf))
)

;"picket","V","1.GerA_dAlanA/KUztA_gAdanA"
;On sports day, ground will be picketed.
;--"2.XaranA_xenA"
;Some workers of the company want to picket.
;
