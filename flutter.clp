;@@@ Added by 14anu-ban-05 on (18-03-2015)
;Her eyelids fluttered but did not open.[OALD]
;उसकी पलकें झपकीं परन्तु नहीं खुलीं . [manual]

(defrule flutter2
(declare (salience 4901))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 eyelid)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Japaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter2   "  ?id "  Japaka )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-03-2015)
;She fluttered her eyelashes at him.  [OALD]
;उसने   अपनी बरौनियाँ  उसकी ओर झपकाईं . [manual]

(defrule flutter3
(declare (salience 4902))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 eyelid|eyelash)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JapakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter3   "  ?id "  JapakA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-03-2015)
;He felt his stomach flutter when they called his name.	[OALD]
;उसने  घबराहट महसूस की  जब उन्होंने उसको बुलाया . 		[manual]

(defrule flutter5
(declare (salience 5001))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) stomach)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) GabarAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flutter.clp  flutter5  "  ?id "  " (- ?id 1) "  GabarAhata  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-03-2015)
;Her sudden arrival caused quite a flutter.	[OALD]
;उसका अचानक आगमन   अत्यन्त हलचल का कारण बना. 	[manual]

(defrule flutter6
(declare (salience 5002))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 cause)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halacala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter6   "  ?id "  halacala )" crlf))
)



;@@@ Added by 14anu-ban-05 on (18-03-2015)
;He had a flutter at the stock market today. 		[from flutter.clp]
;usane Aja satte ke bAjZAra meM bAjZI lagAyI.		[from flutter.clp]

(defrule flutter7
(declare (salience 5003))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI  ?id ?id1)
(id-root ?id1 market)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAjZI_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter7   "  ?id "  bAjZI_lagA )" crlf))
)

;------------------------ Default Rules ----------------------
;"flutter","N","1.uwwejanA"
;He was in a flutter on clearing the hurdle.
(defrule flutter0
(declare (salience 5000))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter0   "  ?id "  uwwejanA )" crlf))
)

;"flutter","V","1.PadZaPadZAnA"
;This place is so calm that you can even hear the flutter of wings of the birds.
(defrule flutter1
(declare (salience 4900))
(id-root ?id flutter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PadZaPadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flutter.clp 	flutter1   "  ?id "  PadZaPadZA )" crlf))
)

;"flutter","V","1.PadZaPadZAnA"
;This place is so calm that you can even hear the flutter of wings of the birds.
;--"2.havA_meM_hilanA"
;All the flags in the stadium were fluttering in the strong wind.
;--"3.hilAnA"
;The vamp fluttered her eyes coquettishly at the hero in the film.
;--"4.wejZa_raPwAra_se_XadZakanA"
;Due to excitement his heart was fluttering.

;"flutter","N","1.uwwejanA"
;He was in a flutter on clearing the hurdle.
;--"2.kampana"
;The sound of the song had a flutter due to long use of the tape.
;--"3.bAjZI_lagAnA"
;He had a flutter at the stock market today.

;
;LEVEL 
;
;
;Headword : flutter
;
;Examples --
;
;"flutter","V","1.PadZaPadZAnA"
;This place is so calm that you can even hear the flutter of wings of the birds.
;yaha jagaha iwanI SAnwa hE ki wuma cidZiyoM ke paroM kA PadZaPadZAnA BI suna sakawe ho.
;
;--"2.havA meM hilanA"
;All the flags in the stadium were fluttering in the strong wind.
;stediyama meM sAre Jande wejZa havA meM hila rahe We.  havA meM hilanA <-- PadZaPadZAnA 
;
;--"3.hilAnA"
;The vamp fluttered her eyes coquettishly at the hero in the film.
; Pilma meM KalanAyikA hIro para apanI AzKe hilAkara cocalebAjI kara rahI WI. hilanA <--PadZaPadZAnA)
;
;--"4.wejZa raPwAra se XadZakanA"
;Due to excitement his heart was fluttering.
;uwwejanA ke kAraNa usakA xila weja raPwAra se XadZaka rahA WA <--PadZaPadZA rahA WA
;
;"flutter","N","1.GabarAhata"
;He was in a flutter of mind while participating in the hurdle race.
;rukAvata resa meM BAga lewe samaya usakA ximAga GabarAhata kI sWiwi meM WA.GabarAnA <-- XImI 
;gawi se PadZaPadZAnA<--PadZaPadZAnA
;
;--"2.kampana"
;The sound of the song had a flutter due to long use of the tape.
;tepa kA aXika prayoga hone ke kAraNa gAne kI AvAjZa kampana kara rahI WI,(XImI gawi se UwAra
;caDZAva<--hilanA<--PadZaPadZAnA
;
;--"3.bAjZI lagAnA"
;He had a flutter at the stock market today.
;usane Aja satte ke bAjZAra meM bAjZI lagAyI.bAjZI lagAnA<--satte meM uwAra-caDZAva<--hilanA<--PadZaPadZAnA  
;
;nota:-
;uparyukwa samaswa vAkyoMkA( kriyA Ora saMjFA) yaxi viSleRaNa kareM wo 'flutter'Sabxa kA arWa aXikAMSa vAkyoM meM eka hI Sabxa 'PadZaPadZAnA' se nikAlA jA sakawA hE.
;
;sUwra : PadaPadAnA`
;
;
;
;
