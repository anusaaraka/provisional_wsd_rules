
;$$$ Modified by 14anu-ban-03 (02-12-2014)
;Aparna sen's movie has received critical acclaim.
;अपर्णा सेन की फिल्मो ने  विवेचनात्मक प्रशंसा प्राप्त की है। [manual]
(defrule critical1
(declare (salience 4900))
(id-root ?id critical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id)       ;added by 14anu-ban-03 (02-12-2014)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivecanAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  critical.clp 	critical1   "  ?id "  vivecanAwmaka )" crlf))
)

;"critical","Adj","1.vivecanAwmaka"
;He looks at every thing with his critical views. 
;Aparna sen's movie has received critical acclaim. 
;--"2.nAjZuka"
;The condition of the patient is very critical.
;--"3.saMkatamaya"
;A critical temperature of water is 100 degrees C--its boiling point at standard atmospheric pressure
;
;

;@@@ Added by 14anu-ban-03 (04-02-2015)
;The condition of the patient is very critical. [hinkhoj]
;मरीज की स्थिति अत्यन्त नाजुक है . [self]
(defrule critical2
(declare (salience 5000))
(id-root ?id critical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id) 
(id-root ?id1 condition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAjZuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  critical.clp 	critical2   "  ?id "  nAjZuka )" crlf))
)
;--------------------- Default Rules ---------------

(defrule critical0
(declare (salience 5000))
(id-root ?id critical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samIkRAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  critical.clp 	critical0   "  ?id "  samIkRAwmaka )" crlf))
)

