;@@@ Added by 14anu-ban-08 on (17-09-2014)
;The Lawalong sanctuary is located in south - west of Chatra its location is in the west of Babara in Chatra – Chandawa road . (Tourism corpus)
;लावालौंग  अभ्यारण्य  चतरा  से  दक्षिण-पश्चिम  में  स्थित  है  इसकी  स्थिति  चतरा-चंदवा  मार्ग  में  बाबरा  से  पश्चिम  में  है  ।         
(defrule location0
(declare (salience 100))        ;salience increased by 14anu-ban-08 (24-03-2015)
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)              ;added by 14anu-ban-08 (24-03-2015)
(kriyA-in_saMbanXI ?id1 ?id2)         ;added by 14anu-ban-08 (24-03-2015)
(id-root ?id2 east|west|north|south)  ;added by 14anu-ban-08 (24-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp      location0   "  ?id "  sWiwi )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;Jharkhand's historical , religious and natural location , festivals , dance , music , green dense forests , waterfalls , hill stations … if you glance over these then it seems as if the earth having adorned herself is awaiting her lover .  (Tourism Corpus)
;झारखंड  के  ऐतिहासिक  ,  धार्मिक  और  प्राकृतिक  स्थल  ,  उत्सव  ,  नृत्य  ,  संगीत  ,  हरे  घने  वन  ,  जलप्रपात  ,  हिल  स्टेशन…  इन  पर  नजर  दौड़ायें  तो  यूँ  लगता  है  जैसे  धरती  सोलह  श्रंगार  कर  अपने  प्रियतम  की  राह  देख  रही  हो 
;We were told that rhinoceroses have a strange habit of passing stool at a fixed location .  (Tourism Corpus)
;हमें बताया गया कि गैंडों में एक नियत स्थान पर मल त्यागने की अत्यंत विचित्र आदत पाई जाती है ।  
(defrule location1
(declare (salience 0))          ;salience decreased by 14anu-ban-08 (24-03-2015)
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa  ?id ?id1)     ;commented by 14anu-ban-08 (24-03-2015)
;(id-root ?id1 religious|historical|fix)  ;commented by 14anu-ban-08 (24-03-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location1   "  ?id "  sWAna )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;If the team leader reaches on a flat surface and no convenient location for anchoring is visible to him then there also deadman comes in use .  (Tourism Corpus)
;जब दल का नेता बर्फ़ के समतल मैदान पर पहुँच जाए और उसे एंकरिंग के लिए कोई सुविधाजनक स्थान दिखाई न दे तो वहाँ पर भी डैडमैन काम में आता है ।     
(defrule location2
(declare (salience 101))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(id-root ?id1 visible)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location2   "  ?id "  sWAna )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;Foreign tourists reach at these locations to view the temple architecture or unique face of faith .  (Tourism Corpus)
;विदेशी यात्री इन स्थानों पर मंदिर वास्तुशिल्प या आस्था के अनोखे रूप को देखने पहुँचते हैं ।  
(defrule location3
(declare (salience 102))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 reach)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location3   "  ?id "  sWAna )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;The Baikal Fort is the favourite location for the shooting of Malayalam , Tamil , Bollywood cinemas .  (Tourism Corpus)  
;मलयालम  ,  तमिल  ,  बॉलीवुड  सिनेमाओं  की  शूटिंग  की  प्रिय  लोकेशन  है  बेक्कल  किला  ।        
(defrule location4
(declare (salience 103))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 Fort)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lokeSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location4   "  ?id "  lokeSana )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;The important location of Delhi has made it the centre of power for one thousand years .  (Tourism Corpus)
;दिल्ली  की  महत्वपूर्ण  अवस्थिति  ने  इसे  एक  हजार  साल  से  सत्ता  का  केंद्र  बना  दिया  है  ।        
;The location of the national parks , forest reserves , tiger reserves and other protected areas in our country is in different views , residing places and climate areas  .     (Tourism Corpus)
;हमारे  देश  के  राष्ट्रीय  उद्यानों  ,  वनारण्यों  ,  बाघ  रिजर्वों  व  अन्य  सुरक्षित  क्षेत्रों  की  अवस्थिति  अलग  -  अलग  प्रदृश्यों  ,  वास  -  स्थलों  और  जलवायु  क्षेत्रों  में  है  । 
(defrule location5
(declare (salience 104))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 Delhi|area|park|particle|point)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location5   "  ?id "  avasWiwi )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;Likewise, space is homogeneous and there is no (intrinsically) preferred location in the universe.    (Tourism Corpus)
;इसी प्रकार, दिक्स्थान समाङ्गी है तथा विश्व में (मूलभूत रूप से) कोई अधिमत अवस्थिति नहीं है.
;Caution: the phenomena may differ from place to place because of differing conditions at different locations.    (Tourism Corpus)
;सावधान: विभिन्न अवस्थितियों में विभिन्न परिस्थितियाँ होने के कारण स्थान परिवर्तन के साथ परिघटनाएँ परिवर्तित हो सकती हैं.
(defrule location6
(declare (salience 105))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 different|prefer|same)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location6   "  ?id "  avasWiwi )" crlf))
)

;@@@ Added by 14anu-ban-08 on (17-09-2014)
;It does not depend on the temperature, pressure or location of the object in space.     (Tourism Corpus)
;यह पिण्ड के ताप, दाब या दिक्काल में उसकी अवस्थिति पर निर्भर नहीं करता.
(defrule location7
(declare (salience 106))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
(id-root ?id1 depend)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location7   "  ?id "  avasWiwi )" crlf))
)

;@@@ Added by 14anu-ban-08 on (24-03-2015)
;This shows that the location of decimal point is of no consequence in determining the number of significant figures.  [NCERT]
;यह दर्शाता है कि सार्थक अङ्कों की सङ्ख्या निर्धारित करने में, दशमलव कहाँ लगा है इसका कोई महत्व नहीं होता.  [NCERT]
;यह दर्शाता है कि सार्थक अङ्कों की सङ्ख्या निर्धारित करने में, दशमलव का स्थान लगा है इसका कोई महत्व नहीं होता.  [self]
(defrule location8
(declare (salience 105))
(id-root ?id location)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 point)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  location.clp 	location8   "  ?id "  sWAna )" crlf))
)
