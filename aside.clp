;@@@Added by 14anu-ban-02(10-03-2015)
;We put some money aside for our holiday.[cambridge]
;हमने हमारी छुट्टियों के लिये कुछ पैसे अलग  रखे है . [self]
(defrule aside2
(declare (salience 100))
(id-root ?id aside)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 put)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aside.clp 	aside2   "  ?id "  alaga )" crlf))
)

;@@@Added by 14anu-ban-02(10-03-2015)
;Money worries aside, things are going well. 	;run the sentence on parser no. 216
;पैसो की चिंता छोड़कर, चीजें अच्छी जा रहीं हैं . [self]
(defrule aside3
(declare (salience 100))
(id-root ?id aside)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CodZakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aside.clp 	aside3   "  ?id " CodZakara )" crlf))
)

;@@@Added by 14anu-ban-02(10-03-2015)
;He made several mocking asides about the inadequacy of women.[oald]
;उसने स्त्रियों की अयोग्यता के बारे में कई  उपहासपूर्ण जनान्तिक बनाए . [self]
(defrule aside4
(declare (salience 100))
(id-root ?id aside)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 mock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janAnwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aside.clp 	aside4   "  ?id " janAnwika )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$Modified by 14anu-ban-02(10-03-2015)  ;Meaning changed from 'raxxa_karanA' to 'eka_ora'.
;I tossed the book aside and got up.(oald)
;मैंने पुस्तक फेंकी एक ओर और उठ गया. (manual)
;"aside","Adv","1.raxxa_karanA"
;You must put aside any idea of holiday this year .
(defrule aside0
(declare (salience 0))	;salience reduce to 0 by 14anu-ban-02(10-03-2015)
(id-root ?id aside)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aside.clp 	aside0   "  ?id "  eka_ora )" crlf))
)

;"aside","N","1.svagawa_ukwi"
;One could understand Hamlet's mind by his asides.
(defrule aside1
(declare (salience 0))
(id-root ?id aside)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svagawa_ukwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aside.clp 	aside1   "  ?id "  svagawa_ukwi )" crlf))
)


;"aside","Adv","1.raxxa_karanA"
;You must put aside any idea of holiday this year .
;--"2.alaga_raKanA"
;My father has kept aside some money for his retirement.
;
;"aside","Adv","1.raxxa_karanA"
;You must put aside any idea of holiday this year .
;
;LEVEL 
;Headword : aside
;
;
;Examples --
;"aside","Adv","1.kinAre"
;You must put aside any idea of holiday this year .
;isa varRa CuttI para jAne kA koI BI KayAla kinAre raKa xo.<--eka_ora 
;--"2.alaga_raKanA"
;My father has kept aside some money for his retirement.
;mere piwA ne apane ritAyarameMta ke bAxa ke liye kuCa pEsA alaga raKa xiyA hE. <--eka_ora alaga se
;
;"aside","N","1.svagawa_ukwi"
;One could understand Hamlet's mind by his asides.
;hEmaleta ke ximAga meM kyA cala rahA hE yaha usakI svagawa_ukwiyoM se samaJa sakawe hEM.
;<--eka ora jA kara
;
;sUwra : eka_ora^svagawokwi
;
