;@@@ Added by 14anu-ban-02 (05-12-2014)
;Sentence: Similarly, magnetic permeability µ is the ability of a substance to acquire magnetisation in magnetic fields.[ncert]
;Translation: इसी प्रकार चुम्बकीय पारगम्यता µ किसी पदार्थ की चुम्बकीय क्षेत्रों में चुम्बकन अर्जित करने की योग्यता होती है.[ncert]
(defrule ability0  
(declare (salience 0)) 
(id-root ?id ability) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id yogyawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ability.clp  ability0  "  ?id "  yogyawA )" crlf)) 
)

;@@@ Added by 14anu-ban-02 (05-12-2014)
;For the authorised travels of tourist groups Indian Railways have taken a step to fix fares as per their credit ability .[tourism]
;भारतीय रेल ने पर्यटक समूहों की अधिकृत यात्राओं के लिए उनकी क्रयक्षमता के अनुरूप किरायों को तय करने के लिए कदम उठाया है .[tourism]
;The speed ability of scooters is till 80 kms per hour .[tourism]
;स्कूटरों की गति क्षमता 80 कि.मी. प्रति घंटा तक की है .[tourism]
(defrule ability1  
(declare (salience 100)) 
(id-root ?id ability) 
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 credit|scooter)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kRamawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ability.clp  ability1  "  ?id "  kRamawA )" crlf)) 
)


;@@@ Added by 14anu-ban-02 (05-12-2014)
;If we talk about the country then the range of tourist spots has the ability to attract tourists of various natures and does not disappoint the disciples of adventure , art and culture , romance , religious devotion to natural beauty .[tourism]
;यदि देश की बात करें तो यहाँ पर्यटन स्थलों की विविधता भिन्न-भिन्न प्रकृति के पर्यटकों को अपनी ओर आकर्षित करने की क्षमता रखती है और एडवेंचर कला संस्कृति रोमांस धार्मिक आस्था से लेकर प्राकृतिक सौंदर्य के मुरीदों को निराश नहीं करती |[tourism]
(defrule ability2  
(declare (salience 100)) 
(id-root ?id ability) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)
(id-root ?id1 attract)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kRamawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ability.clp  ability2  "  ?id "  kRamawA )" crlf)) 
)

