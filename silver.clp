
(defrule silver0
(declare (salience 5000))
(id-root ?id silver)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rajawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silver.clp 	silver0   "  ?id "  rajawa )" crlf))
)

;"silver","Adj","1.rajawa/rAjawa"
;He has an attractive silver sports car.
;
(defrule silver1
(declare (salience 4900))
(id-root ?id silver)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAzxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silver.clp 	silver1   "  ?id "  cAzxI )" crlf))
)

;"silver","N","1.cAzxI"
;Silver is used to electroplate reactive metals.
;--"2.cAzxI_kA_paxaka"
;He won four silvers for his state.
;
(defrule silver2
(declare (salience 4800))
(id-root ?id silver)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAzxI_caDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silver.clp 	silver2   "  ?id "  cAzxI_caDZA )" crlf))
)

;"silver","V","1.cAzxI_caDZAnA"
;He owns a fine silvered bracelet.
;


;@@@ Added by 14anu-ban-07, 30-7-14
;चांदी के बर्तन  :
;Furniture, silver ware, jewelery and gift items of Thailand are very famous.(parallel corpus)
;थाईलैंड  का  फर्नीचर  , चांदी के बर्तन  ,  ज्वैलरी  व  उपहार  का  सामान  अत्यंत  प्रसिद्ध  है  ।(manually)

(defrule silver_ware
(declare (salience 5200))
(id-root ?id silver)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) ware)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  =(+ ?id 1) cAMxI_ke_barwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " silver.clp   silver_ware  "  ?id " "(+ ?id 1)"  cAMxI_ke_barwana )" crlf))
)



