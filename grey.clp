;@@@ Added by 14anu-ban-05 on (02-04-2015)
;She was completely grey by the age of thirty. [OALD]
;वह तीस साल की उम्र से पूरी तरह से अधेड़ दिखने लगी थी .	[MANUAL]

(defrule grey1
(declare (salience 101))
(id-root ?id grey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-by_saMbanXI  ?id ?id1)
(id-root ?id1 age)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXedZa_xiKane_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grey.clp  	grey1   "  ?id "  aXedZa_xiKane_lagA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (02-04-2015)
;Ours is a company that isn’t run by grey men in suits.		 [OALD]
;हमारा एक ऐसी कम्पनी है जो सूटों में अधेड़ आदमियों के द्वारा  सञ्चालित नहीं किया जाता है . 		[MANUAL]
;She had to talk to some grey under-secretary from the Ministry.	[OALD]
;उसको मन्त्री मण्डल से किसी अधेड़ उप-सचिव तक बातचीत करनी पडी .		[MANUAL]

(defrule grey2
(declare (salience 102))
(id-root ?id grey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXedZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grey.clp  	grey2   "  ?id "  aXedZa )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (02-04-2015)
;The old man’s beard was mostly grey.	 [OALD]
;वृद्ध आदमी की दाढी प्रायः सफेद थी .		 [MANUAL]
;His face was grey with pain.		[OALD]
;उसका चेह्रा दर्द से सफेद था . 			[MANUAL]

(defrule grey3
(declare (salience 103))
(id-root ?id grey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 face|beard|hair)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saPexa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grey.clp  	grey3   "  ?id "  saPexa )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (02-04-2015)
;A light grey suit. [OALD]
;एक हल्का  स्लेटी सूट . [MANUAL]

(defrule grey4
(declare (salience 104))
(id-root ?id grey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 suit)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sletI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grey.clp  	grey4   "  ?id "   sletI )" crlf))
) 

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (02-04-2015)
;The sky looks very grey. I think it’s going to rain.   [OALD]
;आसमान अत्यन्त धूँधला दिख रहा है .मैं सोचता हूँ कि वर्षा होने जा रहा है .      [MANUAL]

(defrule grey0
(declare (salience 100))
(id-root ?id grey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XUzXalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grey.clp      grey0   "  ?id "  XUzXalA )" crlf))
)

