;Added by Meena(19.7.11)
;When the dollar is in a free-fall, even central banks can not stop it. 
(defrule be00
(declare (salience 5000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id is)
(id-root =(- ?id 3) when)
;(id-root =(- ?id 1) ?inanimate)
;(id-root =(- ?id 1)  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "inanimate.gdbm" ?str)))
(id-root =(- ?id 1) ?str);As suggested by Chaitanya Sir removed inanimate.gdbm and modified the fact as shown by Roja (03-12-13) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp        be00   "  ?id "  ho )" crlf))
)

;previous_minus_two=the	howeM_hEM	0
;previous_minus_three=the	howeM_hEM	0
;The students are mischievous.
;The bright students are studious.
;The above rules need to be modified as, 'the starting point of previous chunk is 'the' then ...
;It is considered to be good.
(defrule be0
(declare (salience 5000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id be )
(id-word =(- ?id 1) to )
(id-word =(- ?id 2) considered)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be0   "  ?id "  - )" crlf))
)

;Added by Meena(9.9.09)
;Are these your trousers? 
(defrule are0
(declare (salience 5000))
;(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id are )
(id-root ?id1 trousers|scissors)
(kriyA-subject ?id ?id2)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  be.clp        are0   "  ?id "  hE )" crlf))
)

;Modified by sheetal(17-03-10)
;Grace may not be possible to fix the problem .
(defrule be5
(declare (salience 4500))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(or (id-word ?id am )(id-word =(+ ?id 1) possible));'or-condition' is added by sheetal
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp        be5   "  ?id "  hE )" crlf))
)

(defrule be6
(declare (salience 4400))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-word ?id Are )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_hE))
(assert (id-H_vib_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be6   "  ?id "  kyA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  be.clp         be6   "  ?id " hE )" crlf))
)

(defrule be7
(declare (salience 4300))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-word ?id Is )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_hE))
(assert (id-H_vib_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be7   "  ?id "  kyA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  be.clp         be7   "  ?id " hE )" crlf))
)

(defrule be8
(declare (salience 4200))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-word ?id Was )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_WA))
(assert (id-H_vib_mng ?id WA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be8   "  ?id "  kyA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  be.clp         be8   "  ?id "  WA )" crlf))
)

(defrule be9
(declare (salience 4100))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-word ?id Were )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_WA))
(assert (id-H_vib_mng ?id WA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be9   "  ?id "  kyA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  be.clp         be9   "  ?id " WA )" crlf))
)

(defrule be10
(declare (salience 4000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-word ?id Am )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_hE))
(assert (id-H_vib_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be10   "  ?id "  kyA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  be.clp         be10   "  ?id " hE )" crlf))
)

;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir 17-02-17
;generalised the rule with only 'were' and 'if'
(defrule be16
(declare (salience 3400))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id were )
(id-word =(- ?id 2) if)
;(id-word =(- ?id 1) I ) ; commented by Shirisha Manju 17-02-17
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id howA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be16   "  ?id "  howA )" crlf))
)

(defrule be21
(declare (salience 2900))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_pICe_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " be.clp	be21  "  ?id "  " ?id1 "  ke_pICe_raha  )" crlf))
)

(defrule be24
(declare (salience 2600))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 intimate)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GaniRTa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " be.clp	be24  "  ?id "  " ?id1 "  GaniRTa_ho  )" crlf))
)

;$$$ Modified by Shirisha Manju 06-09-2016 Suggested by Soma Madam
;$$$Modified by 14anu-ban-02(06-04-2016)
;###[COUNTER STATEMENT]This time tomorrow I will be in Canada.[sd_verified]
;###[COUNTER STATEMENT]isa samaya kala mEM kanAdA meM hoUzgA.[sd_verified]
;"be","V","1.honA"
;Don't worry be happy.
;$$$ Modified by 14anu07 Karishma Singh, MNNIT Allahabad on 25/06/2014
(defrule be25
(declare (salience 2500))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) happy) ;added by Shirisha Manju 17-02-17
;(kriyA-kqxanwa_karma  ?id1 ?id) ;Added by 14anu-ban-02(06-04-2016)  ;commented by Shirisha Manju -- This condition appears to be incorrect
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp       be25   "  ?id "  raha)" crlf))
)

;$$$ Modified id-root fact to id-word fact by Shreya Singhal (16-09-19)
;### Counter Example ### Is it that which characterize humans?
;@@@ Added by 14anu07 Karishma Singh, MNNIT Allahabad on 2/07/2014
;The security forces - be it the local police , the Border Security Force ( BSF ) or the army.
;सुरक्षा बल - चाहे वह स्थानीय पुलिस हो , सीमा सुरक्षा बल ( बीएसएफ ) या फिर सेना.
(defrule be30
(declare (salience 3000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) it)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAhe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be30   "  ?id "  cAhe_ho)" crlf))
)

;Added by human
(defrule be26
(declare (salience 2400))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) with )
(id-cat_coarse =(+ ?id 1) a)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be26   "  ?id "  ho )" crlf))
)

;Modified root meaning as word meaning
(defrule be32
(declare (salience 1800))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 2) a)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id howA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  be.clp 	be32   "  ?id "  howA_hE )" crlf))
)

;Modified root meaning as word meaning
(defrule be33
(declare (salience 1700))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 3) a)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id howA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  be.clp 	be33   "  ?id "  howA_hE )" crlf))
)

;@@@ Added by 14anu26   [26-06-14]
;He wants to be left in peace.
;वह  शांति में छोड़ दिया जाना चाहता है.
;वह चाहता है कि उसे शांति से रहने दिया जाये.                 (by 14anu-ban-02(12-01-2015))
(defrule be34
(declare (salience 3000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) left|given|taken)
(id-word =(- ?id 1) to)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be34   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu-ban-02 (19-11-2014)
;Crops can also be dumped in the street during a public protest; this custom has been common in the European Union.[agriculture]
;किसी सार्वजनिक विरोध के दौरान फसलें सड़क में भी  डंप की जा सकती है/फेंकी जा सकती  है ,यह रिवाज यूरोपीय संघ में आम हो गया है  |[self]
(defrule be35
(declare (salience 3000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) has)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be35   "  ?id "  ho_jA )" crlf))
)

;@@@ Added by Shirisha Manju 06-09-2016 Suggested by Soma Madam
;This time tomorrow I will be in Canada.[sd_verified]
;isa samaya kala mEM kanAdA meM hoUzgA.[sd_verified]
(defrule be36
(declare (salience 3000))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(or (to-infinitive ? ?id)(and (root-verbchunk-tam-chunkids ? ? ? ?aux ?id) (id-root ?aux will|shall|may|must|would|should|can|could)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp       be36   "  ?id "  ho )" crlf))
)

;-------------------------------------- Default rules ----------------------
(defrule be2
(declare (salience 800)) ;salience reduced from 4800 to 800 by Shirisha Manju  17-02-17
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id is|are|am )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hE)); Modified by sukhada (word_mng as root_mng) Ex: There is a dog and a cat here .
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  be.clp  	be2   "  ?id "  hE )" crlf))
)

;"were","V","1.We"
;They were at home.
;"was","V","1.WA"
;He was late for his appointment.
(defrule be3
(declare (salience 700)) ;salience reduced from 4700 to 700 by Shirisha Manju  17-02-17
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id was|were)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be3   "  ?id "  WA )" crlf))
)

;Added by Shirisha Manju 17-02-17
(defrule be_default
(declare (salience 500))
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  be.clp 	be_default   "  ?id "  ho)" crlf))
)

;given_word=being && category=noun      $jIva)
;"being","N","1.jIva"
;We saw a movie of strange beings from other planet.
;--"2.haswI"
;I detest violence with my whole being.
;--"3.aswiwva"
;What is the purpose of our being.
(defrule be11
(declare (salience 3900)) ;salience reduced from 3900 to 900 by Shirisha Manju  17-02-17
(id-root ?id be)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id being )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jIva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  be.clp        be11   "  ?id "  jIva )" crlf))
)

