
(defrule trip0
(declare (salience 5000))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trip.clp trip0 " ?id "  galawI_kara )" crlf)) 
)

(defrule trip1
(declare (salience 4900))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trip.clp trip1 " ?id "  galawI_kara )" crlf)) 
)

(defrule trip2
(declare (salience 4800))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trip.clp trip2 " ?id "  galawI_kara )" crlf)) 
)

(defrule trip3
(declare (salience 4700))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 galawI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trip.clp	trip3  "  ?id "  " ?id1 "  galawI_kara  )" crlf))
)

(defrule trip4
(declare (salience 4600))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJa_kara_gira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip4   "  ?id "  ulaJa_kara_gira )" crlf))
)

(defrule trip5
(declare (salience 4500))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip5   "  ?id "  yAwrA )" crlf))
)

;"trip","N","1.yAwrA"
;We made a trip to Badrinath.
;--"2.ladaKadAhata"
;The drug gave him a trip.
;--"3.girAvata"
;
;LEVEL 
;Headword : trip
;
;Examples --
;"trip","V","1.PuxakanA/uCalanA
;She came tripping along the river bank.
;vaha naxI ke wata ke sAWa-sAWa PuxakawI huI AI.
;
;"2.ladZaKadZA kara giranA"-aXikawara jaba manuRya jyAxA uCalawA hE,wo usake ladZaKadZA kara 
;               girane ke avasara aXika ho jAwe hEM-uCalanA
;The boy tripped over a brick.
;ladZakA Izta ke Upara ladZaKadZA kara gira gayA
;
;"3.purje kA Cataka jAnA/ruka jAnA"-kisI BI kala purje kA vAswava meM eka waraha se
;giranA hI usakI kArya praNAlI ko Tappa kara xewA hE-CatakanA yA uCala kara giranA-uCalanA
;
;One of the units of the power station has tripped.
;vixxayuwa gqha kI ikAIyoM meM se eka ruka gayI hE.
;
;"4.mAxaka xravya lekara kalpanAjagawa meM rahanA" -evaM ladZaKadZAnA-uCalanA
;She was tripped out after taking drugs.
;naSIle paxArWa lene ke bAxa vaha kalpanA jagawa meM WI.
;
;"trip","N",1.yAwrA/BramaNa 
;She is going on a official trip.
;vaha eka sarakArI yAwrA para jA rahI hE.
;
;                                                          
;    nota:--yaxi'trip'Sabxa ke saBI vAkyoM ke arWoM ko XyAna se xeKeM wo saBI arWo 
;         ko xo nABikIya arWa se jodZa kara sUwra banAyA jA sakawA hE
;
;  sUwra : yAwrA[<uCalanA]

;@@@Added by 14anu-ban-07,(13-03-2015)
;She's been on a real power trip since she became the office manager.(cambridge)
;वह एक वास्तविक शक्ति का अनुभव कर रही  है क्योंकि वह दफ्तर प्रबन्धक बन गयी है . (manual)
(defrule trip6
(declare (salience 4600))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 power)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava))
(assert (id-wsd_viBakwi ?id1 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip6   "  ?id "  anuBava )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  trip.clp trip6  "  ?id1 " kA)" crlf)
)
)

;@@@Added by 14anu-ban-07,(13-03-2015) 
;removed (id-root ?id1 stair) by 14anu-ban-07,(19-03-2015) 
;She broke her ankle when she had a nasty trip on the stairs.(cambridge)
;उसने उसका टखना तोड लिया जब उसने सीढियों पर बुरी तरह से ठोकर खाई  .(manual)
(defrule trip7
(declare (salience 4700))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Tokara_KAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip7   "  ?id "  Tokara_KAI )" crlf)
)
)

;@@@Added by 14anu-ban-07,(13-03-2015)
;She looked stunning as she tripped down the stairs in her ball gown.(cambridge)
;वह  अत्यंत सुंदर दिख रही थी जैसे  ही वह उसके बॉल वाले चोगे में नीचे नजाकत से आई. (manual)
(defrule trip8
(declare (salience 4700))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najZAkawa_se_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip8   "  ?id "  najZAkawa_se_A )" crlf)
)
)

;@@@Added by 14anu-ban-07,(13-03-2015)
;A special system prevents the circuitry being tripped accidentally by a power surge or lightning strike.(cambridge)
; एक विशेष प्रणाली एक शक्ति आवेश या बिजली  गिरने के कारण संयोगवश विद्युत्-परिपथ तन्त्र को बंद होने से  रोकती है . (manual)
(defrule trip9
(declare (salience 4700))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 circuitry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trip.clp 	trip9   "  ?id "  bMxa_ho )" crlf)
)
)

;@@@Added by 14anu-ban-07,(13-03-2015) 
;changed meaning 'gira_jA' as Tokara_KAkara_gira_jA by 14anu-ban-07,(19-03-2015)
;She tripped up on the rug.(cambridge)
;वह कालीन पर ठोकर खाकर गिर गई .  (manual)
(defrule trip10
(declare (salience 4800))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-on_saMbanXI  ?id ?id2)
(id-root ?id2 rug|carpet)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Tokara_KAkara_gira_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trip.clp	trip10  "  ?id "  " ?id1 "  Tokara_KAkara_gira_jA  )" crlf))
)


;@@@Added by 14anu-ban-07,(13-03-2015)
;She said goodbye and tripped off along the road.(cambridge)
;उसने अलविदा कहा और सडक के किनारे से तेजी से चली गयी. (manual)
(defrule trip11
(declare (salience 4800))
(id-root ?id trip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) wejZI_se_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trip.clp	trip11  "  ?id "  " (+ ?id 1) "  wejZI_se_cala  )" crlf))
)
