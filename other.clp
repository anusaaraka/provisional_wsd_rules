;@@@ Added by 14anu16
;I don't know any French people other than you.
;मैं आप के अलावा अन्य किसी भी फ्रांसीसी लोगों को नहीं जानता हूँ.
(defrule other5
(declare (salience 5000))
(id-root ?id other)
(id-root =(+ ?id 1) than)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) ke_alAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  other.clp     other5   "  ?id "  "(+ ?id 1) "  ke_alAvA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (08-12-2014)
;@@@ Added by 14anu16
;He crashed into a car coming the other way.
;वह विपरीत मार्ग से आते हुए गाडी के अन्दर ढहा .
(defrule other6
(declare (salience 5050)) ;increased salience '5000' to '5050' by 14anu-ban-09 on (08-12-2014)
(id-root ?id other)
;(id-root =(- ?id 1) the) ;commented by 14anu-ban-09 on (08-12-2014)
;(id-word =(+ ?id 1) way|side) ;commented by 14anu-ban-09 on (08-12-2014)
(viSeRya-viSeRaNa  ?id1 ?id) ;Added by 14anu-ban-09 on (08-12-2014)
(id-root ?id1 way|side) ; Added by 14anu-ban-09 on (08-12-2014)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id viparIwa)) ;commented by 14anu-ban-09 on (08-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viparIwa_mArga )) ;Added by 14anu-ban-09 on (08-12-2014)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp     other6   "  ?id "  viparIwa )" crlf)) ;commented by 14anu-ban-09 on (08-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  other.clp     other6   "  ?id "  " ?id1 "  viparIwa_mArga )" crlf)) ;Added by 14anu-ban-09 on (08-12-2014)
)


(defrule other0
(declare (salience 5000))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other0   "  ?id "  xUsarA )" crlf))
)

(defrule other1
(declare (salience 4900))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other1   "  ?id "  anya )" crlf))
)

;"other","Det","1.anya"
;Ravi && two other men came to my place last night.
;Call any other person.
;--"2.xUsarA"
;We live the other side of the river.
;--"3.bAkI"
;Give this book to Mohan && keep the others for Ram.
;
(defrule other2
(declare (salience 4800))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other2   "  ?id "  xUsarA )" crlf))
)

;"other","Pron","1.xUsarA"
;Hameed first lifted the green book to read && then the other.
;Of the two options offered first is better than the other.
;--"2.anya"
;This place is preferable to any other.
;
(defrule other3
(declare (salience 4700))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other3   "  ?id "  anya )" crlf))
)

;In other words ... (other is marked as adjective --JJ)

;Commented by 14anu-ban-09 on (26-11-2014)
;NOTE- As there is no example sentence for this rule.The meaning of this rule is coming from multi_word_expressions.txt.Because of this reason, commented this rule.
;(defrule other4
;(declare (salience 5000))
;(id-root ?id other)
;(id-root =(- ?id 1) each)
;;(id-root ?id1 each)
;(id-root ?id2 resemble)
;?mng <-(meaning_to_be_decided ?id)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) eka_xUsare))
;(assert (id-H_vib_mng ?id ke))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  other.clp     other4   "  ?id "  "(- ?id 1) "  eka_xUsare )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  other.clp     other4  "  ?id " ke )" crlf))
;)

;@@@ Added by 14anu-ban-09 on (03-10-2014)
;If we have a collection of point masses, the force on any one of them is the vector sum of the gravitational forces exerted by the other point masses as shown in Fig 8.4.  [NCERT CORPUS]
;yaxi hamAre pAsa binxu xravyamAnoM kA koI saFcayana hE, wo unameM se kisI eka para bala anya binxu xravyamAnoM ke kAraNa guruwvAkarRaNa baloM ke saxiSa yoga ke barAbara howA hE jEsA ki ciwra 8.4 meM xarSAyA gayA hE. 

(defrule other7
(declare (salience 5800))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-root ?id1 mass)
(id-root ?id2 point)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other7   "  ?id "  anya )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (08-12-2014)
;###[Counter Example]###As mentioned in the introductory section, one pole is designated the North pole and the other, the South pole. [NCERT CORPUS]
;जैसा कि पहले भूमिका में बताया जा चुका है, एक ध्रुव को उत्तर और दूसरे को दक्षिण ध्रुव कहते हैं. [NCERT CORPUS]
;@@@ Added by 14anu11
;बाकी समय में वे स्थिर रहते हैं या वैसा दिखते हैं और उस समय वे लोगों को समझदारी से काम करने की सलाह देते दिखते हैं .
;At other times he is static , or seemingly so , counselling others to prudence. 
(defrule other8
(declare (salience 5500))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 counsel) ;added by 14anu-ban-09 on (08-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id logoM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other8   "  ?id "  logoM )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-02-2015)
;It's possible to pass on the virus to others through physical contact.  [cald]
;यह सम्भव है कि शारीरिक सम्पर्क से दूसरो को वायरस फैलता हैं .  			 [Own Manual]

(defrule other9
(declare (salience 5500))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other9   "  ?id "  xUsarA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;Power is not something that is possessed such as blue eyes or red hair but manifests itself in terms of relations with others.	[bnc Corpus]
;शक्ति कोई ऐसी वस्तु नहीं है जिसको रखा जा सकता है जैसेकि नीली आँखे या लाल बाल , लेकिन दूसरों के साथ संबंध के संद्रभ में स्वतः ही प्रकट हो जाता है. 		[bnc manual]
;शक्ति कुछ ऐसी चीज़ नहीं है जिससे युक्त हो सकते है जैसे कि नीली आँखे या लाल बाल , लेकिन दूसरों के साथ संबंध के संद्रभ में स्वतः ही प्रकट हो जाती है.  [Self]
(defrule other10
(declare (salience 5500))
(id-root ?id other)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI  ?id1 ?id)
(id-root ?id1 relation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  other.clp 	other10   "  ?id "  xUsarA )" crlf))
)


;In other words ... (other is marked as adjective --JJ)

