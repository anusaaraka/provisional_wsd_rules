
;Added by human
(defrule tape0
(declare (salience 5000))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 record)
(viSeRya-on_saMbanXI ?id1 ?id) ;Replaced viSeRya-on_viSeRaNa as viSeRya-on_saMbanXI programatically by Roja 09-11-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teparikArdara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape0   "  ?id "  teparikArdara )" crlf))
)

(defrule tape1
(declare (salience 4900))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) deck)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tepa_rikArdara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape1   "  ?id "  tepa_rikArdara )" crlf))
)

(defrule tape2
(declare (salience 4800))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape2   "  ?id "  pattI )" crlf))
)

(defrule tape3
(declare (salience 4700))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIwe_se_bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape3   "  ?id "  PIwe_se_bAzXa )" crlf))
)

;@@@Added by 14anu15  Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
; The papers were in a pile, tied together with a tape.
;कागज फीते एक साथ बाँधा हुआ ढेर में थे . 
(defrule tape4
(declare (salience 4800))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape4   "  ?id "  PIwA )" crlf))
)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;Police seized various books and tapes. 
;पुलीस ने विभिन्न पुस्तकें और टेप जब्त किए .
(defrule tape5
(declare (salience 4800))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(conjunction-components  ? ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tepa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape5   "  ?id "  tepa )" crlf))
)

;@@@Added by 14anu15,Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;I lent her my Bob Marley tapes. 
;मैंने  बोब् मारले कें कैसेट प्रदान किए . 
(defrule tape6
(declare (salience 4800))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kEseta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape6   "  ?id "  kEseta )" crlf))
)


;"tape","V","1.PIwe_se_bAzXanA"
;You tape the bundle.
;--"2.aBileKana_karanA"
;He taped his entire speech.
;
;

;@@@Added by 14anu-ban-07,(28-04-2015)
;He taped his entire speech.(same file)
;उसने उसका सम्पूर्ण भाषण रिकार्ड किया . (manual)
(defrule tape7
(declare (salience 4800))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 speech)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rikArda_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape7   "  ?id "  rikArda_kara )" crlf))
)

;@@@Added by 14anu-ban-07,(28-04-2015)
;Private conversations between the two had been taped and sent to a newspaper.(oald)
;दोनो के बीच के व्यक्तिगत वार्तालाप को रिकार्ड किया  और समाचारपत्र  को भेजा दिया था .  (manual)
(defrule tape8
(declare (salience 4900))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 conversation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rikArda_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape8   "  ?id "  rikArda_kara )" crlf))
)

;@@@Added by 14anu-ban-07,(28-04-2015)
;Someone had taped a message on the door.(oald)
;किसीने दरवाजे पर सन्देश चिपकाया था . (manual)
(defrule tape9
(declare (salience 4900))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-on_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape9   "  ?id "  cipakA )" crlf))
)

;@@@Added by 14anu-ban-07,(28-04-2015)
;He injured himself playing football and his arm was taped up.(cambridge)
;उसने फुटबॉल खेलता हुआ स्वयं को घायल किया और उसकी बाँह पर पट्टी बाँधी गयी थी . (manual)
(defrule tape10
(declare (salience 5000))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI_bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape10   "  ?id "  pattI_bAzXa )" crlf))
)

;@@@Added by 14anu-ban-07,(28-04-2015)
;She taped a note to the door.(cambridge)
;उसने दरवाजे पर एक नोट चिपकाया. (manual)
(defrule tape11
(declare (salience 5100))
(id-root ?id tape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 note)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tape.clp 	tape11   "  ?id "  cipakA )" crlf))
)


