
(defrule seem0
(declare (salience 5000))
(id-root ?id seem)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id seeming )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prawIyamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  seem.clp  	seem0   "  ?id "  prawIyamAna )" crlf))
)

;"seeming","Adj","1.prawIyamAna"
;He is a seeming man.
;
(defrule seem1
(declare (salience 4900))
(id-root ?id seem)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawIwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seem.clp 	seem1   "  ?id "  prawIwa_ho )" crlf))
)

;"seem","V","1.prawIwa honA/laganA/jAna padZanA"
;It seems that it will rain heavily today.
;


;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu20 ,MNNIT alld.
;There seems to be opportunity
;;There seems to be an opportunity.[Sentence improved by 14anu-ban-01 on (27-12-2014)]
;vahAz para mOkA prawIwa hE.
;एक अवसर प्रतीत होता है.[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule seem3
(declare (salience 5100))
(id-root ?id seem)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1)  to)
(id-word =(+ ?id 2)  be) 
(id-cat_coarse ?id verb)
;(kriyA-kriyArWa_kriyA  ?id ?id1)  ; commented by 14anu-ban-01 on (27-12-2014) because seems_to_be will always mean prawIwa_ho and specified condition is not required.
;(id-cat_coarse ?id1 noun) ; commented by 14anu-ban-01 on (27-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) prawIwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " seem.clp	seem3  "  ?id "  " (+ ?id 1) "  "  (+ ?id 2) "  prawIwa_ho  )" crlf))
)
