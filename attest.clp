;@@@Added by 14anu-ban-02(02-03-2015)
;Sentence: Contemporary accounts attest to his courage and determination.[oald]
;Translation:  समकालीन विवरण उसकी हिम्मत और  दृढ़ता को प्रमाणित करता है.[self]
(defrule attest0 
(declare (salience -1)) 
(id-root ?id attest) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pramANiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  attest.clp  attest0  "  ?id "  pramANiwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  attest.clp  attest0   "  ?id " ko )" crlf)) 
) 

;@@@Added by 14anu-ban-02(02-03-2015)
;Decision of monetary bill , if it is not attested by speaker then that will not be considered as monetary bill and his decision will be final and obligatory .[karan singla]
;धन बिल का निर्धारण स्पीकर करता है यदि धन बिल पे स्पीकर साक्ष्यांकित नही करता तो वह धन बिल ही नही माना जायेगा उसका निर्धारण अंतिम तथा बाध्यकारी होगा.[karan singla]
;यदि धन बिल पर स्पीकर द्वारा हस्ताक्षर नहीं किया गया  तो वह धन बिल ही नही माना जायेगा और उसका निर्धारण अंतिम तथा बाध्यकारी होगा.[self]
(defrule attest1 
(declare (salience 100)) 
(id-root ?id attest) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 speaker|principle|dean)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id haswAkRara_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  attest.clp  attest1  "  ?id "  haswAkRara_kara )" crlf))
) 
