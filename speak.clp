
(defrule speak0
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id spoke )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wIlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  speak.clp  	speak0   "  ?id "  wIlI )" crlf))
)

;"spoke","N","1.wIlI{sAikila_iwyAxi_kI}"
;The ball hit so hard that almost all spokes are broken.
;
(defrule speak1
(declare (salience 4900))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id spoke )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bola))
(assert (id-H_vib_mng ?id yA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  speak.clp  	speak1   "  ?id "  bola )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  speak.clp      speak1   "  ?id " yA )" crlf))
)

;"spoke","V","1.bolA"
;The words you spoke are not liked by anyone.
;
(defrule speak2
(declare (salience 4800))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speak.clp 	speak2   "  ?id "  bola )" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-04-2015) 
;She spoke to both sides in the dispute in an attempt to smooth things over.[oald]
;उसने चीजों को ठीक करने के प्रयास में  विवाद में [डूबे] दोनों पक्षों से बात की . [self]
(defrule speak3
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa_kara/bAwacIwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speak.clp 	speak3  "  ?id "  bAwa_kara/bAwacIwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015) 
;He spoke for forty minutes at the conference.[speak.clp]
;उसने सभा में चालीस मिनट भाषण दिया.[speak.clp]
(defrule speak4
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?)
(id-root ?id1 conference)
(kriyA-at_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BARaNa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speak.clp 	speak4  "  ?id "  BARaNa_xe )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015)
;The words you spoke are not liked by anyone.[speak.clp]
;जो शब्द आपने कहे वे किसी को भी पसन्द नहीं आए . [self]
(defrule speak5
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id  ?id2)
(id-root ?id1 word)
(viSeRya-jo_samAnAXikaraNa  ?id1  ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speak.clp 	speak5  "  ?id "  kaha )" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-04-2015)
;I can’t speak for the others, but I’d love to come myself.[oald]
;मैं दूसरों की ओर से नहीं बोल सकता , परन्तु मैं स्वयं आना पसन्द करूँगा . [self]
(defrule speak6
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) for)
(kriyA-for_saMbanXI  ?id ?id1)
(pada_info (group_head_id ?id1)(preposition =(+ ?id 1)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) ora_se_bola))
(assert (id-wsd_viBakwi ?id1 kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  speak.clp 	speak6  "  ?id " kI)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  speak.clp 	speak6  "  ?id "  "(+ ?id 1)"  ora_se_bola )" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-04-2015)
;Our party speaks for the poor and unemployed.[oald]
;हमारी पार्टी कमज़ोर/गरीब और बेरोजगारों का  प्रतिनिधित्व करती है . [self]
(defrule speak7
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) for)
(id-root ?id1 poor|unemployed)
(kriyA-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) prawiniXiwva_kara))
(assert (id-wsd_viBakwi ?id1 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  speak.clp 	speak7  "  ?id1 " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  speak.clp 	speak7  "  ?id " "(+ ?id 1)"  prawiniXiwva_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015) 
;Everything here speaks of perfect good taste.[oald]
;यहां  सब-कुछ सम्पूर्ण/संपन्न आकर्षक शैली/पसंद का अहसास कराता है .  [self]
(defrule speak8
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 taste|choice)
(kriyA-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ahasAsa_karA))
(assert (id-wsd_viBakwi ?id1 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  speak.clp 	speak8  "  ?id1 " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speak.clp 	speak8  "  ?id "  ahasAsa_karA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015)
; I will continue to speak out on matters of public concern. [oald]
;मैं सरकारी उद्यम के विषयों पर साहस के साथ बोलना जारी रखूँगा . [self]
(defrule speak9
(declare (salience 5000))
(id-root ?id speak)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAhasa_ke_sAWa_bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  speak.clp 	speak9  "  ?id " "?id1"  sAhasa_ke_sAWa_kaha )" crlf))
)




;default_sense && category=verb	bAwacIwa kara	0
;"speak","V","1.bAwacIwa karanA"
;He spoke to me regarding my studies.
;--"2.bolanA"
;He speaks well on this topic.
;
;LEVEL 
;Headword : speak
;
;Examples  -- 
;
;"speak","V","bolanA"
;Speak clearly
;spaRta bolo
;Do you speak English?
;kyA wuma aMgrejI bolawe ho?
;--"2.BARaNa_xenA"-bolanA
;He spoke for forty minutes at the   conference
;usane saBA meM cAlIsa minata BARaNa xiyA 
;--"3.bAwacIwa_karanA"-bolanA
;I was speaking to him only yesterday
;mEneM kala hI usase kala hI bAwa kara rahA WA.
;
;vyAKyA - uparyukwa vAkya 4.meM bAwacIwa_karane kA wAwparya hE ki Apasa me bolanA  
;
;sUwra : bolanA`
