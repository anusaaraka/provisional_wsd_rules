;@@@ Added by 14anu-ban-01 on (12-02-2015)
;The prime minister's speech fuelled speculation that she is about to resign.[cald]
;प्रधान मंत्री के भाषण ने इस अटकलबाज़ी को बढ़ा दिया कि वे इस्तीफा देने वाली हैं. [self]
(defrule speculation1
(declare (salience 1000))
(id-root ?id speculation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 fuel|prompt|raise|end|avoid|encourage|dismiss|bring|hear)
(kriyA-object  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id atakalabAjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " speculation.clp  	speculation1   "  ?id "  atakalabAjI)" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (12-02-2015)
;There was widespread speculation that she was going to resign.[oald]
;दूर दूर तक फैली हुई अटकलें थीं कि वह त्याग पत्र देने जा रही थी .  [self]
(defrule speculation2
(declare (salience 1000))
(id-root ?id speculation)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?)	;It can be restricted later when counter examples will occur.
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id atakala))
(assert (id-wsd_number ?id p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number  " ?*prov_dir* "  speculation.clp     speculation2   "  ?id " p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " speculation.clp  	speculation2   "  ?id "  atakala)" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (12-02-2015)
;Democrats are pushing a measure to curb speculation in oil markets.[COCA]--> Parse no.4
;प्रजातन्त्रवादी तेल के बाजारों में [हो रही] मुनाफाखोरी नियन्त्रित करने के उपाय को कार्यान्वित करने पर जोर दे रहे हैं .   [self]
;Speculation in oil .[oald]
;तेल में मुनाफाखोरी . [self]
(defrule speculation3
(declare (salience 1000))
(id-root ?id speculation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 oil|food|brokerage|commodity|market)
(viSeRya-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id munAPAKorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " speculation.clp  	speculation3   "  ?id "   munAPAKorI)" crlf))
)

;------------------------ Default Rules --------------------

;@@@ Added by 14anu-ban-01 on (12-02-2015)
;Our speculations proved right.[oald]
;हमारी परिकल्पनाएँ सच्ची साबित हुईं. [self]
(defrule speculation0
(declare (salience 0))
(id-root ?id speculation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parikalpanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " speculation.clp  	speculation0   "  ?id "   parikalpanA)" crlf))
)

