;@@@ Added by 14anu-ban-08 (16-02-2015)
;Lost property.   [hindkhoj]
;खोई हुई सम्पत्ति.  [self]
(defrule lost0
(declare (salience 0))
(id-root ?id lost)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id KoyA_huA))   ;modify 'KoI_huI' to 'KoyA_huA' by 14anu-ban-08 (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  lost.clp 	lost0   "  ?id " KoyA_huA )" crlf))   ;modify 'KoI_huI' to 'KoyA_huA' by 14anu-ban-08 (20-02-2015)
)

;@@@Added by 14anu-ban-08 (16-02-2015)    ;Runs on parser 3, root problem in this 'lost'
;The lost kinetic energy of the fluid gets converted into heat energy.  [NCERT]
;तरल की लुप्त गतिज ऊर्जा ऊष्मा ऊर्जा में परिवर्तित हो जाती है.  [NCERT]     ;modified by 14anu-ban-08 (28-02-2015)
(defrule lost1
(declare (salience 4200))
;(Domain physics)                  ;commented by 14anu-ban-08 (28-02-2015)  
(id-word ?id lost)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 energy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lupwa))    ;changed meaning from 'kRaya' to 'lupwa' by 14anu-ban-08 (28-02-2015)  
;(assert (id-domain_type  ?id physics))   ;commented by 14anu-ban-08 (28-02-2015)  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lost.clp 	lost1   "  ?id " lupwa )" crlf))      
;(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  lost.clp 	lost1   "  ?id "  physics )" crlf))  ;commented by 14anu-ban-08 (28-02-2015)  
)
