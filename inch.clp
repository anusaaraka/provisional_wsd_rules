;@@@ Added by 14anu-ban-06 (17-02-2015)
;He escaped death by an inch. (OALD)
;वह मृत्यु से बाल-बाल बची. (manual)
(defrule inch2
(declare (salience 5100))
(id-root ?id inch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 2) by)
(id-root =(- ?id 1) an)
(kriyA-object ?id2 ?id1)
(kriyA-by_saMbanXI ?id2 ?id)
(id-root ?id1 death|accident)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) bAla-bAla))
(assert  (id-wsd_viBakwi   ?id1  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " inch.clp	 inch2  "  ?id "  " =(- ?id 1) =(- ?id 2) " bAla-bAla  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  inch.clp      inch2   "  ?id1 " se )" crlf)
)
)

;----------------------- Default Rules --------------------
;"inch","N","1.iMca"
;Could you please move an inch.
(defrule inch0
(declare (salience 5000))
(id-root ?id inch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inch.clp 	inch0   "  ?id "  iMca )" crlf))
)

;"inch","V","1.XIre_XIre_Age_baDZanA"
;They inched their way across the narrow footbridge.
(defrule inch1
(declare (salience 4900))
(id-root ?id inch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XIre_XIre_Age_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inch.clp 	inch1   "  ?id "  XIre_XIre_Age_baDZa )" crlf))
)

