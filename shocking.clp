;@@@ Added by 14anu-ban-01 on (13-11-2014)
;This is a shocking news.[self:with reference to oald]
;यह एक चौंका देने वाला समाचार है[self]

(defrule shocking00
(declare (salience 0))
(id-root ?id shocking )
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cOMkA_xe))
(assert (make_verbal_noun ?id))
(assert (id-wsd_viBakwi ?id vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  shocking.clp  	shocking00   "  ?id "  cOMkA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  shocking.clp  	shocking00   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  shocking.clp  	shocking00   "  ?id " vAlA)" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (13-11-2014)
;She was the only witness of that shocking sight.[self]
;वह उस दहला देने वाले दॄश्य की इकलौती गवाह थी[self]

(defrule shocking0
(declare (salience 1000))
(id-root ?id shocking )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 sight|scene|death|murder|crime)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xahalA_xe))
(assert (make_verbal_noun ?id))
(assert (id-wsd_viBakwi ?id vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  shocking.clp  	shocking0   "  ?id "  xahalA_xe)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  shocking.clp  	shocking0   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  shocking.clp  	shocking0   "  ?id " vAlA)" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (13-11-2014)
;The food here is shocking.[shock.clp]
;यहाँ का खाना बहुत खराब है[self]

(defrule shocking1
(declare (salience 5000))
(id-root ?id shocking )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 food|dish|cuisine)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shocking.clp 	shocking1   "  ?id "  bahuwa_KarAba )" crlf))
)
