;@@@ Added by 14anu22
;I like my pen.
;मैं मेरी कलम को पसन्द करता हूँ . 
(defrule pen0
(declare (salience 4000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " pen.clp    pen0  "  ?id "  kalama )" crlf))
)

;@@@ Added by 14anu22.
;She penned everything.
;उसने सब कुछ लिखा.
(defrule pen1
(declare (salience 4000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " pen.clp    pen1  "  ?id "  liKa )" crlf))
)

;@@@ Added by 14anu22
;She penned down everthing.
;उसने सब कुछ लिखा.
(defrule pen2
(declare (salience 5100))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down )
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pen.clp pen2 " ?id " " ?id1 "  liKa)" crlf)))

;$$$ Modified by 14anu-ban-09 on (27-11-2014)
;@@@ Added by 14anu22
;I made a nice pen for animals.
;मैने पशुओं के लिये अच्छा बाडा बनाया.
(defrule pen3
(declare (salience 5500))
(id-root ?id pen)
(id-root ?id1  animal|cattle|cow|buffalo|goat|sheep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id2 ?id) ;added by 14anu-ban-09 on (27-11-2014)
(kriyA-for_saMbanXI  ?id2 ?id1) ;added by 14anu-ban-09 on (27-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " pen.clp    pen3  "  ?id "  bAdA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (27-11-2014)
;NOTE-There is a root problem in the below sentence.The root problem will correct soon.The rule is ok!
;$$$ Modified by 14anu09
;corrected meaning 'bAde_meM_bMwa_kara' as 'bAde_meM_baMwa_kara'
;@@@ Added by 14anu22
;I penned the animals.
;मैने पशुओं को बाडे मे बन्द किया.
(defrule pen4
(declare (salience 6000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1) ;added by 14anu-ban-09 on (27-11-2014)
(id-root ?id1  animal|cattle|cow|buffalo|goat|sheep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAde_meM_baMxa_kara)) 
;corrected spelling 'bAde_meM_bMwa_kara' as 'bAde_meM_baMxa_kara' by 14anu-ban-09 on (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " pen.clp    pen4  "  ?id " bAde_meM_baMxa_kara)" crlf))
)

;@@@ Added by 14anu09 [26-06-14]
;Pen up the details of the conversation.
;वार्तालप की व्यौरा रेखाङ्कित कीजिए . 
(defrule pen5
(declare (salience 4000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 reKAMkiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pen.clp pen5 " ?id " " ?id1 "  reKAMkiwa_kara)" crlf)))

;@@@ Added by 14anu09 
;Pen in the cattle.
;	पशु में बाडे में बन्त कीजिए . 
;पशु बाडे में बन्द कीजिए . [Anusaaraka] ;added by 14anu-ban-09 on (09-12-2014)
(defrule pen6
(declare (salience 7000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-word ?id2 in)
(id-root ?id1  animal|cattle|cow|buffalo|goat|sheep)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 bAde_meM_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pen.clp pen6 " ?id " " ?id2 "  bAde_meM_baMxa_kara)" crlf)))

;@@@ Added by 14anu-ban-09 on (09-12-2014)
;The whole family were penned up in one room for a month. [OALD]
;पूरे परिवार को एक महीने के लिए एक कमरे में बन्द कर दिया गया था . [Self] 
;The robbers penned up the whole family in one room for a month. [OALD-Active form of upper sentence in same CLP file]
;डाकुओं ने पूरे परिवार को एक महीने के लिए एक कमरे में बन्द कर दिया. [Self] 

(defrule pen7
(declare (salience 4000))
(id-root ?id pen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up)
(kriyA-in_saMbanXI  ?id ?id2)
;(kriyA-object  ?id ?id2)
(id-root ?id2 room)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pen.clp pen7 " ?id " " ?id1 "  baMxa_kara_xe)" crlf)))


