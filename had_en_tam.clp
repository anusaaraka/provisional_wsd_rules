;Commented by Shirisha Manju -- This rule need to be add in had_not_en file
;;Added by sheetal(5-10-09)
;;He had not seen him since that graduation day .
;(defrule had_en_tam0
;(declare (salience 5000))
;(id-TAM ?id had_en)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id see)
;=>
;(retract ?mng)
;(assert (id-E_tam-H_tam_mng ?id had_en yA_WA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  had_en_tam.clp   had_en_tam0  "  ?id "  yA_WA )" crlf))
;)

;@@@ Added by Shirisha Manju Suggested by Chaitanya Sir 12-02-2015
;By the nineteenth century, enough evidence had accumulated in favor of atomic hypothesis of matter.
;unnIsavIM SawAbxI waka paxArWa kI paramANvIya parikalpanA ke samarWana meM kAPI sAkRya ekawriwa ho gae We .
(defrule had_en_tam1
(declare (salience 5000))
(id-TAM ?id had_en)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?pid))
(id-wsd_root_mng ?pid waka)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id had_en 0_gayA_WA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  had_en_tam.clp   had_en_tam1  "  ?id "  0_gayA_WA )" crlf))
)

