;@@@ Added by 14anu-ban-11 (28-03-2015)
;The little wretches have splashed water all over the bathroom. (oald)
;छोटे शैतान ने पूरे स्नानघर पर पानी के छींटे डाल दिये . (self)
(defrule wretch1
(declare (salience 10))
(id-root ?id wretch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 little)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SEwAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wretch.clp 	wretch1   "  ?id " SEwAna )" crlf))
)


;@@@ Added by 14anu-ban-11 (28-03-2015)
;The poor wretch has lost his mother.(oald) 
;दीन बेचारे ने अपनी माँ को खो दिया है . (self)
(defrule wretch0
(declare (salience 00))
(id-root ?id wretch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wretch.clp 	wretch0   "  ?id " becArA )" crlf))
)

