;@@@ Added by 14anu-ban-06 (06-04-2015)
;The source of infection may be a person who is incubating an infectious disease. (OALD)
;संक्रमण का स्रोत एक ऐसा व्यक्ति हो सकता है जिसमें छूत की बीमारी पनप रही है . (manual)
(defrule incubate1
(declare (salience 2000))
(id-root ?id incubate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id panapa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incubate.clp 	incubate1   "  ?id "  panapa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (06-04-2015)
;The female bird incubates the eggs for about 16 days. (cambridge)
;मादा चिडिया लगभग 16 दिनों तक अण्डे सेती है . (manual)
(defrule incubate0
(declare (salience 0))
(id-root ?id incubate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incubate.clp 	incubate0   "  ?id "  se )" crlf))
)
