
(defrule float0
(declare (salience 5000))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id floating )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id asWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  float.clp  	float0   "  ?id "  asWira )" crlf))
)

;"floating","Adj","1.asWira"
;he has only a floating interest in that girl.
;
;
(defrule float1
(declare (salience 4900))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-about_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " float.clp float1 " ?id "  PElA )" crlf)) 
)

(defrule float2
(declare (salience 4800))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " float.clp	float2  "  ?id "  " ?id1 "  PElA  )" crlf))
)

(defrule float3
(declare (salience 4700))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-around_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " float.clp float3 " ?id "  PElA )" crlf)) 
)

(defrule float4
(declare (salience 4600))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-around_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " float.clp float4 " ?id "  PElA )" crlf)) 
)

(defrule float5
(declare (salience 4500))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-around_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " float.clp float5 " ?id "  PElA )" crlf)) 
)

(defrule float6
(declare (salience 4400))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " float.clp	float6  "  ?id "  " ?id1 "  PElA  )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (28-11-2014)
;changed default meaning from 'baha' to 'wEra'
;A similar effect was observed when it was placed on a piece of cork which was then allowed to float in still water.[NCERT]
;EsA hI vyavahAra waba BI xeKane meM AwA WA jaba isako eka koYrka ke Upara raKa kara, usako Tahare hue pAnI meM wErAyA jAwA WA.[NCERT]
;EsA hI vyavahAra waba BI xeKane meM AwA WA jaba isako eka koYrka ke Upara raKa kara, usako Tahare hue pAnI meM wErane ke lie CodZa xiyA gayA WA.[MANUAL]
(defrule float7
(declare (salience 4300))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float7   "  ?id "  wEra )" crlf))
)

(defrule float8
(declare (salience 4200))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wErane_vAlA_paxArWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float8   "  ?id "  wErane_vAlA_paxArWa )" crlf))
)

;"float","N","1.wErane vAlA paxArWa"
;Do not forget to attach a float to the fishing line to prevent its sinking.
;--"2.JAzkI"
;There were many interesting floats in the Republic Day parade.
;--"3.rejZagArI"
;Bus conducters do not keep float with them to avoid giving change to passengers.
;
;
;@@@ Added by 14anu-ban-05 on (15-11-2014)
;Pictures of astronauts floating in a satellite show this fact.[NCERT-MODIFIED]
;vAyu meM wErawe anwarikRayAwriyoM ke ciwra TIka isI waWya ko xarSAwe hEM.[NCERT]
(defrule float9
(declare (salience 5000))
(Domain physics)
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 satellite|air|water)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id wErawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float9  "  ?id "   wErawA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  float.clp       float9   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (28-11-2014)
;You have watched specks of dust floating in air in a sunbeam entering through your window.[NCERT]
;Apane apanI KidakI se Ane vAle sUrya ke prakASa-puFja meM XUla ke kaNa wErawe xeKe hoMge.[NCERT]
;Apane apanI KidakI se Ane vAle sUrya ke prakASa-puFja meM XUla ke kaNa ko wErawe huye xeKe hoMge. [MANUAL]
(defrule float10
(declare (salience 5000))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat ?id gerund_or_present_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 dust)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wErawA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float10  "  ?id "   wErawA_ho )" crlf))
)

;@@@ Added by 14anu-ban-05 on (28-11-2014)
;A compass needle consists of a magnetic needle which floats on a pivotal point.[NCERT]
;cumbakIya xiksUcaka meM eka cumbakIya suI eka XurI para svawanwrawApUrvaka GUma sakawI hE.[NCERT]
;cumbakIya xiksUcaka eka cumbakIya suI se banI howI hE jo keMxrIya biMxu para svawanwrawApUrvaka GUma sakawI hE.[MANUAL]
(defrule float11
(declare (salience 5000))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 point)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svawanwrawApUrvaka_GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float11  "  ?id "   svawanwrawApUrvaka_GUma )" crlf))
)

;@@@ Added by 14anu-ban-05 on (28-11-2014)
;They float the logs down the river to the town. [OALD]
;उन्होंने लट्ठों को शहरों की तरफ नदी में नीचे की ओर बहाया.[MANUAL]
(defrule float12
(declare (salience 5000))
(id-root ?id float)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 log)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  float.clp 	float12  "  ?id "   bahA )" crlf))
)
