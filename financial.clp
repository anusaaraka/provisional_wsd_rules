;$$$ Modified by 14anu-ban-05 on (27-01-2015)
;In the financial month 2012, the rates of stock decreased by 1%.
;वित्तीय महीने 2012meM, स्टॉक की दरें 1 % तक घटीं . 
;@@@ Added by 14anu23 2/07/2014
;In the financial year 2012, the rates of stock decreased by 20%. 
;वित्तीय वर्ष 2012 में, शेयर की दरों में 20% की कमी हुई.
(defrule financial1
(declare (salience 5100))
(id-root ?id financial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)	;added by 14anu-ban-05 on (27-01-2015) 
(id-root ?id2 year|month|tenure) ;modified from (+ ?id 1) to ?id2 by 14anu-ban-05 on (27-01-2015) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viwwIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  financial.clp 	financial1   "  ?id "  viwwIya )" crlf))
)


;@@@ Added by 14anu23 2/07/2014
;To be in financial difficulties.  [oxfordlearnersdictionaries.com]
;आर्थिक कठिनाइयों में होना.
(defrule financial0
(declare (salience 5000))
(id-root ?id financial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArWika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  financial.clp 	financial0   "  ?id "  ArWika )" crlf))
)

