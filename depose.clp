;@@@ Added by 14anu-ban-04 (23-04-2015)
;She was nervous when the time to depose before the jury finally arrived.           [merriam-webster]
;वह भयभीत थी जब  जूरी/न्यायपीठ के सामने अन्ततः गवाही देने का समय आया .                                  [self]
(defrule depose4
(declare (salience 4030))
(id-root ?id depose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa ?id ?id1)
(kriyA-vAkya_viBakwi ?id1 ?id2)
(id-root ?id2 before)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gavAhI_xe))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depose.clp 	depose4   "  ?id "  gavAhI_xe)" crlf))
)
 
;$$$ Modified by 14anu-ban-04 (23-04-2015)
;I can depose when time arises.                [same clp file]
;जब समय  होगा तो मैं साक्षी दे सकता हूँ .                         [self]
;$$$ modified by Pramila(Banasthali University) on 06-01-2014
;condition added (not(kriyA-object  ?id ?id1))
(defrule depose0
(declare (salience 4900))
(id-root ?id depose)
?mng <-(meaning_to_be_decided ?id)
;(not(kriyA-object  ?id ?id1))               ;commented by 14anu-ban-04 (23-04-2015)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)                      ;added by 14anu-ban-04 (23-04-2015)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))     ;added by 14anu-ban-04 (23-04-2015)
(kriyA-vAkya_viSeRaNa ?id ?id2)                    ;added by 14anu-ban-04 (23-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAkRI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depose.clp 	depose0   "  ?id "  sAkRI_xe )" crlf))
)

;"depose","VI","1.sAkRI_xenA"
;I can depose when time arises
;
(defrule depose1
(declare (salience 4000))
(id-root ?id depose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxacyuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depose.clp 	depose1   "  ?id "  paxacyuwa_kara )" crlf))
)

;"depose","VT","1.paxacyuwa_karanA"
;The people depose political parties with wrong motives.
;

;@@@ Added by Pramila(Banasthali University) on 06-01-2014
;He deposed that the accused was there.            ;shiksharthi
;उसने बयान दिया कि अभियुक्त वहाँ पर था.
;The witness didn't depose that he had seen the man running away.           ;shiksharthi
;गवाह ने यह बयान नहीं दिया कि उसने आदमी को भागते हुए देखा था.
(defrule depose2
(declare (salience 5000))
(id-root ?id depose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bayAna_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depose.clp 	depose2   "  ?id "  bayAna_xe )" crlf))
)

;This rule is commented by 14anu-ban-04 on (17-12-2014) because the root of 'deposition' is not depose.So ,this rule transferred to deposition.clp. 
;@@@ Added by 14anu13 on 17-6-14
;It is now believed that this is not necessarily so but is due to deposition of fatty matter and fibrous substances in the arteries as per one ' s lifestyle .
;अब समझा जाता है कि ऐसा नहीं है , बल्कि धमनियों में वसीय एवं रेशेदार पदार्थों के निक्षेपण के कारण ऐसा हो सकता है .
;(defrule depose3
;(declare (salience 5000))
;(id-root ?id depose)
;(id-root ?id1 substances|matter)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-of_saMbanXI  ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nikRepaNa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depose.clp 	depose3   "  ?id "  nikRepaNa )" crlf))
;)
