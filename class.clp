;@@@ Added by Preeti(21-1-14)
;See me after class. [Oxford Advanced Learner's Dictionary]
;kakRA ke bAxa muJase milie.
;$$$ Modified by 14anu13 on 16-06-14      [removed on condition '(viSeRya-viSeRaNa  ?id ?)' to make class4 fire so I am giving that sentence as example for which this condition was wrong and class4 should be fired]
;The summit with the intermediate class of soldiers ended yesterday.
;मध्य वर्ग के सैनिको के साथ शिखर सम्मेलन कल समाप्त हो गया |

(defrule class2
(declare (salience 5060))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (kriyA-subject  ? ?id) (viSeRya-of_saMbanXI  ? ?id) (kriyA-after_saMbanXI  ? ?id) (kriyA-in_saMbanXI  ? ?id) (kriyA-for_saMbanXI  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class2   "  ?id "  kakRA )" crlf))
)

;@@@ Added by 14anu13 on 16-06-14
;The summit with the intermediate class of soldiers ended yesterday.
;मध्य वर्ग के सैनिको के साथ शिखर सम्मेलन कल समाप्त हो गया |
(defrule class04
(declare (salience 5060))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class04   "  ?id "  varga )" crlf))
)


;@@@ Added by 14anu-ban-03(26-08-2014)
;The resolution of such an electron microscope is limited finally by the fact that electrons can also behave as waves; (You will learn more about this in class XII).      [NCERT-CORPUS]
;isa prakAra ke ilektroYna - sUkRmaxarSI kA viBexana BI anwawaH isI waWya xvArA sImiwa howA hE ki ilektroYna BI warafgoM kI waraha vyavahAra kara sakawe hEM (isa viRaya meM viswAra se Apa kakRA @XII meM paDefge).          [NCERT-CORPUS]
;She split the class into groups of four. [oald]
;उसने कक्षा को चार समूहों में विभाजित कर दिया.
(defrule class4
(declare (salience 5060))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(samAsa_viSeRya-samAsa_viSeRaNa  ?  ?id) (kriyA-object ? ?id)) ; added 'kriyA-object' relation by  14anu-ban-03 (13-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class4   "  ?id "  kakRA )" crlf))
)

;@@@ Added by Preeti(21-1-14)
;She is a real class performer. [Oxford Advanced Learner's Dictionary]
;vaha eka buwa hI acCI aBinewA hE.
(defrule class3
(declare (salience 5050))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(or(id-cat_coarse ?id adjective) (id-cat_coarse =(+ ?id 1) noun))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class3   "  ?id "  acCA )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-10-2014)
;Circular motion is a familiar class of motion that has a special significance in daily-life situations. [ncert]
;वृत्तीय गति एक जाना-मानी श्रेणी है  जिसका हमारे दैनिक जीवन में विशेष महत्त्व है . [manual]
(defrule class5
(declare (salience 5060))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-word ?id1 familiar)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class5   "  ?id "  SreNI )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-10-2014)
;Another class of satellites are called the Polar satellites.[ncert]
;उपग्रह की अन्य श्रेणी को ध्रुवीय उपग्रह कहते हैं.[ncert]
(defrule class6
(declare (salience 5070))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 satellite)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class6   "  ?id "  SreNI )" crlf))
)

;$$$ Modified by 14anu-ban-03 (07-04-2015)
;@@@ Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 19.06.2014 email-id:sahni.gourav0123@gmail.com
;A society in which class is more important than ability.
;कौन सी जाति में समाज योग्यता की अपेक्षा अधिक महत्त्वपूर्ण है . 
(defrule class7
(declare (salience 4800))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)  ;added 'id1' by 14anu-ban-03 (07-04-2015)
(id-root ?id1 society)  ;added by 14anu-ban-03 (07-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class7   "  ?id "  jAwi )" crlf))
)

;$$$ Modified by 14anu-ban-03 (11-12-2014)  
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 19.06.2014 email-id:sahni.gourav0123@gmail.com
;It was good accommodation for a hotel of this class.
;इस वर्ग के होटल के लिए अच्छे में रुकने की व्य्वस्था था . 
;इस श्रेणी के होटल के लिए अच्छे में रुकने की व्य्वस्था था . [given by 14anu-ban-03 (11-12-2014)]
(defrule class8
(declare (salience 6000))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 hotel|conference|flight|party)
(viSeRya-of_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI))  ;meaning changed from 'SreNI kA' to 'SreNI' by 14anu-ban-03 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class8   "  ?id "  SreNI )" crlf))
)


;@@@ Added by 14anu-ban-03 (04-04-2015)
;He was the fourth player chosen in the 2007 draft, but the first of his class to reach the major leagues.         [oald]
;वह 2007 दल में चुना गया चौथा खिलाड़ी था, परन्तु  मुख्य संघ  तक  पहुँचने के लिए अपने वर्ग में प्रथम था  .    [self]
(defrule class9
(declare (salience 5060))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id) 
(id-root ?id1 first)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class9   "  ?id "  varga )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-04-2015)
;The brightest pupil in the class.[oald]
;कक्षा में सबसे अधिक तेज विद्यार्थी .[self] 
(defrule class10
(declare (salience 4800))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)  
(id-root ?id1 pupil)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class10   "  ?id "  kakRA )" crlf))
)


;------------------------------ Default rules -------------------
;$$$ Modified mng from 'varga' to 'SreNI' by Preeti 29-01-14.
;Ex:He always travels business class. 
(defrule class0
;(declare (salience 5000))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI));meaning changed by Preeti
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class0   "  ?id "  SreNI )" crlf))
)

;$$$ Modified mng from 'vargIkaraNa_kara' to 'vargIkqwa_kara' by Preeti 29-01-14. 
;Ex: I wouldn't have classed you as a Shakespeare fan.
(defrule class1
;(declare (salience 4900))
(id-root ?id class)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vargIkqwa_kara));meaning changed by Preeti
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  class.clp 	class1   "  ?id "  vargIkqwa_kara )" crlf))
)

;"class","V","1.vargIkaraNa_karanA"
;Words in the dictionary are classed invarious grammatical categories.
;
;LEVEL 
;
;
;               `class' sUwra (nibanXa)
;               -------
;
;`class' Sabxa ke viviXa prayoga--  
;-------------------------
;
;"class","N","1.varga"
;I belong to the middle class.
;
;--"2.SreNI" ----< vaha SreNI jo eka viSiRta varga ke liye hE ---< varga
;I travel in second-class compartment of the train.
;
;--"3.kakRA" ----< vixyArWiyoM kA eka viSiRta varga ----< varga
;My collegue && I had studied in the same class.
;
;--"4.jAwi" ---- < varga
;Whales belong to the mammal class.
;
;--"5.viSeRawA" ---- < viSeRa kAraNoM se varga kI pqWakawA < varga
;Hardy was in a different class from his contemperories.
;
;--"6.SreRTawA" ---- < viSeRawA < viSeRa kAraNoM se varga kI pqWakawA < varga
;Our football team lacks class.
;
;"class","V","1.vargIkaraNa_karanA" ---- < varga`      
;Words in the dictionary are classed invarious grammatical categories.
;------------------------------------------------------------------------
;
;sUwra : kakRA`[<varga`]
;---------
;
;sUwra mAwra 
;
;varga`   
;
;kyoM nahIM ?
;
;      `class' Sabxa ke upara ke viBinna prayogoM ke mUla meM varga Sabxa ko socA jA 
;sakawA hE . varga se wAwparya- eka EsA pqWak samUha, jo kinhIM kAraNoM se alaga parigaNiwa
;howA hE . isa xqRti se isa Sabxa ko upariliKiwa viBinna vargoM ke rUpa meM rUDa xeKA
;jA sakawA hE . 
;
;-- kakRA, SreNI, jAwi viBinna varga hEM . apane-apane kinhIM viSeRa kAraNoM se ye 
;xUsaroM se apanI-apanI pqWakawA banAye hue hEM . 
;   
;-- viSeRawA . kinhIM viSeRa kAraNoM se varga kI pqWakawA hE, awaH viSeRawA ke lie
;
;BI isakA prayoga Ama ho gayA lagawA hE .
;
;-- SreRTawA . kinhIM viSiRta XyAwavya kAraNoM se SreRTawA mAnI jAwI hE . isa 
;waraha varga se viSeRawA, viSeRawA se SreRTawA kI kadiyAz jodI jA sakawI hEM . 
;
;-- saMjFArUpa meM varga hE, isalie isakA kriyArUpa vargIkaraNa karanA ho gayA prawIwa 
;howA hE .   
;
;
