
;$$$ Modified by 14anu-ban-03 (05-02-2015)
;$$$  Modified by Preeti(12-12-13) Parasar problem
;One day he chanced upon Emma's diary and began reading it. [old clp]
;eka xina use emma kI dAyarI acAnaka_mila gayI Ora use paDanA AramBa kiyA.
;She had chanced on an old teacher of hers in a shop.  ;[this sentense is working on parser11)
;vaha eka xukAna para apane tIcara se acAnaka mila gayI WI
(defrule chance0
(declare (salience 5000))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)  ;uncommented by 14anu-ban-03 (05-02-2015)
(id-word ?id1 on|upon)
;(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acAnaka_mila_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " chance.clp	chance0  "  ?id "  " ?id1 "  acAnaka_mila_jA  )" crlf))
)

;$$$  Modified by Preeti(12-12-13)
;Please give me a chance to explain. [ Oxford Advanced Learner's Dictionary]
;kqpayA samaJAne ke liye muJe avasara xIjie.
(defrule chance1
(declare (salience 4950))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(subject-subject_samAnAXikaraNa  ? ?id) (kriyA-object_2  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance1   "  ?id "  avasara )" crlf))
)

;$$$ Modified by 14anu-ban-03 (04-02-2015)
;$$$  Modified by Preeti(12-12-13)
;They chanced to be staying at the same hotel. [ Oxford Advanced Learner's Dictionary]
;unake usI hotala meM rahane kA saMyoga banA.
(defrule chance2
(declare (salience 4850))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object  ?id ?))
(kriyA-kriyArWa_kriyA  ?id ?id1) 
(id-root ?id1 stay)    ;added by 14anu-ban-03 (04-02-2015)
;(and(kriyA-vAkyakarma  ?id ?id1) (id-root ?id1 stay)))  ;commented by 14anu-ban-03 (04-02-2015)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyoga_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance2   "  ?id "  saMyoga_bana )" crlf))
)

;default_sense && category=verb	saMyogavaSa_ho_jA	0
;"chance","V","1.saMyogavaSa_ho_jAnA" Changed by VC
;I chanced to meet my old friend in the city. 
;
;

;@@@ Added by 14anu-ban-03 (04-02-2015)
;In this national park along with elephant safari you can also get the chance to see a tiger. [tourism]
;इस नेशनल पार्क में आपको एलीफेंट सफारी के साथ बाघ  देखने का मौका भी मिल सकता है . [self]
;But then there was no likelihood that we would get a chance to ride in it .[tourism]
;लेकिन तब संभावना नहीं थी कि अगले दिन उसमें सवारी का मौका मिलेगा . [self]
(defrule chance6
(declare (salience 4900))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(id-root ?id1 see|ride)    ;'ride' is added by 14anu-ban-03 (11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOkA))  ;changed meaning from'avasara' to 'mOkA' by 14anu-ban-03 (11-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance6   "  ?id "  mOkA )" crlf))
)

;commented by 14anu-ban-03 (11-02-2015)
;@@@ Added by 14anu-ban-03 (04-02-2015)
;But then there was no likelihood that we would get a chance to ride in it .[tourism]
;लेकिन तब संभावना नहीं थी कि अगले दिन उसमें सवारी का मौका मिलेगा . [self]
;(defrule chance7
;(declare (salience 4900))
;(id-root ?id chance)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
;(id-root ?id1 ride)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mOkA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance7   "  ?id "  mOkA )" crlf))
;)

;@@@Added by 14anu-ban-02(15-01-2016)
;There are chance meetings with strangers that interest us from the first moment, before a word is spoken. [Crime And Punshment]
;कभी-कभी हमारी कुछ ऐसे अजनबियों से मुलाकात हो जाती है जिनमें हमें एक शब्द बातचीत के बिना भी पहले पल से ही दिलचस्पी पैदा हो जाती है.[Crime And Punshment]
(defrule chance7
(declare (salience 5000))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 yAxqcCika_BeteM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " chance.clp	chance7  "  ?id "  " ?id1 "  yAxqcCika_BeteM  )" crlf))
)


;---------------------- Default Rules ---------------------

;@@@ Added by Preeti(12-12-13)
;I stayed hidden; I could not chance coming out.[ Oxford Advanced Learner's Dictionary]
;mEM CipA rahA; mEM sAmane Ane kA joKima_nahIM_uTA sakA.
(defrule chance3
(declare (salience 4800))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  joKima_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance3   "  ?id "  joKima_uTA )" crlf))
)

;@@@ Added by Preeti(12-12-13)
;There is a slight chance that he will be back in time. [ Oxford Advanced Learner's Dictionary]
;WodI sI saMBAvanA hE ki vaha samaya para vApisa AyegA.
(defrule chance4
(declare (salience 4900))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance4   "  ?id "  saMBAvanA )" crlf))
)

;@@@ Added by Preeti(12-12-13)
(defrule chance5
(declare (salience 4900))
(id-root ?id chance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chance.clp 	chance5   "  ?id "  Akasmika )" crlf))
)

