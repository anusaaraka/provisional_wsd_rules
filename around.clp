
(defrule around0
(declare (salience 5000))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) is|are|be|was|were|been|am)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsapAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around0   "  ?id "  AsapAsa )" crlf))
)

(defrule around1
(declare (salience 4900))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) area|region|location|part|expanse|surface area)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_iXara_uXara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around1   "  ?id "  meM_iXara_uXara )" crlf))
)

;Modified by 14anu-ban-02 (01-12-2014)
;Since its time period is around 100 minutes it crosses any altitude many times a day.[ncert]
;चूँकि इन उपग्रहों का आवर्तकाल लगभग 100 मिनट होता है, अतः ये किसी भी अक्षांश से दिन में कई बार गुजरते हैं.[ncert]
;$$$ modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 16-dec-2013 
;They lose around 30 to 40 per cent of their moisture here.
;उनसे लगभग 30 से 40 प्रतिशत की नमी निकल जाती है।
(defrule around2
(declare (salience 5000));salience increased from 4800 to 5000 14anu-ban-02(01-12-2014) because around0 was getting fired.
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) integer|whole number|number); commented by Garima Singh
(id-cat_coarse =(+ ?id 1) number|integer); added by Gariam Singh
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around2   "  ?id "  lagaBaga )" crlf))
)

(defrule around3
(declare (salience 4700))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around3   "  ?id "  lagaBaga )" crlf))
)

;"around","Adv","1.lagaBaga"
;There are around 20,000 people in the stadium today.
;--"2.cAro_ora"
;The children were running around in the garden.
;--"3.GerevAlA"
;The town was 100 kilometers around.
;




;Added by Meena(3.3.10)
;The earth must revolve around the sun , Copernicus reasoned .
(defrule around4
(declare (salience 4600))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) ?kriyA) 
(kriyA-around_saMbanXI  ?id1 ?id2)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_cAroM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around4   "  ?id "  ke_cAroM_ora )" crlf))
)




(defrule around5
(declare (salience 4600))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id particle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hara_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around5   "  ?id "  hara_ora )" crlf))
)

;"around","Part","1.hara_ora"
;He looked around.
;Someone will show you around.
;--"2.iXara_uXara"
;There were insects scampering around.
;--"3.kuCa_nahIM_karawe_hue"
;Several people were sitting around looking at the passers by.
;--"4.mOjUxa_honA"
;There were more fish around in the tzar's reign.
;--"5.Asa_pAsa"
;There is no one around.
;--"6.pICe_kI_ora"
;He turned around && ran back to the station for his bag.
;



;Salience reduced by Meena(3.3.10)
(defrule around6
(declare (salience 0))
;(declare (salience 4500))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_iXara_uXara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp 	around6   "  ?id "  ke_iXara_uXara )" crlf))
)

;"around","Prep","1.iXara_uXara"
;A large number of people were running around the place.
;Books were lying around the room.
;--"2.cAroM_ora"
;He had tied a cloth around his waist.
;--"3.Asa_pAsa"
;I saw him around the garden in the afternoon.
;--"4.GUma_kara"
;The thief ran into a man as he turned around the corner.
;
;@@@ Added by 14anu-ban-01 on (20-10-2014)
;If you look around, you will come across many examples of rotation about an axis, a ceiling fan, a potter's wheel, a giant wheel in a fair, a merry-go-round and so on (Fig 7.3 (a) and (b)).[NCERT corpus]
;यदि आप अपने चारों ओर देखें तो आपको छत का पङ्खा, कुम्हार का चाक (चित्र 7.3(a) एवं (b)) , विशाल चक्री-झूला (जॉयन्ट व्हील), मेरी-गो-राउण्ड जैसे अनेक ऐसे उदाहरण मिल जायेंगे जहाँ किसी अक्ष के परितः घूर्णन हो रहा हो.[NCERT corpus]
(defrule around7
(declare (salience 4600))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id1 ?id)
(id-root ?id1 look)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAroM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around7   "  ?id "  cAroM_ora )" crlf))
)

;$$$ Modified by 14anu-ban-02 (01-12-2014)
;For example, the same law of gravitation (given by Newton) describes the fall of an apple to the ground, the motion of the moon around the earth and the motion of planets around the sun.[ncert]
;उदाहरण के लिए, समान गुरुत्वाकर्षण का नियम (जिसे न्यूटन ने प्रतिपदित किया) पृथ्वी पर किसी सेब का गिरना, पृथ्वी के परितः चन्द्रमा की परिक्रमा तथा सूर्य के परितः ग्रहों की गति जैसी परिघटनाओं की व्याख्या करता है.[ncert]
;@@@ Added by 14anu-ban-01 on (20-10-2014)
;This movement of the axis of the top around the vertical is termed precession.[NCERT corpus]
;ऊर्ध्वाधर के परितः लट्टू की अक्ष का इस प्रकार घूमना पुरस्सरण कहलाता है.[NCERT corpus]
(defrule around8
(declare (salience 4600))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-around_saMbanXI  ? ?id1)
(id-root ?id1 vertical|horizontal|earth|sun);'earth' and 'sun' is added by 14anu-ban-02(01-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariwaH))
(assert (id-wsd_viBakwi ?id1 ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  around.clp    around8   "  ?id1 " ke)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around8   "  ?id "  pariwaH)" crlf))
)


;@@@ Added by 14anu-ban-11 on (26-11-2014)
;Around the same time, in 1887, it was found that certain metals, when irradiated by ultraviolet light, emitted negatively charged particles having small speeds.(Ncert) 
;लगभग उसी समय, 1887 में, यह पाया गया कि जब कुछ निश्चित धातुओं को पराबैंगनी प्रकाश द्वारा किरणित करते हैं तो कम वेग वाले ऋण-आवेशित कण उत्सर्जित होते हैं.(Ncert)
(defrule around9
(declare (salience 400))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-around_saMbanXI  ?id1 ?id2)
(id-root ?id2 time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around9   "  ?id "  lagaBaga )" crlf))
)

;@@@ Added by 14anu-ban-02(04-02-2015)
;The arrangement of iron filings around the wire.[NCERT 12_04]
;लौह चूर्ण कणों का तार के चारों ओर अभिविन्यास.[NCERT]
(defrule around10
(declare (salience 1000))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-around_saMbanXI  ?id1 ?id2)
(id-root ?id2 wire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_cAroM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around10   "  ?id "  ke_cAroM_ora )" crlf))
)

;@@@ Added by 14anu-ban-02(17-02-2015)
;Earth satellites are objects which revolve around the earth.[ncert 11_08]
;भू उपग्रह वह पिंड है जो पृथ्वी के परितः परिक्रमण करते हैं.[ncert]
(defrule around11
(declare (salience 4600))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id1 ?id)
(id-root ?id1 revolve|rotate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariwaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around11   "  ?id "  pariwaH)" crlf))
)


;@@@ Added by 14anu-ban-02 (11-02-2016)
;Humans have always been curious about the world around them.[NCERT 11_01]
;मानव की सदैव अपने चारों ओर फैले विश्व के बारे में जानने की जिज्ञासा रही है.[NCERT 11_01]
(defrule around12
(declare (salience 1000))
(id-root ?id around)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-around_saMbanXI  ?id1 ?id2)
(id-root ?id1 world)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_cAroM_ora_ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  around.clp    around12   "  ?id "  ke_cAroM_ora_ke )" crlf))
)
