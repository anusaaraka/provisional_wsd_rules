;@@@ Added by 14anu-ban-01 on (15-10-2014)
;For measurement of small masses of atomic / sub-atomic particles etc., we make use of mass spectrograph in which radius of the trajectory is proportional to the mass of a charged particle moving in uniform electric and magnetic field.[NCERT corpus]
;अति सूक्ष्म कणों, जैसे परमाणुओं, अवपरमाणुक कणों आदि के लघु द्रव्यमानों के मापन के लिए हम द्रव्यमान-स्पेक्ट्रमलेखी का प्रयोग करते हैं, जिसमें, एकसमान विद्युत एवं चुम्बकीय क्षेत्र में गतिमान, आवेशित कणों के प्रक्षेप-पथ की त्रिज्या उस कण के द्रव्यमान के अनुक्रमानुपाती होती है.[NCERT corpus]
(defrule small3
(declare (salience 5000))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 mass)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laGu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp 	small3  "  ?id " laGu )" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;The key property of fluids is that they offer very little resistance to shear stress; their shape changes by application of very small shear stress.[Ncert Corpus]
;तरलों का मूल गुण यह है कि वह विरूपण प्रतिबल का बहुत ही न्यून प्रतिरोध करते हैं ; फलतः थोडे से विरूपण प्रतिबल लगाने से भी उनकी आकृति बदल जाती है.[Ncert Corpus]
;Small fire that threw out a lot of heat.(oald) on (25-02-2015)
;थोड़ी सी आग जिसने  बहुत सारी ऊष्मा उत्पन्न की . (manual)
(defrule small4
(declare (salience 5000))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 stress|shear|fire) 	;added 'fire' to the list by 14anu-ban-01 on (25-02-2015)
(id-cat ?id adjective|adjective_comparative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA_sA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp 	small4 "  ?id " WodA_sA )" crlf))
)


;$$$ Modified by Shirisha Manju - suggested by sukhada (05-05-2015) --- Removed 'amount' from list
;$$$ Modified by 14anu-ban-11 on (21-11-2014)---- added 'amount' in list.
;There is some residual benefit of fertilizers as the crops take up a small amount of the nutrients two and three years later.(agriculture) 
;उत्पादक के कुछ अवशेष लाभ जैसे कि फसलों के पोषक तत्वों की थोडी मात्रा दो और तीन साल बाद थी.(manual)
;@@@ Added by 14anu-ban-01 on (21-10-2014)
;When a force is applied on body, it is deformed to a small or large extent depending upon the nature of the material of the body and the magnitude of the deforming force.[Ncert Corpus]
;jaba kisI piMda para eka bala lagAyA jAwA hE wo usameM WodA yA aXika virUpaNa ho jAwA hE jo piMda ke xravya kI prakqwi waWA virUpaka bala ke mAna para nirBara karawA hE.[Ncert Corpus]
(defrule small5
(declare (salience 5000))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 extent) 
(id-cat ?id adjective|adjective_comparative)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " small.clp 	small5 "  ?id "    " ?id1 " WodA  )" crlf))
)

;@@@ Added by 14anu-ban-11 on (29-11-2014)
;Thus, if the intensity of matter wave is large in a certain region, there is a greater probability of the particle being found there than where the intensity is small.(Ncert) 
;अतः किसी विशिष्ट क्षेत्र में यदि द्रव्य-तरङ्ग की तीव्रता अधिक है, तब उसकी तुलना में जहाँ तीव्रता कम है, कण के पाए जाने की प्रायिकता अधिक होगी.(Ncert)
(defrule small6
(declare (salience 5100))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 intensity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp 	small6 "  ?id " kama )" crlf))
)

;@@@ Added by Shirisha Manju - suggested by sukhada (05-05-2015) 
;There is some residual benefit of fertilizers as the crops take up a small amount of the nutrients two and three years later.(agriculture) 
;उत्पादक के कुछ अवशेष लाभ जैसे कि फसलों के पोषक तत्वों की थोडी मात्रा दो और तीन साल बाद थी.
(defrule small7
(declare (salience 5000))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 amount) 
(id-cat ?id adjective|adjective_comparative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " small.clp        small7 "  ?id " WodA  )" crlf))
)

;------------------------ Default Rules ----------------------
;Commented by Shirisha Manju 05-05-2015 -- same rule exist
;(defrule small0
;(declare (salience 5000))
;(id-root ?id small)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat ?id adjective|adjective_comparative|adjective_superlative)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id CotA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp    small0   "  ?id "  CotA )" crlf))
;)
;;modified CotA- CotA by manju

(defrule small1
(declare (salience 4900))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp    small1   "  ?id "  CotA )" crlf))
)

(defrule small2
(declare (salience 4800))
(id-root ?id small)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  small.clp    small2   "  ?id "  CotA )" crlf))
)

;"small","Adj","1.CotA"
;The hat is too small for me.
;--"2.nagaNya"   
;He is a small man in eyes of his boss.
;
;

