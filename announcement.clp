;@@@Added by 14anu-ban-02(08-02-2016)
;I saw their wedding announcement in the newspaper.[mw]
;मैंने समाचारपत्र में उनका विवाहोत्सव विज्ञापन देखा .[self] 
(defrule announcement1
(declare (salience 100))	
(id-root ?id announcement)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?)
(id-root ?id1 see)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vijFApana))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  announcement.clp 	announcement1   "  ?id "  vijFApana )" crlf))
)
;-----------------------Default_rule------------------------------
;@@@Added by 14anu-ban-02(08-02-2016)
;I have an important announcement to make.[oald]
;मुझे एक महत्वपूर्ण घोषणा करनी है[self]
(defrule announcement0
(declare (salience 0))	
(id-root ?id announcement)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GoRaNA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  announcement.clp 	announcement0   "  ?id "  GoRaNA )" crlf))
)
