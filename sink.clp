;$$$ Modified by 14anu02 on 25.06.14
;@@@ Added by jagriti(4.2.2014)
;A part of the road is sinking.[rajpal]
;सडक का एक भाग धंस रहा है . 
(defrule sink0
(declare (salience 5000))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(viSeRya-of_saMbanXI ?id1 ?id2)
(id-root ?id2 road|building|ground)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xazsa))	;meaning changed from 'XaMsa' to 'Xazsa' by 14anu02 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink0   "  ?id "  Xazsa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (03-01-2015)
;@@@ Added by jagriti(4.2.2014)
;His voice sank into a whisper.[oald]
;उसकी आवाज मंद पडकर फुसफुसाहट में बदल गयी .
;His voice sank to a whisper.[oald];By 14anu-ban-01:This is proper oald sentence
;उसकी आवाज फुसफुसाहट में बदल गयी .[self]
(defrule sink1
(declare (salience 4900))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 voice)
(kriyA-to_saMbanXI ?id ?);added by 14anu-ban-01 on (03-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_jA));changed "maMxa_padZa" to "baxala_jA" by 14anu-ban-01 on (03-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink1   "  ?id " baxala_jA )" crlf));changed "maMxa_padZa" to "baxala_jA" by 14anu-ban-01 on (03-01-2015)
)

;$$$ Modified by 14anu-ban-01 on (03-01-2015):Removed 'knee' from the list
;@@@ Added by jagriti(4.2.2014)
;The wounded soldier sank (= fell) to the ground.[cambridge dict]
;घायल हुआ सैनिक जमीन पर गिरा . 
;$$$ Added knee,earth,land in the list by 14anu21 on 12.06.2014
(defrule sink3
(declare (salience 4700))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1) 
(id-root ?id1 floor|ground|earth|land);removed 'knee' by 14anu-ban-01 on (03-01-2015)
(not(kriyA-object ?id ?)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink3   "  ?id "  gira )" crlf))
)

;@@@ Added by jagriti(4.2.2014)
;I sank into an armchair.[oald]
;मैं एक आरामकुर्सी में बैठा . 
(defrule sink4
(declare (salience 4600))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1) 
(id-root ?id1 armchair|chair|seat)
(not(kriyA-object ?id ?)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bETa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink4   "  ?id "  bETa )" crlf))
)

;@@@ Added by jagriti(4.2.2014)
;To sink a well/shaft/mine.[oald]
;They sank a well in the field.[rajpal]
;उन्होंने क्षेत्र में कुँआ खोदा . 
(defrule sink5
(declare (salience 4500))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1) 
(id-root ?id1 well|shaft|mine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink5   "  ?id "  Koxa )" crlf))
)

;@@@ Added by jagriti(4.2.2014)
;The water in the river is sinking.[rajpal]
;पानी नदी में घट रहा है . 
;The price of dollar is sinking.[rajpal]
;डॉलर का मूल्य घट रहा है . 
(defrule sink6
(declare (salience 4400))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1) 
(id-root ?id1 price|water)
(not(kriyA-object ?id ?)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink6   "  ?id "  Gata )" crlf))
)

;$$$ Modified by 14anu02 on 25.06.14
;@@@ Added by jagriti(4.2.2014)
;He sank his teeth into the apple.[rajpal]
;उसने सेब के अंदर अपने दाँत धँसा दिए. 
(defrule sink7
(declare (salience 4300))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?) 
(kriyA-object ?id ?id2)
(id-root ?id2 tooth)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XazsA_xe))	;meaning changed from 'XaMsa' to 'Xazsa' by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink7   "  ?id "  XazsA_xe )" crlf))
)

;"sunken","Adj","1.jalamagna"
;The squa divers had discovered a sunker ship on Friday.
;
(defrule sink9
(declare (salience 4000))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id sunken )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jalamagna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sink.clp  	sink9  "  ?id "  jalamagna )" crlf))
)

;@@@ Added by 14anu21 on 13.06.2014 
;He sank three bottles of milk. 
;उसने तीन बोतल दूध पीया.
(defrule sink12
(declare (salience 500));salience increased from 100 to 500 by 14anu-ban-01 on (03-01-2015)
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?)
(kriyA-object ?id ?id1)
(id-root ?id1 glass|cup|bottle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink12   "  ?id "  pI )" crlf))
)

;Commented by 14anu-ban-01 on (03-01-2015) because correct meaning is coming from default rule sink10
;@@@ Added by 14anu21 on 12.06.2014 
;Bomb sank all four ships.
;बम से सभी चार जहाज़ डूबे.
;$$$ Modified by 14anu21 on 13.06.2014 by adding : "not(id-root ?id1 milk|water|beer|alochol))"
;### He sank three bottles of milk.
;### वह दूध तीन बोतलें डूबीं . (Before adding "not(id-root ?id1 milk|water|beer|alochol))" )
;### वह तीन बोतल दूध पी गया .
(defrule sink13
(declare (salience 100))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?)
(kriyA-object ?id ?id1)
(not(id-root ?id1 glass|cup|bottle|milk|water|beer|alochol))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dUba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink13   "  ?id "  dUba )" crlf))
)

;.....Default Rule....
(defrule sink10
(declare (salience 100))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dUba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink10   "  ?id "  dUba )" crlf))
)

(defrule sink11
(declare (salience 100))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hOxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink11   "  ?id "  hOxI )" crlf))
)

;"sink","N","1.hOxI"
;Utensils are washed in the kitchen sink.
;
;

;@@@ Added by 14anu-ban-11 on (03-12-2014)
;The wheels started to sink into the mud.(oald)
;पहियों ने कीचड मे धँसना शुरु किया .(manual) 
(defrule sink14
(declare (salience 500))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 mud)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xazsa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink14   "  ?id "  Xazsa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (03-12-2014)
;They sank three pints each in 10 minutes.(oald) 
;उन्होंने  हर 10 मिनटों में तीन डेढ पाव  गटक लिए .(manual)
(defrule sink15
(declare (salience 510))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 pint)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gataka_le))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink15   "  ?id "  gataka_le )" crlf))
)



;@@@ Added by 14anu-ban-11 on (03-12-2014)
;The pound has sunk to its lowest recorded level against the dollar.(oald)
;डौलर के बजाये पाउन्ड अपने निम्न रजिस्टर मेँ दर्ज किया हुआ स्तर से गिर गया है . (manual)
(defrule sink16
(declare (salience 520))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 pound)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sink.clp 	sink16   "  ?id "  gira_jA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (03-01-2015)
;The old man had sunk to his knees.[oald]
;वह वृद्ध पुरुष उकडू बैठ गया था.[self]
(defrule sink17
(declare (salience 500))
(id-root ?id sink)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 knee)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) ?id1 ukadU_bETa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sink.clp 	sink17   "  ?id "  " (+ ?id 1) "  " (+ ?id 2) "  "?id1" ukadU_bETa_jA)" crlf))
)

