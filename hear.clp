;@@@ Added by 14anu-ban-06 (10-03-2015)
;I was so sorry to hear of your father’s death.(OALD)[parser no-17]
;मुझे आपके पिता की मृत्यु के बारे में सुनकर बहुत खेद है . (manual)
(defrule hear2
(declare (salience 5300))
(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 of)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_bAre_meM_suna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hear.clp	hear2  "  ?id "  " ?id1 "  ke_bAre_meM_suna  )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;It was good to hear from him again. (OALD)[parser no.- 65]
;फिर से उस से सुनकर अच्छा लगा था . (manual)
(defrule hear3
(declare (salience 5300))
(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 from)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 se_suna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hear.clp	hear3  "  ?id "  " ?id1 "  se_suna  )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;I know you’re furious with me, but please hear me out.(OALD)[parser no.- 8]
;मैं जानता हूँ,आप मेरे साथ क्रोधित हैं परन्तु कृपया मुझे सुन ले . (manual)
(defrule hear4
(declare (salience 5300))
(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 suna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hear.clp	hear4  "  ?id "  " ?id1 "  suna_le  )" crlf))
)
;---------------------- Default Rules -------------------

(defrule hear0
(declare (salience 5000))
(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id hearing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sunavAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hear.clp  	hear0   "  ?id "  sunavAI )" crlf))
)

;given_word=hearing && word_category=noun	$SravaNa_Sakwi)

;"hearing","N","1.SravaNa_Sakwi"
;bama XamAke se usa bacce kI"hearing"(SravaNa Sakwi)calI gaI.
;--"2.sunavAI"
;He has gone to the court for the hearing of his case.






;Modified by Meena
;I heard a yelp from the hostel last night .
(defrule hear1
(declare (salience 4900))
(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suna))
;(assert (kriyA_id-object_viBakwi ?id ko))   ;commented by Meena(3.02.10)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hear.clp 	hear1   "  ?id "  suna )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hear.clp      hear1   "  ?id " ko )" crlf)
)
)

;default_sense && category=verb	sunAI xe	0
;"hear","V","1.sunAI xenA"
;bahare manuRya ko"hear"nahIM xewA,(sunAI nahIM padZawA)
;
;
