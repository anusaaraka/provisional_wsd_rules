;$$$  Modified the meaning by Preeti(7-12-13)
;The project wasted a considerable amount of time and money. 
;pariyojanA ne samaya Ora pEse kI eka mahawvapUrNa mAwrA vyarWa_meM_barabAxa kI.
(defrule considerable0
(declare (salience 5000))
(id-root ?id considerable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  considerable.clp 	considerable0   "  ?id "  kAPI )" crlf))
)

;$$$ Modified by 14anu-ban-03 (16-10-2014)
;### COUNTER EXAMPLE ### Considerable progress has already been made in this direction (for example, in conversion of solar energy, geothermal energy, etc., into electricity), but much more is still to be accomplished. [NCERT]
;इस दिशा में पहले से ही काफी प्रगति हो चुकी है (उदाहरण के लिए सौर ऊर्जा, भू - तापीय ऊर्जा आदि के विद्युत ऊर्जा में रूपान्तरण के रूप में) परन्तु इसे और अधिक सम्पादित किया जाना अभी शेष है. [NCERT]
;@@@ Added by Preeti(7-12-13)
;The origins of human language remain a matter of considerable debate. [by mail]
;mAnavIya BARA kA mUla mahawvapUrNa bahasa kA viRaya rahawA hEM.
(defrule considerable1
(declare (salience 5050))
(id-root ?id considerable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 debate) ;added by 14anu-ban-03 (16-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawvapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  considerable.clp 	considerable1   "  ?id "  mahawvapUrNa )" crlf))
)

;@@@ Added by 14anu-ban-03(5-9-2014)
;While the chair is rotating with considerable angular speed stretch your arms horizontally. [NCERT-CORPUS]
;jabaki kursI paryApwa koNIya cAla se GUma rahI ho apanI BujAoM ko kREwija xiSA meM PElAiye.
(defrule considerable2
(declare (salience 5050))
(id-root ?id considerable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 speed)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paryApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  considerable.clp 	considerable2   "  ?id "  paryApwa )" crlf))
)


;(defrule considerable1
;(declare (salience 4900))
;(id-root ?id considerable)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id bahuwa_aXika))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ;considerable.clp 	considerable1   "  ?id "  bahuwa_aXika )" crlf))
;)

;"considerable","Adj","1.bahuwa_aXika"
;A considerable quantity of manpower is required to do this work.
;
;
