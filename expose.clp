;$$$ Modified by 14anu-ban-04 (10-01-2015)          ----changed meaning from 'rAja_KolanA' to 'rAja_Kola'
;$$$ Modifed by 14anu19
;no need of rule expose1,2,3,4,5.these can be combined into one.
(defrule expose0
(declare (salience 5000))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)        ;interchange 'id' and 'id1'  by 14anu-ban-04 (10-01-2015)
(id-word ?id1 him|her|me|us|them|you)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAja_Kola))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose0   "  ?id "  rAja_Kola )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose0   "  ?id " kA )" crlf)
)
)

;commented by 14anu19
;(defrule expose1
;(declare (salience 4900))
;(id-root ?id expose)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 her)
;(kriyA-object ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id rAja_KolanA))
;(assert (kriyA_id-object_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose1   "  ?id "  rAja_KolanA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose1   "  ?id "  kA )" crlf)
;)
;)

;commented by 14anu19
;(defrule expose2
;(declare (salience 4800))
;(id-root ?id expose)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 them)
;(kriyA-object ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id rAja_KolanA))
;(assert (kriyA_id-object_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose2   "  ?id "  rAja_KolanA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose2   "  ?id " kA )" crlf)
;)
;)

;commented by 14anu19
;(defrule expose3
;(declare (salience 4700))
;(id-root ?id expose)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 me)
;(kriyA-object ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id rAja_KolanA))
;(assert (kriyA_id-object_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose3   "  ?id "  rAja_KolanA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose3   "  ?id " kA )" crlf)
;)
;)

;commented by 14anu19
;(defrule expose4
;(declare (salience 4600))
;(id-root ?id expose)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 us)
;(kriyA-object ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id rAja_KolanA))
;(assert (kriyA_id-object_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose4   "  ?id "  rAja_KolanA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose4   "  ?id " kA )" crlf)
;)
;)

;commented by 14anu19
;(defrule expose5
;(declare (salience 4500))
;(id-root ?id expose)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 you)
;(kriyA-object ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id rAja_KolanA))
;(assert (kriyA_id-object_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose5   "  ?id "  rAja_KolanA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expose.clp    expose5   "  ?id " kA )" crlf)
;)
;)

;@@@ added by Pramila(BU) on 12-12-2013
;$$$ Modified By 14anu17
;If the plants were directly exposed to the hot summer sun, the leaf temperature would exceed 40 degrees and the leaves would burn and turn black.
;यदि पौधों को सीधे ही गर्मी में सूर्य के सामने खुला छोड़ दिया जाए तो पत्तियों पर तापमान बढ़कर 40° सेंटीग्रेड हो जाएगा और पत्तियां जल जाएगी तथा काली पड़ जाएंगी।
(defrule expose6
(declare (salience 4800))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 sAmane_KulA_CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " expose.clp	expose6  "  ?id "  " ?id1 "  sAmane_KulA_CodZa_xe  )" crlf))
);MOdified Rule Name by 14anu17


;$$$ Modified by 14anu17
(defrule expose7
(declare (salience 4000))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose7   "  ?id "  Kola )" crlf))
);MOdified Rule Name by 14anu17

;@@@ added by Pramila(BU) on 12-12-2013
;He published an expose of the graft.      ;sentence of this file
;उसने रिश्वत का पर्दाफाश करने वाला लेख प्रकाशित किया.
(defrule expose8
(declare (salience 4800))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parxAPASa_karane_wAlA_leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose8   "  ?id "  parxAPASa_karane_wAlA_leKa)" crlf))
)


;@@@ Added by Pramila(BU) on 12-12-2013
;Apply the suntan cream liberally to exposed areas every three hours and after swimming.       ;oald
;हर तीन घंटे में और तैराकी के बाद खुले हुए भागो पर धूप से बचाने वाली क्रीम धीरे-धीरे लगाइए. 
(defrule expose9
(declare (salience 4800))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kule_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose9   "  ?id "  Kule_ho )" crlf))
)
;default_sense && category=verb	Kola	0
;"expose","VT","1.KolanA"
;He exposed the rampant corruption in his department through his article.
;--"2.vivaraNa"
;He published an expose of the graft && corruption government offices.
;
;

;[NOTE THIS RULE IS NOT FIRIED UNTILL ROOT PROBLEM OF 'MEASLES' IS NOT SOLVED]   by 14anu-ban-04 (10-01-2015)
;$$$ Modified by 14anu-ban-04 (10-01-2015)        ---changed meaning from 'praxarSiwa_kara' to 'saMparka_meM_lA'
;@@@ Added By 14anu17
;Giving the vaccines separately would leave children exposed to measles.
;अलग-अलग टीकाएँ देना खसरा को  प्रदर्शित कर बच्चों को छोड जाएगा . 
;अलग से टीके देना  बच्चों को खसरा के संपर्क में  लाना होगा.             ;translation corrected by 14anu-ban-04 (10-01-2014)
(defrule expose10
(declare (salience 4801))
(id-root ?id expose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)           ;changed relation from 'viSeRya-viSeRaNa ' to 'kriyA-to_saMbanXI '
(id-root ?id1 measles)                  ;added by 14anu-ban-04 (10-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMparka_meM_lA))
(assert (make_verbal_noun ?id)) ;added by 14anu-ban-04 (10-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  expose.clp      expose10   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expose.clp 	expose10   "  ?id "  saMparka_meM_lA )" crlf))
)
