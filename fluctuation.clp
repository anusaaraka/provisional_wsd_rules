;@@@ Added by 14anu-ban-05 on (10-01-2015)
;There is wide fluctuation in the prices of commodities in times of disturbances.[Hinkhoj]
;अशांति के समय में वस्तुओं की कीमतों में व्यापक अस्थिरता आती है [SELF]
(defrule fluctuation0
(declare (salience 100))
(id-root ?id fluctuation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asWirawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fluctuation.clp  	fluctuation0   "  ?id "  asWirawA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-01-2015)
;The fluctuations in the output of cotton have an important bearing on availability and price .[KARAN SINGLA]
;कपास के उत़्पादन में उतार - चढ़ाव का उपलब़्धि तथा कीमतों पर महत़्वपूर्ण असर होता है .[KARAN SINGLA]
(defrule fluctuation1
(declare (salience 101))
(id-root ?id fluctuation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 output|production|pressure)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwAra-caDZAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fluctuation.clp  	fluctuation1   "  ?id "  uwAra-caDZAva )" crlf))
)


