;########################################################################

;#  Copyright (C) 2014-2015 14anu04 Archit Bansal(archit.bansal18@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`
;@@@ Added by 14anu04 on 18-June-2014
;The second leg of the match will be played today.
;मैच का दूसरा चरण आज खेला जाएगा. 
(defrule leg0
(declare (salience 1000))
(id-root ?id leg)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 match|race|fixture|series|game|trip|journey)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  leg.clp     leg0   "  ?id "  caraNa )" crlf))
)

;$$$Modified by 14anu-ban-08 (16-01-2015)    ;spelling corrected
;@@@ Added by 14anu04 on 18-June-2014
;His leg was broken. 
;उसकी टाँग टूट गयी थी. 
(defrule leg1
(declare (salience 800))
(id-root ?id leg)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)           ;spelling corrected from 'course' to 'coarse' by 14anu-ban-08 (16-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAzga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  leg.clp     leg1   "  ?id "  tAzga )" crlf))
)

;$$$Modified by 14anu-ban-08 (16-01-2015)    ;added id-root, commented constraint
;@@@ Added by 14anu04 on 18-June-2014
; The police legged down the mob.
;पुलीस ने टोली को खदेडा. 
(defrule leg2
(declare (salience 800))
(id-root ?id leg)             ;added by 14anu-ban-08 (16-01-2015)
;(id-word ?id legged)         ;commented by 14anu-ban-08 (16-01-2015)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id KaxedA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  leg.clp     leg2   "  ?id1 "  " ?id "  KaxedA  )" crlf))
)

;@@@Added by 14anu-ban-08 (09-03-2015)
;The final leg of the trip was by donkey.  [oald]
;आखरी मंजिल भ्रमण करने की डोनकी के निकट थी.  [self]
(defrule leg3
(declare (salience 1300))
(id-root ?id leg)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(viSeRya-viSeRaNa ?id ?id1)          
(id-root ?id1 final)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  maMjila)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  leg.clp     leg3   "  ?id "   maMjila )" crlf))
)


