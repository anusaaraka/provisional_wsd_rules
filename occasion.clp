
;@@@ Added by Pramila(BU) on 14-02-2014
;He had no occasion to buy a scooter.          ;shiksharthi
;उसके पास स्कूटर खरीदने का कारण है.
;This is one of the occasions of his success.            ;shiksharthi
;यह उसकी सफलता के कारणों में से एक है.
(defrule occasion0
(declare (salience 5000))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(saMjFA-to_kqxanwa  ?id ?id1)(viSeRya-of_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion0   "  ?id "  kAraNa )" crlf))
)

;@@@ Added by Pramila(BU) on 14-02-2014
;It should have been an occasion for rejoicing, but she could not feel any real joy.    ;oald
;यह आनन्द लेने के लिए एक मौका होना चाहिए था, लेकिन वह कोई वास्तविक आनन्द नही ले सकी.
(defrule occasion1
(declare (salience 5000))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion1   "  ?id "  mOkA)" crlf))
)

;@@@ Added by Pramila(BU) on 14-02-2014
;I'll speak to him about it if the occasion arises    ;oald
;यदि मौका मिलता है तो मैं उसे इसके बारे में बताऊँगा .
;The bride took/used the occasion to make a short speech.  ;cald
;दुल्हन ने एक संक्षिप्त भाषण देने के लिए मौके का प्रयोग किया.
(defrule occasion2
(declare (salience 5000))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-subject  ?kri ?id)(kriyA-object  ?kri ?id))
(id-root ?kri arise|take|use)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion2   "  ?id "  mOkA)" crlf))
)


;@@@ Added by Pramila(BU) on 14-02-2014
;I have met him on several occasions.        ;oald
;मैं उससे कई बार मिला हूँ.
(defrule occasion3
(declare (salience 4900))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?kri ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root-category-suffix-number ?id ? noun s p)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  occasion.clp 	occasion3   "  ?id "  bAra )" crlf))
)

;@@@ Added by Pramila(BU) on 14-02-2014
;I have a suit but I only wear it on special occasions.   ;cald
;मेरे पास एक सूट है परन्तु मैं इसे केवल विशेष अवसर पर पहनती हूँ.
;this rule is added because conditions of sentence 'I have met him on several occasions.' with this sentence.
(defrule occasion4
(declare (salience 5000))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?kri ?id)
(id-root ?kri wear|sing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion4   "  ?id "  avasara )" crlf))
)

;$$$ Modified by 14anu-ban-03 (28-7-2014)
;Salana fair is held on the occasion of Shivaratri in Dalgando. [tourism corpus]
;xalagando me SivarAwrI ke avasara para sAlAnA melA lagawA hEM.
(defrule occasion5
(declare (salience 5050))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1);added by 14anu-ban-03
(id-root ?id1 Holi|Diwali|Poornima|Shivratri|Birthday|Mahashivratri|goddess) ;added by 14anu-ban-03
;(id-cat_coarse ?id1 noun|propN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion5   "  ?id "  avasara )" crlf))
)

;"occasion","N","1.avasara/prasaMga"
;On rare occassions, she dressed up.
;--"2.mOkA"
;I don't get much occasion to speak German.
;
(defrule occasion6
(declare (salience 4900))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimiwwamAwra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion6   "  ?id "  nimiwwamAwra_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on 23-8-14
;The use of seminary canopy would have been for musical performances in occasions of the goddess . [Parallel Corpus]
;सभामंडप  का  उपयोग  देवी  के  उपलक्ष्य  में  नृत्यगीतादि  सभाओं  के  लिए  होता  होगा  । [Manual]
;सभामंडप  का  उपयोग  देवी  के  उपलक्ष  में  नृत्यगीतादि  सभाओं  के  लिए  होता  होगा  । [Own Manual]
;The symbol of success of Akbar's reign, Buland Darwaza, is a scenic site which the king had made on the occasion of the victory over Gujarat .[Parallel Corpus]
;बादशाह  अकबर  के  शासनकाल  की  बुलंदियों  का  प्रतीक  बुलंद  दरवाजा  एक  दर्शनीय  स्थल  है  ,  जिसका  निर्माण  बादशाह  ने  गुजरात  विजय  के  उपलक्ष  में  करवाया  था  ।

(defrule occasion7
(declare (salience 5000))
(id-root ?id occasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  occasion.clp 	occasion7   "  ?id "  upalakRa )" crlf))
)


;"occasion","V","1.nimiwwamAwra_honA"
;The child's remarks occasioned lots of mirth. 
;
