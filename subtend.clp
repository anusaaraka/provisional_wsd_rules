;----------------------------------------DEFAULT RULE----------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (14-09-14)
;An ability that used to subtend ideological claims to documentary authenticity.[COCA]
;एक क्षमता/ योग्यता  जो कि वैचारिक  अधिकारों  को दस्तावेज़ी प्रमाणिकता  में कक्षान्तरित करती थी .[self]
(defrule subtend0 
(declare (salience -1)) 
(id-root ?id subtend) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kakRAnwariwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  subtend.clp  subtend0  "  ?id "  kakRAnwariwa_kara )" crlf)) 
) 
;----------------------------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-01 on (14-09-14)
;Sentence: If d is the diameter of the planet and α the angular size of the planet (the angle subtended by d at the earth), we have α = d/D (2.2) <the> angle can be measured from the same location on the earth.[NCERT corpus:changed 'The' to 'the']
;Translation: यदि d ग्रह का व्यास और α उसका कोणीय आमाप (d द्वारा पृथ्वी के किसी बिन्दु पर अन्तरित कोण) हो, तो α = d/D (2.2) कोण को, पृथ्वी की उसी अवस्थिति से मापा जा सकता है.[NCERT corpus]
(defrule subtend1 
(declare (salience 3000)) 
(id-root ?id subtend) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 angle)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anwariwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  subtend.clp  subtend1  "  ?id "  anwariwa)" crlf)) 
) 



