;@@@ Added by Anita--14-06-2014
;A church service was held in remembrance of the victims. [cambridge dictionary]
;चर्च-सेवा पीड़ितों के स्मरण में रखी गई थी ।
;A service was held in remembrance of local soldiers killed in the war. [oxford learner's dictionary]
;सेवा युद्ध में मारे गए स्थानीय सैनिकों के स्मरण में रखी गई थी ।
(defrule remembrance0
(declare (salience 4900))
(id-root ?id remembrance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remembrance.clp 	remembrance0   "  ?id "  smaraNa )" crlf))
)

;@@@ Added by Anita--14-06-2014
;The cenotaph stands as a remembrance of those killed during the war. [oxford learner's dictionary]
;स्मारक युद्ध में जो मारे गए थे उनकी याद का प्रतीक है ।
;He smiled at the remembrance of their first kiss. [oxford learner's dictionary]
;वह अपने प्रथम चुंबन की याद में मुसकराया ।
(defrule remembrance1
(declare (salience 4800))
(id-root ?id remembrance)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-as_saMbanXI  ?kri ?id)(kriyA-at_saMbanXI  ?kri ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remembrance.clp 	remembrance1   "  ?id "  yAxa )" crlf))
)

;@@@ Added by Anita--16-06-2014
;The mere remembrance of God keeps the sufferings of human beings at bay. [news-dev]
;भगवान का स्मरण करने मात्र से ही मनुष्य कष्टों से मुक्त हो जाता है।
(defrule remembrance3
(declare (salience 5100))
(id-root ?id remembrance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remembrance.clp 	remembrance3   "  ?id "  smaraNa_kara )" crlf))
)
