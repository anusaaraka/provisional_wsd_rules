;@@@ Added by 14anu-ban-06 (24-04-2015)
;An inimical stare. (OALD)[parser no.- 2]
;अमित्रतापूर्ण निर्निमेष दृष्टि . (manual)
(defrule inimical1
(declare (salience 2000))
(id-root ?id inimical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 stare)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amiwrawApUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inimical.clp 	inimical1   "  ?id "  amiwrawApUrNa )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (24-04-2015)
; These policies are inimical to the interests of society. (OALD)
;ये नीतियाँ समाज के रूचि के लिए हानिकारक हैं . (manual)
(defrule inimical0
(declare (salience 0))
(id-root ?id inimical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAnikAraka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inimical.clp 	inimical0   "  ?id "  hAnikAraka )" crlf))
)
