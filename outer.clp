
(defrule outer0
(declare (salience 5000))
(id-root ?id outer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhya-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outer.clp 	outer0   "  ?id "  bAhya- )" crlf))
)

(defrule outer1
(declare (salience 4900))
(id-root ?id outer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outer.clp 	outer1   "  ?id "  bAhya )" crlf))
)

;"outer","Adj","1.bAhya"
;The outer layer of skin is made of cells && follicles.
;--"2.suxUra"
;He stays, the address shows, in outer Mangolia.
;--"3.kenxrAvasArI"
;There is some problem on the outer rim of the front wheel.
;
;

;Added by sheetal(2-01-10).
(defrule outer2
(declare (salience 4950))
(id-root ?id outer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) signal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAharI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outer.clp     outer2   "  ?id "  bAharI )" crlf))
)
;The train waited at the outer signal .


;Modified by 14anu-ban-09 on (15-12-2014)
;NOTE- Moved out7 from 'out.clp' to 'outer.clp' by 14anu-ban-09 on (15-12-2014)
;@@@ Added by 14anu6 on 13/6/2014***********
;The out surface of the ship was blue.
;The outer surface of the ship was blue.  ;modified sentence from "out surface" to "outer surface" by 14anu-ban-09 on (15-12-2014)
;जहाज की बाहरी सतह नीली थी . 
(defrule outer3
(declare (salience 4900))
(id-root ?id outer)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAharI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outer.clp 	outer3   "  ?id "  bAharI )" crlf))
)


