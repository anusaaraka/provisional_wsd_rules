;@@@ Added by 14anu-ban-06 (24-03-2015)
;There was a slight hiccup in the timetable. (OALD)
;समय सारणी में थोडी गडबडी थी . (manual)
(defrule hiccup1
(declare (salience 2000))
(id-root ?id hiccup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadabadI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hiccup.clp 	hiccup1   "  ?id "  gadabadI )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (24-03-2015)
;I ate too quickly and got hiccups.(OALD)
;मैंने ज्यादा ही फटाफट खाया और हिचकियाँ आई . (manual) 
(defrule hiccup0
(declare (salience 0))
(id-root ?id hiccup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hicakI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hiccup.clp 	hiccup0   "  ?id "  hicakI )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-03-2015)
;I can't stop hiccuping - does anyone know a good cure?(cambridge)[parser no.-4 ]
;मैं हिचकी आना बन्द नहीं कर सकता हूँ- कोई एक अच्छा इलाज जानता है? (manual)
(defrule hiccup2
(declare (salience 0))
(id-root ?id hiccup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hicakI_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hiccup.clp 	hiccup2   "  ?id "  hicakI_A )" crlf))
)

