;@@@ Added by 14anu-ban-05 on (15-01-2015)
;The mechanic galvanized the iron door of the car.	[hinkhoj]
;कारीगर ने कार को लोहे के दरवाजे से कलई किया.			[manual]
(defrule galvanize0
(declare (salience 100))
(id-root ?id galvanize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalaI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  galvanize.clp 	galvanize0   "  ?id "  kalaI_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (15-01-2015)
;The prospect of his mother coming to stay galvanized him.  [cambridge]
;उसकी माँ  के रहने के लिए आने की संभावना ने उसे उत्तेजित कर दिया.		    [manual]
(defrule galvanize1
(declare (salience 101))
(id-root ?id galvanize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 prospect|deadline|urgency)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejiwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  galvanize.clp 	galvanize1   "  ?id "  uwwejiwa_kara_xe )" crlf))
)	
