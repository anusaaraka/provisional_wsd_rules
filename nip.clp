
(defrule nip0
(declare (salience 5000))
(id-root ?id nip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikotI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nip.clp 	nip0   "  ?id "  cikotI )" crlf))
)

;"nip","N","1.cikotI"
;He felt a sharp pinch in his leg.
;
(defrule nip1
(declare (salience 4900))
(id-root ?id nip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikotI_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nip.clp 	nip1   "  ?id "  cikotI_kAta )" crlf))
)


;$$$ Modified by 14anu-ban-08 (09-12-2014)     ;Change meaning from 'kucala_dAlane' to 'kucala_dAla'
;@@@ added by 14anu11
;The established religious as well as the social organisations opposed him and even tried to nip Basava ' s revolution in the bud .
;प्रतिष्ठित सम्प्रदाय तथा सामाजिक संगठनों ने न केवल बसव का विरोध किया अपितु उसके आंदोलन को पनपने से पहले ही कुचल डालने की कोशिश की .
(defrule nip2
(declare (salience 5000))
(id-root ?id nip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(to-infinitive  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kucala_dAla))     ;Change meaning from 'kucala_dAlane' to 'kucala_dAla' by 14anu-ban-08 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nip.clp 	nip2   "  ?id "  kucala_dAla )" crlf))
)



;"nip","VT","1.cikotI_kAtanA"
;She nipped me on the arm.
;--"2.nukasAna_pahuzcAnA"
;The frost nipped the young shoots.
;--"3.jalxI_karanA"
;The car nipped in ahead of me.
;
