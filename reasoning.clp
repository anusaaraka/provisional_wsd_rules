;@@@ Added by 14anu-ban-10 on (14-10-2014)
;This is incorrect reasoning.  [ncert corpus]
;yaha viveka ke viparIwa hE.    [ncert corpus]
(defrule reasoning1
(declare (salience 200))
(id-root ?id reasoning)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1 )
(id-root ?id1 incorrect )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viveka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  reasoning.clp  	reasoning1  "  ?id "  viveka )" crlf))
)

;@@@Added by Manasa (15-02-2016)
;The scientific method involves several interconnected steps: Systematic observations, controlled experiments, qualitative and quantitative reasoning, mathematical modelling, prediction and verification or falsification of theories. ( NCERT )
(defrule reasoning2
(declare (salience 500))
(id-root ?id reasoning)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1 )
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vivecanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  reasoning.clp         reasoning2  "  ?id "  vivecanA )" crlf))
)

;----------------------------- Default rule ---------------------

;@@@ Added by 14anu-ban-10 on (14-10-2014)
;By the same reasoning, any one of them may be called action and the other reaction. [ncert corpus]
;isI safkewa ke AXAra para inameM se kisI BI eka ko kriyA waWA xUsare ko prawikriyA kahA jA sakawA hE. [ncert corpus]
(defrule reasoning0
(declare (salience 000))
(id-root ?id reasoning)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id safkewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  reasoning.clp  	reasoning0   "  ?id "  safkewa )" crlf))
)


