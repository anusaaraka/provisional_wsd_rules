;$$$ Modified by 14anu-ban-05 on (24-11-2014)
; added constraints music, dance
;@@@ Added by 14anu23 13/06/2014
;He is a popular figure in modern music.
;वह आधुनिक सङ्गीत में  प्रसिद्ध व्यक्ति  है . 
(defrule figure20
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id figure )
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 music|dance)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prasixXa_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  figure.clp  	figure20   "  ?id "  prasixXa_vyakwi )" crlf))
)

(defrule figure0
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id figured )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id alaMkqwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  figure.clp  	figure0   "  ?id "  alaMkqwa )" crlf))
)

;"figured","Adj","1.alaMkqwa"
;
;
;I can't figure out his rudeness.
;mEM usakI beruKZI ko samaJa nahIM pA rahA hUz
(defrule figure1
(declare (salience 4900))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " figure.clp	figure1  "  ?id "  " ?id1 "  samaJa  )" crlf))
)

(defrule figure2
(declare (salience 4800))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " figure.clp figure2 " ?id "  anumAna_kara )" crlf)) 
)

(defrule figure3
(declare (salience 4800));salience changed by 14anu07 4700 --> 4800
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anumAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " figure.clp	figure3  "  ?id "  " ?id1 "  anumAna_kara  )" crlf))
)

;@@@ Added by 14anu23 13/06/2014
;Figure of God.
;देवता की  मूर्ति  .  
(defrule figure21
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)  
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " figure.clp figure21 " ?id "  mUrwi )" crlf)) 
)


(defrule figure4
(declare (salience 4600))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samAXAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " figure.clp	figure4  "  ?id "  " ?id1 "  samAXAna_kara  )" crlf))
)

;I finally figured out why this program is so slow.
;Added by Sheetal(02-08-10)
(defrule figure5
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_lagA))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " figure.clp	figure5  "  ?id "  " ?id1 "  pawA_lagA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  figure.clp       figure5   "  ?id " ne )" crlf))
)

(defrule figure6
(declare (salience 4400))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAXAna_ho));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " figure.clp figure6 " ?id "  samAXAna_ho )" crlf)) 
)

(defrule figure7
(declare (salience 4300))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samAXAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " figure.clp	figure7  "  ?id "  " ?id1 "  samAXAna_ho  )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (12-03-2015)  ;added condition '(viSeRya-in_saMbanXI  ?id1 ?id)'
;The dots in this figure represent the data points and the solid curve is the theoretical prediction based on the assumption that the target atom has a small, dense, positively charged nucleus.[NCERT]
;isa ciwra meM xiKAe gae biMxu prayoga meM prApwa AzkadoM ko nirUpiwa karawe hEM Ora sanwawa vakra sExXAnwika pUrvAnumAna hE jo isa kalpanA para AXAriwa hE ki paramANu meM eka sUkRma saGana waWA XanAveSiwa nABika hE.[NCERT]

;@@@ Added by Pramila(Banasthali University) on 24-10-2013
;As seen from the figure, only the two faces 1 and 2 will contribute to the flux; electric field lines are parallel to
; the other [faces] and they, therefore, do not contribute to the total flux.             ;physics
;जैसा चित्र से दृष्टिगोचर होता है , केवल दो फलक1 तथा 2 ही फ्लक्स में योगदान देङ्गे; विद्युत क्षेत्र रेखाएँ अन्य फलकों के समान्तर हैं और वे इसीलिए कुल फ्लक्स में योगदान नहीं देतीं .
;The central figure in the painting is the artist's daughter.           ;oald
(defrule figure8
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-from_saMbanXI  ?id1 ?id)(viSeRya-in_saMbanXI  ?id ?id1)(viSeRya-in_saMbanXI  ?id1 ?id))     ;condition added by Pramila on 22-11-2013 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure8   "  ?id "  ciwra )" crlf))
)

;@@@ Added by Pramila(Banasthali University) on 24-10-2013
;His name figures in the list of suspects.              [old clp] 
;उसका नाम संदिग्ध लोगों की सूची में आता है.
(defrule figure9
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure9   "  ?id "  A )" crlf))
)

;@@@ Added by Pramila(Banasthali University) on 22-11-2013
;Figures for April show a slight improvement on previous months.  ;oald
;By 2009, this figure had risen to 14 million.             ;oald
(defrule figure10
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-for_saMbanXI  ?id ?id1)(and(kriyA-subject  ?id1 ?id)(id-root ?id1 rise)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AzkadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure10   "  ?id "  AzkadZA )" crlf))
)

;@@@ Added by Pramila(Banasthali University) on 22-11-2013
;Are you any good at figures?               ;oald
(defrule figure11
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka_gaNiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure11   "  ?id "  aMka_gaNiwa )" crlf))
)

;$$$Modified by 14anu-ban-05 on (25-11-2014)
;decreased salience and added central 
;He was undoubtedly the central figure in the seminars which took place in Basava's house .[KARAN SINGLA]
;निस़्संदेह बसव के घर होने वाली चर्चाओं में वह केऩ्द्रीय व़्यक़्ति था .[KARAN SINGLA]
;@@@ Added by Pramila(Banasthali University) on 22-11-2013
;A political figure.      ;oald
;A figure of authority.     ;oald
;One of the most popular figures in athletics.   ;oald
(defrule figure12
(declare (salience 4950))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa  ?id ?id1)(viSeRya-of_saMbanXI  ?id ?id1))
(id-root ?id1 political|authority|popular|central)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure12   "  ?id "  vyakwi )" crlf))
)


;@@@ Added by Pramila(Banasthali University) on 22-11-2013
;She's always had a good figure.                ;oald 
(defrule figure13
(declare (salience 4500))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dOla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure13   "  ?id "  dOla )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Somaji and Aditiji on 08-11-14
;Figure 12.2 shows a schematic diagram of this experiment. (ncert)
;ciwra 12.2 meM isa prayoga ke vyavasWiwa ciwra ko xarSAyA gayA hE .(ncert)
;ciwra 12.2 isa prayoga kA eka yojanA baxXa ciwra xiKAwA hE.(anu)
(defrule figure16
(declare (salience 4100))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse =(+ ?id 1) number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp   figure16   "  ?id "  ciwra )" crlf))
)

;@@@ Added by 14anu13  on 30-06-14
;If the figure is to represent Baladeva , the brother of Narayana , put earrings into his ears , and give him eyes of a drunken man .
;यदि आकृति बलदेव को दर्शाती हो जो नारायण के भाई है , तो उनके कान में बुंदे ( कर्णफूल ) पहनाओ और उनकी आंखें शराबी की - सी बनाओ .
(defrule figure22
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?  ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure22   "  ?id "  Akqwi )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-12-2014)
;These 'engineers built a chariot on which they placed a magnetic figure with arms outstretched.[NCERT]
;ina 'iMjIniyaroM' ne eka raWa banAyA jisa para unhoMne cumbaka kI banI huI eka prawimA lagAI, jisakA eka hAWa bAhara PElA huA WA.[NCERT]
(defrule figure23
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 magnetic)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawimA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure23   "  ?id "  prawimA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-12-2014)
;The figure swiveled around so that the finger of the statuette on it always pointed south.[NCERT]
;raWa para lagI huI prawimA isa waraha GUma jAwI WI ki usakI azgulI hameSA xakRiNa kI ora safkewa kare.[NCERT]
(defrule figure24
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 swivel)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawimA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure24   "  ?id "  prawimA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (17-01-2015)
;The dots stand for polarisation perpendicular to the plane of the figure.	[NCERT]
;biMxuoM ke xvArA ciwra ke wala ke lambavawa XruvaNa ko xarSAyA gayA hE.	[NCERT]
(defrule figure25
(declare (salience 5000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 plane)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure25   "  ?id "  ciwra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-02-2015)
;You'll need to dig deep into the records to find the figures you want. [OALD]
;जिन आँकड़ों को आप  प्राप्त करना चाहते हैं आपको उन्हें  अभिलेखों में अच्छी तरह से जाँचने की जरूरत है  .[manual]
(defrule figure26
(declare (salience 5001))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-kriyArWa_kriyA  ?id1 ?id2)
(kriyA-object  ?id2 ?id)
(id-root ?id1 dig)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMkadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure26   "  ?id "  AMkadA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (12-03-2015)
;His salary is now in six figures.[OALD]
;उनका वेतन अब छह  अंकों में  है.	[manual]

(defrule figure27
(declare (salience 5001))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure27   "  ?id "  aMka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (12-03-2015)
; Inflation is in double figures. [OALD]
;मुद्रास्फीति की दर दोहरे अंकों में है.	[manual]

(defrule figure28
(declare (salience 5002))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 single|double)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure28   "  ?id "  aMka )" crlf))
)

;-------------------------------default rules--------------------------------
;$$$Modified by 14anu-ban-05 on (25-11-2014)
;changed default meaning from 'aMkadA' to 'ciwra'
;The figure has been redrawn in Fig. 3.6 choosing different scales to facilitate the calculation.[NCERT]
;gaNanA kI AsAnI ke lie isa ciwra ko ciwra 3.6 meM alaga pEmAnA lekara punaH KIzcA gayA hE.[NCERT]
;The figure depicts one-half of a cycle.[NCERT]
;ciwra meM xolanoM ke AXe cakra ko xarSAyA gayA hE.[NCERT]
;@@@ Added by Pramila(Banasthali University) on 22-11-2013
;$$$ Modified by 14anu07 on 2/06/2014
;changed meaning from 'aMka' to 'AMkadA'
;The figure shot up to 65 per cent. 
;आङ्कडा 65 प्रतिशत तक बढा
(defrule figure14
(declare (salience 4000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure14   "  ?id "  ciwra )" crlf))
)

(defrule figure15
(declare (salience 4000))
(id-root ?id figure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  figure.clp 	figure15   "  ?id "  kalpanA_kara )" crlf))
)

;default_sense && category=verb	xiKAI_xe	0
;"figure","V","1.xiKAI_xenA"
;His name figures in the list of suspects.
;--"2.kalpanA_karanA"
;By the way they were talking he figured it out that they will not come.
;
;
