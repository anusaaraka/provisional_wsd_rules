

;@@@ Added by 14anu-ban-09 on (31-01-2015)
;In Hindu family father is the only provider.		[Hinkhoj]
;हिन्दू परिवार में पिता एकमात्र प्रबन्धक होते हैं .

(defrule provider0
(declare (salience 5500))	;salience increased '0000' to '5500' by 14anu-ban-09 on (02-02-2015)
(id-root ?id provider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)		;added by 14anu-ban-09 on (02-02-2015)
(id-root ?id1 father)					;added by 14anu-ban-09 on (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prabanXaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provider.clp 	provider0   "  ?id "  prabanXaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-01-2015)
;From the ages Uttarakhand has been the provider of spiritual shelter and peace for the Indians .	[Total_tourism]
;युग-युगांतर से उत्तराखंड भारतीयों के लिए आध्यात्मिक शरणस्थल और शांति प्रदाता रहा है .	[Total_tourism]

(defrule provider1
(declare (salience 5000))		
(id-root ?id provider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provider.clp 	provider1   "  ?id "  praxAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-02-2015)
;Its three dimensional shaktis are respectively goddess Khadga-Khappar Dharani Mother Kali , riches provider Mother Mahalakshmi and all knowledge provider Mother Mahasaraswati .	[Total_tourism]
;उसकी त्रिगुणात्मक शाक़्तियाँ क्रमश: देवी खड़्ग-खप्पर धारणी माँ काली , धनधान्य की अधिष़्ठात्री माँ महालक्ष्मी व अखिल ज्ञान-विज्ञान प्रदायिनी माँ महासरस्वती हैं .[Total_tourism]
;उसकी त्रिगुणात्मक शाक़्तियाँ क्रमश: देवी खड़्ग-खप्पर धारणी माँ काली , धनधान्य की प्रदायिनी माँ महालक्ष्मी व अखिल ज्ञान-विज्ञान प्रदायिनी माँ महासरस्वती हैं . [manual]

(defrule provider3
(declare (salience 5000))		
(id-root ?id provider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 Mahasaraswati|Mahalakshmi)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAyinI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provider.clp 	provider3   "  ?id "  praxAyinI )" crlf))
)

;------------------------DEFAULT RULE--------------------------------------------------------------


;@@@ Added by 14anu-ban-09 on (02-02-2015)
;Not only this , the most pious salvation provider river Mother Godavari , who salvaged Vishnu and Shiva from the curse of Vrinda and brahmahatya , also flows on the land of Nasik itself .	[Total_tourism]
;इतना ही नहीं विष़्णु व शिव को वृंदा व ब्रह्महत्या के शाप से मुक़्त करने वाली परम पावनी मुक़्तिदायनी नदी माँ गोदावरी भी नासिक की धरा पर ही बहती है .		[Total_tourism]

(defrule provider2
(declare (salience 0000))		
(id-root ?id provider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provider.clp 	provider2   "  ?id "  praxAwA )" crlf))
)
