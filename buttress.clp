;@@@Added by 14anu-ban-02(18-04-2015)
;The mother had always been the buttress of our family in trying times.[mw]
;माँ हमेशा कठिन समय में हमारे परिवार का सहारा रही थी . [self]
(defrule buttress1 
(declare (salience 100)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 family)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sahArA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress1  "  ?id "  sahArA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-04-2015)
;It was necessary to strengthen the building with large external buttresses. [oald]
;इमारत  को चौड़ी बाहरी टेक से मजबूत बनाना आवश्यक था .[self] 
(defrule buttress2 
(declare (salience 100)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-with_saMbanXI ?id1 ?id)
(id-root ?id1 strengthen)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id teka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress2  "  ?id "  teka )" crlf)) 
) 


;@@@Added by 14anu-ban-02(18-04-2015)
;The sharp increase in crime seems to buttress the argument for more police officers on the street. [oald]
;अपराध में तेज वृद्धि अधिक पुलिस अधिकारियों के  सड़क पर होने के तर्क को   पुश्ट करती हुई प्रतीत होती है . [self]
(defrule buttress4
(declare (salience 100)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object ?id ?id1)
(id-root ?id1 argument)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id puSta_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress4  "  ?id "  puSta_kara )" crlf)) 
) 


;@@@Added by 14anu-ban-02(18-04-2015)
;The theory has been buttressed by the results of the experiment. [mw]
;सिद्धान्त प्रयोग के परिणामों के द्वारा सिद्ध किया गया है . [self]
(defrule buttress5
(declare (salience 100)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject ?id ?id1)
(id-root ?id1 theory)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sixXa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress5  "  ?id "  sixXa_kara )" crlf)) 
)


;--------------------default_rules---------------------------


;@@@Added by 14anu-ban-02(18-04-2015)
;Sentence: After the wall collapsed, the construction company agreed to rebuild it with a buttress.[mw]
;Translation:दीवार के ढहने के  बाद ,निर्माण कम्पनी इसको मजबूती के साथ फिर बनाने के लिये  मान गई .   [self]
(defrule buttress0 
(declare (salience 0)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id majZabUwI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress0  "  ?id "  majZabUwI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-04-2015)
;Sentence: The galleries were well buttressed by huge timbers.[oald]
;Translation: बरामदा विशाल लकड़ियों के द्वारा अच्छी तरह से  मजबूत किया गया था .   [self]
(defrule buttress3 
(declare (salience 0)) 
(id-root ?id buttress) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id majZabUwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  buttress.clp  buttress3  "  ?id "  majZabUwa_kara )" crlf)) 
) 


