;@@@ Added by 14anu-ban-04 on (08-04-2015)
;In a total eclipse, the moon completely covers the disc of the sun.               [oald]
;खग्रास ग्रहण में, चन्द्रमा सूर्य के चक्र को  पूरी तरह से  ढक लेता है .                                       [self]
(defrule disc1
(declare (salience 20))
(id-root ?id disc)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?kri ?id)
(kriyA-subject ?kri ?id1)
(id-root ?id1 moon)
(id-root ?kri cover)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakra))        
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disc.clp 	disc1   "  ?id " cakra )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 on (08-04-2015)
;This recording is available on disc and on tape.                    [cald]
;यह ध्वन्यालेखन डिस्क पर और टेप पर उपलब्ध है .                                    [self]
(defrule disc0
(declare (salience 10))
(id-root ?id disc)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id diska))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disc.clp 	disc0   "  ?id " diska )" crlf))
)

