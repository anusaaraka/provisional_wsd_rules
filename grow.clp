(defrule grow0
(declare (salience 5000))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahuwa_badZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow0  "  ?id "  " ?id1 "  bahuwa_badZA_ho  )" crlf))
)

;He has grown into of that shirt.
;yaha kamIjZa usake pahanane ke lie bahuwa badZI hE 
;
;
(defrule grow1
(declare (salience 4900))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XIre-XIre_pasaMxa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow1  "  ?id "  " ?id1 "  XIre-XIre_pasaMxa_A  )" crlf))
)

;muJe yaha kamIjZa pasaMxa nahIM WI jaba wumane ise KarIxA WA,lekina aba XIre-XIre yaha muJe pasaMxa Ane lagI hE
;I didn't like this shirt when you bought it,but it's growing on me now.
;$$$ Modified by 14anu18 (Removed kriyA-upasarga condition)
;Lisa has grown out of her old shoes.
;लीसा अपने पुराने जूतों के लिए बहुत बड़ी हो गई है.
(defrule grow2
(declare (salience 4800))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) out)
(kriyA-of_saMbanXI ?id ?id2)
(kriyA-subject ?id ?)

=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) bahuwa_badZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow2  "  ?id "  " (+ ?id 1) "  bahuwa_badZA_ho  )" crlf))
)

;He has grown out of that shirt.
;yaha kamIjZa usake pahanane ke lie bahuwa badZI hE
;Added by human
(defrule grow3
(declare (salience 4700))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(id-cat_coarse =(+ ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow3   "  ?id "  ho )" crlf))
)

;adj_comp_-_nervous && category=verb	ho	0
;adj_comp_-_angry && category=verb	ho	0
(defrule grow4
(declare (salience 4600))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " grow.clp grow4 " ?id "  baDa )" crlf)) 
)

(defrule grow5
(declare (salience 4500))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow5  "  ?id "  " ?id1 "  baDa  )" crlf))
)

;$$$ Modified by Manasa ( 5-02-2016 ) -- added kriyA-object relation 
;$$$ Modified by 14anu-ban-05 on (09-01-2015)   ---------added 'or' and 'PropN' category in the relation 
;@@@ Added by 14anu26  [20-06-14]
;Rama/He grew mustard on his field.
;राम/उस ने उसकी जमीन मेँ सरसोँ उगाई.
(defrule grow7
(declare (salience 4500))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(kriyA-object ?id ?id2)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ugA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow7   "  ?id "  ugA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;They grow, collide, fracture and break apart.[NCERT]
;ye vikasiwa howe hEM, takarAwe hEM, tUtawe hEM waWA tukade-tukade hokara pqWaka ho jAwe hEM.[MANUAL]
(defrule grow8
(declare (salience 5000))
(Domain physics)
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 they)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id vikasiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow8   "  ?id "  vikasiwa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  grow.clp      grow8   "  ?id "  physics )" crlf))
)

;@@@Added by 14anu18
;My friend and I grew apart after school.
;मेरा मित्र और मैं विद्यालय के बाद अलग हो गए . 
(defrule grow1_1
(declare (salience 5000))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-word ?id1 apart)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 alaga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow1_1  "  ?id "  " ?id1 "  alaga_ho  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (02-02-2015)
;Concern is growing over the number of children excluded from school. [oald]
;स्कूल से बाहर किये गये बच्चों की संख्या पर चिंता बढ़ रही है . [manual]
(defrule grow9
(declare (salience 4500))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 over)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grow.clp	grow9  "  ?id "  " ?id1 "  baDa  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (02-03-2015)
;A formal garden is carefully designed and kept according to a plan, and it is not allowed to grow naturally.[cald]
;एक सुव्यवस्थित उद्यान सावधानी से बनाया गया है और रखा एक योजना के अनुसार रखा गया है, और इसे स्वाभाविक रूप से विकसित होने की अनुमति नहीं है.	[manual_suggested by Aditi ma'am]
(defrule grow10
(declare (salience 5001))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyArWa_kriyA  ?id1 ?id)	;commented by 14anu-ban-05 on (26-03-2015)
;(id-root ?id1 allow)			;commented by 14anu-ban-05 on (26-03-2015)
(kriyA-kriyA_viSeRaNa  ?id ?id1)   	;added by 14anu-ban-05 on (26-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow10   "  ?id "  vikasiwa_ho )" crlf))
)


;@@@ Added by 14anu-ban-05 on (26-03-2015)
;She felt a flam of anger flicker and grow. [OALD] 
;उसने क्रोध के आवेग को झलकता  हुआ और बढ़ता हुआ  महसूस किया. [manual]   ;run on parser 3 of multiparse
(defrule grow11
(declare (salience 5002))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 flame)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow11   "  ?id "  baDa )" crlf))
)



;--------------------- Default Rules ------------------

;$$$ modified by 14anu26  [21-06-14]
;Sugarcane is grown mainly in Uttar Pradesh.
;ईख  प्रमुख रूप से उत्तर प्रदेश में उपजाई जाती है .
(defrule grow6
(declare (salience 4400))
(id-root ?id grow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upajA )) ;meaning changed from baDZa to upajA 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grow.clp 	grow6   "  ?id "  upajA )" crlf))
)


;default_sense && category=verb	upajA	0
;"grow","V","1.upajAnA"
;Sugarcane is grown mainly in Uttar Pradesh.
;
;
