;$$$ Modified by 14anu-ban-09 on (08-04-2015)        ;NOTE-Modified 'kArbanic' to 'kArbanika'
;These substances are not allowed in organic production.  	[oald]
;इन पदार्थों ने कार्बनिक की पैदावार होने नहीं दी. 	[Manual] 

;Added by human beings:
(defrule organic0
(declare (salience 5001))	;increased salience '5000' to '5001' by 14anu-ban-09 on (08-04-2015)
(id-root ?id organic)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 compound|production)	;modified 'id-word' to 'id-root' and added 'production'
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArbanika))	;modified 'kArbanic' to 'kArbanika' by 14anu-ban-09 on (08-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organic.clp 	organic0   "  ?id "  kArbanika )" crlf))		;modified 'kArbanic' to 'kArbanika' by 14anu-ban-09 on (08-04-2015)
)

(defrule organic1
(declare (salience 4900))
(id-root ?id organic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMxriya_sambanXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organic.clp 	organic1   "  ?id "  inxriya_saMbaMXI )" crlf))
)




;@@@ Added by 14anu11
;That gave a certain organic unity to it .
;इस कारण इसमें एक तरह की सहज एकता थी .
(defrule organic2
(declare (salience 5000))
(id-root ?id organic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id shja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organic.clp 	organic2   "  ?id " shja )" crlf))
)


;"organic","Adj","1.inxriya_saMbaMXI"
;He treats organic diseases.
;--"2.kArbanika"
;Hydrocarbons are organic compounds.
;
;
