
;Added by Meena(25.7.11)
;At this point, the Dow was down about 35 points. 
(defrule point0
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-at_saMbanXI  ?id1 ?id)(kriyA-on_saMbanXI  ?id1 ?id)(kriyA-to_saMbanXI  ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  point.clp      point0   "  ?id "  samaya )" crlf))
)

;######[Counter Example]#########The ice point and the steam point of water are two convenient fixed points and are known as the freezing and boiling points.#############  [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;jala kA himAMka waWA BApa - biMxu xo suviXAjanaka niyawa biMxu hEM, jinheM himAMka waWA kvaWanAMka kahawe hEM. [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;$$$ Modified by Sonam Gupta MTech IT Banasthali 11-3-2014 (modified relation and silence)
;At this point, the Dow was down about 35 points. [verified sentence]
;इस समय पर, डोव ३५ अंकों से पीछे था .
;$$$ Modified by Sonam Gupta MTech IT Banasthali 10-1-2014 (modified meaning from pOintsa to aMka)
;Added by Meena(25.7.11)
;At this point, the Dow was down about 35 points. 
(defrule point1
(declare (salience 5200)) ;Salience reduced '5800' to '5200' by 14anu-ban-09 on (04-11-2014)
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id points)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  point.clp      point1   "  ?id "  aMka )" crlf))
)
 


;Added by Meena(6.5.11)
;Each exercise focuses on a different grammar point.
(defrule point2
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) grammar)
(samAsa_viSeRya-samAsa_viSeRaNa ?id =(- ?id 1)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muxxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  point.clp      point2   "  ?id "  muxxA )" crlf))
)



(defrule point3
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id pointing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id tIpatApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  point.clp  	point3   "  ?id "  tIpatApa )" crlf))
)

;"pointing","N","1.tIpatApa/tipakArI"
;The pointing used in the castle is of good quality
;
;
(defrule point4
(declare (salience 4900))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp	point4  "  ?id "  " ?id1 "  xiKA  )" crlf))
)

;He pointed out his car to the watchman.
;usane apanI kAra cOkIxAra ko xiKAI
(defrule point5
(declare (salience 4800))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XyAna_AkarRiwa_kara))
(assert (kriyA_id-object_viBakwi ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp	point5  "  ?id "  " ?id1 "  XyAna_AkarRiwa_kara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  point.clp     point5   "  ?id "  kI_ora )" crlf)
)

;Added by Sheetal(02-08-10)
;(defrule point6
;(declare (salience 4950))
;(id-root ?id point)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 out)
;(kriyA-upasarga ?id ?id1)
;(kriyA-object  ?id ?obj)
;(id-word ?obj that)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XyAna xilA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp	point6  "  ?id "  " ?id1 "  XyAna xilA  )" crlf))
;)

;$$$ Modified by Sonam Gupta MTech IT Banasthali 12-3-2014 (increased silence)
;particle_out_- && category=verb	nirxeSa_kara	100
;PP_null_out && category=verb	nirxeSa_kara	100
;Added by Human
(defrule point7
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 pointed)
(id-word ?id1 teeth)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukIle))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point7   "  ?id "  nukIle )" crlf))
)

(defrule point8
(declare (salience 4500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id pointed)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point8   "  ?id "  nukIlA )" crlf))
)

;"pointed","Adj","1.nukIlA"
;This pencil has pointed edges on both sides
;--"2.suspaRta"
;He gave a pointed talk on the issue.
;
;
(defrule point9
(declare (salience 4400))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iSArA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point9   "  ?id "  iSArA_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;The road sign points left. [Cambridge]
;सड़क दायी तरफ की ओर इशारा करती है .
(defrule point10
(declare (salience 4400))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(kriyA-subject  ? ?id)(viSeRya-det_viSeRaNa  ?id ?)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iSArA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point10   "  ?id "  iSArA_kara )" crlf))
)

;$$$ Modified by Sonam Gupta MTech IT Banasthali 12-3-2014 (increased silence)
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Pruning keeps the plants bushy, so that there are more branches and thus more plucking points.  [Gyannidhi]
;छंटाई से ये पौधे झाड़ी ही बने रहते हैं और इससे इसकी शाखाएं भी अधिक होती हैं और इससे पत्तियां भी काफी मात्रा में तोड़ी जाती हैं।
(defrule point11
(declare (salience 5300))
(id-word ?id points)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(viSeRya-viSeRaNa  ?id ?)(kriyA-aBihiwa  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point11   "  ?id "  mAwrA
 )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (31-03-2015)
;I thought you put your points very well. [oald]	;added by 14anu-ban-09 on (31-03-2015)
;मैंने सोचा कि आपने अपनी बहुत अच्छी बातें रखीं . 	                    [Manual]	;added by 14anu-ban-09 on (31-03-2015)
;$$$ Modified by 14anu-ban-01 on (23-07-2014):in such cases point is always 'countable'.
;In Lothal one point is cleared that it must have been a huge centre of trade .
;लोथल  में  एक  बात  तो  स्पष्ट  हो  ही  जाती  है  कि  यह  व्यापार  का  बड़ा  केंद्र  रहा  होगा  ।
;@@@ Added by Sonam Gupta MTech IT Banasthali 3-1-2014
;To prove his point, the robber-chief decided to climb through the window himself.
;अपनी बात सिद्ध करने के लिए डाकुअों के सरदार ने खुद चढ़कर खिड़की में से उतरने का फैसला किया।
(defrule point12
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 prove|clear|put); added 'clear' by 14anu-ban-01 on (23-07-2014)	; added 'put' by 14anu-ban-09 on (31-03-2015)
(or (kriyA-subject  ?id1 ?id)(kriyA-object  ?id1 ?id)) ; added 'kriyA-subject' by 14anu-ban-01 on(23-07-2014) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point12   "  ?id "  bAwa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;Could I make a point about noise levels? [Cambridge]
;क्या मैं शोर के स्तर के बारे में राय दे सकता हूँ?
(defrule point13
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rAya_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point13  "  ?id "  " ?id1 "  rAya_xe )" crlf))
)


;$$$ Modified by Sonam Gupta MTech IT Banasthali 12-3-2014 (added important to ?id1)
;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;He explained his point by drawing a diagram.  [Cambridge]
;उसने अपनी बात एक चित्र बनाकर समझाई.
(defrule point14
(declare (salience 5200))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object  ?id1 ?id)(viSeRya-viSeRaNa  ?id ?id1))
(id-root ?id1 explain|describe|elucidate|illustrate|argue|important)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point14   "  ?id "  bAwa )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;I take your point about cycling, but I still prefer to walk. [Cambridge]
;मैं साइक्लिंग के बारे में आपके बात मानता हूँ, परन्तु मैं फिर भी चलना पसन्द करूँगा.
(defrule point15
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 take)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point15  "  ?id "  " ?id1 "  bAwa_mAna )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;She's always complaining that the office is cold. Well, she's got a point. [Cambridge]
;वह हमेशा शिकायत करती है दफ्तर ठण्डा है. खैर, उसकी बात में दम है.
(defrule point16
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 get)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_meM_xama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point16  "  ?id "  " ?id1 "  bAwa_meM_xama )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;I thought he was never going to get to the point.  [Cambridge]
;मुझे लगा कि वो कभी भी मुद्दे पर नहीं आएगा.
(defrule point17
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muxxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point17   "  ?id "  muxxA )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;The point of a pencil. [OALD]
;पेन्सिल की नोक.
;The point of a knife. [OALD]
;चाकू की नोक.
;The point of a pin. [OALD]
;आलपिन की नोक.
(defrule point18
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 pencil|pen|knife|pin|needle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id noka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point18   "  ?id "  noka )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;What's the point of studying if you can't get a job afterwards?  [Cambridge]
;अध्ययन करने का मतलब क्या है यदि आप बाद में नौकरी प्राप्त नहीं कर सकते हैं? 
(defrule point19
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI  ?id ?)(viSeRya-kqxanwa_viSeRaNa  ?id ?)(viSeRya-in_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawalaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point19   "  ?id "  mawalaba )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;The fact that he doesn't want to come is beside the point - he should have been invited. [Cambridge]
;यह तथ्य कि वह आना नहीं चाहता है जरूरी नहीं है- उसको आमन्त्रित करना चाहिये था . 
(defrule point20
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-beside_saMbanXI  ? ?id)
(id-root ?id1 beside)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jarUrI_nahIM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point20  "  ?id "  " ?id1 "  jarUrI_nahIM )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;What he says is true up to a point. [Cambridge]
;वह जो कहता है एक सीमा तक सत्य है .  
(defrule point21
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id1 ?id)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sImA_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point21  "  ?id "  " ?id1 "  sImA_waka )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (04-08-2014)
;@@@ Added by Sonam Gupta MTech IT Banasthali 10-1-2014
;I know she's bossy, but she has lots of good points too.  [Cambridge]
;मैं जानता हूँ वह धौंस देने वाली है , परन्तु उसकी बहुत सारी अच्छी आदते भी हैं .
(defrule point22
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1) ;Added by 14anu-ban-09
(id-root ?id1 good|bad) ;Added by 14anu-ban-09
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Axawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point22   "  ?id "  Axawa )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;Chemistry never was my strong point.  [Cambridge]
;रसायन-शास्त्र मेरा मजबूत पक्ष कभी नहीं था . 
(defrule point23
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 strong|weak|plus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point23   "  ?id "  pakRa )" crlf))
)

;NOTE-Removed this rule.As this rule is merged with point45 rule, both were identical. ;added by 14anu-ban-09 on (29-11-2014)
;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;She made several interesting points in the article. [OALD]
;उसने लेख में कई रोचक बिन्दू दिए . 
;(defrule point24
;(declare (salience 5500))
;(id-root ?id point)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id ?id1)
;(id-root ?id1 interesting|remarkable|motivating|main|critical|key|crucial|focal)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id binxU))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point24   "  ?id "  binxU )" crlf))
;)



;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;That's the whole point. [OALD]
;पूरा सार यही है . 
(defrule point25
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 whole|complete|entire)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point25   "  ?id "  sAra )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-10-2014)
;[COUNTER EXAMPLE-NCERT CORPUS] #####Thus, all the shells exert a gravitational force at the point outside just as if their masses are concentrated at their common center according to the result stated in the last section. ######### 
;awaH sprifga - bala xvArA kiyA gayA kArya kevala sire ke binxuoM para nirBara karawA hE. [NCERT CORPUS]  ;added wx-notation on (25-10-2014)
;At some point in your career. [Suggested by Aditi mam] ;added by 14anu-ban-09 on (25-10-2014)
;आपके कैरियर में किसी क्षण/दौर पर . [Self] ;added by 14anu-ban-09 on (25-10-2014)
;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;Many people suffer from mental illness at some point in their lives. [OALD]
;कई लोग अपने जीवन के कुछ क्षणों में मानसिक बीमारी से ग्रस्त होते हैं  . 
(defrule point26
(declare (salience 5400))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-at_saMbanXI  ? ?id) commented by 14anu-ban-09
(viSeRya-in_saMbanXI  ?id ?id1) ;added by 14anu-ban-09 
(id-root ?id1 life|career) ;added by 14anu-ban-09 ;added 'career' by 14anu-ban-09 on (25-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRaNa/xOra)) ;added another meaning 'xOra' by 14anu-ban-09 on (25-10-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point26   "  ?id "  kRaNa/xOra )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;I'll wait for you at the meeting point in the arrivals hall. [OALD]
;मैं आगन्तुक हाल में सभा की जगह पर आपकी प्रतीक्षा करूँगा. 
(defrule point27
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-beyond_saMbanXI  ? ?id)(and(kriyA-at_saMbanXI  ? ?id)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point27   "  ?id "  jagaha )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;To win a point. [OALD]
;एक अंक जीतना . 
(defrule point28
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 win|lose|gain|decrease|increase)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point28   "  ?id "  aMka )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (17-10-2014)
;Also that the incident ray, reflected ray and the normal to the reflecting surface at the point of incidence lie in the same plane (Fig. 9.1). [NCERT CORPUS] ;added by 14anu-ban-09 on (15-11-2014) 
;isake awirikwa, Apawiwa kiraNa, parAvarwiwa kiraNa waWA parAvarwaka pqRTa ke Apawana biMxu para aBilamba eka hI samawala meM howe hEM (ciwra 9.1) . [NCERT CORPUS] ;added by 14anu-ban-09 on (15-11-2014) 
;Now we join the point of the intersection of these two lines to the origin O. [NCERT CORPUS]
;जिस बिन्दु पर यह दोनों रेखाएं एक दूसरे को काटती हैं,उसे मूल बिन्दु O से जोड देते हैं. [NCERT CORPUS]
;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;The stars were points of light in the sky. [OALD]
;आसमान में तारे प्रकाश के बिन्दु थे . 
(defrule point29
(declare (salience 5600))
(id-root ?id point)
;(id-word ?id points) ;commented by 14anu-ban-09 on (17-10-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1) ;changed '?' to '?id1'
(id-root ?id1 intersection|light|incidence) ;added by 14anu-ban-09 ;added 'incidence' by 14anu-ban-09 on (15-11-2014) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point29   "  ?id "  binxu )" crlf))
)

;Added by Shirisha Manju (24-08-13) Suggested by Chaitanya Sir
;Because light could travel through vacuum and it was felt that a wave would always require a medium to propagate from one point to the other.
(defrule point_default_rule
(declare (salience 300))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biMxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point_default_rule   "  ?id "  biMxu )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;I am bound to say I disagree with you on this point.
;मुझे कहना ही पङेगा कि मैं आपकी इस बात से असहमत हूँ .
(defrule point30
(declare (salience 5300))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?verb ?)
(kriyA-on_saMbanXI  ?verb ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point30   "  ?id "  bAwa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;I see your point, but I don't think everyone will agree. [M-W]
;मैं आपकी बात समझता हूँ, परन्तु मुझे नहीं लगता कि हर कोई सहमत होगा .
(defrule point31
(declare (salience 5300))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 see|stretch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point31   "  ?id "  bAwa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;Let me make one final point.  [M-W]
;मुझे एक आखिरी बात रखने दीजिये .
(defrule point32
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point32  "  ?id "  " ?id1 "  bAwa_raKa )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (22-10-2014)
;[Counter Example] ##### What's the point of studying if you can't get a job afterwards? ######   [Cambridge]
;अध्ययन करने का मतलब क्या है यदि आप बाद में नौकरी प्राप्त नहीं कर सकते हैं?  
;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;My point is simply that we must do something to help the homeless. [M-W]
;मेरा विचार सिर्फ इतना है कि हमें बेघरों के लिये कुछ करना चाहिये .
(defrule point33
(declare (salience 5300))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-RaRTI_viSeRaNa  ?id ?id1)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 my);added by 14anu-ban-09 on (22-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point33   "  ?id "  vicAra )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;Maybe there's a better way to get your point across.  [M-W]
;सम्भवतः आपकी बात समझने के लिये कोई बेहतर तरीका हो .
(defrule point34
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
(kriyA-object  ?id1 ?id)
(id-root ?id1 get)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_samaJanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point34  "  ?id "  " ?id1 "  bAwa_samaJanA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;I don't want to labor the point, but I think I should mention again that we are running out of time.  [M-W]
;मैं इस बात को दोहराना नहीं चाहता, परन्तु मैं सोचता हूँ कि मुझे फिर से बताना चाहिये कि हमारे पास समय कम है .
(defrule point35
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 labor|belabor)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_xoharAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point35  "  ?id "  " ?id1 "  bAwa_xoharAnA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;It took several paragraphs for her to come to the point of her argument.   [M-W]
;उसे अपने मुद्दे पर आने के लिये कई अनुच्छेद लग गये .
(defrule point36
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 come|get)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muxxe_para_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point36  "  ?id "  " ?id1 "  muxxe_para_A)" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;If you drive while drunk, you could lose your license, but even more to the point, you could kill someone.   [M-W]
;यदि आप पीकर ड्राईविंग करते हैं, आप अपना लाइसेन्स खो सकते हैं, परन्तु इससे भी जरूरी, आप किसी की जान ले सकते हैं .
(defrule point37
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id1 ?id)
(id-root ?id1 more)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 isase_BI_jarUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point37  "  ?id "  " ?id1 "  isase_BI_jarUrI )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;We met at a point halfway between the two cities.   [M-W]
;हम दो शहरों के बीच की जगह पर मिले .
;Ellis Island in Manhattan was the point of entry of many American immigrants.   [M-W]
;मैनहैट्टन में एलिस आईलैण्ड अमरीकी शरणार्थियों के लिये प्रवेश की जगह था .
(defrule point38
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-at_saMbanXI  ?id1 ?id)(viSeRya-of_saMbanXI  ?id ?id1))
(id-root ?id1 meet|go|contact|join|cross|assemble|reunite|entry|admission|access|opening|entrance|submission|admittance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point38   "  ?id "  jagaha )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 3-2-2014
;We walked all day and were beyond the point of exhaustion.   [M-W]
;हम पूरे दिन चले और सीमा से परे थक गए .
(defrule point39
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-beyond_saMbanXI  ? ?id)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point39   "  ?id "  sImA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (17-10-2014)
;[COUNTER EXAMPLE] ##### Thus the work done by the spring force depends only on the end points. ##### [NCERT CORPUS]
;awaH sprifga - bala xvArA kiyA gayA kArya kevala sire ke binxuoM para nirBara karawA hE. [NCERT CORPUS]
;@@@ Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;He beat his opponent on points.  [M-W]
;उसने अपने प्रतिद्वन्दियों को अंकों से हराया .
(defrule point40
(declare (salience 5600))
(id-root ?id point)
(id-word ?id points)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?id1 ?id) ;modified '?' to '?id1' by 14anu-ban-09 
(id-root ?id1 beat) ;added by 14anu-ban-09 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point40   "  ?id "  aMka )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;She sharpened the pencil down to a point.   [M-W]
;उसने पेन्सिल को एक नोक तक पैना किय् .
(defrule point41
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 sharpen|sharp|taper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id noka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point41   "  ?id "  noka )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;A line drawn between two points.   [M-W]
;दो बिन्दुओं के बीच में खींची हुई रेखा .
(defrule point42
(declare (salience 5600))
(id-root ?id point)
(id-word ?id points)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-between_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point42   "  ?id "  binxU )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (04-11-2014)
;[Counter Example]#########The ice point and the steam point of water are two convenient fixed points and are known as the freezing and boiling points.#############  [NCERT CORPUS]
;jala kA himAMka waWA BApa - biMxu xo suviXAjanaka niyawa biMxu hEM, jinheM himAMka waWA kvaWanAMka kahawe hEM. [NCERT CORPUS] 
;@@@ Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;He had a temperature of one hundred and four point two.  [M-W]
;उसे एक सौ चार दशमलब दो का तापमान था .
(defrule point43
(declare (salience 5700))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
(kriyA-object  ? ?id) ;added by 14anu-ban-09 on (04-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaSamalaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point43   "  ?id "  xaSamalaba )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;I understand his feelings up to a point.   [M-W]
;मैं एक हद तक उसकी भावनाएँ समझता हूँ  .
(defrule point44
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-upasarga  ?verb ?id1)
(kriyA-to_saMbanXI  ?verb ?id)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haxa_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point44  "  ?id "   haxa_waka )" crlf))
)

;NOTE-The rule point24 is merged with this rule bacause both were identical. ;added by 14anu-ban-09 on (29-11-2014)
;$$$ Modified by 14anu-ban-09 on (17-10-2014)
;A convenient point to take moments about is G. [NCERT CORPUS]
;eka EsA binxu jisake pariwaH AGUrNa jFAwa karane se suviXA rahegI @G hE. [NCERT CORPUS]
;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;When you hold a pencil in front of you against some specific point on the background a wall and look at the pencil 
;first through your left eye A closing the right eye and then look at the pencil through your right eye B closing the 
;left eye you would notice that the position of the pencil seems to change with respect to the point on the wall.  [physics ncert]
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिन्दु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, 
;और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिन्दु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.
(defrule point45
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 specific|particular|opposite|convenient|interesting|remarkable|motivating|main|critical|key|crucial) ;added 'convenient' by 14anu-ban-09 ;added 'interesting|remarkable|motivating|main|critical|key|crucial' by 14anu-ban-09 on (29-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu)) ;changed meaning from 'binxU' to 'binxu' by 14anu-ban-09 on (17-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point45  "  ?id "   binxU )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;When you hold a pencil in front of you against some specific point on the background a wall and look at the pencil 
;first through your left eye A closing the right eye and then look at the pencil through your right eye B closing the 
;left eye you would notice that the position of the pencil seems to change with respect to the point on the wall.  [physics ncert]
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिन्दु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, 
;और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिन्दु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.
(defrule point46
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point46  "  ?id "   binxU )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;The distance between the two points of observation is called the basis. [physics ncert]
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिन्दु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, 
;और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिन्दु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.
(defrule point47
(declare (salience 5900))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-between_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point47  "  ?id "   binxU )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 14-3-2014
;From B or the point of the maximum height the ball falls freely under the acceleration due to gravity. [physics ncert]
;इस समय में गेन्द बिन्दु A से B पर पहुञ्चती है .
(defrule point48
(declare (salience 5900))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?noun ?)
(viSeRya-of_saMbanXI  ?id ?noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point48  "  ?id "   binxU )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning from 'wAnI' to 'wAna'
;@@@ Added by 14anu21 on 03.07.2014
;He pointed a gun at her. [oxford]
;उसने इशारा की बन्दूक उसपर . 
;उसने उसपर बन्दूक तानी.
(defrule point049
(declare (salience 5900))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object  ?id ?id1)(kriyA-subject ?id ?id1))
(id-root ?id1 gun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point049  "  ?id " wAna   )" crlf))
)


;default_sense && category=verb	iSArA kara	0
;"point","V","1.iSArA karanA"
;He pointed towards the broken window
;--"2.niSAnA bAzXanA/lakRya karanA"
;He pointed the gun at the lion
;--"3.kI ora honA"
;A rose plant's spines point upward
;--"4.mahawwva xenA"
;This story points the duties && rights of the person
;--"5.tIpa karanA"
;He points the broken wall
;
;



;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_point23
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 strong|weak|plus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   sub_samA_point23   "   ?id " pakRa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_point23
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 strong|weak|plus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   obj_samA_point23   "   ?id " pakRa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_point24
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 interesting|remarkable|motivating|main|critical|key|crucial)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   sub_samA_point24   "   ?id " binxU )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_point24
(declare (salience 5500))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 interesting|remarkable|motivating|main|critical|key|crucial)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   obj_samA_point24   "   ?id " binxU )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_point35
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 labor|belabor)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_xoharAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " point.clp   sub_samA_point35   "   ?id " " ?id1 " bAwa_xoharAnA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_point35
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 labor|belabor)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_xoharAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " point.clp   obj_samA_point35   "   ?id " " ?id1 " bAwa_xoharAnA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_point45
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 specific|particular|opposite)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   sub_samA_point45   "   ?id " binxU )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_point45
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 specific|particular|opposite)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   obj_samA_point45   "   ?id " binxU )" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-09-2014)
;added 'reference|ice' by 14anu-ban-09 on (04-11-2014)
;The ice point and the steam point of water are two convenient fixed points and are known as the freezing and boiling points.  [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;ye xo niyawa biMxu vaha wApa hEM jina para SuxXa jala mAnaka xAba ke aXIna jamawA waWA ubalawA hE.
;jaba xAba waWA selsiyasa wApa mApakrama, xo supariciwa wApa mApakrama hEM. [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;For the definition of any standard scale, two fixed reference points are needed. [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;kisI BI mAnaka mApakrama ke lie xo niyawa saMxarBa biMxuoM kI AvaSyakawA howI hE. [NCERT CORPUS] ;added by 14anu-ban-09 on (04-11-2014)
;Thus the work done by the spring force depends only on the end points. [NCERT CORPUS] added by 14anu-ban-09 on (17-10-2014)
;awaH sprifga - bala xvArA kiyA gayA kArya kevala sire ke binxuoM para nirBara karawA hE. [NCERT CORPUS] 
;The ice and steam point have values 32 ° F and 212 ° F respectively, on the Fahrenheit scale and 0 ° C and 100 ° C on the Celsius scale. [NCERT CORPUS]
;PArenahAita mApakrama para himAMka waWA BApa-biMxU ke kramaSaH 32 F waWA 212 F hEM jabaki selsiyasa mApakrama para enake mAna  kramaSaH O C waWA 100 O C hEM. [Pdf Suggestion]

(defrule point49
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 steam|end|reference|ice) ;added 'end' by 14anu-ban-09 on (17-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point49   "   ?id " binxu )" crlf))
)

;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;Collect points.
;अंक बटोरो.
;Rack up points.(Source: thefreedictionary.com )
;अंक इकठ्ठे करो.
(defrule point050
(declare (salience 5900))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 collect|rack|accumulate|gather|amass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point050  "  ?id "   aMka )" crlf))
)

;Removed by 14anu-ban-09 on (01-01-2015)
;NOTE-This rule is same as point050. So,removed the rule.
;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;Collect points.
;अंक बटोरो.
;(defrule point051
;(declare (salience 5900))
;(id-root ?id point)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(kriyA-subject  ?id ?id1)
;(id-root ?id1 collect|rack|accumulate|gather|amass)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id aMka))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point051  "  ?id "   aMka )" crlf))
;)

;@@@ Added by 14anu-ban-09 on (06-09-2014)
;These two points are the temperatures at which pure water freezes and boils under standard pressure. [NCERT CORPUS]
;ye xo niyawa biMxu vaha wApa hEM jina para SuxXa jala mAnaka xAba ke aXIna jamawA waWA ubalawA hE. 

(defrule point50
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(ssubject-subject_samAnAXikaraNa  ?id ?id1)
(id-root ?id1 temperature) ;more constaints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point50   "   ?id " binxU )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-09-2014)
;We can’t simply fix the Colorado by piping water from another place, as Brian Richter recently pointed out. [AnusaarakaGoogle_17_Jul_2014]
;hama kolArAdo ko mahaja eka anya jagaha se pAepa ke mAXyama se pAnI Beja kara TIka nahIM kara sakawe, jEsA briyAna ricatara ne hAla hI meM XyAna xilAyA. [Own Manual]

(defrule point51
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-root ?id1 out)
(id-root ?id2 recently)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_xilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point51   "   ?id " XyAna_xilA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (22-10-2014)
;[counter Example] #######What's the point of studying if you can't get a job afterwards?###########  [Cambridge] (Same CLP file)
;अध्ययन करने का मतलब क्या है यदि आप बाद में नौकरी प्राप्त नहीं कर सकते हैं? 
;@@@ Added by 14anu-ban-10 on (19-08-2014)
;In a turbulent flow the velocity of the fluids at any point in space varies rapidly and randomly with time.  
;vikRubXa pravAha meM kisI biMxu para warala kA vega xruwa waWA yAxqcCika rUpa se samaya meM baxalawA rahawA hE.
(defrule point52
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 space) ;added by 14anu-ban-09 on (22-10-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biMxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point52  "  ?id "  biMxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-10-2014)
;Similarly, if the eye-lens focusses the incoming light at a point behind the retina, a convergent lens is needed to compensate for the defect in vision. [NCERT CORPUS]        ;added by 14anu-ban-09 on (13-01-2015)
;इसी प्रकार, यदि नेत्र लेंस किसी वस्तु के प्रतिबिंब को दृष्टिपटल के पीछे किसी बिंदु पर फोकसित करता है तो इसे प्रतिकारित करने के लिए अभिसारी लेंस की आवश्यकता होती है. [NCERT CORPUS]                                   ;added by 14anu-ban-09 on (13-01-2015)
;This device is placed at a point inside the fluid. [NCERT CORPUS] ;added by 14anu-ban-09 on (17-10-2014)
;yaxi vicAraNIya biMxu 1 ko warala (mAnA pAnI) ke SIrRa Palaka para sWAnAnwariwa kara xiyA jAe jo vAyumaNdala ke lie KulA hE wo @P 1 ko vAyumaNdalIya xAba ( @P @a ) xvArA waWA @P 2 ko @P se prawisWApiwa kiyA jA sakawA hE. [NCERT CORPUS]
;Thus, all the shells exert a gravitational force at the point outside just as if their masses are concentrated at their common center according to the result stated in the last section. [NCERT CORPUS]
;isa prakAra saBI Kola pqWvI ke bAhara kisI binxu para isa prakAra guruwvAkarRaNa bala Aropiwa karezge jEse ki ina saBI KoloM ke xravyamAna piCale anuBAga meM varNiwa pariNAma ke anusAra unake uBayaniRTa kenxra para safkenxriwa hEM. 

(defrule point53
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 exert|place|focus)             ;added 'place' by 14anu-ban-09 on (17-10-2014) 
 					     ;added 'focus' by 14anu-ban-09 on (13-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point53   "   ?id " binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-10-2014)
;Thus radio waves broadcast from an antenna can be [received at points] far away where the direct wave fail to reach on account of the curvature of the earth. [NCERT CORPUS]
;isa prakAra kisI EntenA xvArA kiyA gayA rediyo warafga prasAraNa una sWAnoM para BI grahaNa kiyA jA sakawA hE jo bahuwa xUra hE waWA pqWvI kI vakrawA ke kAraNa jahAz warafgeM sIXe nahIM pahuzca pAwIM. [NCERT CORPUS]

(defrule point54
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 receive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point54   "   ?id " sWAna )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-10-2014)
;Hence there is no point in giving the period to a hundredth.  [NCERT CORPUS]
;इसलिए दोलन काल का मान सेकण्ड के सौवें भाग तक व्यक्त करने का कोई अर्थ नहीं है. [NCERT CORPUS]

(defrule point55
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) no)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point55   "   ?id " arWa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-10-2014)
;removed 'be' by 14anu-ban-09 on (22-10-2014)
;[Counter Example] ###### My point is simply that we must do something to help the homeless.###### [M-W] ;added by 14anu-ban-09 on (22-10-2014)
;मेरा विचार सिर्फ इतना है कि हमें बेघरों के लिये कुछ करना चाहिये . 
;If the point 1 under discussion is shifted to the top of the fluid (say water), which is open to the atmosphere, P 1 may be replaced by atmospheric pressure (Pa) and we replace P2 by P. [NCERT CORPUS]
;yaxi vicAraNIya biMxu 1 ko warala (mAnA pAnI) ke SIrRa Palaka para sWAnAnwariwa kara xiyA jAe jo vAyumaNdala ke lie KulA hE wo @P 1 ko vAyumaNdalIya xAba ( @P @a ) xvArA waWA @P 2 ko @P se prawisWApiwa kiyA jA sakawA hE. [NCERT CORPUS]

(defrule point56
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 shift|fix) ;added 'fix' by 14anu-ban-09 on (21-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point56   "   ?id " binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-10-2014)
;Note, the point of contact of the top with ground is fixed. [NCERT CORPUS]
;ध्यान दें कि लट्टू का वह बिन्दु जहाँ यह धरातल को छूता है, स्थिर है. [NCERT CORPUS]
;We know from experience that the axis of such a spinning top moves around the vertical through its point of contact with the ground, sweeping out a cone as shown in Fig. 7.5(a). [NCERT CORPUS]
;apane anuBava ke AXAra para hama yaha jAnawe hEM ki isa prakAra GUmawe lattU kI akRa, BUmi para isake samparka - binxu se gujarawe aBilamba ke pariwaH eka Safku banAwI hE jEsA ki ciwra 7.5(@a) meM xarSAyA gayA hE. [NCERT CORPUS]

(defrule point57
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 contact)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point57   "  ?id "  binxu )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  point.clp       point57   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (22-10-2014)
;In Fig. 10.3 point 1 is at height h above a point 2. [NCERT CORPUS]
;ciwra 10.3 meM biMxu 1 biMxu 2 se @h UzcAI para hE. [NCERT CORPUS]
(defrule point58
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 be)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point58   "   ?id " binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-10-2014)
;In Fig. 10.3 point 1 is at height h above a point 2. [NCERT CORPUS]
;ciwra 10.3 meM biMxu 1 biMxu 2 se @h UzcAI para hE. [NCERT CORPUS]
(defrule point59
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-above_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point59   "   ?id " binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (25-10-2014)
;The same principle holds good when we measure the weight of an object by a spring balance hung from a fixed point e.g. the ceiling. [NCERT CORPUS]
;yahI sixXAnwa usa samaya lAgU howA hE jaba hama kisI sWira binxu, jEse Cawa se latakI kisI kamAnIxAra wulA se kisI piNda kA BAra mApawe hEM. [NCERT CORPUS]

(defrule point60
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 fix) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWira_binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point60  "  ?id "  "  ?id1 "  sWira_binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-11-2014)
;We measure the angle between the two directions along which the planet is viewed at these two points. [NCERT CORPUS]
;ina xo sWiwiyoM se graha kI prekRaNa xiSAoM ke bIca kA koNa mApa liyA jAwA hE. [NCERT CORPUS]

(defrule point61
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 view) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point61   "   ?id " sWiwi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-11-2014)
;When the orbit of a satellite becomes elliptic, both the K.E. and P. E. vary from point to point. [NCERT CORPUS]
;जब किसी उपग्रह की कक्षा दीर्घवृत्तीय होती है तो उसकी K.E तथा P.E दोनों ही पथ के हर बिन्दु पर भिन्न होती हैं. [NCERT CORPUS]
;जब किसी उपग्रह की कक्षा दीर्घवृत्तीय होती है तो उसकी K.E तथा P.E दोनों ही पथ के बिन्दु से बिन्दु पर भिन्न होती हैं. [Self]
;Individually the kinetic energy K and the potential energy V( x) may vary from point to point, but the sum is a constant. [NCERT CORPUS]
;पृथक रूप से, गतिज ऊर्जा K और स्थितिज ऊर्जा V(x) एक स्थिति से दूसरी स्थिति तक परिवर्तित हो सकती है परन्तु इनका योगफल अचर रहता है. [NCERT CORPUS]
;पृथक रूप से, गतिज ऊर्जा K और स्थितिज ऊर्जा V(x) एक बिन्दु से बिन्दु पर परिवर्तित हो सकती है परन्तु इनका योगफल अचर रहता है. [Self]

(defrule point62
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-from_saMbanXI  ?id1 ?id)
(id-root ?id1 vary) 
(id-word =(- ?id 1) from)
(id-word =(+ ?id 1) to)
(id-word =(+ ?id 2) point)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng (- ?id 1) ?id (+ ?id 1) (+ ?id 2) binxu_se_binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp   point62  "   (- ?id 1) "  "   ?id " " (+ ?id 1) " "  (+ ?id 2) " binxu_se_binxu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-11-2014)
;A dot appears like the tip of an arrow pointed at you, a cross is like the feathered tail of an arrow moving away from you. [NCERT CORPUS]
;koI dAta (biMxu) ApakI ora safkewa karawe wIra kI noMka jEsA prawIwa howA hE waWA kroYsa kisI wIra kI pafKayukwa pUzCa jEsA prawIwa howA hE. [NCERT CORPUS]

(defrule point63
(declare (salience 4400))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 arrow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkewa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point63   "  ?id "  safkewa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-11-2014)
;As pointed out in the Chapter 1, the field E is not just an artefact but has a physical role. [NCERT CORPUS]
;jEsA ki aXyAya 1 meM nirxiRta kiyA jA cukA hE ki vixyuwa kRewra @E mAwra Silpa waWya hI nahIM hE, paranwu isakI BOwika BUmikA BI hE. [NCERT CORPUS]

(defrule point64
(declare (salience 5000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(kriyA-in_saMbanXI  ?id ?id2)
(id-root ?id1 out)
(id-root ?id2 Chapter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxiRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp 	point64   "  ?id "  nirxiRta_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-11-2014)
;Take care not to look into the laser beam directly and not to point it at anybody's face. [NCERT CORPUS]
;XyAna raKie ki lesara kiraNa-puFja meM kaBI BI sIXA na xeKeM Ora na hI ise kisI ke cehare para dAleM. [NCERT-CORPUS]

(defrule point65
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point65   "   ?id " dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-11-2014)
;The focus on the side of the (original) source of light is called the first focal point, whereas the other is called the second focal point. [NCERT CORPUS] 
;prakASa ke srowa kI ora sWiwa Pokasa ko praWama Pokasa biMxu kahawe hEM jabaki xUsarA xviwIya Pokasa biMxu kahalAwA hE . [NCERT CORPUS] 

(defrule point66
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 focal)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Pokasa_biMxu)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " point.clp  point66  "  ?id "  " ?id1 "  Pokasa_biMxu )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  point.clp       point66   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-12-2014) 
;NOTE-There is a parser problem in 2nd occurance of 'point'.This sentence is already sent for parser correction.
;The tip which points to the geographic north is called the north pole and the tip which points to the geographic south is called the south pole of the magnet. [NCERT CORPUS]
;इसका वह सिरा जो भौगोलिक उत्तर की ओर सङ्केत करता है, उत्तरी ध्रुव और जो भौगोलिक दक्षिण की ओर सङ्केत करता है, चुम्बक का दक्षिणी ध्रुव कहलाता है. [NCERT CORPUS]

(defrule point67
(declare (salience 5600))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 north|south)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkewa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point67  "  ?id "  safkewa_kara )" crlf))
)

;$$$ Modified by Shirisha Manju 19-12-14
;@@@ Addead by 14anu24 [16-6-14]
;One point is clear : there is no"one-size-fits-all"approach to managing TMJ.
;लेकिन एक बात स्पष्ट हैः टी . एम . जे को संभालने में सबके लिए केवल एक ही तरीका नहीं अपनाया जा सकता .
(defrule point68
(declare (salience 5600)); salience decreased by Shirisha Manju
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?noun ?);commented by Shirisha Manju
(subject-subject_samAnAXikaraNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa)) ;changed meaning 'bAta' as 'bAwa' by Shirisha Manju 19-12-14
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " point.clp  point68  "  ?id "   bAwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;NOTE :- Parser problem. Run on parser no. 97.
;A drop of 57 points on the Dow Jones index.	[drop.clp]
;dO jonsa kI iMdEksa meM 57 xaSamalaba kI girAvata huI hEM.	[self]
(defrule point69
(declare (salience 5700))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
(kriyA-of_saMbanXI  ?id1 ?id)
(id-root ?id1 drop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaSamalaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point69   "  ?id "  xaSamalaba )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;NOTE :- Parser problem. Run on parser no. 112.
;In the presence of an external field B0, which is strong enough, and at low temperatures, the individual atomic dipole moment can be made to align and point in the same direction as B0.	[NCERT CORPUS]
;पर्याप्त शक्तिशाली बाह्य चुम्बकीय क्षेत्र B0 की उपस्थिति में एवं निम्न तापों पर अलग-अलग परमाणुओं के द्विध्रुव आघूर्ण सरल रेखाओं में और B0 की दिशा के अनुदिश संरेखित किए जा सकते हैं.	[NCERT CORPUS]
;पर्याप्त शक्तिशाली बाह्य चुम्बकीय क्षेत्र B0 की उपस्थिति में एवं निम्न तापों पर अलग-अलग परमाणुओं के द्विध्रुव आघूर्ण सरल रेखाओं में संरेखित किए  और B0 की दिशा में संकेत किए  जा सकते हैं.	[self]

(defrule point70
(declare (salience 5700))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 direction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkewa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  point.clp     point70  "  ?id "  saMkewa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;The crane is securely anchored at two points.	[oald]
;करेन मज़बूती से दोनों कांटों से बन्धा हुआ है .		[self]

(defrule point71
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 anchor)             
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAztA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point71   "   ?id " kAztA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-03-2015)
;These two disagreed at every point where disagreement was possible. 	[Report-Set 2]
;ये दो प्रत्येक तथ्य पर असहमत हुए जहाँ असहमति सम्भव थी . 				[Anusaaraka]

(defrule point72
(declare (salience 6000))
(id-root ?id point)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 disagree)             
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waWya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " point.clp   point72   "   ?id " waWya )" crlf))
)
