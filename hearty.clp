;@@@ Added by 14anu-ban-06 (23-02-2015)
;We ate a hearty breakfast before we set off. (cambridge)
;इससे पहले  हम रवाना होते हमने भर-पेट नाश्ता खाया    . (manual)
(defrule hearty1
(declare (salience 2000))
(id-root ?id hearty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 breakfast|lunch|dinner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara-peta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hearty.clp 	hearty1   "  ?id "  Bara-peta )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-02-2015)
;She has a hearty dislike of any sort of office work. (cambridge)
;उसको किसी भी तरह के कार्यालय कार्य में बहुत ज्यादा अरुचि है . (manual)
(defrule hearty2
(declare (salience 2100))
(id-root ?id hearty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 dislike|like)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hearty.clp 	hearty2   "  ?id "  bahuwa_jyAxA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (23-02-2015)
;Hearty congratulations to everyone involved. (OALD)
;सभी सम्मिलित होने वालो को हार्दिक बधाइयाँ . (manual)
(defrule hearty0
(declare (salience 0))
(id-root ?id hearty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hArxika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hearty.clp   hearty0   "  ?id "  hArxika )" crlf))
)

