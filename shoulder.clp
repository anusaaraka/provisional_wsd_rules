
(defrule shoulder0
(declare (salience 5000))
(id-root ?id shoulder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kanXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shoulder.clp 	shoulder0   "  ?id "  kanXA )" crlf))
)

;"shoulder","N","1.kanXA"
;He had a injury in his shoulder.
;--"2.kamIjZa_kA_kazXA"
;The shoulders of his newly bought shirt were black in color.
;
(defrule shoulder1
(declare (salience 4900))
(id-root ?id shoulder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kazXA_para_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shoulder.clp 	shoulder1   "  ?id "  kazXA_para_uTA )" crlf))
)

;"shoulder","V","1.kazXA_para_uTAnA{BAra}"
;He shouldered the responsibility of his brother's children after his brother's death.
;

;@@@Added by 14anu19 (02-07-2014)
;She stood shoulder to shoulder with her husband. [coca]
;वह अपने पति के साथ कन्धे से कन्धा मिला कर खडी हुई
(defrule shoulder2
(declare (salience 5000))
(id-root ?id shoulder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(- ?id 1) to)
(id-word =(- ?id 2) shoulder)
;(kriyA-with_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 2) kanXe_se_kanXA_milA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shoulder.clp shoulder2  "  ?id "  " (- ?id 2) "  kanXe_se_kanXA_milA_kara  )" crlf))
)


;@@@Added by 14anu19 (02-07-2014)
; It is women who mainly shoulder responsibility for the care of elderly and disabled relatives .
;स्त्रियाँ है जो प्रमुख रूप से बुजुर्ग और विकलाङ्ग सगों की देखभाल के  उत्तरदायित्व को स्वीकार करती हैं 
(defrule shoulder3
(declare (salience 5000))
(id-root ?id shoulder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 blame|burden|responsibility|cost)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shoulder.clp         shoulder3   "  ?id "  svIkAra_kara )" crlf))
)
