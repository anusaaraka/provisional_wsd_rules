
(defrule wet0
(declare (salience 5000))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet0   "  ?id "  gIlA )" crlf))
)

;"wet","Adj","1.gIlA"
;The carpet is wet.
;
(defrule wet1
(declare (salience 4900))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet1   "  ?id "  varRA )" crlf))
)

;"wet","N","1.varRA"
;Keep away from the wet.
;--"2.gIlApana"
;There is some wet on the wall.
;--"3.nIrasa"
;He doesn't show any emotions.He seems to be a wet person.
;
(defrule wet2
(declare (salience 4800))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gIlA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet2   "  ?id "  gIlA_kara )" crlf))
)

;"wet","VT","1.gIlA_karanA"
;Don't get wet in the rain.
;
;@@@ Added by 14anu-ban-06 Karanveer Kaur (Banasthali Vidyapith) 25-07-2014
;After bathing in the beauty of this Lake Manyara National Park enriched with famous and enchanting sceneries we were returning with a wet heart .  (Parallel Corpus)
;isa prasixXa Ora mohaka prAkqwika xqSyoM se samqxXa isa leka manayArA neSanala pAraka kI suxarawA meM snAna ke bAxe hama eka xraviwa hqxaya se lOta rahe We.
(defrule wet3
(declare (salience 5500))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) heart)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id(+ ?id 1) xraviwa_hqxaya_se ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wet.clp	 wet3  "  ?id "  " (+ ?id 1)  " xraviwa_hqxaya_se   )" crlf))
)

;@@@ Added by 14anu-ban-02 (28-07-2014)
;The forests of Kudremukh National Park are semi-evergreen and wet deciduous  .
;कुद्रेमुख  राष्ट्रीय  उद्यान  के  वन  अर्द्ध  सदाहरित  और  नम  पर्णपाती  हैं  ।
;Sal tree covers the 80 percent part of the forest of Dudhwa National Park and fields of grass , lakes , ponds and rivers are in the 20 percent wet land  .
;दुधवा  राष्ट्रीय  उद्यान  की  वनभूमि  के  80  प्रतिशत  भाग  में  साल  वृक्ष  हैं  और  20  प्रतिशत  नम  भूमि  में  घास  के  मैदान  ,  झील  ,  तालाब  और  नदियाँ  हैं  ।
;In Simlipal National Park tropical wet peninsular sal forest , wet mixed , deciduous forest without sal and dry mixed ( without sal ) deciduous forest are found  .
;सिमलीपाल  राष्ट्रीय  उद्यान  में  उष्णकटिबंधीय  नम  प्रायद्वीपीय  साल  वन  ,  नम  मिश्रित  ,  पर्णपाती  साल  सहित  वन  और  शुष्क  मिश्रित  (  साल  रहित  )  पर्णपाती  वन  पाए  जाते  हैं  ।
;The forests of Silent Valley National Park are wet evergreen , semi-evergreen and wet deciduous  .
;शांत  घाटी  उद्यान  के  वन  नम  सदाहरित  ,  अर्द्ध  सदारहित  और  नम  पर्णपाती  हैं  ।
(defrule wet4
(declare (salience 5000))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 mountainous|deciduous|land|forest)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet4   "  ?id "  nama )" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-04-2015)
;Don't be so wet, she laughed.(oald)
;इतने कमजोर मत बनो ,वह हँसी . (self)
(defrule wet5
(declare (salience 5001))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 so)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet5   "  ?id "  kamajora)" crlf))
)

;@@@ Added by 14anu-ban-11 on (18-04-2015)
;Don’t be such a wet! (cald)
;ऐसे कमजोर मत रहिए! (self)
(defrule wet6
(declare (salience 4901))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(id-root ?id1 such)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet6   "  ?id "  kamajora)" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-04-2015)
;I could feel the wet of her tears. (oald)
;मैं उसके आँसू के बारे में गीलापन महसूस कर सका .  (self)
(defrule wet7
(declare (salience 4902))
(id-root ?id wet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 tears)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gIlApana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wet.clp 	wet7   "  ?id "  gIlApana)" crlf))
)

