;@@@ Added by 14anu-ban-06 (23-02-2015)
;A few horses danced nervously in the howling wind.(COCA)
;कुछ घोडों ने भीषण हवा में घबराते हुए नृत्य किया . (manual)
;The howling storm was extending the night. (COCA)
;भीषण आँधी रात विस्तृत कर(लम्बा कर) रही थी . (manual)
(defrule howling1
(declare (salience 2000))
(id-word ?id howling)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 wind|storm)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BIRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  howling.clp  	howling1   "  ?id "  BIRaNa )" crlf))
)

;------------------------ Default Rules ----------------------
;$$$ Modified by 14anu-ban-06 (23-02-2015)
;copied from howl.clp
;It was a howling success.(COCA)
;यह एक जबर्दस्त सफलता थी.(manual)
;"howling","Adj","1.mahAna saPalawA"
;usane parIkRA meM mahAna saPalawA prApwa kI.
(defrule howling0
(declare (salience 0))
(id-word ?id howling)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jabarxaswa));meaning changed from 'mahAna_saPalawA' to 'jabarxaswa' by 14anu-ban-06 (23-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  howling.clp   howling0   "  ?id "  jabarxaswa )" crlf))
)

