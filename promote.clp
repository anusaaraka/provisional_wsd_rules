;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################



;@@@ Added by Sonam Gupta MTech IT Banasthali 23-1-2014
;In the Faculties of Law and Medicine changes of a fundamental character intended to promote thoroughness were introduced. [Gyannidhi]
;कानून और चिकित्सा संकायों में संपूर्ण ज्ञान को बढ़ावा देने के उद्देश्य से बुनियादि किस्म के परिवर्तन किये गये।
(defrule promote1
(declare (salience 4900))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-kriyArWa_kriyA  ? ?id)(to-infinitive  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDAvA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote1   "  ?id "  baDAvA_xe )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 23-1-2014
;The band is promoting their new album. [Cambridge]
;बैन्ड अपने नये अलबम का प्रचार कर रहा है .
(defrule promote2
(declare (salience 5000))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 album|movie|band|party|group|line|fashion)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote2   "  ?id "  pracAra_kara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 23-1-2014
;Mediators were present to promote dialogue. [MW]
;बीच बचाव करने वाले बातचीत में सहायता करने के लिए उपस्थित थे .
(defrule promote3
(declare (salience 5000))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 dialogue|talk|discussion|conversation|chat|negotiation|growth|development)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote3   "  ?id "  sahAyawA_kara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 23-1-2014
;She's just been promoted to manager. [Cambridge]
;उसका प्रबन्धक के पद के लिए तरक्की हुई है .
(defrule promote4
(declare (salience 4000))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warakkI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote4   "  ?id "  warakkI_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-11-2014)
;Zero tillage is a fundamental management practice that promotes crop stubble retention under longer unplanned fallows when crops cannot be planted. [Agriculture]
;शून्य जुताई एक मूलभूत प्रबंधन प्रथा है जो लंबे समय तक अनियोजित जुताई के तहत चारा फसल को बढ़ावा देती है जब फसलें बोयी नहीं  जा सकतीं. [Manual]

(defrule promote6
(declare (salience 4900))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 retention)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-root ?id2 crop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDAvA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote6   "  ?id "  baDAvA_xe )" crlf))
)

;@@@Added by 14anu18 (02-07-14)
;She worked hard and was soon promoted. [OALD]
;उसने कङी मेहनत की और जल्द ही उसकी तरक्की हुई.
(defrule promote55
(declare (salience 4500))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warakkI_ho))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote55   "  ?id " warakkI_ho )" crlf))
)

;@@@ Added By 14anu17
;The area is being promoted as a tourist destination.
;क्षेत्र एक पर्यटक गन्तव्य की तरह उभारा जा रहा है . 
(defrule promote7
(declare (salience 100))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBAranA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote7   "  ?id "  uBAranA )" crlf))
)


;########################################################33Default Rule#######################################################################

;$$$Modified by 14anu18(02-07-14)
;Meaning changed from warakkI_ho to baDAvA_xe
;A round table promotes interaction.
;एक गोल सभा पारस्परिक व्यवहार को बढावा देती है . 
;@@@ Added by Sonam Gupta MTech IT Banasthali 23-1-2014

(defrule promote5
(declare (salience 4000))
(id-root ?id promote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDAvA_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote5   "  ?id " baDAvA_xe )" crlf))
)

;Removed by 14anu-ban-09 on (01-01-2015)
;NOTE- The relation given below is not satisying for the example sentence.And also, meaning is coming from default rule.So, no need of this rule.
;@@@ Added By 14anu17
;Promoting good health
;अच्छे स्वास्थ्य को बढ़ावा देना.
;(defrule promote8
;(declare (salience 100))
;(id-root ?id promote)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(vAkya-vAkya_saMbanXI  ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id baDZAvA_xenA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  promote.clp 	promote8   "  ?id "  baDZAvA_xenA )" crlf))
;)
