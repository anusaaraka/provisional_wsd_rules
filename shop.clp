
;Modified by Meena(3.3.11)
;Ulsoor lake is an ideal place for sightseeing, boating and shopping. 
;"shop","V","1.bAjZAra_karanA"
;I'm shopping for Christmas presents.
(defrule shop1
(declare (salience 4900))
(id-root ?id shop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id shopping)
(id-cat_coarse ?id verb|verbal_noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarIxArI_kara))
;(assert (id-wsd_root_mng ?id bAjZAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shop.clp 	shop1   "  ?id "  KarIxArI_kara )" crlf))
)

;@@@Added by 14anu18
; I want to shop around a little before I decide on these boots.
;इन जूतों पर निर्णय लेने से पहले मैं मूल्य की तुलना करना चाहता हू.
(defrule shop1_1
(declare (salience 5000))
(id-root ?id shop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) around) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya_ki_wulanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shop.clp 	shop1_1   "  ?id " mUlya_ki_wulanA_kara )" crlf))
)


;@@@Added by 14anu-ban-11 on (09-03-2015)
;He didn't expect his own mother to shop him to the police. (oald)
;उसे अपनी माँ से उम्मीद नहीं  थी  कि पुलिस से उसको पकडवायेगी  .                [self]
(defrule shop2
(declare (salience 5001))
(id-word ?id shop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 police) 
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pakadZavAyegI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  shop.clp 	shop2   "  ?id " pakadZavAyegI )" crlf))
)

;------------------------ Default Rules ----------------------
;"shop","N","1.xukAna"
;I went to the chemist's shop to buy some medicines.
(defrule shop0
(declare (salience 5000))
(id-root ?id shop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xukAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shop.clp 	shop0   "  ?id "  xukAna )" crlf))
)

;"shop","N","1.xukAna"
;I went to the chemist's shop to buy some medicines.
;--"2.kAraKAnA"
;

