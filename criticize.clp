;@@@ Added by 14anu-ban-03 (11-03-2015)
;We need to criticize our options. [M-W]
;हमें हमारे विकल्प की समीक्षा करने की जरूरत है . [manual]
(defrule criticize1
(declare (salience 10))
(id-root ?id criticize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 option)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samIkRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   criticize.clp 	criticize1  "  ?id "  samIkRA_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (11-03-2015)
;People criticized when the police failed arresting politicians.[pdf report]
;जब पुलिस राजनीतिज्ञों को पकडने मे असफल हुई  तब लोगों ने उनकी आलोचना की . [manual]
(defrule criticize0
(declare (salience 00))
(id-root ?id criticize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   criticize.clp       criticize0   "  ?id "  AlocanA_kara )" crlf))
)


