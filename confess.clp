;$$$ Modified by 14anu-ban-03 (04-04-2015)
;She confessed that she had taken the money. [confess.clp]
;उसने अङ्गीकार किया कि उसने पैसे लिए थे . [manual] 
(defrule confess1
(declare (salience 4900))
(id-root ?id confess)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)    ;added by 14anu-ban-03 (04-04-2015)
(id-root ?id1 take)   ;added by 14anu-ban-03 (04-04-2015)
(id-cat_coarse ?id verb)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMgIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confess.clp 	confess1   "  ?id "  aMgIkAra_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (04-04-2015)
;I confess that I know nothing about computers. [oald]
;मैं मानता हूँ कि मैं सङ्गणकों के बारे में कुछ भी नही जानता हूँ . [manual] 
(defrule confess2
(declare (salience 4900))
(id-root ?id confess)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)
(id-root ?id1 know)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  mAna))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confess.clp 	confess2  "  ?id "  mAna )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-04-2015)
;She confessed to the murder. [oald]
;उसने खून का दोष स्वीकार किया . [manual]
(defrule confess3
(declare (salience 4900))
(id-root ?id confess)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  xoRa_svIkAra_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confess.clp 	confess3  "  ?id "  xoRa_svIkAra_kara )" crlf))
)

;---------------------------------------default rule---------------------------------------------

;$$$ Modified by 14anu-ban-03 (04-04-2015)
;He confessed that he had stolen the money.[oald]
;उसने स्वीकार किया कि उसने पैसे चुराए हैं. [manual] 
;"confess","V","1.xoRa_svIkAra_kara"
(defrule confess0
(declare (salience 00))
(id-root ?id confess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  svIkAra_kara))  ;meaning changed from 'xoRa_svIkAra_kara' to 'svIkAra_kara' by 14anu-ban-03 (04-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confess.clp 	confess0   "  ?id "  svIkAra_kara )" crlf))
)


;"confess","VT","1.aMgIkAra_karanA"

