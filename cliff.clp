;@@@ Added by 14anu-ban-03 (27-01-2015)
;With the help of the wind the nature also inks the crux of its ideas on these desert cliffs of the expansive size .[tourism]
;प्रकृति भी हवा की सहायता से इन रेगिस्तानी विस्तृत आकार के टीलों पर अपने विचारों के सार के नमूने भी अंकित कर देती है . [tourism]
(defrule cliff1
(declare (salience 500))
(id-root ?id cliff)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cliff.clp 	cliff1   "  ?id "  tIlA )" crlf))
)

;------------------- Default Rule ------------------------
;@@@ Added by 14anu-ban-03 (27-01-2015)
;At a distance of about 400 metres from the sea shore in Kanyakumari on a large cliff is the Vivekanand Memorial .[tourism]
;kanyAkumArI meM samuxra wata se karIba 400 mItara xUra eka viSAla catZtAna para vivekAnaMxa memoriyala hE .[tourism]
(defrule cliff0
(declare (salience 00))
(id-root ?id cliff)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id catZtAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cliff.clp    cliff0   "  ?id "  catZtAna )" crlf))
)

