;$$$ Modified by 14anu-ban-01 on (05-02-2016)
;@@@ Added by 14anu03 on 16-june-2014
;Is this the way to talk to your father ? 
;क्या यह आपके पिता से बातचीत करने का तरीका है? 
(defrule way100
(declare (salience 5000))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(saMjFA-to_kqxanwa ?id ?id1)
(id-word ?id1 talk|act|do|speak|hold)		;added 'do' to the list ;added 'speak,hold' to the list by 14anu-ban-01 on (17-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way100   "  ?id "  warIkA )" crlf))
)

;It has influenced education in different ways.
;Behaviour is a specific response which can be seen && observed in an active way.
;###[MERGED RULE] ### way1, way8 in way0 by 14anu-ban-11 on (12-11-2014)
;$$$ Modified by 14anu-ban-11 on (26-09-2014)
(defrule way0
(declare (salience 5000))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) different);commented by 14anu-ban-11 on (26-09-2014)
(viSeRya-viSeRaNa ?id ?id1);added by 14anu-ban-11 on (26-09-2014)
(id-root ?id1 different|active|constructive);added by 14anu-ban-11 on (26-09-2014) ;added 'active' by 14anu-ban-11 on (12-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way0   "  ?id "  warIkA )" crlf))
)


;Commented by 14anu-ban-11 on 12-11-2014 -- merged in way0 rule 
;$$$ Modified by 14anu-ban-11 on (26-09-2014)
;(defrule way1
;(declare (salience 4900))
;(id-root ?id way)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) active) ;commented by 14anu-ban-11 on (26-09-2014)
;(viSeRya-viSeRaNa ?id ?id1) ;added by 14anu-ban-11 on (26-09-2014)
;(id-root ?id1 active);added by 14anu-ban-11 on (26-09-2014)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id warIkA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way1   "  ?id "  warIkA )" crlf))
;)

;Behaviour is a specific response which can be seen && observed in an active way.
(defrule way2
(declare (salience 100))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way2   "  ?id "  mArga )" crlf))
)

;default_sense && category=noun	paWa	0
;"way","N","1.paWa"
;This way leads to the field
;

;$$$ Modified by 14anu-ban-01 on (05-02-2016)
;### COUNTER EXAMPLE : I told you we should have done it my way! 	;added by 14anu-ban-01 on (17-02-2016)
;$$$ Modified by 14anu-ban-11 on (28-11-2014)
;Do it the way you have always done it .
;उस प्रकार करो जैसा तुमने हमेसा किया है. [manual]
;उसी प्रकार करो जैसा तुमने हमेशा किया है. [Translation improved by 14anu-ban-01]
;Added by sheetal(18-01-10).
(defrule way3
(declare (salience 4950))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)		
(viSeRya-det_viSeRaNa  ?id ?id3)		;added by 14anu-ban-01 on (17-02-2016)
(id-root ?id3 the)				;added by 14anu-ban-01 on (17-02-2016)
(object-object_samAnAXikaraNa  ?id2 ?id)	;added by 14anu-ban-01 on (05-02-2016)
(kriyA-object  ?id1 ?id2)			;added by 14anu-ban-01 on (05-02-2016)
(id-root ?id1 do|see|keep|like|leave|say|tell|use|make|play|be|know|spend|call|put|take|handle|remember|run|get|feel|cover|love|treat|explain|hear|enjoy|experience|answer|accept|appreciate|read|sing|use|pronounce|buy|build|approach|control|consider|cut|describe|decorate|design|digest|express|get|hide|hold|live|maintain|present|show|treat|twist|understand|write|give|want|win)	;added by 14anu-ban-01 => words taken from the sentences of COCA
;(id-root =(- ?id 1) the)			;commented by 14anu-ban-01
;(or (kriyA-kriyA_viSeRaNa ?id1 ?id)(kriyA-object_2 ?id1 ?id)(kriyA-object ?id1 ?id)) ;Comment this relation by 14anu-ban-11 on (28-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usI_prakAra))	;changed meaning from 'usa_prakAra' to 'usI_prakAra' by 14anu-ban-01]
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp       way3   "  ?id "  usI_prakAra )" crlf))	;changed meaning from 'usa_prakAra' to 'usI_prakAra' by 14anu-ban-01]
)
;Do it the way you have always done it .
;I really like the way you do your hair .

;Added by Aditya and Hardik(17.7.13)

;"ora"
;Turn your face this way.
;I turned my face that way.

;"warIkA"
;I don't think this will work, we better start looking for an alternative way.
;I don't know which way i am supposed to do it.

;@@@ Added by Pramila(BU) on 21-02-2014
;Turn your face this way.
;अपना चेहरा इस ओर कीजिए.
(defrule way4
(declare (salience 4800))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-object  ?kri ?id1)
(id-root ?kri turn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way4   "  ?id "  ora )" crlf))
)

;@@@ Added by Pramila(BU) on 21-02-2014
;That way, bacteria will never know that they have achieved the kind of threshold necessary to establish an infection in the body.
;उस प्रकार, बैक्टीरिया को  कभी पता नहीं चलेगा कि वे शरीर में संक्रमण की स्थापना के लिए आवश्यक सीमा तक पहुँच चिके है.
(defrule way5
(declare (salience 4800))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?kri ?id)
(kriyA-vAkyakarma  ?kri ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way5   "  ?id "  prakAra )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (05-02-2016) 
;### COUNTER EXAMPLE :"This is the way to go.	[sd_verified]"
;@@@ Added by Pramila(BU) on 21-02-2014
;What's the best way of approaching this problem?   ;oald
;इस समस्या को हल करने का सबसे अच्छा तरीका क्या है.
;$$$ Modified by 14anu21 on 26.06.2014 by adding "this" to the list (id-root ?id1 what|which|this)
;This is my way of solving the problem.
;यह समस्या को हल करने का मेरा मार्ग है . 
;यह समस्या को हल करने का मेरा तरीका है . [Translation improved by 14anu-ban-01: This sentence is handled in rule way13 ]
(defrule way6
(declare (salience 4800))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)	
(id-root ?id1 what|which)			;removed 'this' from the list
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way6   "  ?id "  warIkA )" crlf))
)

;Commented by 14anu-ban-01 on (17-02-2016) because conditions specified in this rule are inappropriate and this example is handled in rule way15
;;@@@ Added by Pramila(BU) on 21-02-2014
;;It was obvious that, with Sankaran Nair as Education Member, Sharp was not having everything his own way.   ;gyanidhi
;;यह स्पष्ट था कि शंकरन नायर के शिक्षा सदस्य होते हुए शार्प प्रत्येक कार्य अपनी तरीके से नहीं कर पा रहे थे।
;(defrule way7
;(declare (salience 4800))
;(id-root ?id way)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id1 ?id)
;(kriyA-object  ?kri ?id1)
;(id-root ?kri have|do)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id warIkA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way7   "  ?id "  warIkA )" crlf))
;)

;Commented by 14anu-ban-11 on 12-11-2014 -- merged in way0 rule 
;@@@ Added by 14anu18 (14-06-14)
;But you need to do this in a constructive way which will help them to change their behaviour
;परन्तु आपको एक रचनात्मक तरीके में यह करने की जरूरत है कि जो उनको उनका बर्ताव बदलने के लिए सहायता करेगा .
;(defrule way8
;(declare (salience 5000))
;(id-root ?id way)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id warIkA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way8   "  ?id "  warIkA )" crlf))
;)

;@@@ Added by 14anu-ban-11 on (10-11-2014)
;The rotating vectors that represent harmonically varying scalar quantities are introduced only to provide us with a simple way of adding these quantities using a rule that we already know.(NCERT) 
;आवर्ती रूप से परिवर्तित होने वाली अदिश राशियों को, घूर्णन सदिशों द्वारा निरूपित करने से हम इन राशियों का संयोजन एक सरल विधि द्वारा, एक पहले से ही ज्ञात नियम का प्रयोग करके, कर सकते हैं.(NCERT)
(defrule way9
(declare (salience 5010))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 simple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way9   "  ?id "  viXi )" crlf))
)


;@@@ Added by 14anu-ban-11 on (12-11-2014)
;The moment you enter inside the way you are welcomed, it appears from that only that here attention has been paid to divine peace.[KARAN SINGLA]
;Apa praveSa karawe hEM, mArga ke anxara Apa svAgawa kie gaye hEM vaha kRaNa yaha usase xiKawA hE sirPa ki yahAz XyAna amana BaviRyavANI karane ke lie lABaxAyI huA gayA hE .[KARAN SINGLA]
(defrule way10
(declare (salience 4000))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-inside_saMbanXI  ?id1 ?id)   ;added relation by 14anu-ban-11 on (12-11-2014)
(id-root ?id1 welcome)           ;added by 14anu-ban-11 on (12-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way10   "  ?id "  prakAra )" crlf))
)


;@@@ Added by 14anu-ban-11 on (28-11-2014)
;The phenomena of interference, diffraction and polarisation were explained in a natural and satisfactory way by the wave picture of light.(Ncert)
;प्रकाश के तरङ्ग-चित्र के द्वारा व्यतिकरण, विवर्तन तथा ध्रुवण की घटनाओं की स्वाभाविक एवं सन्तोषजनक रूप में व्याख्या की जा चुकी थी.(Ncert)
(defrule way11
(declare (salience 500))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 satisfactory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way11   "  ?id "  rUpa )" crlf))
)

;Commented by 14anu-ban-01 on (05-02-2016) because it's unnecessary as 'viSeRya-det_viSeRaNa' is an insufficient as well as inappropriate condition and this rule is firing irrelevantly in sentences like "This is the way to go ."
;@@@ Added by 14anu17
;And they will let you know the ways your complaint can be handled.
; और वे आप तरीके बता दे आपकी शिकायत सम्भाली जा सकती है . 
;(defrule way12
;(declare (salience 4951))
;(id-root ?id way)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id warIkA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp       way12   "  ?id "  warIkA )" crlf))
;)



;@@@ Added by 14anu-ban-01 on (05-02-2016)
;They have their own way of processing.	[COCA]
;उनका सोचने का अपना तरीका है .[self]
;This is my way of solving the problem.	[way6]	; In this sentence, parser isn't giving required relation "viSeRya-of_saMbanXI" because of which right now this rule will not fire in this case. 
;यह समस्या को हल करने का मेरा तरीका है . [way6]
(defrule way13
(declare (salience 4900))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way13   "  ?id "  warIkA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-02-2016)
;I beg your pardon, I was way off. [COCA]
;माफ कीजिए,मैं बहुत अशिष्ट था. [self]
(defrule way14
(declare (salience 4900))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 be)
(id-root =(+ ?id 1) off)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way14   "  ?id "  bahuwa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-02-2016)
;I told you we should have done it my way! [oald]
;मैंने आपको बताया था कि हमें इसे मेरे तरीके से करना चाहिये!  [self]
;It was obvious that, with Sankaran Nair as Education Member, Sharp was not having everything his own way.   [gyanidhi]
;यह स्पष्ट था कि शंकरन नायर के शिक्षा सदस्य होते हुए शार्प प्रत्येक कार्य अपने तरीके से नहीं कर पा रहे थे।	[self]
(defrule way15
(declare (salience 4950))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa ?id ?)
(not(kriyA-on_saMbanXI  ?id2 ?id))
(kriyA-object  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  way.clp 	way15   "  ?id "  warIkA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2016)
;He answered it in right way.	[right.clp]
;उसने सही तरीके से इसका उत्तर दिया .	[self]
(defrule way16
(declare (salience 4950))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 right)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sahI_warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  way.clp 	way16   "  ?id "  " ?id1 " sahI_warIkA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2016)
;She will make sure we go the right way. 	[COCA]
;वह आश्वस्त करेगी कि हम सही मार्ग पर जाएँ.		[self] 
(defrule way17
(declare (salience 4950))
(id-root ?id way)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 go)
(viSeRya-viSeRaNa ?id ?id2)
(id-root ?id2 right)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 sahI_mArga))
;(assert (kriyA_id-object_viBakwi ?id para))
(assert (id-H_vib_mng ?id para))
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  way.clp 	way17  "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng    " ?*prov_dir* "  way.clp 	way17   "  ?id " para )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  way.clp 	way17   "  ?id "  " ?id2 " sahI_mArga)" crlf)
)
