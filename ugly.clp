;@@@ Added by 14anu-ban-07, (23-03-2015)
;An ugly face.(oald)
;कुरूप चेहरा . (manual)
(defrule ugly0
(id-root ?id ugly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kurUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ugly.clp 	ugly0   "  ?id "  prakAra )" crlf))
)
;@@@ Added by 14anu-ban-07, (23-03-2015)
;There were ugly scenes outside the stadium.(cambridge)
;स्टेडियम् के बाहर भयङ्कर दृश्य थे . (manual)
(defrule ugly1
(declare (salience 1000))
(id-root ?id ugly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 scene|incident)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BayaMkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ugly.clp 	ugly1   "  ?id "  BayaMkara )" crlf))
)

;@@@ Added by 14anu-ban-07, (23-03-2015)
;The demonstration turned ugly when a group of protesters started to throw bottles at the police.(cambridge)
;प्रदर्शन ने भयङ्कर मोड लिया जब विरोधक  समूह ने पुलिस पर बोतलें फेंक शुरु किया . (manual)
(defrule ugly2
(declare (salience 1000))
(id-root ?id ugly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 demonstration)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BayaMkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ugly.clp 	ugly2   "  ?id "  BayaMkara )" crlf))
)


