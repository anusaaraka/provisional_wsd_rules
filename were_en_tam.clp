;$$$ Modified by 14anu-ban-11 on (26-11-214) "Added Print statement for passive "
;Added "bunch" in the list (Meena 31.3.11)
;Added by Meena(10.5.10)
;The box contained many books , some of which were badly damaged .
(defrule were_en_tam0
(declare (salience 5000))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id damage|bunch)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en 0_gayA_WA))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp    were_en_tam0  "  ?id "  0_gayA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam0  "  ?id " passive )" crlf));Added relation by 14anu-ban-11 on (26-11-2014)

)



;$$$ Modified by 14anu-ban-11 on (26-11-214) "Added Print statement for passive "
;They were born in Patna.
(defrule were_en_tam1
(declare (salience 5000))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id born);changed from "id1" to "id" by 14anu-ban-11 on (26-11-2014)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp  	were_en_tam1  "  ?id "  yA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam1  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam1  "  ?id " passive )" crlf));Added relation by 14anu-ban-11 on (26-11-2014)
)


;$$$ Modified by 14anu-ban-11 on (26-11-214) "Added Print statement for passive "
; Protoplasm were known as the physical basis for life.
(defrule were_en_tam2
(declare (salience 4900))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id  know);change from "id1" to "id" by 14anu-ban-11 on (26-11-2014)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_jAwA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp  	were_en_tam2  "  ?id "  yA_jAwA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam2  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam2  "  ?id " passive )" crlf));Added relation by 14anu-ban-11 on (26-11-2014)
)

;$$$ Modified by 14anu-ban-04 on (03-04-2015)
;note THERE IS NO EXAMPLE SENTENCE FOR 'subject_viBakwi ?id se' 
;###[COUNTER EXAMPLE]### The town walls were built as a defence against enemy attacks.       [oald]
;###[COUNTER EXAMPLE]### शत्रु के  हमलों  से सुरक्षा के लिए नगर की दीवारों को  बनाया गया था .                       [self]
;$$$ Modified by 14anu-ban-11 on (26-11-2014) "Added Print statement for passive "
;$$$ Modified by Neha Maurya, BHU on 17-07-2014 -- Added "test|make|use|find|kill|give|force|conduct|go|tell|left|build|bring" in the list
;The twins were tested for mental skills.(COCA)
;judavoM ke mAnasika kRamawA kI jAzca kI gayI WI.(Manual translation)
;The fruit was eaten.
(defrule were_en_tam3
(declare (salience 4850))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id ask|test|make|use|find|kill|give|force|conduct|go|tell|left|build|bring)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_gayA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
;(assert (kriyA_id-subject_viBakwi ?id se))        ;commented by 14anu-ban-04 on (03-04-2015)
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp  	were_en_tam3  "  ?id "  yA_gayA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam3  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam3  "  ?id " passive )" crlf))                           ;Added relation by 14anu-ban-11 on (26-11-2014)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam3  "  ?id " se )" crlf))            ;Added relation by 14anu-ban-11 on (26-11-2014)           ;commented by 14anu-ban-04 on (03-04-2015)
)





;$$$ Modified by 14anu-ban-11 on (26-11-214) "Added Print statement for passive "
(defrule were_en_tam4
(declare (salience 4800))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_gayA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp   were_en_tam4  "  ?id "  yA_gayA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam4  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam4  "  ?id " passive )" crlf));Added relation by 14anu-ban-11 on (26-11-2014)
)





;Added by sheetal
;My many female friends were angered by the hearings .
(defrule were_en_tam5
(declare (salience 4850))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?by_s)
(id-root ?by_s hearing)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA))
(assert (root_id-TAM-vachan ?id were_en p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp   were_en_tam5  "  ?id "  yA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam5  "  ?id " were_en p )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (26-11-214) "Added Print statement for passive "
;@@@ Added by Pramila(BU) on 11-02-2014
;Research had received a great stimulus at his hands and teachers and students in the postgraduate departments were engaged in widening 
;the bounds of knowledge.        ;gyannidhi
;शोध को उनके हाथों बहुत प्रोत्साहन मिला और स्नातकोत्तर विभागों में अध्यापक और छात्र ज्ञान की सीमाओं के विस्तार में लगे हुए थे।।
(defrule were_en_tam6
(declare (salience 4850))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id engage)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_huA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp   were_en_tam6  "  ?id "  yA_huA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam6  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam6  "  ?id " passive )" crlf));Added relation by 14anu-ban-11 on (26-11-2014)
)


;@@@ Added by 14anu-ban-03 (03-03-2015)
;The hills were cloaked in thick mist. [oald]
;पहाडियाँ घनी धुन्ध में ढकी हुई थीं . [manual]
(defrule were_en_tam7
(declare (salience 5000))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id cloak)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA_WA))
(assert (root_id-TAM-vachan ?id were_en p))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp  	were_en_tam7  "  ?id "  yA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  were_en_tam.clp   were_en_tam7  "  ?id " were_en p )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam7  "  ?id " passive )" crlf))
)

;@@@ Added by 14anu-ban-02 on (14-02-2016)
;From the sixteenth century onwards, great strides were made in science in Europe.	[NCERT corpus]
;सोलहवीं शताब्दी से यूरोप में विज्ञान के क्षेत्र में अत्यधिक प्रगति हुई.	[NCERT corpus]
(defrule were_en_tam8
(declare (salience 5000))
(id-TAM ?id were_en)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 strides)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id were_en yA))
(assert (id-tam_type ?id passive))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  were_en_tam.clp    were_en_tam8  "  ?id "  yA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  were_en_tam.clp    were_en_tam8  "  ?id " passive )" crlf))
)
