;@@@Added by 14anu-ban-02 (01-04-2015)
;She runs with blazing speed.[mw]
;वह तेज रफ्तार से दौडती है . [self]
(defrule blazing1 
(declare (salience 100)) 
(id-word ?id blazing) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 speed)
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id wejaZ)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blazing.clp  blazing1  "  ?id "  wejaZ )" crlf)) 
) 


;------------------------------------default_rule -------------------------------------------

;@@@Added by 14anu-ban-02 (01-04-2015)
;Sentence: A blazing hot day.[oald]
;Translation: तप्ता हुआ गरम दिन .[self] 
(defrule blazing0 
(declare (salience 0)) 
(id-word ?id blazing) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id wapwA_huA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blazing.clp  blazing0  "  ?id "  wapwA_huA )" crlf)) 
) 
