;@@@ Added by 14anu-ban-02 (11-11-2014)
;Since work and energy are so widely used as physical concepts, alternative units abound and some of these are listed in Table 6.1.[ncert]
;चूँकि कार्य एवं ऊर्जा व्यापक रूप से भौतिक धारणाओं के रूप में प्रयोग किए जाते हैं, अतः ये वैकल्पिक मात्रकों से भरपूर हैं और उनमें से कुछ सारणी 6.1 में सूचीबद्ध हैं.[ncert]
(defrule abound0
(declare (salience -1))
(id-root ?id abound)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarapUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abound.clp 	abound0   "  ?id "  BarapUra )" crlf))
)

;@@@ Added by 14anu-ban-02 (11-11-2014)
;Animal life of the order of deer , giant , and malabar squirrel abound in plenty in this scenic hill station in india . a tour to idukki would reveal the selfsame .[karan singla]
;डीयर , जाइन्ट और मालाबार स्कवायरल के क्रम का वन्य जीवन भारत में इस पहाडी क्षेत्र द्रिश्यावली में प्रचुर मात्रा में भरा हुआ है , इदुकी की यात्रा इसी को उजागर करती है .[karan singla]
(defrule abound1
(declare (salience 100))
(id-root ?id abound)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abound.clp 	abound1   "  ?id "  pracura )" crlf))
)
