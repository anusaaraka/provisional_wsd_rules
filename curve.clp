;@@@ Added by 14anu-ban-03 (28-01-2015)
;The roof which is on the entrance is curvaceous from the middle which means above the middle entrance and in a similar way there is a curve in the balcony above it.[tourism]
;द्रष़्टव्य यह है कि मुखार पर जो छ्ज्जा है वह बीच में अर्थात मध्य मेहराब के ऊपर वक्राकार है और इसी प्रकार उसके ऊपर मुँडेर में भी वक्र है .[tourism]
;मुख्य द्वार पर जो छ्ज्जा है वह बीच में से वक्राकार है अर्थात मध्य द्वार के ऊपर और इसी प्रकार उसके ऊपर मुँडेर में भी वक्र है . [manual] ;added by 14anu-ban-03 (02-02-2015)
(defrule curve3
(declare (salience 5050))  ;salience increased by 14anu-ban-03 (02-02-2015)
(id-root ?id curve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 balcony) ;added by 14anu-ban-03 (02-02-2015)
=> 
(retract ?mng)
(assert (id-wsd_root_mng ?id vakra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve3   "  ?id "  vakra )" crlf))
)


;@@@ Added by 14anu-ban-03 (28-01-2015)
;With a hairpin curve this charming valley hypnotises tourists .[tourism]
;हेयर पिन मोड़ लिये हुये यह मनोरम घाटी पर्यटकों का मन मोह लेती है .[tourism]
(defrule curve4
(declare (salience 5050))   ;salience increased by 14anu-ban-03 (02-02-2015)
(id-root ?id curve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) 
(id-root ?id1 hairpin)  ;added by 14anu-ban-03 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id modZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve4   "  ?id "  modZa )" crlf))
)

;@@@ Added by 14anu-ban-03 (28-01-2015)
;Instead of the normal curve linear format , the sculpture of Mahabodhi temple is minar - like . [tourism]
;सामान्य वक्र रेखीय स्वरूप के स्थान पर , महाबोधि मन्दिर की वास्तु मीनारनुमा है . [tourism]
(defrule curve5
(declare (salience 5050))   ;salience increased by 14anu-ban-03 (02-02-2015)
(id-root ?id curve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id) 
(id-root ?id1 format)  ;added by 14anu-ban-03 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vakra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve5   "  ?id "  vakra )" crlf))
)

;--------------------- Default Rules ---------------------------
(defrule curve0
(declare (salience 5000))
(id-root ?id curve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vakra_reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve0   "  ?id "  vakra_reKA )" crlf))
)

;"curve","N","1.vakra_reKA"
;There is a curve in the road
;
(defrule curve1
(declare (salience 4900))
(id-root ?id curve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vakra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve1   "  ?id "  vakra_ho )" crlf))
)

;"curve","VTI","1.vakra_honA"
;The road curved suddenly to the right

;@@@ Added by 14anu-ban-03 (28-01-2015)
;It appears to be an amazing city due to its curved mountains , beautiful architecture of churches and houses and dense pine trees. [tourism]
;यह घुमावदार पहाड़ों , खूबसूरत गिरजाघरों , मकानों की शिल्पकला और सघन चीड़ के वृक्षों से एक अद़्भुत शहर नजर आता है . [tourism]
(defrule curve2
(declare (salience 4900))
;(id-root ?id curve)
(id-word ?id curved)   ;replace id-root by id-word and replace curve by curved by 14anu-ban-03 (02-02-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumAvaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  curve.clp 	curve2   "  ?id "  GumAvaxAra )" crlf))
)


;
;LEVEL 
;
;
;'curve' Sabxa kA sUwra banAne kA prayAsa :- 
;
;"curve"
;
;"teDZA-meDZA"  
;The roads in the hilly areas are curved.  
;parvawIya ilAko meM sadZake teDZI-meDZI hEM.       <--mudZI_huI <--modZavAlI
;
;"modZa"
;The iron rod has many curves.
;lohe kI CadZa meM kaI modZa hEM
;
;"mudZanA"
;The path curved suddenly.
;pagadaNdI acAnaka mudZa gayI.
;
;ina uxAharaNo ko XyAna se xeKA jAya we ina saba meM eka AMwarika saMbaMxa hEM.yahAz "curve"   kA mUla arWa 'modZa' nikalawA hEM.jo kuCa isa prakAra nikalawA hEM.     
;
;sUwra : modZa`
;
;
