(defrule first0
(declare (salience 5000))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp 	first0   "  ?id "  praWama )" crlf))
)

;"first","Adj","1.praWama"
;He always comes first in his class.
;--"2.pahalA"
;There is a huge crowd on the first train of the day.
;
;default_sense && category=adverb	sabase_pahale	0
;PraWama will work in this case also.

;$$$ Modified by 14anu-ban-05 on (20-02-2015)
;The first model of atom was proposed by J. J. Thomson in 1898.[NCERT]
;san 1898 meM je. je. toYmasana ne paramANu kA pahalA moYdala praswAviwa kiyA.[NCERT]
;Note-Remove sentences added by 14anu17 as it is not an appropriate sentence.
;@@@ Added by 14anu17
;First make it sure that.
;पहले निश्चित कर लिजिए .
(defrule first_tmp
(declare (salience 5100))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahalA))	; changed meaning from 'pahale' to 'pahalA' by 14anu-ban-05 on (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first_tmp   "  ?id "  pahalA )" crlf))
)	; changed meaning from 'pahale' to 'pahalA' by 14anu-ban-05 on (20-02-2015)


;Note- There is a parser problem in this sentence so run this sentence on parser no-10 of multiple parse
;@@@ Added by 14anu-ban-05 on (20-02-2015)
;First make it sure that everything is alright.[Suggested by Chaitanya Sir]
;पहले निश्चित कर लिजिए कि सब कुछ ठीक है. [manual]
(defrule first_tmp1
(declare (salience 4901))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 make)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahalA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first_tmp1   "  ?id "  pahalA )" crlf))
)

;Modified  by Meena:18.8.09 :  used the relation(kriya-kriyA_viSeRaNa) instead of (id-cat..)and meaning as "pahale" 
;It plunged first its nose into the river.
;I'll finish my work first.
(defrule first1
(declare (salience 4900))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first1   "  ?id "  pahale )" crlf))
)


;Added by Meena(12.5.10)
;It was in Paris that Debussy first heard Balinese music .
(defrule first2
(declare (salience 4900))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  =(+ ?id 1) ?id)
;(test(> ?id1 ?id))   ;this condition needs to be checked (Meena 17.5.10)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahalI_bAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first2   "  ?id "  pahalI_bAra )" crlf))
)





;Added by Meena(18.8.09)
;Who came first in the race ? 
(defrule first3
(declare (salience 4900))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first3   "  ?id "  praWama )" crlf))
)

;"first","Adv","1.sabase_pahale"
;He was the first to enter the cave.
;--"2.ke_pahale"
;Before coming to the party I would like to first finish this work.
;He said that he will kill himself first before agreeing to the demands made by his wife.
;
(defrule first4
(declare (salience 4800))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp 	first4   "  ?id "  praWama )" crlf))
)

;"first","N","1.praWama"
;It was a first for the player to score a double century in one innings.
;

;@@@ Added by 14anu-ban-05 Prajna Jha on 31.07.2014
;Let us first guess the answer based on our common experience.[NCERT] 
;sabase pahale hama apane sAmAnya anuBavoM ke AXAra para isa praSna ke uwwara kA anumAna lagAez.
(defrule first5
(declare (salience 5500))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 guess|explain|define|introduce|realize|focus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvapraWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first5   "  ?id "  sarvapraWama )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-08-2014)
;The curiosity to learn about the world, unravelling the secrets of nature is the first step towards the discovery of science. [NCERT]
;saMsAra ke bAre meM sIKane kI jijFAsA, prakqwi ke rahasyoM ko sulaJAnA vijFAna kI Koja kI ora pahalA caraNa hE.[NCERT]
(defrule first6
(declare (salience 5100))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 step|digit|measurement|case|particle|object)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first6   "  ?id "  pahalA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-08-2014)
;We shall first see what the center of mass of a system of particles is and then discuss its significance.[NCERT]
;pahale hama yaha xeKeMge ki xravyamAna keMxra kyA hE Ora Pira isake mahawva para prakASa dAleMge.[NCERT]
;It was first thought that this change could be described by the rate of change of velocity with distance.[NCERT]  
;gElIliyo ne pahale socA ki vega meM ho rahe parivarwana kI isa xara ko xUrI ke sApekRa vyakwa kiyA jA sakawA hE.[NCERT]
(defrule first7
(declare (salience 5500))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 think|state|introduce|see)	;more constraints can be added 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first7   "  ?id "  pahale )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-08-2014)
;Therefore, it is first necessary to learn the language of vectors.[NCERT]
;awaeva sarvapraWama hama saxiSoM kI BARA (arWAwa saxiSoM ke guNoM evaM unheM upayoga meM lAne kI viXiyAz) sIKefge .[NCERT]
(defrule first8
(declare (salience 5500))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 necessary)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvapraWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first8   "  ?id "  sarvapraWama )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-08-2014)
;This law was established by Galileo Galilei (1564-1642) who was the first to make quantitative studies of free fall.[NCERT]
;isa niyama ko sarvapraWama gElIliyo gElilI (1564 - 1642) ne prawipAxiwa kiyA WA jinhoMne mukwa rUpa se girawI huI vaswu kA pahalI bAra viXivawa parimANAwmaka aXyayana kiyA WA .[NCERT]
(defrule first9
(declare (salience 5500))
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvapraWama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp   first9   "  ?id " sarvapraWama  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-11-2014)
;The idea is to first form mono-molecular layer of oleic acid on water surface. [NCERT]
;isa viXi kA mUla AXAra, jala ke pqRTa para olIka amla kI eka ekANvika parawa banAnA hE.[NCERT]
(defrule first10
(declare (salience 5000))
(Domain physics)
(id-root ?id first)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(+ ?id 1) form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  first.clp 	first10  "  ?id "   -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  first.clp      first10   "  ?id "  physics )" crlf))
)


