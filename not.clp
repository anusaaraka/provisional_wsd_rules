; @@@ Added by 14anu22 priya panthi(6.5.2014) mnnit priyapanthi8@gmail.com
(defrule not0
(id-root ?id not)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nahIM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  not.clp    not0  " ?id " nahIM )" crlf)
))


;@@@ Added by 14anu22
; A secured loan gives security to the lender , not to you .
; एक रक्षित कर्ज उधारदाता को, ना कि आपको सुरक्षा देता है . 
(defrule not1
(declare (salience 5001))
(id-root ?id not)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to )
(id-word =(- ?id 1) ~but)
(id-root =(+ ?id 2)  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) nA_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " not.clp  not1 "  ?id "  " (+ ?id 1) "  nA_ki )" crlf))
)

