;@@@ Added by 14anu-ban-04 (03-03-2015)
;She made no attempt to disguise her surprise.                   [oald]
;उसने अपने  विस्मय को छिपाने के लिए कोई  प्रयास  नहीं  किया .                      [self]
(defrule disguise2
(declare (salience 4910))
(id-root ?id disguise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id CipA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disguise.clp     disguise2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disguise.clp 	disguise2  "  ?id "  CipA )" crlf))
)

;@@@ Added by 14anu-ban-04 (03-03-2015)
;He is a master of disguise.                     [oald]
;वह वेश परिवर्तन करने में  माहिर है .                             [self]
(defrule disguise3
(declare (salience 5010))
(id-root ?id disguise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 master)
=>
(retract ?mng)
(assert (make_verbal_noun ?id))
(assert (id-wsd_root_mng ?id veSa_parivarwana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  disguise.clp      disguise3   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disguise.clp 	disguise3  "  ?id "  veSa_parivarwana_kara )" crlf))
)


;NOTE [RUN THIS SENTENCE ON PARSER NO 2]
;@@@ Added by 14anu-ban-04 (03-03-2015)
;She usually goes out in disguise to avoid being bothered by the public.                 [cald]
;वह जनता के द्वारा परेशान न होने से  बचने के लिए   आम तौर पर छद्मवेश में बाहर जाती है   .                             [self]
(defrule disguise4  
(declare (salience 5010))
(id-root ?id disguise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CaxmaveSa))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disguise.clp 	disguise4   "  ?id "  CaxmaveSa )" crlf))
)


;-------------------------------------------------------DEFAULT RULES-----------------------------------------------------------------------

(defrule disguise0
(declare (salience 5000))
(id-root ?id disguise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disguise.clp 	disguise0   "  ?id "  BeRa )" crlf))
)

;"disguise","N","1.BeRa"
;The theatrical notion of disguise is always associated with catastrophe in his
;stories.
;
(defrule disguise1
(declare (salience 4900))
(id-root ?id disguise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeRa_baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disguise.clp 	disguise1   "  ?id "  BeRa_baxala )" crlf))
)

;"disguise","VT","1.BeRa_baxalanA"
;The CID officer disguised himself as a potter to hide his identity.
;
