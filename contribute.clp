
(defrule contribute0
(declare (salience 5000))
(id-root ?id contribute)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 to)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vajaha_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " contribute.clp	contribute0  "  ?id "  " ?id1 "  vajaha_bana  )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-12-2014)
;If it is free to move, it will thus move contributing to a current.[ncert]
;यदि यह गति करने के लिए स्वतन्त्र है तो यह भी गतिमान होकर विद्युत धारा उत्पन्न करेगा.[ncert]
(defrule contribute2
(declare (salience 4900))
(id-root ?id contribute)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 current)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id uwpanna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  contribute.clp   contribute2  "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contribute.clp   contribute2   "  ?id "  uwpanna_ho )" crlf))
)


;------------------ Default rules --------------------------
;His excessive drinking(alcohol) contributed to his downfall.
;jZyAxA pInA usake pawana kA kAraNa banA
(defrule contribute1
(declare (salience 4900))
(id-root ?id contribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahayoga_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contribute.clp 	contribute1   "  ?id "  sahayoga_xe )" crlf))
)

;"contribute","VT","1.sahayoga_xenA"
;His work has contributed new information on a difficult problem.
;--"2.sahAyawA_karanA/caMxA_xenA"  
;You should contribute old clothes for the victims of flood.
;Contribute some money to the charity collection.
;--"3.BAga_lenA"
;The chairman encouraged everyone to contribute to the discussion.
;--"4.prakASana_hewu_xenA"  
;She contributes small poems regularly to her school magazine.
;
;
