;@@@ Added by Anita--12-08-2014
;We now recapitulate the most important observations of the present section. [ncert]
;अब हम प्रस्तुत खण्ड में वर्णित महत्वपूर्ण तथ्यों को संक्षेप में दुहराते हैं  ।
(defrule recapitulate0
(declare (salience 4900))
(id-root ?id recapitulate)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 observation)
(kriyA-object  ?id ?id1)
(kriyA-aXikaraNavAcI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkRepa_meM_xuharA))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recapitulate.clp 	recapitulate0   "  ?id "  saMkRepa_meM xuharA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  recapitulate.clp 	recapitulate0   "  ?id1 "  ko )" crlf))
)

;@@@ Added by Anita--12-08-2014
;The entire symphony was recapitulated in the last four bars.
;पूरे सिम्फनी पिछले चार बार में  संक्षेप में दुहराई गई थी ।
(defrule recapitulate1
(declare (salience 4800))
(id-root ?id recapitulate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkRepa_meM_xuharA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recapitulate.clp 	recapitulate1   "  ?id "  saMkRepa_meM_xuharA_jA )" crlf))
)
;################################################default-rule#################################

;@@@ Added by Anita--12-08-2014
;I shall now recapitulate what the police have done in the matter. [Your Dictionary]
; पुलिस इस विषय में जो कर चुकी है अब मैं सार रूप में फिर से कहूँगा ।  
(defrule recapitulate_default-rule
(declare (salience 0))
(id-root ?id recapitulate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAra_rUpa_meM_Pira_se_kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recapitulate.clp 	recapitulate_default-rule   "  ?id "  sAra_rUpa_meM_Pira_se_kaha )" crlf))
)
