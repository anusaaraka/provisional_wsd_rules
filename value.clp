
(defrule value0
(declare (salience 5000))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value0   "  ?id "  mUlya )" crlf))
)

;"value","N","1.mUlya"
;The value of the rupee has gone up
;--"2.sixXAnwa_jinameM_samAja_kI_viSeRa_AsWA_ho"
;Youngsters have different set of values
;--"3.kisI_akRara_xvArA_praxarSiwa_rASi_kA_mUlya"
;Find the value of `x'
;

;$$$ Modified by 14anu-ban-01 on (04-02-2016)
;I value my privacy. 	[COCA]		;added by 14anu-ban-01 on (04-02-2016)
;मैं अपनी व्यक्तिगतता को महत्व देता हूँ.	[self]
(defrule value1
(declare (salience 4900))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))	;added by 14anu-ban-01 on (04-02-2016)
(assert (id-wsd_root_mng ?id mahawva_xe ))	;changed meaning from 'mola_lagA' to 'mahawva_xe' by 14anu-ban-01 on (04-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  value.clp 	value1   "  ?id " ko )" crlf)	;added by 14anu-ban-01 on (04-02-2016)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value1   "  ?id "  mahawva_xe )" crlf))	
)	;changed meaning from 'mola_lagA' to 'mahawva_xe' by 14anu-ban-01 on (04-02-2016)

(defrule value2
(declare (salience 4800))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mola_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value2   "  ?id "  mola_ho )" crlf))
)

;"value","V","1.mola_ho[lagA]"
;default_sense && category=verb	mola_lagA	0
;"value","VT","1.mola_lagAnA"
;He valued the car for rupees one lakh
;--"2.Axara_karanA"
;I value his friendship.
;


;@@@ Added by Prachi Rathore[6-3-14]
;The value of π = 3.1415926... is known to a large number of significant figures.[ncert]
;π = 3.1415926... का मान बहुत अधिक सार्थक अङ्कों तक ज्ञात है. ...
(defrule value3
(declare (salience 5500))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI  ?id ?id1)(viSeRya-as_saMbanXI  ?id ?id1))
(or(id-root ?id1 number|velocity)(id-cat_coarse ?id1 number))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value3   "  ?id "  mAna)" crlf))
)

;@@@ Added by 14anu-ban-07 (16-10-2014)
;Every calculated quantity which is based on measured values, also has an error.(ncert corpus)
;प्रत्येक परिकलित राशि, जो मापित मानों पर आधारित होती है, में भी कुछ त्रुटि होती है.(ncert corpus)
(defrule value4
(declare (salience 5500))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value4   "  ?id "  mAna)" crlf))
)

;@@@ Added by 14anu24
;Chaildcare is a well respected and valued career with dedicated people doing an expert job. 
;चाइल्डकेअर एक प्रतिष्ठित तथा सम्मानित पेशा है ऋसमें समर्पित लोग विशेष &amp; ता का काम करते हैं .
(defrule value5
(declare (salience 4800))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 career)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAniwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp    value5   "  ?id "  sammAniwa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-02-2016)
;I really value him as a friend.	[oald]
;मैं उसको मित्र की तरह बहुत मानता  हूँ . 	[self: translation can be improved]
(defrule value6
(declare (salience 4900))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value6   "  ?id "  mAna )" crlf)q)
)

;@@@ Added by 14anu-ban-01 on (04-02-2016)
;The area is valued for its vineyards.	[oald]
(defrule value7
(declare (salience 4900))
(id-root ?id value)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-for_saMbanXI  ?id ?id2)
(id-cat_coarse ?id verb)
(not(id-cat_coarse ?id2 number))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  value.clp 	value7   "  ?id "  jAnA_jA )" crlf)q)
)

