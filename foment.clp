
;@@@ Added by 14anu-ban-05 on (17-03-2015)
;They accused him of fomenting political unrest. [OALD]
;उन्होंने उसपर राजनैतिक अशान्ति उकसाने का आरोप लगाया . [manual]

(defrule foment0
(declare (salience 100))
(id-root ?id foment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foment.clp 	foment0   "  ?id "  ukasA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (17-03-2015)
;He was accused of fomenting violence. [merriam-webster]
;उसपर हिंसा भड़काने का आरोप लगाया गया था . 	[manual]

(defrule foment1
(declare (salience 101))
(id-root ?id foment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 violence|tension)			;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BadZakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foment.clp 	foment1   "  ?id "  BadZakA )" crlf))
)

