;@@@Added by 14anu-ban-02(03-03-2015)
;Sentence: Her parents seemed very amiable.[oald]
;Translation: उसके माँ बाप अत्यन्त मिलनसार प्रतीत हुए . [self]
(defrule amiable0 
(declare (salience 0)) 
(id-root ?id amiable) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id milanasAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amiable.clp  amiable0  "  ?id "  milanasAra )" crlf)) 
) 

;@@@Added by 14anu-ban-02(03-03-2015)
;She had an amiable conversation with her friend.[mw]
;उसका उसके मित्र के साथ मधुर वार्तालाप हुआ . [self]
(defrule amiable1 
(declare (salience 100)) 
(id-root ?id amiable) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 conversation)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id maXura)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amiable.clp  amiable1  "  ?id "  maXura )" crlf)) 
) 
