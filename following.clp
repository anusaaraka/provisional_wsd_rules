;Added by Pramila(Banasthali University)
;You can perform following activity at your home.
(defrule following0
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 activity|text|image)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnaliKiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following0   "  ?id "  nimnliKiwa )" crlf))
)

;Added by Pramila(Banasthali University)
;It is performed in the following event.
(defrule following1
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 event|incident)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following1   "  ?id "  agalA )" crlf))
)

;Added by Pramila(Banasthali University)
;The following is an extract from her diary.
(defrule following2
(declare (salience 4800))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following2   "  ?id "  agalA )" crlf))
)

;@@@ Added by 14anu20 dated 14/06/2014
;Following are rules of science.
;निम्नलिखित विज्ञान के नियम हैं . 
(defrule following02
(declare (salience 5800))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(subject-subject_samAnAXikaraNa  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnaliKiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following02   "  ?id "  nimnaliKiwa )" crlf))
)


;Added by Pramila(Banasthali University)
;The shop has a small but loyal following.
(defrule following3
(declare (salience 4800))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following3   "  ?id "  loga )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (29-11-2014)
;@@@ Added by (14anu11)
;You can help them by doing the following : Spell out words with fridge magnets , or letter tiles from word games .
;निम्नलिखित का अनुसरण करके आप उन की मदद कर सकते हैंः .
;छोटे बच्चों को बिन्दुओं को जोडते हुए अक्षरों का आकार बनाने की या आप के द्वारा पेन्सिल के आंके गये आकारों पर पेन चलाने की आदत डलवाएं .
(defrule following8
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa ?id ?id1) ;commmented by 14anu-ban-05 (29-11-2014)
(id-cat_coarse ?id noun)
;(or(viSeRya-viSeRaNa  8 9) 	;commented by 14anu-ban-05 (29-11-2014)
;(viSeRya-det_viSeRaNa  8 7)	;commented by 14anu-ban-05 (29-11-2014)
;(kriyA-object  6 8))		;commented by 14anu-ban-05 (29-11-2014)
(or(viSeRya-viSeRaNa  ?id ?) 	;modified by 14anu-ban-05 (29-11-2014)
(viSeRya-det_viSeRaNa  ?id ?)	 ;modified by 14anu-ban-05 (29-11-2014)
(kriyA-object  ? ?id))    ;modified by 14anu-ban-05 (29-11-2014)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnliKiwa))	;spelling modified by 14anu-ban-05  (29-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following8   "  ?id "  nimnliKiwa )" crlf))
)

;----------------------------------------- Default Rules ----------------------------------
;Added by Pramila(Banasthali University)
(defrule following4
(declare (salience 3000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following4   "  ?id "  loga )" crlf))
)

(defrule following5
(declare (salience 3000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following5   "  ?id "  agalA )" crlf))
)

(defrule following6
(declare (salience 3000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hone_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following6   "  ?id "  hone_para )" crlf))
)

;Added by Pramila(Banasthali University)
(defrule following7
(declare (salience 3000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnaliKiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  following.clp 	following7   "  ?id "  nimnliKitwa )" crlf))
)


;"following","Prep","1.hone_para"
;There was a major uprising following his arrest.
;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_following0
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 activity|text|image)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnaliKiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " following.clp   sub_samA_following0   "   ?id " nimnliKiwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_following0
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 activity|text|image)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimnaliKiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " following.clp   obj_samA_following0   "   ?id " nimnliKiwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_following1
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 event|incident)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " following.clp   sub_samA_following1   "   ?id " agalA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_following1
(declare (salience 5000))
(id-root ?id following)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 event|incident)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " following.clp   obj_samA_following1   "   ?id " agalA )" crlf))
)
