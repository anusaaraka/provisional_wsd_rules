(defrule invite0
(declare (salience 5000))
(id-root ?id invite)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
;(id-word ?id inviting )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AmaMwriwa_kara))
(assert (kriyA_id-object_viBakwi  ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invite.clp    invite0   "  ?id "  AmaMwriwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi  " ?*prov_dir* "  invite.clp    invite0   "  ?id " ko )" crlf)
)
)
; He made a mistake in inviting John.

;@@@ Added by 14anu-ban-06 (26-02-2015)
;The neighbours invited us in for coffee. (cambridge) [parser no. 4]
;पडोसियों ने कॉफी के लिए हमें आमन्त्रित किया . (manual)
(defrule invite3
(declare (salience 5100))
(id-root ?id invite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AmaMwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " invite.clp	invite3  "  ?id "  " ?id1 "  AmaMwriwa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-06 (26-02-2015)
;Let's invite some people over.(cambridge)
;चलिये हम कुछ लोगों को आमन्त्रित करें . (manual)
(defrule invite4
(declare (salience 5200))
(id-root ?id invite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AmaMwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " invite.clp	invite4  "  ?id "  " ?id1 "  AmaMwriwa_kara  )" crlf))
)


;---------------------- Default Rules -------------------

;$$$ Modified by 14anu-ban-06 (26-02-2015)
;Added by Meena(3.3.10)
;The 5 thousand people invited by Bob attended .
;बॉब के द्वारा आमन्त्रित किए गये 5 हजार लोग उपस्थित हुए . (manual);added by 14anu-ban-06 (26-02-2015)
(defrule invite1
(declare (salience 4500))
(id-root ?id invite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)	;added by 14anu-ban-06 (26-02-2015)
;(kriyA-by_saMbanXI  ?id ?id1)	;commented by 14anu-ban-06 (26-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AmaMwriwa_kara));meaning changed from 'AmaMwriwa' to 'AmaMwriwa_kara' by 14anu-ban-06 (26-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invite.clp    invite1   "  ?id "  AmaMwriwa_kara )" crlf))
)


;We wish to invite participants from all across the world to participate in this Yagna through this site.


;@@@ Added by 14anu-ban-06 (26-02-2015)
;I didn't get an invite to their wedding. (cambridge)
;मैंने उनके विवाहोत्सव पर निमन्त्रण प्राप्त नहीं किया था . (manual)
(defrule invite2
(declare (salience 0))
(id-root ?id invite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimanwraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invite.clp    invite2   "  ?id "  nimanwraNa )" crlf))
)
