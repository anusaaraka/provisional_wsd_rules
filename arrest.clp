;@@@Added by 14anu-ban-02(13-03-2015)
;They failed to arrest the company's decline.[oald]
;वे कम्पनी की गिरावट रोकने में असफल हुए . [self]
(defrule arrest2
(declare (salience 100))
(id-root ?id arrest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 decline)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rokanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arrest.clp 	arrest2   "  ?id "  rokanA )" crlf))
)


;@@@Added by 14anu-ban-02(13-03-2015)
;An unusual noise arrested his attention.[oald]
;असामान्य शोर ने उसका ध्यान खींचा .  [self]
(defrule arrest3
(declare (salience 100))
(id-root ?id arrest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 attention)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KIFca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arrest.clp 	arrest3   "  ?id "  KIFca )" crlf))
)

;@@@Added by 14anu-ban-02(13-03-2015)
;He arrested on the way to the hospital.[oald]
;अस्पताल जाने के रास्ते में उसका दिल धड़कना रुक गया .[self] 
(defrule arrest4
(declare (salience 100))
(id-root ?id arrest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) noun|pronoun )
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 hospital)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xila_XadZakanA_ruka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arrest.clp 	arrest4   "  ?id "  xila_XadZakanA_ruka_jA )" crlf))
)

;-------------------------------------------default_rules--------------------
;$$$Modified by 14anu-ban-02(13-03-2015)
;meaning changed from 'pakada' to 'giraPwAra_kara'
;She was arrested for drug-related offences.[oald]
;वह ड्रग सम्बन्धित अपराधों के लिये गिरफ्तार की गयी थी . [self]
;उसको drug-related अपराधों के लिए गिरफ्तार किया गया था . [self]	;modify by 14anu-ban-02(19-03-2015)
(defrule arrest0
(declare (salience 0))	;salience reduce to 0 from 5000 by  14anu-ban-02(13-03-2015)
(id-root ?id arrest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id giraPwAra_kara))
(assert (kriyA_id-subject_viBakwi ?id ko))	;commented by 14anu-ban-02(13-03-2015)	;uncommented by 14anu-ban-02(19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arrest.clp 	arrest0   "  ?id "  giraPwAra_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  arrest.clp    arrest0   "  ?id "  ko )" crlf)	;commented by 14anu-ban-02(13-03-2015)	;uncommented by 14anu-ban-02(19-03-2015)
)
)

(defrule arrest1
(declare (salience 0))	;salience reduce to 0 from 5000 by  14anu-ban-02(13-03-2015)
(id-root ?id arrest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id giraPwArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arrest.clp 	arrest1   "  ?id "  giraPwArI )" crlf))
)

;"arrest","N","1.giraPwArI"
;Increase of crime rate led to several arrests.
;--"2.avaroXa"
;He suffered a cardiac arrest.
;
;
