;$$$ Modified by 14anu-ban-03 (12-12-2014)
;The sand of this shore is coarse wherein aquatic plant Saawla is spread . [total tourism]
;yahAz ke sAgara wata kI rewa motI hE jisameM samuxrI pOXe sAvalA biKare padZe hEM . [total tourism]
;@@@ Added by 14anu13 on 01-07-14
(defrule coarse0
(declare (salience 1000))
(id-root ?id coarse)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  motA))  ;meaning changed from 'xAnexAra' to 'motA' by 14anu-ban-03 (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coarse.clp 	coarse0   "  ?id "  motA  )" crlf))
)


;@@@ Added by 14anu13 on 01-07-14
;And when the king disapproved of her doing so , she gave him an angry reply , and used coarse language towards him .
;और जब राजा ने उसके ऐसा करने की निंदा की तो उसने क्रुद्ध होकर बडी अभद्र भाषा का प्रयोग किया .
(defrule coarse1
(declare (salience 2000))
(id-root ?id coarse)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 	 ?id)
(id-root ?id1 language)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  aBaxra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coarse.clp 	coarse1   "  ?id "   aBaxra )" crlf))
)

