;@@@Added by 14anu-ban-02(22-04-2015)
;They were living in abysmal ignorance.[mw]
;वे अथाह अज्ञानता में रह रहे थे . [self]
(defrule abysmal1
(declare (salience 100))
(id-root ?id abysmal)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ignorance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aWAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abysmal.clp 	abysmal1   "  ?id "  aWAha )" crlf))
)


;-------------default_rules--------------------------------------

;@@@Added by 14anu-ban-02(22-04-2015)
;His manners are abysmal.[oald]
;उसके आचरण बहुत खराब हैं .[self] 
(defrule abysmal0
(declare (salience 0))
(id-root ?id abysmal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abysmal.clp 	abysmal0   "  ?id "  bahuwa_KarAba )" crlf))
)
