;@@@ Added by 14anu-ban-06 (17-04-2015)
;Hostilities began just after midnight. (cambridge)
;मध्यरात्री के ठीक बाद  झगडे शुरु हो गए  . (manual)
(defrule hostility1
(declare (salience 2000))
(id-root ?id hostility)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(kriyA-after_saMbanXI ?id1 ?id2)
(id-root ?id2 midnight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JagadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostility.clp 	hostility1   "  ?id "  JagadZA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (17-04-2015)
;There was open hostility between the two schools. (OALD)
;दो विद्यालयों के बीच खुली दुश्मनी थी . (manual)
(defrule hostility0
(declare (salience 0))
(id-root ?id hostility)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuSmanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostility.clp 	hostility0   "  ?id "  xuSmanI )" crlf))
)
