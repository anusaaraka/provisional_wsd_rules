;$$$ Modified by 14anu-ban-01 on (16-10-2014) [Corrected spelling and added example sentence]
;I saw Josh for just one second.[COCA]
;मैंने उसे सिर्फ एक सेकंड के लिये देखा.[self]
(defrule second0
(declare (salience 5000))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) one)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sekaMda));corrected spelling from 'sekaMxa' to 'sekaMda' by 14anu-ban-01 on (16-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second0   "  ?id "  sekaMda )" crlf))
)

;"second","Det","1.xUsarA"
;She is his second wife.
(defrule second2
(declare (salience 4800))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
 (or (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id )( id-cat_coarse ?id determiner)) ; Added by sukhada Ex: Can we open a second browser window.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second2   "  ?id "  xUsarA )" crlf))
)

;@@@Added by 14anu18 (18-06-14)
;The second method is better. 
;दूसरी प्रणाली अधिक बेहतर है . 
(defrule second6
(declare (salience 5000))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second6   "  ?id "  xUsarA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (09-02-2015)
;Dimensionally, we have (B) = (F / qv) and the unit of B are Newton second / (coulomb metre).[NCERT corpus]
;विमीय रीति से हम जानते हैं कि [B] = [F/qv] तथा B का मात्रक न्यूटन /कूलॉम मीटर है. [NCERT corpus]
(defrule second7
(declare (salience 5000))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sekaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second7   "  ?id "  sekaNda )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-02-2015)
;The limiting speed is about 11.2 kilometres per second.[second.clp]
;प्रतिबन्धक रफ्तार लगभग 11.2 किलोमीटर प्रति सेकण्ड  है .  [self]
(defrule second8
(declare (salience 5000))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-per_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sekaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second8   "  ?id "  sekaNda )" crlf))
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
;I thought the boats were going to collide, but one sheered off at the last second.[cald]
;मुझे लगा था नावें टकराने वाली हैं , परन्तु अन्तिम क्षण में एक झटके से मुड गयी . [self]
(defrule second9
(declare (salience 4800))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 last)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRaNa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second9  "  ?id "  kRaNa )" crlf))	
)

;@@@ Added by 14anu-ban-01 on (15-04-2015)
;The champion is leading by 18 seconds.  [oald]
;विजेता 18 सेकण्ड से आगे है.  [self]
(defrule second10
(declare (salience 5000))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sekaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second10   "  ?id "  sekaNda )" crlf))
)


;----------------------- Default Rules ------------------
;"second","Adj","1.xUsarA/xviwIya"
;He stood second in his class.
(defrule second1
(declare (salience 4900))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second1   "  ?id "  xUsarA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (09-02-2015)
;First, that it travels with enormous speed and second, that it travels in a straight line.[NCERT corpus]
;पहली, यह अत्यधिक तीव्र चाल से गमन करता है तथा, दूसरी, यह सरल रेखा में गमन करता है.[NCERT corpus]
;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir (05-01-14)
;Modified meaning 'kRaNa' as 'sekanda'
;The limiting speed is about 11SYMBOL-DOT2 kilometres per second. ### This sentence handled in second8
;"second","N","1.kRaNa"
;There are sixty seconds in a minute.
(defrule second3
(declare (salience 4700))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))	;changed "sekanda" to "xUsarA" by 14anu-ban-01 on (09-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second3   "  ?id "  xUsarA )" crlf))	;changed "sekanda" to "xUsarA" by 14anu-ban-01 on (09-02-2015)
)

;"second","Pron","1.xUsarA"
;She is his second wife.
(defrule second4
(declare (salience 4600))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second4   "  ?id "  xUsarA )" crlf))
)

;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir (05-01-14)
;Modified meaning 'samarWana_karara' as 'samarWana_kara'
;"second","V","1.samarWana_kararanA"
;She seconded the proposal of making the old man as the president.
(defrule second5
(declare (salience 4500))
(id-root ?id second)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  second.clp 	second5   "  ?id "  samarWana_kara )" crlf))
)



;"second","V","1.samarWana_kararanA"
;She seconded the proposal of making the old man as the president.
;--"2.sahArA_xenA"
;I was ably seconded in this research by my son.
;--"3.sWAnAnwariwa_karanA"
;The police officer was seconded from a town to a district head quarters.
;

;"second","N","1.kRaNa"
;There are sixty seconds in a minute.
;--"1.xviwIya_SreNI"
;I came to know that he has come second in B.A.
;

