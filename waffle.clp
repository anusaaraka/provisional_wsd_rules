
;@@@ Added by 14anu-ban-11 on (23-04-2015)
;Waffles go well with a hot cup of coffee.(hinkhoj) 
;मीठी रोटियाँ कॉफी के गरम कप के साथ अच्छा लगती हैं . (self)
(defrule waffle2
(declare (salience 20))
(id-root ?id waffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 go)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mITI_rotI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  waffle.clp 	waffle2  "  ?id "  mITI_rotI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-04-2015)
;I do not have a waffle iron.(hinkhoj)
;मेरे पास लोहे का तवा नहीं है .(self)
(defrule waffle3
(declare (salience 30))
(id-root ?id waffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 iron)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  waffle.clp 	waffle3   "  ?id "  wavA)" crlf))
)

;--------------------------------- Default Rules ---------------------------------------

;@@@ Added by 14anu-ban-11 on (23-04-2015)
;The report is just full of waffle.(oald)
;रिपोर्ट अस्पष्ट लेख से भरी हुई है . (self)
(defrule waffle0
(declare (salience 00))
(id-root ?id waffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta_leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  waffle.clp   waffle0   "  ?id "  aspaRta_leKa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-04-2015)
;If you don't know the answer, it's no good just waffling for pages and pages.(oald)
;यदि आपको उत्तर नहीं अाता हैं , तो यह अच्छी बात नही है केवल पृष्ठों और पृष्ठों पर अस्पष्ट बात लिखना. (self)
(defrule waffle1
(declare (salience 10))
(id-root ?id waffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta_bAwa_liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  waffle.clp   waffle1   "  ?id "  aspaRta_bAwa_liKa)" crlf))
)


