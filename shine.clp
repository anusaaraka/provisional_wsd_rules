
(defrule shine0
(declare (salience 5000))
(id-root ?id shine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shine.clp 	shine0   "  ?id "  camaka )" crlf))
)

;"shine","N","1.camaka"
;Give your shoes a good shine.
;
(defrule shine1
(declare (salience 4900))
(id-root ?id shine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shine.clp 	shine1   "  ?id "  camaka )" crlf))
)

(defrule shine2
(declare (salience 4800))
(id-root ?id shine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shine.clp 	shine2   "  ?id "  camakA )" crlf))
)

;"shine","V","1.camakanA"
;I like to watch the stars shining in the night sky.
;

;@@@ Added new rule by 14anu20 dated on 13/06/2014
;The Sun was shining bright.
;सूरज तेज चमक रहा था .

(defrule shine3
(declare (salience 5400))
(id-root ?id shine)
(id-word =(+ ?id 1) bright)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) weja_camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shine.clp  shine3  "  ?id "  " (+ ?id 1) "  weja_camaka  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu20 on 13/06/2014
;The Sun was shining down.
;सूरज डुब रहा था 
;सूरज डूब रहा था [Translation modified by 14anu-ban-01 on (30-12-2014)]
(defrule shine4
(declare (salience 5900))
(id-root ?id shine)
(id-word ?id1 down)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 dUba));changed "duba" to "dUba" and "?id" to "?id1" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shine.clp  shine4  "  ?id "  " ?id1 "  dUba  )" crlf));changed "duba" to "dUba" by 14anu-ban-01 on (30-12-2014)
)

