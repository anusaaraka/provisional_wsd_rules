;@@@ Added by 14anu-ban-11 on (22-04-2015)
;He took another bite of the wafer.(coca)
;उसने बिस्कुट का एक और कौर लिया . (self)
(defrule wafer2
(declare (salience 5001))
(id-root ?id wafer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 bite)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biskuta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wafer.clp 	wafer2   "  ?id "  biskuta )" crlf))
)

;@@@ Added by 14anu-ban-11 on (22-04-2015)
;Tewa wafer bread made with blue corn. (coca)
;Tewa ब्रेड के टुकडे नीले मकई के दाने से बनाऐ जाते है . (self)
(defrule wafer3
(declare (salience 5002))
(id-root ?id wafer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 bread)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wafer.clp 	wafer3   "  ?id "  tukadZA)" crlf))
)


(defrule wafer0
(declare (salience 5000))
(id-root ?id wafer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pApadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wafer.clp 	wafer0   "  ?id "  pApadZa )" crlf))
)

;"wafer","N","1.pApadZa"
;She loves potato wafers.
;
(defrule wafer1
(declare (salience 4900))
(id-root ?id wafer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tikiyA_se_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wafer.clp 	wafer1   "  ?id "  tikiyA_se_baMxa_kara )" crlf))
)

;"wafer","VT","1.tikiyA_se_baMxa_karanA"
