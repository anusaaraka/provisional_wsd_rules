
(defrule solace0
(declare (salience 5000))
(id-root ?id solace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAnwvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solace.clp 	solace0   "  ?id "  sAnwvanA )" crlf))
)

;"solace","N","1.sAnwvanA/upaSama"
;Every one can find solace in music.


;;"solace","V","1.sAnwvanA_xenA/xuHKa_kama_kara_xenA"
;He gives his parents solace while performing his duties.
(defrule solace1
(declare (salience 4900))
(id-root ?id solace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAnwvanA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solace.clp 	solace1   "  ?id "  sAnwvanA_xe )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-03-2015)
;She smiled, as though solaced by the memory. [oald]
;वह मुस्कराई, जैसे कि यादों ने उसे सन्तुष्ट कर दिया हो. [self]
(defrule solace2
(declare (salience 5000))
(id-root ?id solace)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 memory|mind|thought)
(kriyA-by_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwuRta_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solace.clp 	solace2   "  ?id "  sanwuRta_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-03-2015)
;When his wife left him, he found solace in the whisky.[cald]
;जब उसकी पत्नी उसको छोड कर चली गई, उसे मदिरा/शराब  में मन_का_ बहलाव/विनोद मिला .[self] 
(defrule solace3
(declare (salience 5100))
(id-root ?id solace)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 rum|whisky|beer|bottle)
(kriyA-in_saMbanXI  ?id1 ?id2)		
(kriyA-object  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mana_kA_bahalAva/vinoxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solace.clp 	solace3   "  ?id "  mana_kA_bahalAva/vinoxa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (11-03-2015)
;Music was a great solace to me.[cald]
;सङ्गीत मेरे लिए एक बडी राहत था . [self]
(defrule solace4
(declare (salience 5100))
(id-root ?id solace)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 music|art)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAhawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solace.clp 	solace4   "  ?id "  rAhawa )" crlf))
)

