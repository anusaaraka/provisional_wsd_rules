;@@@ Added by 14anu-ban-06 (12-03-2015)
;Because the train was delayed, all the coaches were completely jammed. (cambridge)[parser no. 3]
;क्योंकि रेलगाडी देरी से थी, सभी कोच पूरी तरह से ठसाठस भरे थे .(manual) 
(defrule jammed1
(declare (salience 2000))
(id-word ?id jammed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 coach)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TasATasa_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammed.clp 	jammed1   "  ?id "  TasATasa_BarA )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-03-2015)
;Hundreds more people were waiting outside the jammed stadium.(OALD)[parser no. 18]
;सैंकडों और लोग ठसाठस भरे स्टेडियम् के बाहर प्रतीक्षा कर रहे थे . (manual)
(defrule jammed2
(declare (salience 2500))
(id-word ?id jammed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 stadium)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TasATasa_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammed.clp 	jammed2   "  ?id "  TasATasa_BarA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (12-03-2015)
;This drawer is jammed.(cambridge)[parser no. 2]
;यह दराज जाम है . (manual)
(defrule jammed0
(declare (salience 0))
(id-word ?id jammed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammed.clp 	jammed0   "  ?id "  jAma )" crlf))
)

