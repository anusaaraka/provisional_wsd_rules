
(defrule title0
(declare (salience 5000))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id titled )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aBijAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  title.clp  	title0   "  ?id "  aBijAwa )" crlf))
)

;"titled","Adj","1.aBijAwa"
;He is a titled king.
;
(defrule title1
(declare (salience 4900))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title1   "  ?id "  nAma )" crlf))
)

;"title","N","1.nAma"
;He looked for books with the word `jazz' in the title.
;--"2.SIrRaka"
;The novel has a very catchy title.
;--"3.upAXi"
;He held the title for two years.
;`Your majesty' is the appropriate title to use in addressing a king.
;--"4.mAnopAXi"
;Professor didn't like his friends to use his formal title.
;--"5.siranAmA"
;The titles go by faster than I can read.
;
(defrule title2
(declare (salience 4800))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnopAXi_mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title2   "  ?id "  mAnopAXi_mila )" crlf))
)

;Write titles under pictures ( for example , dog , mummy , house ) to show them that words belong to things .
;यह दिखाने के लिए कि शब्दों का संबंध वस्तुओं से है , तस्वीरों के नीचे उन का शीर्षक / नाम लिखें ( उदाहरण के लिए , कुत्ता , मां , मकान ) .
;@@@ Added by avni(14anu11)
(defrule title3
(declare (salience 5000))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-under_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIRk))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title3   "  ?id "  SIRk )" crlf))
)
;"title","V","1.mAnopAXi_milanA"
;Ashoka was titled as'Ashoka the great'
;

;@@@Added by 14anu-ban-07,(16-02-2015)
;She will be defending her title at next month's championships.(oald )
;वह अगले महीने की  प्रतियोगिता में अपने  ख़िताब  को पुनः प्राप्त कर रही होगी .(manual)
(defrule title4
(declare (salience 5100))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 championship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZiwAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title4   "  ?id "  KZiwAba )" crlf))
)

;@@@Added by 14anu-ban-07,(16-02-2015)
;He claims he has title to the land. (oald)
;वह दावा करता है उसके पास भूमि का अधिकार है . (manual)
(defrule title5
(declare (salience 5200))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 land|property)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title5   "  ?id "  aXikAra )" crlf))
)

;@@@Added by 14anu-ban-07,(16-02-2015)
;Their first album was titled ‘Made in Valmez’.(cambridge)
;उनके पहले एलबम को 'मॆड इन् वल्मेज'नाम दिया था . (manual)
(defrule title6
(declare (salience 4900))
(id-root ?id title)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 album|book)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAma_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  title.clp 	title6   "  ?id "  nAma_xe )" crlf))
)
