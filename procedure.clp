
;----------------------------DEFAULT RULE------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (04-03-2015)
;The procedure for logging on to the network usually involves a password. 	[oald.com]
;नेटवर्क से सत्रारंभ करने की कार्यविधि अक्सर सङ्केत शब्द सम्मिलित करती है . 			[self] 
(defrule procedure0
(declare (salience 000))
(id-root ?id procedure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryaviXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  procedure.clp 	procedure0   "  ?id "  kAryaviXi )" crlf))
)

;----------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (04-03-2015)
;We repeat our defining procedure.	[NCERT CORPUS]
;उपरोक्त विवरण में आए पदों को व्युत्पन्न करने में हमने जिस पद्धति का प्रयोग किया है उसको दोहराते हैं.	[NCERT CORPUS]
;हम हमारी परिभाषित पद्धति को दोहराते हैं.		[self]
(defrule procedure1
(declare (salience 1000))
(id-root ?id procedure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(kriyA-object  ?id1 ?id)
(id-root ?id1 repeat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxXawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  procedure.clp 	procedure1   "  ?id "  paxXawi )" crlf))
)
