
(defrule mind0
(declare (salience 5000))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id minded )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ximAga_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mind.clp  	mind0   "  ?id "  ximAga_vAlA )" crlf))
)

;"minded","Adj","1.ximAga vAlA"
;He is a keen minded person.
;


;$$$ Modified by Bhagyashri (15-06-2016).
;$$$ Modified by 14anu-ban-08 (25-02-2015)  ;added constraint
;For a man of 80, he has a remarkably agile mind.  [cald]
;80 साल की उम्र में आदमी का दिमाग आसाधरण रूप से बहुत तेज हैं.  [self]
;Added by Meena(25.1.11)
;The letter was clearly the product of a twisted mind.
(defrule mind1
(declare (salience 4800))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 twist|brilliant|good|keen|creative|evil|suspicious|criminal|conscious|subconscious|agile|sharp|great|fast)     ;added 'agile' by 14anu-ban-08 (25-02-2015). ;Added 'sharp', 'fast' and 'great' by Bhagyashri (15-06-2016).
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp      mind1   "  ?id "  ximAga )" crlf))
)



;Added by human beings
(defrule mind2
(declare (salience 4900))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 make )
(viSeRya-up_saMbanXI ?id1 ?id) ;Replaced viSeRya-up_viSeRaNa as viSeRya-up_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp 	mind2   "  ?id "  - )" crlf))
)

; he made up his mind to tell a story.


;Salience reduced by Meena(25.1.11)
(defrule mind3
(declare (salience 0))
;(declare (salience 4800))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp 	mind3   "  ?id "  mana )" crlf))
)




(defrule mind4
(declare (salience 4700))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Apawwi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp 	mind4   "  ?id "  Apawwi_kara )" crlf))
)





(defrule mind5
(declare (salience 4600))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Apawwi_ho))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp 	mind5   "  ?id "  Apawwi_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  mind.clp      mind5   "  ?id " ko  )" crlf))
)

;"mind","V","1.Apawwi_honA[karanA]"
;He does not mind the noise.
;He does not mind if I borrow his books.
;I do not mind him smoking occasionally.
;--"2.XyAna_xenA"
;Mind your language.
;Don't mind his rudeness.
;Would you mind the baby for an hour.
;
;

;$$$Modified by 14anu-ban-08 (09-01-2015)      
;meaning changed from 'bnAne' to 'banA'
;@@@ Added by 14anu11
;मुझे उम्मीद तो है कि अब , जब कि हिंदुस्तान को आजादी मिलने वाली है और हिंदुस्तान में विज्ञान भी जमाने के साथ आगे बढ रहा है , इस नये हिंदुस्तान की दिक्कतों को तेजी से , सभी क्षेत्रों में 
;योजनाबद्ध तरीके से विकास कर हल करने की कोशिश की जायेगी और उसके नजरिये को भी ज्यादा से ज्यादा वैज्ञानिक बनाने की कोशिश की जायेगी .
;I do hope that now , when India is on the verge of independence and science in India too is coming of age , it will try to solve the
;problems of the new India by rapid , planned development on all sectors and try to make her more and more scientifically minded. 
(defrule mind6
(declare (salience 4700))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))           ;changed meaning from 'bnAne' to 'banA' by 14anu-ban-08 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mind.clp 	mind6   "  ?id "  banA )" crlf)                                            ;changed meaning from 'bnAne' to 'banA' by 14anu-ban-08 
))

;$$$Modified by 14anu-ban-08 (17-02-2015)    ;added fast to the list
;His mind clicks very fast.  [click.clp file]
;उसका दिमाग अत्यन्त तेज चलता है .   [self]
;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_mind1
(declare (salience 4800))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 twist|brilliant|good|keen|creative|evil|suspicious|criminal|conscious|subconscious|fast)     ;added 'fast' by 14anu-ban-08(17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " mind.clp   sub_samA_mind1   "   ?id " ximAga )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_mind1
(declare (salience 4800))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 twist|brilliant|good|keen|creative|evil|suspicious|criminal|conscious|subconscious)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " mind.clp   obj_samA_mind1   "   ?id " ximAga )" crlf))
)

;@@@ Added by Bhagyashri Kulkarni (17-06-16)
;Mind your own business.
;अपने काम से काम रखो
(defrule mind7
(declare (salience 4500))
(id-root ?id mind)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 own)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  mind.clp  	mind7   "  ?id " kAma_raKa  )" crlf))
)
