
(defrule previous0
(declare (salience 5000))
(id-root ?id previous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  previous.clp 	previous0   "  ?id "  piCalA )" crlf))
)

(defrule previous1
(declare (salience 4900))
(id-root ?id previous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvavarwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  previous.clp 	previous1   "  ?id "  pUrvavarwI )" crlf))
)

;"previous","Adj","1.pUrvavarwI"
;The students were asked to complete all of the exercises of the previous 
;lessons before starting a new one.
;
;

;@@@ Added by 14anu-ban-09 on (09-10-2014)
;However, if you apply force to a lump of putty or mud, they have no gross tendency to regain their previous shape, they have no gross tendency to regain their previous shape, and they get permanently deformed. [NCERT CORPUS]
;jaba eka pafka piMda para bala lagAwe hEM wo piMdaka meM apanA prAramBika AkAra prApwa karane kI pravqwwi nahIM howI hE Ora yaha sWAyI rUpa se virUpiwa ho jAwA hE. [NCERT CORPUS]

(defrule previous2
(declare (salience 5000))
(id-root ?id previous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 shape)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAraMBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  previous.clp 	previous2   "  ?id "  prAraMBika )" crlf))
)

