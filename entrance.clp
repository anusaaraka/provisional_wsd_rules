;@@@ Added by 14anu-ban-04 (29-01-2015)
;I’ll meet you at the main entrance.                     [oald]
;मैं आपसे मुख्य द्वार पर मिलूँगा . 	                                [self]
(defrule entrance2
(declare (salience 20))
(id-root ?id entrance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 main|rear|back)
=>        
(retract ?mng)
(assert (id-wsd_root_mng ?id xvAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrance.clp 	entrance2   "  ?id "  xvAra)" crlf))
)

;NOTE:[THERE IS A PARSER PROBLEM IN THIS SENTENCE SO IT RUNS ON PARSE NO. 3]
;@@@ Added by 14anu-ban-04 (29-01-2015)
;I was entranced by the bird’s beauty.                       [olad]
;मैं चिड़ियों की सुंदरता से मोहित हो गया था.                              [self]
(defrule entrance3
(declare (salience 20))
(id-root ?id entrance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive)
(kriyA-by_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mohiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrance.clp 	entrance3  "  ?id "  mohiwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (29-01-2015)
;The entrance of Nagaland is connected with several major cities of the country through Dimapur road , rail and airways .   [tourism-corpus]
;नागालैंड  का  प्रवेशद्वार  दीमापुर  सड़क  ,  रेल  और  हवाई  मार्ग  से  देश  के  कई  प्रमुख  शहरों  से  जुड़ा  है  ।                                  [tourism-corpus]
(defrule entrance4
(declare (salience 30))
(id-root ?id entrance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praveSaxvAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrance.clp 	entrance4   "  ?id "  praveSaxvAra )" crlf))
)


;------------------------------------------------------DEFAULT RULES -----------------------------------------------------------------------



;NOTE:[THERE IS A PARSER PROBLEM IN THIS SENTENCE SO IT RUNS ON PARSE NO. 3]
;@@@ Added by 14anu-ban-04 (29-01-2015)
;He has entranced millions of people with his beautifully illustrated books.           [cald]
;उसने लाखों लोगों को अपनी  सुंदर ढंग से सचित्र पुस्तकों से सम्मोहित किया है.                                    [self]
(defrule entrance1
(declare (salience 10))
(id-root ?id entrance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammohiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))                ;added by 14anu-ban-04 on (09-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  entrance.clp     entrance1   "  ?id " ko  )" crlf)                                ;added by 14anu-ban-04 on (09-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrance.clp 	entrance1  "  ?id "  sammohiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (29-01-2015)
;They were refused entrance to the club.                  [oald]
;उनको क्लब में प्रवेश नहीं दिया गया था .                               [self]
(defrule entrance0
(declare (salience 10))
(id-root ?id entrance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praveSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrance.clp 	entrance0   "  ?id "  praveSa )" crlf))
)

