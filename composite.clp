;;@@@ Added by 14anu-ban-05 on (10-09-2015)
;The subconscious mind is a composite of everything one sees, hears and any information the mind collects that it cannot otherwise consciously process to make meaningful sense. [wikipedia]
;अवचेतन मस्तिष्क उन सभी चीजों का संयोजन है जो हम देखते है, सुनते है और कोई जानकारी मन संरचित करता है जिसे यह अन्यथा जाग्रित अवस्था में अर्थपूर्ण संवेदना बनाने के लिये तैयार नहीं कर सकता है.[manual]

(defrule composite2
(declare (salience 5001))
(id-root ?id composite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyojana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  composite.clp 	composite2   "  ?id "  saMyojana )" crlf))
)

;-------------------- Default Rules -------------------------


(defrule composite0
(declare (salience 5000))
(id-root ?id composite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMraciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  composite.clp 	composite0   "  ?id "  saMraciwa )" crlf))
)

;"composite","Adj","1.sammiSra"
;Aluminium is a composite metal.
(defrule composite1
(declare (salience 4900))
(id-root ?id composite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiSra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  composite.clp 	composite1   "  ?id "  sammiSra )" crlf))
)

