
(defrule prove0
(declare (salience 5000))
(id-root ?id prove)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id proven )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sixXa_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  prove.clp  	prove0   "  ?id "  sixXa_huA )" crlf))
)

;"proven","Adj","1.sixXa huA"
;Dr.S.Radhakrishnan was a personality of proven capability.
;
(defrule prove1
(declare (salience 4900))
(id-root ?id prove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sixXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prove.clp 	prove1   "  ?id "  sixXa_kara )" crlf))
)

;default_sense && category=verb	sixXa kara	0
;"prove","V","1.sixXa karanA"
;You should prove your point by logical argumentation rather than raising your voice.
;
;

;@@@ Added by 14anu-ban-09 on (07-11-2014)
;Peanuts are a significant source of resveratrol, a chemical associated with but not proven to cause a reduction in risk of cardiovascular disease and cancer. [Agriculture]
;मूँगफली एक रसायनिक द्रव्य से युक्त रेस्वरट्रॊल का एक महत्वपूर्ण स्रोत हैं परन्तु यह हृद्वाहिनी रोग और कैंसर के खतरे में कमी का कारण सिद्ध नहीं हुआ हैं. [Self] 

(defrule prove2
(declare (salience 4900))
(id-root ?id prove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-root ?id1 cause)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sixXa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prove.clp 	prove2   "  ?id "  sixXa_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-03-2015)    ;NOTE-Run on parser no. 3.
;The game proved to be a dour struggle, with both men determined to win. [oald]
;दोनों पुरुषों के जीतने  के  दृढ़ संकल्प से यह खेल  एक कठोर  संघर्ष साबित हुआ।		[Manual]

(defrule prove3
(declare (salience 4900))
(id-root ?id prove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-root ?id1 struggle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAbiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prove.clp 	prove3   "  ?id "  sAbiwa_ho )" crlf))
)
