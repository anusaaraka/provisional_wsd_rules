;$$$ Modified by 14anu-ban-01 on (30-12-2014) -- meaning changed from wejI to weja
;Modifications done by 14anu23 are not required.Original rule was correct: by 14anu-ban-01 on (30-12-2014)
;$$$ Modified by 14anu23 --- meaning changed from weja to wejI
;Added by Meena(8.02.10)
;We noticed a sharp increase in unemployment in the last few years:In this example also,sharp0 should fire and it is firing.I did not get proper example for this already existing rule.Here, 'sharp' will be noun and occur with another noun-->by 14anu-ban-01 on (30-12-2014)
(defrule sharp00
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id);(viSeRya-viSeRaNa ?id1 ?id) removed 'viSeRya-viSeRaNa' relation by 14anu-ban-01 on (30-12-2014) because sharp0 was not firing because of this condition
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp00   "  ?id "  weja )" crlf))
)



;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;$$$ Modified by 14anu23 on 26/6/14 
; A sharp knife. एक तेज चाकू . 
;Salience reduced by Meena(8.02.10)
(defrule sharp0
;(declare (salience 5000))
(declare (salience 0))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id weja)) ; meaning changed from wejI to weja
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp0   "  ?id "  wEja )" crlf));changed "wejI" to "weja" by 14anu-ban-01 on (30-12-2014)
)

;@@@ Added by 14anu-ban-01 on (15-10-2014)
;A sharp needle when pressed against our skin pierces it.[NCERT corpus]
;जब एक नुकीली सुई हमारी त्वचा में दाब लगाकर रखी जाती है, तो वह त्वचा को बेध देती है. [NCERT corpus]
(defrule sharp1
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 needle|nail)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sharp.clp       sharp1   "  ?id "  nukIlA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
;The consultants were a group of men in sharp suits. 
;सलाहकार सजीला सूटों में आदमियों का समूह थे .   
(defrule sharp2
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)	;commented by 14anu-ban-01 on (30-12-2014)
(viSeRya-viSeRaNa  ?id1 ?id)	; added '?id1' by 14anu-ban-01 on (30-12-2014)
(id-root ?id1 jacket|suit|cloth); changed  =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sajIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp2   "  ?id "  sajIlA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
;A sharp note . 
;एक ऊँचा स्वर .  
(defrule sharp3
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)	;commented by 14anu-ban-01 on (30-12-2014)
(viSeRya-viSeRaNa  ?id1 ?id)	; added '?id1' by 14anu-ban-01 on (30-12-2014)
(id-root ?id1 note|voice|pitch)	; changed  =(+ ?id 1) as ?id1 and added 'pitch' to the list by 14anu-ban-01 on (30-12-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp3   "  ?id "  UzcA )" crlf))
)


;@@@ Added by 14anu23 on 26/6/14
;A sharp photographic image.
;एक सुस्पष्ट फोटोग्राफी सम्बन्धी प्रतिमा . 
;एक सुस्पष्ट फोटोग्राफी सम्बन्धी प्रतिबिंब .[Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule sharp4
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 photo|image|pic|landscape|portrait)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id suspaRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp4   "  ?id "  suspaRta )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
; At three o'clock sharp. 
; तीन पर बजे सटीक. 
;ठीक तीन बजे[Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule sharp5
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 AM|PM|o'clock)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id TIka));changed meaning from "satIka" to "TIka" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp5   "  ?id "  TIka )" crlf));changed meaning from "satIka" to "TIka" by 14anu-ban-01 on (30-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
;He kept a sharp lookout for any strangers. 
;उसने किसी अजनबी के लिए एक पैनी चौकी रखी .
;उसने अजनबियों के लिए पैनी चौकसी रखी[Translation improved by 14anu-ban-01 on (30-12-2014)]  
(defrule sharp6
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 lookout|see|view|perceive|scrutinize|sight|look)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pEnA));changed "pEnI" to "pEnA" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp6   "  ?id "  pEnA)" crlf));changed "pEnI" to "pEnA" by 14anu-ban-01 on (30-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
;A sharp criticism. 
;एक तीखी आलोचना .
;A sharp turn:Example taken from sharp0 by 14anu-ban-01 on (30-12-2014)
;एक तीखा मोड .[sharp.clp] 
(defrule sharp7
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 criticism|review|notice|excoriation|turn);added "turn" by 14anu-ban-01 on (30-12-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wIKA ));changed "wIKI" to "wIKA" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp7   "  ?id "  wIKA)" crlf));changed "wIKI" to "wIKA" by 14anu-ban-01 on (30-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu23 on 26/6/14
;The firm had to face some sharp practice from competing companies. 
;व्यापारिक कम्पनी को मुकाबला करती हुईं कम्पनियों से कुछ बेईमान अभ्यास का सामना करना पडा . 
;उस व्यापारिक कम्पनी को मुकाबला करती हुईं कम्पनियों से कुछ कानूनी किन्तु अनैतिक व्यवहार का सामना करना पडा .[14anu-ban-01 on (30-12-2014):Suggested by Chaitanya Sir]
(defrule sharp8
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 practice);changed =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
=>
(retract ?mng)
;(assert (id-wsd_word_mng ?id beImAna )) ;commented by 14anu-ban-01 on (30-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1   kAnUnI_kinwu_anEwika_vyavahAra)) ;added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sharp.clp       sharp8 "  ?id "  " ?id1 "  kAnUnI_kinwu_anEwika_vyavahAra  )" crlf))
);added by 14anu-ban-01 on (30-12-2014)


;@@@ Added by 14anu-ban-01 on (30-12-2014)
;We need to give young criminals a sharp shock:Example taken from sharp00
;हमें युवा/तरुण अपराधियों को एक गहरा सदमा देने की जरूरत है . [self]
(defrule sharp9
(declare (salience 5100))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 shock)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp9   "  ?id "  gaharA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;There is a sharp decline in the rate of interest.[shiksharthi ]
; ब्याज की दर में भारी गिरावट आई है.[self]
(defrule sharp10
(declare (salience 5000))
(id-root ?id sharp)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 decline|drop|fall)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sharp.clp       sharp10   "  ?id "  BArI)" crlf))
)



;There is a sharp rise in crime .
;Added by Veena Bagga (22-01-10)
