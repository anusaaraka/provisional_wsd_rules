;$$$ Modified by 14anu-ban-08 (20-02-2015)   ;added relation and constraint,salience increased
;@@@ Added by 14anu-ban-03 Yashwini Dhaka (19-07-2014)
;Insulin promotes the movement of glucose into cells. [COCA]
;insulina koSikAoM ke anxara glUkoja ke pravAha ko baDAva xewA hEM.
(defrule movement0
(declare (salience 5101))    ;salience increased by 14anu-ban-08 (20-02-2015)
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)       ;added by 14anu-ban-08 (20-02-2015)
(id-root ?id1 glucose)                ;added by 14anu-ban-08 (20-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " movement.clp movement0 " ?id " pravAha )" crlf))
)

;$$$Modified by 14anu-ban-08 (26-11-2014)      ;added 'national' in the list
;He led the national movement.
;उसने राष्ट्रिय आन्दोलन को मार्ग दिखाया .
;@@@ Added by 14anu-ban-03 Yashwini Dhaka (21-07-2014)
;Martyr Mangal Pandey who ignited the fire of freedom movement in 1857 was the product of this very soil . [parallel corpus]
;1857 me svawanwrawA AMxolana kI Aga sulagAne vAle ShIxa maMgala pANde isI mittI kI uwpawwi We.
(defrule movement1
(declare (salience 5000))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1) (viSeRya-viSeRaNa ?id ?id1))
(id-word ?id1 freedom|non-coorperation|child-rearing|national)                 ;constraint is added by 14anu-ban-08 (26-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMxolana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " movement.clp movement1 " ?id " AMxolana)" crlf))
)

;$$$Modified by 14anu-ban-08 (10-01-2015)   ;constraint is added
;@@@ Added by 14anu-ban-06 (28-7-14)
;There is movement of pilgrims at the pilgrimages all year round .  (Parallel Corpus)
;wIrWayAwrA para pUre sAla Bara wIrWayAwriyoM kA AvAgamana rahawA hE.
(defrule movement2
(declare (salience 5350))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) ;added ?id1 by 14anu-ban-08 (10-01-2015)
(id-root ?id1 pilgrim)               ;added by 14anu-ban-08 (10-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvAgamana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement2   "  ?id "  AvAgamana )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-10-2014)
;This movement of the axis of the top around the vertical is termed precession. [NCERT CORPUS]
;UrXvAXara ke pariwaH lattU kI akRa kA isa prakAra GUmanA purassaraNa kahalAwA hE. [NCERT CORPUS]

(defrule movement3
(declare (salience 5350))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 axis)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement3   "  ?id "  GumanA )" crlf))
)

;Remove this rule because this rule is firing from movement02 by 14anu-ban-08 (10-01-2015)
;@@@Added by 14anu07 0n 02/07/2014
;It is a national movement to help thyroid sufferers.
;यह थाइराइड के मरीज़ो की सहायता करने लिए एक राष्ट्रिय आन्दोलन है . 
;(defrule movement4
;(declare (salience 5000))
;(id-root ?id movement)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) to)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id AMxolana))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "   movement.clp  	 movement4   "  ?id "  AMxolana)" crlf))
;)

;Commented by Maha Laxmi --- Movement0 and  movement7 are same rules with different action
;;@@@Added by 14anu07 0n 04/07/2014
;;The movements of the firm are questionable.
;;व्यापारिक कम्पनी की गतिविधि सन्देहयुक्त हैं .
;(defrule movement7
;(declare (salience 4500))
;(id-root ?id movement)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id  gawiviXi))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "   movement.clp  	 movement7   "  ?id "   gawiviXi
;)" crlf))
;)

;@@@Added by 14anu07 0n 04/07/2014
;Her body movements are nice.
;उसके शरीरिक चलन अच्छे हैं . 
(defrule movement5
(declare (salience 5200))       ;Salience is increased from 4900 to 5200 by 14anu-ban-08 (10-01-2015)
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) body)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  calana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "   movement.clp  	 movement5   "  ?id "   calana
)" crlf))
)

;@@@Added by 14anu07 0n 04/07/2014
; The movements of her body are nice.
;उसके शरीर के चलन अच्छे हैं 
(defrule movement6
(declare (salience 5351))     ;Salience is increased from 5350 to 5351 by 14anu-ban-08 (10-01-2015) 
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 body) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  calana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "   movement.clp  	 movement6	" ?id "   calana
)" crlf))
)

;$$$ Modified by 14anu-ban-08 (20-02-2015)   
;If the movement is anticlockwise, the resultant is towards you.  [NCERT]
;यदि गति वामावर्त है तो परिणामी आपकी ओर सङ्केत करेगा.  [NCERT]
;@@@ Added by 14anu09[14-6-14]
;DEFAULT RULE
(defrule movement7
(declare (salience 500))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)               ;changed the category from ' adjective' to 'noun' by 14anu-ban-08 (20-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement7   "  ?id "  gawi )" crlf))
)


;@@@ Added by 14anu09[14-6-14]
;It is a national movement to help thyroid sufferers.          ;example is added by 14anu-ban-08 (10-01-2015) 
;यह थाइराइड के मरीज़ो की सहायता करने लिए एक राष्ट्रिय आन्दोलन है . 
;He led the national movement.
;उसने राष्ट्रिय आन्दोलन को मार्ग दिखाया .
;The movement came to a standstill.         ;remove this unwanted example it doesn't show relation with this rule by 14anu-ban-08 (10-01-2015)
;आन्दोलन ठहराव को आया .
(defrule movement02
(declare (salience 5000))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-word ?id1 national)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMxolana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement02   "  ?id "  AMxolana )" crlf))
)


;@@@added by 14anu09[14-6-14]
;The movement of the ball annoyed him.
;गोले की गतिविधि ने उसको परेशान किया . 
(defrule movement03
(declare (salience 5100))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id ?id1)(viSeRya-in_saMbanXI ?id ?id1))
;(id-word ?id1 ball|)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawiviXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement03   "  ?id "  gawiviXi )" crlf))
)

;@@@ added by 14anu09[14-6-14]
;The troop's movement was stopped.	 
;दल की गतिविधि रुकी गयी थी . 
(defrule movement8
(declare (salience 5200))            ;Salience is increased from 5100 to 5200 by 14anu-ban-08 (10-01-2015)
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) every|little|gentle|sudden|slow|drastic|army|troop|enemy)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawiviXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement8   "  ?id "  gawiviXi )" crlf))
)



;$$$Modified by 14anu-ban-08 (26-11-2014)
;###[Counter Example]###The troop's movement was stopped.	 [same clp file]
;###[Counter Example]###दल की गतिविधि रुकी गयी थी .
;@@@ added by 14anu09[14-6-14]
;They watched his movements.
;उन्होंने उसकी गतिविधि को देखा . 
(defrule movement9
(declare (salience 5100))
(id-root ?id movement)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
(id-root ?id1 his)                         ;constraint added by 14anu-ban-08 (26-11-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawiviXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  movement.clp 	movement9   "  ?id "  gawiviXi )" crlf))
)


