
;@@@ Added by 14anu-ban-11 on (09-02-2015)
;Turn the soil over with a spade.(oald)
;फावडे से  मिट्टी  को ऊपर  उठाइये . (self)
(defrule spade0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id spade)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PAvadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spade.clp   spade0   "  ?id "  PAvadA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (09-02-2015)
;You must play a spade if you have one.(oald) 
;आपको हुकुम का पत्ता डालना चाहिए यदि आपके पास है . (self)
(defrule spade1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id spade)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hukuma_kA_pawwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spade.clp   spade1   "  ?id "  hukuma_kA_pawwA )" crlf))
)





