;Added by sheetal(25-02-10)
;Buying of shares was brisk on Wall Street today .
(defrule brisk0
(declare (salience 5000))
(id-root ?id brisk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 share)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  brisk.clp     brisk0   "  ?id "  weja )" crlf))
)

;@@@Added by 14anu-ban-02(15-01-2015)
;His tone became brisk and businesslike.(oald)
;उसकी आवाज विश्वासपूर्ण और व्यावहारिक हो गयी.(manual)
(defrule brisk3
(declare (salience 100))
(id-root ?id brisk)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ? ?id ?id1)
(id-root ?id1 businesslike)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSvAsapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  brisk.clp     brisk3   "  ?id "  viSvAsapUrNa )" crlf))
)

;------------------------ Default rules --------------------
;@@@ Added  by 14anu01 on 23-06-2014
;They brisk up her pace. 
;
(defrule brisk1
(declare (salience 6000))
(id-root ?id brisk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  brisk.clp     brisk1   "  ?id "  wejZa_kara )" crlf))
)

;$$$ Modified by 14anu-ban-02(15-01-2015)
; Changed meaning 'PurwIlA' as 'weja'
;@@@ Added  by 14anu01 on 23-06-2014
;A good brisk walk. 
;एक अच्छा तेज चाल .
(defrule brisk2
(declare (salience 5000))
(id-root ?id brisk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  brisk.clp     brisk2   "  ?id "   weja)" crlf))
)

