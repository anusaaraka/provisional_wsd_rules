
(defrule stuff0
(declare (salience 5000))
(id-root ?id stuff)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmagrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stuff.clp 	stuff0   "  ?id "  sAmagrI )" crlf))
)
;$$$$$$
;Modified by jagriti(11.12.2013)......default meaning modified from TUzsanA to BaranA
(defrule stuff1
(declare (salience 4900))
(id-root ?id stuff)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stuff.clp 	stuff1   "  ?id "  Bara )" crlf))
)

;"stuff","V","1.TUzsanA"
;He stuffed the bag with his books.
;
;

;@@@ Added by 14anu-ban-11 on (05-03-2015)
;I don't believe in all that stuff about ghosts. (oald)
;मैं भूतों के बारे में उस सभी कार्यक्रम में नहीं मानता हूँ . (self)
(defrule stuff2
(declare (salience 5001))
(id-root ?id stuff)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 believe)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryakrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stuff.clp 	stuff2   "  ?id "  kAryakrama)" crlf))
)


