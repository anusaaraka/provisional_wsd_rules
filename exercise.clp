;$$$ Modified by 14anu-ban-04 (22-01-2015)               ------changed meaning from 'WA' to 'pada'
;@@@ Added by 14anu11
;Sri Aurobindo has exercised a deep influence on educated Hindus . 
;श्री अरविंद का शिक्षित हिंदुओं पर गहरा प्रभाव था .
;श्री अरविंद का शिक्षित हिंदुओं पर गहरा प्रभाव पड़ा है .          ;translation corrected by 14anu-ban-04 (22-01-2015) 
(defrule exercise2
(declare (salience 5000))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 influence)                ;added by 14anu-ban-04 (22-01-2015) 
;(kriyA-subject  ?id ?id2)                 ;commented by 14anu-ban-04 (22-01-2015)    
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pada))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise2   "  ?id "  pada)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  exercise.clp    exercise2    "  ?id " kA )" crlf))
)

;@@@ Added by 14anu-ban-04 (05-02-2015)  
;The Court of Appeal exercised its jurisdiction to order a review of the case.             [oald]
;कोर्ट ऑफ अपील  ने मामले के पुनरावलोकन के लिए अपने न्याय अधिकार का प्रयोग करते हुए आदेश दिया .                      [self]
(defrule exercise3
(declare (salience 5000))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 jurisdiction)                
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  exercise.clp    exercise3    "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise3  "  ?id "  prayoga_kara)" crlf))
)

;@@@ Added by 14anu-ban-04 (05-02-2015)  
;An improper exercise of a discretionary power.                         [oald]
;एक विवेकाधिकार का अनुचित प्रयोग .                                             [self]
(defrule exercise4
(declare (salience 5010))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise4   "  ?id "  prayoga )" crlf))
)


;@@@ Added by 14anu-ban-04 (05-02-2015) 
;It would be a useful exercise for you to say the speech aloud several times.                     [cald]  
;बार-बार जोर से भाषण बोलना आप के लिए एक उपयोगी अभ्यास  होगा .      [self]  ;translation modified by 14anu-ban-04 on (06-02-2015)
(defrule exercise5
(declare (salience 5010))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(saMjFA-to_kqxanwa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise5   "  ?id "  aByAsa )" crlf))
)

;@@@ Added by 14anu-ban-04 (05-02-2015) 
;I am much exercised by her silence.                        [same clp file]
;mEM usakI cuppI se ciMwA meM padZa jAwA hUz.               [same clp file]
(defrule exercise6
(declare (salience 4910))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 silence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciMwA_meM_pada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise6   "  ?id "   ciMwA_meM_ pada )" crlf))
)

;----------------------- Default rules-----------------------------------------------------------------------------------------------------

(defrule exercise0
(declare (salience 5000))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAyAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise0   "  ?id "  vyAyAma )" crlf))
)

;"exercise","N","1.vyAyAma"
;We must do exercises regularly.
;--"2.aByAsa"
;I couldn't do the last exercise of my maths book.
;
(defrule exercise1
(declare (salience 4900))
(id-root ?id exercise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAyAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exercise.clp 	exercise1   "  ?id "  vyAyAma_kara )" crlf))
)

;"exercise","V","1.vyAyAma_karanA"
;We must exercise regularly.
;
;LEVEL 
;Headword : exercise
;
;Examples --
;
;"exercise","N","1.aByAsa" 
;I couldn't do the last exercise on translation.
;mEM anuvAxavAlA aMwima aByAsa nahIM kara pAyA. 
;--"2.vyAyAma/kasarawa"
;You ought to do muscle-exercises every morning. 
;wumhe rojZa savere vyAyAma karanA cAhie.
;
;"exercise","VT","1.vyAyAma karanA"  
;He exercises in the gymnasium. 
;vaha vyAyAmaSAlA meM vyAyAma karawA hE.
;--"2.kasarawa karanA"
;This mathematical problem is exercising our minds. 
;yaha aMkagaNiwa kI samasyA hamAre ximAga kI kasarawa karavA rahI hE. <---maswiRka se vyAyAma karavAnA
;--"3.aByAsa_meM_lAnA"
;Parents should exercise enough authority on their children.
;mAwA-piwA ko apane baccoM para saKwI aByAsa meM lAnA cAhiye.
;--"4.ciMwA meM dAlanA"
;I am much exercised by her silence. 
;mEM usakI cuppI se ciMwA meM padZa gayI hUz.<--cuppI kA kAraNa jAnane ke liye ximAga kI kasarawa karanA 
;
;ukwa uxAharaNoM se 'exercise' kA mUla arWa 'aByAsa' nikala kara AwA hE. 'aByAsa' se vyAyAma judZa jAwA hE. awaH isakA sUwra hogA -
;
;
;  sUwra : aByAsa^vyAyAma
