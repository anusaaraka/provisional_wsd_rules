;##############################################################################
;#  Copyright (C) 2014-201 Amit kumar jha(amitjha823@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02149, USA.
;
;##############################################################################


;@@@ Added by 14anu23 23/06/2014
(defrule ordinary0
(declare (salience 4000))
(id-root ?id ordinary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saba_ke_prayoga_kI_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordinary.clp 	ordinary0   "  ?id "  saba_ke_prayoga_kI_vaswu )" crlf))
)


;@@@ Added by 14anu23 23/06/2014
;Jeffrey Steenson is Canon to the Ordinary in the Episcopal diocese of the Rio Grande. 
(defrule ordinary1
(declare (salience 4001))
(id-root ?id ordinary)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI   ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnIya_XarmAXyakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordinary.clp 	ordinary1   "  ?id "  sWAnIya_XarmAXyakRa )" crlf))
)


;@@@ Added by 14anu23 23/06/2014
(defrule ordinary2
(declare (salience 4000))
(id-root ?id ordinary)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXAraNa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordinary.clp 	ordinary2   "  ?id " sAXAraNa   )" crlf))
)


;---------------------DEFAULT RULE---------------------------------------------------------
;@@@ Added by 14anu-ban-09 on (9-9-14)
;The meal was very ordinary. [OALD]
;Bojana kAPI svABAvika WA. [Own Manual]
;भोजन अत्यन्त स्वाभाविक था . [Anusaaraka Output]
(defrule ordinary00
(id-root ?id ordinary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svABAvika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordinary.clp 	ordinary00  "  ?id " 	svABAvika )" crlf))
)
;--------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on (9-9-14)
;Ordinary people like you and me. [OALD]
;sAmAnya loga ApakI Ora merI waraha. [Own Manual]
(defrule ordinary01
(declare (salience 5000))
(id-root ?id ordinary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 person) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmAnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordinary.clp 	ordinary01  "  ?id " sAmAnya )" crlf))
)
