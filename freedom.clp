;@@@ Added by 14anu-ban-05 on (16-04-2015)
;Enjoy the freedom of the outdoors .[OALD]
;बाहरी दुनिया की स्वच्छंदता का आनंन्द  उठाइए . [MANUAL]
(defrule freedom1
(declare (salience 101))
(id-root ?id freedom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 outdoors)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svacCaMxawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freedom.clp 	freedom1   "  ?id "  svacCaMxawA  )" crlf))
)

;--------------------------------------- Default Rules ----------------------------

;@@@ Added by 14anu-ban-05 on (16-04-2015)
;As a society we value freedom and privacy.[OALD]
;एक समाज के रूप में हम स्वतंत्रता और गोपनीयता को महत्व देते हैं.  [MANUAL]
(defrule freedom0
(declare (salience 100))
(id-root ?id freedom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svawaMwrawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freedom.clp  freedom0   "  ?id "  svawaMwrawA )" crlf))
)


