import sys, string
import re

def tsplit(string, delimiters):
    """Behaves str.split but supports multiple delimiters."""
    
    delimiters = tuple(delimiters)
    stack = [string,]
    
    for delimiter in delimiters:
        for i, substring in enumerate(stack):
            substack = substring.split(delimiter)
            stack.pop(i)
            for j, _substring in enumerate(substack):
                stack.insert(i+j, _substring)
            
    return stack

tmp = []
tmp1 = []
printout_stmt = []
printout_stmt1 = []
rule_name = []
dq = '\"'
dq_cnt = 0
assert_stmt_count = 0
print_stmt_count = 0
f = open(sys.argv[1],"r")
for line in f:
        ##previous_line = tmp
        ##tmp = line
        if line.strip().startswith('(defrule'):
	   tmp1 = tsplit(line,('(',')',' ','\t','\n'))
           tmp1 = filter(None, tmp1)
           rule_name = tmp1[1]
        if line.strip().startswith('(assert'):
           assert_stmt_count = assert_stmt_count + 1;
        if line.strip().startswith('(printout wsd_fp "(dir_name-file_name-rule_name'):
           printout_stmt = line
           printout_stmt = printout_stmt[:printout_stmt.find(";")-1]
           printout_stmt1 = printout_stmt
	   dq_cnt = 0
           while dq in printout_stmt:
                dq_cnt=dq_cnt+1;
                
                ind = printout_stmt.find("\"")
                ind1 = printout_stmt[printout_stmt.find("\"")+1:].find("\"")
#                print "dq_cnt = %d  ind = %d  " % ( dq_cnt, ind)
                if (ind1 == -1):
                   break
                elif (dq_cnt == 1):
                   printout_stmt = printout_stmt[printout_stmt.find("\"")+1:]
                elif (dq_cnt%2 == 0 and (printout_stmt[ind-1] == " " or printout_stmt[ind-1] == "\t")):
                   printout_stmt = printout_stmt[printout_stmt.find("\"")+1:]
                elif (dq_cnt%2 == 1 and (printout_stmt[ind+1] == " " or printout_stmt[ind+1] == "\t")):
                   printout_stmt = printout_stmt[printout_stmt.find("\"")+1:]
                else:
                   print "error near double quote in printout statement :: Space missing before/after \" or \" invalid at  { %s } in rule %s " % (printout_stmt.strip(), rule_name)
                   break
           if(printout_stmt1.find("=") != -1):
                 print "Remove \"=\" for ids from printout statement in rule %s" % (rule_name) 
           print_stmt_count = print_stmt_count + 1;
	   tmp1 = tsplit(line,('(',')',' ','\t','"'))
           tmp1 = filter(None, tmp1)
           if (tmp1[0] != "printout"):
              print "Mistake in print statement ", rule_name 
           if (tmp1[1] != "wsd_fp"):
              print "Wrong file pointer", rule_name 
           if (tmp1[2] not in ['dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng','dir_name-file_name-rule_name-id-wsd_root_mng','dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng','dir_name-file_name-rule_name-id-wsd_word_mng','dir_name-file_name-rule_name-kriyA_id-object2_viBakwi','dir_name-file_name-rule_name-kriyA_id-object_viBakwi','dir_name-file_name-rule_name-kriyA_id-subject_viBakwi','dir_name-file_name-rule_name-make_verbal_noun','dir_name-file_name-rule_name-id-H_tam_mng','dir_name-file_name-rule_name-id-H_vib_mng','dir_name-file_name-rule_name-kriyA_id-object1_viBakwi','dir_name-file_name-rule_name-id-wsd_number','dir_name-file_name-rule_name-id-wsd_viBakwi','dir_name-file_name-rule_name-id-preceeding_part_of_verb','dir_name-file_name-rule_name-id-wsd_root','dir_name-file_name-rule_name-id-attach_emphatic','dir_name-file_name-rule_name-root_id-TAM-vachan','dir_name-file_name-rule_name-id-eng_src','dir_name-file_name-rule_name-id-domain_type','dir_name-file_name-rule_name-id-tam_type']):
              print "Mistake in printout statment", rule_name 
           if (tmp1[3] != "?*prov_dir*"):
              print "Error in defglobal variable", rule_name 
           if (tmp1[4] != sys.argv[1]):
              print "Wrong file name in rule ", rule_name 
           if (tmp1[5] != rule_name):
              print "Wrong rule name in rule ", rule_name 
        if  line.strip().startswith(')'):
            if (assert_stmt_count != print_stmt_count) :
               print "count of printout and assert statements differ in rule " ,rule_name
            assert_stmt_count = 0
	    print_stmt_count = 0
        

f.close()
tmp2 = []
tmp3 = []
tmp4 = []
a = [] 
main_list = []
prep_word_list = []
i=0
j=0
flag=0
f = open(sys.argv[1],"r")
for line in f:
        if line.strip().startswith('(defrule'):
           tmp1 = line.split(' ')
           rule_name = tmp1[1].strip('\n')
           for line in f:
                if line.startswith(';'):
                   continue
                if line.startswith('=>'):
	           break 
                if "(meaning_to_be_decided" in line:
                   tmp3 =line.split('(')
                   tmp1 = tmp3[1].split(' ')
                   tmp1[1] = tmp1[1].strip()
                   main_id = tmp1[1].replace(')','')
                   main_list.append(main_id)
                else:
                    if " |" in line or "| " in line:
                        print "Space error at \"|\" symbol in rule" , rule_name 
                    a = tsplit(line,('(',')',' ','&'))
                    tmp2.append(a)
           for i in range(len(tmp2)):
               for j in range(len(tmp2[i])):
                   if tmp2[i][j] == ';':
                      continue
                      tmp2[i][j] = tmp2[i][j].strip()
                      tmp2[i][j] = tmp2[i][j].replace(')','')
#           print tmp2 ,"+++" 

           for i in range(len(tmp2)):
               for j in range(len(tmp2[i])):
                    if not set(tmp2[i]).isdisjoint(main_list):
                       if (tmp2[i][j].startswith('?') and len(tmp2[i][j])>1):
                          if tmp2[i][j] not in  main_list:
                             main_list.append(tmp2[i][j])
#           for i in range(len(tmp2)):
#               for j in range(len(tmp2[i])):
#                    if not set(tmp2[i]).isdisjoint(main_list):
#                       if (tmp2[i][j].startswith('?') and len(tmp2[i][j])>1):
#                          if tmp2[i][j] not in  main_list:
#                             main_list.append(tmp2[i][j])

           for i in range(len(tmp2)):
               for j in range(len(tmp2[i])):
                   if "_saMbanXI" in tmp2[i][j] :
#                     print  tmp2[i][j][tmp2[i][j].index('-')+1:tmp2[i][j].index('_')]
                      prep_word_list.append(tmp2[i][j][tmp2[i][j].index('-')+1:tmp2[i][j].index('_')])
               	      for j in range(len(tmp2[i])):
                               #print tmp2[i][j]
	                       if (tmp2[i][j].startswith('?') and len(tmp2[i][j])>1):
        	                  if tmp2[i][j] not in  main_list:
                	             main_list.append(tmp2[i][j])
                      continue

           for i in range(len(tmp2)):
               for j in range(len(tmp2[i])):
                    if not set(tmp2[i]).isdisjoint(main_list):
                       if (tmp2[i][j].startswith('?') and len(tmp2[i][j])>1):
                          if tmp2[i][j] not in  main_list:
                             main_list.append(tmp2[i][j])

 
#           print main_list ,"+++" 
#           print  prep_word_list,"+++" 
           
           for i in range(len(tmp2)):
               for j in range(len(tmp2[i])):
                   if (tmp2[i][j].startswith('?') and len(tmp2[i][j])>1):
                       if tmp2[i][j] not in  main_list:
                          if tmp2[i][j+1] not in prep_word_list: 
                             print "Connectivity missing between the ids in rule ::::: " ,rule_name 
                             print main_list , tmp2[i][j]


           #print ':::::::::::::\n'
           tmp2 = []
           main_list = []
