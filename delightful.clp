;@@@ Added by 14anu-ban-04 (14-03-2015)
;Our new neighbours are delightful.                      [oald]
;हमारे नये पड़ोसी दिलचस्प हैं .                  [manual]                    
(defrule delightful1
(declare (salience 20))
(id-root ?id delightful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilacaspa))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delightful.clp 	delightful1  "  ?id " xilacaspa )" crlf))
)

;@@@ Added by 14anu-ban-04 (14-03-2015)
;Trekking here is in itself a dangerous and delightful experience .              [tourism-corpus]
;यहाँ पर ट्रैकिंग करना अपने आप में एक जोखिमपूर्ण एवं सुखद अनुभव है ।                                                                            [tourism-corpus]             
(defrule delightful2
(declare (salience 20))
(id-root ?id delightful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1  experience) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delightful.clp 	delightful2   "  ?id " suKaxa )" crlf))
)


;@@@ Added by 14anu-ban-04 (14-03-2015)
;Although one can come to Nainital throughout the year , but the weather here is very delightful in summers .        [tourism-corpus]      
;वैसे  वर्ष  भर  नैनीताल  आया  जा  सकता  है  ,  लेकिन  गरमियों  में  यहाँ  का  मौसम  बहुत  सुहावना  होता  है  ।              [tourism-corpus] 
(defrule delightful3
(declare (salience 30))
(id-root ?id delightful)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 weather)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suhAvanA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delightful.clp 	delightful3   "  ?id " suhAvanA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (14-03-2015)
;Because this is a protected forest beings area therefore coming here with the object of coming only for natural variance is the most delightful accomplishment .                  [tourism-corpus]        
;क्योंकि यह एक आरक्षित वन्य जीव क्षेत्र है अतः यहाँ पर केवल प्राकृतिक विचरण के उद्देश्य से आना ही सर्वाधिक आनंददायक उपलब्धि है ।              [tourism-corpus]      
(defrule delightful0
(declare (salience 10))
(id-root ?id delightful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AnanxaxAyaka)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delightful.clp 	delightful0   "  ?id " AnanxaxAyaka )" crlf))
)
