
(defrule mate0
(declare (salience 5000))
(id-root ?id mate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id mating )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id subaha_kI_prArWanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mate.clp  	mate0   "  ?id "  subaha_kI_prArWanA )" crlf))
)

;"mating","N","1.subaha kI prArWanA"
;My friend Rosy attends matins every saturday.
;
(defrule mate1
(declare (salience 4900))
(id-root ?id mate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mate.clp 	mate1   "  ?id "  sAWI )" crlf))
)

;"mate","N","1.sAWI"
;He's off for a drink with his mates.
;--"2.jodZa"
;The bird seems to have lost his mate.
;
;


;$$$ Modified by 14anu-ban-08 (12-12-2014)           ;changed meaning from 'saMBoga karanA' to 'saMBoga_kara'  
;@@@ Added by 14anu06(Vivek Agarwal) on 21/6/2014*********
;Some animals mate during spring.
;कुछ जानवर वसंत के दौरान संभोग करते हैं.
(defrule mate2
(declare (salience 5000))
(id-root ?id mate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBoga_kara))     ;changed meaning from 'saMBoga karanA' to 'saMBoga_kara' by 14anu-ban-08 (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mate.clp 	mate2   "  ?id "  saMBoga_kara )" crlf))                       ;changed meaning from 'saMBoga karanA' to 'saMBoga_kara' by 14anu-ban-08 (12-12-2014)
)
