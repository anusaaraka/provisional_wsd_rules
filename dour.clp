;@@@ Added by 14anu-ban-04 (23-03-2015)
;The game proved to be a dour struggle, with both men determined to win.          [oald]   ;run this sentence on parse no. 3
;दोनों पुरुषों के जीतने  के  दृढ़ संकल्प से यह खेल  एक कठोर  संघर्ष साबित हुआ।                                                                     [self]
(defrule dour1
(declare (salience 20))
(id-root ?id dour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 struggle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dour.clp    dour1  "  ?id "  kaTora)" crlf))
)


;@@@ Added by 14anu-ban-04 (23-03-2015)
;He was a striking figure with a long, dour face.                    [oald]
;एक लम्बा, गम्भीर चेहरे के साथ उसका एक ध्यान आकर्षित करनेवाला  रूप था .                 [self]
(defrule dour2
(declare (salience 20))
(id-root ?id dour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMBIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dour.clp    dour2  "  ?id "  gaMBIra)" crlf))
)

;-------------------------------------------------------DEFAULT RULE ----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (23-03-2015)
;He was a dour middle-aged man.                                   [oald]
;वह एक नीरस प्रौढ आदमी था .                                            [self]
(defrule dour0
(declare (salience 10))
(id-root ?id dour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIrasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dour.clp    dour0  "  ?id "  nIrasa)" crlf))
)
