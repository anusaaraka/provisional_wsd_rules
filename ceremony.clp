;$$$ Modified by 14anu-ban-03 (10-12-2014)
;Added by Meena(26.02.10)
;Actress Whoopi Goldberg and singer Michael Jackson attended the ceremony .
(defrule ceremony0
(declare (salience 00))  ;salience reduced from 4900 to 00 by 14anu-ban-03 (10-12-2014)
(id-root ?id ceremony)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id ?id1)  ;commented by 14anu-ban-03 (10-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAroha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ceremony.clp      ceremony0   "  ?id "  samAroha )" crlf)
)
)


;@@@ Added by 14anu23 19/06/2014
; To leave without ceremony. 
;बिना शिष्टाचार  के  छोड़कर चले  अाना .
(defrule ceremony1
(declare (salience 4900))
(id-root ?id ceremony)
?mng <-(meaning_to_be_decided ?id)
(kriyA-without_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRtAcAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ceremony.clp      ceremony1   "  ?id "  SiRtAcAra )" crlf)
)
)

