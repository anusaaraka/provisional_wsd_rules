
;@@@ Added by 14anu-ban-03 (30-03-2015)
;Thieves conned him out of his life savings. [cald]
;चोरों ने उसकी जीवन बचत को ठग लिया . [manual]
(defrule conned1
(declare (salience 10))
(id-word ?id conned)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Taga_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  conned.clp 	conned1  "  ?id "  Taga_le )" crlf))
)

;@@@ Added by 14anu-ban-03 (30-03-2015)
;He conned his way into the job using false references. [oald]
;उसने झूठे सन्द्र्भ का उपयोग करते हुए उसके काम को छल से पा लिया .  [manual]
(defrule conned2
(declare (salience 10))
(id-word ?id conned)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 job)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Cala_se_pA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  conned.clp 	conned2  "  ?id "  Cala_se_pA_le )" crlf))
)

;--------------------------------- Default rules -----------------------------------

;@@@ Added by 14anu-ban-03 (30-03-2015)
;I was conned into buying a useless car. [oald]
;मुझे एक व्यर्थ की गाडी खरीदने मे धोखा मिला था .  [manual]
(defrule conned0
(declare (salience 00))
(id-word ?id conned)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XoKA_mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  conned.clp   conned0   "  ?id "  XoKA_mila )" crlf))
)


