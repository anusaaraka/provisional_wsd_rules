;@@@ Added by Jagriti(3.4.2014)
;On his orders a surprise search was conducted by Tehsildaars and other departmental officers on Thursday.
;उनके निर्देश पर तहसीलदारों के साथ-साथ अन्य अधिकारियों ने वीरवार को कई विभागों के कार्यालयों में औचक छापेमारी की।
(defrule surprise4
(declare (salience 5100))
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 visit|search)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ocaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surprise.clp 	surprise4   "  ?id "  Ocaka )" crlf))
)

;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu27 on 28/6/2014***********
;By setting up the ANC , it seeks to reduce the likelihood of surprises on its eastern flank .
;सो , अंडमान - निकोबार कमान की स्थापना करके वह अपनी पूर्वी सीमा को किसी अचानक घटनाक्रम से सुरक्षित रखना चाहता है .
;अंडमान-निकोबार कमाण्ड की स्थापना करके वह अपनी पूर्वी सीमा पर हो सकने वाली अप्रत्याशित घटनाओं की  सम्भावना को कम करने का प्रयास करता है.[Translation improved by 14anu-ban-01 on (12-01-2015)]
(defrule surprise31
(declare (salience 5000));salience increased from 4500 to 5000 by 14anu-ban-01
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?id1)
(id-root ?id1 flank|coast|ghat);added by 14anu-ban-01 on (12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aprawyASiwa_GatanA));changed "acAnaka" to "aprawyASiwa_GatanA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surprise.clp 	surprise31   "  ?id " aprawyASiwa_GatanA  )" crlf))
)

(defrule surprise0
(declare (salience 5000))
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id surprising )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id AScaryajanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  surprise.clp  	surprise0   "  ?id "  AScaryajanaka )" crlf))
)

;"surprising","Adj","1.AScaryajanaka"
;Tajmahal is very surprising.
;
(defrule surprise1
(declare (salience 4900))
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AScarya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surprise.clp 	surprise1   "  ?id "  AScarya )" crlf))
)

(defrule surprise2
(declare (salience 4700))
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AScaryacakiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surprise.clp 	surprise2   "  ?id "  AScaryacakiwa_ho )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (07-02-2015)
;"surprise","V","1.AScaryacakiwa karanA"
;He surprised everyone by his behaviour at the party.[same clp file]
(defrule surprise3
(declare (salience 4800))
(id-root ?id surprise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AScaryacakiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))	;added by 14anu-ban-01 on (07-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surprise.clp 	surprise3   "  ?id "  AScaryacakiwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  surprise.clp 	surprise3   "  ?id " ko )" crlf)	;added by 14anu-ban-01 on (07-02-2015)
)
)

