;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################


;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
;She scribbled his phone number on a scrap of paper.
;उसने कागज के टुकडे पर उसका टेलीफोन नम्बर लिखा.
(defrule scribble1
(declare (salience 3500))
(id-root ?id scribble)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scribble.clp 	scribble1   "  ?id "  liKa )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
;There was a scribbled message on the back of the ticket.
;टिकट के पीछे एक अस्पष्ट सन्देश था.
(defrule scribble2
(declare (salience 3500))
(id-root ?id scribble)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scribble.clp 	scribble2   "  ?id "  aspaRta )" crlf))
)

;-----DEFAULT RULE-----
;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
(defrule scribble0
(declare (salience 2000))
(id-root ?id scribble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_course ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GasIta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scribble.clp 	scribble0   "  ?id "  GasIta )" crlf))
)
