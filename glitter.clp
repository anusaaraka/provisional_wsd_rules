;@@@ Added by 14anu-ban-05 on (20-01-2015)
;He was attracted by the glitter of Hollywood.[cambridge]
;वह हॉलीवुड की चमक-दमक से आकर्षित हो गया था।	              [manual]
(defrule glitter4
(declare (salience 4801))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka-xamaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter4   "  ?id "  camaka-xamaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-01-2015)
;Famous as the city of Nawabs , in every corner of Lucknow glamour and glitter of Nawabs can be seen .[TOURISM]
;navAboM ke Sahara ke nAma se maSahUra laKanaU ke cappe - cappe para navAbI SAno - SOkawa kI CApa xeKI jA sakawI hE .[TOURISM]
(defrule glitter5
(declare (salience 4802))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(conjunction-components  ?c ?id1 ?id)
(id-word ?id1 glamour )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?c ?id SAno-SOkawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " glitter.clp  glitter5  "  ?id1 "  " ?c " " ?id "  SAno-SOkawa  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-01-2015)
;In Deepavali even night glitters like the day .[tourism]
;xIpAvalI meM rAwa BI xina jEsI jagamagAwI hE .[tourism]
;In Deepavali the whole of the house is seen glittering with the light of diyas .[tourism]
;xIpAvalI meM pUrA Gara xIyoM kI roSanI se jagamagAwA najara AwA hE .[tourism]
(defrule glitter6
(declare (salience 4803))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 night|whole)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jagamagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter6   "  ?id "  jagamagA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-01-2015)
;A different kind of happiness glitters on the face of every human of Gujarat .[tourism]
;gujarAwa ke hareka mAnava ke cehare para eka alaga hI KuSI JalakawI hE .[tourism]
(defrule glitter7
(declare (salience 4803))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter7   "  ?id "  Jalaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-01-2015)
;Kadamat , Kalpani Minsoy , beautiful blue lakes of Bangaram island of this coast seem as if sapphire is glittering on gems .[tourism]
;isa wata kI kaxAmawa , kAlapAnI minsAya , baMgArAma xvIpa kI suMxara nIlI JIleM Ese lagawI hEM mAno mANika para nIlama JilamilA rahA ho .[tourism]
(defrule glitter8
(declare (salience 4804))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JilamilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter8   "  ?id "  JilamilA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-01-2015)
;Parser problem- showing 'glitter' as verb instead of adjective   ;running correctly on 2nd parser
;The glittering water of Naukuchia Taal will appear as if a valuable gem studded on the breast of the earth is shimmering .[tourism]
;nOkuciyA wAla kA JilamilAwA huA pAnI EsA lagegA , jEse XarawI kI CAwI para jadZA koI kImawI nagInA JilamilA rahA ho [tourism]
(defrule glitter9
(declare (salience 5001))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 water|star)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JilamilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter9   "  ?id "  JilamilA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (21-01-2015)
;The glitter of Dashahra festival during October in Mysore is worth seeing .[tourism]
;mEsUra meM aktUbara ke samaya xaSaharA wyOhAra kI rOnaka xeKane lAyaka howI hE .[tourism]
;The glitter of Water Sports Complex in the evening time is worth seeing .[tourism]
;SAma ke samaya vAtara sportZsa koYmpalekZsa kI rOnaka xeKane yogya howI hE .[tourism]
(defrule glitter10
(declare (salience 4804))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 see)	;more constraints can be added
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rOnaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter10   "  ?id " rOnaka )" crlf))
)


;@@@ Added by 14anu-ban-05 on (21-02-2015)
;He has a glittering career ahead of him.	[OALD]
;उसके आगे एक शानदार कैरियर है.			[manual]
;Squalor and poverty lay behind the city's glittering facade.	[OALD]
;गंदगी और गरीबी शहर के शानदार मुखौटे के पीछे निहित है.	[manual]
(defrule glitter11
(declare (salience 4805))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id gerund_or_present_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 career|facade)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter11   "  ?id " SAnaxAra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-02-2015)
;The awards were given at a glittering ceremony in Rashtrapati Bhavan.[from glitter.clp]
;पुरस्कार राष्ट्रपति भवन में एक भव्य समारोह में दिया गया.  				[manual]

(defrule glitter12
(declare (salience 5001))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id gerund_or_present_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ceremony)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bavya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter12   "  ?id " Bavya )" crlf))
)

;------------------- Default rules -------------------
(defrule glitter0
(declare (salience 5000))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id glittering )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id camakIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  glitter.clp  	glitter0   "  ?id "  camakIlA )" crlf))
)

(defrule glitter1
(declare (salience 4900))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id glittering )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id camakawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  glitter.clp  	glitter1   "  ?id "  camakawA_huA )" crlf))
)

;"glittering","Adj","1.camakawA_huA"
;He gifted me a glittering pen on my birthday.
;--"2.SAnaxAra"
;The awards were given at a glittering ceremony in Rashtrapati Bhavan.
;
;
(defrule glitter2
(declare (salience 4800))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter2   "  ?id "  camaka )" crlf))
)

;"glitter","N","1.camaka"
;The glitter of gold attracts everyone.
;
(defrule glitter3
(declare (salience 4700))
(id-root ?id glitter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glitter.clp 	glitter3   "  ?id "  camaka )" crlf))
)

;"glitter","V","1.camakanA"
;All that glitters is not gold.
;
