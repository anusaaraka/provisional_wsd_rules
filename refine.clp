(defrule refine0
(declare (salience 5000))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id refined )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nirmala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  refine.clp  	refine0   "  ?id "  nirmala )" crlf))
)

;"refined","Adj","1.nirmala"
;He is a man of refined tastes in dance && music.
;
(defrule refine1
(declare (salience 4900))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refine.clp 	refine1   "  ?id "  nirmala_kara )" crlf))
)

;"refine","VTI","1.nirmala_karanA"
;The process of refining oil is tedious.
;--"2.suXAranA"   
;We need to refine our old techniques of production. 
;
;
;@@@ Added by 14anu-ban-10 on (5-8-14) ;(parellel corpus)
;Since Cavendish's experiment, the measurement of G has been refined and the currently accepted value is.  
;kEvendiSa prayoga ke bAxa @G ke mApana meM pariRkaraNa hue waWA aba @G kA pracaliwa mAna isa prakAra hE. 
(defrule refine2
(declare (salience 5020))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ?id ?id1)(kriyA-since_saMbanXI ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariRkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refine.clp 	refine2  "  ?id "  pariRkaraNa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-8-14)  ;(parellel corpus)
;A number of insects do just this and they have actually refined this art ! ;(parellel corpus)
;afKa kIdoM kA sirPa yaha karaWA hE ora ve yaha kalA vAsWava meM nikAra cuke hEM!
(defrule refine3
(declare (salience 5400))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1);(kriyA-subject ?id ?id2))
(id-root ?id1 art)
;(id-root ?id2 dance|music)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niKAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refine.clp 	refine3  "  ?id " niKAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-8-14)       ;(parellel corpus)
;Indian Music and Dance which its own style is much refined and is very popular .
;भारत में संगीत तथा नृत्य की अपनी शैलियां भी विकसित हुईं जो बहुत ही लोकप्रिय हैं ।
;BArata me sagIwa waWA nqwya kI apanI SEliyAM BI niKAra huI jo bahuwa hI lokapriya hEM.
;A number of insects do just this and they have actually refined this art !
;afKa kIdoM kA sirPa yaha karaWA hE ora ve yaha kalA vAsWava meM nikAra cuke hEM!
(defrule refine5
(declare (salience 5400))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id2)
(id-root ?id2 dance|music)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niKAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refine.clp 	refine5  "  ?id " niKAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-8-14)
;but that got refined pretty soon , later .      ;(parellel corpus)
;लेकिन बाद में जल्दी ही बेहतर हो जायेगा
(defrule refine4
(declare (salience 5300))
(id-root ?id refine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behawara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  refine.clp 	refine4  "  ?id " behawara )" crlf))
)


