
(defrule link0
(declare (salience 5000))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link0   "  ?id "  kadI )" crlf))
)

;"link","N","1.kadI"
;Links of a chain. 
;She provides a link between the two groups.
;
(defrule link1
(declare (salience 4900))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadI_joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link1   "  ?id "  kadI_joda )" crlf))
)

;"link","V","1.kadI_jodanA"
;Link the two pieces of the chains.
;Linking the evidence is a difficult job.
;

;@@@--- Added by Nandini(29-4-14)
;We offer advice to Polish companies who want to link up with Western businesses.
;हम पोलिश कम्पनियों को सलाह देते हैं जो यूरोपियन व्यापार से अपने को जोड़ना चाहती हैं .
(defrule link2
(declare (salience 4950))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga ?id ?id1)
(or(kriyA-with_saMbanXI  ?id ?id2)(kriyA-from_saMbanXI  ?id ?id2))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jodaZnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " link.clp	link2  "  ?id "  " ?id1 "  jodaZnA  )" crlf))
)


;@@@--- Added by Nandini (29-4-14)
;The department is interested in developing closer links with industry.[OLAD}
;viBAga uxyoga ke sAWa aXika vikAsa kA majabUwa sambanXa sWApiwa karane meM ruci raKawA hE.
(defrule link3
(declare (salience 5050))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 develop)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link3   "  ?id "  sambanXa )" crlf))
)

;@@@--- Added by Nandini (29-4-14)
;The video cameras are linked to a powerful computer.[OLAD]
(defrule link4
(declare (salience 4950))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judaZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link4   "  ?id " judaZ )" crlf))
)


;@@@ Added by 14anu-ban-08 (08-10-2014)
;(See for example this incident in which a person inserted a fake biography linking a prominent journalist to the Kennedy assassinations
;and Soviet Russia as a joke on a co-worker which went undetected for four months,saying afterwards he "did not know Wikipedia was used as a
;serious reference tool"                 [wiki]
;(उदाहरण के लिए यहाँ घटना देखें जिसमें एक व्यक्ति ने बाद में यहाँ कहते हुए कि वह नहीं जानता था कि विकिपीडिया को एक अहम उद्धरण के साधन के रूप में इस्तेमाल किया जाता है, एक सहकर्मी के ऊपर परिहास के रूप में एक विशिष्ट पत्रकार को कैनेडी गुप्तघात और सोवियत रूस से जोड़ते हुए एक फ़र्ज़ी जीवनी लिखी जो कि चार महीनों तक गुप्त रही.       [manual]
(defrule link5
(declare (salience 5000))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 assassination)           ;Added by 14anu-ban-08 (12-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link5   "  ?id " joda )" crlf))
)

;@@@ Added by 14anu-ban-08 (13-11-2014)
;Ford's involvement with the soybean opened many doors for agriculture and industry to be linked more strongly than ever before.[Agriculture]
;सोयाबीन के साथ फोर्ड की भागीदारी होने से कृषि और उद्योग को पहले से अधिक मजबूती से  जोड़ने के लिए कई दरवाजे खोल दिये.     [Self]
(defrule link6
(declare (salience 5001))
(id-root ?id link)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 strongly)           
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  link.clp 	link6   "  ?id " joda )" crlf))
)
