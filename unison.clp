;@@@ Added by 14anu13 on 13-06-14
;They clapped in unison and congratulated her.
;उन्होने सामन्जस्य मे ताली बजाई व उसे बधाई दी |
(defrule unison2
(declare (salience 5200))
(id-root ?id unison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanjasya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unison.clp 	unison2   "  ?id " sAmanjasya )" crlf))
)

;$$$ Modified by 14anu-ban-07 (18-12-2014)
;@@@ Added by 14anu13 on 13-06-14
;The songs are sung in unison and the singer decides how and which way the notes and syllables are to be pronounced .
; गाने एक स्वर मे गाए गये हैं और गायक और निश्चय करता है जो मार्ग टिप्पणियाँ और अक्षर कैसे उच्चारण किए जाने हैं . 
(defrule unison1
(declare (salience 5100))
(id-root ?id unison)
(id-root ?id1 sing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun);changed from 'verb' to 'noun' by 14anu-ban-07 (18-12-2014)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_svara_meM));meaning changed from 'ek_svara_me' to 'eka_svara_meM' by 14anu-ban-07 (18-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unison.clp 	unison1   "  ?id " eka_svara_meM   )" crlf))
)


;@@@added by 14anu13  on 13-06-14
;Both men said, in unison.
;दोनो मित्र एक साथ बोले |
(defrule unison0
(id-root ?id unison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unison.clp 	unison0   "  ?id "  eka_sAWa )" crlf))
)
