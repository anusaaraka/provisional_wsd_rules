;@@@ Added by 14anu-ban-03 (02-04-2015)
;I think my uncle still conceives of me as a four-year-old. [cald]
;मुझे लगता है कि मेरा चाचा अभी भी मुझे  चार साल का समझते है . [manual]
(defrule conceive1
(declare (salience 10))
(id-root ?id conceive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceive.clp 	conceive1   "  ?id "  samaJa )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-04-2015)
;He couldn't conceive of a time when he would have no job. [cald]
;वह उस समय की कल्पना भी नहीं कर सकता जब उसके पास कोई काम नहीं था . [manual]
(defrule conceive2
(declare (salience 10))
(id-root ?id conceive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceive.clp 	conceive2   "  ?id "  kalpanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-04-2015)
;The exhibition was conceived by the museum's director. [cald]
;प्रदर्शनी सङ्ग्रहालय के निर्देशक के द्वारा आयोजित की गयी थी . [manual]
(defrule conceive3
(declare (salience 10))
(id-root ?id conceive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 director)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceive.clp 	conceive3   "  ?id "  Ayojiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-04-2015)
;Do you know exactly when you conceived? [cald]
;क्या आप जानते हैं कि वास्तव में आप कब गर्भ से हुए? [manual]
(defrule conceive4
(declare (salience 10))
(id-root ?id conceive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 when)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garBa_se_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceive.clp 	conceive4   "  ?id "  garBa_se_ho )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (02-04-2015)
;I can't conceive how anyone could behave so cruelly. [cald]
;मैं विचार नहीं कर सकता हूँ कि कोई भी इतना निर्दयतापूर्वक व्यवहार  कैसे कर सकता हैं. [manual]
(defrule conceive0
(declare (salience 00))
(id-root ?id conceive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceive.clp 	conceive0   "  ?id "  vicAra_kara )" crlf))
)

