;@@@ Added  by 14anu-ban-03 (18-03-2015)
;His cruel and callous comments made me shiver. [oald]
;उसकी निर्दयी एवं संवेदनाहीन टिप्पणियों ने मुझे थरथरा/कँपकँपा दिया . [self]
(defrule callous2
(declare (salience 10))
(id-root ?id callous)               
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 comment)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMvexanAhIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  callous.clp     callous2   "  ?id "  saMvexanAhIna )" crlf))
)

;-------------------------------Default Rules-----------------------

;@@@ Added  by 14anu-ban-03 (18-03-2015)
;A callous attitude. [oald]
;एक कठोर रवैया. [manual]
(defrule callous0
(declare (salience 00))
(id-root ?id callous)               
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  callous.clp     callous0   "  ?id "  kaTora )" crlf))
)

;@@@ Added  by 14anu-ban-03 (18-03-2015)
;I know it sounds callous but we really need the money. [hinkhoj]
;मैं जानता हूँ यह बेरूखी लगेगी परन्तु वास्तव में हमें पैसों की आवश्यकता है . [manual]
(defrule callous1
(declare (salience 00))
(id-root ?id callous)               
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id berUKI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  callous.clp     callous1   "  ?id "  berUKI )" crlf))
)

