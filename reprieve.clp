;@@@ Added by 14anu-ban-10 on (19-02-2015)
;70 jobs have been reprieved until next April.[oald]
;70 नौकरियों  को अगले अप्रैल तक  विराम दिया गया है।[manual]
(defrule reprieve2
(declare (salience 5100))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1 )
(id-root ?id1 job)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAma_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve2   "  ?id "  verAma_xe )" crlf))
)
;@@@ Added by 14anu-ban-10 on (19-02-2015)
;Reprieve a condemned prisoner.[hinkhoj]
;एक दण्डित  क़ैदी के लिये  दण्ड या प्राणदंड की थोड़े समय के लिये रोक .[manual]
(defrule reprieve3
(declare (salience 5200))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prANaxaMda_kI_WodeZ_samaya_ke_liye_roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve3   "  ?id "  prANaxaMda_kI_WodeZ_samaya_ke_liye_roka )" crlf))
)
;@@@ Added by 14anu-ban-10 on (19-02-2015)
;The prisoner was given a last minute reprieve.[hinkhoj]
;कैदी को आखिरी मिनट   दण्ड या प्राणदंड की थोड़े समय के लिये रोक  दी गई थी।manual]
(defrule reprieve4
(declare (salience 5300))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1 )
(id-root ?id1 last)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prANaxaMda_kI_WodeZ_samaya_ke_liye_roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve4   "  ?id "  prANaxaMda_kI_WodeZ_samaya_ke_liye_roka )" crlf))
)
;@@@ Added by 14anu-ban-10 on (19-02-2015)
;A reprieved murderer.[oald]
;एक  हत्यारा की  प्राणदण्ड स्थगित की गायी.[manual]
(defrule reprieve5
(declare (salience 5400))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?  ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prANaxaMda_sWagiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve5   "  ?id "  prANaxaMda_sWagiwa)" crlf))
)

;------------------------ Default Rules ----------------------

;"reprieve","N","1.CUta{prANaxaNda_iwyAxi}"
;The prisoner was given a last minute reprieve.
(defrule reprieve0
(declare (salience 5000))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve0   "  ?id "  CUta )" crlf))
)

;"reprieve","VT","1.CUta_xenA{prANaxaNda_iwyAxi}"
;Reprieve a condemned prisoner.  
(defrule reprieve1
(declare (salience 4900))
(id-root ?id reprieve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUta_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reprieve.clp 	reprieve1   "  ?id "  CUta_xe )" crlf))
)

