
(defrule handy0
(declare (salience 5000))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nipuNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy0   "  ?id "  nipuNa )" crlf))
)

;"handy","Adj","1.nipuNa"
;Arjuna was handy in archery .
;arjuna XanurvixyA meM nipuNa WA.
;
(defrule handy1
(declare (salience 4900))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy1   "  ?id "  xakRa )" crlf))
)

;@@@ Added by Prachi Rathore[21-1-14]
;Don't throw that away—it might come in handy.[oald]
;इसे मत फेंकिए — यह काम आ सकता है . 
(defrule handy2
(declare (salience 5500))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 come)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy2   "  ?id "  kAma )" crlf))
)

;$$$ Modified by 14anu-ban-06 (16-02-2015)
;### [COUNTER EXAMPLE] ### First-time visitors to France will find this guide particularly handy. 
;(cambridge)
;### [COUNTER EXAMPLE] ### पहली बार फ्रांस के दर्शक इस पथप्रदर्शक को विशेष रूप से उपयोगी पाएँगे . (manual)
;@@@ Added by Prachi Rathore[21-1-14]
;Always keep a first-aid kit handy.[oald]
;एक प्राथमिक चिकित्सा उपकरण समूह हमेशा पास रखिए . 
(defrule handy3
(declare (salience 5500))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa  ?id1 ?id);added 'id1' by 14anu-ban-06 (16-02-2015)
(id-root ?id1 kit);added by 14anu-ban-06 (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy3   "  ?id "  pAsa )" crlf))
)


;@@@ Added by Prachi Rathore[21-1-14]
; Some handy hints for removing stains[oald]
;कुछ आसान सुझाव है धब्बे निकालने के लिये  . 
(defrule handy4
(declare (salience 5400))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(kriyA-kriyA_viSeRaNa  ? ?id)(viSeRya-viSeRaNa  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy4   "  ?id "  AsAna )" crlf))
)
;"handy","N","1.xakRa/sulaBa"

;$$$ Modified by 14anu-ban-06 (29-11-2014)
;@@@ Added by 14anu24
;Have receipts and any other documents handy.
;रसीदें और दूसरे कोई दस्तावेज तैयार कर के पास  रख लीजिए
(defrule handy5
(declare (salience 5800))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(viSeRya-viSeRaNa  ?id ?);commented by 14anu-ban-06 (29-11-2014)
(viSeRya-viSeRaNa  ?id1 ?id);added by 14anu-ban-06 (29-11-2014)
(id-root ?id1 document);added by 14anu-ban-06 (29-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_raKa));meaning changed from 'pAsa_raKeM' to 'pAsa_raKa' by 14anu-ban-06 (13-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy5   "  ?id "  pAsa_raKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-02-2015)
;Our house is very handy for the station.(OALD)
;हमारा घर स्टेशन के लिए बहुत सुविधाजनक है . (manual)
(defrule handy6
(declare (salience 5500))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 station|market|shop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suviXAjanaka));meaning changed from 'najaxIka' to 'suviXAjanaka' by 14anu-ban-06 (24-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy6   "  ?id "  suviXAjanaka )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-02-2015)
;First-time visitors to France will find this guide particularly handy. (cambridge)
;पहली बार फ्रांस के दर्शक इस पथप्रदर्शक को विशेष रूप से उपयोगी पाएँगे . (manual)
(defrule handy7
(declare (salience 5500))
(id-root ?id handy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 guide)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayogI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handy.clp 	handy7   "  ?id "  upayogI )" crlf))
)


