
(defrule note0
(declare (salience 5000))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id noted )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viKyAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  note.clp  	note0   "  ?id "  viKyAwa )" crlf))
)

;"noted","Adj","1.viKyAwa"
;M.S.Subbhalakshmi is a noted singer.
;
;
;$$$Modified by 14anu-ban-08 (28-01-2015)              
;He noted down the whole story in five minutes. 
;usane pUrI kahAnI pAzca minata meM liKa xI.
(defrule note1
(declare (salience 4900))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)                          ;commented relation by 14anu-ban-08 (28-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 liKa_xe))        ;changed meaning from 'liKa' to 'liKa_xe' by 14anu-ban-08 (28-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " note.clp	note1  "  ?id "  " ?id1 "  liKa_xe )" crlf))
)

(defrule note2
(declare (salience 4800))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tippaNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note2   "  ?id "  tippaNI )" crlf))
)

;"note","N","1.tippaNI"
;She took all the notes from her friend.
;--"2.citTI"
;Jim gave her a note for attending the party.
;--"3.svara"
;The singer held the note too long.
;--"4.nota"
;We gave him a hundred rupee note.
;--"5.tIkA"
;A brief note is given in the introduction of this book.
;--"6.XyAna"
;It was a performance worthy of note.
;
(defrule note3
(declare (salience 4700))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_se_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note3   "  ?id "  XyAna_se_xeKa )" crlf))
)

;"note","VT","1.XyAna_se_xeKanA"
;Take note of this chemical reaction.

;@@@ Added by Nandini
;$$$ Modified by 14anu13  on 13-06-14  (spelling corrected  from "swara" to "svara")
;Ann blew a few notes on the trumpet.
;ann ne wurahI para kuCa swara bajAe.
;ann ne wurahI para kuCa svara bajAe.           ;Hindi Translation is modified by 14anu-ban-08 (09-12-2014)
(defrule note4
(declare (salience 4850))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 blow)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note4   "  ?id " svara )" crlf))
)
;@@@ Added by 14anu-ban-08 on 27-08-2014
;Note the weight of the rod W acts at its center of gravity G.      [NCERT]
;ध्यान दें कि छड का भार W इसके गुरुत्व केन्द्र G पर कार्य करता है. 
(defrule note5
(declare (salience 4900))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note5   "  ?id " XyAna_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-10-2014)
;Note, the point of contact of the top with ground is fixed. [NCERT CORPUS]
;XyAna xeM ki lattU kA vaha biMxu jahAz yaha XarAwala ko CUwA hE, sWira hE. [NCERT CORPUS]
(defrule note6
(declare (salience 4900))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 fix)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note6   "  ?id " XyAna_xe )" crlf))
)



;@@@ Added by 14anu24 [17-6-14]
;Make a note of the conversation and to whom you spoke .
;अपनी बातचीत के विवरण कहीं लिख दीजिए और यह भी कि आपने वहां जिस से शिकायत की उसका नाम क्या है .
(defrule note7
(declare (salience 5000))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp     note7   "  ?id "  vivaraNa )" crlf))
)

;@@@ Added by 14anu23 on 26/6/14
;The note of the nightingale.
;बुलबुल का स्वर . 
(defrule note8
(declare (salience 4800))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp     note8   "  ?id "  svara )" crlf))
)

;@@@ Added by 14anu23 on 26/6/14
;A sharp note .
;एक ऊँचा स्वर . 
(defrule note9
(declare (salience 4800))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
(id-root =(- ?id 1) practice|high|low|sharp|soft)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp     note9   "  ?id "  svara )" crlf))
)

;Further , since there is one - reed - to - one - note relationship only discontinuous and staccato melodies are possible 
;इसमें एक सुर के लिए एक रीड की व्यवस्था है इसलिए केवल खंडित एवं स्थूल धुनें ही संभव हैं .
;@@@ Added by 14anu11
(defrule note10
(declare (salience 4800))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note10   "  ?id "  swara )" crlf))
)

;$$$ Modified by 14anu-ban-08 (10-12-2014)           ;Meaning changed from 'swara' to 'svara'
;@@@ Added by 14anu11
;The singer holds the tuntune in the hand and plucks the string with his forefinger , to give the base note and a kind of rhythm .
;गायक इस तुनतुने को अपने हाथ में पकडे रहता है तथा ऊंगली से तार को छेड कर विभिन्न स्वर निकालता रहता है .
(defrule note11
(declare (salience 4800))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id2)
(kriyA-object  ?id3 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara))       ;Meaning changed from 'swara' to 'svara' by 14anu-ban-08 (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note11   "  ?id "  svara )" crlf))                                     ;Meaning changed from 'swara' to 'svara' by 14anu-ban-08 (10-12-2014)
)




;LEVEL 
;
;
;Headword : note
;
;Examples --
;
;1.The use of" Note" in different meanings.
;
;yahAz hama "Note" kA "nota karane" ke arWa me le rahe hE.("kriyA")  
;uxAharaNa ke lie;
;
;"note","1.XyAna xenA"
;His performance was worthy of note.
;usakI axAkArI "XyAna xene" yogya WIM
;Please note his words.
;usakI bAwo ko "XyAna xoM"
;Did you note how dirty her hands were?
;kyA wumane usake mEle hAWoM para" XyAna xiyA?"
;
;--"2.XyAna_se_liKanA"
;He sat taking  notes of everything that was said.
;vaha bETa kara kahI gayI sArI bAwoM ko "XyAna_se_liKane" lagA.
;Make a note of the dates of the examination.
;parIkRA ke wiWiyo ko "XyAna_se_liKo" .
;
;"note","N"1.tippaNI"
;The movie ended on an optimistic note.
;Pilma kI samApwi ASAvAxI "tippaNI" ke rUpa meM huI.
;
;--"nota(rUpayA)"
;We only  exchange notes && traveller's cheques.
;hama sirPa yAwriyoM ke ceMka Ora "notoM" kI axalA- baxalI karawe hE.
;
;yahAz ina cAroM arWo meM eka mUla arWa sAmane AwA hEM,"
;
;"note <-----  kisI bAwa para XyAna xenA".wo isa ke AXAra para nota kA sUwra hogA,
;
;wo vAkyo kA anuvAxa kuCa Ese hogA. 
;
;1.vaha bETa kara kahI gayI sArI bAwoM para "XyAna xene" lagA.
;2.parIkRA ke wiWiyoM para "XyAna xo".
;3.usane apane riSawexAroM ko XanyavAxa kI citTI BejI.{unakA "XyAna raKawe hue"}
;4.Pilma kI samApwi ASAvAxI rUpa me huI.{"XyAna xene kI bAwa hE."}
;5.isa bAra usane kuCa alaga bAwoM para "XyAna xiyA WA."
;6.usakI axAkArI "XyAna xene" yogya WI.
;7.usakI bAwoM para "XyAna xo."
;8.kyA wumane usake mEle hAWoM para "XyAna xiyA."
;
;yahAz xo vAkyoM meM "XyAna xenA"nahIM bETa rahA hE,wo vahAz para eka nayA Sabxa raKa sakawe   hE "nota"jo saBI saMrxaBo meM ina kriyAo meM arWa ko banAe raKawA hE.
;
;9.usake gAneM ke pahale suro ko nota karawe hI unnahone vAhavAhI kI
;10.hama sirPa yAwriyoM ke ceMka Ora nota kI axalA- baxalI karawe hE.
;
;wo 'note' kA sUwra hogA
;
; sUwra : nota[<XyAna_xenA]
;
;

;@@@ Added by 14anu13  on 13-06-14
; The songs are sung in unison and the singer decides how and which way the notes and syllables are to be pronounced .
; गाने एक स्वर मे गाए गये हैं और गायक और निश्चय करता है जो मार्ग स्वर और अक्षर कैसे उच्चारण किए जाने हैं . 
(defrule note12
(declare (salience 4850))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 syllables)
(conjunction-components ?  $? ?id $? ?id1 $?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note12   "  ?id " svara )" crlf))
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;Indian government introduced a new one rupee note.  [Anusaraaka-Agama team observation]
;भारतीय सरकार ने एक रुपय का नोट जारी किया.  [self]
(defrule note13
(declare (salience 4850))
(id-root ?id note)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 rupee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nota))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  note.clp 	note13   "  ?id " nota )" crlf))
)

