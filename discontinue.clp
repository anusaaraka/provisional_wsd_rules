;$$$ Modified by 14anu-ban-04 (17-04-2015)        ------changed meaning from 'jZArI_na_raha' to 'jZArI_nahIM_raha'
;The support from our sponsoring agency will discontinue after March 31.                 [same clp file]
;मार्च 31 के बाद हमारे प्रायोजित करनेवाले अभिकरण से समर्थन जारी नहीं रहेगा .                         [self]
(defrule discontinue0
(declare (salience 10))                     ;salience reduced from '5000' to '10' by 14anu-ban-04 (17-04-2015)
(id-root ?id discontinue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZArI_nahIM_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discontinue.clp 	discontinue0   "  ?id "  jZArI_nahIM_raha )" crlf))
)
;"discontinue","VI","1.jZArI_na_rahanA"

;"discontinue","V","1.jZArI_na_rahanA2"
;$$$ Modified by 14anu-ban-04 (17-04-2015)           ------changed meaning from 'jZArI_na_raha' to 'banxa_kara'
;The bank is discontinuing this service.              [cald]
;बैंक इस सेवा को बन्द कर  रहा है .                               [self]
(defrule discontinue1
(declare (salience 20))                                ;salience reduced from '4800' to '20' by 14anu-ban-04 (17-04-2015)
(id-root ?id discontinue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)                                   ;added by 14anu-ban-04 (17-04-2015)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))                 ;added by 14anu-ban-04 (17-04-2015)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  discontinue.clp    discontinue1  "  ?id " ko  )" crlf)  ;added by 14anu-ban-04 (17-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discontinue.clp 	discontinue1   "  ?id "  banxa_kara )" crlf))
)

;$$$ Modified by 14anu-ban-04 (17-04-2015)
;It was decided to discontinue the treatment after three months.              [oald]
;तीन महीनों  के बाद इलाज को जारी न रखने का फैसला किया गया था .                               [self]
(defrule discontinue2
(declare (salience 40))                        ;salience reduced from '4800' to '40' by 14anu-ban-04 (17-04-2015)
(id-root ?id discontinue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)                        ;added by 14anu-ban-04 (17-04-2015)
(id-root ?id1 treatment|study)                 ;added by 14anu-ban-04 (17-04-2015)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))                  ;added by 14anu-ban-04 (17-04-2015)
(assert (id-wsd_root_mng ?id jZArI_na_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  discontinue.clp    discontinue2  "  ?id " ko  )" crlf);added by 14anu-ban-04 (17-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discontinue.clp 	discontinue2   "  ?id "  jZArI_na_raKa )" crlf))
)

;"discontinue","VT","1.jZArI_na_raKanA"
;They will discontinue the financial support from next year.
;
