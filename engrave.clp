;@@@ Added by 14anu-ban-04 (10-03-2015)
;The silver cup was engraved with his name.              [oald]
;चाँदी  का कप उसके नाम से उत्कीर्ण किया गया था . [manual]
;His name was engraved on the silver cup.                   [oald]
;उसका नाम रजत कप पर उत्कीर्ण किया गया था .  [manaul]                
(defrule engrave2
(declare (salience 4910))
(id-root ?id engrave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-on_saMbanXI ?id ?id1)(kriyA-with_saMbanXI ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkIrNa_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engrave.clp 	engrave2   "  ?id "  uwkIrNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-03-2015)
;The date of the accident remains engraved on my mind.              [oald]
;दुर्घटना की तारीख मेरे मन पर छपी   रही .                                         [self]
(defrule engrave3
(declare (salience 4920))
(id-root ?id engrave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 mind|heart|memory) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Capa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engrave.clp 	engrave3   "  ?id "  Capa )" crlf))
)

;------------------------ Default Rules ----------------------

;"engraving","N","1.XAwu-lakadZI_Axi_para_ciwra_Koxane_kI_vixyA"
;Sita bought an old engraving of the cathedral.
(defrule engrave0
(declare (salience 5000))
(id-root ?id engrave)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id engraving )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XAwu-lakadZI_Axi_para_ciwra_Koxane_kI_vixyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  engrave.clp  	engrave0   "  ?id "  XAwu-lakadZI_Axi_para_ciwra_Koxane_kI_vixyA )" crlf))
)

;"engrave","VT","1.KoxanA"
;Rats engrave the land.
(defrule engrave1
(declare (salience 4900))
(id-root ?id engrave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engrave.clp 	engrave1   "  ?id "  Koxa )" crlf))
)

