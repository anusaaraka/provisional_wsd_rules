
(defrule trespass0
(declare (salience 5000))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-against_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trespass.clp trespass0 " ?id "  aparAXa_kara )" crlf)) 
)

(defrule trespass1
(declare (salience 4900))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aparAXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trespass.clp	trespass1  "  ?id "  " ?id1 "  aparAXa_kara  )" crlf))
)

(defrule trespass2
(declare (salience 4800))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awikramaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trespass.clp 	trespass2   "  ?id "  awikramaNa_kara )" crlf))
)

(defrule trespass3
(declare (salience 4700))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awikramaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trespass.clp 	trespass3   "  ?id "  awikramaNa )" crlf))
)

;"trespass","N","1.awikramaNa"
;It is a tresspass to enter other people's property without their permission.
;


;@@@Added by 14anu-ban-07,(14-03-2015)
;They said we should stay another night, but I didn't want to trespass on their hospitality.(cambridge)(parser no. 17)
;हमें एक और रात रहना चाहिए उन्होंने कहा, परन्तु मैं नहीं चाहा था उनका आतिथ्य का फायदा उठाना  . (manual)
(defrule trespass4
(declare (salience 5000))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PAyaxA_uTA))
(assert (id-wsd_viBakwi ?id2 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trespass.clp	trespass4  "  ?id "  " ?id1 "  PAyaxA_uTA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  trespass.clp trespass4  "  ?id2 " kA)" crlf)
)
)

;@@@Added by 14anu-ban-07,(14-03-2015)
;Forgive us our trespasses as we forgive those who trespass against us.(coca)
;हमें हमारे अपराधो के लिए माफ कीजिए जैसे  हमने माफ किया उन्हे जिन्होने हमरे विरुद्ध अपराध किया हैं . (manual)
(defrule trespass5
(declare (salience 4900))
(id-root ?id trespass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id2)
(object-object_samAnAXikaraNa  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trespass.clp 	trespass5   "  ?id "  aparAXa )" crlf))
)

