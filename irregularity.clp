;@@@ Added by 14anu-ban-06 (12-03-2015)
;The west of the island is famous for the irregularity of its coastline. (cambridge)
;द्वीप का पश्चिम उसके समुद्री किनारे की अनियमितता के लिए प्रसिद्ध है . (manual)
(defrule irregularity0
(declare (salience 0))
(id-root ?id irregularity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyamiwawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregularity.clp 	irregularity0   "  ?id "  aniyamiwawA )" crlf))
)


;@@@ Added by 14anu-ban-06 (12-03-2015)
;The paint will cover any irregularity in the surface of the walls.(OALD)
;रंग दीवारों की सतह पर कोई भी विषमता ढक लेगा . (manual)
(defrule irregularity1
(declare (salience 2000))
(id-root ?id irregularity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id2 ?id)
(kriyA-in_saMbanXI ?id2 ?id1)
(id-root ?id1 surface)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRamawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregularity.clp 	irregularity1   "  ?id "  viRamawA )" crlf))
)
