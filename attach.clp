;@@@ Added by 14anu-ban-05 Prajna Jha on 05.08.2014
;Attach the coupon to the front of your letter.
;kUpana ko patra ke muKa pqRTa se jodZo
(defrule attach0
(declare (salience 2000))
(id-root ?id attach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attach.clp   attach0   "  ?id " jodZa )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 25.07.2014
;Daman is attached to the state of Gujarat in the east and the Arabian Sea in the west .
;xamana pUrva meM gujarAwa rAjya se Ora paScima meM araba sAgara se judZA huA hE.
(defrule attach1
(declare (salience 2000))
(id-root ?id attach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attach.clp   attach1   "  ?id " judZa )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha  on 25.07.2014
;Two half pictures and one Lakulish statue attached on the internal wall of the canopy seem to be installed later .
;maMdapa kI AByaMwarIya BAga para lagi xo arxXaciwra Ora eka lakulISa prawimA bAxa meM lagAI gaI lagawi hE
(defrule attach2
(declare (salience 2100))
(id-root ?id attach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 wall|form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attach.clp   attach2   "  ?id " lagA )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha  on 25.07.2014
;I attach a copy of my notes for your information.
;mEM apane tippaNiyoM ki pratIlipi ko wumhAre janakArI ke liye sanlagana kara rahA hUz  
(defrule attach3
(declare (salience 2000))
(id-root ?id attach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(or (id-root ?id1 I|we|he|she)(id-cat_coarse ?id1 PropN))
(kriyA-object  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sanlagana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  attach.clp   attach3   "  ?id " sanlagana_kara)" crlf))
)


