;@@@ Added by 14anu01 on 18-06-2014
;There is a mouse in the room.
;कमरे में एक चूहा है . 
(defrule mouse0
(declare (salience 5000))
(id-root ?id mouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-on_saMbanXI  ?id ?)(viSeRya-in_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cUhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mouse.clp  	mouse0   "  ?id "  cUhA )" crlf))
)

;@@@ Added by 14anu01 on 18-06-2014
;The mouse for my computer isn't working. 
;मेरे सङ्गणक के लिए माउस काम नहीं कर रहा है . 
(defrule mouse1
(declare (salience 5500))
(id-root ?id mouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-for_saMbanXI  ?id ?)(viSeRya-of_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id  mAusa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mouse.clp  	mouse1   "  ?id "   mAusa )" crlf))
)

;@@@ Added by 14anu01 on 18-06-2014
;That field is Faber's main area for mousing of lizards.
;वह क्षेत्र छिपकली का खोजने के लिए फाबेर का प्रमुख क्षेत्र है .
(defrule mouse2
(declare (salience 4500))
(id-root ?id mouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KojanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  mouse.clp  	mouse2   "  ?id "   KojanA )" crlf))
)



