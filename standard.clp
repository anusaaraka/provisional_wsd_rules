;$$$ Modified by 14anu-ban-01 on (15-10-2014)
;Translation added by 14anu-ban-01
;This is the standard procedure.[self with reference to merriam-webster]
;यह ही मानक कार्यविधि है.[self]
;The average reading of clock 1 is much closer to the standard time than the average reading of clock 2.  [NCERT corpus]
;GadI 1 xvArA lie gae samaya ke pATyAfka, GadI 2 xvArA lie gae samaya ke pATyAfkoM kI wulanA meM, mAnaka samaya ke aXika nikata hE.[NCERT corpus] 
;घडी 1 द्वारा लिए गए समय के पाठ्याङ्क, घडी 2 द्वारा लिए गए समय के पाठ्याङ्कों की तुलना में, मानक समय के अधिक निकट है.[NCERT corpus]
(defrule standard0
(declare (salience 5000))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnaka)) ;Replaced prAmANika with mAnaka by Shirisha Manju Suggested by Chaitanya Sir for the below sentences
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  standard.clp 	standard0   "  ?id "  mAnaka )" crlf))
);corrected the meaning from 'prAmANika' to 'mAnaka' only in this line[there was a meaning mismatch in assert and printout statement] by 14anu-ban-01 on (15-10-2014)

;"standard","Adj","1.prAmANika"
;All languages should be accepted in standard form.
;--"2.mAnya"
;NCERT textbooks are a standard for CBSE students.
;--"3.mUla"
;Please charge the standard cost of this tea set.
;
(defrule standard1
(declare (salience 4900))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxarSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  standard.clp 	standard1   "  ?id "  AxarSa )" crlf))
)

;"standard","N","1.AxarSa"
;Don't let your standards to be vanished.
;--"2.mAnaka"
;Meter is an internationally accepted standard for measuring length.
;--"3.swara"
;We expect high standards of studies in convent schools.
;--"4.kakRA/xarajA"
;Now I am in the 10th standard.
;

;@@@ Added by 14anu-ban-01 on (15-10-2014)
;We now use an atomic standard of time, which is based on the periodic vibrations produced in a cesium atom.[NCERT corpus]
;अब हम समय-मापन हेतु समय का परमाण्वीय मानक प्रयोग करते हैं जो सीजियम परमाणु में उत्पन्न आवर्त कम्पनों पर आधारित है.[NCERT corpus]
;The national standard of time interval 'second' as well as the frequency is maintained through four cesium atomic clocks.[NCERT corpus]
;चार सीजियम परमाणु घडियों के माध्यम से, समय-अन्तराल के राष्ट्रीय मानक 'सेकन्ड' का अनुरक्षण किया जाता है.[NCERT corpus]

(defrule standard2
(declare (salience 5000))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  standard.clp 	standard2   "  ?id "  mAnaka)" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-10-2014)
;This is the basis of the cesium clock, sometimes called atomic clock, used in the national standards.[NCERT corpus]
;यही राष्ट्रीय मानक के रूप में प्रयुक्त सीजियम घडी, जिसे परमाणु घडी भी कहते हैं, का आधार है.[NCERT corpus]
;Such standards are available in many laboratories.[NCERT corpus]
;ऐसे मानक अनेक प्रयोगशालाओं में उपलब्ध हैं.[NCERT corpus]
;In principle they provide portable standard.[NCERT corpus]
;सिद्धान्ततः वे एक सुबाह्य मानक उपलब्ध कराती हैं.[NCERT corpus]
;In our country, the NPL has the responsibility of maintenance and improvement of physical standards, including that of time, frequency, etc..[NCERT corpus]
;हमारे देश में, सभी भौतिक मानकों (जिनमें समय और आवृत्ति आदि के मानक भी शामिल हैं) के अनुरक्षण और सुधार का दायित्व NPL का है.[NCERT corpus]
;Meter is an internationally accepted standard for measuring length.[standard.clp]
(defrule standard3
(declare (salience 4900))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 national|portable|physical|such|accepted)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  standard.clp 	standard3   "  ?id "  mAnaka)" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-10-2014)
;Now I am in the 10th standard.[standard.clp]
;अब मैं दसवीं कक्षा में हूँ.[self]
(defrule standard4
(declare (salience 4900))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 be|study)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  standard.clp 	standard4   "  ?id "  kakRA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-10-2014)
;We expect high standards of studies in convent schools.[standard.clp]
;हम कान्वेंट विद्यालयों में उच्च स्तर के अध्ययन की आशा करते हैं.[self]
(defrule standard5
(declare (salience 5000))
(id-root ?id standard)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 high)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ucca_swara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " standard.clp	standard5  "  ?id "  " ?id1 "  ucca_swara  )" crlf))
)

