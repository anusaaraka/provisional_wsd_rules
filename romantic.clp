
(defrule romantic0
(declare (salience 5000))
(id-root ?id romantic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id romAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romantic.clp 	romantic0   "  ?id "  romAnI )" crlf))
)

(defrule romantic1
(declare (salience 4900))
(id-root ?id romantic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romantic.clp 	romantic1   "  ?id "  kalpiwa )" crlf))
)
;@@@ Added by Anita--23.5.2014
;We can take Satyajit Ray's comments further and wonder whether his depiction of the village is ;romantic. [SOCIOLOGY TEXT BOOK CLASS XI]
;हम सत्यजीत रे की टिप्पणी के आगे जा सकते हैं और आश्चर्यचकित हो सकते है कि  क्या गाँव का उसका वर्णन  रूमानी है ।
(defrule romantic2
(declare (salience 4950))
(id-root ?id romantic)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id romAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  romantic.clp 	romantic2   "  ?id "  romAnI )" crlf))
)

;"romantic","Adj","1.kalpiwa"
;She has highly romantic ideas of life rather than reality.  
;--"2.romAnI
;Keats was one of the greatest romantic poets of his time. 
;
;
