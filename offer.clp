;Removed by 14anu-ban-09 on (04-12-2014)
;NOTE-This sentence is already handled in rule offer7. So, not needed.
;Added by Sonma Gupta MTech IT Banasthali 2013
;Dear Lord, we offer up our prayers. [Veena mam Translation]
;हे भगवान, हम अपनी प्रार्थना भेट करते हैं .
;(defrule offer0
;(declare (salience 5000))
;(id-root ?id offer)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?)
;(kriyA-upasarga  ?id ?)
;(kriyA-subject  ?id ?)
;(kriyA-subject  ?id ?)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id BeMta_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  offer.clp  	offer0   "  ?id "  BeMta_kara )" crlf))
;)



(defrule offer1
(declare (salience 4900))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id offering )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  offer.clp  	offer1   "  ?id "  BeMta )" crlf))
)

;"offering","N","1.BeMta"
;He gave her a diamond ring as a peace offering.
;
(defrule offer2
(declare (salience 4800))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer2   "  ?id "  praswAva )" crlf))
)


;$$$ Modified by 14anu-ban-05 Prajna Jha on 30-07-2014
;Added by Sonma Gupta MTech IT Banasthali 2013
;"I'll do the cooking, " he offered.  [Veena mam Translation]
;"मैं खाना पकाऊंगा,"उसने खाना पेशकश की.
(defrule offer3
(declare (salience 4700))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?id1) ;added ?id1 by 14anu-ban-05 Prajna Jha on 30.07.2014
(kriyA-subject  ?id ?id2) ;added ?id2 by 14anu-ban-05 Prajna Jha on 30.07.2014
(id-root ?id1 do) ;Added by 14anu-ban-05 Prajna Jha on 30.07.2014
(id-root ?id2 he|she) ;Added by 14anu-ban-05 Prajna Jha on 30.07.2014
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSakaSa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer3   "  ?id "  peSakaSa_raKa )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;She was offered a job in Paris.  [Veena mam Translation]
;उसको पेरिस में काम का  प्रस्ताव दिया गया था . 
(defrule offer4
(declare (salience 4600))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
(kriyA-subject  ?id ?)
(kriyA-karma  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer4   "  ?id "  praswAva_xe )" crlf)
)
)

;@@@ Added by 14anu24
;You may be looking after a child from baby to teenager , able to offer them emotional support and build self - esteem as they grow .
;संभव है कि आप किसी बच्चे की शिशु से किशोरावस्था तक की आयु तक देखभाल करें और उसे भावनात्मक सहारा दें और उसका आत्मविश्वास विकसित करें .
(defrule offer_tmp
(declare (salience 4500))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object_2  ?id ?id1)
(id-word ?id1 support)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp    offer_tmp   "  ?id "  xe )" crlf)
)
)


;Added by Sonma Gupta MTech IT Banasthali 2013
;I feel bad that I didn't offer them any food to them. [Veena mam Translation]
;मुझे बुरा लग रहा है कि मैंने उन्हें कुछ भी खाने को नहीं दिया .
(defrule offer5
(declare (salience 4500))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(or(and(kriyA-vAkya_viBakwi  ?id ?)(kriyA-kriyA_niReXaka ?id ?)(kriyA-vAkyakarma  ? ?id))(and(kriyA-subject  ?id ?)(kriyA-object  ?id ?)(kriyA-for_saMbanXI  ?id ?)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer5   "  ?id "  xe )" crlf))
)


;$$$ Modified by Shirisha Manju 18-12-14 -- changed meaning 'praxAna_karanA' as 'praxAna_kara'
;@@@ Added by 14anu24
;Do not give in if you are offered much less than you have asked for : keep pressing for what you think is fair . Contact ABTA or AITO if the tour operator is a member .
;अगर वो उससे कम देना स्वीकार करें , तो उनकी बत मत मानिए और उतना माऋगते रहिए ऋतना आप उचित समझें . सैर आयोऋत करने वाले अगर सदस्य हैं 
;तो आबटा या आइटो से संपर्क करें .
(defrule offer07
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp    offer07   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 28-07-2014
;On the last day the statues are brought out in procession and offered in the rivers.[TOURISM CORPUS]
;anwima  xina  prawimAoM  ko  julUsa  meM  nikAlA  jAwA  hE  waWA  naxiyoM  meM  visarjiwa  kiyA  jAwA  hE
(defrule offer007
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(kriyA-in_saMbanXI  ?id ?id2)
(id-root ?id1 statue)
(id-root ?id2 river)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id visarjiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp   offer007   "  ?id " visarjiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 28-07-2014
;1008 earthen pots of sacred water , milk , sandal , flower etc. will be offered this year .[TOURISM CORPUS]
;isa  varRa  1008  kalaSa  paviwra  jala  ,  xUXa  ,  caMxana  ,  PUla  iwyAxi  arpiwa  kie  jAezge
(defrule offer08
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1 )
(id-root ?id1 pot|water|flower|sandal)
;(kriyA-kAlavAcI  ?id ?id2 )
;(id-root ?id1 year|month|day)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arpiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp   offer08   "  ?id " arpiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-08-2014)
;Dear Lord, we offer up our prayers. [Veena mam Translation]
;he BagavAna, hama apanI prArWanA Apako arpaNa karawe hE.

(defrule offer7
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id1 up)
(id-root ?id2 prayer)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 arpaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " offer.clp  offer7  "  ?id "  " ?id1 "  arpaNa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-11-2014)
;Can I offer you some wine? [OALD]
;kyA mEM wumahe Wodi-sI vAina xe sakawI hUz. [Own Manual]
(defrule offer8
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object_2  ?id ?id1)
(id-root ?id1 wine|juice|tea|coffee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer8   "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-11-2014)
;Most of the non-metals like glass, porcelain, plastic, nylon, wood offer high resistance to the passage of electricity through them. [NCERT CORPUS]
;काँच, पॉर्सेलेन, प्लास्टिक, नॉयलोन, लकडी जैसी अधिकांश अधातुएँ अपने से होकर प्रवाहित होने वाली विद्युत पर उच्च प्रतिरोध लगाती हैं. [NCERT CORPUS]
;अधिकांश अधातुएँ जैसे काँच, पॉर्सेलेन, प्लास्टिक, नॉयलोन, लकडी अपने से विद्युत प्रवाह पर उच्च प्रतिरोध लगाती हैं. [Self]

(defrule offer9
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 resistance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer9   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-12-2014)
;These offer advice and information to shoppers and traders , and deal with problems and complaints . [deal.clp]
;ये खरीददारों और व्यापारियों को सलाह और सूचना देते हैं और समस्याओं और शिकायतों को सुलझातें हैं. [Manual]

(defrule offer11
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 information|advice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer11   "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;The hotel offers excellent facilities for families. [OALD]
;विश्रामालय ने परिवारों के लिए उत्तम सुविधाएँ प्रदान करी | [Self] 
;The job didn't offer any prospects for promotion. [OALD]
;नौकरी ने तरक्की की संभावनाएँ प्रदान नहीं करी थी | [Self] 

(defrule offer10
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 facility|prospect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer10   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-01-2015)
;This biography offers a few glimpses of his life before he became famous. [Cambridge Advanced Learner's Dictionary]
;यह जीवनी उसके मशहूर होने से पहले उसके जीवन की कुछ झलके प्रस्तुत करती हैं. [Self]

(defrule offer12
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 glimpse)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer12   "  ?id "  praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-03-2015)
;I offered him my commiseration.  [OALD]
;मैंने उसको अपनी हमदर्दी दिखाई.	    [Manual]
(defrule offer13
(declare (salience 5000))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object_2  ?id ?id1)
(id-root ?id1 commiseration)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer13   "  ?id "  xiKA )" crlf))
)

;-------------------Default rules -------------------
(defrule offer6
(declare (salience 4400))
(id-root ?id offer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva_raKa))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offer.clp 	offer6   "  ?id "  praswAva_raKa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  offer.clp     offer6   "  ?id " kA )" crlf))
)

; praswAva_raKanA is better; see paxasuwra: Amba
;default_sense && category=verb	praxAna_kara	0
;"offer","VTI","1.praxAna karanA"
;
;
;LEVEL 
;Headword : offer
;
;Examples --
;
;"offer","N","1.praswAva"
;I am very happy for your kind offer of help.
;mEM sahAyawA_xene ke wumhAre praswAva se bahuwa KuSa hUz.
;She has received an offer for marriage from a boy living in the USA.
;amarIkAnivAsI eka ladZake se use SAxI kA praswAva AyA hE.
;The company made him a tempting offer of a high salary.
;kampanI ne usake sAmane badZe vewana kA eka AkarRaka praswAva raKA hE.
;"offer","VTI","1.sAmane_raKanA"
;ciwra ke liye svitajZaralEMda ke dIlara ne 2 miliyana dOYlara sAmane_raKe.
;--"2.bAwa_uTAnA" <-- sahAyawA_karane kA praswAva_sAmane_raKanA
;You could have offered them help.
;Apa unheM sahAyawA xene kI bAwa uTA sakawe We.
;--"3.peSa_karanA"  <--pIne kA praswAva sAmane raKanA
;Mira offered him a drink.
;mIrA ne usako eka peya peSa kiyA.
;
;ukwa uxAharaNoM se EsA lagawA hE ki aMgrejZI Sabxa 'offer' kA mUla arWa kisI
;praswAva ko xUsare ke vicArArWa sAmane raKanA hE. aMgrejZI Sabxa 'propose'
;kA arWa BI 'praswAva_raKanA' howA hE. wo praSna yaha uTawA hE ki xono meM
;anwara kyA hE ?
;'offer' ke uxAharaNoM para xqRti dAlane para lagawA hE ki yahAz arWa mAwra
;'praswAva_raKanA' nahIM hE apiwu praswAva raKane kA karwA kriyA ko
;sampanna karane meM svayaM kI BUmikA BI spaRta kara xewA hE. 'propose' meM
;'kyA_kiyA_jA_sakawA_hE' kA arWa praXAna hE jabaki 'offer' meM 
;'mEM yaha karane ke liye wEyAra hUz' kA BAva praXAna hE.
;isakA sUwra nimna ho sakawA hE --
;
;sUwra : praswAva^sAmane_raKanA
;
