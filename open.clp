
(defrule open0
(declare (salience 5000))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id opening )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id KulI_jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  open.clp  	open0   "  ?id "  KulI_jagaha )" crlf))
)

;"opening","N","1.KulI_jagaha/Cixra"
;There was a small opening between the trees
;--"2.uxaGAtana"
;The opening received good critical reviews
;--"3.sWAna"
;There are few openings in publishing for new graduates            
;
;
(defrule open1
(declare (salience 4900))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " open.clp	open1  "  ?id "  " ?id1 "  Kola  )" crlf))
)

(defrule open2
(declare (salience 4800))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id opening )
(not (kriyA-object ?id ?));$$$ This fact added by Roja(17-05-14). Suggested by Chaitanya Sir. Counter Ex: Excuse me for opening your letter by mistake.
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kula))
;(assert (id-H_vib_mng ?id ing)) ;Commented by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  open.clp  	open2   "  ?id "  Kula )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  open.clp       open2   "  ?id " ing )" crlf))
))

(defrule open3
(declare (salience 4800));salience changed from 4700 to 4800
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id opening ) ;commented by Shirisha Manju
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
;(assert (id-H_vib_mng ?id ing)) ;Commented by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  open.clp  	open3   "  ?id "  Kola )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  open.clp       open3   "  ?id " ing )" crlf))
))

;Commented by Shirisha Manju
;$$$ Modified by 14anu-ban-09 on 28-07-2014
;Custom is that the Badrinath Temple is opened during the prayer time only. [Parallel Corpus]
;प्रथा  है  कि  बद्रीनाथ  मंदिर  को  केवल  आरती  के  समय  ही  खोला  जाता  है  ।
;This temple is opened only once in a year for the whole day during the birth anniversary of Parashuram on Akshaya Tritiya. [Parallel Corpus]
;यह  मंदिर  साल  में  केवल  एक  बार  अक्षय  तृतीया  को  परशुराम  जयंती  पर  दिन  भर  दर्शनार्थियों  के  लिए  खोला  जाता  है  ।
;Mr. Chen opened the car door for his wife. [OALD]
;He opened the letter and read it. [Own Manual]
;She opened her bag and took out her passport. [OALD]
;usane apanA jolA  KolA Ora apanA pAsaporta bAhara nikAlA. [Own Manual]
;Please unfold this map.
;kqpyA isa mAnaciwra ko Kola xeM
;(defrule open4
;(declare (salience 4600))
;(id-root ?id open)
;?mng <-(meaning_to_be_decided ?id) 
;(or(kriyA-object ?id ?)(kriyA-subject ?id ?id1)) ;added 'kriyA-subject' by 14anu-ban-09
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))) ;added by 14anu-ban-09
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Kola))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open4   "  ?id "  Kola )" crlf))
;)

;$$$ Modified by 14anu-ban-09 on (28-07-2014)
;There are several such mysterious tunnels in this fort of Thalassery that open towards the sea. [Parallel Corpus]
;तलश्शेरी  के  इस  किले  में  कई  ऐसी  रहस्यपूर्ण  सुरंगे  हैं  जो  समुद्र  की  तरफ  खुलती  हैं  ।
;The door opened and Alan walked in. [OALD]
;xarawAjA KulA Ora elana cala Kara nMxara AyA. [Own Manual]
;The doors of the bus open automatically. [OALD]
;basa ke xaravAje svawaH kola gayA. [Own Manual]
(defrule open5
(declare (salience 4500))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1) ;added by 14anu-ban-09
(id-root ?id1 ?str) ;added by 14anu-ban-09
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)));added by 14anu-ban-09
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open5   "  ?id "  Kula )" crlf))
)

(defrule open6
(declare (salience 4400))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open6   "  ?id "  Kola )" crlf))
)

(defrule open7
(declare (salience 4300))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open7   "  ?id "  KulA )" crlf))
)

(defrule open8
(declare (salience 4200))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open8   "  ?id "  KulA )" crlf))
)

;"open","Adj","1.KulA"
;The thief escaped through the open gate.
;--"2.niRkapata"
;He was quite open about his reasons for leaving.
;--"3.anirNIwa"
;The matter was discussed && left open for the time being.
;--"4.AramBa_karanA"
;He opened a conversation on the issue of animal rights.
;
;

;Added by sheetal(5-01-10).
(defrule open9
(declare (salience 4950))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 conversation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AramBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open9   "  ?id "  AramBa_kara )" crlf))
)
;He opened a conversation on the issue of animal rights.



;Dr. Singh said, "We are open to a reasoned debate on all these issues". Added by Sukhada(23-8-11).
(defrule open10
(declare (salience 4950))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open10  "  ?id "  KulA_huA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (15-12-2014)
;@@@ Added by 14anu09[21-06-14]
;I asked him to open up. ;Removed by 14anu-ban-09 on (15-12-2014)
;मैंने उसको खुल जाने के लिए कहा .  ;Removed by 14anu-ban-09 on (15-12-2014)
;They asked me to open up. ;Added by 14anu-ban-09 on (15-12-2014)
;उसने मुझे खुलकर बोलने के लिए कहा. 		;Added by 14anu-ban-09 on (15-12-2014)
(defrule open011
(declare (salience 5000))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
;(kriyA-anaBihiwa_subject  ?id ?id2) ;commented by 14anu-ban-09 on (15-12-2014)
(kriyA-subject ?id ?id2) ;Added by 14anu-ban-09 on (15-12-2014)
(id-root ?id2 he|him|she|her|they|them)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kula_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " open.clp	open011  "  ?id "  " ?id1 "  Kula_jA  )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (15-12-2014)
;Corrected spelling from 'KOla_xe' to 'Kola_xe'
;@@@ Added by 14anu09[21-06-14]
;They asked me to open up. ;Removed by 14anu-ban-09 on (15-12-2014)
;उसने मुझे खुलने के लिए कहा. ;Removed by 14anu-ban-09 on (15-12-2014)
;I asked him to open up. ;Added by 14anu-ban-09 on (15-12-2014)
;मैंने उसको खुल जाने के लिए कहा . ;Added by 14anu-ban-09 on (15-12-2014)
;उसने मुझे खोल दने के लिए कहा. [Self] ;Improved translation by 14anu-ban-09 on (15-12-2014) 
(defrule open012
(declare (salience 5000))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
;(kriyA-anaBihiwa_subject  ?id ?id2) ;commented by 14anu-ban-09 on (15-12-2014)
(kriyA-subject ?id ?id2) ;Added by 14anu-ban-09 on (15-12-2014)
(id-root ?id2 I|we|us|me)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kola_xe)) ;changed meaning 'KOla_xe' to 'Kola_xe' by 14anu-ban-09 on (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " open.clp	open012  "  ?id "  " ?id1 "  Kola_xe  )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;We are in September, the bazaar will remain open only for a few more days. [Gyannidhi]
;अभी सितम्बर है, बाजार कुछ अधिक दिनों तक ही खुला रहेगा . 
(defrule open11
(declare (salience 4800))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(kriyA-upasarga  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open11  "  ?id "  KulA )" crlf))
)




;@@@ Added by Sonam Gupta MTech IT Banasthali 2-1-2014
;We can ask the gardeners to gather the buds just as they are about to open every evening. [Gyannidhi]
;हम माली को कलियाँ इकठ्ठा करने के लिये कह सकते हैं जैसे ही वे प्रत्येक सन्ध्या खिलने वाली हो 
(defrule open12
(declare (salience 5500))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 bud)
(saMjFA-to_kqxanwa  ?adv ?id)
(kriyA-samakAlika_kriyA  ?vrb ?adv)
(kriyA-object  ?vrb ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open12  "  ?id "  Kila )" crlf))
)

;@@@ Added by 14anu11
;Unsung Heroes : Shooter Abhinav Bindra with his junior world record and senior World Cup medal and badminton player 
;Abhinn Shyam Gupta who won the French Open title .
;गुमनाम नायकः निशानेबाज अभिनव बिंद्रा के पास जूनियर विश्व रिकॉर्डऋ और सीनियर विश्व कप खिताब है , तो बैडऋमिंटन खिलडऋई अभिऋ श्याम गुप्त ने ऋएंच ओपन खिताब जीता .
(defrule open13
(declare (salience 5000))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) title)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aOpin))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open13   "  ?id "  aOpin )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;NOTE- There is a parser problem in this sentence.So, run this sentence on parser no. 9.
;His eyes snapped open. [OALD]
;उसकी आँखे एकदम/झट से खुल गई. [Self]

(defrule open14
(declare (salience 5050))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kula_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open14  "  ?id "  Kula_jA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-01-2015)
;They asked me to open up. [ask.clp]
;उन्होनें मुझे खुलकर बोलने के लिए कहा. [Self]

(defrule open015
(declare (salience 4050))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kula_kara_bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " open.clp  open015  "  ?id "  " ?id1 "  Kula_kara_bola )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-01-2015)
;The story opens with a murder. 	[OALD]
;कहानी खून से शुरू  होती है.			[Self]
;How does the play open?		[OALD]
;खेल कैसे शुरू होता है?				[Self]

(defrule open15
(declare (salience 4800))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 story|play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SurU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open15  "  ?id "  SurU_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-02-2015)
;Mary opened the car door. 			[Hinkhoj]
;मेरी ने कार का दरवाजा खोला . 			[Self]

(defrule open16
(declare (salience 4500))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-cat_coarse ?id2 PropN) 
(kriyA-object ?id ?id1) 
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp 	open16   "  ?id "  Kola )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  open.clp      open16   "  ?id " ne )" crlf))
)


;@@@ Added by 14anu-ban-09 on (05-02-2015)
;He opened a new business.		[Hinkhoj.com]
;उसने नया उद्योग  प्रारम्भ किया.   		[Self]

(defrule open17
(declare (salience 4950))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 business)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open17   "  ?id "  prAramBa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-02-2015)
;This research opens the possibility of being able to find a cure for the disease.	[cald]
;यह शोध बीमारी की चिकित्सा को पाने में समर्थ होने की सम्भावना का पता लगाता है .   		[Self]

(defrule open18
(declare (salience 4950))
(id-root ?id open)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 research)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  open.clp      open18   "  ?id "  pawA_lagA )" crlf))
)

