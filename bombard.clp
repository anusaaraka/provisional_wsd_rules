;@@@ Added by 14anu-ban-02 (02-12-2014)
;Sentence: Which is still present in its original form even though British had bombarded it .[tourism]
;Translation: जो आज भी अपनी पहले की स्थिति में मौजूद है जबकि 1857 में इस किले पर अंग्रेजों ने गोले बरसाए थे .[tourism]
(defrule bombard0 
(declare (salience 0)) 
(id-root ?id bombard) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id golA_barasA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bombard.clp  bombard0  "  ?id "  golA_barasA )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (02-12-2014)
;The cyclotron is used to bombard nuclei with energetic particles, so accelerated by it, and study the resulting nuclear reactions.[ncert]
;साइक्लोट्रॉन का उपयोग इसमें त्वरित ऊर्जायुक्त कणों द्वारा नाभिक पर बमबारी करके परिणामी नाभिकीय अभिक्रियाओं का अध्ययन करने के लिए किया जाता है.[ncert]
;साइक्लोट्रॉन ऊर्जायुक्त कणों से नाभिक पर बमबारी करने के लिये उपयोग होते है,तभी ऊर्जायुक्त कणों से त्वरित होते है,और नाभिकीय अभिक्रियाओं का परिणाम का अध्ययन करते है.[manual]
;साइक्लोट्रॉन अपने द्वारा त्वरित ऊर्जायुक्त कणों से नाभिक पर बमबारी करने पर परिणामी नाभिकीय अभिक्रियाओं का अध्ययन करने के लिए प्रयोग किया जाता है.[manual]
(defrule bombard1 
(declare (salience 100)) 
(id-root ?id bombard) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 particle)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bamabArI_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bombard.clp  bombard1  "  ?id "  bamabArI_kara )" crlf)) 
) 
