;@@@Added by 14anu-ban-02(06-02-2015)
;Sentence: We are flying at an altitude of 6 000 metres.[oald]
;Translation: हम 6 000 मीटर कि ऊँचाई पर उड़ रहे हैं.[manual]
(defrule altitude0 
(declare (salience 0)) 
(id-root ?id altitude) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id UzcAI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  altitude.clp  altitude0  "  ?id "  UzcAI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(06-02-2015)
;Sentence:The athletes trained at altitude in Mexico City.[oald]	;run the sentence on parser no. 231.
;Translation:मेक्सिको शहर में व्ययामियों को ऊँचे स्थान पर प्रशिक्षित किया जाता है.[manual]
(defrule altitude1 
(declare (salience 100)) 
(id-root ?id altitude) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-at_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id Uzce_sWAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  altitude.clp  altitude1  "  ?id "  Uzce_sWAna )" crlf)) 
) 
