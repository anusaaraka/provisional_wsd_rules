
;###COUNTER EXAMPLE### This is an example of the application of the superposition principle. [NCERT]
;###COUNTER EXAMPLE###यह अध्यारोपण सिद्धान्त के एक अनुप्रयोग का उदाहरण है.[NCERT]
;$$$ Modified by 14anu-ban-02(4.08.14)
;Removed (id-word =(+ ?id 1) of) condition
;This is the Pascal's law for transmission of fluid pressure and has many applications in daily life.[NCERT corpus]
;तरल दाब के संचरण के लिए यह "पास्कल का नियम" है तथा दैनिक जीवन में इसके कई उपयोग हैं.[NCERT corpus]
;In practice, it has a large number of useful applications and can help explain a wide variety of phenomena for low viscosity incompressible fluids.[NCERT corpus]
;वास्तव में इसके कई उपयोग हैं जो कम श्यानता तथा असंपीड्य तरलों की बहुत सी घटनाओं की व्याख्या कर सकते हैं.[NCERT corpus]
(defrule application0
(declare (salience 4000))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?) ;added by 14anu-ban-02 on (09-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp 	application0   "  ?id "  upayoga )" crlf))
)



;Added by sheetal
;Please enclose a curriculum vitae with your letter of application .
(defrule application2
(declare (salience 4950))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1  letter)
(viSeRya-of_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application2   "  ?id "  - )" crlf))
)

;$$$Modified by 14anu-ban-02(15-01-2015)
;forward is added in the list.
;He forwarded the application to magistrate. 
;वह मैजिस्ट्रेट को आवेदन-पत्र बढाया . 
;$$$ Modified by 14anu07 on 30/06/2014 -- added 'consider' to the list
;Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 18-nov-2013
;They accepted some applications and rejected others.[old]
;उन्होंने कुछ आवेदन-पत्र को स्वीकार किया और अन्य नामञ्जूर किए 
;Argentina has submitted an application to host the World Cup.[cambridge]
;अर्जेंटीना ने विश्व कप की मेजबानी के लिए एक आवेदन-पत्र जमा किया है
(defrule application3
(declare (salience 5500))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ?id1 ?id)(kriyA-subject ?id1 ?id))
(id-root ?id1 submit|reject|accept|fill|send|consider|forward);added send in the list by Garima Singh. eg: I've sent off applications for four different jobs.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana-pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application3   "  ?id "  Avexana-pawra )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 8-jan-2014
;The judge refused her application for bail.
;न्यायाधीश ने जमानत के लिए उसकी अर्जी को नामजूंर कर दिया.
(defrule application4
(declare (salience 5500))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?kri ?id1)
(id-word ?id1 bail)
(kriyA-object  ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application4   "  ?id "  arjI )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 16-jan-2014
;My application got lost in the post.
;मेरा आवेदन पञ डाक में खो गया
(defrule application5
(declare (salience 5500))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana-pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application5   "  ?id "  Avexana-pawra )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 15/03/2014
;Applications must be in by April 30.[oald]
;आवेदन पञ ३० अप्रैल तक पहुँच जाने चाहिये
(defrule application6
(declare (salience 5500))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id)
(kriyA-in_by_saMbanXI  ?kri ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana-pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application6   "  ?id "  Avexana-pawra )" crlf))
)


;@@@ Added by 14anu-ban-02(04-08-2014)
;The key property of fluids is that they offer very little resistance to shear stress; their shape changes by application of very small shear stress.[NCERT corpus]  
;तरलों का मूल गुण यह है कि वह विरूपण प्रतिबल का बहुत ही न्यून प्रतिरोध करते हैं ; फलतः थोड़े से विरूपण प्रतिबल लगाने से भी उनकी आकृति बदल जाती है.[NCERT corpus]
(defrule application7
(declare (salience 4000))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp 	application7   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-02(04-08-2014)
;Another restriction on application of Bernoulli theorem is that the fluids must be incompressible, as the elastic energy of the fluid is also not taken into consideration.[NCERT corpus]
;बर्नूली प्रमेय पर एक और प्रतिबंध है कि यह असंपीड्य तरलों पर ही लागू होता है, क्योंकि तरलों की प्रत्यास्थ ऊर्जा को नहीं लिया गया है.[NCERT corpus]
(defrule application8
(declare (salience 4100))
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 theorem)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp 	application8   "  ?id "  lAgU )" crlf))
)

;commented by 14anu-ban-02(15-01-2015)
;correct meaning is coming from application3
;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
; He forwarded the application to magistrate. 
;वह मैजिस्ट्रेट को आवेदन-पत्र बढाया . 
;(defrule application9
;(declare (salience 5000))
;(id-root ?id application)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id ?)
;(id-word =(+ ?id 1) to)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Avexana-pawra))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp       application9   "  ?id "  Avexana-;pawra )" crlf))
;)




;*******************DEFAULT RULES**********************************

;These are some of the applications of space science.
(defrule application1
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id application)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  application.clp 	application1   "  ?id "  prayoga )" crlf))
)
;default_sense && category=noun	prArWanA_pawra	0
;"application","N","1.prArWanA_pawra"
;Application forms are available with the booking clerk from 11.00 to 16.00 hr.
;Avoid frequent application of the brakes .
;--"2.prayoga"
;The application of 'BURNOL' on burns is in common use nowadays .
;Street application of traffic rules avoid so many accidents .
;
;
