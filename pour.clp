



;Added by Sonam Gupta MTech IT Banasthali 2013
;We can't go out in this weather - it's pouring! [Cambridge]
;हम इस मौसम में बाहर नहीं जा सकते हैं- यह मूसलाधार वर्षा हो रही है!
(defrule pour1
(declare (salience 4900))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(and(kriyA-subject  ?id ?)(kriyA-vAkyakarma  ?id ?))(and(kriyA-aXikaraNavAcI  ?id ?)(kriyA-object  ?id ?)(kriyA-subject  ?id ?)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUsalAXAra_varRA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour1   "  ?id "  mUsalAXAra_varRA_ho )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;I poured the milk into a jug. [Cambridge]
;मैंने सुराही के अन्दर दूध बहाया . 
(defrule pour2
(declare (salience 4800))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-into_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahAyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour2   "  ?id "  bahAyA )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2013
;The crowd poured out into the street. [Cambridge]
;भीड सडक के अन्दर से बाहर उमडी . 
(defrule pour3
(declare (salience 4700))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 crowd|letters|station)
(or(and(kriyA-upasarga  ?id ?)(kriyA-subject  ?id ?id1)(kriyA-into_saMbanXI  ?id ?))(and(kriyA-upasarga  ?id ?)(kriyA-subject  ?id ?id1)(kriyA-kriyArWa_kriyA  ? ?id)(to-infinitive  ? ?id))(and(kriyA-subject  ?id ?)(kriyA-kqxanwa_karma  ? ?id)(kriyA-of_saMbanXI  ?id ?id1)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id umadI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour3   "  ?id "  umadI )" crlf))
)


;$$$ Modified by Shruti Singh MTech CS Banasthali 28-08-2016
;Pour three to four drops of lemon juice in that.[health corpus]
;wIna cAra bUxa nIbU kA rasa dAlie.
;Added by Sonam Gupta MTech IT Banasthali 2013
;Can I pour you a drink?  [OALD]
;क्या मैं आपके लिये पेय या शरबत डाल सकता हूँ?
(defrule pour4
(declare (salience 6000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 you|drink|tea|coffee|milk|water|softdrink|lemonade|cup|glass|drop|lemon)
(kriyA-object ?id ?id1) ;added by Shruti Singh MTech CS Banasthali 2016
;(or(and(kriyA-kqxanwa_karma  ?id ?id1)(kriyA-subject  ?id ?))(and(kriyA-object  ?id ?id1)(kriyA-upasarga  ?id ?)(kriyA-subject  ?id ?)(kriyA-kqxanwa_karma  ? ?id))(and(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?)(kriyA-for_saMbanXI  ?id ?))(and(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?))(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-kriyA_viSeRaNa  ?id ?)(kriyA-into_saMbanXI  ?id ?id1))) ;commented by Shruti Singh MTech CS Banasthali 2016
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour4   "  ?id "  dAla )" crlf))
)


;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;Golden sunshine poured down. [Gyannidhi]
;सुनहरी धूप निकली.
(defrule pour5
(declare (salience 4500))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 down)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nikalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pour.clp	pour5  "  ?id "  " ?id1 "  nikalI )" crlf))
;changed 'poke.clp	pour5' to 'pour.clp	poke1' by 14anu-ban-09 on (02-04-2015)
)	

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;Pour the sauce over the pasta. [oald]
;पास्ता पर चटनी डालिए.		[Manual]

(defrule pour7
(declare (salience 5000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI  ?id ?id1)
(id-root ?id1 pasta|bread|jam)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour7   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;Pour away as much fat as possible from the roasting pan.  [oald]
; भर्जन पात्र/भुनने की कढाई से जितनी सम्भव उतनी चिकनाई बाहर निकाल दीजिए .                       [Manual]

(defrule pour8
(declare (salience 5000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 pan|vessel)	
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id2 fat|starch) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour8   "  ?id "  nikAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;Pour this water in a long test tube and shine the laser light from top, as shown in Fig. 9.13 (c).   [ncert]
;इस जल को एक लम्बी परखनली में उलटिए तथा लेसर प्रकाश को इसके ऊपर से डालिए जैसा कि चित्र 9.13 ( c ) में दर्शाया गया है .           [ncert]

(defrule pour9
(declare (salience 5000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 water|oil|milk)	
(viSeRya-in_saMbanXI  ?id1 ?id2)
(id-root ?id2 tube|vessel|pan) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour9   "  ?id "  ulata )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;I've poured you a cup of tea.    [oald]
;मैं आपके लिए एक कप में चाय डाल चुका हूँ .           [ncert]

(defrule pour10
(declare (salience 5000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id2 tube|vessel|flask|cup|glass) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour10   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;The rain continued to pour down. [oald]
;वर्षा तेज होना शुरू हो गई थी .                            	     [Manual]

(defrule pour11
(declare (salience 4500))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
(kriyA-subject ?id ?id2)
(id-root ?id2 rain)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 weja_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pour.clp	pour11  "  ?id "  " ?id1 "  weja_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;NOTE-Parser problem. As parser not treating 'with' as a particle. So, rule not fire. Otherwise rule is ok!
;It's pouring with rain. [oald]
;वर्षा तेज हो रही हैं.	                   [manual]

(defrule pour12
(declare (salience 4500))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 with)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 weja_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pour.clp	pour12  "  ?id "  " ?id1 "  weja_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;It's pouring outside.	[oald]
;मूसलाधार वर्षा हो रही हैं . 		[Manual]

(defrule pour13
(declare (salience 4500))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-aXikaraNavAcI  ?id ?id1)
(id-root ?id1 outside)
(kriyA-subject  ?id ?id2)	 
(not(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mUsalAXAra_varRA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pour.clp	pour13  "  ?id "  " ?id1 "   mUsalAXAra_varRA_ho )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule pour0
(declare (salience 5000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUsalAXAra_varRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour0   "  ?id "  mUsalAXAra_varRA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;NOTE-Example sentence need to be added.

(defrule pour6
(declare (salience 0000))
(id-root ?id pour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pour.clp 	pour6   "  ?id "  baha )" crlf))
)

;"pour","V","1.bahanA"
;His tears were pouring down while he heard that news.
;--"2.bahA xenA"
;She poured out all the remaining water. 
;--"3.mUsalaXAra barasanA"
;It's pouring outside.
;--"4.pravAhiwa_honA"
;Commuters were pouring in to the station.
;
;
