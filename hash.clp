;@@@ Added by 14anu-ban-06 (02-04-2015)
;Please press the hash key to continue.(cambridge)[parser no.- 4]
;कृपया जारी रखने के लिए हैश दबाइए . (manual)
(defrule hash1
(declare (salience 2000))
(id-root ?id hash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 key|symbol)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hESa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hash.clp 	hash1   "  ?id "  hESa )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-04-2015)
;He made a complete hash of the last question.(cambridge)
;उसने आखिरी प्रश्न में सब गडबड कर दी. (manual)
(defrule hash2
(declare (salience 2000))
(id-root ?id hash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 question|problem)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadZabadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hash.clp 	hash2   "  ?id "  gadZabadZa )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-04-2015)
;You two hash out the details of the presentation.(cambridge)
;आप दोनो प्रतिपादन के विवरण पर विचार-विमर्श करें . (manual)
(defrule hash3
(declare (salience 2100))
(id-root ?id hash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra-vimarSa_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hash.clp	hash3  "  ?id "  " ?id1 "  vicAra-vimarSa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hash.clp       hash3   "  ?id " para )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-04-2015)
;The first interview was all right but I think I hashed up the second one. (cambridge)
;पहला साक्षात्कार ठीक था परन्तु मुझे लगता हैं कि मैंने दूसरा वाला बिगाड दिया . (manual)
(defrule hash4
(declare (salience 2200))
(id-root ?id hash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bigAdZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hash.clp	hash4  "  ?id "  " ?id1 "  bigAdZa_xe  )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

;@@@ Added by 14anu-ban-06 (02-04-2015)
;Eggs and hash. (cambridge)             --------------[parser no.-3]
;अण्डे और कीमा . (manual)
;You can buy lumps of hash in the markets .(COCA)
;बाजारों में आप कीमे के ढेर खरीद सकते हैं . (manual)
(defrule hash0
(declare (salience 0))
(id-root ?id hash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hash.clp 	hash0   "  ?id "  kImA )" crlf))
)

