;Added by Meena(4.3.11)
;Golconda fort is situated on the western outskirts of hyderabad. 
(defrule situate0
(declare (salience 5000))
(id-root ?id situate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id situated)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id sWiwa))
(assert (id-wsd_root_mng ?id hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  situate.clp     situate0   "  ?id " hE  )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha Banasthali Vidyapith (18.07.2014)
;Who told Ram he should situate himself thus? [COCA]
;kisane rAma ko bawAyA ki use svayaM ko isa prakAra basanA cAhie?[MANUAL]
(defrule situate2
(declare (salience 8000))
(id-root ?id situate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 her|herself|him|himself|them|themselves)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id basa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situate.clp   situate2   "  ?id " basa )" crlf))
)


;---------------------- Default rules -----------------------
;Added by Meena(4.3.11)
;They plan to situate the bus stop at the corner of the road.
;To understand this issue, it must first be situated in its context.
(defrule situate1
(declare (salience 4900))
(id-root ?id situate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situate.clp    situate1   "  ?id "  raKa )" crlf))
)


;@@@ Added by 14anu-ban-05 Prajna Jha M.Tech CS Banasthali Vidyapith on 17.07.2014
;Ayodhya is situated at the banks of the sacred river Sarayu. 
;ayoXyA paviwra naxI sarayU ke wata para sWiwa hE.
(defrule situate01
(declare (salience 5500))
(id-root ?id situate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not (kriyA-object ?id ? ))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  situate.clp     situate01   "  ?id " sWiwa  )" crlf))
)
