;@@@ Added by 14anu-ban-03 (16-02-2015)
;She has got chubby cheeks. [hinkhoj]
;उसके गोलमटोल गाल है. [manual]
(defrule cheek0
(declare (salience 00))
(id-root ?id cheek)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cheek.clp 	cheek0   "  ?id "  gAla )" crlf))
)

;NOTE: working on parser no.29
;@@@ Added by 14anu-ban-03 (16-02-2015)
;The poor lived cheek by jowl in industrial mining towns in Victorian England. [cald]
;विक्टोरिया इंग्लैंड के औद्योगिक खान खोदान नगरों में गरीब घुलमिलकर रहते थे. [manual]
(defrule cheek1
(declare (salience 100))
(id-root ?id cheek)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-by_saMbanXI ?id ?id1)
(id-root ?id1 jowl)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Gulamilakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cheek.clp   cheek1  "  ?id "  " ?id1 "  Gulamilakara  )" crlf))
)
