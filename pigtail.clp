;-----------------------------DEFAULT RULE------------------------------------------------------------------------------------
(defrule pigtail0
(declare (salience 0000))
(id-root ?id pigtail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotI_cotI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pigtail.clp 	pigtail0   "  ?id "  CotI_cotI )" crlf))
)
;-----------------------------------------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 6-8-14
;So the ropes are always made of a number of thin wires braided together, like in pigtails, for ease in manufacture, flexibility and strength.[NCERT CORPUS]
;इसलिए व्यापारिक निर्माण में लचक तथा प्रबलता के लिए ऐसे रस्सों को हमेशा वेणी की तरह बहुत से पतले तारों को गुम्फित करके आसानी से बनाया जाता है.
(defrule pigtail1
(declare (salience 4900))
(id-root ?id pigtail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id2)
(kriyA-in_saMbanXI ?id2 ?id)
(id-root ?id1 wire)
(id-root ?id2 braided)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id veNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pigtail.clp 	pigtail1   "  ?id "  veNI )" crlf))
)
