;@@@ Added by 14anu-ban-05 on (04-03-2015)
;Finance for education comes from taxpayers. 	[OALD]
;शिक्षा के लिए धन करदाताओं से आता है.			[MANUAL]

(defrule finance2
(declare (salience 5001))
(id-root ?id finance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finance.clp 	finance2   "  ?id "  Xana )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-03-2015)
;It's about time you sorted out your finances. [OALD]
;लगभग यह समय था कि आप अपनी आमदनी तय कर सकते थे.    [manual]

(defrule finance3
(declare (salience 5002))
(id-root ?id finance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id1 pronoun))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AmaxanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finance.clp 	finance3   "  ?id "  AmaxanI )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-03-2015)
;The building project will be financed by the government. [OALD]
;निर्माण परियोजना के लिए सरकार के द्वारा  आर्थिक प्रबन्ध किया जाएगा.	[MANUAL]

(defrule finance4
(declare (salience 4901))
(id-root ?id finance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArWika_prabanXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finance.clp 	finance4   "  ?id "  ArWika_prabanXa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;"finance","N","1.ArWika_vyavasWA"
;He has financed his studies through loan from bank.
;He has not been able to manage his finances properly resulting in a big loss.
(defrule finance0
(declare (salience 5000))
(id-root ?id finance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArWika_vyavasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finance.clp 	finance0   "  ?id "  ArWika_vyavasWA )" crlf))
)

;"finance","V","1.ArWika_prabanXa"
;The company has arranged finance for their new project through issue of bonds.
(defrule finance1
(declare (salience 4900))
(id-root ?id finance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArWika_prabanXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finance.clp 	finance1   "  ?id "  ArWika_prabanXa )" crlf))
)

