;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;This digital audio player supports multiple formats.
;यह डिजिटल श्राव्य प्लेअर बहुत फॉर्मेट चलाने में समर्थ है . 
(defrule player1
(declare (salience 4400))
(id-root ?id player)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 media|audio|video|music|cassette|record|dvd|mp3|cd)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pleara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  player.clp 	player1   "  ?id "  pleara )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (20-02-2015)
;Every black violin player lost his job. 	[coca]
;प्रत्येक काले सारङ्गी वादक ने अपना काम खो दिया/खोया .  	[self]
;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;He is a very good trumpet player.
;वह एक अत्यन्त अच्छा तुरही वादक है .
(defrule player2
(declare (salience 4400))
(id-root ?id player)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 trumpet|cornet|violin)		;added 'violin' by 14anu-ban-09 on (20-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAxaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  player.clp 	player2   "  ?id "  vAxaka )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;The player in the act did a great job.
;नाटक में अदाकार ने बढिया काम किया .
(defrule player3
(declare (salience 4400))
(id-root ?id player)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 movie|act|play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axAkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  player.clp 	player3   "  ?id "  axAkAra )" crlf))
)


;-------DEFAULT RULE-------
;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;He is the best baseball player.
;वह सर्वोत्तम बेसबाल खिलाडी है.
(defrule player0
(declare (salience 1400))
(id-root ?id player)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KilAdZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  player.clp 	player0   "  ?id "  KilAdZI )" crlf))
)

