;##############################################################################
;#  Copyright (C) 2013-2014  Prachi Rathore (prachirathore02@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by Prachi Rathore[29-1-14]
;She says awful things when she's in a temper. [oald]
;वह अज़ीब सी बातें कहती है जब वह क्रोध में होती है . 
;That's a terrible thing to say![oald]
;वह कहने के लिये एक खराब बात है! 
(defrule thing0
(declare (salience 5500))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object  ?id1 ?id)(saMjFA-to_kqxanwa  ?id ?id1))
(id-root ?id1 say)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing0   "  ?id "  bAwa )" crlf))
)

;@@@ Added by 14anu-ban-07 (12-02-2015)
;You did a beautiful thing in helping those poor children. ( cambridge)
;तुमने गरीब बच्चों कि सहायता करके अच्छा कार्य किया.(manual)
(defrule thing3
(declare (salience 5600))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id2)
(kriyA-object  ?id1 ?id)
(id-root ?id2 help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing3   "  ?id "  kArya )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-03-2015)
;A terrible thing happened last night.(oald)
;पिछली रात एक खराब घटना  हुई . (manual)
(defrule thing4
(declare (salience 4100))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 terrible)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing4   "  ?id "  GatanA )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-03-2015)
;Training isn't the same thing as education.(cambridge)
;प्रशिक्षण शिक्षा के  समान नहीं है . (manual)
(defrule thing5
(declare (salience 4200))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 same)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing5   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-03-2015)
;"Why won't you come to New York with me?" "For one thing, I don't like flying, and for another, I can't afford it."(cambridge)
;"मेरे साथ न्यूयार्क को क्यों नहीं आएँगे आप?" 	"एक कारण , मैं उडना पसन्द नहीं करता हूँ," और दूसरे, मैं इसका खर्च नहीं उठा सकता हूँ ."(manual) 
(defrule thing6
(declare (salience 4300))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
(kriyA-for_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing6   "  ?id "  kAraNa )" crlf))
)
;xxxxxxxxxxxx Default Rule xxxxxxxxxx
;@@@ Added by Prachi Rathore[29-1-14]
;
(defrule thing2
(declare (salience 4000))
(id-root ?id thing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cIjaZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thing.clp 	thing2   "  ?id "  cIjaZ )" crlf))
)
