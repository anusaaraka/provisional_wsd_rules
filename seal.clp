
;$$$ Modified by 14anu-ban-01 on (03-03-2015)
;"sealing","N","1.sIla kA SikArI"
;Sealing is prohibited.[seal.clp]
;सील मछली का शिकार निषेध है . [self]
(defrule seal0
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id sealing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sIla_maCalI_kA_SikAra))	;changed "sIla_kA_SikArI" to "sIla_kA_SikAra" by 14anu-ban-01 on (03-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  seal.clp  	seal0   "  ?id "  sIla_maCalI_kA_SikAra )" crlf))  ;changed "sIla_kA_SikArI" to "sIla_kA_SikAra" by 14anu-ban-01 on (03-03-2015)
)

;@@@ Added by 14anu-ban-01 on (07-03-2015)
;He sealed down the envelope and put a stamp on it.[cald]
;उसने एनवलप बन्द किया और उसपर एक टिकट लगाया . [self]
(defrule seal02
(declare (salience 4900))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 down|up)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " seal.clp 	seal02    "  ?id "  "?id1 "  banxa_kara  )" crlf))
)



;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014*****
;Their fate was sealed.
;उनका भाग्य निश्चित हो गया था . 
;उनका भाग्य निश्चित हो चुका था[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule seal3
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 fate|destiny|future|doom)	;added 'doom' by 14anu-ban-01 on (20-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_ho));changed "niSciwa_ho_gayA" to "niSciwa_ho" by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal3   "  ?id " niSciwa_ho)" crlf));changed "niSciwa_ho_gayA" to "niSciwa_ho" by 14anu-ban-01 on (27-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu-ban-11 on (03-12-2014)
;They drank a glass of wine to seal their new friendship. (oald)
;उन्होंने उनकी नयी मैत्री मजबूत बनाने के लिए वाइन का एक गिलास पिया . (Anusaaraka)
;उन्होंने अपनी नयी दोस्ती को मजबूत करने के लिए वाइन का एक गिलास पिया .[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule seal4
(declare (salience 4900))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 friendship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majabUwa_kara));changed "majabUwa_banA " to "majabUwa_kara" by 14anu-ban-01 on (27-12-2014)
(assert (make_verbal_noun ?id));added by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  seal.clp 	seal4    "  ?id " )" crlf);added by 14anu-ban-01 on (27-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal4   "  ?id "  majabUwa_kara )" crlf));changed "majabUwa_banA " to "majabUwa_kara" by 14anu-ban-01 on (27-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (03-03-2015)
;Inside the sealed plastic, his bread and cheese was squashed but fresh.[coca]
;बंद प्लास्टिक के अन्दर,उसके ब्रेड और चीज मसल जाने के बावजूद  ताजे थे.[self] 
;@@@ Added by 14anu-ban-11 on (03-12-2014)
;The organs are kept in sealed plastic bags.(oald)
;अङ्ग बन्द प्लास्टिक थैलों में रखे गये हैं . (anusaaraka)
(defrule seal5
(declare (salience 400))
(id-word ?id sealed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 bag|plastic)	;added "plastic" by 14anu-ban-01 on (03-03-2015)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id banxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  seal.clp 	seal5   "  ?id "  banxa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (03-03-2015)
;The floors had been sealed with varnish.[self: with reference to oald]
;फर्श वार्निश से ढक दिए गये थे . [self] 
(defrule seal6
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 varnish|wax)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal6   "  ?id " Daka_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on (03-03-2015)
;He sealed victory with a marvellous shot.[self with reference to oald]
;उसने एक शानदार शॉट के साथ विजय पर [अपनी] मोहर लगाई.  [self] 
(defrule seal7
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 victory|triumph|position)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mohara_lagA))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  seal.clp 	seal7   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal7   "  ?id " mohara_lagA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-03-2015)
;Troops have sealed the borders between the countries.[oald]
;दल देशों के बीच सीमाएँ रक्षित कर चुके हैं .   [self] 
(defrule seal8
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 border|boundary|periphery|outskirt)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal8   "  ?id " rakRiwa_kara)" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-03-2015)
;Clean the seal on/around the fridge door regularly so that it remains airtight.[cald]
;नियमित रूप से फ्रिज के दरवाजे पर लगी हुआ सील साफ कीजिए जिससे कि यह वायु रोधक बनी रहे.  [self]
(defrule seal9
(declare (salience 4900))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 door|fridge|window|jar)	;list can be added
(pada_info (group_head_id ?id1) (preposition  ?id2))
(or(viSeRya-on_saMbanXI  ?id ?id1)(viSeRya-around_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 sIla))
(assert (id-wsd_viBakwi ?id1 para_lagA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  seal.clp 	seal9  "  ?id " para_lagA_huA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " seal.clp 	seal9 "  ?id "  "?id2 "   xeKanA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (20-03-2015)
;He sealed his own doom by having an affair with another woman.[oald]
;उसने एक अन्य स्त्री के साथ प्रेम सम्बन्ध रखकर अपने ही दुर्भाग्य को  निश्चित किया था.[self]
(defrule seal10
(declare (salience 5000))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 fate|destiny|future|doom)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal10   "  ?id "  niSciwa_kara)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule seal1
(declare (salience 4900))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mohara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal1   "  ?id "  mohara )" crlf))
)

;"seal","v","1.banxa_karana"
;i sealed an envelope.
(defrule seal2
(declare (salience 4800))
(id-root ?id seal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seal.clp 	seal2   "  ?id "  banxa_kara )" crlf))
)



;"seal","v","1.banxa_karana"
;i sealed an envelope.
;--"2.mohara_lagana"
;official documents are sealed by government.
;--"3.roka_lagana"
;indian government sealed borders of rajasthan for protection.
;
;
;"seal","n","1.mohara[sila_macali]"
;--"2.sila_macali"
;seals are found in coastal areas.
;--"3.mohara/muxra"
;every official paper has the seal of central government on it.
;--"4.vaha_vaswu_jisake_xvara_muhara_lagai_jawi_he"
;i know a person who makes excellent seals.
;--"5.jodzane_ke_lie_prayukwa_hone_vala_paxarwa"
;i bought a seal to fill gap in the water pipe.
;--"6.surakra_ke_lie_prayukwa_hone_vala_paxarwa"
;ballot boxes were closed with a seal.
;
