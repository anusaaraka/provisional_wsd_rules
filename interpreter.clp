;@@@ Added by 14anu-ban-06 (14-04-2015)
;She is one of the finest interpreters of Debussy's music. (OALD)
;वह डिबसी के सङ्गीत के सबसे बढ़िया जानकारो में से एक है . (manual)
(defrule interpreter1
(declare (salience 2000))
(id-root ?id interpreter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 music)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpreter.clp 	interpreter1   "  ?id "  jAnakAra )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (14-04-2015)
;She works as an interpreter in Brussels.(cambridge)
;वह ब्रूसेल्स में एक अनुवादक के रूप में काम करती है . (manual)
(defrule interpreter0
(declare (salience 0))
(id-root ?id interpreter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuvAxaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpreter.clp 	interpreter0   "  ?id "  anuvAxaka )" crlf))
)
