;@@@ Added by 14anu22
(defrule team0
(declare (salience 5000))
(id-root ?id team)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " team.clp team0 " ?id "  xala )" crlf)) 
)

;@@@ Added by 14anu22
(defrule team1
(declare (salience 5000))
(id-root ?id team)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xala_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " team.clp team1 " ?id " xala_banA )" crlf)) 
)

;@@@ Added by 14anu22
(defrule team2
(declare (salience 5000))
(id-root ?id team)
(id-word ?id1 up)
(kriyA-upasarga  ?id ?id1)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) xala_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " team.clp team2 " ?id " " (+ ?id 1) " xala_banA)" crlf)))
