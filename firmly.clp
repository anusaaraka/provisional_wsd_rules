
;@@@ Added by 14anu-ban-05 on (08-12-2014)
;In fact, its concepts and laws were formulated in the nineteenth century before the molecular picture of matter was firmly established.[NCERT]
;vAswava meM, isase sambanXiwa avaXAraNAoM waWA niyamoM kA prawipAxana 19 vIM SawAbxI meM usa samaya huA WA jaba xravya ke ANvika svarUpa ko xqDawApUrvaka pramANiwa nahIM kiyA gayA WA .[NCERT]
(defrule firmly0
(declare (salience 1000))
(id-root ?id firmly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqDawApUrvaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  firmly.clp 	firmly0   "  ?id "  xqDawApUrvaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-12-2014)
;However, when Thomas Young performed his famous interference experiment in 1801, it was firmly established that light is indeed a wave phenomenon.[NCERT]
;waWApi, jaba toYmasa yafga ne san 1801 meM apanA vyawikaraNa sambanXI prasixXa prayoga kiyA waba yaha niSciwa rUpa se pramANiwa ho gayA ki vAswava meM prakASa kI prakqwi warafgavawa hE.[NCERT]
(defrule firmly1
(declare (salience 1000))
(id-root ?id firmly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-vAkyakarma  ?id1 ?id2)
(id-root ?id1 establish)    ;more constraints can be added
(id-root ?id2 phenomenon)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  firmly.clp 	firmly1   "  ?id "  niSciwa_rUpa_se )" crlf))
)
