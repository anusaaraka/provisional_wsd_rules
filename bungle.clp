;@@@Added by 14anu-ban-02(20-04-2015)
;They bungled the job.[oald]
;उन्होंने काम फूहडपने से किया . [self]
(defrule bungle1
(declare (salience 100)) 
(id-root ?id bungle) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(id-root ?id1 job)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id PUhadZapane_se_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bungle.clp  bungle1  "  ?id "  PUhadZapane_se_kara )" crlf)) 
) 
;-------------------default_rules---------------------------------------
;@@@Added by 14anu-ban-02(20-04-2015)
;The government bungled badly in planning the campaign.[mw]
;सरकार ने अभियान की योजना बनाने में बुरी तरह से घपला किया . [self]
(defrule bungle0
(declare (salience 0)) 
(id-root ?id bungle) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id GapalA_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bungle.clp  bungle0  "  ?id "  GapalA_kara )" crlf)) 
) 
