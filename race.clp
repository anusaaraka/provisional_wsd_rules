;@@@ Added by 14anu03 on 01-july-14
;The race course was huge.
;दौड का मैदान विशाल था .
(defrule race101
(declare (salience 5500))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 course)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xOdZa_kA_mExAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  race.clp     race101   "  ?id "  " ?id1 "  xOdZa_kA_mExAna )" crlf))
)

;@@@ Added by 14anu03 on 01-july-14
;Jonathan said you were taking him to some kind of horse race tomorrow.
;जोनाथन ने कहा कि तुम उसको कल घुड़दौड़ दिखाने ले जा रहे थे . 
(defrule race100
(declare (salience 5500))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 horse)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GudZaxOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  race.clp     race100   "  ?id "  " ?id1 "  GudZaxOdZa )" crlf))
)

;@@@ Added by Anita-7.4.2014
;The children came racing. [Advanced Learner's English-Hindi Dictionary] [parse problem]
;बच्चे दौड़ते हुए आए ।
(defrule race0
(declare (salience 5000))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-for_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race0   "  ?id " xOdZa_ho )" crlf))
)


;@@@ Added by Anita-5.4.2014
;This custom is found in people of all races throughout the world.  [oxford learner's dictionary]
(defrule race1
(declare (salience 4900))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race1   "  ?id " varga )" crlf))
)

;@@@ Added by Anita-5.4.2014
;He admired Canadians as a hardy and determined race. [oxford learner's dictionary]
;उसने केनेडियनों की एक साहसी और दृढ़ संकल्पी जाति के रूप में  प्रशंसा की ।
;$$$ Modified by 14anu13 on 01-07-14      [added condition "(viSeRya-of_saMbanXI 	?id 	 ?)"]
;According to the popular belief , they are a race of men changed into monkeys on account of the help which they had afforded to Rama when making war against the demons.
;ऐसा लोक - विश्वास है कि वे मानव जाति के ही थे लेकिन चूंकि उन्होंने उस समय राम की सहायता की थी जब वे राक्षसों से युद्ध कर रहे थे तो उन्हें वानरों में बदल दिया गया था |
(defrule race2
(declare (salience 4800))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-as_saMbanXI  ? ?id)(viSeRya-of_saMbanXI 	?id 	 ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race2   "  ?id " jAwi )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
; @@@ Added by Anita-7.4.2014
;She raced for the one thousand metre race and won the prize. [Advanced Learner's English-Hindi Dictionary}
;वह एक हजार मीटर की दौड़ दौड़ी और पुरस्कार जीत लिया । 
(defrule race3
(declare (salience 4700))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1) ;added '?id1' by 14anu-ban-10 on (11-12-2014)
(id-root ?id1 race) ;added by 14anu-ban-10 on (11-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race3   "  ?id " xOdZa )" crlf))
)

;@@@ Added by Anita-9.4.2014 
;During the FC, a special session was devoted to ' prevention of an arms race in outer space ' on Tuesday October 19. ;[YourDictionary]
;एफ सी के दौरान , मंगलवार अक्टूबर १९ को एक खास बैठक   ' अंतरिक्ष के बाहर हथियारों की स्पर्धा की रोक-थाम ' को समर्पित की गई ।
(defrule race4
(declare (salience 5100))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 prevention)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race4   "  ?id " spARXA )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@ Added by Anita - 9.4.2014
;He has been racing for over ten years. [cambridge dictionary]
;वह दस से ज्यादा वर्षों से दौड़ में भाग ले रहा है ।
(defrule race5
(declare (salience 5200))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-for_saMbanXI  ?id ?) ;commented by 14anu-ban-10 on (11-12-2014)
(kriyA-over_saMbanXI  ?id ?id1 );added by 14anu-ban-10 on (11-12-2014)
(id-root ?id1 year) ;added by 14anu-ban-10 on (11-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa_meM_BAga_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race5   "  ?id " xOdZa_meM_BAga_le )" crlf))
)


;His race of life was much successful.
;उसकी जीवन -यात्रा काफी सफल रही ।

;@@@ Added by 14anu-ban-10 on (03-02-2015)
;A special dairy is being run in the Munnar area under the Indo-Swiss project in which cattles of more than a 100 race have been kept .[tourism corpus]
;मुन्नार  क्षेत्र  में  भारत-स्विस  परियोजना  के  अंर्तगत  एक  खास  डेयरी  चलाई  जा  रही  है  जिसमें  सौ  से  भी  अधिक  नस्लों  के  पशु  रखे  गये  हैं  ।[tourism corpus]
(defrule race6
(declare (salience 5300))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 cattle)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nasla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race6   "  ?id " nasla )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;We should not discriminate people on the grounds of sex,race,culture,or language,etc.[hinkhoj]
;हमें  वर्ग के,जाति,शिष्टता,या भाषा के आधार पर लोगों मे अंतर करना चाहिए . [manual]
(defrule race7
(declare (salience 5500))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  $? ?id $?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race7   "  ?id "  jAwi )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;Thick black clouds raced across the sky.[hinkhoj]
;घने काले बादल आसमान में जल्दी दौड़ते हुए . [manual]
(defrule race8
(declare (salience 5600))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 cloud)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI_xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race8   "  ?id " jalxI_xOdZa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;The race for the presidency.[hinkhoj]
;अध्यक्षता के लिए जाति  . [manual]
(defrule race9
(declare (salience 5700))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 presidency)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race9   "  ?id " jAwi )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;Journalists are an interesting race.[hinkhoj]
;पत्रकार रोचक व्यक्तियों का समूह हैं . [manual]
(defrule race10
(declare (salience 5800))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 interesting)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwiyoM_kA_samUha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race10   "  ?id "  vyakwiyoM_kA_samUha )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;The race of time may leave you behind if you slumber.[hinkhoj]
;समय की प्रजाति   आपको पीछे छोड सकती है यदि आप  निष्क्रिय होते हैं . [manual]
(defrule race11
(declare (salience 5900))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prajAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race11   "  ?id "  prajAwi)" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;Breed a race of cattles which can survive drought.[hinkhoj] 
;पशुओं की  जीव धारियों की श्रेणी का पालन-पोषण करना जो  अकाल के बाद जीवित रह सकते हैं . [manual]
(defrule race12
(declare (salience 6000))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1  cattle)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIva_XAriyoM_kI_SreNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race12   "  ?id " jIva_XAriyoM_kI_SreNI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-03-2015)
;Aryans were the people of ancient and noble race.[hinkhoj]
; प्राचीन भारत के निवासी  पुराने और  भव्य मानव जाति के लोग थे . [manual]
(defrule race13
(declare (salience 6100))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1  noble)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnava_jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race13   "  ?id " mAnava_jAwi)" crlf))
)
;######################################default-rule####################################
;@@@ Added by Anita-5.4.2014
;He is already in training for the big race against Bailey. [oxford learner's dictionary]
;वे बेली के खिलाफ बड़ी दौड़ के लिए प्रशिक्षण में पहले से ही है ।
(defrule race_default-rule
(declare (salience 0))
(id-root ?id race)
?mng <-(meaning_to_be_decided ?id)
;(or(viSeRya-for_saMbanXI  ? ?id)(viSeRya-det_viSeRaNa  ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  race.clp 	race_default-rule   "  ?id " xOdZa )" crlf))
)
