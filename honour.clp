
;@@@ Added by Prachi Rathore[27-3-14]
;Rajesh Gavre, the President of the MNPA teachers association, honoured the school by presenting the award.
;मनपा शिक्षक संघ के अध्यक्ष राजेश गवरे ने स्कूल को भेंट देकर सराहना की।
(defrule honour2
(declare (salience 5000))
(id-root ?id honour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 school)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarAhanA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honour.clp 	honour2   "  ?id "  sarAhanA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  honour.clp 	honour2     "  ?id "  kI )" crlf))
)

;@@@ Added by 14anu-ban-06 (24-01-2015)
;Chief minister Hooda was honoured with a traditional cap and gown on the handloom and handicraft stall of prize winner Gopal Prasad Verma and even on the Baagad print stall of Padmashree Ramkishor Chheepa .(parallel corpus)
;पुरस्कार  विजेता  गोपाल  प्रसाद  वर्मा  के  हथकरघा  और  हस्तशिल्प  स्टॉल  तथा  पद्मश्री  रामकिशोर  छीपा  के  बागड़  प्रिंट  स्टॉल  पर  भी  मुख्यमंत्री  हुड्डा  को  एक  परम्परागत  टोपी  और  गाऊन  से  सम्मानित  किया  गया  ।(parallel corpus)
(defrule honour3
(declare (salience 5000))
(id-root ?id honour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAniwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honour.clp 	honour3   "  ?id "  sammAniwa_kara )" crlf))
)

;---------------------- Default Rules --------------------------

(defrule honour0
(declare (salience 5000))
(id-root ?id honour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honour.clp 	honour0   "  ?id "  sammAna )" crlf))
)

;"honour","N","1.sammAna"
;apane se badZe saBI logo kA'honour'mAna sammAna karanA cAhiye.
;
(defrule honour1
(declare (salience 4900))
(id-root ?id honour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honour.clp 	honour1   "  ?id "  sammAna_kara )" crlf))
)

;"honour","V","1.sammAna_karanA"
;mEM apane saBI mehamAnoM kA 'honoured'(sammAnakarawA) hUz.
;

