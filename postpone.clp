
;-----------------------------DEFAULT RULE--------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;The event has been postponed indefinitely due to lack of interest.	[oald]	;added by 14anu-ban-09 on (11-03-2015)
;कार्यक्रम रूचि की कमी के कारण अनिश्चित काल के लिए आगे बढाया गया है . 		[manual]	;added by 14anu-ban-09 on (11-03-2015)

(defrule postpone0
(declare (salience 000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone0   "  ?id "  Age_baDA )" crlf))
)

;--------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;The game has already been postponed three times.  [oald]
;खेल तीन बार पहले से ही स्थगित कर दिया गया है . [manual]

(defrule postpone1
(declare (salience 1000))  
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-subject ?id ?id1)
(id-root ?id1 meeting|game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWagiwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone1   "  ?id "  sWagiwa_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;We'll have to postpone the meeting until next week. [oald]
;हमें अगले सप्ताह तक बैठक स्थगित करनी पडेगी .  [manual]

(defrule postpone2
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-object ?id ?id1)
(id-root ?id1 meeting|game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWagiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone2  "  ?id "  sWagiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-09 on (10-03-2015)
;They have agreed to postpone repayment of the loan to a future unspecified date.  [oald]
;वे एक भविष्य अनिर्दिष्ट तिथि को ऋण का पुनर्भुगतान विलम्ब करने के लिए मान चुके हैं .	[manual]

(defrule postpone3
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-object ?id ?id1)
(id-root ?id1 repayment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vilaMba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone3 "  ?id "  vilaMba_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;It was an unpopular decision to postpone building the new hospital.  [oald]
;यह नया अस्पताल बनाने को टाल देना अलोकप्रिय निर्णय था . [manual]

(defrule postpone4
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(saMjFA-to_kqxanwa  ?id1 ?id)
(id-root ?id1 decision) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone4 "  ?id "  tAla_xe )" crlf))
)


;@@@ Added by 14anu-ban-09 on (11-03-2015)
;Ruth wrote at once, asking Maria to postpone her visit. 	[oald]
;रूथ ने  एकाएक लिखा था कि मरीअ को उसके आने में देर करने को कहा हुआ . 	[manual]

(defrule postpone5
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-object ?id ?id1)
(id-root ?id1 visit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xera_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone5 "  ?id "  xera_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-03-2015)
;The inevitable conflict was merely postponed till the next meeting.   [oald]
;अपरिहार्य विरोध को महज़ अगली बैठक तक स्थगित कर दिया गया था . 		       [manual]

(defrule postpone6
(declare (salience 1000))  
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-till_saMbanXI ?id ?id1)
(id-root ?id1 meeting|game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWagiwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone6   "  ?id "  sWagiwa_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-03-2015)
;We've had to postpone going to France because the children are ill.   [cald]
;हमने फ्रांस जाना टाल दिया क्योंकि बच्चे अस्वस्थ हैं .  			       [manual]

(defrule postpone7
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-kqxanwa_karma  ?id ?id1)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id2 PropN) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone7 "  ?id "  tAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-03-2015)
;They decided to postpone their holiday until next year. 	       [cald]
;उन्होंने उनका अवकाश अगले वर्ष तक के लिए टाल देने का फैसला किया . 		       [manual]

(defrule postpone8
(declare (salience 1000))
(id-root ?id postpone)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 holiday|trip) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  postpone.clp 	postpone8 "  ?id "  tAla_xe )" crlf))
)

