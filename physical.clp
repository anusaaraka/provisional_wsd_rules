
(defrule physical0
(declare (salience 0000)); salience reduced by 14anu-ban-10
(id-root ?id physical)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) characteristics)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BOwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  physical.clp 	physical0   "  ?id "  BOwika )" crlf))
)

(defrule physical1
(declare (salience 4900))
(id-root ?id physical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BOwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  physical.clp 	physical1   "  ?id "  BOwika )" crlf))
)

(defrule physical2
(declare (salience 4800))
(id-root ?id physical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SArIrika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  physical.clp 	physical2   "  ?id "  SArIrika )" crlf))
)

;"physical","Adj","1.SArIrika"
;Physical Education is must to every student.
;--"2.BOwika"
;It is a physical impossibility to be in two places at once.
;
;
;@@@ Added by 14anu-ban-10 on (18-8-14)
;One kind of response from the earliest times has been to observe the physical environment carefully, look for any meaningful patterns and relations in natural phenomena, and build and use new tools to interact with nature. 
;Axi kAla - se prawikriyA eka prakAra kA sAvaXAnI se BOwika paryAvaraNa xeKane vAlA, prAkqwika pariGatanA meM kisI arWapUrNa namUne Ora sambanXa DUzDanA, Ora banAnA Ora upayoga karanA naye OjAra prakqwi ke sAWa prawikriyA karane ke lie huA hE .
(defrule physical3
(declare (salience 5000))
(id-root ?id physical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 environment)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BOwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  physical.clp 	physical3  "  ?id " BOwika )" crlf))
)
