;@@@ Added by 14anu-ban-01 on (14-11-2014)
;Smoking is strictly forbidden.[oald]
;धूम्रपान पूरी तरह से/पूर्णतः निषेध  है .[self]
;Soy-based infant formula (SBIF) is sometimes given to infants who are not being strictly breastfed.[agriculture corpus]
;सोया आधारित शिशु फार्मूला ( SBIF ) कभी कभी उन शिशुओं को  दिया जाता है जिन्हे  पूरी तरह से  स्तनपान नहीं कराया जाता है.[manual]
(defrule strictly0
(declare (salience 0))
(id-root ?id strictly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrI_waraha_se/pUrNawaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strictly.clp 	strictly0   "  ?id "  pUrI_waraha_se/pUrNawaH )" crlf))
)


;@@@Added by 14anu-ban-01 on (14-11-2014)
;She was brought up very strictly.[oald]
;उसका पालन-पोषण बहुत/काफी सख्ती से किया गया था.[self]
(defrule strictly1
(declare (salience 100))
(id-root ?id strictly)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 bring)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saKwI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strictly.clp 	strictly1   "  ?id "  saKwI_se )" crlf))
)
