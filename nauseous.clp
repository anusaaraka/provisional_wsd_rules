;@@@ Added by 14anu03 on 21-june-14
;That smell is making me nauseous.
;वह गन्ध मुझे उल्टी जैसा बना रही है .
(defrule nauseous1
(declare (salience 6500))
(id-root ?id nauseous)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 me)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ultI_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nauseous.clp      nauseous1   "  ?id " ultI_jEsA)" crlf))
)


;@@@ Added by 14anu-ban-08 (09-12-2014)
;He felt nauseous due to the smell.     [OALD]
;उसे गंध के कारण घृणाजनक महसूस हुआ.      [Self]
(defrule nauseous0
(declare (salience 0))
(id-root ?id nauseous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GqNAjanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nauseous.clp      nauseous0   "  ?id " GqNAjanaka )" crlf))
)
