;Commented by 14anu-ban-01 on (11-12-2014) because the meaning can come from default rule and the rule is unnecessary.Also,the example given is incomplete and not correct.
;$$$ Modified by 14anu17
;If you are not satified with there response.
;(defrule satisfy0
;(declare (salience 5000))
;(id-root ?id satisfy)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id satisfied )
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id saMwuRta))
;(assert (id-H_vib_mng ?id yA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  satisfy.clp  	satisfy0   "  ?id "  saMwuRta )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  satisfy.clp    satisfy0   "  ?id " yA )" crlf))
;)

;@@@ Added by 14anu-ban-01 on (11-12-2014)
;He was satisfied only after seeing all the details.[self]
;वह सभी बारीकियों को देखने के बाद ही संतुष्ट हुआ.[self]
(defrule satisfy3
(declare (salience 4800))
(id-root ?id satisfy)
?mng <-(meaning_to_be_decided ?id)
(kriyA-after_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwuRta_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  satisfy.clp 	satisfy3   "  ?id "  sanwuRta_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-12-2014)
;She has satisfied the conditions for entry into the college.[satisfy.clp]
;उसने कॉलेज में प्रवेश /दाखिले के लिये सारी औपचारिकताएँ पूरी कर दी हैं.[self]
(defrule satisfy4
(declare (salience 4800))
(id-root ?id satisfy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 condition|formality|term)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  satisfy.clp 	satisfy4   "  ?id "  pUrI_kara )" crlf))
)

;--------------------- Default rules --------------------

(defrule satisfy1
(declare (salience 4900))
(id-root ?id satisfy)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id satisfied )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMwuRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  satisfy.clp  	satisfy1   "  ?id "  saMwuRta )" crlf))
)

;"satisfied","Adj","1.saMwuRta"
;Labour is satisfied with their salary.
;

;Translation added by 14anu-ban-01 on (11-12-2014)
;The explanation could not satisfy him. [self:with reference to COCA]
;स्पष्टीकरण उसे संतुष्ट नहीं कर सका [self]
(defrule satisfy2
(declare (salience 0));Salience reduced to 0 from 4800 by 14anu-ban-01 on (11-12-2014)
(id-root ?id satisfy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwuRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  satisfy.clp 	satisfy2   "  ?id "  sanwuRta_kara )" crlf))
)

;"satisfy","V","1.sanwuRta_karanA"
;She wasn't satisfied by my explanation. 
;--"2.pUrA_karanA"
;She has satisfied the conditions for entry into the college.
;--"3.sanxeha_xUra_karanA"
;My assurances don't satisfy him, he's still sceptical?.
;

;Commented by 14anu-ban-01 on (11-12-2014) as it is same as satisfy1
;Add by samapika (29.3.10)
;Labour is satisfied with their salary.
;(defrule satisfy3
;(declare (salience 4900))
;(id-root ?id satisfy)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id satisfied)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id saMwuRta))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  satisfy.clp   satisfy3   "  ?id "  saMwuRta )" crlf))
;)
