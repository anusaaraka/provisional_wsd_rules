;@@@ Added by 14anu-ban-05 on (15-10-2014)
;Start the stop-watch and note the reading of the thermometer after fixed interval of time, say after every one minute of stirring gently with the stirrer.[NCERT]
;virAma GadI calAie waWA prawyeka niyawa samaya anwarAla jEse 1 minata ke paScAw vidolaka se XIre-XIre vidoliwa karawe hue wApamApI ke pATyAfka nota kIjie. [ncert]
(defrule gently2
(declare (salience 5000))
(Domain physics)
(id-root ?id gently)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 stir)	;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id XIre_XIre))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gently.clp 	gently2   "  ?id "  XIre_XIre )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  gently.clp       gently2   "  ?id "  physics )" crlf))
)



;@@@ Added by 14anu-ban-11 on (21-10-2014)
;Press down gently to flatten.(COCA)
;समतल होने के लिए नीचे नरमी से दबाइए . (manual)
(defrule gently0
(declare (salience 100))
(id-root ?id gently)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naramI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gently.clp 	gently0   "  ?id "  naramI_se )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (24-03-2015)
;And then she heard the words that surprised and startled, confused her, so that the green eyes that flashed and sparked with anger softened and melted as she looked at him and heard each gently spoken syllable.		[bnc-corpus]
;और फिर उसने वह शब्द सुने जो उसे आश्चर्यचकित और चौंका दिया, उसको  उलझन में  डाल दिया, जिससे कि वह हरी  आँखें जो गुस्से से चमक और दहक रहीं थी नरम हो गई और पिघल गई जब उसने उसकी ओर देखा  और  धीरे से बोले गये प्रत्येक शब्दांश को सुना.[manual]
;@@@ Added by 14anu-ban-11 on (21-10-2014)
;If you stretch a helical spring by gently pulling its ends, the length of the spring increases slightly.
;(NCERT)
;यदि किसी कुण्डलित स्प्रिंग के सिरों को धीरे से खींचकर विस्तारित किया जाए तो स्प्रिंग की लम्बाई थोडी बढ जाती है.(NCERT)
(defrule gently1
(declare (salience 400))
(id-root ?id gently)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 pull|speak)		;added 'speak' by 14anu-ban-05 on (24-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XIre_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gently.clp 	gently1   "  ?id "  XIre_se )" crlf))
)

;@@@Added by 14anu-ban-02(05-03-2016)
;The flowers were gently swaying in the breeze.[oald]
;फूल मन्द हवा में हल्के से लहरा रहे थे.[self]
(defrule gently3
(declare (salience 400))
(id-root ?id gently)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 sway)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halke_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gently.clp 	gently3   "  ?id "  halke_se )" crlf))
)
