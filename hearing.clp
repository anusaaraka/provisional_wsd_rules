
;$$$ Modified by 14anu24
;Added by Meena(5.12.09)
;Many people were angered by the hearings . 
(defrule hearing0
(declare (salience 5000))
;(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id hearing )
(or(kriyA-by_saMbanXI  ?id1 ?id)(viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa  ?id ?id1))
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sunavAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hearing.clp       hearing0   "  ?id "  sunavAI )" crlf))
);corrected the file name to hearing.clp from hear.clp

;@@@ Added by 14anu-ban-06 (02-09-14)
;You must ask for permission for this , in writing or on your attendance form , well before the date of your hearing , explaining why you need more than two witnesses .(parallel corpus)
;उन की अनुमति लेने के लिए सुनवाई की तिथि के काफ़ी समय पूर्व , आप को अपने अटेंडेंस फ़ॉर्म पर लिखित रुप में समझाना होगा कि आप को दो से अधिक गवाह लाने की ज़रुरत क्यों है .
(defrule hearing2
(declare (salience 5100))
(id-root ?id hearing )
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 day|date)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sunavAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hearing.clp       hearing2   "  ?id "  sunavAI )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;He attended every court hearing. (COCA)
;वह अदालत की प्रत्येक सुनवाई में उपस्थित हुआ . (manual)
(defrule hearing3
(declare (salience 5200))
(id-root ?id hearing )
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 court)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sunavAI))
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hearing.clp       hearing3   "  ?id "  sunavAI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  hearing.clp      hearing3   "  ?id1 " kA )" crlf))
)

;------------------------------- Default Rules ----------------
;Added by Meena(5.12.09)
;The explosion damaged his hearing . 
(defrule hearing1
(declare (salience 0))
;(id-root ?id hear)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id hearing )
;(or(kriyA-by_saMbanXI  ?id1 ?id)(viSeRya-viSeRaNa ?id ?id1))
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id  SravaNa_Sakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hearing.clp       hearing1   "  ?id "   SravaNa_Sakwi  )" crlf))
)

