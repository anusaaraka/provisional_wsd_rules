;$$$ Modified by 14anu-ban-01 on (09-01-2015) 
(defrule object0
(declare (salience 5000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id objects )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu));changed "word_mng" to "root_mng" by 14anu-ban-01 on (09-01-2015) 
(assert (id-wsd_number ?id s))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  object.clp  	object0   "  ?id "  vaswu )" crlf);changed "word_mng" to "root_mng" by 14anu-ban-01 on (09-01-2015) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number  " ?*prov_dir* "  object.clp     object0   "  ?id " s )" crlf))
)

; Added by human beings
(defrule object1
(declare (salience 4900))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 non-living)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object1   "  ?id "  vaswu )" crlf))
)

(defrule object2
(declare (salience 4800))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 living)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object2   "  ?id "  vaswu )" crlf))
)

(defrule object3
(declare (salience 4700))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 dense)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object3   "  ?id "  vaswu )" crlf))
)

(defrule object4
(declare (salience 4600))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object4   "  ?id "  vaswu )" crlf))
)

;default_sense && category=noun	karma	0
(defrule object5
(declare (salience 4500))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Apawwi_vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object5   "  ?id "  Apawwi_vyakwa_kara )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 28-1-2014
;In his reply Lord Hardinge stated that he did not think that Government had shown themselves wanting 
;in sympathy for the aims and objects of the University and pointed out the extent of financial assistance 
;which the University had already received from his Government. [Gyannidhi]
;अपने उत्तर में लार्ड हार्डिंग ने कहा कि वे नहीं मानते कि विश्वविद्यालय के लक्ष्यों और उद्देश्यों के प्रति सरकार ने कभी भी सहानुभूति की कमी महसूस होने दी 
;और उस वित्तीय सहायता की ओर ध्यान आकर्षित किया जो विश्वविद्यालय अब तक उनकी सरकार से प्राप्त कर चुका है।

;$$$ modified by Pramila(BU) on 13-02-2014 [condition '(viSeRya-of_saMbanXI  ?id ?id1)'and '(kriyA-in_saMbanXI  ?id1 ?id)' is added]
;What is the object of your visit ?        ;shiksharthi
;तुम्हारी मुलाकात का क्या उद्देश्य है.
;He is determined in his object.  ;shiksharthi
;वह अपने उद्देश्य में दृढ़ है.
;$$$ Modified by 14anu13 on 27-06-14    [condition '(viSeRya-RaRTI_viSeRaNa 	?id 	 ?)(saMjFA-saMjFA_samAnAXikaraNa  ?id 	 ?)' is added]
;It is evident that a greedy man strains to effect his object , the man who strains becomes tired , and the tired man pants ; so the panting is the result of greediness .
;यह स्पष्ट है कि लोभी मनुष्य अपने उद्देश्य की प्राप्ति के लिए उद्यम करता है और जो उद्यम करता है वह थक जाता है और थका हुआ मनुष्य हांफने लगता है . 
(defrule object6
(declare (salience 5500))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-for_saMbanXI  ? ?id)(viSeRya-of_saMbanXI  ?id ?id1)(kriyA-in_saMbanXI  ?id1 ?id)(viSeRya-RaRTI_viSeRaNa  ?id 	?)(saMjFA-saMjFA_samAnAXikaraNa 	?id 	 ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object6   "  ?id "  uxxeSya )" crlf))
)


;"object","V","1.Apawwi vyakwa karanA"
;I object to his use of indecent language.
;
;


;@@@ Added by Pramila Banasthali 13-02-2014
;His object was to win the match.  ;shiksharthi
;उसका उद्देश्य मैच जीतना था.
(defrule object7
(declare (salience 5500))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?kri ?id)
(kriyA-kriyArWa_kriyA  ?kri ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object7   "  ?id "  uxxeSya )" crlf))
)


;@@@ Added by Pramila Banasthali 13-02-2014
;He became the object of ridicule.  ;shiksharthi
;वह उपहास का पात्र बन गया.
(defrule object8
(declare (salience 5600))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id2)
(subject-subject_samAnAXikaraNa  ?sub ?id)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object8   "  ?id "  pawra )" crlf))
)


;$$$ Modified by Sonam Gupta MTech IT Banasthali 14-3-2014 (added move in ?id1)
;@@@ Added by Sonam Gupta MTech IT Banasthali 13-3-2014
;Otherwise the object is said to be at rest with respect to this frame of reference. [ncert]
;अन्यथा वस्तु को उस निर्देश तन्त्र के सापेक्ष विरामावस्था में मानते है...
(defrule object9
(declare (salience 5600))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 say|move)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object9   "  ?id "  vaswu )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-09-2014)
;Consider a constant force F acting on an object of mass m.. [NCERT CORPUS]
;mAnA ki eka acara bala @F,kisI @m xravyamAna ke piNda para laga rahA hE .

(defrule object10
(declare (salience 6000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?id1 ?id)
(id-root ?id1 act)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object10   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  object.clp       object10   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-09-2014)
;Table 6.2 lists the kinetic energies for various objects. [NCERT CORPUS]
;सारणी 6.2 में विभिन्न पिण्डों की गतिज ऊर्जाएँ सूचीबद्ध हैं.

(defrule object11
(declare (salience 6000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id1 ?id)
(id-root ?id1 energy)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object11   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  object.clp       object11   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-10-2014)
;A rigid body generally means a hard solid object having a definite shape and size. [NCERT CORPUS]
;sAXAraNawayA xqDa piMda kA arWa howA hE eka EsA kaTora Tosa paxArWa jisakI koI niSciwa Akqwi waWA AkAra ho. [NCERT CORPUS]

(defrule object12
(declare (salience 6000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 solid)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id paxArWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object12   "  ?id "  paxArWa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  object.clp 	object12   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (09-10-2014)
;If you perform an experiment in your laboratory today and repeat the same experiment (on the same objects under identical conditions) after a year, the results are bound to be the same.  [NCERT CORPUS]
;yaxi Apa Aja apanI prayogaSAlA meM koI prayoga kareM waWA apane usI prayoga ko (sarvasama avasWAoM meM unhIM piNdoM ke sAWa) eka varRa paScAw xoharAez wo Apako samAna pariNAma prApwa honA eka bAXyawA hE. [NCERT CORPUS]

(defrule object13
(declare (salience 6000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id1 ?id)
(id-root ?id1 experiment)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object13   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  object.clp 	object13   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (21-10-2014)
;It does not depend on the temperature, pressure or location of the object in space. [NCERT CORPUS]
;yaha piMda ke wApa, xAba yA xikkAla meM usakI avasWiwi para nirBara nahIM karawA. [NCERT CORPUS]

(defrule object14
(declare (salience 5000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 space)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object14   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  object.clp 	object14   "  ?id "  physics )" crlf)
)
)

;$$$ Modified by 14anu-ban-01 on (09-01-2015):added 'motion' to the list.
;We studied the motion of objects.[Mailed by Soma Mam]
;हमने ऐसे पिंडों की गति के बारे में अध्ययन किया.[Mailed by Soma Mam]
;@@@ Added by 14anu-ban-09 on (21-10-2014)
;The sizes of the objects we come across in the universe vary over a very wide range. [NCERT CORPUS]
;hameM viSva meM jo piMda xiKAI xewe hEM una piMdoM kI AmApoM meM anwara kA eka viswqwa parisara hE. [NCERT CORPUS]
(defrule object15
(declare (salience 5000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 size|motion)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object15   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  object.clp 	object15   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (21-10-2014)
;The night sky with its bright celestial objects has fascinated humans since time immemorial.   [NCERT CORPUS]
;anAxi kAla se hI rAwri ke AkASa meM camakane vAle KagolIya piMda use sammohiwa karawe rahe hEM. [NCERT CORPUS]

(defrule object16
(declare (salience 6000))
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 celestial) 
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id piNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object16   "  ?id "  piNda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  object.clp 	object16   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 02.06.2014 email-id:sahni.gourav0123@gmail.com
;sentence:If you're late, you'll defeat the whole object of the exercise.
;hindi translate:यदि आप दिवङ्गत हैं, तो आप व्यायाम का पूरा उद्देश्य हराएँगे . 
(defrule object17
(declare (salience 5600)) ;salience increased '4900' to '5600' by 14anu-ban-09 on (12-01-2015)
(id-root ?id object)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 life|experiment|project|exercise|work)
(or (viSeRya-of_saMbanXI ?id ?id1)(viSeRya-in_saMbanXI ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  object.clp 	object17   "  ?id "  lakRya )" crlf))
)

