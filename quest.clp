
;@@@ Added by Anita-11-06-2014
;We remarked in section 1.1 that unification is a basic quest in physics. [By mail]
;हमने अनुभाग 1.1 में यह टिप्पणी की है कि एकीकरण भौतिकी की मूलभूत खोज है..
(defrule quest2
(declare (salience 4800))
(id-root ?id quest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id ?sam)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quest.clp 	quest2   "  ?id "  Koja )" crlf))
)

;------------------------ Default Rules ----------------------

;"quest","N","1.walASa"
;Sages did penance in quest for truth
(defrule quest0
(declare (salience 4000)); reduce salience by anita
(id-root ?id quest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quest.clp 	quest0   "  ?id "  walASa )" crlf))
)

;"quest","V","1.walASanA"
;He continued to quest for clues.
(defrule quest1
(declare (salience 4900))
(id-root ?id quest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quest.clp 	quest1   "  ?id "  walASa )" crlf))
)

