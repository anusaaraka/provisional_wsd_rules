;---------------------------------DEFAULT RULE-------------------------------------------------

;@@@ Added by 14anu-ban-09 on (04-03-2015)
;She organized people to work for social justice.		[merrian-webster]
;उसने सामाजिक न्याय  के विषय में काम करने के लिए लोगों को सङ्गठित किया . 	[self]

(defrule organize0
(declare (salience 000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgaTiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize0   "  ?id "  saMgaTiwa_kara )" crlf))
)

;-----------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (04-03-2015)
; They hired a professional to help organize their wedding.	[oald]	;added by 14anu-ban-09 on (14-03-2015)
;उन्होंने विवाहोत्सव आयोजित करने में उनकी सहायता करने के लिए व्य्वसायी काम पर रखा . [manual] ;added by 14anu-ban-09 on (14-03-2015)
;Virat Singh organized last rituals for dog.	[Report-set 1]	;added by 14anu-ban-09 on (12-03-2015)
;विराट सिंह ने कुत्ते के लिए  आखिरी क्रियापद्धति आयोजित की . 	;added by 14anu-ban-09 on (12-03-2015) 
;I have been delegated to organize the Christmas party.	[oald]
;मैं क्रिसमस पार्टी  को आयोजित करने के लिए नियुक्त किया गया हूँ .         [manual]

(defrule organize1
(declare (salience 1000))	;salience increased '000' to '1000' by 14anu-ban-09 on (05-03-2015)
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(or(id-root ?id1 party|event|trip|festival|ritual|wedding)(id-word ?id1 fund-raiser))	;Added 'ritual' by 14anu-ban-09 on (12-03-2015)	;Added 'wedding' and (id-word ?id1 fund-raiser) by 14anu-ban-09 on (14-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize1   "  ?id "  Ayojiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-09 on (04-03-2015)
;NOTE- When 'me' added to human.txt. Then, uncomment (id-root ?id3 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) and comment (id-root ?id3 me).
;My mother is always trying to organize me.	[oald]
;मेरी माँ हमेशा मुझे सुनियोजित बनाने का प्रयास करती रहती है .    [manual]

(defrule organize2
(declare (salience 2000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id3)
;(id-root ?id3 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id3 me)
(kriyA-kriyArWa_kriyA  ?id4 ?id)
(kriyA-subject  ?id4 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id4 try)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id suniyojiwa_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize2   "  ?id "  suniyojiwa_banA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (05-03-2015)
;Modern computers can organize large amounts of data very quickly. 	[oald]
;आधुनिक संगणक अत्यन्त  तेज़ी से डेटा की अधिक मात्रा को सुव्यवस्थित कर सकते हैं .         [manual]

(defrule organize5
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)	
(id-root ?id1 computer|brain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize5   "  ?id "  suvyavasWiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;You should try and organize your time better. 		[oald]
;आपको आपका समय बेहतर तरीके से सुव्यवस्थित करने का और प्रयास करना चाहिए  .         [manual]

(defrule organize6
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id2 ?id3)
(kriyA-subject  ?id ?id1)
(kriyA-subject  ?id2 ?id1)	
(id-root ?id3 time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize6   "  ?id "  suvyavasWiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;I'm sure you don't need me to organize you. 		[oald]
;मैं विश्वस्त हूँ कि आपको मेरी आपको योजना बनाने के लिए नहीं आवश्यकता है .          [manual]

(defrule organize7
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(id-root ?id1 need)
(kriyA-object  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize7   "  ?id "  yojanA_banA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;The festival was organized jointly with their French counterparts. 	[oald]
;त्यौहार उनके फ्रांसीसी प्रतिस्थानीयों  के साथ मिल कर आयोजित किया गया था .               [manual]

(defrule organize8
(declare (salience 1000))	
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 party|event|trip|festival)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize8   "  ?id "  Ayojiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;She had organized a car to meet me at the airport.    	   [oald]
;उसने मुझसे विमानपत्तन में मिलने के लिए गाडी का प्रबन्ध किया था .          [manual]

(defrule organize9
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)	
(id-root ?id1 car)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prabaMXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize9   "  ?id "  prabaMXa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-03-2015)
;He needs someone to help him organize his work.    	   [oald]
;उसने मुझसे विमानपत्तन में मिलने के लिए गाडी का प्रबन्ध किया था .          [manual]

(defrule organize10
(declare (salience 1000)) 
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)	
(id-root ?id1 work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suniyojiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize10   "  ?id "  suniyojiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-03-2015)  ;NOTE- Run on parser no. 4.
;The company has tried to prevent the workers from organizing.    	   [oald]
;कम्पनी ने कार्यकर्ताओं को सङ्गटन बनाने से रोकने का प्रयास किया है .        		   [manual]

(defrule organize11
(declare (salience 1000)) 
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id gerund_or_present_participle)
(viSeRya-from_saMbanXI ?id1 ?id)	
(id-root ?id1 worker)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgatana_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize11   "  ?id "  saMgatana_banA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-03-2015)
;Can you help me organize my files?  			 [freedictionary.com]
;क्या आप मुझे मेरी फाइलें सुव्यवस्थित करने में सहायता कर सकते हैं?         [manual]

(defrule organize12
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 file)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp 	organize12   "  ?id "  suvyavasWiwa_kara )" crlf))
)


;@@@ Added by Rajini (20-07-2016)
;Functions are organized on Kartik full moonlight and third lighted fortnight of Vaisakh in Shwetambar Jain Temple and between Kartik Shukla to full moonlight in Digambar Jain Temple. 
;कार्तिक पूर्णिमा और वैशाख शुक्ल तृतीया को श्वेताम्बर जैन मन्दिर में और कार्तिक शुक्ल से पूर्णिमा के बीच दिगम्बर जैन मन्दिर में कार्यक्रम का आयोजन किया जाता है .

(defrule organize13
(declare (salience 1000))
(id-root ?id organize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(kriyA-karma 	?id 	 ?id1)
(kriyA-on_saMbanXI 	?id 	 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojana_kara))
(if ?*debug_flag* then
  (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organize.clp         organize13   "  ?id "  Ayojana_kara )" crlf))
)

