;$$$ Modified by 14anu-ban-04 (16-04-2015)           --changed meaning from 'visPota_kara' to 'Poda/visPotiwa_kara'
;There was a huge bang as if someone had exploded a rocket outside.                [oald]   ;run on parse no. 2
;एक विशाल तेज आवाज थी मानो किसीने बाहर रॉकेट फोड़ा/विस्फोटित किया था .  
(defrule explode0
(declare (salience 5000))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Poda/visPotiwa_kara))           
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode0   "  ?id "  Poda/visPotiwa_kara )" crlf))
)

;$$$ Modified by 14anu-ban-04 (16-04-2015)           --changed meaning from 'gussA_uwAra' to 'uwwejiwa_ho_jA/Aga_babUlA_ho_jA'
;@@@ Added by 14anu23 on 26/6/14
;Suddenly Charles exploded with rage.
;अचानक चार्लेशचं ने रोष के साथ गुस्सा उतारा .
;अचानक चार्ल्स क्रोध से उत्तेजित हो गया.                             ;modified translation by 14anu-ban-04 (16-04-2015)  
(defrule explode2
(declare (salience 5000))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 rage|fury|ire|dander|indignation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejiwa_ho_jA/Aga_babUlA_ho_jA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode2   "  ?id "  uwwejiwa_ho_jA/Aga_babUlA_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-04-2015)
;The population has exploded in the last ten years.               [cald]   
;जनसंख्या पिछले दस वर्षों में   बढ़ गई है .                                        [self]
(defrule explode3
(declare (salience 4910))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 population)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa_jA))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode3   "  ?id "  baDa_jA )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-04-2015)
;This book finally explodes a myth about the origin of the universe.                   [cald]
;अन्ततः यह पुस्तक  ब्रह्माण्ड की उत्पत्ति के बारे में कल्पित कथा को गलत साबित करती है .                           [self]
;At last, a women's magazine explodes the myth that thin equals beautiful.             [oald]
;आखिरकार, स्त्री की पत्रिका एक कल्पित कथा को कि पतला सुन्दर के समान है उसे गलत साबित करती है   .                 [self]
(defrule explode4
(declare (salience 5010))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 myth|theory)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id galawa_sAbiwa_kara))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  explode.clp     explode4   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode4  "  ?id "  galawa_sAbiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-04-2015)
;The protest exploded into a riot.                 [oald]
;विरोध दंगे में  बदल गया .                                      [self]
;After ten minutes the game exploded into life.           [oald]
;दस मिनटों बजे के बाद खेल उत्साह में बदल गया .                       [self]
(defrule explode5
(declare (salience 5010))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_jA))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode5  "  ?id "  baxala_jA )" crlf))
)

;---------------------------------- Default Rules -----------------------------------

;$$$ Modified by 14anu-ban-04 (16-04-2015)           --changed meaning from 'visPota_ho' to  'PUta'
;The firework exploded in his hand.                   [oald]
;पटाखा उसके हाथ में फूटा .                                   [self]
(defrule explode1
(declare (salience 4900))
(id-root ?id explode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explode.clp 	explode1   "  ?id "  PUta )" crlf))
)

;Changed by Amba from PUtanA to visPota_karanA/honA based on paxasUwra
;default_sense && category=verb	PUta	0
;"explode","VI","1.PUtanA"
;The champagne bottle exploded.
;
;
