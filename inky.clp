;@@@ Added by 14anu-ban-06 (17-03-2015)
;It was night and the water looked cold and inky black.(cambridge) [parser no. 15]
;रात थी और पानी ठण्डा और स्याह काला दिखा . (manual)
(defrule inky1
(declare (salience 2000))
(id-root ?id inky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 black|blackness)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id syAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inky.clp 	inky1   "  ?id "  syAha )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

;@@@ Added by 14anu-ban-06 (17-03-2015)
;Inky fingers. (cambridge) [parser no. 9]
;स्याही लगी उंगलियाँ .(manual)
(defrule inky0
(declare (salience 0))
(id-root ?id inky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id syAhI_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inky.clp 	inky0   "  ?id "  syAhI_lagA )" crlf))
)

