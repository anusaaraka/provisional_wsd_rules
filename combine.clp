
(defrule combine0
(declare (salience 5000))
(id-root ?id combine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  combine.clp 	combine0   "  ?id "  milA )" crlf))
)

(defrule combine1
(declare (salience 4900))
(id-root ?id combine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  combine.clp 	combine1   "  ?id "  mila )" crlf))
)

;"combine","V","1.milanA[milAnA]"
;Combine resources.
;These forces combined with others

;@@@ Added by 14anu-ban-03 (15-10-2014)
;To make such estimates, we should learn how errors combine in various mathematical operations. [ncert]
;यह आकलन करने के लिए कि यह त्रुटि कितनी होगी हमें यह सीखना होगा कि विभिन्न गणितीय सङ्क्रियाओं में त्रुटियाँ किस प्रकार संयोजित होती हैं.[ncert]
(defrule combine2
(declare (salience 4900))
(id-root ?id combine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyojiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  combine.clp 	combine2   "  ?id "  saMyojiwa_ho )" crlf))
)


;@@@ Added by 14anu-ban-10 on (25-10-2014)
;Low density gases obey these laws, which may be combined into a single relationship.  [ncert corpus]
;kama Ganawva para gEseM ina niyamoM kA pAlana karawI hEM jinheM ekala saMbaMXa meM samAyojiwa kiyA jA sakawA hE.[ncert corpus]
(defrule combine3
(declare (salience 5100))
(id-root ?id combine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAyojiwa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  combine.clp 	combine3   "  ?id "  samAyojiwa_kara )" crlf))
)
