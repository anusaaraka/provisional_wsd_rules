
(defrule wedge0
(declare (salience 5000))
(id-root ?id wedge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUztA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wedge.clp 	wedge0   "  ?id "  KUztA )" crlf))
)

;"wedge","N","1.KUztA"
;He used the wedge to separate the wood into two parts.
;
(defrule wedge1
(declare (salience 4900))
(id-root ?id wedge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUztA_gAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wedge.clp 	wedge1   "  ?id "  KUztA_gAdZa )" crlf))
)

;"wedge","VI","1.KUztA_gAdZanA"
;I gave the carpenter a wedge tool.


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;Shoes with wedge heels.(oald)
;वैज एडियों के  जूते . (self)
(defrule wedge2
(declare (salience 5001))
(id-root ?id wedge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(viSeRya-with_saMbanXI  ?id2 ?id1)
(id-root ?id1 heel)
(id-root ?id2 shoe)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vEja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wedge.clp 	wedge2   "  ?id "  vEja)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;The boat was now wedged between the rocks.(oald)
;अब नाव चट्टानों के बीच अटका गयी थी . (self)
(defrule wedge3
(declare (salience 4901))
(id-root ?id wedge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-between_saMbanXI  ?id ?id1)
(id-root ?id1 rock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id atakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wedge.clp 	wedge3   "  ?id "  atakA)" crlf))
)



