;$$$ Modified by 14anu-ban-01 on (21-03-2015):Example changed, meaning modified
;@@@ Added by 14anu-ban-11 on (03-03-2015)
;The manager said that the team's win on Saturday would be a spur to even greater effort this season.[oald]
;प्रबन्धक ने कहा कि शनिवार को दल की जीत इस सीज़न में [खिलाड़ियों द्वारा] और भी अधिक प्रयास [करने]  के लिए एक प्रोत्साहन रहेगी .  (self)
(defrule spur0
(declare (salience 00))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prowsAhana))	;changed AvAhana to 'prowsAhana' by 14anu-ban-01 on (21-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur0   "  ?id "  prowsAhana)" crlf))	;changed AvAhana to 'prowsAhana' by 14anu-ban-01 on (21-03-2015)
)

;@@@ Added by 14anu-ban-11 on (03-03-2015)
;Her difficult childhood spurred her on to succeed. (oald)
;उसका कष्टपूर्ण बचपन ने उसको  कामयाब होने के लिये  प्रोत्साहित किया . (self)
(defrule spur1
(declare (salience 10))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prowsAhiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur1   "  ?id "   prowsAhiwa_kara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (03-03-2015)
;The fire, spurred by high temperatures and strong winds, had burnt more than 140 acres. (oald)
;अधिक तापमान और सशक्त हवाओं के द्वारा तेज हुई  आग ने 140 एकड से ज्यादा  जला दिया. (self)
(defrule spur2
(declare (salience 20))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur2  "  ?id "  weja_ho)" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-03-2015)
;How can parents capitalize on the computer's potential for spurring their child's development?[Anusaaraka RnD Evaluation:set4]
;माँ बाप अपने बच्चे के विकास को बढाने के लिए सङ्गणक की क्षमता का  लाभ कैसे उठा सकते हैं? [self]
(defrule spur3
(declare (salience 1000))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 development|growth|intrest|production)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  spur.clp 	spur3  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spur.clp 	spur3   "  ?id "  baDZA)" crlf))
)


;@@@ Added by 14anu-ban-01 on (21-03-2015)
;Spurred on by her early success, she went on to write four more novels in rapid succession.[oald]
;अपनी प्रारम्भिक सफलता से प्रोत्साहित होकर ,उसने त्वरित अनुक्रम में चार अधिक उपन्यास लिखना जारी रखा. [self:more faithful]
;अपनी प्रारम्भिक सफलता से प्रोत्साहित होकर ,वह त्वरित अनुक्रम में चार अधिक उपन्यास लिखती चली गई [self:more natural]
(defrule spur4
(declare (salience 10))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 on)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prowsAhiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   spur.clp 	spur4   "  ?id "  " ?id1 "  prowsAhiwa_ho  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (21-03-2015)
;His speech was a powerful spur to action.(oald)
;उसका भाषण अभियान के लिये  एक शक्तिशाली आवाहन था . (self)
(defrule spur5
(declare (salience 00))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 action)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvAhana))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur5   "  ?id "  AvAhana)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (23-03-2015)
;The remains of Troy are scattered over the mound of Hissarlik, situated on the spur of a limestone plateau on the northwest coast of Turkey.[coca]
;ट्रॉय के अवशेष  हिस्सरलिक का टीला जो कि तुर्की के पश्चिमोत्तरीय समुद्र तट पर  एक चूने_के _पत्थर _वाले पठार के पर्वत स्कन्ध पर स्थित है,पर बिखरे हुए हैं .  (self)
(defrule spur6
(declare (salience 10))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 plateau)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parvawa_skanXa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur6   "  ?id "  parvawa_skanXa)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (23-03-2015)
;That short spur of the hallway ended at a men's room.[self: with reference to coca]
;प्रवेश-कक्ष का वह अपर्याप्त छोटा_रास्ता आदमी के कमरे में समाप्त हुआ .  (self)
(defrule spur7
(declare (salience 10))
(id-root ?id spur)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 hallway)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA_rAswA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  spur.clp   spur7   "  ?id "  CotA_rAswA)" crlf))	
)
