;@@@ Added by 14anu-ban-06 (13-03-2015)
;All that gossip about Linda was just pure invention. (cambridge)
;लिन्डा के बारे में वह सब अफ़वाहे सिर्फ मनगढन्त थी . (manual)
(defrule invention1
(declare (salience 2000))
(id-root ?id invention)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 gossip)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id managaDZaMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invention.clp 	invention1   "  ?id "  managaDZaMwa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (13-03-2015)
;The world changed rapidly after the invention of the phone. (cambridge)
;विश्व टेलीफोन के आविष्कार के बाद शीघ्रता से बदला . (manual)
(defrule invention0
(declare (salience 0))
(id-root ?id invention)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AviRkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invention.clp        invention0   "  ?id "  AviRkAra )" crlf))
)

