;@@@ Added by 14anu-ban-05 Prajna Jha on 12-08-2014
;Shooting of the films like Awara , Jis Desh Me Ganga Bahati Hai , Ashoka The Great have been done in Bhedaghat
;भेड़ाघाट में  आवारा , जिस देश में गंगा बहती है , अशोका दी ग्रेट जैसे कई फिल्मों की शूटिंग की जा चुकी है .
;The shooting of many films has been done here .
;यहाँ अनेक फिल्मों की शूटिंग की जा चुकी है ।
(defrule shooting0
(declare (salience 1000))
(id-root ?id shooting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SUtiMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  shooting.clp     shooting0   "  ?id " SUtiMga )" crlf))
)


;@@@ Added by 14anu-ban-05 Prajna Jha on 12-08-2014
;Soon after Joining the revolutionary movement , Naren was initiated into the art of shooting and bomb - making .
;क्रांतिकारी आंदोलन में शामिल होने के कुछ समय बाद नरेऩ्द्र को बंदूक चलाने तथा बम बनाने का काम सिखाया गया था .
(defrule shooting1
(declare (salience 1000))
(id-root ?id shooting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 art)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxUka_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  shooting.clp     shooting1   "  ?id " baMxUka_calA )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 12-08-2014
;Rajpal himself has trained the national shooting teams of Nepal and Mauritius .
;राजपाल ने नेपाल और मॉरिशस की राष्ट्रीय निशानेबाज  दलों को प्रशिक्षित किया है .
(defrule shooting2
(declare (salience 5000))
(id-root ?id shooting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id niSAnebAja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  shooting.clp     shooting2   "  ?id " niSAnebAja  )" crlf))
)

