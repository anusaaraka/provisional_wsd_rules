;@@@ Added by 14anu-ban-04 (19-11-2014)
;One of these studies, published in the Journal of Nutrition,[100] concludes that there are no clinical concerns with respect to nutritional adequacy, sexual development, neurobehavioral development,immune development, or thyroid disease.                [agriculture]
;पोषण के जर्नल में प्रकाशित हुए ,इन अध्ययनों में से एक का  [100] निष्कर्ष निकाला है कि वहाँ पोषण पर्याप्तता, यौन विकास ,नेउरोबेहविओरल् (neurobehavioral) विकास , प्रतिरक्षा विकास, या थायराइड की बीमारी के सन्दर्भ में रोगशय्या संबंधी प्रयोजन नहीं हैं.                            [manual]
(defrule development0
(declare (salience 10))                               ;salience decreased from '100' to '10'   by 14anu-ban-04 (09-04-2015)
;(Domain agriculture)                                          ;commmented by 14anu-ban-04 (09-04-2015)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id development)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikAsa))
;(assert (id-domain_type  ?id agriculture))        ;commmented by 14anu-ban-04 (09-04-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  development.clp development0   "  ?id "  agriculture )" crlf)                                             ;commmented by 14anu-ban-04 (09-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  development.clp 	development0   "  ?id "  vikAsa )" crlf))
)


;@@@ Added by 14anu-ban-04 (19-11-2014)
;A hypothesis of the development of peanut allergy has to do with the way peanuts are processed in North America versus other countries, such as Pakistan and China, where peanuts are widely eaten.                [agriculture]
;मूँगफली एलर्जी की  वृद्धि की परिकल्पना उत्तरी अमेरिका  के विपरीत अन्य देशों  में जिस प्रकार मूँगफली का प्रसंस्करण होता उस से संबन्धित है  जैसे पाकिस्तान और चीन जँहा मूँगफली अधिक मात्रा में  खाई जाती है|       [manual]
(defrule development1
(declare (salience 200))
(Domain agriculture)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id development)
(id-cat_coarse ?id noun)      
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1 allergy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi)) 
(assert (id-domain_type  ?id agriculture))         
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  development.clp development1   "  ?id "  agriculture )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  development.clp development1   "  ?id "  vqxXi )" crlf))
)


;@@@ Added by 14anu-ban-04 (13-03-2015)
;Two developments diminished this competition.              [RND evaluation report set -4]
;दो घटनाओं ने इस स्पर्धा का महत्व कम कर दिया .                              [self]
(defrule development2
(declare (salience 100))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id development)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)            ;changed 'kriyA-subject' to 'viSeRya-saMKyA_viSeRaNa'  by 14anu-ban-04 (21-03-2015)
;(id-root ?id1 diminish)                       ;commented by 14anu-ban-04 (21-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatanA))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  development.clp 	development2   "  ?id "  GatanA )" crlf))
)



