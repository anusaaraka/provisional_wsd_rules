(defrule reek0
(declare (salience 5000))
(id-root ?id reek)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bU_A))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reek.clp    reek0   "  ?id "  bU_A )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  reek.clp    reek0   "  ?id " se )" crlf)
)

)
;@@@ Added by 14anu-ban-10 on (20-02-2015)
;His breath reeked of tobacco.[oald]
;उसकी सांस  मे तंबाकू कि  बू आयी ।[manual]
(defrule reek2
(declare (salience 5200))
(id-root ?id reek)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI  ?id ?id1 )
(id-root ?id1 tobacco)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bU_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reek.clp 	reek2   "  ?id "  bU_A)" crlf))
)
;@@@ Added by 14anu-ban-10 on (20-02-2015)
;The reek of diesel smoke is harmful to health.[hinkhoj]
;डीजल के धुएं की भाप स्वास्थ्य के लिए हानिकारक है ।[manual]
(defrule reek3
(declare (salience 5200))
(id-root ?id reek)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1 )
(id-root ?id1 smoke)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reek.clp 	reek3   "  ?id "  BApa)" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-04-2015)
;@@@ Added by 14anu-ban-10 on (20-02-2015)
;The reek of cigarettes and beer.[oald]
;सिगरेट और बियर की दुर्गंध .[manual]
(defrule reek4
(declare (salience 5300))
(id-root ?id reek)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1 )
(id-root ?id1 cigarette|beer)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurgaMXa));changed meaning from 'xurgaXa' to 'xurgaMXa' by 14anu-ban-06 (25-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reek.clp 	reek4   "  ?id "  xurgaMXa)" crlf))
)
