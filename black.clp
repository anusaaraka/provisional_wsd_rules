;$$$Modified by 14anu-ban-02(27-03-2015)     ;meaning changed from 'nirASAjanaka' to 'anXakAramaya'
;The future looks pretty black.[oald]
;भविष्य काफी अन्धकारमय दिखता है . [self]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;The future looks pretty black.[oald]
;भविष्य काफी निराशाजनक दिखता है . 
(defrule black4
(declare (salience 5000))
;(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-word ?id1 future)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anXakAramaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp 	black4   "  ?id "  anXakAramaya )" crlf))
)

;$$$Modified by 14anu-ban-02(27-03-2015)       ;meaning changed from 'kroXiwa' to 'uxAsa'and added 'affecting_id-affected_ids' fact.
;She's been in a really black mood all day.[oald]
;वह सारा दिन काफी उदास थी.[self]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;She's been in a really black mood all day.[oald]
;वह सारा दिन काफी क्रोधित मूड में था
;Rory shot her a black look.[oald]
(defrule black5
(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 mood)	;'look' is removed from the list by 14anu-ban-02(27-03-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id kroXiwa))	;commented by 14anu-ban-02(27-03-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uxAsa))
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp 	black5   "  ?id "  kroXiwa )" crlf));commented by 14anu-ban-02(27-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " black.clp 	black5  "  ?id "  " ?id1 "  uxAsa )" crlf))
)	

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;A power failure blacked out the city last night.[oald]
;बिजली की विफलता ने कल रात शहर को अंधकारमय कर दिया.
(defrule black6
(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject  ?id ?sub)
(id-word ?id1 out)
(id-word ?sub failure|power)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aMXakAramaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " black.clp 	black6  "  ?id "  " ?id1 "  aMXakAramaya_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;A house with blacked out windows.
;काली रंगी खिड़कियों वाला घर
(defrule black7
(declare (salience 5000))
(id-word ?id black)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
(id-root ?obj window)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAlI_raMgI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " black.clp 	black7  "  ?id "  " ?id1 "  kAlI_raMgI )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;The driver had probably blacked out at the wheel.[oald]
;ड्राइवर गाड़ी चलाते हुये अचेत हो गया था
(defrule black8
(declare (salience 4500))
(id-word ?id black)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?sub)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acewa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " black.clp 	black8  "  ?id "  " ?id1 "  acewa_ho_jA )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
;The unions have blacked all imports from the country.[oald]
;यूनियन ने विदेश से सभी आयात का बहिष्कार किया है.
(defrule black9
(declare (salience 5000))
(id-word ?id black)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 import)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahiRkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp     black9   "  ?id "  bahiRkAra_kara )" crlf))
)


;@@@ Added by 14anu05 13/06/2014
;A star was sucked into the black hole nearby.
;तारा पास के ब्लैक होल के अन्दर चूसा गया था.
(defrule black10
(declare (salience 5600))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 hole|Hole)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 blEka_hola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  black.clp     black10   "  ?id "  " ?id1 "  blEka_hola )" crlf))
)

;@@@Added by 14anu-ban-02(27-03-2015)
;The play is a black comedy.[oald]
;नाटक एक व्यङ्गपूर्ण कॉमेडी है.[self]
(defrule black11
(declare (salience 4000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 comedy|humour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyafgapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp     black11   "  ?id "  vyafgapUrNa )" crlf))
)

;@@@Added by 14anu-ban-02(27-03-2015)
;Two black coffees, please.[oald]
;कृपया दो दूधरहित कॉफी करना . [self]
(defrule black12
(declare (salience 4000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 coffee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUXarahiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp     black12   "  ?id "  xUXarahiwa )" crlf))
)



;***********************DEFAULT RULES***************************

;Added by Meena(18.7.11)
;No, it was not Black Monday. 
(defrule black0
(declare (salience 5000))
(id-root ?id black|Black)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id1 ?id)(proper_noun-waxviSiRta_proper_noun ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id))
;(id-root ?id1 day|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)
(id-root ?id1 day|monday|tuesday|wednesday|thursday|friday|saturday|sunday);Modified to lowcase by Roja(13-06-13). As now we are using NER to get PropN info , NER doesnt recognize weekdays as Named Entities. So changed to lowcase. Ex:No, it was not Black Monday. 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSuBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp     black0   "  ?id "  aSuBa )" crlf))
)




;Salience reduced by Meena(18.7.11)
(defrule black1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp 	black1   "  ?id "  kAlA )" crlf))
)


;Salience reduced by Meena(18.7.11)
(defrule black2
(declare (salience 0))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp 	black2   "  ?id "  kAlA )" crlf))
)


;;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10/03/2014
(defrule black3
(declare (salience 0))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  black.clp 	black3   "  ?id "  kAlA_kara )" crlf))
)

;"black","Adj","1.kAlA/SyAma"
;The shop specializes in black leather jackets.
;black deeds
;--"2.aMXerA"
;The future looked black
;a black moonless night
;--"3.BayaMkara"
;A face black with fury
;--"4.uxAsa/sogI"
;Black humor
;Black propaganda
;
;
;"black","Adj","1.kAlA/SyAma"
;The shop specializes in black leather jackets.
;black deeds
;--"2.aMXerA"
;The future looked black
;a black moonless night
;--"3.BayaMkara"
;A face black with fury
;--"4.uxAsa/sogI"
;Black humor
;Black propaganda
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_black5
(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 mood|look)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kroXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " black.clp   sub_samA_black5   "   ?id " kroXiwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_black5
(declare (salience 5000))
(id-root ?id black)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 mood|look)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kroXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " black.clp   obj_samA_black5   "   ?id " kroXiwa )" crlf))
)
