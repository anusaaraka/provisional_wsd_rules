;@@@ Added by 14anu-ban-02 (27-09-2014)
;Sentence: The plot of the applied force is shown in Fig. 6.4.[ncert]
;Translation: चित्र 6.4 में आरोपित बल का आलेख प्रदर्शित किया गया है [ncert]
(defrule applied1 
(declare (salience 100)) 
(id-root ?id applied) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 force)
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id Aropiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  applied.clp  applied1  "  ?id "  Aropiwa )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (27-09-2014)
;Sentence: If an applied syringe taken from an infected person is used by another person then virus goes in the healthy person .[karan singla]
;Translation: जिसमें संक्रमित व्यक्ति द्वारा लिए गए इंजेक्शन में प्रयुक्त सिरिंज का प्रयोग दूसरा व्यक्ति करे तो वायरस स्वस्थ व्यक्ति में चला जाता है ।[karan singla]
(defrule applied0 
(declare (salience -1)) 
(id-root ?id applied) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prayukwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  applied.clp  applied0  "  ?id "  prayukwa )" crlf)) 
) 
