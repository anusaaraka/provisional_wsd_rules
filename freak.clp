;@@@ Added by 14anu-ban-05 on (13-03-2015)
;She was treated like a freak because she didn't want children. [OALD]
;वह बच्चों को नहीं चाहती थी इसलिये उसके साथ एक सनकी की तरह व्यवहार किया गया था.[manual]

(defrule freak2
(declare (salience 5001))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-like_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanakI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freak.clp 	freak2   "  ?id "  sanakI )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-03-2015)
;He's going out with a real freak.[OALD]
;वह एक  असामान्य व्यक्ति के साथ बाहर जा रहा है .[manual]

(defrule freak3
(declare (salience 5002))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asAmAnya_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freak.clp 	freak3   "  ?id "  asAmAnya_vyakwi )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-03-2015)
;He freaked out when he heard he'd got the job. [cald]
;उसने जब सुना कि  उसको नौकरी मिल गई थी तो वह  उत्तेजित हो  गया था.[manual]

(defrule freak4
(declare (salience 5003))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwwejiwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " freak.clp  freak4  "  ?id "  " ?id1 "  uwwejiwa_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-03-2015)
;This song just freaks me out whenever I hear it.[cald]
;यह गाना मुझे आनन्दित कर देता है जब कभी मैं इसे सुनता हूँ . [manual]

(defrule freak5
(declare (salience 5004))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
(kriyA-subject  ?id ?id2)
(id-root ?id2 song)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Ananxiwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " freak.clp  freak5  "  ?id "  " ?id1 "  Ananxiwa_kara_xe )" crlf))
)



;@@@ Added by 14anu-ban-05 on (16-03-2015)
;By some freak of fate they all escaped without injury.[oald]	
;भाग्यवशात्  वे बिना चोट के बाहर निकले.  [manual]

(defrule freak6
(declare (salience 5004))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) of)
(id-word =(+ ?id 2) fate)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) BAgyavaSAw))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " freak.clp  freak6  "  ?id "  " (+ ?id 1) "  " (+ ?id 2) " BAgyavaSAw )" crlf))
)


;------------------------ Default Rules ----------------------

;"freak","N","1.ajIba"
;It was a freak accident.
(defrule freak0
(declare (salience 5000))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ajIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freak.clp 	freak0   "  ?id "  ajIba )" crlf))
)

;"freak","V","1.awyaXika_prawikriyA"
;My friends freaked out when they saw my shaven head.
(defrule freak1
(declare (salience 4900))
(id-root ?id freak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyaXika_prawikriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freak.clp 	freak1   "  ?id "  awyaXika_prawikriyA )" crlf))
)

