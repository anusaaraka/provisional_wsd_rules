(defrule season0
(declare (salience 5000))
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id seasoning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id masAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  season.clp  	season0   "  ?id "  masAlA )" crlf))
)

;"seasoning","N","1.masAlA/namaka-mirca"
;Credit of this tasty food goes to seasonings.
;
(defrule season1
(declare (salience 4900))
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id seasoned )
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id anuBavI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  season.clp  	season1   "  ?id "  anuBavI )" crlf))
)

;"seasoned","Adv","1.anuBavI"
;He is a seasoned teacher.
;
;
;### Salience reduced by 14anu5 on 30.06.14
;The rule fired for : He scored his first goal of the season on Saturday.
;Anusaaraka : उसने शनिवार को ऋतु का उसका प्रथम लक्ष्य खरोंचा .
;Man : उसने शनिवार को सीज़न का उसका प्रथम गोल किया .
(defrule season2
(declare (salience 3000)) ;Salience reduced by 14anu5 on 30.06.14
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id qwu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  season.clp 	season2   "  ?id "  qwu )" crlf))
)

(defrule season3
(declare (salience 4700))
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  season.clp 	season3   "  ?id "  svAxiRta_banA )" crlf))
)

;"season","V","1.svAxiRta banAnA"
;I have made this food season by using more spices.
;--"2.sIanA"
;Furniture made of oak that has been well-seasoned.
;

;@@@ Added by 14anu05 GURLEEN BHAKNA on 30.06.14
;He scored his first goal of the season on Saturday.
;उसने शनिवार को सीज़न का उसका प्रथम गोल किया .  
(defrule season4
(declare (salience 4700))
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 goal|episode|match|player|play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIjZana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  season.clp 	season4   "  ?id "  sIjZana )" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;He was confirmed as captain for the rest of the season. [oald]
;वह  शेष  वर्ष के लिए कप्तान के रूप में पक्का किया गया था . [manual] 
(defrule season5
(declare (salience 4700))
(id-root ?id season)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 rest)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  season.clp 	season5   "  ?id "  varRa )" crlf))
)



;LEVEL 
;Headword : season
;
;Examples --
;
;"season","N","1.qwu"
;Spring season is very delightful.
;vasanwa kA mOsama bahuwa suhAvanA howA hE.
;--"mOsama"
;Months of October && November used to be the cricketing season in the Northern India.
;hinxuswAna ke uwwarI BAgoM meM aktUbara Ora navambara ke mahIne kriketa Kelane kA mOsama huA karawe We.
;
;"season","V","1.COMkanA"
;She seasoned the curry with aromatic spices.
;usane Sorabe ko KuSabUxAra masAloM se COMkA.
;--"2.sIJanA"
;The house was decorated with furniture made of oak that has been well-seasoned.
;Gara acCI waraha se sIJe hue oka ke ParnIcara se sajA huA WA.
;
;
;'season' Sabxa kA anwarnihiwa sUwra
;
;                     qwu (saMjFA)  
;                      |
;                      |
;                     Qwu (kriyA)
;                      |
;               -----------------
;              |                 |
;     Qwu sahane yogya banAnA      qwu anusAra banAnA
;              |                 |
;            siJAnA              COMkanA
;          {pakkA banAnA}
;
;yAni bIjArWa wo 'qwu' hI hE Ora qwu ke kisa pakRa ko lekara kriyA prayoga kiyA bAkI arWa usa para nirBara hEM. awaH
;
;sUwra  : qwu`   
