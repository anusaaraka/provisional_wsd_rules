;@@@ Added by Anita 29-11-13 
;"Do you realize (that) this is the third time you've forgotten?" she said angrily.
;उसने गुस्से से (में) कहा – क्या तुम जानते हो कि यह तीसरी बार है जब तुम भूल गए ।
(defrule realize0
(declare (salience 15))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize0   "?id "  jAna)" crlf))
)

;@@@ Added by Anita 29-11-13
;Lots of money, a luxury house, a fast car - Danny had realized all his ambitions ;by the age of 25.
;बहुत सारा पैसा ,बड़ा सा घर , कार –  इन सब अपनी महत्वकांक्षाओं को डेनी ने २५ वर्ष की आयु में ही साकार कर लिया था ।
(defrule realize1
(declare (salience 20))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?)
;(viSeRya-viSeRaNa  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize1   "?id "  sAkAra_kara)" crlf))
)

;@@@ Added by Anita 29-11-13
;Ten years later her worst fears were realized.
;दस साल के बाद उसका बुरा सपना सत्य सिद्ध हुआ ।
(defrule realize2
(declare (salience 25))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sixXa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize2   "?id "  sixXa_ho)" crlf))
)

;@@@ Added by Anita 29-11-13
;The paintings are expected to realize £500,000 each.
;प्रत्येक पेंटिंग्स पॉउन्ड ५००,०००  तक में बिकने वाली है .
(defrule realize3
(declare (salience 30))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize3   "?id "  bika)" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-10-2014)
;In Chapter 7, we studied the rotation of the bodies and then realized that the motion of a body depends on how mass is distributed within the body.  [ncert corpus]
;aXyAya 7 meM hamaneM piNdoM ke GUNana ke bAre meM paDZA Ora samaJA ki kisI piNda kI gawi isa bAwa para kEse nirBara karawI hE ki piNda ke aMxara xravyamAna kisa prakAra viwariwa hE.[ncert corpus]
(defrule realize5
(declare (salience 200))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize5   "?id " samaJa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-11-2014)
;However, it was only about 200 years ago, in 1820, that it was realized that they were intimately related.[ncert corpus]
;Pira BI lagaBaga 200 varRa pUrva, 1820 meM yaha spaRta anuBava kiyA gayA ki ina xonoM meM atUta sambanXa hE.[ncert corpus]
(defrule realize6
(declare (salience 300))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-vAkyakarma  ?id ? )(kriyA-in_saMbanXI  ?id ? ))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize6   "?id " anuBava_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-11-2014)
;We now realize that in earlier studies we assumed, without saying so, that rotational motion and / or internal motion of the particles were either absent or negligible.[ncert corpus]
;aba hama yaha samaJa sakawe hEM, ki pUrva ke aXyayanoM meM, hamane bina kahe hI yaha mAna liyA WA ki nikAya meM GUrNI gawi, evaM kaNoM meM Anwarika gawi yA wo WI hI nahIM Ora yaxi WI wo nagaNya WI.[ncert corpus]
(defrule realize7
(declare (salience 400))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-aXikaraNavAcI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa_saka));meaning changed from samaJa_sakawe to samaJa_saka by 14anu-ban-10 on (25-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize7   "?id " samaJa_saka)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-11-2014)
;In the beginning of the twentieth century, it was realized that Newtonian mechanics, till then a very successful theory, could not explain some of the most basic features of atomic phenomena.[ncert corpus]
;bIsavIM SawAbxI ke AramBa meM yaha anuBava kiyA gayA ki usa samaya kA sarvAXika saPala nyUtanI yAnwrikI sixXAnwa paramANvIya pariGatanAoM ke kuCa mUla viSiRta lakRaNoM kI vyAKyA karane meM asamarWa hE.[ncert corpus]
;We now realize that in earlier studies we assumed, without saying so, that rotational motion and / or internal motion of the particles were either absent or negligible.[ncert corpus]
;aba hama yaha samaJa sakawe hEM, ki pUrva ke aXyayanoM meM, hamane bina kahe hI yaha mAna liyA WA ki nikAya meM GUrNI gawi, evaM kaNoM meM Anwarika gawi yA wo WI hI nahIM Ora yaxi WI wo nagaNya WI.[ncert corpus]
(defrule realize8
(declare (salience 500))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1)
(id-root ?id1 assume)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  samaJa_saka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp  realize8   "?id " samaJa_saka )" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-03-2015)
;His furniture realized Rs.10000.at the sale.[hinkhoj]
;उसके फर्नीचर ने  Rs.10000 का मूल्य पाया  सेल मै. [manual]
(defrule realize9
(declare (salience 600))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1  furniture)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  mUlya_pAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp  realize9   "?id "  mUlya_pAnA )" crlf))
)

;@@@ Modified by 14anu-ban-10 on (17-03-2015)
;She realized that she had been cheating her.[hinkhoj]
;उसने  जानना  कि वह उसे साथ धोखेबाज़ी करती रही है. [manual]
(defrule realize10
(declare (salience 700))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1 )
(id-root ?id1 cheat)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize10   "?id "  jAna ) " crlf))
)

;@@@ Modified by 14anu-ban-10 on (17-03-2015)
;She realize our ideas must be substantiated into actions.[hinkhoj]
;उसने समझा की  हमारी योजना कार्य मे सिद्ध करती होंगी. [manual]
(defrule realize11
(declare (salience 800))
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1 )
(id-root ?id1  substantiate)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize11   "?id " samaJA )" crlf))
)

;#####################default rule################################
;@@@ Added by Anita 29-11-13
;They didn't realize the danger they were in.  [cambridge Dictionary]
;उन्हें अहसास नहीं हुआ कि वे कितने खतरे में थे ।
(defrule realize4
(id-root ?id realize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ahasAsa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realize.clp 	realize4   "?id "  ahasAsa_ho )" crlf))
)
