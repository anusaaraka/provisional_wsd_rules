;@@@ Added by 14anu-ban-10 on (23-02-2015)
;The garden was a riot of colours.[oald]
;बगीचा रंगो का एक भव्य प्रदर्शन था ।[manual]
(defrule riot2
(declare (salience 5100))
(id-root ?id riot)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?  )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bavya_praxarSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  riot.clp 	riot2   "  ?id "  Bavya_praxarSarna )" crlf))
)

;@@@ Added by 14anu-ban-10 on (23-02-2015)
;The movie is a laugh riot.[oald]
; चलचित्र एक मनोरंजक है.[manual]
(defrule riot3
(declare (salience 5200))
(id-root ?id riot)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoraMjaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  riot.clp 	riot3   "  ?id "  manoraMjaka )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule riot0
(declare (salience 5000))
(id-root ?id riot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaMgA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  riot.clp 	riot0   "  ?id "  xaMgA )" crlf))
)

;"riot","VT","1.xaMgA_karanA"
;During campaigning protesters rioted with opposition.
(defrule riot1
(declare (salience 4900))
(id-root ?id riot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaMgA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  riot.clp 	riot1   "  ?id "  xaMgA_kara )" crlf))
)

