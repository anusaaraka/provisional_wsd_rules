;@@@ Added by 14anu-ban-06 (20-04-2015)
; Many birds have a remarkable homing instinct. (OALD)[parser no.- 3]
;बहुत सारी चिडियों में घर लौटने का विलक्षण सहजज्ञान है . (manual)
(defrule homing1
(declare (salience 2000))
(id-word ?id homing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 instinct)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gara_lOtanA))
(assert  (id-wsd_viBakwi   ?id  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  homing.clp 	homing1   "  ?id "  Gara_lOtanA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  homing.clp      homing1   "  ?id " kA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (20-04-2015)
;The president's car is equipped with a homing device. (cambridge)[parser no.- 4]
;अध्यक्ष की गाडी लक्ष्यभेदी मिसाइल उपकरण के साथ लैस है . (manual)
(defrule homing0
(declare (salience 0))
(id-word ?id homing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRyaBexI_misAila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  homing.clp 	homing0   "  ?id "  lakRyaBexI_misAila )" crlf))
)
