
;@@@Added by 14anu-ban-07,(12-03-2015)
;You need to trawl through a lot of data to get results that are valid.(cambridge)
;आपको परिणाम जो वैध हैं  प्राप्त करने के लिए बहुत सारे  डेटा की छानबीन करने की जरूरत है . (manual)
(defrule trawl0
(id-root ?id trawl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CAnabIna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trawl.clp 	trawl0   "  ?id "  CAnabIna_kara )" crlf))
)

;@@@Added by 14anu-ban-07,(12-03-2015)
;They trawl these waters for cod.(cambridge)
;वे इस पानी में कोड मछली के लिए   खेंचू जाल से मछली मारते हैं . (manual)
(defrule trawl1
(declare (salience 1000))
(id-root ?id trawl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KeFcU_jAla_se_maCalI_mAra))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trawl.clp 	trawl1   "  ?id "  KeFcU_jAla_se_maCalI_mAra )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  trawl.clp      trawl1   "  ?id " meM )" crlf)
)
)
