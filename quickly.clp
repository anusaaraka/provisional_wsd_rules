;@@@ Added by 14anu-ban-11 aditi on 04-08-2014
;We'll repair it as quickly as possible.(old)
;He replied to my letter very quickly.
;usane awyanwa PatAPata mere pawra ko javAba xiyA.
(defrule quickly1
(declare (salience 10))
(id-root ?id quickly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PatAPata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* "  quickly.clp  quickly1   "  ?id "  PatAPata )" crlf))
)


;@@@ Added by 14anu-ban-11 on (12-12-2014)
;Act quickly , as there are tight time limits .(14anu)
;क्योंकि यहाँ तङ्ग समय सीमाए हैं,फुर्ती से कार्य करें.(manual)
(defrule quickly2
(declare (salience 20))
(id-root ?id quickly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 act)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PurwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* "  quickly.clp  quickly2   "  ?id "  PurwI )" crlf))
)

;-------------------DEFAULT RULE------------------------------------------------------
;@@@ Added by 14anu-ban-11 aditi on 04-08-2014
;She walked quickly away.(old)
;vaha jalxI xUra calI gayI.
;It quickly became clear that she was dying. 
;yaha jalxI spaRta ho gayA ki vaha mara rahI WI.
(defrule quickly0
(declare (salience 0))
(id-root ?id quickly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quickly.clp     quickly0   "  ?id "  jalxI )" crlf))
)

