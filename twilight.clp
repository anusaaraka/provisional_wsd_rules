;@@@Added by 14anu-ban-07,(21-02-2015)
;I could make out a dark figure in the twilight. (cambridge)
;मैंने धुँधले प्रकाश में एक काली आकृति पहचान ली .(manual) 
(defrule twilight0
(id-root ?id twilight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XuzXalA_prakASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twilight.clp 	twilight0   "  ?id "  XuzXalA_prakASa )" crlf))
)

;@@@Added by 14anu-ban-07,(21-02-2015)
;I knew him when he was in the twilight of his career.(cambridge)
;मैं उसको जानता था जब वह अपने कैरियर के पतन में था . (manual)
(defrule twilight1
(declare (salience 1000))
(id-root ?id twilight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twilight.clp 	twilight1   "  ?id "  pawana )" crlf))
)
