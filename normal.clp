;@@@Added by 14anu-ban-08 (09-02-2015)
;Let this velocity have a component vp parallel to the magnetic field and a component vn normal to it.  [NCERT]
;मान लीजिए इस वेग का चुम्बकीय क्षेत्र के समान्तर अवयव vp तथा इस क्षेत्र के अभिलम्बवत अवयव vn है.  [NCERT]
(defrule normal2
(declare (salience 5001))
(Domain physics)
(id-root ?id normal)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id2 component)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBilambavawa))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  normal.clp 	normal2   "  ?id "  aBilambavawa)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  normal.clp	normal2   "  ?id "  physics )" crlf))
)

;------------------------ Default Rules ---------------------

;"normal","Adj","1.sAmAnya"
;Serve wine at normal room temperature.
(defrule normal0
(declare (salience 5000))
(id-root ?id normal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmAnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  normal.clp 	normal0   "  ?id "  sAmAnya )" crlf))
)

;$$$ Modiifed by Manasa ( 11-02-2016 )  meaning changed from " - " to sAXAraNa_sWiwi
;;"normal","N","1."
;Things have returned to normal after the curfew.
(defrule normal1
(declare (salience 4900))
(id-root ?id normal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXAraNa_sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  normal.clp 	normal1   "  ?id " sAXAraNa_sWiwi  )" crlf)))



;"normal","Adj","1.sAmAnya"
;Serve wine at normal room temperature.
;--"2.prakqwa"
;He is  a perfectly normal child.

