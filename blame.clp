;@@@Added by 14anu-ban-02(27-03-2015)
;The government will have to take the blame for the riots.[oald]
;सरकार को दङ्गों के लिए जिम्मेदारी लेनी पडेगी . [self]
(defrule blame2
(declare (salience 100))
(id-root ?id blame)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ?id1)	;need sentences to restrict the rule
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jimmexArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blame.clp 	blame2   "  ?id "  uwwaraxAyiwva )" crlf))
)


;@@@Added by 14anu-ban-02(27-03-2015)
;She still blames him for Tony’s death.[cald]
;वह अभी भी  उसको टोनी की मृत्यु के लिए  दोषी ठहराती है . [self]
(defrule blame3
(declare (salience 100))
(id-root ?id blame)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)	;need sentences to restrict the rule
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRI_TaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blame.clp 	blame3   "  ?id "  xoRI_TaharA )" crlf))
)


;@@@Added by 14anu-ban-02(27-03-2015)
;When a team loses, it’s always the manager who takes the blame.[cald]
;जब दल हारता है, तब हमेशा प्रबन्धक है जो जिम्मेदारी लेता है . [self]
(defrule blame4
(declare (salience 100))
(id-root ?id blame)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 take)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jimmexArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blame.clp 	blame4   "  ?id "  uwwaraxAyiwva )" crlf))
)


;---------------------------default_rules-------------------------------------

(defrule blame0
(declare (salience 0))	;salience reduce to 0 from 5000 by 14anu-ban-02(27-03-2015)
(id-root ?id blame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blame.clp 	blame0   "  ?id "  xoRa )" crlf))
)

;"blame","VI","1.xoRa_lagAnA/aparAXI_TaharAnA"
;We blamed the accident on her
(defrule blame1
(declare (salience 0))	;salience reduce to 0 from 4900 by 14anu-ban-02(27-03-2015)
(id-root ?id blame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blame.clp 	blame1   "  ?id "  xoRa_lagA )" crlf))
)

