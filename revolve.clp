
(defrule revolve0
(declare (salience 5000))
(id-root ?id revolve)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id revolving )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id parikramI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  revolve.clp  	revolve0   "  ?id "  parikramI )" crlf))
)

;"revolving","Adj","1.parikramI"
;The five star hotel has a revolving restaurant.
;
(defrule revolve1
(declare (salience 100))
(id-root ?id revolve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  revolve.clp 	revolve1   "  ?id "  GUma )" crlf))
)

;"revolve","VTI","1.GUmanA"
;His life revolves around his office
;The earth revolves round the sun.
;

;@@@ Added by Shirisha Manju Suggested by Somaji and Aditiji on 08-11-14
;In the classical picture of an atom, the electron revolves round the nucleus much like the way a planet revolves round the sun. (ncert)
;परमाणु के क्लासिकी चित्रण में , इलेक्ट्रॉन नाभिक के चारों ओर ठीक ऐसे ही परिक्रमा करता है जैसे कि सूर्य के चारों ओर ग्रह परिक्रमा करते हैं .
(defrule revolve2
(declare (salience 4900))
(id-root ?id revolve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) round)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) parikramA_kara))
(assert (kriyA_id-object_viBakwi ?id kA)) ;Suggested object_viBakwi by Chaitanya Sir for above example (03-03-15)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " revolve.clp  revolve2  "  ?id "  " (+ ?id 1) "  parikramA_kara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  revolve.clp      revolve2   "  ?id " kA )" crlf)
)
