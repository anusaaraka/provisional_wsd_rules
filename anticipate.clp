;commented by 14anu-ban-02(27-02-2015)
;need to be written in anticipation file
;@@@ Added by 14anu13 on 14-06-14
;He bought extra food in anticipation of more people coming than he'd invited.
;वह अधिक भोजन ले आया इस पूर्वानुमान मे कि  जितने लोग उसने बुलाए है उससे अधिक लोग आएगे |

;(defrule anticipate0
;(id-root ?id anticipate)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id pUrvAnumAna))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipate.clp 	anticipate0   "  ?id " pUrvAnumAna ;)" crlf))
;)

;$$$Modified by 14anu-ban-02(27-02-2015)
;@@@ Added by 14anu13 on 14-06-14
;Other anticipated rewards of genetic engineering include safer , more potent vaccines , microbial strains capable of producing larger quantities of antibiotics and other drugs to cure a number of serious diseases .
;आनुवंशिक इंजीनियरी की भावी उपलब्धियों में सुरक्षित तथा अधिक प्रभावक्षम वैक्सीन बनाना तथा बहुत से रोगों को ठीक करने वाले  प्रतिजीवाणुओं की अधिक मात्रा बनाने की योजनाएं शामिल हैं .

(defrule anticipate1
(declare (salience 100));salience added by  14anu-ban-02(27-02-2015)
(id-root ?id anticipate )
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) reward|recompense|prize|winnings|award|honour|profit|advantage|benefit|bonus|premium);change id-word to id-root by14anu-ban-02(07-01-2015)
(viSeRya-viSeRaNa  =(+ ?id 1) ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAvI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipate.clp 	anticipate1   "  ?id " BAvI)" crlf))
)

;commented by 14anu-ban-02(27-02-2015)
;need to be written in anticipation file
;@@@ Added by 14anu13 on 14-06-14
;The courtroom was filled with anticipation.
;अदालत प्रत्याशा से भर गया था|

;(defrule anticipate2
;(id-root ?id anticipate)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(kriyA-with_saMbanXI  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id prawyASA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipate.clp 	anticipate2   "  ?id " prawyASA )" ;crlf))
;)

;@@@Added by 14anu-ban-02(27-02-2015)
;It is anticipated that inflation will stabilize at 3%.[oald]
;यह पूर्वानुमान किया हुआ है कि महँगाई 3 % तक स्थिर रहेगी. [self]
(defrule anticipate4
(declare (salience 100))
(id-root ?id anticipate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?id1)	;need sentences to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvAnumAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipate.clp 	anticipate4   "  ?id " pUrvAnumAna_kara)"crlf))
)
;---------------------------------------default-rules-----------------------------------------------------
;@@@Added by 14anu-ban-02(27-02-2015)
;We don't anticipate any major problems.[oald]
;हम किसी भी बड़ी समस्या की उम्मीद नहीं करते हैं . [self]
(defrule anticipate3
(declare (salience 0))
(id-root ?id anticipate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ummIxa_kara))
(assert (kriyA_id-object_viBakwi ?id  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipate.clp 	anticipate3   "  ?id " ummIxa_kara)"crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  anticipate.clp 	anticipate3   "  ?id " kA )" crlf)
))
