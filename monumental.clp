;$$$ Modified by 14anu06 on 13/6/2014
(defrule monumental0
(declare (salience 5000))
(id-root ?id monumental)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 knowledge|effort|denial|skill|talent) ; added 'knowledge|denial|skill|talent' by 14anu06 on 13/6/2014***
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AScaryajanaka))  ; changed meaning 'mahaw' as 'AScaryajanaka' by 14anu06 on 13/6/2014***
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  monumental.clp 	monumental0   "  ?id " AScaryajanaka )" crlf))
)

(defrule monumental1
(declare (salience 4900))
(id-root ?id monumental)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smArakIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  monumental.clp 	monumental1   "  ?id "  smArakIya )" crlf))
)

;"monumental","Adj","1.smArakIya"
;--"2.bqhawa"
;
;
