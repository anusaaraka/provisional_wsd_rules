;@@@ Added by 14anu-ban-01 on (05-01-2016)
;In a little while, the door was opened a tiny crack: the old woman eyed her visitor with evident distrust through the crack, and nothing could be seen but her little eyes, glittering in the darkness. [Crime And Punishment]
; थोड़ी ही देर बाद दरवाजा जरा-सा खुला. बुढ़िया ने दरार में से अजनबी को स्पष्ट अविश्वास के साथ देखा, अँधेरे में उसकी चमकती हुई, छोटी-छोटी आँखों के अलावा कुछ भी दिखाई नहीं दे रहा था  [ Crime And Punishment] 
(defrule tiny0
(declare (salience -1))
(id-root ?id tiny)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaZrA_sA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tiny.clp 	tiny0   "  ?id "  jaZrA_sA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-01-2016)
;She held the tiny baby in her arms. [oald]
;उसने नन्हे शिशु को बाहों में उठाया[self] 
(defrule tiny1
(declare (salience 100))
(id-root ?id tiny)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 baby|finger)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nanhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tiny.clp     tiny1   "  ?id "  nanhA )" crlf))
)
