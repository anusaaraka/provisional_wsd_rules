;@@@ Added by Preeti(28-12-13)
;People often confuse me and my twin sister. 
;loga aksara muJa meM Ora merI judavAz bahana meM gadabada karawe hEM.
(defrule confuse1
(declare (salience 1050))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-cat_coarse ?id2 conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadabada_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse1   "  ?id "  gadabada_kara )" crlf))
)

;@@@ Added by Preeti(28-12-13)
;I do not see how anyone could confuse me with my mother! 
;mEM nahIM pawA hE ki kEse koI muJa meM Ora merI mAz meM  gadabada_kara sakawA hE!
(defrule confuse2
(declare (salience 1050))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id  ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadabada_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse2   "  ?id "  gadabada_kara )" crlf))
)

;commented by 14anu-ban-03 (09-04-2015)
;@@@ Added by 14anu-ban-03 (04-08-2014)
;This law restricted for a conservative force should not be confused with the general law of conservation of energy of an isolated system (which is the basis of the First Law of Thermodynamics). [ncert]
;saMrakRI bala ke lie prawibanXiwa isa niyama ko kisI viyukwa nikAya ke lie vyApaka UrjA saMrakRaNa niyama (jo URmAgawikI ke pahale niyama kA AXAra hE) se Bramiwa nahIM honA cAhie.  [ncert]
;(defrule confuse3
;(declare (salience 2000))
;(id-root ?id confuse)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-with_saMbanXI  ?id  ?id1)  ;added by 14anu-ban-03 (29-11-2014)
;(id-root ?id1 law)    ;added by 14anu-ban-03 (29-11-2014)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Bramiwa_ho)) ;meaning is changed from 'Bramiwa' to 'Bramiwa_ho' by 14anu-ban-03 (10-10-2014)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse3   "  ?id "  Bramiwa_ho )" crlf))
;)


;@@@ Added by 14anu-ban-03 (09-04-2015)
;You are confusing me in front of other candidate. [hinkhoj]
;आप अन्य उम्मीदवार के सामने मुझे लज्जित कर रहे हैं . [manual]
(defrule confuse4
(declare (salience 2001))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_front_of_saMbanXI  ?id  ?id1)
(id-root ?id1 candidate)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lajjiwa_kara))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse4   "  ?id "  lajjiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (09-04-2015)
;He confused me very much. [hinkhoj]
;उसने मुझे अत्यन्त भ्रमित किया .  . [manual]
(defrule confuse5
(declare (salience 2000))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(object-object_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 much)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bramiwa_kara))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse5   "  ?id "  Bramiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (09-04-2015)
;These questions confuse even the experts. [hinkhoj]
;ये प्रश्न निपुण को भी उलझन में डाल देते हैं . [manual]
(defrule confuse6
(declare (salience 2000))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 expert)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJana_meM_dAla_xe))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse6   "  ?id "  ulaJana_meM_dAla_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (09-04-2015)
;The sudden rise in share prices has confused economists. [hinkhoj]
;शेयर मूल्यों में अचानक वृद्धि ने अर्थशास्त्रियों को चकरा दिया है .  [manual]
(defrule confuse7
(declare (salience 2000))
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 economist)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakarA_xe))
(assert (kriyA_id-object_viBakwi ?id ko))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  confuse.clp     confuse7   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse7   "  ?id "  cakarA_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (09-04-2015)
;My sister confused my room with posters. [hinkhoj]
;मेरी बहन ने पोस्टरों के साथ मेरा कमरा अस्तव्यस्त किया . [manual]
(defrule confuse8
(declare (salience 2000))   
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aswavyaswa_kara))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse8   "  ?id "  aswavyaswa_kara )" crlf))
)


;#############################Defaults rule#######################################

;$$$ Modified by 14anu-ban-03 (09-04-2015)
;$$$ Modified by 14anu13 on 24-06-14
;I was so confused that I could hardly compose my thoughts. 
;मैं इतना भ्रमित था कि मैंने मुश्किल से अपने विचार शान्त किए|
;मैं इतना भ्रमित हुआ था कि मैंने मुश्किल से अपने विचार शान्त किए|  [translation improved by 14anu-ban-03 (09-04-2015)
;@@@ Added by Preeti(28-12-13)
;These advertisements simply confused the public. 
;ye vijFApana  vAswava meM janawA ko ulaJA xewe hEM.
(defrule confuse0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (09-04-2015)
(id-root ?id confuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bramiwa_ho))          ; modified default meaning "ulaJA_xe" to "Bramiwa" by 14anu13  ;meaning changed from 'Bramiwa' to 'Bramiwa_ho' by 14anu-ban-03 (09-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confuse.clp 	confuse0   "  ?id "  Bramiwa_ho )" crlf))
)

