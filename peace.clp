;-------------------------------DEFAULT RULE--------------------------------------------------

;@@@ Added by 14anu-ban-09 on 25-08-2014
;The negotiators are trying to make peace between the warring factions. [OALD]
;cArwAkAro ne yuxXarawa gatoM ke bIca amana lAne kI koSiSa karI. [Own Manual]
 
(defrule peace0
(declare (salience 0000))
(id-root ?id peace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peace.clp 	peace0   "  ?id "  amana )" crlf))
)
;-------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 25-08-2014
;Only peace loving people get time for artistry . [Tourism Corpus]
;शांति प्रिय  प्रजा  को  ही  कलाकारीगीरी  के  लिए  समय  मिलता  है  ।


(defrule peace1
(declare (salience 3000))
(id-root ?id peace)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) loving)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) SAMwi_priya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " peace.clp  peace1  "  ?id "  " + ?id 1 "  SAMwi_priya  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-01-2015)
;A yajna was conducted from the eleventh day to the fourteenth day of the fortnight in Garh Mukteshwar for the peace of the souls killed in the war of the Mahabharata . 	[Total_tourism]
;एकादशी से चतुर्दशी तक गढ़ मुक्तेश्वर में महाभारत युद्ध में मारे गए सभी लोगों की आत्मा की शांति के लिए यज्ञ किया गया .	[Total_tourism]

(defrule peace2
(declare (salience 4000))
(id-root ?id peace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peace.clp 	peace2   "  ?id "  SAMwi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-01-2015)
;During that time lamps flowing with the stream of the Ganges appear very peace giving and charming . 	[Total_tourism]
;उस वक्त गंगा की धारा के साथ बहते दीपों का दृश्य बड़ा ही शांतिदायक और मनोहारी प्रतीत होता है . 	[Total_tourism]

(defrule peace3
(declare (salience 3000))
(id-root ?id peace)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) giving)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) SAMwi_xAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " peace.clp  peace3  "  ?id "  " (+ ?id 1) "  SAMwi_xAyaka  )" crlf))
)
