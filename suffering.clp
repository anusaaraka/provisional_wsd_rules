;@@@ Added by 14anu-ban-01 on (16-02-2015).
;Death finally brought an end to her suffering.[oald]
;मृत्यु ने  सदा के लिए (उसकी पीड़ा)/(उसके कष्ट) का अन्त कर दिया [self]
(defrule suffering0
(declare (salience 0))
(id-root ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIdA/kaRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suffering.clp 	suffering0   "  ?id " pIdA/kaRta )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-02-2015).
;The hospice aims to ease the sufferings of the dying.[oald]
;आश्रम मरणासन्न रोगियों के  दुखः को कम करने का उद्देश्य रखता है .  [self]
(defrule suffering1
(declare (salience 100))
(id-root ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuKaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suffering.clp 	suffering1   "  ?id " xuKaH)" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-02-2015).
;It became his personal mission to heal suffering children.[COCA]
;पीडित बच्चों को ठीक करना उसका वैयक्तिक लक्ष्य बन गया. [self]
(defrule suffering2
(declare (salience 100))
(id-word ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pIdiwa/xuHKI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  suffering.clp 	suffering2   "  ?id " pIdiwa/xuHKI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-02-2015).
;Most of the money spent on foreign aid never gets to the suffering people.[coca]
;विदेशी सहायता पर खर्च किया जाने वाला ज्यादातर पैसा  पीड़ितों तक नहीं पहुँच पाता है .  [self]
;(defrule suffering3
;(declare (salience 1000))
;(id-root ?id suffering)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 person|man)
;(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pIdiwa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  suffering.clp 	suffering3  "  ?id "  " ?id1 "  pIdiwa  )" crlf))
;)


;@@@ Added by 14anu-ban-01 on (16-02-2015).
; Thus through final purification, suffering souls could ascend to heaven.[COCA]
;इस प्रकार अन्तिम स्वच्छीकरण के द्वारा व्यथित आत्माएँ ऊपर स्वर्ग को जा सकीं . [self]
(defrule suffering4
(declare (salience 1000))
(id-root ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 soul|body)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suffering.clp 	suffering4   "  ?id " vyaWiwa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-02-2015).
;I am ready to risk suffering death by the hands of terrorists for my country .[self:with reference to COCA]
;मैं अपने देश के लिए आतङ्कवादियों के हाथों कष्टमयी मृत्यु का जोखिम उठाने को भी तैयार हूँ . [self]
(defrule suffering5
(declare (salience 1000))
(id-root ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 death|health)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaRtamayI/kaRtakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suffering.clp 	suffering5  "  ?id " kaRtamayI/kaRtakara)" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-02-2015).
;Hopefully her suffering years will end soon.[self: with reference to COCA]
;आशापूर्वक उसके कठिन/कष्टप्रद वर्ष शीघ्र ही समाप्त होंगे .  [self] 
(defrule suffering6
(declare (salience 100))
(id-word ?id suffering)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str)			
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kaRtapraxa/kaTina/xuHKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  suffering.clp 	suffering6   "  ?id " kaRtapraxa/kaTina/xuHKaxa)" crlf))
)

