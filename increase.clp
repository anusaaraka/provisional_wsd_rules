
;Added by Meena(14.5.10)
;We informed the new employees that no salary increase would be possible . 
(defrule increase0
(declare (salience 5000))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 salary)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vewana_vqxXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " increase.clp  increase0  "  ?id "  " ?id1 "  vewana_vqxXi  )" crlf))
)




;Salience reduced by Meena(14.5.10)
(defrule increase1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase1   "  ?id "  vqxXi )" crlf))
)

;He increased his speed to overtake the car.
;उसने गाडी आगे निकलने के लिए उसकी रफ्तार की/मे वृद्धि किया. 
;ere we must have to add vibhakti if we use meaning vqxXi_kara.
;(defrule increase2
;(declare (salience 4900))
;(id-root ?id increase)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id vqxXi_kara))
;(assert (kriyA_id-object_viBakwi ?id me)) ;Added by Prachi Rathore(19-10-13)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase2   "  ?id "  ;vqxXi_kara )" crlf))
;)




;$$$ Modified by Shirisha Manju 10-11-14; changed viBakwi 'me' as 'meM' and added print statement
(defrule increase3
(declare (salience 4800))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi_ho))
(assert (kriyA_id-subject_viBakwi ?id meM)) ;Added by Prachi Rathore(19-10-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase3   "  ?id "  vqxXi_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  increase.clp      increase3   "  ?id " meM )" crlf)
)
)

;"increase","V","1.vqxXi_karanA[honA]"
;He increased his speed to overtake the car.
;
;;

;$$$ Modified by 14anu-ban-06 (19-11-2014)
;### [COUNTER EXAMPLE] ### The use of different species in rotation allows for increased soil organic matter (SOM), greater soil structure, and improvement of the chemical and biological soil environment for crops.(agriculture)
;### [COUNTER EXAMPLE] ### आवर्तन में विभिन्न प्रजातियों का उपयोग भूमि कार्बनिक पदार्थ (सोम) में वृद्धि करने,अधिक से अधिक भूमि संरचना ,और फसलों के लिए रसायनिक द्रव्य और जौविक  भूमि पर्यावरण में सुधार में सहायक होता है .(manual)
;### [COUNTER EXAMPLE] ### With more SOM, water infiltration and retention improves, providing increased drought tolerance and decreased erosion.(agriculture)
;### [COUNTER EXAMPLE] ### अधिक सॉम से, पानी रिसना और अवरोधन सुधरता है,बशर्ते कि अनावृष्टि सहनशक्ति में वृद्धि होती हैं,और भूक्षरण में कमी होती हैं. (manual)
;added by Prachi Rathore(18-10-13)

;The cost of the project has increased dramatically since it began.
;शुरू होने के बाद से प्रोजेक्ट का खर्चा बड़ी तेजी से बढ चुका है .
;The government has been forced to climb down over the issue of increased taxes. 
;सरकार बढे हुए करों के विषय में नीचे की ओर जाने के लिए मजबूर की गयी है . 
;Her anxieties are shared by an increasing number of women.
;उसकी उत्सुकता स्त्रियों की बढती हुइ तादात के द्वारा  साझा की गयीं हैं . 
(defrule increase4
(declare (salience 4800))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ?)(kriyA-kriyA_viSeRaNa ?id ?)(kriyA-object))
;(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ?)(viSeRya-viSeRaNa ? ?id)(kriyA-kriyA_viSeRaNa ?id ?)(kriyA-object));removed relation '(viSeRya-viSeRaNa ? ?id)' by 14anu-ban-06 (19-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase4   "  ?id "  baDa )" crlf))
)

;@@@ Added by 14anu-ban-06 (08-11-2014)
;Great Britain with introduction in Israel appear to show that delaying exposure to peanuts can dramatically increase the risk of developing peanut allergies.(agriculture)
;ग्रॆट ब्रिटन इस्रैल में परिचय के साथ दिखाता है कि मूँगफली के अनावरण में देरी मूँगफली की एलर्जि का खतरा नाटकीय ढङ्ग से बढा सकती है.(manual)
;After this, more packets in flight will not increase the received rate.(COCA)
;इसके बाद, उड़ान में ज्यादातर पैकेट प्राप्त दर को नहीं बढाएँगे .(manual)
;Other enticements undertaken by the community colleges will likely increase the cost of education.(COCA)
;सामाजिक कालेजों के द्वारा अन्य लिए गये उत्तरदायित्व  शिक्षा की कीमत सम्भवतः बढाएँगे . (manual)
(defrule increase5
(declare (salience 4800))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 risk|rate|cost)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase5   "  ?id "  baDA )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-11-2014)
;The college has increased its intake of students by 50 percent this year. (cambridge);added by 14anu-ban-06 (11-02-2015)
;इस वर्ष कालेज ने 50 प्रतिशत के द्वारा विद्यार्थियों की भर्ती में वृद्धि की है . (manual)
;Farms were increasing production to meet with government demands, and Henry Ford became a great leader in the soybean industry.(agriculture)
;खेत सरकार की मांगों को  पूरा करने के लिए  उत्पादन में वृद्धि  कर रहे थे,और हेनरी फोर्ड सोयाबीन उद्योग में एक महान नेता बन गए।(manual)
(defrule increase6
(declare (salience 4900))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 production|intake);added 'intake' by 14anu-ban-06 (11-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi_kara))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase6   "  ?id "  vqxXi_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  increase.clp      increase6   "  ?id1 " meM )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (19-11-2014)
;With more SOM, water infiltration and retention improves, providing increased drought tolerance and decreased erosion.(agriculture)
;अधिक सॉम से, पानी रिसना और अवरोधन सुधरता है,बशर्ते कि अनावृष्टि सहनशक्ति में बढोतरी,और भूक्षरण में कमी होती हैं. (manual)
;The use of different species in rotation allows for increased soil organic matter (SOM), greater soil structure, and improvement of the chemical and biological soil environment for crops.(agriculture)
;आवर्तन में विभिन्न प्रजातियों का उपयोग भूमि कार्बनिक पदार्थ (सोम) में बढोतरी,अधिक से अधिक भूमि संरचना ,और फसलों के लिए रसायनिक द्रव्य और जौविक  भूमि पर्यावरण में सुधार में सहायक होता है .(manual)
(defrule increase7
(declare (salience 4900))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 tolerance|matter)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDowarI))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase7   "  ?id "  baDowarI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  increase.clp      increase7   "  ?id1 " meM )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (19-11-2014)
;After this, more packets in flight will not increase the received rate.(COCA)
;इसके बाद, उड़ान में ज्यादातर पैकेट प्राप्त दर को नहीं बढाएँगे .(manual)
;Other enticements undertaken by the community colleges will likely increase the cost of education.(COCA)
;सामाजिक कालेजों के द्वारा अन्य लिए गये उत्तरदायित्व  शिक्षा की कीमत सम्भवतः बढाएँगे . (manual)
(defrule increase8
(declare (salience 4900))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 tax|number)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase8   "  ?id "  baDa )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-12-2014)
;Each time the acceleration increases the energy of the particle.(NCERT)
;हर बार त्वरण से कण की ऊर्जा में वृद्धि होती है .(NCERT)
;हर बार त्वरण कण की ऊर्जा को बढाता है. (NCERT-improvised)
(defrule increase9
(declare (salience 4800))
(id-root ?id increase)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 energy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increase.clp 	increase9   "  ?id "  baDA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  increase.clp       increase9   "  ?id " ko )" crlf)
)
)
