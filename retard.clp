;@@@ Added by 14anu-ban-10 on (16-10-2014)
;Galileo concluded that an object moving on a frictionless horizontal plane must neither have acceleration nor retardation, i.e. it should move with constant velocity.  [ncert corpus]
;gElIliyo ne yaha niRkarRa nikAlA ki kisI GarRaNa rahiwa kREwija samawala para gawiSIla kisI vaswu meM na wo wvaraNa honA cAhie Ora na hI manxana, arWAw ise ekasamAna vega se gawi karanI cAhie .[ncert corpus]
(defrule retard0
(declare (salience 0000))
(id-root ?id retard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  retard.clp 	 retard0 "  ?id "  manxana )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-10-2014)
;Objects (i) moving down an inclined plane accelerate, while those (ii) moving up retard.  [ncert corpus]
;kisI ( @i ) Anawa samawala para nIce kI ora gawimAna vaswueM wvariwa howI hEM jabaki ( @ii ) wala para Upara kI ora jAne vAlI vaswuoM meM manxana howA hE.[ncert corpus]
(defrule retard1
(declare (salience 200))
(id-root ?id retard)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxana ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  retard.clp 	retard1 "  ?id "  manxana)" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-10-2014) 
;Likewise, an external force is needed also to retard or stop motion.  [ncert corpus]
;isI prakAra gawi ko rokane aWavA manxa karane ke lie BI bAhya bala kI AvaSyakawA howI hE.[ncert corpus]
(defrule retard2
(declare (salience 400))
(id-root ?id retard)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  retard.clp 	retard2 "  ?id "  manxa_kara)" crlf))
)

