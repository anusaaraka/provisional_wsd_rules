;@@@ Added by 14anu-ban-06 (27-02-2015)
;The plot of the film was too involved - I couldn't understand it. (cambridge)[parser no. 2]
;फ़िल्म का कथानक ज्यादा ही जटिल था- मैं उसे समझ नहीं सका . (manual)
(defrule involved0
(declare (salience 0))
(id-root ?id involved)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jatila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  involved.clp   involved0   "  ?id "  jatila )" crlf))
)


;@@@ Added by 14anu-ban-06 (27-02-2015)
;Try not to become too emotionally involved with the children in your care. (cambridge)[parser no. 4]
;आपकी देखभाल में बच्चों के साथ ज्यादा ही भावुकतापूर्वक सम्बद्ध होने का  प्रयास न कीजिए . (manual)
(defrule involved1
(declare (salience 2000))
(id-root ?id involved)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka ?id ?id1)
(id-root ?id1 emotionally|romantically)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambaxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  involved.clp   involved1   "  ?id "  sambaxXa )" crlf))
)
