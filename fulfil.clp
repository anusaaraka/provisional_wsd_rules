
;@@@ Added by 14anu-ban-05 on (10-12-2014)
;The solder situated at Araam temple was brought in use to fulfil the needs of palace premises and garden .[TOURISM]
;ArAma manxira sWiwa tAzkA mahala parisara waWA bAga kI jarUrawoM ko pUrA karane ke kAma meM AwA hogA .[TOURISM]
(defrule fulfil0
(declare (salience 5000))
(id-root ?id fulfil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fulfil.clp  	fulfil0   "  ?id "  pUrA_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-12-2014)
;Elang mountainous area is fulfilled with beautiful and natural views .[TOURISM]
;elAMga parvawIya kRewra KUbasUrawa va prAkqwika najAroM se paripUrNa hE .[TOURISM]
(defrule fulfil1
(declare (salience 5000))
(id-root ?id fulfil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id paripUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fulfil.clp  	fulfil1   "  ?id "  paripUrNa )" crlf))
)
