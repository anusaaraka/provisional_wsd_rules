;@@@Added by 14anu-ban-02(02-04-2015)
;One of several inns in the Lake District offering bonny accommodations and bountiful breakfasts.    [merriam-webster]	;run the sentence on parser no. 4
;लेक जिला में कई सरायों में से एक में अच्छी  रुकने की व्य्वस्था और प्रचुर जलपान उपलब्ध है.[self] 
(defrule bonny1 
(declare (salience 100)) 
(id-root ?id bonny) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 accomodation|accommodations) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id acCA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bonny.clp  bonny1  "  ?id "  acCA )" crlf)) 
) 

;--------------------default_rules---------------------------------
;@@@Added by 14anu-ban-02(02-04-2015)
;Sentence: A bonny baby.[cald]
;Translation: सुन्दर शिशु . [self]
(defrule bonny0 
(declare (salience 0)) 
(id-root ?id bonny) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sunxara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bonny.clp  bonny0  "  ?id "  sunxara )" crlf)) 
) 
