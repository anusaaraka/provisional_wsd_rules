;@@@ Added by 14anu-ban-10 on (20-03-2015)
;She did not expect the cold reception she received from her superiors.[hinkhoj]
;उसने उसके हल्के स्वीकृति  की आशा नहीं की थी जो उसे  उसके अफसर  से प्राप्त हुई.[manual]
(defrule reception1
(declare (salience 200))
(id-root ?id reception)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   svIkqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reception.clp 	reception1   "  ?id "  svIkqwi)" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-03-2015)
;Reception of T.V. programmes is satisfactory in Delhi.[hinkhoj]
;फ्3 कार्यक्रमों का अधिग्रहण दिल्ली में सन्तोषजनक है . 
(defrule reception2
(declare (salience 300))
(id-root ?id reception)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?  )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   aXigrahaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reception.clp 	reception2   "  ?id "   aXigrahaNa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-03-2015)
;That issue of the magazine had a favourable reception.[hinkhoj]
;पत्रिका के उस विषय का एक सहायक ग्रहण करने का ढ़ंग  था . [manual]
(defrule reception3
(declare (salience 400))
(id-root ?id reception)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 favourable)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   grahaNa_karane_kA_DZaMga ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reception.clp 	reception3   "  ?id "  grahaNa_karane_kA_DZaMga )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (20-03-2015)
;Her sons wedding reception was good.[hinkhoj]
;उसके बेटा का विवाहोत्सव का स्वागत अच्छा था . [manual]
(defrule reception0
(declare (salience 100))
(id-root ?id reception)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  svAgawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reception.clp        reception0   "  ?id "   svAgawa)" crlf))
)

