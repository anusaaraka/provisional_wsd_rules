;@@@ Added by 14anu-ban-04 (24-04-2015)
;He darted an impatient look at Vicky.         [oald]             ;run on parse no. 3
;उसने विकी पर अधीर नज़र डाली .                        [self]
(defrule dart3
(declare (salience 50))
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 look)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart3  "  ?id "   dAla )" crlf))
)

;@@@ Added by 14anu-ban-04 (24-04-2015)
;He darted Vicky an impatient look.         [oald]                 ;run on parse no. 16
;उसने विकी पर अधीर नज़र डाली.                     [self]
(defrule dart4
(declare (salience 40))                        
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(or(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id1 PropN))
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id2 look)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para))  
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  dart.clp     dart4   "  ?id "  para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart4  "  ?id "   dAla )" crlf))
)


;@@@ Added by 14anu-ban-04 (24-04-2015)
;She made a dart for the door.         [oald]
;उसने दरवाजे की ओर दौड़ लगाई .                [self]
;We made a dart for the exit.           [cald]
;हमने गमन की ओर दौड़  लगाई.                 [self]
(defrule dart5
(declare (salience 30))                         
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id)
(kriyA-for_saMbanXI ?kri ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart5   "  ?id "  xOda )" crlf))
)

;@@@ Added by 14anu-ban-04 (24-04-2015)
;Her favorite game is darts.                 [merriam-webster]   
;उसका मनपसन्द खेल डार्ट है .                          [self]
(defrule dart6
(declare (salience 20))                
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(subject-subject_samAnAXikaraNa  ?id1 ?id)(viSeRya-of_saMbanXI ?id1 ?id))
(id-root ?id1 game|tournament) 
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id dArta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  dart.clp 	dart6   "  ?id "  dArta)" crlf))
)

;@@@ Added by 14anu-ban-04 (24-04-2015)
;Nina felt a sudden dart of panic.             [oald]
;नीन ने अचानक संत्रास का  अहसास/अनुभव महसूस किया .      [self]
(defrule dart7
(declare (salience 20))                
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?kri feel) 
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ahasAsa/anuBava))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  dart.clp 	dart7  "  ?id "  ahasAsa/anuBava)" crlf))
)

;$$$ Modified by 14anu-ban-04 (24-04-2015)        ----changed meaning from 'PeMka' to 'nikAla' 
;The frog darted its tongue at a fly.                  [merriam-webster]
;मेढक ने मक्खी की ओर अपनी जीभ निकाली .                          [self]
(defrule dart2  
(declare (salience 30))                        ;salience reduced from '4800' to '30' by 14anu-ban-04 (24-04-2015)
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart2   "  ?id "  nikAla )" crlf))
)

;----------------------------------------- Default Rules -----------------------
(defrule dart0
(declare (salience 10))                ;salience reduced from '5000' to '10' by 14anu-ban-04 (24-04-2015)
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart0   "  ?id "  BAlA )" crlf))
)

;"dart","N","1.BAlA/ballama"
;A trekker was injured by a dart shot by the attacking tribals.
;

;$$$ Modified by 14anu-ban-04 (24-04-2015)        ;----changed meaning from 'Japata' to 'xOda' 
;A dog darted across the road in front of me.              [oald]
;कुत्ता मेरे सामने सड़क की  ओर दौड़ा .                                 [self]
(defrule dart1
(declare (salience 10))                         ;salience reduced from '4900' to '10' by 14anu-ban-04 (24-04-2015)
(id-root ?id dart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dart.clp 	dart1   "  ?id "  xOda )" crlf))
)

;"dart","VI","1.JapatanA/Jatapata_cala_padZanA/nikala_padZanA"
;His forefinger darted in all directions as he spoke
;
;"dart","VT","1.PeMkanA"
