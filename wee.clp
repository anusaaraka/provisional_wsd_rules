;@@@ Added by 14anu-ban-11 on (20-01-2015)
;The train arrived at the station in the wee hours.(example of wee2 )
;रेलगाडी  सुबह-सुबह स्टेशन पर आई . (manual)
(defrule wee3
(declare (salience 4900)) ; added salience
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 hour)
=>
(retract ?mng)
;(assert (id-wsd_word_mng ?id kuCa)) ;commented by 14anu-ban-11 on (03-02-2015) 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 subaha-subaha))  ;Added by 14anu-ban-11 on (03-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  wee.clp  	wee0   "  ?id "  kuCa )" crlf)) ;commented by 14anu-ban-11 on (03-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wee.clp  wee3  "  ?id "  " ?id1 "  subaha-subaha )" crlf))  ;Added by 14anu-ban-11on (03-02-2015)
)

;@@@ Added by 14anu-ban-11 on (04-02-2015)
;If you put wee bit of water it gradually blossoms.(coca)
;यदि आपने थोडा सा पानी  डाला तो यह धीरे धीरे खिलता है . (self)
(defrule wee4
(declare (salience 4901)) 
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 bit)
=>
(retract ?mng)
 (assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 WodA_sA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wee.clp  wee4  "  ?id "  " ?id1 "  WodA_sA )" crlf))  
)

;@@@ Added by 14anu-ban-11 on (04-02-2015)
;I’m going for a wee.(oald)
;मैं पेशाब जा रहा हूँ . (self)
(defrule wee5
(declare (salience 4902)) 
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI  ?id1 ?id)
(id-root ?id1 go)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wee.clp 	wee5   "  ?id "  peSAba )" crlf))
)

;-------------------- Default rules ---------------------

(defrule wee0
(declare (salience 5000))
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id weed )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id GAsa_PUsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  wee.clp  	wee0   "  ?id "  GAsa_PUsa )" crlf))
)

;"weed","N","1.GAsa_PUsa"
;Weeds spread in the garden.
;
(defrule wee1
(declare (salience 4900))
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id weed )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id moWA_hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  wee.clp  	wee1   "  ?id "  moWA_hatA )" crlf))
)

;"weed","VT","1.moWA_hatAnA"
;Remove the weeds from the lawn.
;


;$$$ Modified by 14anu-ban-11 on (03-02-2015)
;I was in the wee room when she came.(oald) ;Added by 14anu-ban-11 on (20-01-2015) 
;जब वह आई मै छोटे से कमरे मे था.(manual)
(defrule wee2
(declare (salience 4800))
(id-root ?id wee)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)  ;commented by 14anu-ban-11 on (03-02-2015)
(id-cat_coarse ?id noun) ;Added by 14anu-ban-11 on (03-02-2015)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id CotA_sA)) ;commented by 14anu-ban-11 on (03-02-2015)
(assert (id-wsd_root_mng ?id CotA_se));Added by 14anu-ban-11 on (03-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wee.clp 	wee2   "  ?id "  CotA_sA )" crlf));commented by 14anu-ban-11 on (03-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wee.clp 	wee2   "  ?id "  CotA_se )" crlf));Added by 14anu-ban-11 on (03-02-2015)
)

;"wee","Adj","1.CotA_sA[>AramBika]"
;--"2.CotA_sA"
;
