;$$$Modified by 14anu-ban-02(14-01-2015)
;catagory added.
;He got the breakthrough.
;उसने सफलता प्राप्त की .
;@@@ Added by 14anu03 on 23-june-14
;He got the breakthrough.
;उसने सफलता प्राप्त की .
(defrule breakthrough0
(declare (salience 0))
(id-root ?id breakthrough)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id1 ?id)	;commented by 14anu-ban-02(14-01-2015)
;(id-word ?id1 got)		;commented by 14anu-ban-02(14-01-2015)
(id-cat_coarse ?id noun)	;added by 14anu-ban-02(14-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  breakthrough.clp 	breakthrough0   "  ?id "  saPalawA )" crlf))
)

;@@@ Added by 14anu03 on 23-june-14
;He was able to breakthrough that defence.
;वह रक्षा भेदने मे समर्थ था .
(defrule breakthrough1
(declare (salience 100))	;salience increased to 100 from 0 by 14anu-ban-02(14-01-2015)
(id-root ?id breakthrough)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 defence|army)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bexane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  breakthrough.clp 	breakthrough1   "  ?id "  Bexane )" crlf))
)
