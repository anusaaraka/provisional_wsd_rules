;@@@ Added 14anu-ban-07, 04-08-2014
;A pollen grain blowing in the wind is unlikely to land on a receptive stigma.(sentence taken from blow.clp file)
;हवा में उडते हुए एक पराग कण एक ग्रहणशील वर्तिकाग्र पर उतरना असम्भव है .
(defrule unlikely0
(id-root ?id unlikely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asamBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unlikely.clp 	unlikely0   "  ?id "  asamBava )" crlf))
)

;@@@ Added 14anu-ban-07, 04-08-2014
;She gave me an unlikely explanation for her behaviour.(oald)
;उसके बर्ताव के लिए उसने मुझे एक अविश्वसनीय व्याख्या दी . 
(defrule unlikely1
(declare (salience 1500))
(id-root ?id unlikely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 explanation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aviSvasanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unlikely.clp 	unlikely1   "  ?id "  aviSvasanIya )" crlf))
)
