;@@@Added by 14anu-ban-08 (02-03-2015)
;2 . Decision of monetary bill , if it is not attested by speaker then that will not be considered as monetary bill and his decision will be final and obligatory .  [karan singla]
;2 . धन बिल का निर्धारण स्पीकर करता है यदि धन बिल पे स्पीकर साक्ष्यांकित नही करता तो वह धन बिल ही नही माना जायेगा उसका निर्धारण अंतिम तथा बाध्यकारी होगा. [karan singla]
(defrule monetary0
(declare (salience 0))
(id-root ?id monetary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  monetary.clp 	monetary0   "  ?id "  Xana )" crlf))
)

;@@@Added by 14anu-ban-08 (02-03-2015)
;All monetary transactions were looked after by her.  [hinkhoj]
;पैसे से संबंधित लेन-देन वह  सँभालेगी.  [self]
(defrule monetary1
(declare (salience 10))
(id-root ?id monetary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 transaction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEse_se_saMbaMXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  monetary.clp 	monetary1   "  ?id "  pEse_se_saMbaMXiwa )" crlf))
)
