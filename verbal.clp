
(defrule verbal0
(declare (salience 5000))
(id-root ?id verbal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOKika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  verbal.clp 	verbal0   "  ?id "  mOKika )" crlf))
)

(defrule verbal1
(declare (salience 4900))
(id-root ?id verbal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOKika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  verbal.clp 	verbal1   "  ?id "  mOKika )" crlf))
)

;"verbal","Adj","1.mOKika"
;Verbal communication helps people to communicate well
;
;
;@@@Added by 14anu-ban-07,(30-03-2015)
;The job applicant must have good verbal skills.(oald)
;काम प्रार्थी में अच्छी शाब्दिक प्रवीणता होनी चाहिये. (manual)
(defrule verbal2
(declare (salience 5000))
(id-root ?id verbal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 skill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAbxika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  verbal.clp 	verbal2   "  ?id "  SAbxika )" crlf))
)
