;$$$ Modified by 14anu-ban-06 (31-01-2015)
;The recent hike in train fares came as a shock to commuters.(cambridge)
;हाल ही में रेलगाडी किराये में  उछाल आने-जाने वालो के लिए सदमे की तरह आया . (manual)
;@@@ Added by 14anu03 on 24-june-14
;There was sudden hike in the prices of petrol.
;पेट्रोल के मूल्यों में अचानक उछाल अायी . 
(defrule hike100
(declare (salience 5500))
(id-root ?id hike)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 price|fare); removed 'prices' , modified 'id-word' as 'id-root' and added 'fare' by 14anu-ban-06 (31-01-2015)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id uCAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hike.clp  	hike100   "  ?id "  uCAla )" crlf))
)

;$$$ Modified by 14anu-ban-06 (15-11-2014)
;@@@ Added by 14anu04 on 29-5-2014
;The vegetable prices were hiked.
;सब्जी मूल्य बढाए गये थे .
(defrule hike03
(declare (salience 5600))
(id-root ?id hike)
(id-root ?id1 price|rate|interest|cost|use)
(kriyA-subject ?id ?id1);added by 14anu-ban-06 (15-11-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb|noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hike.clp     hike03   "  ?id "  baDA )" crlf))
)

;@@@ Added 14anu-ban-06 (31-01-2015) 
;The government hiked up the price of milk by over 40%.(OALD)
;सरकार ने दूध का मूल्य 40 % से अधिक बढा दिया . (manual)
(defrule hike3
(declare (salience 5000))
(id-word ?id hiked)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDZA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hike.clp	hike3  "  ?id "  " ?id1 "  baDZA_xe  )" crlf))
)

;@@@ Added by 14anu-ban-06 (07-02-2015)
;She hiked up her skirt and waded into the river. (OALD)
;उसने अपनी स्कर्ट ऊपर उठायी और नदी में कठिनाई से चली . (manual)
(defrule hike4
(declare (salience 5300))
(id-word ?id hiked)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 skirt|kilt|trouser)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Upara_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hike.clp	hike4  "  ?id "  " ?id1 "  Upara_uTA  )" crlf))
)
;------------------Default Rules ----------------------------
(defrule hike0
(declare (salience 5000))
(id-root ?id hike)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id hiking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pExala_laMbI_yAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hike.clp  	hike0   "  ?id "  pExala_laMbI_yAwrA )" crlf))
)

;"hiking","N","1.pExala laMbI yAwrA"
;parvawArohI"hiking" ke lie viSeRa prakAra ke jUwe pahanawe hEM.
;
(defrule hike1
(declare (salience 4900))
(id-root ?id hike)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExala_laMbI_yAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hike.clp 	hike1   "  ?id "  pExala_laMbI_yAwrA )" crlf))
)

;"hike","N","1.pExala_laMbI_yAwrA"
;amaranAWa jAne vAle yAwrI'hike' karawe hE
;
(defrule hike2
(declare (salience 4800))
(id-root ?id hike)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAxa_yAwrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hike.clp 	hike2   "  ?id "  pAxa_yAwrA_kara )" crlf))
)

;"hike","V","1.pAxa_yAwrA_karanA"
;caMxraSeKara ne kanyAkumArI se kaSmIra waka'hike' kI WI.
;
