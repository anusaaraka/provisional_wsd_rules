;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu06(VIvek Agarwal) on 20/6/2014*****
;He withdrew his nomination.
;उसने उसका नामाङ्कन वापिस लिया . 
(defrule withdrew0
(declare (salience 5000))
(id-root ?id withdraw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApisa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  withdraw.clp  withdrew0   "  ?id "  vApisa_le)" crlf))
)

;@@@ Added by 14anu06(Vivek Agarwal) on 20/6/2014*****
;He withdrew from public life.
;उसने सार्वजनिक जीवन से अलग कराया 
(defrule withdrew1
(declare (salience 4900))
(id-root ?id withdraw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object ?id ))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  withdraw.clp  withdrew1   "  ?id "  alaga_karA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-02-2015)
;He is totally withdrawn from his friends.(hinkhoj)
;वह उसके मित्रों से पूर्ण रूप से अमिलनसार है .(anusaaraka) 
(defrule withdrew2
(declare (salience 4901))
(id-root ?id withdraw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 friend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amilanasAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  withdraw.clp  withdrew2   "  ?id "  amilanasAra)" crlf))
)
 
;@@@ Added by 14anu-ban-11 on (08-04-2015)
;He gestured to the guards and they withdrew.    [OALD]
;उसने सुरक्षाकर्मियों को इशारा किया और वे  हट गये .     [MANUAL]
(defrule withdrew3
(declare (salience 4902))
(id-root ?id withdraw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 they)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  withdraw.clp  withdrew3   "  ?id "  hata_jA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (18-04-2015)
;At the age of 70, she withdrew into graceful retirement.[OALD]   
;70 की उम्र में, उसने  विनीत भाव से  सेवा-निवृत्ति ले लिया . [MANUAL]
(defrule withdrew4
(declare (salience 4903))
(id-root ?id withdraw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 retirement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_liyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  withdraw.clp  withdrew4   "  ?id "  le_liyA)" crlf))
)

