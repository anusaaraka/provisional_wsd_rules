;@@@ Added by 14anu-ban-11 on (11-04-2015)
;John was shunted sideways to a job in sales.(oald)
;जॉन बिक्री में काम करने के लिये मुडा गया था . (self) (not sure)
(defrule shunt2
(declare (salience 10))
(id-root ?id shunt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mudZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shunt.clp    shunt2  "  ?id "  mudZa)" crlf))
)


;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (11-04-2015)
;This person guides the driver to shunt the train. (hinkhoj)
;इस व्यक्ति ने  रेलगाडी को दूसरी पटरी पर ले जाने के लिए चालक का मार्गदर्शन किया है . (self)
(defrule shunt0
(declare (salience 00))
(id-root ?id shunt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarI_patarI_para_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shunt.clp 	shunt0  "  ?id "  xUsarI_patarI_para_le_jA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (11-04-2015)
;If the artery is too narrow, a small 4mm shunt is inserted to widen it.(oald)
;यदि धमनी ज्यादा ही सङ्कीर्ण है,तो उसको चौडा करने के लिए छोटा 4mm विद्युत् उपमार्ग  को डाल दिया  है . (self)(not sure)
(defrule shunt1
(declare (salience 01))
(id-root ?id shunt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuw_upamArga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shunt.clp 	shunt1  "  ?id "  vixyuw_upamArga)" crlf))
)

