;@@@ Added by 14anu-ban-05 on (23-01-2015)
;A statue was erected to glorify the country's national heroes.[cald]
;देशभक्त वीरों को गौरवान्वित करने के लिए एक मूर्ति देश  स्थापित किया गया था. [manual]
(defrule glorify0
(declare (salience 100))
(id-root ?id glorify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gOravAnviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glorify.clp 	glorify0   "  ?id "  gOravAnviwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (23-01-2015)
;In the temples of Jagat and Unwas the Mahishamardini form of Durga has been glorified .[tourism]
;jagawa Ora unavAsa ke maMxiroM meM xurgA ke mahiRamarxinI rUpa ko pUjA jAwA hE .[manual]
(defrule glorify1
(declare (salience 101))
(id-root ?id glorify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 temple)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUjA_jA))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glorify.clp 	glorify1   "  ?id "  pUjA_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  glorify.clp 	glorify1   "  ?id " ko )" crlf))
)

