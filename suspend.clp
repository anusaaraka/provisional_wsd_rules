
;@@@ Added by 14anu-ban-01 on (03-02-2015)
;A thin long piece of a magnet, when suspended freely, pointed in the north-south direction.[NCERT-corpus]
;चुम्बक का एक पतला लम्बा टुकडा, [स्वतन्त्रतापूर्वक] लटकाए जाने पर, हमेशा उत्तर-दक्षिण दिशा के अनुदिश ठहरता था.[NCERT-corpus]
;When suspended freely, these poles point approximately towards the geographic north and south poles, respectively.[NCERT-corpus]
;जब छड चुम्बक को [स्वतन्त्रतापूर्वक] लटकाया जाता है तो ये ध्रुव क्रमशः लगभग भौगोलिक उत्तरी एवं दक्षिणी ध्रुवों की ओर सङ्केत करते हैं[NCERT-corpus]
(defrule suspend2
(declare (salience 4900))
(id-root ?id suspend)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 freely)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 latakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  suspend.clp 	suspend2   "  ?id "  " ?id1 "  latakA )" crlf))
)


;--------------------- Default Rules --------------
(defrule suspend0
(declare (salience 5000))
(id-root ?id suspend)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id suspended )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWagiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  suspend.clp  	suspend0   "  ?id "  sWagiwa )" crlf))
)

;"suspended","Adj","1.sWagiwa"
;
(defrule suspend1
(declare (salience 4900))
(id-root ?id suspend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWagiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspend.clp 	suspend1   "  ?id "  sWagiwa_kara )" crlf))
)

;default_sense && category=verb	nilambiwa_kara	0
;"suspend","V","1.nilambiwa_karanA"
;He was suspended from his job for misconduct towards his superior.
;--"2.sWagiwa_karanA"
;Police had suspended the case.
;
;
