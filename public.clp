
(defrule public0
(declare (salience 5000))
(id-root ?id public)
?mng <-(meaning_to_be_decided ?id)
(or (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) (id-cat_coarse ?id adjective)) ;Added by sukhada Ex: Do not smoke at the public places.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArvajanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  public.clp 	public0   "  ?id "  sArvajanika )" crlf))
)

;Commented by Shirisha Manju : this rule is similar to public0 with different meaning. No example sentence is given
;;@@@ Added by avni (14anu11)
;(defrule public2
;(declare (salience 5050))
;(id-root ?id public)
;?mng <-(meaning_to_be_decided ?id)
;(or (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) (id-cat_coarse ?id adjective)) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id janawA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  public.clp 	public2  "  ?id "  janawA )" crlf))
;)

;The Gulf national , whose arrest is yet to be made public , revealed that he underwent arms training at the Al Qaida ' s
;खाडी के इस निवासी की गिरतारी को अभी सार्वजनिक नहीं किया गया है . उसने बताया कि उसने कंधार के पास अल . कायदा के अल फारूक शिविर में हथियारों का प्रशिक्षण लिया था .
;@@@ Added by avni (14anu11)
(defrule public3
(declare (salience 6000))
(id-root ?id public)
(id-root =(- ?id 1) make)
?mng <-(meaning_to_be_decided ?id)
(or (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) (id-cat_coarse ?id adjective)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArvajanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  public.clp 	public3  "  ?id "  sArvajanika )" crlf))
)

;$$$ Modified by Shirisha Manju 7-10-2016
;They have promised that the development will not encroach on public land. 
;ve vAxA_kara cuke hEM ki vikAsa sArvajanika BUmi para awikramaNa_nahIM karegA.
;@@@ Added by 14anu-ban-09 on (20-02-2015)
;A campaign to ban smoking in public places.		[oald]	;added by 14anu-ban-09 on (09-03-2015)
;सार्वजनिक स्थानों में धूम्रपान पर प्रतिबंध लगाने के लिए एक अभियान. 	[self]	;added by 14anu-ban-09 on (09-03-2015)
;The campaign was designed to increase public awareness of the problem. [same clp file]
;समस्या की सार्वजनिक जागरुकता को बढ़ाने के लिए अभियान बनाया गया हैं . 	[manual]
;We should not pluck the flowers from the public park. [pluck.clp]
;हमें सार्वजनिक उद्यान से फूल नहीं तोडने चाहिए . 	[Anusaaraka]

(defrule public4
(declare (salience 5500))
(id-root ?id public)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 park|awareness|place|land)	;added 'place' by 14anu-ban-09 on (09-03-2015) ;added 'land' by Shirisha Manju 7-10-2016
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArvajanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  public.clp 	public4   "  ?id "  sArvajanika )" crlf))
)

;"public","Adj","1.sArvajanika"
;The campaign was designed to increase public awareness of the problem.
;
;"public","N","1.janawA"
;The public has a right to know what is in this report.
(defrule public1
(declare (salience 4900))
(id-root ?id public)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  public.clp 	public1   "  ?id "  janawA )" crlf))
)

