
(defrule tile0
(declare (salience 5000))
(id-root ?id tile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaparEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tile.clp 	tile0   "  ?id "  KaparEla )" crlf))
)


;You can help them by doing the following : Spell out words with fridge magnets , or letter tiles from word games 
;निम्नलिखित का अनुसरण करके आप उन की मदद कर सकते हैंः .
;छोटे बच्चों को बिन्दुओं को जोडते हुए अक्षरों का आकार बनाने की या आप के द्वारा पेन्सिल के आंके गये आकारों पर पेन चलाने की आदत डलवाएं .
;@@@ Added by avni(14anu11)
(defrule tile2
(declare (salience 5000))
(id-root ?id tile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root =(- ?id 1) letter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peMsil_ke_aMke_gye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tile.clp 	tile2   "  ?id "  peMsil_ke_aMke_gye)" crlf))
)




;"tile","N","1.KaparEla"
;Tiles are used for roof .
;--"2.patiyA"
;She is sitting on a tile.
;
(defrule tile1
(declare (salience 4900))
(id-root ?id tile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaparEla_se_Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tile.clp 	tile1   "  ?id "  KaparEla_se_Daka )" crlf))
)

;"tile","VT","1.KaparEla_se_Daka"
;The roof top is tiled  by straw.
;
