;@@@ Added by 14anu-ban-03 (30-03-2015)
;I don’t like seeing animals in coops. [oald]
;मैं पशुओं को पिन्जरे में देखना पसन्द नहीं करता हूँ . [manual]
(defrule coop3
(declare (salience 5000))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(kriyA-object ?id1 ?id2)
(id-root ?id1 see)
(id-root ?id2 animal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pinjarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coop.clp 	coop3   "  ?id "  pinjarA )" crlf))
)


;@@@ Added by 14anu-ban-03 (30-03-2015)
;The hostages had been cooped for so long that they couldn't cope with the outside world. [cald]
;बन्धक व्यक्ति इतने लम्बे समय के लिए कैद किए गये थे कि वे बाहरी दुनिया का सामना नहीं कर सके . [manual]
(defrule coop4
(declare (salience 5000))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 hostage)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kExa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coop.clp 	coop4  "  ?id "  kExa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (30-03-2015)
;All the mines in this area were cooped in the 1980s. [cald]
;इस क्षेत्र की सभी खानों को 1980s में बन्द कर दिया गया था . [manual]
(defrule coop5
(declare (salience 5000))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 mine)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coop.clp 	coop5   "  ?id " baMxa_kara_xe )" crlf))
)

;$$$ Modified by 14anu-ban-03 (30-03-2015)
;You can't coop her up in the house. [same clp]
;आप घर में उसको बन्द नहीं रख सकते हैं . [manual]
(defrule coop0
(declare (salience 5000))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_raKa))  ;meaning changed from 'kisI_ko_kisI_CotI_jagaha_meM_raKa' to 'banxa_raKa' by 14anu-ban-03 (30-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " coop.clp	coop0  "  ?id "  " ?id1 "  banxa_raKa  )" crlf))
)

; You can't coop her up in the house.
;wuma use Gara meM nahIM raKa sakawe

;---------------------------------------------Default Rules-------------------------------------

(defrule coop1
(declare (salience 4900))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xadZabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coop.clp 	coop1   "  ?id "  xadZabA )" crlf))
)

;"coop","N","1.xadZabA"
;Don't keep the chickens in the small coop.
;
(defrule coop2
(declare (salience 4800))
(id-root ?id coop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xadZabe_meM_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coop.clp 	coop2   "  ?id "  xadZabe_meM_baMxa_kara )" crlf))
)

;"coop","VT","1.xadZabe_meM_baMxa_karanA"
;She had been cooped up indoors all day because of her board exams.
;
