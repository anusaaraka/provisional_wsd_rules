;@@@ Added by 14anu-ban-04 (18-04-2015)
; She has just disentangled herself from a painful relationship.             [oald]
;उसने अभी दुखदायी सम्बन्ध से स्वयं को  अलग किया है .                [self]
(defrule disentangle1
(declare (salience 30))
(id-root ?id disentangle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disentangle.clp 	disentangle1   "  ?id "  alaga_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (18-04-2015)
;He tried to disentangle his fingers from her hair.             [oald]
;उसने उसके केश से अपनी उंगलियाँ निकालने का प्रयास किया .                      [self]
(defrule disentangle2
(declare (salience 40))
(id-root ?id disentangle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 truth|finger)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disentangle.clp 	disentangle2   "  ?id "  nikAla )" crlf))
)


;-------------------------------------------------------DEFAULT RULES-----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (18-04-2015)
;He was sitting on the deck disentangling a coil of rope.    [oald]
;वह डेक पर बैठा  रस्सी के  गुच्छे को सुलझा रहा था .                            [self]
;I tried to disentangle the wires under my desk.            [cald]
;मैंने मेरे मेज़ के अन्दर तारों को सुलझाने का प्रयास किया .                    [self]
(defrule disentangle0
(declare (salience 10))
(id-root ?id disentangle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert(kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id sulaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disentangle.clp     disentangle0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disentangle.clp 	disentangle0   "  ?id "  sulaJA )" crlf))
)

