;Added by Meena(9.3.10)
;Thomas Edison tried two thousand different materials in search of a filament for the light bulb . 
(defrule material0
(declare (salience 0))           ;Decreased Salience by 14anu-ban-08
(id-root ?id material)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxArWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  material.clp       material0   "  ?id "  paxArWa )" crlf))
)

;@@@ Added by 14anu-ban-08 on 16-08-2014
;From Eq. (9.16), we see that to reduce the bending for a given load, one should use a material with a large Young's modulus Y.  
;samIkaraNa (9.16) se hama xeKawe hEM ki kisI xiye hue BAra ke lie baMkana kama karane ke lie Ese xravya kA upayoga karanA cAhie jisakA yaMga guNAMka Y aXika ho.
(defrule material1
(declare (salience 5000))
(id-root ?id material)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-det_viSeRaNa  ?id ?id1)(viSeRya-of_saMbanXI ?id1 ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xravya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  material.clp       material1   "  ?id "  xravya )" crlf))
)

;@@@ Added by 14anu-ban-08 on (29-11-2014)
;The world has an astonishing variety of materials and a bewildering diversity of life and behavior.    [NCERT]
;संसार में पदार्थों के आश्चर्यचकित करने वाले प्रकार तथा जीवन एवं व्यवहार की विस्मयकारी विभिन्नताएँ हैं.     [NCERT]
(defrule material2
(declare (salience 5001))
(id-root ?id material)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 variety)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxArWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  material.clp       material2   "  ?id "  paxArWa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (24-04-2015)
;She's collecting material for her latest novel.  [oald]
;उसने अपनी आने वाले उपन्यास के लिए विचार एकत्रित किये.  [self]
(defrule material3
(declare (salience 5002))
(id-root ?id material)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 novel)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  material.clp       material3   "  ?id "  vicAra )" crlf))
)
