;@@@ Added by 14anu-ban-04 (11-04-2015)
;The judge enjoined them from selling the property.                 [merriam-webster]
;न्यायाधीश ने संपत्ति  बेचने से उन पर रोक लगाया .                                     [self]
(defrule enjoin1
(declare (salience 20))
(id-root ?id  enjoin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-from_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para))  
(assert (id-wsd_root_mng ?id roka_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  enjoin.clp     enjoin1   "  ?id "  para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   enjoin.clp 	 enjoin1  "  ?id "  roka_lagA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (11-04-2015)
;Sita's mother enjoined her to behave properly before guests.                  [hinkhoj]
;अतिथियों के सामने  अच्छी तरह से व्यवहार करने के लिए सीता की माँ ने उसको आदेश दिया   .                   [self]
(defrule enjoin0
(declare (salience 10))
(id-root ?id  enjoin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   enjoin.clp 	 enjoin0  "  ?id "  AxeSa_xe)" crlf))
)


