;Commented by 14anu-ban-01 on (18-04-2015) as this will be handled in ragged.clp
;(defrule rag0
;(declare (salience 5000))
;(id-root ?id rag)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id ragged )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id guxadZiyA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rag.clp  	rag0   "  ?id "  guxadZiyA )" crlf))
;)

;"ragged","Adj","1.guxadZiyA"
;There is a ragged && barefoot old woman.
;--"2.Pate-purAne_vaswra"   
;He has a ragged coat. 
;
(defrule rag1
(declare (salience 4900))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciWadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag1   "  ?id "  ciWadZA )" crlf))
)

;"rag","N","1.ciWadZA"
;She uses an old rag to clean the floor.
;--"2.nimna_koti_kA_samAcAra_pawra"
;I read this news in a local rag.
;--"3.Pate_purAne_vaswra"
;My shirt was worn to rags.
;
(defrule rag2
(declare (salience 4800))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CedZa-CAdZa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag2   "  ?id "  CedZa-CAdZa_kara )" crlf))
)

;"rag","V","1.CedZa-CAdZa_karanA"
;They have ragged her after the college. 
;

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;I read this news in a local rag.[rag.clp]
;मैंने ये समाचार स्थानीय निम्न कोटि के समाचार पत्र में  पढ़ा. [self]
(defrule rag3
(declare (salience 4900))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 news|article)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimna_koti_kA_samAcAra_pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag3   "  ?id "  nimna_koti_kA_samAcAra_pawra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;I don’t know how you can bear to read that awful rag![oald]
;मैं नहीं जानता कि आप वह  घोर निम्न कोटि का समाचार पत्र पढना  कैसे सहन कर सकते हैं! [self]
(defrule rag4
(declare (salience 4900))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 read)
(kriyA-object  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimna_koti_kA_samAcAra_pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag4   "  ?id "  nimna_koti_kA_samAcAra_pawra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;I saw people dressed in rags begging on the street.[oald]
;मैंने सडक पर गुदडियों/फटे_पुराने_वस्त्रों  में  विनती करते हुए  लोगों को देखा .  [self]
(defrule rag5
(declare (salience 4900))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 dress)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pate_purAne_vaswra/guxadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag5   "  ?id "  Pate_purAne_vaswra/guxadZI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;My boss is always ragging on me.[oald]
;मेरा बॉस हमेशा मुझपर असन्तुष्टता प्रकट करता रहता है .  [self]
(defrule rag6
(declare (salience 4900))
(id-root ?id rag)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-on_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asanwuRtawA_prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rag.clp 	rag6   "  ?id "  asanwuRtawA_prakata_kara )" crlf))
)
