
;@@@ Added by 14anu-ban-03 (16-04-2015)
;When was the bridge constructed? [oald]
;पुल का निर्माण  कब हुआ था?  [manual]
(defrule construct1
(declare (salience 10))   
(id-root ?id construct)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 bridge|road|building|machine)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id nirmANa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  construct.clp  construct1  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  construct.clp  construct1   "  ?id "  nirmANa_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-04-2015)
;To construct a theory. [oald]
;सिद्धान्त की रचना होना . [manual]
(defrule construct2
(declare (salience 10))   
(id-root ?id construct)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 novel|theory)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id racanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  construct.clp  construct2  "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  construct.clp  construct2  "  ?id "  racanA_ho )" crlf))
)

;------------------------------------ Default Rules --------------------------------------------

;@@@ Added by 14anu-ban-03 (16-04-2015)
;To construct a triangle.  [oald]
;त्रिकोण को बनाना  .  [manual]
(defrule construct0
(declare (salience 00))
(id-root ?id construct)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  construct.clp  construct0   "  ?id "  banA )" crlf))
)


