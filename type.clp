
(defrule type0
(declare (salience 5000))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  type.clp 	type0   "  ?id "  prakAra )" crlf))
)

;"type","N","1.prakAra"
;There are various types of roses in the garden.
;
(defrule type1
(declare (salience 4900))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAIpa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  type.clp 	type1   "  ?id "  tAIpa_kara )" crlf))
)

;"type","VT","1.tAIpa_karanA{taMkaNa_karanA}"
;He has to type the script.
;This typewriter types well.
;

;@@@ Added by 14anu-ban-07, (23-03-2015)
;Type in your password.(cambridge)(parser no.2)
;आपके सङ्केत शब्द टाईप कीजिए . (manual)
(defrule type2
(declare (salience 5000))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 tAIpa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " type.clp	type2  "  ?id "  " ?id1 "  tAIpa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07, (23-03-2015)
;Could you type up the minutes from the meeting, please?(cambridge)
;क्या आप बैठक से कृपया मिनट (पहले) टाईप कर सकते हैं? (manual)
(defrule type3
(declare (salience 5100))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 tAIpa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " type.clp	type3  "  ?id "  " ?id1 "  tAIpa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07, (23-03-2015)
;This letter will need to be typed out again.(oald)
;यह पत्र फिर से टाईप करना पड़ेगा . (manual)
(defrule type4
(declare (salience 5100))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 tAIpa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " type.clp	type4  "  ?id "  " ?id1 "  tAIpa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07, (23-03-2015)
;Blood samples were taken from patients for typing.(oald)
;मरीजों के खून के नमूने  लिए गये थे प्रकार का पता लगाने के लिए  .(manual) 
(defrule type5
(declare (salience 5000))
(id-root ?id type)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-for_saMbanXI  ?id1 ?id)
(id-root ?id1 patient)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra_kA_pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  type.clp 	type5   "  ?id "  prakAra_kA_pawA_lagA )" crlf))
)

