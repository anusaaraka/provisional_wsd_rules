
(defrule mortal0
(declare (salience 5000))
(id-root ?id mortal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id marwya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mortal.clp 	mortal0   "  ?id "  marwya )" crlf))
)

;"mortal","Adj","1.marwya/maraNaSIla"
;All the living beings on earth are mortal.
;


;$$$Modified by 14anu-ban-02(13-04-2016)
;No mortal can live without sleep.[sd_verified]
;koI manuRya nIMxa ke binA nahIM raha sakawA hE.[self]
(defrule mortal1
(declare (salience 0))	;salience decrease to 0 from 4900
(id-root ?id mortal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manuRya))	;meaning changed to 'manuRya' from 'marwya'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mortal.clp 	mortal1   "  ?id "  manuRya )" crlf))
)

;"mortal","N","1.marwya"
