;$$$Modified by 14anu-ban-01 on (12-01-2015)
;####Counter Example### After the star struck the earth there was a fine glowing.[The Wilding -By C.S. Friedman]
;@@@ Added by 14anu04 13-June-2014
;He was star struck by Shahrukh's presence.
;वह शाहरूख की उपस्थिति से पर्भावित् था.
;वह शाहरूख की उपस्थिति से प्रभावित् था. [Translation improved by 14anu-ban-01 on (12-01-2015)]
(defrule star_tmp
(declare (salience 5000))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 struck)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)	;added by 14anu-ban-01 on (12-01-2015)
;(test (=(+ ?id 1) ?id1))		;commented by 14anu-ban-01 on (12-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praBAviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  star.clp     star_tmp   "  ?id "  " ?id1 "  praBAviwa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;One born under a lucky star is believed to be lucky.[old sentence] 
;kisI acCe nakRawra meM janma lene vAlA vyakwi BagyaSAlI mAnA jAwA hE.[self]
;किसी अच्छे नक्षत्र में जन्म लेने वाला व्यक्ति भग्यशाली माना जाता है.[self]
(defrule star2
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 lucky)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakRawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star2   "  ?id "  nakRawra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;Lata Mangeshkar is a well known star.[old sentence]
;lawA maMgeSakara eka jAnI mAnI haswI hEM.[self]
;लता मंगेशकर एक जानी मानी हस्ती हैं.[self]
(defrule star3
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-word ?id1 known)
(viSeRaNa-viSeRaka  ?id1 ?id2)
(id-root ?id2 well)
(id-cat_coarse ?id noun)
;The below conditions will enhance the rule but are commented because 'Lata Mangeshkar' is not in the 'human.gdbm' database.
;(subject-subject_samAnAXikaraNa  ?sub ?id)
;(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 jAnI_mAnI_haswI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " star.clp	 star3 "  ?id "  "?id1 "  "?id2 " jAnI_mAnI_haswI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;Taj is a 5 star hotel.[old sentence]
;wAja eka pAzca siwArA hotala hE.[self]
;ताज एक पाँच सितारा होटल है.[self]
(defrule star4
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) 2|3|5|7|two|three|five|seven);
(id-cat_coarse =(- ?id 1) number);cardinal numbers can also be added if we have a category for them. Eg:cd
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id siwArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star4   "  ?id "  siwArA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;She was the star in this serial.[old sentence]
;isa XArAvAhika meM aBinewrI vaha WI.[self] 
;इस धारावाहिक में अभिनेत्री वह थी. [self]
(defrule star5
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 serial|movie|album|film)
(kriyA-subject ?be ?sub)
(subject-subject_samAnAXikaraNa  ?sub ?id)
(id-root ?be be)
(id-root ?sub she);this condition can be generalized by adding a database for female names or popular female stars.
(id-cat_coarse ?id noun)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinewrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star5   "  ?id "  aBinewrI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;He was the star in this serial.[old sentence]
;isa XArAvAhika meM aBinewA vaha WA.[self] 
;इस धारावाहिक में अभिनेता वह था. [self]
(defrule star6
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 serial|movie|album|film)
(kriyA-subject ?be ?sub)
(subject-subject_samAnAXikaraNa  ?sub ?id)
(id-root ?be be)
(id-root ?sub he);this condition can be generalized by adding a database for male names or popular male stars.
(id-cat_coarse ?id noun)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinewA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star6   "  ?id "  aBinewA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;Mohan is Rama's star pupil.[old sentence]
;mohana ramA kA uwkqRta vixyArWI hE.[old sentence]
(defrule star7
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 pupil|student)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkqRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star7   "  ?id " uwkqRta )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;She starred in several films.[old sentence]
;usane kaI PilmoM meM muKya BUmikA axA kI.[old sentence]
(defrule star8
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 serial|movie|album|film)
(id-cat_coarse ?id verb)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya_BUmikA_axA_kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star8  "  ?id "  muKya_BUmikA_axA_kI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-08-2014)
;The starred lessons are to be read carefully.[old sentence]
;wArAMkiwa pAToM ko XyAna se paDA jAnA hE.[self]
;तारांकित पाठों को ध्यान से पढा जाना है.[self]
(defrule star9
(declare (salience 4900))
(id-root ?id starred)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 lesson);list can be modified when required.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wArAMkiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star9  "  ?id "  wArAMkiwa)" crlf))
)

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;the million - strong Indian population in the US proved its undying love for Hindi cinema , lining up at theatres to see the 
;latest offerings from Bollywood and turning out in droves for the live shows of Hindi film stars and musicians 
;सन् - ऊण्श्छ्ष् - 2001 में अमेरिका में रहने वाले 10 लख भारतीयों ने ताजा हिंदी फिल्में और हिंदी फिल्मी सितारों , संगीतकारों के शो देखने के लिए भारी संया में जुटकर हिंदी सिनेमा के प्रति अपना 
;अगाध प्रेम प्रदर्शित किया 
;@@@ Added by 14anu11
(defrule star10
(declare (salience 5000))
(id-root ?id star)
;(id-root =(- ?id 1) film)	;commented by 14anu-ban-01
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)	;added by 14anu-ban-01
(id-root ?id1 film)				;added by 14anu-ban-01
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  siwArA));changed "siwAroM" to "siwArA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star10   "  ?id "   siwArA)" crlf));changed "siwAroM" to "siwArA" by 14anu-ban-01 on (12-01-2015)
)

;@@@Added by 14anu-ban-11 on (09-03-2015)
;She's the star of the show! (oald)
;वह नाटक की मुख्य कलाकार है! (self)
(defrule star11
(declare (salience 4900))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 show)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya_kalAkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star11  "  ?id "  muKya_kalAkAra)" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-03-2015)
;Rock stars have to get used to being plagued by autograph hunters. (oald)
;रॉकस्टार को हस्ताक्षर शिकारियों के द्वारा परेशान होने की आदत हो गई है .        [manual] 
(defrule star12
(declare (salience 400))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 rock)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  roYkastAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " star.clp 	star12   "  ?id "  " ?id1 "  roYkastAra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (30-03-2015)
;He wrote,directed,and starred in the play.(Merrian Webster) 
;उसने लिखा,निर्देशित किया ,और नाटक में अभिनय किया .(self)
(defrule star13
(declare (salience 10))
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star13  "  ?id "  aBinaya_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;"star","V","1.wArAMkiwa_karanA"
;The starred lessons are to be read carefully.
(defrule star1
(declare (salience 0));reduced to 0 from 4900 by 14anu-ban-01 on (30-08-2014)
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wArAMkiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star1   "  ?id "  wArAMkiwa_kara )" crlf))
)

;"star","N","1.wArA"
;There are infinite stars in the universe.
(defrule star0
(declare (salience 0));reduced to 0 from 4900 by 14anu-ban-01 on (30-08-2014)
(id-root ?id star)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  star.clp 	star0   "  ?id "  wArA )" crlf))
)

;"star","N","1.wArA"
;There are infinite stars in the universe.
;--"2.nakRawra"
;One born under a lucky star is believed to be lucky.
;--"3.prasixXa_vyakwi"
;Lata Mangeshkar is a well known star.
;--"4.wArA_jEsA"
;Stars are used to decorate trees at the time of festivals.
;--"5.siwArA/wArikA/aBinewrI"
;She was the star in this serial.
;Taj is a 5 star hotel.
;


;
;LEVEL 
;
;
;Headword : star
;
;Examples --
;
;"star","N","1.wArA" 
;The stars in the sky guide travellers at night.
;AkASa ke wAre rAwa ko yAwriyoM kA paWapraxarSana karawe hEM.
;
;--"2.haswI{Kela_iwyAxi meM}"-siwArA
;Lata Mangeshkar is a well known star.
;lawA maMgeSakara eka prasixXa haswI hEM 
;
;--"3.siwArA"
;Stars are used to decorate the christmas trees.
;siwAroM kA prayoga krisamasa trI ko sajAne ke liye karawe hEM
;
;Taj is a 5 star hotel.
;wAja pAzca siwArA hotala hE
;
;--"4.uwkqRta"-siwArA
;Mohan is Rama's star pupil.
;mohana ramA kA uwkqRta vixyArWI hE.
;
;--"5. nakRawra"<--wArA<---siwArA
;She was born under a lucky star.
;vaha eka acCe nakRawra meM pExA huI.
;
;"star","V","1.wArAMkiwa karanA"-siwAre dAlanA
;Star the correct words.
;sahI SabxoM ko wArAMkiwa karo.
;
;--"2.muKya BUmikA axA karanA"-stAra kI BUmikA axA kI-siwAre kI BUmikA
;She starred in several films.
;usane kaI PilmoM meM muKya BUmikA axA kI
;
;   nota:--uparyukwa 'star'Sabxa ke samaswa vAkyoM kA yaxi avalokana kareM  
;        wo yaha niRkarRa nikAla sakawe hEM ki saBI arWoM kA mUla arWa
;        'siwArA' Sabxa se nikAlA jA sakawA hE.
;           awaH isakA sUwra nimna prakAra xe sakawe hEM
;
;         sUwra : siwArA
;
