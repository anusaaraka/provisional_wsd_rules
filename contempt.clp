
;@@@ Added by 14anu-ban-03 (20-04-2015)
;The firefighters showed a contempt for their own safety. [oald]
;फाइरर्फाइटर ने अपनी सुरक्षा के लिए  उनको अवहेलना दिखाई . [manual]
(defrule contempt1
(declare (salience 10))
(id-root ?id contempt)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avahelanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contempt.clp    contempt1   "  ?id "  avahelanA )" crlf))
)


;@@@ Added by 14anu-ban-03 (20-04-2015)
;He could be jailed for two years for contempt. [oald]
;उन्हे अदालत की अवहेलना करने के कारण दो वर्षों की सजा हो सकती है . [manual]
(defrule contempt2
(declare (salience 10))
(id-root ?id contempt)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-for_saMbanXI ?id1 ?id) (kriyA-in_saMbanXI ?id1 ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axAlawa_kI_avahelanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contempt.clp   contempt2   "  ?id " axAlawa_kI_avahelanA )" crlf))
)

;-------------------------------------------- Default Rules ----------------------------------

;@@@ Added by 14anu-ban-03 (20-04-2015)
;She looked at him with contempt. [oald]
;उसने तिरस्कार के साथ उसकी ओर देखा . [manual]
(defrule contempt0
(declare (salience 00))
(id-root ?id contempt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wiraskAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contempt.clp    contempt0   "  ?id "  wiraskAra )" crlf))
)


