;$$$ Modified byBhagyashri Kulkarni (2-11-2016)
;Treatment with them at office, home, among friends should be the same as earlier. (health)
;मित्रों में, दफ्तर में, घर में, पहले की तरह  समान व्यवहार होना चाहिए .
;His treatment towards the guests was not cordial.
;अतिथियों के प्रति उसका व्यवहार सौहार्दपूर्ण नहीं था . 
;$$$Modified by 14anu-ban-07,(26-03-2015)
;Peter gets special treatment because he knows the boss. (cambridge)
;पीटर विशेष व्यवहार प्राप्त करता है क्योंकि वह बॉस को जानता है . (manual)
(defrule treatment1
(declare (salience 5100))  ; salience increased from 4900 by 14anu-ban-07,(26-03-2015)
(id-root ?id treatment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa ?id ?id1)(subject-subject_samAnAXikaraNa ?id ?id1))   ;added by 14anu-ban-07,(26-03-2015) ;Added 'subject-subject_samAnAXikaraNa ' by Bhagyashri
(id-root ?id1 special|same|cordial)     ;added by 14anu-ban-07,(26-03-2015) ;Added 'same|cordial' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treatment.clp 	treatment1   "  ?id "  vyavahAra )" crlf))
)

;@@@Added  by 14anu-ban-07,(26-03-2015)
; The barbarous treatment of these prisoners of war.(oald)
;क्रूर सुलूक युद्ध के इन बन्दियों के साथ  . (manual)
(defrule treatment2
(declare (salience 5200))  
(id-root ?id treatment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 barbarous|brutal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sulUka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treatment.clp 	treatment2   "  ?id "  sulUka )" crlf))
)


;@@@Added  by 14anu-ban-07,(26-03-2015)
; The same subject matter gets a very different treatment by Chris Wilson in his latest novel.(cambridge)
;समान विषय वस्तु को मिलता है   एक अत्यन्त अलग वर्णन क्रिस विल्सन के द्वारा उसके सबसे अधिक नवीनतम उपन्यास में.  (manual)
(defrule treatment3
(declare (salience 5300))  
(id-root ?id treatment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 matter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varNana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treatment.clp 	treatment3   "  ?id "  varNana )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule treatment0
(declare (salience 5000))
(id-root ?id treatment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treatment.clp 	treatment0   "  ?id "  ilAja )" crlf))
)


;default_sense && category=noun	barwAva	0
;"treatment","N","1.barwAva"
;His treatment towards the guests was not cordeal.
;--"2.upacAra"
;Trace is undergoing medical treatment.
;--"3.nirUpaNa"
;Kalidasa's treatment of love in Meghasandesha is very vivid.
;
;
