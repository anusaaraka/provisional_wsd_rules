;@@@ Added by 14anu-ban-06 (03-04-2015)
;It was a place where his speech impediment didn't matter. (COCA)
;यह एक ऐसा स्थान था जहाँ भाषण में उसकी हकलाहट महत्व नहीं रखती.  (manual)
(defrule impediment1
(declare (salience 2000))
(id-root ?id impediment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 speech)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hakalAhata))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impediment.clp 	impediment1   "  ?id "  hakalAhata )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  impediment.clp      impediment1   "  ?id1 " meM )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

;@@@ Added by 14anu-ban-06 (03-04-2015)
;The level of inflation is a serious impediment to economic recovery.(OALD)
;मुद्रा स्फीति का स्तर अर्थशास्त्रीय  बहाली के लिए एक गम्भीर बाधा है .  (manual)
(defrule impediment0
(declare (salience 0))
(id-root ?id impediment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impediment.clp 	impediment0   "  ?id "  bAXA )" crlf))
)
