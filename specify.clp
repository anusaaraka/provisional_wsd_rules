;@@@ Added by 14anu-ban-01 on 26-08-14.
;9846:If the court decides in your favour it will make an order requiring the offender to abate the noise nuisance and specify the measures they will have to take to achieve this .[Karan Singla]
;यदि कोर्ट आप के पक्ष में निर्णय दे तो वो अभियोगी को शोरगुल कम करने का आदेश देगा और साथ ही वह उपाय भी बताएगा जो उसे शोर कम करने के लिए करने होंगे .[improvised manually- changed यह to उपाय:could not get a better word for measure here]
;13645:This will , among other things , specify the intervals at which payments will be made .[Karan Singla]
;यह और बातों के अलावा , यह बताएगा कि भुगतान कब - कब देने होंगे ।[Karan Singla]
;72377:The old texts also specify that members of a court should not connive with the king if he acts unjustly .[Karan Singla]
;प्राचीन ग्रंथ यह भी बताते हैं कि यदि राजा कोई अनुचित कार्य करे तो न्यायालय के सदस्यों को उसका साथ नहीं देना चाहिए .[improvised: changed 'ग्रंथों का यह भी कथन है' to ' यह भी बताते हैं']
(defrule specify0
(declare (salience 0))
(id-root ?id specify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  specify.clp  	specify0  "  ?id " bawA )" crlf))
)


;@@@ Added by 14anu-ban-01 on 26-08-14.
;Note that when mole is used, the elementary entities must be specified.[NCERT ccorpus]
;ध्यान दीजिए, मोल का उपयोग करते समय मूल सत्ताओं का विशेष रूप से उल्लेख किया जाना चाहिए.[NCERT ccorpus]
;XyAna xIjie, mola kA upayoga karawe samaya mUla sawwAoM kA viSeRa rUpa se ulleKa kiyA jAnA cAhie.[NCERT ccorpus]
(defrule specify1
(declare (salience 1000))
(id-root ?id specify)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 entity)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa_rUpa_se_ulleKa_kara))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  specify.clp  	specify1 "  ?id " viSeRa_rUpa_se_ulleKa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  specify.clp    specify1   "  ?id " kA )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on 26-08-14.
;36631:In his notice , the member is required to specify the point s that he wishes to raise .[Karan Singla]
;अपनी सूचना में सदस़्य के लिए उस मुद्दे या उन मुद्दों का उल़्लेख करना अपेक्षित है जो वह उठाना चाहता हो .[Karan Singla]
(defrule specify2
(declare (salience 1000))
(id-root ?id specify)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 point)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  specify.clp  	specify2 "  ?id " ulleKa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  specify.clp    specify2   "  ?id " kA )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on 26-08-14.
;24534:The law made under the provision may specify the jurisdiction and powers of the tribunals .[Karan Singla]
;इस उपबंध के अधीन बनाई गई विधि अधिकरणों की अधिकारिता तथा शक्तियों का  विशिष्ट निर्देश कर सकती है .[improvised manually:changed 'विनिर्देश कर' to 'विशिष्ट निर्देश']
(defrule specify3
(declare (salience 1000))
(id-root ?id specify)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 power)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta_nirxeSa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  specify.clp  	specify3 "  ?id " viSiRta_nirxeSa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  specify.clp    specify3   "  ?id " kA )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (17-10-2014).
;Since the magnitude of a null vector is zero, its direction can not be specified.[NCERT corpus]
;क्योंकि शून्य सदिश का परिमाण शून्य होता है, इसलिए इसकी दिशा का निर्धारण नहीं किया जा सकता है .[NCERT corpus]
(defrule specify4
(declare (salience 1000))
(id-root ?id specify)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 direction)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirXAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  specify.clp  	specify4 "  ?id " nirXAriwa_kara )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (11-02-2015)
;These entities may be atoms, molecules, ions, electrons, other particles or specified groups of such particles.[NCERT corpus]]
;ये मूल सत्ताएँ परमाणु, अणु, आयन, इलेक्ट्रॉन, अन्य कोई कण अथवा इसी प्रकार के कणों का विशिष्ट समूह हो सकता है. [self]
(defrule specify5
(declare (salience 0))
(id-word ?id specified)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  specify.clp 	specify5 "  ?id "  viSiRta)" crlf))
)

