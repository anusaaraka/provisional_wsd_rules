;@@@ Added by 14anu-ban-06 (18-04-2015)
;A hunger for knowledge. (OALD)
;ज्ञान की तीव्र इच्छा . (manual)
(defrule hunger1
(declare (salience 2000))
(id-root ?id hunger)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 truth|adventure|knowledge|success)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIvra_icCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hunger.clp 	hunger1   "  ?id "  wIvra_icCA )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-04-2015)
;He hungered for knowledge. (OALD)[parser no.-15]
;वह ज्ञान के लिए लालसा रखता था. (manual)
(defrule hunger2
(declare (salience 2100))
(id-root ?id hunger)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAlasA_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hunger.clp 	hunger2   "  ?id "  lAlasA_raKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-04-2015)
;I've never hungered after power. (cambridge)[parser problem]
;मैं शक्ति के लिए कभी लालायित  नहीं रहा हूँ . (manual)
(defrule hunger3
(declare (salience 2200))
(id-root ?id hunger)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAlAyiwa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hunger.clp 	hunger3   "  ?id "  lAlAyiwa_raha )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (18-04-2015)
;Around fifty people die of hunger every day in the camp.(OALD)
;पचास के आस-पास लोग शिविर में भूख से प्रत्येक दिन मर जाते हैं . (manual)
(defrule hunger0
(declare (salience 0))
(id-root ?id hunger)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hunger.clp 	hunger0   "  ?id "  BUKa )" crlf))
)


