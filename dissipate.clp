;@@@ Added by 14anu-ban-04 (07-02-2015)
;She was determined to achieve results and not to dissipate her energies.                  [oald]
;वह परिणाम प्राप्त करने के लिए और अपनी  ऊर्जा नष्ट न करने के लिए निर्धारित की गयी थी .                               [self]
;He dissipated all his income in drinking.                                 [same clp file]
;उसने अपनी सारी आय  शराब पीने में नष्ट  की .                                            [self]
(defrule dissipate2
(declare (salience 4910))
(id-root ?id dissipate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 energy|income)                        ;added income by 14anu-ban-04 on (17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissipate.clp 	dissipate2   "  ?id "  naRta_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (07-02-2015)
;Eventually, his anger dissipated.                                [oald]
;अन्त में, उसका क्रोध दूर हो गया .                                         [self]
(defrule dissipate3
(declare (salience 4920))
(id-root ?id dissipate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 anger) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissipate.clp 	dissipate3  "  ?id "  xUra_ho_jA )" crlf))
)

;COMMENTED BY 14anu-ban-04 on (17-02-2015) because it is unnecessary and correct meaning is coming from 'dissipate2'.
;@@@ Added by 14anu-ban-04 (07-02-2015)
;He dissipated all his income in drinking.                         [same clp file]
;उसने अपनी सारी आय  शराब पीने में  अपव्यय की .                                   [self]
;(defrule dissipate4
;(declare (salience 4910))
;(id-root ?id dissipate)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-in_saMbanXI  ?id ?id1)
;(id-root ?id1 drink) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id apavyaya_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissipate.clp 	dissipate4  "  ?id "  apavyaya_kara )" crlf))
;)

;@@@ Added by 14anu-ban-04 (17-02-2015)
;His anger dissipated as the situation became clear.                   [cald]
;जैसे ही  परिस्थिति   स्पष्ट हो गयी  उसका क्रोध दूर हुआ .                                 [self]
(defrule dissipate5
(declare (salience 4920))
(id-root ?id dissipate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 anger) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissipate.clp 	dissipate5  "  ?id "  xUra_ho )" crlf))
)

;-------------------- Default Rules ---------------

;"dissipated","Adj","1.xurvyasanI"
;He leads a dissipated life .
(defrule dissipate0
(declare (salience 5000))
(id-root ?id dissipate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id dissipated )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xurvyasanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  dissipate.clp  	dissipate0   "  ?id "  xurvyasanI )" crlf))
)

;"dissipate","VT","1.xUra_karanA"
;Her son's letter dissipated all her fears.
;
(defrule dissipate1
(declare (salience 4900))
(id-root ?id dissipate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissipate.clp 	dissipate1   "  ?id "  xUra_kara )" crlf))
)

;"dissipate","VT","1.xUra_karanA"
;Her son's letter dissipated all her fears.
;--"2.apavyaya_karanA"
;He dissipated all his income in drinking.
;
;
