;@@@ Modified by Sonam Gupta MTech IT Banasthali 20-1-2014 ; added 'viSeRya-of_saMbanXI' relation
;In recommending his appointment to the Viceroy, H.H. Risley, Home Secretary to the Government of India, wrote : 
;I have no hesitation in saying that the Hon'ble Mr. Justice Mookerjee is marked out by his scientific attainments, 
;his long connection with the University and the work he has done for it and by his official position as conspicuously  
;qualified for the post of Vice-Chancellor. [gyannidhi]
;वाइसराय को उनकी नियुक्ति के लिए सिफारिश करते हुए भारत सरकार के गृह सचिव एच॰ एच॰ रिस्ले ने लिखा, मुझे यह कहने में ज़रा भी हिचकिचाहट नहीं है 
;कि माननीय न्यायमूर्ति श्री मुकर्जी अपनी वैज्ञानिक उपलब्धियों, विश्वविद्यालय के साथ पुराने सम्बन्धों और उसके सिल उनके द्वारा किये गये कार्यों और अपने 
;राजकीय पद के कारण कुलपति के पद के लिए स्पष्ट रूप से योग्य सिद्ध होते हैं।
;Added by Meena(27.5.11)
;Applicants for posts are called for interview. 
(defrule post00
(declare (salience 5000))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-for_saMbanXI  ?id1 ?id)(viSeRya-of_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp      post00   "  ?id "  paxa )" crlf))
)


;Salience reduced by Meena(27.5.11)
(defrule post0
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaMbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post0   "  ?id "  KaMbA )" crlf))
)

;"post","N","1.KaMbA"
;Tie the tennis net to the post.
;--"2.sWAna"
;The soldiers are at their posts.
;--"3.paxa"
;She is trying for the post of typist.
;--"4.dAka"
;There was post for you this morning.
;
(defrule post1
(declare (salience 4900))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 letter)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) ;this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAka_se_Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post1   "  ?id "  dAka_se_Beja )" crlf))
)

(defrule post2
(declare (salience 4800))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post2   "  ?id "  cipakA )" crlf))
)

;"post","V","1.cipakAnA"
;The results of the exam were posted on the notice board.
;--"2.niyukwa_karanA"
;After several years in Hyderabad he was posted to Delhi.
;--"3.dAka_meM_dAlanA"
;Could you post this leter for me?
;--"4.dAka_se_BejanA"
;They will post me the cheque as soon as they recieve my bill.
;

;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;Your letter is in the post.  [Cambridge]
;आपका पत्र डाक में है . 
(defrule post3
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-in_saMbanXI  ? ?id)(viSeRya-of_saMbanXI  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post3   "  ?id "  dAka )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;Did I get any post today? [Cambridge]
;क्या मैंने आज कोई डाक प्राप्त की? 
(defrule post4
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-subject  ?id1 ?id)(kriyA-object  ?id1 ?id))
(id-root ?id1 arrive|come|delay|get|receive|open)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post4   "  ?id "  dAka )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;A part-time post.      [Cambridge]
;एक अंशकालिक पद . 
(defrule post5
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post5   "  ?id "  paxa )" crlf))
)


;$$$ Modified by 14anu24
;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;I posted a query about arthritis treatment.     [Cambridge]
;मैंने गठिया रोग इलाज के बारे में प्रश्न भेजा . 
(defrule post6
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 query|question|answer|complaint|information|decision|reason);added words decision and reason
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BejA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post6   "  ?id "  BejA )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;Keep me posted on anything that happens while I'm away.    [Cambridge]
;मेरे दूर होने पर जो भी होता है  उसका ब्यौरा दे . 
(defrule post7
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id1 ?id)
(id-root ?id1 keep)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 byOrA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " post.clp  post7  "  ?id "  " ?id1 "  byOrA_xe )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;To take up a post.   [Cambridge]
;पद लेना .
(defrule post8
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id1 ?id)
(id-root ?id1 take)
(kriyA-upasarga  ?id1 ?id2)
(id-root ?id2 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 paxa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " post.clp  post8  "  ?id "  " ?id1 "  paxa_le )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;To resign a post.  [Cambridge]
;पद त्याग देना . 
(defrule post9
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 resign|join|fill|hold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post9   "  ?id "  paxa )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;The guards were ordered not to leave their posts.  [OALD]
;पहरेदारों को उनकी चौकियों को नहीं छोडे जाने का आदेश था . 
(defrule post10
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 leave|join)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 guard|soldier|police|army)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOkI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post10   "  ?id "  cOkI )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 16-1-2014
;I love reading her posts because I learn so much.  [OALD]
;मैं उसका लेख पढना पसन्द करता हूँ क्योँकि मैं इससे सीखता हूँ . 
(defrule post11
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 read|write|study)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post11   "  ?id "  leKa )" crlf))
)

;@@@ Added BY 14anu26  [21-06-14]
;They went Delhi for the post.
;वे पद के लिए दिल्ली गये . 
(defrule post12
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post12   "  ?id "  paxa )" crlf))
)

;$$$ Modifeid by 14anu-ban-09 on (27-01-2015)
;@@@ Added by 14anu20 on 27.06.2014.
;Her fitness for post is needed.
;पद के लिए उसके स्वस्थ होने की आवश्यकता  है .
(defrule post13
(declare (salience 5200))		;Salience increased '4900' to '5200' by 14anu-ban-09 on (27-01-2015)
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-for_saMbanXI  ?id1 ?id)		;Commented by 14anu-ban-09 on (27-01-2015)
;(kriyA-object  ?id1 ?id2)		;Commented by 14anu-ban-09 on (27-01-2015)
(viSeRya-for_saMbanXI  ?id1 ?id)	;Added by 14anu-ban-09 on (27-01-2015)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id2)	;Modified '?id2' to '?id1' and '?id3' to '?id2'by 14anu-ban-09 on (27-01-2015)
(id-root ?id1 fitness|capable|fit|intellect|reference)	;Modified '?id2' to '?id1' by 14anu-ban-09 on (27-01-2015)
;(id-root ?id3 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))	;Commented by 14anu-ban-09 on (27-01-2015)	
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))	;Added by 14anu-ban-09 on (27-01-2015)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post13   "  ?id "  paxa )" crlf))
)

;@@@Added by 14anu07 on 25/06/2014
(defrule post14
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-object  ?id1 ?id)
(id-root =(+ ?id 1) bite|war)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post14   "  ?id "  bAxa)" crlf))
)


;"post","V","1.cipakAnA"
;The results of the exam were posted on the notice board.
;--"2.niyukwa_karanA"
;After several years in Hyderabad he was posted to Delhi.
;--"3.dAka_meM_dAlanA"
;Could you post this leter for me?
;--"4.dAka_se_BejanA"
;They will post me the cheque as soon as they recieve my bill.
;

;@@@Added by 14anu-ban-09 on (04-02-2015)
;NOTE- When 'Delhi' is added to place.gdbm. Then, comment (id-cat_coarse ?id1 PropN) and uncomment (id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))) .
;After several years in Hyderabad he was posted to Delhi.	[Hinkhoj]
;हैदराबाद में कई सालों के बाद उसे दिल्ली में नियुक्त किया गया.			[Self]

(defrule post15
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str))))
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post15   "  ?id "  niyukwa_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-02-2015)
;Most of our employees get posted abroad at some stage. 	[oald]
;हमारे बहुत से कर्मचारी किसी चरण पर विदेश में नियुक्त कर दिये गये थे . 		[Self]

(defrule post16
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 employee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara_xiye_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post16   "  ?id "  niyukwa_kara_xiye_jA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-02-2015)
;NOTE- When 'guard' is added to human.txt. Then, uncomment (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) and comment (id-root ?id1 guard)
;Guards have been posted along the border. 	[oald]
;रक्षक सीमा के पास नियुक्त किए गये हैं .			[Self] 

(defrule post17
(declare (salience 5500))
(id-root ?id post)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id1 guard)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  post.clp 	post17   "  ?id "  niyukwa_kara) " crlf))
)

