;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Pramila(Banasthali University) on 27-01-2014
;There are several witnesses who will testify for the defence.      [problem sentence]
;कई गवाह हैं जो प्रतिपक्ष के लिये गवाही देंगी .
(defrule defence0
(declare (salience 5000))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawipakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence0   "  ?id "  prawipakRa )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-04-2015)
;I play in defence.        [cald]
;मैं बचाव में खेलता हूँ .            [self]
;He plays on defense        [oald]
;वह बचाव में खेलता  है .            [self]
(defrule defence3
(declare (salience 4010))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-in_saMbanXI ?id1 ?id)(kriyA-on_saMbanXI ?id1 ?id))
(id-root ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence3   "  ?id "  bacAva )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-04-2015)
;A witness for the defence.                      [oald]
;प्रतिपक्ष के लिए  गवाह.                                 [self]
(defrule defence4
(declare (salience 4020))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI   ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawipakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence4   "  ?id "  prawipakRa )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-04-2015)
;Her defence was that she was somewhere completely different at the time of the crime.           [oald]
;उसका उत्त्र था कि वह अपराध के समय पर बखूबी  कहीं दूर थी .                                   [self]
(defrule defence5
(declare (salience 4020))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
(kriyA-subject ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence5  "  ?id "  uwwra)" crlf))
)

;@@@ Added by 14anu-ban-04 (04-04-2015)
;The judge remarked that ignorance was not a valid defence.                     [cald]
;न्यायाधीश ने टिप्पणी की कि नादानी  एक वैध दलील नहीं थी .                                         [self]
(defrule defence6
(declare (salience 4030))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa   ?id ?id1)
(id-root ?id1 effective|valid)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xalIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence6  "  ?id "  xalIla)" crlf))
)

;@@@ Added by 14anu-ban-04 (04-04-2015)
;All I can say, in defence of my actions, is that I had little choice.             [cald]
;मैं मेरी हरकतों की सफ़ाई  में बस यही कह सकता हूँ,कि मेरा नहीं के बराबर विकल्प था .                         [self]
(defrule defence7
(declare (salience 4020))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence7  "  ?id "  saPAI)" crlf))
)

;@@@ Added by 14anu-ban-04 (04-04-2015)
;What defence did you use in that last game?                 [cald]
;उस आखिरी खेल में आपने  किस/कौनसी चाल का  उपयोग किया ?                  [self]
(defrule defence8
(declare (salience 4020))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id)
(kriyA-in_saMbanXI ?kri ?id1)
(id-root ?id1 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence8  "  ?id "  cAla)" crlf))
)

;@@@ Added by 14anu-ban-04 (04-04-2015)
;The town walls were built as a defence against enemy attacks.       [oald]
;शत्रु के  हमलों  से सुरक्षा के लिए नगर की दीवारों को  बनाया गया था .                       [self]
(defrule defence9
(declare (salience 4020))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-against_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence9  "  ?id "  surakRA)" crlf))
)

;---------------------deafult rules-------------------------------

;@@@ Added by Pramila(Banasthali University) on 27-01-2014
;The war has ended but government spending on defence is still increasing.        ;cald
;युद्ध समाप्त हो गया है लेकिन सुरक्षा पर सरकारी खर्च अब भी बढ़ रहा है.
(defrule defence1
(declare (salience 4000))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence1   "  ?id "  rakRA )" crlf))
)

;@@@ Added by Pramila(Banasthali University) on 27-01-2014
(defrule defence2
(declare (salience 0))
(id-root ?id defence)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defence.clp 	defence2   "  ?id "  rakRA )" crlf))
)
