
(defrule reshuffle0
(declare (salience 5000))
(id-root ?id reshuffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pere_baxala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reshuffle.clp 	reshuffle0   "  ?id "  Pere_baxala_kara )" crlf))
)

;"reshuffle","V","1.Pere_baxala_karanA"
(defrule reshuffle1
(declare (salience 4900))
(id-root ?id reshuffle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reshuffle.clp 	reshuffle1   "  ?id "  PeMta )" crlf))
)

;"reshuffle","VT","1.PeMtanA"
;Reshuffle the cards please.  
;--"2.Pera-baxala_karanA"
;The cabinet at the centre has been reshuffled.  

;@@@ Added by Anita--9.7.2014
;Rajaram Pandey found out that he had not being included in the cabinet reshuffle that was to be carried out on Friday.
;राजाराम पाण्डेय को पता चला कि शुक्रवार को कैबिनेट में हुए  उलट-फेर में उन्हें शामिल नहीं किया गया था । 
(defrule reshuffle2
(declare (salience 5100))
(id-root ?id reshuffle)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-jo_samAnAXikaraNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pera_baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reshuffle.clp 	reshuffle2   "  ?id "  Pera_baxala )" crlf))
)