
(defrule insure0
(declare (salience 5000))
(id-root ?id insure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bImA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insure.clp 	insure0   "  ?id "  bImA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  insure.clp    insure0   "  ?id " kA )" crlf) 
)
)

(defrule insure1
(declare (salience 4900))
(id-root ?id insure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bImA_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insure.clp 	insure1   "  ?id "  bImA_karA )" crlf))
)

;"insure","V","1.bImA_karanA[karAnA]"
;We should insure against accidents.

;@@@ Added by 14anu26    [28-06-14]
;His breeding kennels have insured that this breed will be perpetuated.
;उसके बच्चे पैदा करने वाले शिकारी कुत्तों के  झुण्ड स्पष्ट कर चुके हैं कि यह नस्ल स्थायी बन जाएगी . 
(defrule insure2
(declare (salience 5000))
(id-root ?id insure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id 	?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insure.clp 	insure2   "  ?id "  spaRta_kara  )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-02-2015)
;We thought we'd insure against rain by putting a tent up where people could take shelter.(cambridge) ;[parse problem]
;हमने सोचा कि हम तम्बू लगाकर वर्षा से बचाव करेंगे जहाँ लोग आश्रय ले सके .  (manual)
(defrule insure3
(declare (salience 5100))
(id-root ?id insure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacAva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insure.clp 	insure3   "  ?id "  bacAva_kara  )" crlf))
)
