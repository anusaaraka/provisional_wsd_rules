;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;Plaster the walls with advertising.
;विज्ञापनों को दीवारों पर लगाया.
(defrule plaster2
(declare (salience 5000))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(viSeRya-viSeRaNa  ?id ?id1) 		;commented by 14anu-ban-09 on (01-01-2015)
;(viSeRya-with_saMbanXI  ?id1 ?id2) 	;commented by 14anu-ban-09 on (01-01-2015)
(kriyA-object  ?id ?id1) 		;Added by 14anu-ban-09 on (01-01-2015)
(id-root ?id1 wall|building|fence|pillar)
;(id-root ?id2 advertisement|label|sticker) ;commented by 14anu-ban-09 on (01-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster2   "  ?id " lagA )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;Plastered over our differences.
;अपने अंतरों को ढका.
(defrule plaster3
(declare (salience 4900))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI  ?id ?id1)
(id-root ?id1 difference|friendship|relation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster3   "  ?id " Daka )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;His hair was plastered to his forehead.
;उसके बाल उसके माथे पर चिपके थे.
(defrule plaster4
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
;(id-root ?id1 head|forehead|face)   if Rule misfires for verb then uncomment this.....
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster4   "  ?id " cipaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-03-2015)
;She plastered herself in suntan lotion.		[oald] 
;उसके बाल उसके माथे पर चिपके थे.				[manual]

(defrule plaster5
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 lotion)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster5   "  ?id " lagA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-03-2015)
;NOTE-Parser problem. Run on parser no. 2.
;We were plastered from head to foot with mud. 		[oald]
;हम सिर से पाँवों तक कीचड़ से लीप गये थे . 			[manual]

(defrule plaster6
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 mud)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lIpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster6  "  ?id " lIpa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-03-2015)
;Her bedroom wall was plastered with photos of him.  		[oald]
;उसकी कमरे की दीवार पर उसकी तस्वीरों चिपकी हुई थी . 			[manual]

(defrule plaster7
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 photo)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster7 "  ?id " cipaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-03-2015)
;She had photos of him plastered all over her bedroom wall.		[oald]
;उसके तस्वीरें उसके पूरे कमरे की दीवार पर चिपकी हुई थीं . 			[manual]

(defrule plaster8
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI  ?id ?id1)
(id-root ?id1 wall)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster8 "  ?id " cipaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-03-2015)
;The next day their picture was plastered all over the newspapers. 		[oald]
;अगला दिन उनके चित्र पूरे समाचारपत्रों पर चिपके हुए थे .		 			[manual]

(defrule plaster9
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 picture)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster9 "  ?id " cipaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;His walls are plastered with posters of rockstars. [oald]
;उसकी दीवारों पर rockstars के पोस्टर चिपके हुए हैं . 	         [manual]

(defrule plaster10
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 wall|building|fence|pillar)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipake_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster10 "  ?id " cipake_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)		
;The radio station plastered the buses and trains with its advertisement.		[oald]	
;रेडियो स्टेशन ने उसके विज्ञापन बसो और रेलगाडियो पर चिपकाए . 		[manual]

(defrule plaster11
(declare (salience 4800))	
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 advertisement)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster11 "  ?id " cipakA )" crlf))
)



;------------------------ Default Rules ----------------------
;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
(defrule plaster0
(declare (salience 4500))	;Default Rule
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id plAstara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster0   "  ?id " plAstara )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
(defrule plaster1
(declare (salience 4500))	;Default Rule
(id-root ?id plaster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id palaswara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plaster.clp 	plaster1   "  ?id " palaswara_kara )" crlf))
)

