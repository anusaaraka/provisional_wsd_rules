
;@@@ Added by 14anu-ban-11 on (09-02-2015)
;After getting the post of the head girl she, stalked in the corridor. (hinkhoj)
;मुख्य लडकी का पद मिलने के बाद,वह बरामदे में आकड कर चलने लगी . (self)
(defrule stalk2
(declare (salience 4901))
(id-root ?id stalk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 corridor|ground)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akada_kara_calane_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stalk.clp 	stalk2   "  ?id "  Akada_kara_calane_laga)" crlf))
)


;@@@ Added by 14anu-ban-11 on (09-02-2015)
;She claimed that he had been stalking her over a period of three years.(oald)
;उसने दावा किया कि वह तीन वर्षों तक उसका पीछा कर रहा था .(self)
(defrule stalk3
(declare (salience 4900))
(id-root ?id stalk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 he|she|they|we)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pICA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stalk.clp 	stalk3   "  ?id "  pICA_kara )" crlf))
)


;----------------------- Default Rules ------------------

(defrule stalk0
(declare (salience 5000))
(id-root ?id stalk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stalk.clp 	stalk0   "  ?id "  dAlI )" crlf))
)

;"stalk","V","1.Cipakara calanA"
;Tiger stalks in order to get his food.
(defrule stalk1
(declare (salience 4900))
(id-root ?id stalk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cipakara_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stalk.clp 	stalk1   "  ?id "  Cipakara_cala )" crlf))
)

;"stalk","V","1.Cipakara calanA"
;Tiger stalks in order to get his food.
;--"2.akadZakara calanA"        
;After getting the post of the head girl she stalked in the corridor.
;--"3.prakopiwa_honA"  
;That old hut is believed to be stalked by ghosts.
