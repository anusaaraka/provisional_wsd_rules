
;@@@ Added by Preeti(19-5-14)
;He chiseled off a corner of the block. [merriam-webster.com]
;usane KaNda ke kone ko CenI_se_kAtanA.
(defrule chisel2
(declare (salience 4900))
(id-root ?id chisel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CenI_se_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  chisel.clp 	chisel2   "  ?id "  "  ?id1 " CenI_se_kAta )" crlf))
)

;parser problem
;@@@ Added by Preeti(19-5-14)
;They chiseled me out of my money. [wordnetweb.princeton.edu]
;unhoMne mere pEse lUta liye.
(defrule chisel3
(declare (salience 4900))
(id-root ?id chisel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lUta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  chisel.clp 	chisel3   "  ?id "  "  ?id1 " lUta_le)" crlf))
)


;@@@ Added by 14anu-ban-03 (27-01-2015)
;The construction work of Ellora Caves have been done by chiselling high - hard rocks. [tourism]
;elorA guPAoM kA nirmANa kArya UzcI-kaTora catZtAnoM ko kAtakara kiyA gayA hE .[tourism]
(defrule chisel4
(declare (salience 4900))
(id-root ?id chisel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 rock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAtakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chisel.clp 	chisel4   "  ?id "  kAtakara )" crlf))
)

;------------------------ Default Rules -----------------------

;"chisel","N","1.CenI"
;Chisel is used in shaping wood,stone etc.
;
(defrule chisel0
(declare (salience 5000))
(id-root ?id chisel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CenI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chisel.clp 	chisel0   "  ?id "  CenI )" crlf))
)

;$$$ Modified by 14anu-ban-03 (27-01-2015)  ;meaning changed from 'warAsa' to 'warASa'
;$$$  Modified by Preeti(19-5-14)
;Letters were chiseled into a wall. [merriam-webster.com]
;akRara xIvAra para warASe gaye We.
(defrule chisel1
;(declare (salience 4900));salience changed
(id-root ?id chisel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warASa));meaning changed
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chisel.clp 	chisel1   "  ?id "  warASa )" crlf))
)

;"chisel","VT","1.CenI_se_kAtanA"
;The artist chiselled the marble.
;
