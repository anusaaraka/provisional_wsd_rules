;Modified by Meena(5.4.11); added the relations (kriyA-object ?id ?id1) and (id-root ?id1 election|player)
;Alan bet me five dollars Clinton would lose the election. 
;Added by Meena(1.4.10)
;Losing to a younger player was a bitter pill to swallow . 
(defrule lose0
(declare (salience 4900))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-to_saMbanXI ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 election|player)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp      lose0   "  ?id "  hAra )" crlf))
)






(defrule lose1
(declare (salience 5000))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vaMciwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lose.clp	lose1  "  ?id "  " ?id1 "  vaMciwa_ho  )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Chaitanya Sir (11-12-13)
;He has lost his confidence.
(defrule lose2
(declare (salience 100))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj confidence)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ko_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp      lose2   "  ?id "  Ko_xe )" crlf))
)

;$$$ Modified by 14anu-ban-08 (03-03-2015)      ;added constraint
;When the external force is removed, the body moves, gaining kinetic energy and losing an equal amount of potential energy.  [NCERT]
;जब बाह्य बल हटा लिया जाता है तो वस्तु गति करने लगती है और कुछ गतिज ऊर्जा अर्जित कर लेती है, तथा उस वस्तु की उतनी ही स्थितिज ऊर्जा कम हो जाती है.  [NCERT]
;जब बाह्य बल हटा लिया जाता है तो वस्तु गति करने लगती है और कुछ गतिज ऊर्जा अर्जित कर लेती है, तथा उस वस्तु की उतनी ही स्थितिज ऊर्जा खो जाती है.  [self]
;@@@ Added by Shirisha Manju Suggested by Chaitanya Sir (11-12-13)
;He has lost his purse|book.
(defrule lose3
(declare (salience 100))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj purse|book|amount)      ;added 'amount' by 14anu-ban-08 (03-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ko_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp      lose3   "  ?id "  Ko_jA )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Sukhada (13-05-14)
;A fat ugly boy had to eat too many fruits to lose his weight.
;eka BArI kurUpa ladake ko usakA vajana kama karane ke lie bahuwa aXika Pala KAne pade.
(defrule lose4
(declare (salience 100))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj weight)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp      lose4   "  ?id "  kama_kara )" crlf))
)


;She lost the war.
;@@@ Added by 14anu24
;Check what fees you will be expected to pay if you lose the case .
;यह पता कीजिए कि मामला हारने पर आपको कितनी फीस देनी पडेगी .
(defrule lose6
(declare (salience 100))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 case|war)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp     lose6   "  ?id "  hAra )" crlf))
)

;@@@ Added by 14anu21 on 05.07.2014
;We lost because we played badly. 
;हमने खोया क्योँकि हम बुरी तरह से खेले . 
;हम हारे क्योँकि हम बुरी तरह  खेले . 
(defrule lose7
(declare (salience 4990))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id ?id1)
(id-root ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose7   "  ?id "  hAra )" crlf))
)

;@@@ Added by 14anu-ban-08 (04-12-2014)
;If we confine our scope to mechanics, we would say that the kinetic energy of the block is lost due to the frictional force.   [NCERT]
;यदि हम अपने विषय-क्षेत्र को यान्त्रिकी तक ही सीमित रखें तो हम कहेंगे कि गुटके की गतिज ऊर्जा, घर्षण बल के कारण क्षयित हो गई है.    [NCERT]
(defrule lose8
(declare (salience 200))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 energy|work)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRayiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose8   "  ?id " kRayiwa )" crlf))
)

;Remove by 14anu-ban-08 (03-03-2015) beacuse this will fire from lose3
;@@@Added by 14anu-ban-08 (02-03-2015)
;When the external force is removed, the body moves, gaining kinetic energy and losing an equal amount of potential energy.  [NCERT]
;जब बाह्य बल हटा लिया जाता है तो वस्तु गति करने लगती है और कुछ गतिज ऊर्जा अर्जित कर लेती है, तथा उस वस्तु की उतनी ही स्थितिज ऊर्जा कम हो जाती है.  [NCERT]
;(defrule lose9
;(declare (salience 4900))
;(id-root ?id lose)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id ?id1)
;(viSeRya-of_saMbanXI ?id1 ?id2)
;(id-root ?id1 amount)
;(id-root ?id2 energy)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kama_ho))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp      lose9   "  ?id "  kama_ho )" crlf))
;)

;@@@Added by 14anu-ban-08 (18-03-2015)
;Set against the benefits of the new technology, there is also a strong possibility that jobs will be lost.  [oald]
;नयी प्रौद्योगिकी के लाभ के प्रतिकूल, एक भारी सम्भावना यह भी है कि नौकरियाँ चली जाएँगी .  [self]
(defrule lose10
(declare (salience 100))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose10   "  ?id " cala )" crlf))   
)


;@@@Added by 14anu-ban-08 (18-03-2015)
;Thousands of lives were lost in the earthquake.  [oald]
;हजारों जीवन भूकम्प में नष्ट हो गये.  [self]
(defrule lose11
(declare (salience 1000))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 earthquake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_ho))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose11   "  ?id " naRta_ho )" crlf))   
)

;@@@Added by 14anu-ban-08 (24-03-2015)
;Commiserations to the losing team!    [oald]
;हारने वाले दल को सहानुभूति!     [manual]
(defrule lose12
(declare (salience 4990))
(id-word ?id losing)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 team)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hArane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose12   "  ?id "  hArane_vAlA )" crlf))
)

;------------------- Default rules ---------------------

;Children who are handicapped lose out to play.
;apAhija bacce Kelane se vaMciwa raha jAwe hEM
(defrule lose5
(declare (salience 0))
(id-root ?id lose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lose.clp 	lose5   "  ?id "  Ko )" crlf))
)

;default_sense && category=verb	Ko xe	0
;"lose","V","1.Ko xenA"
;He lost his pen.
;His protests were lost in the noise.
;--"2.gazvA xenA"
;He lost a leg in the accident.
;--"3.mita jAnA"
;He has lost his confidence.
;--"4.hAra jAnA"
;They lost the series by 2-0.
;--"5.barabAxa honA"
;Twenty minutes were lost due to the drizzle.
;--"6.pICe ho jAnA"
;My watch loses two minutes every twenty-four hours.
;
;LEVEL 
;Headword : lose
;
;Examples --
;
;"lose","V","1.Ko xenA"
;He lost his pen.
;usane apanA pena Ko xiyA.
;He has lost his confidence.
;usane apanA AwmaviSvAsa Ko xiyA.
;His protests were lost in the noise.
;Sora meM usakA viroXa Ko gayA.
;Twenty minutes were lost due to the drizzle.
;bOCAra ke kAraNa bIsa minata Ko gaye.
;--"2.gazvA_xenA"
;He lost a leg in the accident.
;xurGatanA meM usane eka pAzva gazvA xiyA.
;We lost 30 minutes in the traffic jam.
;trEPika jAma meM hamane wIsa minata gazvA xiye.
;He lost his job because of his foul temper.
;apane kroXI svaBAva ke kAraNa usane apanI nOkarI gazvA xI.
;--"3.kama_honA"
;Rita has lost about 2 kg weight in the past two months.  
;piCale xo mahInoM meM rIwA kA lagaBaga 2 kilo vajZana kama huA hE.
;--"4.pICe ho jAnA"
;My watch loses two minutes every twenty-four hours.
;hara cObIsa GaNte meM merI GadZI 2 minata pICe ho jAwI hE.
;--"5.hAra jAnA"
;They lost the series by 2-0.
;ve 2-0 se SqMKalA hAra gaye.
;
;ukwa uxAharaNoM se 'lose' Sabxa ke xo arWa spaRtawaH uBara kara Awe hEM 
;'KonA' Ora 'hAranA'. SeRa arWa 'gazvA_xenA','kama_honA' Ora 'pICe_rahanA' 
;'KonA' se sambanXiwa lagawe hEM. ina saBI meM mUlawaH kuCa na kuCa Kone kA BAva hE.
;yaxi XyAna se xeKeM wo 'hArane' meM BI 'kuCa KowA' hE. isa xqRti se 'hAranA' BI
;'Kone' ke arWasUwra kI eka kadZI hE. awaH isakA anwarnihiwa sUwra kuCa isaprakAra se banegA  -
;
;anwarnihiwa sUwra ;
;
;KonA - Kone se kuCa kama ho jAnA -pICe raha jAnA -hAranA
;
;isakA sUwra hogA -
;
;sUwra : KonA^hAranA
