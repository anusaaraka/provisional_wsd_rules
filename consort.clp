
;@@@ Added by 14anu-ban-03 (14-04-2015)
;An orchestra by famous consort of the city came to our campus. [same file]
;शहर के प्रसिद्ध सङ्गीतज्ञों के द्वारा एक आर्केस्ट्रा हमारे  कैंपस में आया . [manual]
(defrule consort2
(declare (salience 5000))
(id-root ?id consort)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 famous)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgIwajFoM)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consort.clp 	consort2   "  ?id "  saMgIwajFoM )" crlf))
)


;@@@ Added by 14anu-ban-03 (14-04-2015)
;What were you consorting about?  [self with refrence to oald]
;आप किस विषय के बारे में बातचीत कर रहे थे? [manual]
(defrule consort3
(declare (salience 5000))       
(id-root ?id consort)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwacIwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consort.clp 	consort3   "  ?id "  bAwacIwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (14-04-2015)
;They claimed he had been consorting with drug dealers. [cald]
;उन्होंने दावा किया वह ड्रग व्यापारियों के साथ मेल-जोल रखता था . [manual]
(defrule consort4
(declare (salience 5000))       
(id-root ?id consort)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 dealer)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela-jola_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consort.clp 	consort4  "  ?id "  mela-jola_raKa )" crlf))
)


;-----------------------------------------------default rules------------------------------------------

;$$$ Modified by 14anu23 on 20/06/14
(defrule consort0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (14-04-2015)
(id-root ?id consort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWI)) ;meaning changed from pawnI to sAWI
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consort.clp 	consort0   "  ?id "  saWI )" crlf))
)

;"consort","N","1.pawnI/pawi_KAsakara_kisI_SAsaka_kI"
;The sultans had many consorts.
;--"2.saMgIwajFoM_yA_vAxya_yaMwroM_kA_samUha"
;An orchestra by famous consort of the city came to our campus.
;
(defrule consort1
(declare (salience 00))       ;salience reduced by 14anu-ban-03 (14-04-2015)
(id-root ?id consort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consort.clp 	consort1   "  ?id "  mela_kara )" crlf))
)

;"consort","V","1.mela_karanA"
;Their son has been consorting with drug-addicts for quite sometime.
