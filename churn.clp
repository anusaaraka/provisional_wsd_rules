;$$$  Modified by Preeti(30-07-2014)
;changed meaning  'kisI_vaswu_ko_bahuwa_aXika_mAwrA_meM_uwpanna_kara' to 'banA'
;"churn","V","kisI_vaswu_ko_bahuwa_aXika_mAwrA_meM_uwpanna_kara
;The company churns out thousands of bags every week.
;kaMpanI eka haPwe meM hajZAroM bEga banAwI hE
(defrule churn0
(declare (salience 5000))
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " churn.clp	churn0  "  ?id "  " ?id1 "  banA  )" crlf))
)

;$$$  Modified by Preeti(30-7-14)
;Vast crowds had churned the field into a sea of mud. 
;viSAla BIda ne Kewa ko kIcada ke samuxra meM  maWa xiyA WA.
;"churn","V","1.maWanA"
;Milk is churned to make butter.
(defrule churn2
(declare (salience 4800))
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?);condition added by Preeti
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  churn.clp 	churn2   "  ?id "  maWa )" crlf))
)

;@@@ Added by Preeti(30-07-2014)
;The wheels began to slowly churn.  [merriam-webster.com]
;pahiyoM ne GUmanA XIre_se AramBa kiyA.
(defrule churn4
(declare (salience 4800))
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id1 adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  churn.clp 	churn4   "  ?id "  GUma )" crlf))
)

;@@@ Added by Preeti(30-07-2014)
;He showed them how to churn butter. [merriam-webster.com]
;usane unako xiKAyA kEse makKana_nikAlawe hEM.
(defrule churn5
(declare (salience 4850))
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 butter)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 makKana_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  churn.clp 	churn5   "  ?id "  "  ?id1 " makKana_nikAla)" crlf))
)

;------------------------ Default Rules ----------------------
;"churn","N","1.maWane_kI_nAxa/matakI"
;Milk is shaken in the churn.
(defrule churn1
(declare (salience 4900))
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maWane_kI_nAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  churn.clp 	churn1   "  ?id "  maWane_kI_nAxa )" crlf))
)

;@@@ Added by Preeti(30-07-2014)
;The motorboats churned the water.  [merriam-webster.com]
;motara nOkoM ne pAnI ko wejI_se GUmAyA.
(defrule churn3
(id-root ?id churn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_GUmA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  churn.clp 	churn3   "  ?id "  wejI_se_GUmA )" crlf))
)

