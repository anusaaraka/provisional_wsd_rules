;$$$ Modified by 14anu-ban-04 (13-12-2014)
;@@@ Added by 14anu19 (24-06-2014)
;When Mohan heard about it he sent dancers and musicians to divert his son.
;जब मोहन ने इसके बारे में सुना उसने उसके बेटे को मनोरन्जन करने के लिए नर्तक और सङ्गीतकार भेजा.
;जब मोहन ने इसके बारे में सुना तो उसने अपने बेटे का मनोरंजन करने के लिए  नृतक और संगीतकार को भेजा .   ;translation is corrected by 14anu-ban-04 (13-12-2014)
(defrule divert3
(declare (salience 5500))
(id-root ?id divert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoraMjana_kara))          ;spelling is corrected by 14anu-ban-04 (13-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divert.clp 	divert3   "  ?id "  manoraMjana_kara )" crlf))                                  ;spelling is corrected by 14anu-ban-04  (13-12-2014)                  
)

;------------------- Default rules --------------------
(defrule divert0
(declare (salience 5000))
(id-root ?id divert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divert.clp 	divert0   "  ?id "  modZa )" crlf))
)

;"divert","V","1.modZanA^mArga_baxalanA"

;"divert","VT","1.modZanA/mArga_baxalanA"
;They diverted the traffic because of the demonstration.
;

