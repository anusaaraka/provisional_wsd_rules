
;$$$Modified by 14anu-ban-01 on (12-01-2015):Example added
;Kayaka also implied that no occupation was inferior or superior to another . [superior.clp]
;कायक की धारणा में यह भी निहित था कि कोई भी पेशा दूसरे से छोटा या बडा नहीं  .[superior.clp]
;कायक ने यह भी समझाया कि कोई भी पेशा किसी अन्य पेशे से छोटा या बड़ा नहीं होता.[self]
;कायक ने यह भी समझाया कि कोई भी पेशा किसी अन्य पेशे से निकृष्ट या उत्कृष्ट नहीं होता.
(defrule superior0
(declare (salience 5000))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(or (id-cat_coarse ?id adjective) (subject-subject_samAnAXikaraNa ? ?id)) ;2nd condition Added Sukhada for "He disputed that our program was superior."
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkqRta/badZA/variRTa));added "badZA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior0   "  ?id "  badZA/variRTa )" crlf));added "badZA" by 14anu-ban-01 on (12-01-2015)
)

;"superior","Adj","1.variRTa"
;A captain is superior to a lieutenant in the army.
;--"2.SreRTa"
;The quality of wheat from the northern region is superior.
;

;@@@ Added by 14anu-ban-01 on (18-03-2015)
;A bastard knockoff of a far superior thriller.[mw]
;एक बहुत  बढ़िया/अच्छी  रोमांचक फिल्म का एक घटिया अन्त हुआ . [self]
(defrule superior3
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 thriller|movie|cinema|feature|film|motion|picture|screenplay)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDiyA/acCI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior3   "  ?id "  baDiyA/acCI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-03-2015)
;She was chosen for the job because she was the superior candidate. [cald]
;वह नौकरी के लिए चुनी गयी क्योंकि वह बेहतर उम्मीदवार थी . [self]
(defrule superior4
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 candidate|education|team)	;added 'team' removed 'way' by 14anu-ban-01 on (19-03-2015)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behawara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior4   "  ?id "  behawara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-03-2015)
;That candidate seems superior than others.[self]
;वह उम्मीदवार अन्य की अपेक्षा बेहतर प्रतीत होता है . [self]
;For babies, breastfeeding is superior to bottle-feeding. [cald]
;शिशुओं के लिए, स्तनपान कराना बॉटल_से_दूध_पिलाने से बेहतर है . [self]
(defrule superior5
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 candidate|way|education|breastfeed|model|computer) ;added 'model|computer' by 14anu-ban-01 on (19-03-2015)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behawara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior5   "  ?id "  behawara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-03-2015)
;The quality of wheat from the northern region is superior.[superior.clp]
;उत्तरी क्षेत्र के गेहूँ की  किस्म सबसे_अच्छी/श्रेष्ठ है .  [self] 	;Translation improved by 14anu-ban-01 on (19-03-2015)
(defrule superior6
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 quality)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_acCA/SreRTa))	;added "sabase_acCA" by 14anu-ban-01 on (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior6   "  ?id "  sabase_acCA/SreRTa )" crlf))							;added "sabase_acCA" by 14anu-ban-01 on (19-03-2015)
)


;@@@ Added by 14anu-ban-01 on (19-03-2015)
;The government troops were superior in numbers.[cald]
;सरकारी दल संख्या में अत्यधिक थे .  [self]
(defrule superior7
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 number|figure)
(viSeRya-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyaXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior7   "  ?id "  awyaXika )" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-03-2015)
;A superior smile.[cald]
;एक व्यङ्ग्यात्मक मुस्कराहट . [self]
(defrule superior8
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 manner|smile)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyafgyAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior8   "  ?id "  vyafgyAwmaka )" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-03-2015)
;The soldier was reported to his superior officer for failing in his duties.[cald]
;अपने कार्यों में असफल होने के लिए सैनिक उसके उच्च अधिकारी के समक्ष हाज़िर किया गया था . [self]
(defrule superior9
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 officer|rank|deputy|agent|status|position)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior9   "  ?id "  ucca )" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-03-2015)
;Everything will be of superior quality. [coca]
;सब-कुछ सबसे_अच्छी/श्रेष्ठ  किस्म का रहेगा .  [self]
(defrule superior10
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 quality)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_acCA/SreRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior10   "  ?id "  sabase_acCA/SreRTa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-03-2015)
;He always looks so superior.[oald]
;वह हमेशा इतना मगरूर दिखता है . [self]
(defrule superior11
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id2 look|seem|behave)
(kriyA-subject  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id magZarUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior11   "  ?id "  magZarUra )" crlf))
)


;@@@ Added by 14anu-ban-01 on (19-03-2015)
;They see themselves as being morally superior to people of other faiths.[oald]
;वे स्वयं  को अन्य  विचारधारा वाले लोगों से नैतिक रूप से अधिक ऊँचा समझते हैं . [self]
(defrule superior12
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 socially|morally)
(viSeRya-viSeRaka  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika_UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior12   "  ?id "  sabase_acCA/SreRTa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-03-2015)
;Michael’s superior air had begun to annoy her.[oald]
;माइकल के दम्भपूर्ण_हाव_भाव/दम्भपूर्ण_व्यवहार ने उसे परेशान करना शुरु कर दिया था.  [self]
(defrule superior13
(declare (salience 5500))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 air)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xamBapUrNa_hAva_BAva/xamBapUrNa_vyavahAra))
(assert (id-wsd_viBakwi ?id ne ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  superior.clp 	superior13  "  ?id " ne)" crlf)	
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " superior.clp 	superior13  "  ?id "  "?id1 "  xamBapUrNa_hAva_BAva/xamBapUrNa_vyavahAra )" crlf))
)


;Commented by 14anu-ban-01 on (12-01-2015) because this meaning can be added in default rule.It was firing on;y because of high salience.Also there is no difference between "variRTa" and "badA"
;@@@ Added by 14anu11
;Kayaka also implied that no occupation was inferior or superior to another .
;कायक की धारणा में यह भी निहित था कि कोई भी पेशा दूसरे से छोटा या बडा नहीं .
;(defrule superior2
;(declare (salience 4000));6000 to 4000
;(id-root ?id superior)
;?mng <-(meaning_to_be_decided ?id)
;(or (id-cat_coarse ?id adjective) (subject-subject_samAnAXikaraNa ? ?id))
;(kriyA-vAkyakarma  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id bdA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior2   "  ?id "  bdA )" crlf))
;)

;------------------------ Default Rules ----------------------

;"superior","N","1.badZe_loga"
;Be respectful to your superiors.
(defrule superior1
(declare (salience 4900))
(id-root ?id superior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZe_loga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  superior.clp 	superior1   "  ?id "  badZe_loga )" crlf))
)

