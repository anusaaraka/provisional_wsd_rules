;@@@ Added by 14anu-ban-06 (07-04-2015)
;An indecisive battle.(OALD)
;अनिर्णायक लडाई . (manual)
(defrule indecisive1
(declare (salience 2000))
(id-root ?id indecisive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 battle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anirNAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indecisive.clp 	indecisive1   "  ?id "  anirNAyaka )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (07-04-2015)
;Some blame indecisive leadership for the party’s failure at the polls.(OALD)
;कुछ मतदान में पार्टी की असफलता के लिए ढुलमुल नेतृत्व पर दोष लगाते हैं . (manual)
(defrule indecisive0
(declare (salience 0))
(id-root ?id indecisive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dulamula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indecisive.clp 	indecisive0   "  ?id "  Dulamula )" crlf))
)
