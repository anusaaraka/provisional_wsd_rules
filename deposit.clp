
;@@@ Added by 14anu24
;Furniture often has to be ordered and you may have to pay a deposit .
;अक्सर , फर्नीचर को आर्डर करना पडता है और हो सकता है कि आपको कोऋ भुगतान देना पडे .
(defrule deposit2
(declare (salience 5020))         ;salience increased from 5000 to 5020 by 14anu-ban-04 (11-12-2014)
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-word ?id1 pay)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BugawAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp  deposit2   "  ?id "  BugawAna )" crlf))
)

;@@@ Added by 14anu-ban-04 (08-12-2014)
;The word magnet is derived from the name of an island in Greece called magnesia where magnetic ore deposits were found, as early as 600 BC.  [NCERT-CORPUS]
;'चुम्बक' शब्द यूनान के एक द्वीप मैग्नेशिया के नाम से व्युत्पन्न है, जहाँ बहुत पहले 600 ईसा पूर्व चुम्बकीय अयस्कों के भण्डार मिले थे.             [NCERT-CORPUS]
(defrule deposit3
(declare (salience 5010))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 ore|mineral|coal|gold) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaNdAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit3  "  ?id "  BaNdAra )" crlf))
)

;@@@ Added by 14anu-ban-04 (08-12-2014)
;Although there are large deposits of iron inside the earth, it is highly unlikely that a large solid block of iron stretches from the magnetic north pole to the magnetic south pole.          [NCERT-CORPUS]
;यद्यपि पृथ्वी के अन्दर लोहे के प्रचुर भण्डार हैं तथापि इसकी सम्भावना बहुत ही कम है कि लोहे का कोई विशाल ठोस खण्ड चुम्बकीय उत्तरी ध्रुव से चुम्बकीय दक्षिणी ध्रुव तक फैला हो.    [NCERT-CORPUS]
(defrule deposit4
(declare (salience 5010))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 mineral|iron|coal|gold) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaNdAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit4  "  ?id "  BaNdAra )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-02-2015)
;The bus deposited me miles from anywhere.                  [cald]
;बस ने मुझे किसी भी जगह से मीलों की दूरी पर छोड़ा.                         [self]
(defrule deposit5
(declare (salience 4910))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-subject ?id ?sub)
(id-root ?sub bus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit5   "  ?id " Coda )" crlf))
)

;NOTE [THERE IS A PARSER PROBLEM IN SENTENCE 1 SO IT RUNS IN PARSE NO 2]
;@@@ Added by 14anu-ban-04 (06-02-2015)
;The cuckoo deposits her eggs in other birds' nests.                [cald]
;कोयल अपने अण्डे अन्य चिड़ियों के घोंसलों में  रख देती है .                              [self]
;She deposited some books on my desk.                                [oald]
;उसने कुछ किताबें मेरी मेज पर रख दी.
(defrule deposit6
(declare (salience 4910))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 egg|book)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit6   "  ?id "  raKa_xe )" crlf))
)



;------------------------- Default rules -------------------

(defrule deposit0
(declare (salience 5000))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xarohara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit0   "  ?id "  Xarohara )" crlf))
)

;"deposit","N","1.Xarohara"
;His deposit was refunded when he returned the car
;
(defrule deposit1
(declare (salience 4900))
(id-root ?id deposit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposit.clp 	deposit1   "  ?id "  jamA_kara )" crlf))
)

;"deposit","VT","1.jamA_karanA"
;I want to deposit my savings in a National bank.
;
