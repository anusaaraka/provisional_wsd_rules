;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Sonam Gupta MTech IT Banasthali 11-3-2014
;Thus it is mainly the electromagnetic force that governs the structure of atoms and molecules the dynamics of chemical reactions and the mechanical thermal and other properties of materials. [physics]
;अतः परमाणु तथा अणुओं की संरचना, रासायनिक अभिक्रियाओं की गतिकी, तथा वस्तुओं के यान्त्रिक, तापीय तथा अन्य गुणों का परिचालन मुख्यतः विद्युत चुम्बकीय बल द्वारा ही होता है.
(defrule property1
(declare (salience 3000))
(id-root ?id property)
(id-cat_coarse ?id noun)
?mng <- (meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ?)(viSeRya-viSeRaNa  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property1  " ?id "  guNa )" crlf)))


;@@@ Added by 14anu-ban-09 on (31-07-2014)
;Such substances are called plastic and this property is called plasticity. [NCERT CORPUS]
;isa prakAra ke paxArWa ko plAstika waWA paxArWa ke isa guNa ko plAstikawA kahawe hEM.
(defrule property2
(declare (salience 5000))
(id-root ?id property)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-karma ?id1 ?id)
(id-root ?id1 call)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property2  " ?id "  guNa )" crlf)))

;@@@ Added by 14anu-ban-09 on 31-7-14
;Those books are my property.  [MW]
;ये पुस्तकें मेरी सम्पत्ति हैं .
(defrule property3
(declare (salience 4000))
(id-root ?id property)
(id-cat_coarse ?id noun)
?mng <- (meaning_to_be_decided ?id)
(subject-subject_samAnAXikaradNa ?id1 ?id)
(id-root ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampawwi))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property3  " ?id "  sampawwi )" crlf)))


;@@@ Added by 14anu-ban-09 on (05-09-2014)
;The judge enjoined them from selling the property.                 [merriam-webster]	;added by 14anu-ban-09 on (13-04-2015)
;न्यायाधीश ने संपत्ति  बेचने से उन पर रोक लगाया .                                      [self]	;added by 14anu-ban-09 on (13-04-2015)
;If water did not have this property, lakes and ponds would freeze from the bottom up, which would destroy much of their animal and plant life.  [NCERT CORPUS]
;yaxi jala meM yaha guNa na howA, woJIla waWA wAlAba walI se Upara kI ora jamawe jisase usakA aXikAMSa jalIya jIvana (jala jIva - janwu waWA pOXe) naRta ho jAwA.

(defrule property6
(declare (salience 5000))
(id-root ?id property)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id2 verb)
(kriyA-object  ?id2 ?id)
(kriyA-subject ?id2 ?id1)	;uncommented by 14anu-ban-09 on (13-04-2015)
(id-root ?id1 water) ;more constraints can be added	;uncommented by 14anu-ban-09 on (13-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property6  " ?id "  guNa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-02-2015)
;The technological exploitation of this property is generally credited to the Chinese.  [NCERT CORPUS]
;इस गुण के तकनीकी उपयोग का श्रेय आमतौर पर चीनियों को दिया जाता है.	[NCERT CORPUS]	

(defrule property7
(declare (salience 3000))
(id-root ?id property)
(id-cat_coarse ?id noun)
?mng <- (meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ? ?id) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property7  " ?id "  guNa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-02-2015)
;Lost property.   	[hindkhoj]
;खोई हुई सम्पत्ति.		[manual]
(defrule property8
(declare (salience 3500))
(id-root ?id property)
(id-cat_coarse ?id noun)
?mng <- (meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 lost) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampawwi))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property8  " ?id "  sampawwi )" crlf))
)

;-------------------------- Default rules -----------------------

;@@@ Added by Sonam Gupta MTech IT Banasthali 11-3-2014
;Those books are my property.  [MW]
;ये पुस्तकें मेरी सम्पत्ति हैं .
(defrule property4
(id-root ?id property)
(id-cat_coarse ?id noun)
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampawwi))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property4  " ?id "  sampawwi )" crlf)))

;@@@ Added by Sonam Gupta MTech IT Banasthali 11-3-2014
;Most observed phenomena in daily life are rather complicated manifestations of the basic laws. [physics]
;दैनिक जीवन की अधिकांश प्रेक्षित परिघटनाएँ मूल नियमों की जटिल अभिव्यक्ति ही होती हैं.
(defrule property5
(id-root ?id property)
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampawwi))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  property.clp 	property5  " ?id "  sampawwi )" crlf)))



