;@@@ Added by 14anu-ban-11 on (06-02-2015)
;People having stone in their kidney suffer from serious pain.(hinkhoj)
;लोग जिनके गुरदे में पथरी होती है  गम्भीर दर्द से पीडित हैं . (self)
(defrule stone2
(declare (salience 5001))
(id-root ?id stone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 kidney)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paWarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stone.clp 	stone2   "  ?id "  paWarI )" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-02-2015)
;There is a big stone inside every date.(hinkhoj)
;प्रत्येक सेब के अन्दर एक बडी गुठली है . (anusaaraka)
(defrule stone3
(declare (salience 5002))
(id-root ?id stone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-inside_saMbanXI  ?id ?id1)
(id-root ?id1 apple|mango|peach|date|orange)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guTalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stone.clp 	stone3   "  ?id "  guTalI)" crlf))
)

;------------------- Default Rules --------------------

(defrule stone0
(declare (salience 5000))
(id-root ?id stone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawWara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stone.clp 	stone0   "  ?id "  pawWara )" crlf))
)

;"stone","V","1.pawWara se mAranA"
;The mad man was stoned to death by the villagers.
(defrule stone1
(declare (salience 4900))
(id-root ?id stone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawWara_se_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stone.clp 	stone1   "  ?id "  pawWara_se_mAra )" crlf))
)

;"stone","V","1.pawWara se mAranA"
;The mad man was stoned to death by the villagers.
;--"2.guTalI nikAlanA"
;She stoned the cherries for the dessert.
;
;
