
(defrule command0
(declare (salience 5000))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id commanding )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SakwiSAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  command.clp  	command0   "  ?id "  SakwiSAlI )" crlf))
)

;In his comanding voice

;@@@ Added by 14anu06(Vivek Agarwal) on 19/6/2014******
;He has command over seven languages.
;उसका सात भाषाओं पर  प्रभुत्व है
(defrule command3
(declare (salience 5000))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-over_saMbanXI ?id ?id1)
(id-root ?id1 language|subject|skill|job|trade|topic )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBuwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command3   "  ?id "  praBuwva)" crlf))
)

;@@@Added by 14anu20 on 03.07.2014
;I am at your command.
;मैं आपके अधीन हू़ँ .

(defrule command23_1
(declare (salience 5000))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ? ?id)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command23_1   "  ?id "  aXIna )" crlf))
)


;@@@Added by 14anu20 on 03.07.2014
;He has good command of subject.
;उसे विषय में अच्छी दक्षता है . 
(defrule command23_2
(declare (salience 5000))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 noun)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xakRawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command23_2   "  ?id "  xakRawA )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-02-2015)
;Check that the computer has executed your commands. [oald]
;जाँचिए कि संगणक ने आपके आदेश कार्यान्वित किए हैं . [manual]
(defrule command4
(declare (salience 4900))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 execute)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command4   "  ?id "  AxeSa )" crlf))
)


;------------------- Default Rules -----------------
(defrule command2
(declare (salience 4800))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAsana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command2   "  ?id "  SAsana_kara )" crlf))
)


;"command","VT","1.SAsana_karanA"
;The general commanded a huge army
;--"2.mAzganA"
;This speaker commands a high fee


(defrule command1
(declare (salience 4900))
(id-root ?id command)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAsana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  command.clp 	command1   "  ?id "  SAsana )" crlf))
)


;
