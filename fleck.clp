;@@@ Added by 14anu-ban-05 on (02-04-2015)
;The fabric was red, flecked with gold.[OALD]
;कपडा लाल था और सोने से चित्ती डाली  थी . 	[MANUAL]

(defrule fleck2
(declare (salience 4901))
(id-root ?id fleck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwwI_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fleck.clp 	fleck2   "  ?id "  ciwwI_dAla )" crlf))
)

;------------------------ Default Rules ----------------------

;"fleck","N","1.ciwwI"
;There is a fleck of dust on your coat.
(defrule fleck0
(declare (salience 5000))
(id-root ?id fleck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fleck.clp 	fleck0   "  ?id "  ciwwI )" crlf))
)

;"fleck","V","1.Xabbe_dAlanA"
;While painting the house, he got flecked his clothes with paint.
(defrule fleck1
(declare (salience 4900))
(id-root ?id fleck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xabbe_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fleck.clp 	fleck1   "  ?id "  Xabbe_dAla )" crlf))
)

