;@@@ Added by 14anu-ban-04 (16-01-2015)
;The news had an electric effect on the waiting crowd.           [oald]
;प्रतीक्षा कर रही भीड पर समाचार का  रोमांचक प्रभाव हुआ .                        [self]
(defrule electric2
(declare (salience 4910))
(id-root ?id electric)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 effect)
(viSeRya-on_saMbanXI ?id1 ?id2)
(id-root ?id2 crowd)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id romAncaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  electric.clp 	electric2   "  ?id "  romAncaka )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-01-2015)      
;meaning changed from 'uwwejiwa_karane_vAlA' to 'romAncakArI'  (23-01-2015)
;The atmosphere was electric.               [oald]
;वातावरण  रोमांचकारी था .                         [self]
(defrule electric3
(declare (salience 5010))
(id-root ?id electric)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 atmosphere)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id romAncakArI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  electric.clp 	electric3   "  ?id "  romAncakArI )" crlf))
)

;------------------- Default rules ---------------------
(defrule electric0
(declare (salience 5000))
(id-root ?id electric)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  electric.clp 	electric0   "  ?id "  vixyuwa- )" crlf))
)

(defrule electric1
(declare (salience 4900))
(id-root ?id electric)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bijalI_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  electric.clp 	electric1   "  ?id "  bijalI_kA )" crlf))
)

;"electric","Adj","1.bijalI_kA"
;I am using electric water-heater.
;
;
