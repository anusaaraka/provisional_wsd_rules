;@@@ Added by 14anu-ban-02(11-02-2016)
;Infectious diseases can be acquired in several ways[oald]
;सङ्क्रामक बीमारियाँ कई कारणो से हो सकतीं हैं . [self]
(defrule acquire1
(declare (salience 100))
(id-root ?id acquire)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acquire.clp 	acquire1   "  ?id "  ho )" crlf))
)

;-------------------Default_rule-------------------

;@@@ Added by 14anu-ban-02(11-02-2016)
;She has acquired a good knowledge of English.[oald]
;उसने अङ्ग्रेजी का अच्छा ज्ञान अर्जित किया है . [self]
(defrule acquire0
(declare (salience 0))
(id-root ?id acquire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arjiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acquire.clp 	acquire0   "  ?id "  arjiwa_kara )" crlf))
)
