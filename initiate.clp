
(defrule initiate0
(declare (salience 5000))
(id-root ?id initiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xIkRiwa_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  initiate.clp 	initiate0   "  ?id "  xIkRiwa_vyakwi )" crlf))
)

;"initiate","N","1.xIkRiwa_vyakwi"
;After ablutions the initiate started reciting vedas.
;
(defrule initiate1
(declare (salience 4900))
(id-root ?id initiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AramBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  initiate.clp 	initiate1   "  ?id "  AramBa_kara )" crlf))
)

;"initiate","V","1.AramBa_karanA"
;The sutradhar initiates the sanskrit plays.
;--"2.xIkRA_xenA"
;He was initiated in the ceremony.
;
(defrule initiate2
(declare (salience 4800))
(id-root ?id initiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AramBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  initiate.clp 	initiate2   "  ?id "  AramBa_kara )" crlf))
)

;"initiate","VTI","1.AramBa_karanA"
;Rajaram Mohan Roy initiated social reforms..
;


;$$$ Modified by 14anu-ban-06 (18-03-2015)
;@@@ Added by 14anu27 on 2/7/2014***********
;The optimism of the Government in initiating peace in the Northeast is not shared by those who follow insurgency in the region .
;पर क्षेत्र में बागियों पर नजर रखने वाले प्रेक्षक पूर्वोत्तर में शांति प्रक्रिया शुरू करने में सरकार की आशावादिता से सहमत नहीं हैं .
;changed rule name from 'initiating31' to 'initiate3' by 14anu-ban-06 (18-03-2015)
(defrule initiate3
(declare (salience 5100));salience increased from '4500' to '5100' by 14anu-ban-06 (11-12-2014)
(id-root ?id initiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Suru_kara));meaning changed from 'Suru' to 'Suru_kara' by 14anu-ban-06 (18-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  initiate.clp 	initiate3   "  ?id " Suru_kara  )" crlf))
)
