;@@@ Added by 14anu21 on 24.06.2014
;Another shot was fired.
;एक और दृश्य ने दागा . (Translation before adding rule)
;एक और गोली दागी गई.
;He fired another shot.
;उसने एक और गोली दागी . 
(defrule shot0  
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-subject  ?idverb ?id)(kriyA-object  ?idverb ?id))
(id-root ?idverb fire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot0   "  ?id "  golI )" crlf)
)
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 01-08-2014
;He shot a bullet in the air.
;usane havA meM golI calAyI
;He shot an arrow from the bow.
;usane apanI kamAna se wIra calAya
(defrule shot00
(declare (salience 2000))
(id-root ?id shoot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 arrow|bullet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  shot.clp     shot00   "  ?id " calA  )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 01-08-2014
;Several films have also been shot in Gadsisar lake .
;gadZasIsara  JIla  meM  kaI  PilmoM  kI  SutiMga  BI  kI  gaI  hE 

;Some parts of the Hollywood blockbuster film Blue Lagoon were also shot here .
;hoYlIvuda kI blAkabastara Pilma blU lEgUna ke kuCa BAgoM kI SUtiMga BI yahAz kI gaI WI 
(defrule shot01
(declare (salience 2000))
(id-root ?id shoot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 film|part)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SUtiMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  shot.clp     shot01   "  ?id " SUtiMga  )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 01-08-2014
;I have never tried bowling before, but I thought I would give it a shot.
;mEMne pahale kaBI geMxa PeMkane kA prayAsa nahIM kiyA, paranwu mEMne socA ki mEM iske liye koSISa karUzgA
(defrule shot02
(declare (salience 05))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(id-root ?id1 a)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koSISa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  shot.clp     shot02   "  ?id " koSISa  )" crlf))
)



;@@@ Added by 14anu-ban-01 on (03-03-2015)
;He sealed victory with a marvellous shot.[self with reference to oald]
;उसने एक शानदार शॉट के साथ विजय पर [अपनी] मोहर लगाई.  [self] 
(defrule shot2
(declare (salience 10))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id2 victory|triumph|position|defeat)
(kriyA-object  ?id1 ?id2)
(kriyA-with_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoYta ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot2  "  ?id " SoYta)" crlf)
)
)

;@@@ Added by 14anu-ban-11 on (31-03-2015)
;Let me have a shot at it.(cald)
;मुझे  इस पर प्रयत्न करने दो. (self)
(defrule shot3
(declare (salience 10))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI  ?id ?id1)
(id-root ?id1 it)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayawna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot3   "  ?id "  prayawna)" crlf)
)
)


;@@@ Added by 14anu-ban-11 on (31-03-2015)
;Willian Tell was a very good shot. (hinkhoj)
;विल्लीयान टेल एक बहुत अच्छा निशानेबाज था . (self)
(defrule shot4
(declare (salience 15))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSAnebAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot4   "  ?id "  niSAnebAja)" crlf)
)
)


;@@@ Added by 14anu-ban-11 on (31-03-2015)
;Note:- working properly on parser no.7
;The brakes on this car are shot. (oald)
;इस गाडी का  ब्रेक खराब हैं . (self)
(defrule shot5
(declare (salience 20))
(id-word ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 brake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot5   "  ?id "  KZarAba)" crlf)
)
)

;@@@ Added by  14anu-ban-01 on (10-04-2015)
;His 18-yard shot hit the stanchion.[self: with reference to cald]
;उसका 18-गज़ शॉट गोलपोस्ट तक पहुंचा .[self] 
(defrule shot7
(declare (salience 10))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 hit)
(kriyA-subject  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoYta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot7   "  ?id "  SoYta)" crlf)
)
)

;------------------------------------------Default rules-----------------------------

;@@@ Added by 14anu-ban-01 on (03-03-2015)
;The man fired several shots from his pistol.[oald]
;आदमी ने उसकी पिस्तौल से कई गोलियाँ दागीं . [self]
(defrule shot1
(declare (salience 0))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot1   "  ?id " golI)" crlf)
)
)

;$$$ Modified by 14anu-ban-01 on (10-04-2015):corrected spelling
;@@@ Added by 14anu-ban-11 on (31-03-2015)      ;Note:- working properly on parser no.273
;Her evening dress is made of green shot silk.(cald)
;उसकी सन्ध्याकालीन पोशाक हरे रङ्गबिरङ्गे रेशम से बनाई गयी है . (self)
(defrule shot6
(declare (salience 00))
(id-root ?id shot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMgabiraMgA))	;corrected "raMgabiraMga" to "raMgabiraMgA"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shot.clp    shot6   "  ?id "  raMgabiraMgA)" crlf)	;corrected "raMgabiraMga" to "raMgabiraMgA"
)
)


