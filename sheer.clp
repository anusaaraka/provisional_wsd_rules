
(defrule sheer0
(declare (salience 5000))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-away_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahaka_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " sheer.clp sheer0 " ?id "  bahaka_jA )" crlf)) 
)

(defrule sheer1
(declare (salience 4900))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sheer.clp	sheer1  "  ?id "  " ?id1 "  bahaka_jA  )" crlf))
)

;@@@ Added by jagriti(25.1.2014)
;The suggestion is sheer nonsense. 
;सुझाव बिल्कुल बकवास है.
(defrule sheer2
(declare (salience 4800))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 mischief|nonsense)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bilkula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer2   "  ?id "  bilkula )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (28-02-2015)
;He fell off a sheer cliff.[self: with refernce to oald]
;वह एक ख़डी चट्टान से गिर गया.[self]
;A sheer drop of 100 metres.[cald]
;100 मीटर की खडी गहराई. [self]
;@@@ Added by jagriti(25.1.2014)
;A sheer ladder going upstairs.
;ऊपर की मञ्जिल जाते हुए एक खडी सीढी .   
(defrule sheer3
(declare (salience 4700))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 mountain|ladder|cliff|slope|drop|rock)		;added 'cliff|slope|drop|rock' by 14anu-ban-01 on (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadZA))			;changed 'KadZI' to 'KadZA' by 14anu-ban-01 on (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer3   "  ?id " KadZA )" crlf))						;changed 'KadZI' to 'KadZA' by 14anu-ban-01 on (28-02-2015)
)



;$$$ Modified by 14anu-ban-01 on (28-02-2015):Example changed,constraint added.
;This cloth is made up of sheer nylon.[self: with refernce to oald]
;यह कपडा पारदर्शी नायलन से बना हुआ है . [self]
;@@@ Added by jagriti(25.1.2014) 
(defrule sheer4
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 muslin|nylon|curtain)			;added 'nylon|curtain' and removed 'silk' by 14anu-ban-01 on (28-02-2015).
;NOTE:'Silk' is removed from here because in maximum cases it will have the meaning 'mahIna' and very less cases are there where the meaning is 'pAraxarSI' .It can be handled later when more cases of 'pAraxarSI' arise.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAraxarSI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer4   "  ?id " pAraxarSI )" crlf))
)

(defrule sheer5
(declare (salience 100))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer5   "  ?id "  bahaka_jA )" crlf))
)

(defrule sheer6
(declare (salience 100))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekaxama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer6   "  ?id "  ekaxama )" crlf))
)

;"sheer","Adv","1.ekaxama"
;The ground dropped away sheer at our feet.
;
;

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;The concert was sheer delight.[oald]
;सङ्गीत गोष्ठी पूर्णतया आनंदपूर्ण थी . [self]
(defrule sheer7
(declare (salience 100))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNawayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer7   "  ?id "  pUrNawayA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;We were impressed by the sheer size of the cathedral.[oald]
;हम बडा गिरजा के विशाल आकार से प्रभावित हुए थे. [self]
(defrule sheer8
(declare (salience 100))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 size|number)	;added 'number'  by 14anu-ban-01 on (28-02-2015)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer8   "  ?id "  viSAla )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;His success was due to sheer determination.[cald]
;उसकी सफलता [उसकी] वास्तविक/सच्ची दृढता के कारण थी .  [self]
;His success was due to sheer willpower. [cald]
;उसकी सफलता [उसकी] वास्तविक/सच्ची सङ्कल्प शक्ति  के कारण थी .[self]
(defrule sheer9
(declare (salience 200))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 willpower|determination|persistence|will)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika/saccA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer9   "  ?id "  vAswavika/saccA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
;That was sheer luck.[oald]
;वह केवल/सरासर भाग्य था.  [self]
;It was sheer coincidence that we met.[cald]
;यह केवल संयोग था कि हम मिले .[self] 
;It is insanity perhaps, or sheer stupidity.[coca]
;यह शायद पागलपन, या केवल मूर्खता है . [self]
(defrule sheer10
(declare (salience 200))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 luck|chance|coincidence|stupidity)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kevala/sarAsara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer10   "  ?id "  kevala/sarAsara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;She was wearing a dress of the sheer silk.[copied from sheer4]
;वह महीन रेशम का लिबास पहनी हुई थी . [self]
;She wore a dress of the sheerest silk.[cald]
;उसने (सबसे महीन/महीनतम) रेशम का लिबास पहना था. [self]
(defrule sheer11
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 silk|thread|fiber)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahIna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer11   "  ?id " mahIna )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;At other times, the sheer scale, complexity, or novelty of the military hardware on display was simply amazin.[coca]
;अन्य समय पर, विशुद्ध पैमाना, जटिलता, या  प्रदर्शन पर लगे सैन्य हार्डवेयर की नवीनता वास्तव में आश्चर्यजनक थी . [self]
(defrule sheer12
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 scale)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSuxXa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer12   "  ?id " viSuxXa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
; The sheer scale of the challenge is the point.[coca]
; चुनौती का वास्तविक पैमाना ही मुद्दा है .[self]
; I do wonder about the sheer scale of this mission.[coca]
; मैं इस लक्ष्य के वास्तविक स्तर/पैमाने को लेकर उत्सुकता रखता हूँ . [self]
(defrule sheer13
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 scale)
(viSeRya-of_saMbanXI ?id1 ?)	;saMbanXI can be specified when a counter example arises.
(id-cat ?id adjective|adjective_comparative|adjective_superlative)			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer13   "  ?id " vAswavika )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;I thought the boats were going to collide, but one sheered off at the last second.[cald]
;मुझे लगा था नावें टकराने वाली हैं , परन्तु अन्तिम क्षण में एक झटके से मुड गयी . [self]
(defrule sheer14
(declare (salience 5000))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Jatake_se_muda_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sheer.clp	sheer14  "  ?id "  " ?id1 "  Jatake_se_muda_jA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;Her mind sheered away from images she did not wish to dwell on.[oald]
;उसका मन उन  छवियों/ख्यालों से हट गया जिन पर विचार करने की उसने इच्छा नहीं की थी . [self: more faithful]
;उसका मन उन  छवियों/ख्यालों से हट गया जिन पर विचार करने की उसकी इच्छा नहीं  थी . [self: more natural]
(defrule sheer15
(declare (salience 5100))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 mind|attention)
(kriyA-subject ?id ?id1)
(id-word =(+ ?id 1) off|away)
(kriyA-from_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  (+ ?id 1)  hata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sheer.clp	sheer15  " ?id "   " (+ ?id 1) "   hata_jA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
;The sheer number of steel band programs has steadily increased.[coca]
;ढोल बजानेवाले समूह के प्रोग्रामों की असली सङ्ख्या  स्थिरतापूर्वक/लगातार बढी है . [self]
(defrule sheer16
(declare (salience 200))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 number)
(viSeRya-viSeRaNa ?id1 ?id)
(viSeRya-of_saMbanXI ?id1 ?)
(id-root ?id2 increase|raise|hike|decrease)
(kriyA-subject  ?id2 ?id1)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sheer.clp 	sheer16   "  ?id "  asalI )" crlf))
)


;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_sheer2
(declare (salience 4800))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mischief|nonsense)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bilkula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   sub_samA_sheer2   "   ?id " bilkula )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_sheer2
(declare (salience 4800))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mischief|nonsense)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bilkula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   obj_samA_sheer2   "   ?id " bilkula )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_sheer3
(declare (salience 4700))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mountain|ladder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   sub_samA_sheer3   "   ?id " KadZI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_sheer3
(declare (salience 4700))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mountain|ladder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   obj_samA_sheer3   "   ?id " KadZI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_sheer4
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 silk|muslin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAraxarSI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   sub_samA_sheer4   "   ?id " pAraxarSI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_sheer4
(declare (salience 4600))
(id-root ?id sheer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 silk|muslin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAraxarSI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sheer.clp   obj_samA_sheer4   "   ?id " pAraxarSI )" crlf))
)
