;@@@ Added by 14anu-ban-03 (24-02-2015)
;He is treated as a cipher in his office.[hinkhoj]
;वह अपने दफ्तर में शून्य के रूप में माना जाता है .  [self]
(defrule cipher0
(declare (salience 00))
(id-root ?id cipher)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SUnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cipher.clp 	cipher0   "  ?id "  SUnya )" crlf))
)

;@@@ Added by 14anu-ban-03 (24-02-2015)
;The message was written in cipher. [oald]
;सन्देश गुप्त लिखावट की कुञ्जी में लिखा गया था . [anusaaraka]
(defrule cipher1
(declare (salience 10))
(id-root ?id cipher)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 write)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwa_liKAvata_kI_kuFjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cipher.clp 	cipher1   "  ?id "  gupwa_liKAvata_kI_kuFjI )" crlf))
)

