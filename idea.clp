;Added by sheetal (10-09-09).
;She is full of bright ideas .
;vaha wejasvI vicAroM se BarI huI hE.
(defrule sh-idea0
(declare (salience 4900))	;salience decreses to 4900 from 5000 by 14anu-ban-02(08-02-2016) because it is conflicting with idea8
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(id-word ?id1 bright)
(viSeRya-viSeRaNa  ?id ?id1);Relation added by Roja for the given example. 06-12-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  idea.clp    sh-idea0   " ?id "  vicAra )" crlf))
)

;Added by sheetal (10-09-09).
;Do you have any idea of what he looks like?
;Can you give me an idea of the cost ?
(defrule sh-idea2
(declare (salience 4800))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(or (id-word =(- ?id 1) no|any)(id-word ?id1 cost))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       sh-idea2   "  ?id "  jAnakArI )" crlf))
)


;Added by Prachi Rathore[4-12-13].
;Such a study gives an idea of how human beings are born, grow up, get, old and die.[gyannidhi corpus]
;इस प्रकार का अध्ययन  जानकारी देता है कि मनुष्यों का जन्म कैसे होता है, वे कैसे बढ़ते हैं, कैसे बूढ़े होते हैं और कैसे उनकी मृत्यु होती है।
(defrule idea2
(declare (salience 4800))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 give)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       idea2   "  ?id "  jAnakArI )" crlf))
)

;Added by Prachi Rathore[4-12-13].
;If this is your idea of a joke, then I don't find it very funny.[OALD]
;यदि यह चुटकुले का आपका विचार है, तो  मैं इसको अत्यन्त हास्यास्पद नहीं पाता हूँ .
(defrule idea3
(declare (salience 4800))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       idea3   "  ?id "  vicAra )" crlf))
)


;$$$ Modified by 14anu09[25-06-14]
;@@@ Added by Prachi Rathore[29-3-14].
;You can get an idea of how bad the situation is in the office from the fact that DTO Anil Garg does not have any information regarding the notice on the delivery counter.
;ऑफिस की बदतर हालत का अंदाजा इसी बात से लगाया जा सकता है कि डीटीओ अनिल गर्ग को डिलीवरी काउंटर पर लगे इस नोटिस की जानकारी तक नहीं है।
(defrule idea4
(declare (salience 4800))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(kriyA-object  ? ?id)  ;###She flashed on the idea of writing a novel about her experiences. 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxAjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       idea4   "  ?id "  aMxAjA)" crlf))
)

;@@@ Added by 14anu-ban-06 (11-10-2014)
;Newton built on Galileo's ideas and laid the foundation of mechanics in terms of three laws of motion that go by his name.        (NCERT)
;nyUtana ne gElIliyo kI XAraNAoM ke AXAra para gawi ke wIna niyamoM jo unake nAma se jAne jAwe hEM, ke rUpa meM eka yAnwrikI kI AXAraSilA raKI.(NCERT)
(defrule idea5
(declare (salience 4900))
(Domain physics)
(id-root ?id idea)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-on_saMbanXI ? ?id)(and(kriyA-object ?id1 ?id)(id-root ?id1 give)))
;added (kriyA-object ?id1 ?id)(id-root ?id1 give) by 14anu-ban-06 (17-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAraNA))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  idea.clp 	idea5   "  ?id "  XAraNA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  idea.clp 	idea5   "  ?id "  physics )" crlf)
)
)

;@@@Added by 14anu24
;The idea of partition , more correctly the memory of it,has made Kashmir-or perhaps the entire body of India-Pakistan relationship-an argument where cold reason is at a premium .
;बंटवारे के विचार  ने कश्मीर - भारत - पाकिस्तान संबंधों - को ऐसी बहस का विषय बना दिया है जिसमें तार्किकता के लिए जगह नहीं दिखती .
(defrule idea6
(declare (salience 5500))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 partition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  idea.clp    idea6   " ?id "  vicAra )" crlf))
)

;default rule
;@@@ Added by 14anu-ban-06 (11-10-2014)
;Early thinkers like Aristotle had wrong ideas about it.(NCERT)
;Axya vicArakoM jEse araswU kI bala ke viRaya meM safkalpanA galawa WI.(NCERT)
(defrule idea0
(declare (salience 0))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  idea.clp    idea0   " ?id "  vicAra )" crlf))
)

;@@@ Added by 14anu09 [25-06-14]
;She flashed on the idea of writing a novel about her experiences. 
;उसने उपने अनुभवों के बारे में उपन्यास लिखने के विचार का स्मरण किया . 
(defrule idea7
(declare (salience 4900))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 write|do|build|make|store)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       idea7   "  ?id "  vicAra )" crlf))
)

;@@@Added by 14anu-ban-02(08-02-2016)
;The King was very pleased and rewarded his Prime Minister for his bright idea.[bright.clp]
;राजा अत्यन्त खुश हुये और अपने प्रधान मन्त्री को उनके अच्छे सु्झाव के लिए पुरुस्कार दिया.[self] 
(defrule idea8
(declare (salience 4900))
(id-root ?id idea)
?mng <- (meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id1 ?id)
(id-root ?id1 reward)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suJAva))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  idea.clp       idea8   "  ?id "  suJAva )" crlf))
)



