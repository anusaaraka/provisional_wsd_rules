
;@@@ Added by Prachi Rathore[12-2-14]
;Check that the computer is hooked up to the printer. [oald]
;जाँच करिये कि सङ्गणक मुद्रक से जुडा हुआ है . 
(defrule hook3
(declare (salience 5000))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 judA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hook.clp 	hook3  "  ?id "  " ?id1 " judA_ho  )" crlf))
)


;@@@ Added by Prachi Rathore[12-2-14]
;They formed the band in 2008, hooking up with bass player Rod Byrne.[oald]
;2008 में उन्होंने पुमन्द्रक वादक रोड बायर्न को साथी बनाते हुए  बैंड बनाया . 
(defrule hook4
(declare (salience 5050))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAWI_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hook.clp 	hook4  "  ?id "  " ?id1 " sAWI_banA )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-03-2015)
; He hooked his foot under the stool and dragged it over. (OALD)
;उसने स्टूल के नीचे उसके पाँवों से फँसाया और ऊपर से उसे खींचा . (manual)
(defrule hook5
(declare (salience 5100))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 finger|leg|thumb|foot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook5   "  ?id "  PazsA )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-03-2015)
;She had managed to hook a wealthy husband.(OALD)
;वह एक धनवान पति को फँसाने में सफल हुई थी . (manual)
(defrule hook6
(declare (salience 5200))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)   
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook6   "  ?id "  PazsA )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-03-2015)
;I hooked the first ball for a six.(OALD)
;मैंने छक्के के लिए पहला गेंद मारा . (manual)
;He hooked his shot over the bar. (OALD)
;उसने बार के ऊपर से अपना शॉट मारा . (manual)
(defrule hook7
(declare (salience 5300))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ball|shot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook7   "  ?id "  mArA )" crlf))
)


;@@@ Added by 14anu-ban-06 (17-03-2015)
;Dad hooked a fish.(COCA)
;पिताजी ने मछली पकडी.(manual)
(defrule hook8
(declare (salience 5400))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 fish)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook8   "  ?id "  pakadZA )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-03-2015)
;The images are used as a hook to get children interested in science.(OALD)
;प्रतिबिंब बच्चों की विज्ञान में रुचि लाने के लिए माध्यम के जैसे प्रयोग की जाती है . (manual)
;Well-chosen quotations can serve as a hook to catch the reader's interest.(OALD) 
;सुचयनित उद्धरण पाठक की रूचि लाने के लिए माध्यम के रूप में कार्य कर सकते हैं . (manual)
(defrule hook9
(declare (salience 5200))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-as_saMbanXI ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 quotation|image)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook9   "  ?id "  mAXyama )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

(defrule hook0
(declare (salience 5000))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id hooked )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aMkuSAkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hook.clp  	hook0   "  ?id "  aMkuSAkAra )" crlf))
)

;"hooked","Adj","1.aMkuSAkAra"
;yArda meM krena se 'hooked'(aMkuSAkAra)kAzte meM PazsA kara BArI sAmAna uwArawe hEM.
;
(defrule hook1
(declare (salience 4900))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook1   "  ?id "  uTA )" crlf))
)

;default_sense && category=verb	kAzte se pakadZa	0
;"hook","V","1.kAzte se pakadZanA"
;usane kuez meM se bAltI 'hook'(kAzte se pakadZa kara)nikAlI.
;--"2.geMxa mArane kI eka viXi"
;usane geMxa ko pICe kI ora 'hook' kiyA.
;
;

;@@@ Added by Prachi Rathore[12-2-14]
;Hang your towel on the hook.[oald]
; हुक पर अपना तौलिया लटकाइये. 
(defrule hook2
(declare (salience 4900))
(id-root ?id hook)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id huka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hook.clp 	hook2   "  ?id "  huka )" crlf))
)

