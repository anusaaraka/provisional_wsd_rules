;@@@ Added by 14anu-ban-06 (12-03-2015)
;It was a jammy assignment. (cambridge)
;यह एक आसान कार्यभार था . (manual)
(defrule jammy1
(declare (salience 2000))
(id-root ?id jammy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 assignment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammy.clp 	jammy1   "  ?id "  AsAna )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-03-2015)
;She left jammy fingermarks on the tablecloth.(cambridge)
;वह मेजपोश पर जैम से भरे उंगलियों के निशान छोड कर चली गई . (manual)
(defrule jammy2
(declare (salience 2500))
(id-root ?id jammy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word ?id1 fingers|fingermarks|finger|fingermark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jEma_se_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammy.clp 	jammy2   "  ?id "  jEma_se_BarA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (12-03-2015)
;He wasn't even trying to score - the ball just bounced off the jammy beggar's head into the goal.(cambridge)
;वह स्कोर करने का प्रयास भी नहीं कर रहा था- गेंद खुशकिस्मत भिखारी के सिर से लक्ष्य में  लगी. (manual)
(defrule jammy0
(declare (salience 0))
(id-root ?id jammy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuSakismawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jammy.clp    jammy0   "  ?id "  KuSakismawa )" crlf))
)

