;$$$ Modified by 14anu-ban-03 (08-11-2014)
;####[COUNTER EXAMPLE] ##### We know from experience that a glass of ice-cold water left on a table on a hot summer day eventually warms up whereas a cup of hot tea on the same table cools down.[NCERT CORPUS]
;apane anuBavoM se hama yaha jAnawe hEM ki kisI wapwa garmI ke xina eka meja para raKA barPa ke SIwala jala se BarA gilAsa aMwawogawvA garma ho jAwA hE jabaki wapwa cAya se BarA pyAlA usI meja para TaMdA ho jAwA hE. [NCERT CORPUS]
;Added by Meena(17.9.09)
;Would you like a cup of tea?
(defrule cup0
(declare (salience 5000))
(id-root ?id cup)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id3))  ;added by 14anu-ban-03 (08-11-2014)
;(id-root ?id1 ?)
;(viSeRya-det_viSeRaNa ?id ?id1)   ;commented by 14anu-ban-03 (08-11-2014)
;(not(viSeRya-RaRTI_viSeRaNa ?id ?id1))   ;commented by 14anu-ban-03 (08-11-2014)
(viSeRya-of_saMbanXI ?id ?id2)
(id-root ?id3 of)
(kriyA-object ?id1 ?id)  ;added by 14anu-ban-03 (08-11-2014)
(id-root ?id1 like)  ;added by 14anu-ban-03 (08-11-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id3 ?id2 pyAlA_cAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cup.clp	cup0  "  ?id "  " ?id3 " " ?id2"  pyAlA_cAya  )" crlf))
)




;Added by Meena(17.9.09)
;This is not my cup of tea.
(defrule cup1
(declare (salience 6000))
(id-root ?id cup)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 of)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
(not(viSeRya-det_viSeRaNa ?id ?id1))
(viSeRya-of_saMbanXI ?id ?id3)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ?id3 mawalaba_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cup.clp	cup1  "  ?id "  " ?id1 "  " ?id3" mawalaba_kA  )" crlf))
)


;@@@ Added by 14anu-ban-03 (13-03-2015)
;His name was engraved on the silver cup. [oald]
;उसका नाम रजत कप पर उत्कीर्ण किया गया था . [manual]
;The silver cup was engraved with his name. [oald]
;रजत कप उसके नाम से उत्कीर्ण किया गया था . [manual]
(defrule cup4
(declare (salience 5000))
(id-root ?id cup)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id ?id1) (samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kapa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cup.clp 	cup4   "  ?id "  kapa )" crlf))
)


;------------------Default Rules------------------------

(defrule cup2
(declare (salience 4000))
(id-root ?id cup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pyAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cup.clp 	cup2   "  ?id "  pyAlA )" crlf))
)

(defrule cup3
(declare (salience 4900))
(id-root ?id cup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pyAle_ke_rUpa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cup.clp 	cup3   "  ?id "  pyAle_ke_rUpa_xe )" crlf))
)

;"cup","VT","1.pyAle_ke_rUpa_xenA"
;The girl was holding the berries in her cupped hands
;
;
