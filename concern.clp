;$$$ Modified by 14anu06 
;This article concerns you.
;यह लेख तुम्हारे बारे मे है.
(defrule concern0
(declare (salience 5000))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id concerning|concerns )          ;***added concerns  by 14anu6 *****   
;(id-cat_coarse ?id preposition)            ;**********commented by 14anu6 on 2014******
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  concern.clp  	concern0   "  ?id "  ke_bAre_meM )" crlf))
)

;"concerning","Prep","1.ke_bAre_meM"
;The Board expressed doubts concerning the bowler's bowlng action.
;
(defrule concern1
(declare (salience 4900))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id concerned )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMbanXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  concern.clp  	concern1   "  ?id "  saMbanXiwa )" crlf))
)

;"concerned","Adj","1.saMbanXiwa/viRaya_meM"
;Concerned relatives rushed to the hospital to see the patient.
;
;
;@@@ Added by 14anu-ban-03 on (12-09-2014)
;In the preceding Chapter, our concern was to describe the motion of a particle in space quantitatively. [NCERT CORPUS]
;piCale aXyAya meM hamArA sambanXa xiksWAna meM kisI kaNa kI gawi kA mAwrAwmaka varNana karane se WA.
(defrule concern4
(declare (salience 4800))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concern.clp 	concern4   "  ?id "  sambanXa  )" crlf))
)

;@@@ Added by 14anu-ban-03 (15-10-2014)
;Rather, it deals with systems in macroscopic equilibrium and is concerned with changes in internal energy, temperature, entropy, etc., of the system through external work and transfer of heat.[ncert]
;अपितु यह स्थूल सन्तुलन के निकायों पर विचार करती है, तथा इसका सम्बन्ध बाह्य कार्य तथा ऊष्मा स्थानान्तरण द्वारा निकाय की आन्तरिक ऊर्जा, ताप, ऐन्ट्रॉपी आदि में अन्तर से होता है.[ncert]
(defrule concern5
(declare (salience 4700))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concern.clp 	concern5   "  ?id "  sambanXa_ho )" crlf))
)

;@@@Added by 14anu24
;The arbitrator or arbiter will be independent and an expert in the particular field concerned , and may be a member of the Chartered Institute of Arbitrators .
;आरबिट्रेटर स्वतंत्र होता है , वह संबंधित क्षेत्र का विशेष  होता है और चार्टर्ड इन्सटिट्यूट आफ आरबोट्रेटर्जऋ का सदस्य हो सकता है .
(defrule concern6
(declare (salience 4000))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id concerned)
(id-cat_coarse ?id verb)
;(kriyA-subject  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMbanXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  concern.clp   concern6   "  ?id "  saMbanXiwa )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-04-2015)
;The two concerns decided to connect. [hinkhoj]
;दो संस्थाओं ने मिलने का फैसला किया .  [manual]
(defrule concern7
(declare (salience 5000))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 connect)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMsWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concern.clp 	concern7   "  ?id "  saMsWA  )" crlf))
)

;--------------------------------------Default Rules-----------------------------------
(defrule concern3
(declare (salience 00))  ;Salience reduced by 14anu-ban-03 (13-12-2014)
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cinwiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concern.clp 	concern3   "  ?id "  cinwiwa_ho )" crlf))
)

(defrule concern2
(declare (salience 4800))
(id-root ?id concern)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cinwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concern.clp 	concern2   "  ?id "  cinwA )" crlf))
)

;-----------------------------------------------------------------------------------------------

;default_sense && category=verb	cinwiwa_kara	0
;"concern","VT","1.cinwiwa_karanA"
;She is very much concerned about her health.
;
;
;LEVEL 
;Headword : "concern"
;


;Examples --
;
;1. There is no cause for concern.
;cinwA kA koI kAraNa nahIM hE.
;2. Our main concern is for her safety.
;usakI surakRA hamArA muKya ciMwA viRaya hE.
;3. This matter does not concern them.
;yaha viRaya unase vAswA nahIM raKawA.  <--unakI cinwA kA viRaya nahIM hE
;4. He is employed in a huge industrial concern.
;vaha eka badZe OXyogika saMsWAna meM kAma para raKA gayA hE.
;
;uparaliKiwa vAkyoM meM "concern" Sabxa ke jo Binna arWa A raheM hEM unameM vAswava meM
;saMbaMXa hEM.
;
;vAkya 1. meM "concern" kA jo arWa "cinwA" A rahA hE Ora vAkya 2. meM jo arWa
;"cinwana viRaya" kA A rahA hE, unameM jo BAva mUlawaH prakata ho rahA hE vaha hE "cinwana viRaya yAni saMkRepa meM 'cinwA' kA". 
;
;vAkya 3. meM "concern" kA jo arWa A rahA hE, vaha hE, "cinwA viRaya se vAswA yA saMbaMXa hone" kA.
;
;vAkya 4. meM BI "concern" kA jo arWa "saMsWAna" A rahA hE, vaha vAkya 3. ke arWa 
;"saMbaMXa" se prApwa kiyA jA sakawA hE. wo Pira, vAkya 4. meM "industrial concern"
;ke saMxarBa meM "concern" kA "uxyoga se saMbaMXavAlA" EsA arWa hama prApwa kara sakawe hEM. paranwu yaha PilahAla 'cinwA' se koI sIXA saMbanXa nahIM xiKA rahA. awaH ise alaga raKA jA sakawA hE.
;
;anwarnihiwa sUwra ;
;
;cinwA -- cinwA viRaya -- viRaya se saMbanXiwa - vAswA 
;Parma
;
;wo "concern" ke lie aba hama sUwra isa prakAra xe sakawe hEM.
;
;sUwra : ciMwA^vAswA/Parma
;
