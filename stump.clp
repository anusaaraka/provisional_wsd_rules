;@@@ Added by 14anu-ban-11 on (06-02-2015)
;He stumped around the country trying to build up support.(oald)
;वह सहारा बढाने  का प्रयास करते हुए देश के चारों ओर भाषण देता चला . [manual]
(defrule stump3
(declare (salience 4801))
(id-root ?id stump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
(id-root ?id1 country)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BARaNa_xewA_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stump.clp 	stump3   "  ?id "  BARaNa_xewA_cala )" crlf))
)

(defrule stump1
(declare (salience 4900))
(id-root ?id stump)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTinAI_meM_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stump.clp 	stump1   "  ?id "  kaTinAI_meM_dAla )" crlf))
)


;------------------- Default Rules -------------------

;"stump","N","1.TUzTa"
;Here is the stump of a pencil. 
(defrule stump0
(declare (salience 5000))
(id-root ?id stump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TUzTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stump.clp 	stump0   "  ?id "  TUzTa )" crlf))
)

;"stump","V","1.kaTinAI_meM_padZanA[dAlanA]"
;The lecture stumped everybody.
(defrule stump2
(declare (salience 4800))
(id-root ?id stump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTinAI_meM_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stump.clp 	stump2   "  ?id "  kaTinAI_meM_padZa )" crlf))
)

;"stump","V","1.kaTinAI_meM_padZanA[dAlanA]"
;The lecture stumped everybody.
;--"2.BARaNa_xewe_calanA"
;They stumped the whole district within a week.
;--"3.Auta_kara_xenA"
;The wicket keeper stumped the batsman.
;
;
;"stump","N","1.TUzTa"
;Here is the stump of a pencil. 
;--"2.stampa{kriketa}"
;Sachin's ball hit the stump.
;

