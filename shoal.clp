;@@@ Added by 14anu-ban-11 on (02-04-2015)
;The captain was adviced to steer away the ship from the shoals.(hinkhoj)
;कप्तान ने जहाज को रेत से  दूर रखने का  सुझाव दिया था .(self) 
(defrule shoal1
(declare (salience 10))
(id-root ?id shoal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-from_saMbanXI  ?id1 ?id)
(id-root ?id1 ship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shoal.clp 	shoal1   "  ?id "  rewa)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (02-04-2015)
;In the big lake I saw many fishes swimming in shoals. (hinkhoj)
;बडी झील में मैंने बहुत मछलियों को झुण्डों में तैरते हुए देखा . (self)
(defrule shoal0
(declare (salience 00))
(id-root ?id shoal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JuMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shoal.clp 	shoal0   "  ?id "  JuMda)" crlf))
)






