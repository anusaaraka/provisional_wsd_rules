;@@@ Added by 14anu-ban-03 (09-03-2015)
;The cohort of the public is helping the government to stand the oppositions. [hinkhoj]
;जनता का सहयोग विपक्ष को खडा करने के लिए सरकार की सहायता कर रहा है . [manual]
(defrule cohort0
(declare (salience 00)) 
(id-root ?id cohort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cohort.clp 	cohort0   "  ?id "  sahayoga )" crlf))
)


;@@@ Added by 14anu-ban-03 (09-03-2015)
;Robinson and his cohorts were soon ejected from the hall. [oald]
;रॉबन्सन और उसके साथी हॉल से शीघ्र बाहर आए थे . [manual]
(defrule cohort1
(declare (salience 100)) 
(id-root ?id cohort)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cohort.clp 	cohort1   "  ?id "  sAWI )" crlf))
)
