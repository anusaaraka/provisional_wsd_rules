;@@@ Added by 14anu-ban-11 on (07-02-2015)
;I think white would be too stark for the bedroom.(oald)
;मैं सोचता हूँ कि सफेद शयन कक्ष के लिए ज्यादा ही फीकी रहेगा .(self) 
(defrule stark1
(declare (salience 200))
(id-root ?id stark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 white)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stark.clp 	stark1   "  ?id "  PIkA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (07-02-2015)  ;Note:- working properly on parser no. 438.
;Her stark madness can lead her to commit suicide.(hinkhoj)
;उसका पूर्ण रूप से पगलपन उसको आत्महत्या करने के लिये बाध्य कर सकता है . (self)
(defrule stark2
(declare (salience 300))
(id-root ?id stark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 madness)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stark.clp 	stark2   "  ?id "  pUrNa_rUpa_se )" crlf))
)

;------------------------ Default Rules --------------------

;@@@ Added by 14anu-ban-11 on (07-02-2015)
;We all have to face stark realities of life.(hinkhoj)
;हम सब को जीवन की कठोर वास्तविकता का सामना करना है . (self) 
(defrule stark0
(declare (salience 100))
(id-root ?id stark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stark.clp    stark0   "  ?id "  kaTora )" crlf))
)


