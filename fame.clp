;@@@ Added by 14anu-ban-05 on (13-01-2015)
;In Patna one after another powerful kings ruled who while increasing the fame of their kingdom also every time called their kingdom with a new name .[TOURISM]
;patanA meM eka ke bAxa eka prawApI rAjAoM kA SAsana rahA jinhoMne apane rAjavaMSa kI KyAwi ko baDZAwe hue apanI rAjaXAnI ko hara bAra eka nayA nAma xekara saMboXiwa kiyA .[tourism]
;During the period of Ashoka , the fame of Magadh and its capital Patliputra pervaded all over the world .[tourism]
;samrAta aSoka ke kAlaKaMda meM magaXa Ora usakI rAjaXAnI pAtalIpuwra kI KyAwi xuniyA Bara meM vyApwa WI .[tourism]

(defrule fame2
(declare (salience 4902))
(id-root ?id fame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KyAwi))
(assert (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fame.clp 	fame2   "  ?id "  KyAwi )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  fame.clp 	fame2   "  ?id1 " kA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-01-2015)
;Saint like Hardaul received fame like gods as a result of this great sacrifice .        [tourism]
;आरदउल जैसा साधु ने इस बडे बलिदान के परिणामस्वरूप देवताओं जैसी कीर्ति प्राप्त किया .                  [tourism]
(defrule fame3
(declare (salience 4901))
(id-root ?id fame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 receive)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIrwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fame.clp 	fame3   "  ?id "  kIrwi )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-01-2015)
;Even today the same fame can be seen in the regions near it like Kumrahr , Agamkuan , Bulandi bagh and Kankarbagh . [tourism]
;Aja BI vahI Soharawa isa ke AsapAsa ke kRewroM jEse kumarAhara , agama kuzA , bulaMxI bAga , kaMkadZa bAga kRewroM meM xeKI jA sakawI hE .[manual]
(defrule fame4
(declare (salience 4901))
(id-root ?id fame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 see)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Soharawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fame.clp 	fame4  "  ?id "  Soharawa )" crlf))
)

;----------------- Default rules ---------------------

(defrule fame0
(declare (salience 5000))
(id-root ?id fame)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id famed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prasixXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fame.clp  	fame0   "  ?id "  prasixXa )" crlf))
)

;"famed","Adj","1.prasixXa/prawiRTiwa"
;He lives near the famed Kovalam Beach.
;
(defrule fame1
(declare (salience 4900))
(id-root ?id fame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasixXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fame.clp 	fame1   "  ?id "  prasixXi )" crlf))
)

;"fame","N","1.prasixXi/prawiRTA"
;Sachin has earned lot of fame as a cricketer.
;
;
