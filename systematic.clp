;----------------------------------Default Rule------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (16-08-14).
;31948:Much of the neglect of the Chhokar family in the lead up to and during the first trial might have been averted if there had been systematic communication between the police and the Procurator Fiscal .[Karan Singla]
;यदि पुलिस और प्रोक्यूरेटर फिस्कल के बीच व्यवस्थित संवाद रहा होता तो पहला मुक़दमा शुरु होने तक और मुक़दमे के दौरान छोकर परिवार की ज्यादातर उपेक्षा को 
;रोका जा सकता था .[Karan Singla]

(defrule systematic0
(declare (salience 0))
(id-root ?id systematic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  systematic.clp  	systematic0   "  ?id " vyavasWiwa )" crlf))
)
;-------------------------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (16-08-14).
;356:The systematic errors are those errors that tend to be in one direction, either positive or negative.  [NCERT corpus] 
;क्रमबद्ध त्रुटियाँ वे त्रुटियाँ हैं जो किसी एक दिशा धनात्मक या फिर ऋणात्मक में प्रवृत्त होती हैं.[NCERT corpus] 
;73667:His compositions give us a complete and systematic exposition of the Shatstha / a philosophy .[Karan Singla]
;उसकी रचनाओं से हमें षट्स़्थल के दर्शन का संपूर्ण और क्रमबद्ध विवेचन प्राप़्त होता है .[Karan Singla]
(defrule systematic1
(declare (salience 0))
(id-root ?id systematic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 error|exposition)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kramabaxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  systematic.clp  	systematic1   "  ?id " kramabaxXa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-08-14).
;2826:Newton was the first to study, in a systematic manner, the relation between the heat lost by a body in a given enclosure and its temperature.[NCERT corpus]  
;2826:न्यूटन ऐसे पहले वैज्ञानिक थे जिन्होंने किसी दिए गए अंतःक्षेत्र के भीतर रखे किसी पिण्ड द्वारा लुप्त ऊष्मा तथा उसके ताप के बीच संबंध का योजनाबद्ध अध्ययन किया.[NCERT corpus]
(defrule systematic2
(declare (salience 0))
(id-root ?id systematic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 manner)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id yojanabaxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  systematic.clp  	systematic2   "  ?id " yojanabaxXa )" crlf))
)
