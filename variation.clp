;@@@ Added by 14anu-ban-07 (14-10-2014)
;The human curiosity towards these variations takes him from one place to another even if it is related to religion , language , tradition , food habit ,  architecture or natural environment .(tourism corpus)
;इन्हीं विविधताओं के प्रति इंसान की जिज्ञासा उसे एक स्थान से दूसरे स्थान पर ले जाती है चाहे वह धर्म , भाषा , रीतिरिवाज , खानपान , स्थापत्य या प्राकृतिक परिवेश से संबंधित क्यों न हो । 
;(tourism corpus)
(defrule variation0
(id-root ?id variation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viviXawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  variation.clp 	variation0   "  ?id "  viviXawA )" crlf))
)

;@@@ Added by 14anu-ban-07 (14-10-2014)
;The important point is that a clock's zero error is not as significant for precision work as its variation, because a 'zero-error' can always be easily corrected.(ncert corpus)
;महत्वपूर्ण बात यह है कि घडी की शून्याङ्क त्रुटि, परिशुद्ध कार्य के लिए उतनी महत्वपूर्ण नहीं है जितना इसके समय में होने वाला परिवर्तन है, क्योंकि, शून्याङ्क त्रुटि को तो कभी भी सरलता से दूर किया जा सकता है.(ncert corpus)
;Gases, on the other hand exhibit a large variation in densities with pressure.(ncert corpus)
;इसके विपरित, गैसें दाब में परिवर्तन के साथ घनत्व में अत्यधिक परिवर्तन दर्शाती हैं.(ncert corpus)
;The commonly used property is variation of the volume of a liquid with temperature.(ncert corpus)
;सामान्य उपयोग में आने वाला गुण "ताप के साथ किसी द्रव के आयतन में परिवर्तन" होता है.(ncert corpus)
;These equations give the velocity and the distance traveled as a function of time and also the variation of velocity with distance.(ncert corpus)
;ये समीकरण वस्तु के वेग, और उसके द्वारा चली गई दूरी को समय के फलन के रूप में तथा दूरी के सापेक्ष उसके वेग में परिवर्तन को व्यक्त करते हैं .(ncert corpus)
(defrule variation1
(declare (salience 1000))
(id-root ?id variation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-as_saMbanXI  ? ?id)(viSeRya-viSeRaNa  ?id ?)(viSeRya-of_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  variation.clp 	variation1   "  ?id "  parivarwana )" crlf))
)

;@@@ Added by 14anu-ban-07 (14-10-2014)
;Variation in wilderness in one of the most thrilling games of the world .(tourism corpus)
;बियाबान में विचरण करना विश्व के रोमांचकारी खेलों में से एक है ।(tourism corpus)
;This village is a unique heaven for trekking , forest variation .(tourism corpus)
;यह गाँव ट्रैकिंग , वन विचरण के लिए एक अनोखा स्वर्ग है ।(tourism corpus)
;For the activities of trekking and wilderness variation Kuppad Jubbal is an extremely important pavilion .(tourism corpus)
;ट्रैकिंग एवं बियाबान विचरण की गतिविधियों के लिए कुप्पड़ जुब्बल अत्यंत महत्त्वपूर्ण क्रीड़ास्थल है ।(tourism corpus)
(defrule variation2
(declare (salience 1000))
(id-root ?id variation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)(viSeRya-in_saMbanXI  ?id ?id1)(viSeRya-for_saMbanXI  ?id ?id1))
(id-root ?id1 forest|wilderness)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  variation.clp 	variation2   "  ?id "  vicaraNa )" crlf))
)



