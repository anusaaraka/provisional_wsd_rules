;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;High tides have swamped the coast.[cald]
;ऊँचे ज्वार भाटा समुद्र तट को जलमग्न कर चुके हैं .  [self]
(defrule swamp1
(declare (salience 0))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalamagna_kara/pAnI_se_Bara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  swamp.clp 	swamp1    "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp1   "  ?id "  jalamagna_kara_xe/pAnI_se_Bara_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;The boat was swamped by an enormous wave.[cald]
;नाव को बहुत बडी लहर ने उलट दिया था . [self]
(defrule swamp2
(declare (salience 1000))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 boat|ship|cruise)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulata_xe/palata_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp2   "  ?id "  ulata_xe/palata_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;Foreign cars have swamped the UK market.[cald]
;यूके बाजार विदेशी गाडियों से भर चुका है .  [self]
(defrule swamp3
(declare (salience 1000))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 market|city|town|ground)
(not(kriyA-by_saMbanXI ?id ?))	;Passive construct
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  swamp.clp 	swamp3    "  ?id " se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp3   "  ?id "  Bara)" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;In summer visitors swamp the island.[oald]
;ग्रीष्म में दर्शकों से द्वीप भरता है .  [self]
(defrule swamp4
(declare (salience 1000))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 visitor)
(not(kriyA-by_saMbanXI ?id ?))	;Passive construct
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara_jA/AplAviwa_ho_jA))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  swamp.clp 	swamp4    "  ?id " se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp4   "  ?id "  Bara_jA/AplAviwa_ho_jA)" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;Radio stations have been swamped with requests to play the song. [oald]
;ट्राँसमीटर स्टेशन गाना बजाने के अनुरोधों से सराबोर हो गये हैं . [self]
(defrule swamp5
(declare (salience 1000))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarAbora_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp5   "  ?id "  sarAbora_ho)" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;Don't let feelings of depression swamp you.  [cald]
;निराशा/विषाद की भावनाओं को आपको अभिभूत मत करने दीजिये .  [self]
(defrule swamp6
(declare (salience 1000))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 feeling)
(kriyA-subject  ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBiBUwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  swamp.clp 	swamp6    "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp6   "  ?id "  aBiBUwa_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;The Everglades are an area of swamp in southern Florida.[cald]
;एवर्ग्लॆड्ज दक्षिणी फलोराइडा में  दलदल का  एक क्षेत्र है. [self]
(defrule swamp0
(declare (salience 0))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xalaxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp    swamp0   "  ?id "  xalaxala)" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-04-2015) 
;This is a big task and I am finding myself swamped.[oald]          --Parse no. 31
;यह एक बडा काम है और मैं स्वयं को अभिलुप्त/अभिभूत  पा रहा हूँ . [self]
(defrule swamp7
(declare (salience 0))
(id-root ?id swamp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBilupwa/aBiBUwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swamp.clp 	swamp7   "  ?id "  aBilupwa/aBiBUwa)" crlf))
)
