
;@@@ Added by 14anu-ban-05 on (22-04-2015)
;This formula is used to calculate the area of a circle.	[OALD]
;यह फार्मूला एक वृत्त के क्षेत्रफल की गणना करने के लिए प्रयोग किया जाता है.		[MANUAL]
(defrule formula0
(declare (salience 100))
(id-root ?id formula)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PArmUlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formula.clp 	formula0   "  ?id "  PArmUlA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;All the patients were interviewed according to a standard formula.	[OALD]
;सभी मरीजों का मानक सूत्र के अनुसार इंटरव्यू लिया गया था .		[MANUAL]
(defrule formula1
(declare (salience 101))
(id-root ?id formula)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formula.clp 	formula1   "  ?id "  sUwra )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;They're trying to work out a peace formula acceptable to both sides in the dispute. [OALD]
;वे विवाद में दोनों पक्षों को स्वीकार्य अमन सूत्र पर काम करने की कोशिश कर रहे हैं.			[MANUAL]
(defrule formula2
(declare (salience 101))
(id-root ?id formula)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formula.clp 	formula2   "  ?id "  sUwra )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;He knows the secret formula for the blending of the whisky.	[OALD]
;वह मदिरा के सम्मिश्रण  के लिए गुप्त  विधि जानता है . 				[MANUAL]
(defrule formula3
(declare (salience 102))
(id-root ?id formula)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 blend|make|resolve|deliver)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formula.clp 	formula3   "  ?id "  viXi )" crlf))
)


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;The minister keeps coming out with the same tired formulas.[OALD]
;मन्त्री  वही पुराने फार्मूले के साथ आना जारी रखता है .			[MANUAL]
(defrule formula4
(declare (salience 104))
(id-root ?id formula)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) tired|old)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) purAnA_PArmUlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " formula.clp  formula4  "  ?id "  " (- ?id 1) "  purAnA_PArmUlA  )" crlf))
)
