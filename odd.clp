
;---------------------------------DEFAULT RULE--------------------------------------------------------------------------

(defrule odd0
;(declare (salience 5000)) ;commented by 14anu-ban-09
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ajIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odd.clp 	odd0   "  ?id "  ajIba )" crlf))
)

;@@@ Added by 14anu02 on 18.06.14
;The odds are that he is no longer alive.
;संभावना हैं कि वह अब नहीं जीवित है . 
(defrule odd2
(declare (salience 3450))  
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odd.clp 	odd2   "  ?id "  samBAvanA )" crlf))
)

;----------------------------------------------------------------------------------------------------------------------------------

;$$$ Modified by 14anu-ban-09 on 04-08-2014
;We have the odd situation where banks are also constraining credit right now. [COCA]
;hama eka viRama parisWiwi hE jahAz bEzka PilahAla jamA rASi para prawibaMXa lagAwA hE. [Own Manual]

(defrule odd1
(declare (salience 3000))  ;Salience reduced by 14anu-ban-09 "4900" to "3000"
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
(viSeRya-viSeRaNa  ?id1 ?id) ;added by 14anu-ban-09
(id-root ?id1 situation) ;added by 14anu-ban-09 (NOTE- More constraints can be added.)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odd.clp 	odd1   "  ?id "  viRama )" crlf))
)

;"odd","Adj","1.viRama"
;Three is an odd number.
;



;$$$ Modified by 14anu-ban-09 on 30-08-2014
;Changed meaning from "bAXAe" to "bAXA"
;@@@ Added by 14anu02 on 18.06.14
;She bravely fought against all odds.
;उसने बहादुरी से सब बाधाओ के विरुद्ध लडाई की . 
(defrule odd3
(declare (salience 3500))
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-against_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odd.clp 	odd3   "  ?id "  bAXA )" crlf))
)

;@@@ Added by 14anu-ban-09 on 04-09-2014
;Against all odds, he recovered. [CALD]
;saBI viRama ke virUxXa, vaha TIka ho gayA. [Own Manual]

(defrule odd4
(declare (salience 3550))
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-against_saMbanXI ?id1 ?id)
(id-root ?id1 recover)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odd.clp 	odd4   "  ?id "  viRama )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (24-01-2015)
;### [COUNTER EXAPLE] ### They're very odd people.	[OALD]
;### [COUNTER EXAPLE] ### वे बडे अजीब लोग हैं.		[Self]
;@@@ Addedby 14anu20 on 25/06/2014
;You are wearing odd socks.
;आप भिन्न जोडी के मोजे पहन रहे हैं. 
(defrule odd5
(declare (salience 5100))
(id-root ?id odd)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(id-cat_coarse =(+ ?id 1) noun) ;commented by 14anu-ban-09 on (24-01-2015)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 sock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Binna_jodI))		;modified meaning 'Binna_jodI_ke' to 'Binna_jodI' by 14anu-ban-09 on (24-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " odd.clp odd5 " ?id "  Binna_jodI )" crlf)) 
)							;modified meaning 'Binna_jodI_ke' to 'Binna_jodI' by 14anu-ban-09 on (24-01-2015)

;Removed by 14anu-ban-09 on (24-01-2015) 
;NOTE- The meaning and relations both are not correct.
;### [COUNTER EXAMPLE] ### She had the oddest feeling that he was avoiding her.	[OALD]
;### [COUNTER EXAMPLE] ### उसे बडा अजीब अहसास हुआ चूँकि वह उसे टाल रहा था.		[Self]
;@@@ Added by 14anu20 on 25/06/2014.
;He makes the odd mistake.
;वह कभी कभी गलती करता है 
;(defrule odd6
;(declare (salience 5200))
;(id-root ?id odd)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;(id-word =(- ?id 1) the)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) kaBI_kaBI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " odd.clp	odd6  "  ?id "  " (- ?id 1)"  kaBI_kaBI  )" crlf))
;)

