;@@@ Added by 14anu-ban-10 on (20-02-2015)
;She repaid the compliment with a smile.[hinkhoj]
;उसने एक मुस्कान के साथ  तारीफ लौटाई।[manual]
(defrule repay1
(declare (salience 200))
(id-root ?id repay)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1 )
(id-root ?id1 compliment)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repay.clp 	repay1   "  ?id "  lOtA)" crlf))
)
;@@@ Added by 14anu-ban-10 on (20-02-2015)
;We can never repay you for your kindness shown to us.[hinkhoj]
 ;हमें दिखाया अपकी मेहरबानी  का  हम  कभी    मुआवज़ा चुका  नहीं  सकते हैं .[manual]
(defrule repay2
(declare (salience 300))
(id-root ?id repay)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?  )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muAvajA_cukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repay.clp 	repay2   "  ?id "  muAvajA_cukA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (20-02-2015)
;If you lend me some money,I will repay you next month.[hinkhoj]
;अगर आप मुझे कुछ पैसे उधार दे , तो मैं अगले महीने आपको चुका दुगा।[manual]
(defrule repay0
(declare (salience 100))
(id-root ?id repay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cukA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repay.clp    repay0   "  ?id "  cukA_xe)" crlf))
)

