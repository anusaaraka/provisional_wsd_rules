;@@@ Added by 14anu-ban-01 on (12-02-2015)
;Saffron is a yellowish-orange powder .[self: with reference to collins dictionary]
;केसर पीला-नारंगी पाउडर/चूरा होता है.[self]
(defrule saffron0
(declare (salience 0))
(id-root ?id saffron)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kesara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  saffron.clp  	saffron0  "  ?id " kesara)" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-02-2015)
;I like saffron rice.[self: with reference to cald]
;मुझे केसरिया चावल पसन्द है.[self]
(defrule saffron1
(declare (salience 0))
(id-word ?id saffron)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kesariyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  saffron.clp  	saffron1  "  ?id " kesariyA)" crlf))
)
