
(defrule test0
(declare (salience 5000))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parIkRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test0   "  ?id "  parIkRA )" crlf))
)

;"test","N","1.parIkRA"
;The space programme was a test for our abilities.
;
(defrule test1
(declare (salience 4900))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test1   "  ?id "  jAzca_kara )" crlf))
)

;"test","VT","1.jAzca_karanA"
;Mohan was tested && interviewed for the officer's post.
;

;@@@ Added by Prachi Rathore 7-1-14
;They played well in the first Test against South Africa. 
(defrule test2
(declare (salience 5500))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 play|win)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id testa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test2   "  ?id "  testa )" crlf))
)

;@@@ Added by Prachi Rathore 7-1-14
;The local elections will be a good test of the government's popularity.
(defrule test3
(declare (salience 5000))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI  ?id ?)(viSeRya-viSeRaNa   ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parIkRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test3   "  ?id "  parIkRaNa )" crlf))
)


;What has happened to Laxman since then , all the way to the second Test against England is this : 28 , 38 , 15 , 20 
;उस 281 रन की पारी के बाद लक्ष्मण के रूप में एक विश्वास बन आया था कि अंततः भारत को ऐसा बल्लेबाज मिल गया है जो आक्रमण से नहीं घबराता और सचिन तेंडुलकर वाली टीम में मैच 
;जिताने वाले का बिल्ल लगाने को तैयार है .
;@@@ Added by 14anu11
(defrule test4
(declare (salience 6000))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI  ?id ?)(viSeRya-viSeRaNa   ?id ?))
(viSeRya-det_viSeRaNa  ?id ?)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id testa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test4   "  ?id "  testa )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-03-2015)
;The students tested out their cost-cutting ideas in several companies.(cambridge)
;विद्यार्थियों ने कई कम्पनियों में उनके खर्च में कमी के विचार की जाँच किए . [manaul]
(defrule test5
(declare (salience 5000))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(assert (id-wsd_viBakwi ?id2 kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " test.clp	test5  "  ?id "  " ?id1 "  jAzca_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  test.clp test5  "  ?id2 " kI)" crlf)
)
)

;@@@ Added by Bhagyashri Kulkarni (16-09-2016)
;H.I.V. test is easy, safe and available at public health centres. (health)
;एच.आय.वी जाँच आसान, सुरक्षित और जनता स्वास्थ्य केंद्रों में उपलब्ध है . 
(defrule test6
(declare (salience 5000))
(id-root ?id test)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id  ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  test.clp 	test6   "  ?id "  jAzca )" crlf))
)
