;@@@ Added by 14anu13 on 28-06-14
;He has an extensive collection of Greek antiquities.     [sentence from http://www.oxforddictionaries.com]
;उसके पास ग्रीक पुरावस्तुओं का व्यापक संग्रह है |
(defrule antiquity0
(declare (salience 1000))
(id-root ?id antiquity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAvaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  antiquity.clp 	antiquity0   "  ?id "  purAvaswu )" crlf))
)

;$$$Modified by 14anu-ban-02(08-01-2015)
;@@@ Added by 14anu13 on 28-06-14
;The kings of antiquity , who were industriously devoted to the duties of their office , spent most of their care on the division of their subjects into different classes and orders , which they tried to preserve from intermixture and disorder .
;प्राचीन काल के राजा बडे अध्यवसाय के साथ अपने कर्तव्यों के निर्वाह में लगे रहते थे और अपना अधिकांश ध्यान अपनी प्रजा के विभिन्न वर्गों और व्यवस्थाओं में विभाजन पर केंद्रित करते थे और इस बात का प्रयास करते थे कि वे आपस में न मिल पाएं और उनमें अव्यवस्था पैदा न हो .
(defrule antiquity1
(declare (salience 4000))
(id-root ?id antiquity)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 king);added by 14anu-ban-02(08-01-2015) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAcIna_kAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  antiquity.clp 	antiquity1   "  ?id "  prAcIna_kAla )" crlf))
)
