
(defrule stop0
(declare (salience 5000))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) ing_clause)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop0   "  ?id "  roka )" crlf))
)


; He did not wish to stop playing
(defrule stop2
(declare (salience 4800))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop2   "  ?id "  roka )" crlf))
)


;Modified by Meena(19.7.11)
;When the dollar is in a free-fall, even central banks can not stop it. 
(defrule stop4
(declare (salience 5000))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?subj)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop4   "  ?id "  roka )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stop.clp      stop4   "  ?id "  ko )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (25-03-2015) 
;Stop belittling yourself - your work is highly valued. [cald]
;तुम खुद को छोटा समझना बन्द करो- तुम्हारा कार्य अत्यन्त महत्वपूर्ण है . [self]
(defrule stop7
(declare (salience 4700))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_kriyA  ?id)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-root-category-suffix-number ?id1 ? verb ing ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop7   "  ?id "  banxa_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (25-03-2015) 
;A sentence should always end with a full stop.[stop.clp]
;वाक्य को हमेशा एक पूर्ण विराम के साथ समाप्त होना चाहिए . [self]
(defrule stop8
(declare (salience 4700))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 full)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrNa_virAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   stop.clp 	stop8   "  ?id "  " ?id1 "  pUrNa_virAma  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (25-03-2015) 
;The car came to a full stop.[coca]
;गाडी पूर्ण विराम की स्थिति/अवस्था में आई . [self:more faithful]
;गाडी  पूरी तरह से रुक गई . [self:more natural]
(defrule stop9
(declare (salience 4700))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 full)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id2 come|reach|brake)
(kriyA-to_saMbanXI  ?id2 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrNa_virAma_kI_sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   stop.clp 	stop9   "  ?id "  " ?id1 "  pUrNa_virAma_kI_sWiwi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (25-03-2015) 
;They waited until they found a fairly large gap between the rocks and ordered a full stop.[coca]
;उन्होंने तब तक प्रतीक्षा की जब तक उनको चट्टानों के बीच एक पर्याप्त फ़ासला नहीं मिला और फिर विराम/ठहराव का आदेश दिया . [self]
(defrule stop10
(declare (salience 4700))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 full)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id2 order|tell)
(kriyA-object  ?id2 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 virAma/TaharAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   stop.clp 	stop10   "  ?id "  " ?id1 " virAma/TaharAva )" crlf))
)


;Commented by 14anu-ban-01 on (12-01-2015) because correct meaning is coming from stop06 and this rule has condition similar to (kriyA-kqxanwa_karma  ?id ?id1).
;@@@ Added by 14anu17
;Lightening A couplet in the Rigveda says O Rudra ! please ask your thunderbolt , which you have released in the skies , to stop chasing us upon our earth .
;(defrule stop6
;(declare (salience 4800))
;(id-root ?id stop)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(id-cat_coarse =(+ ?id 1) verb)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id banx_kare))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  stop.clp 	stop6   "  ?id "  banx_kare )" crlf))
;)

;@@@ Added by 14anu21 on 07.07.2014
;Rama did not stop crying. [self]
;राम रोना नहीं रुका था . [Translation before adding rule]
;राम ने रोना बन्द नहीं किया था . 
(defrule stop06
(declare (salience 5500))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-root-category-suffix-number ?id1 ? verb ing ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop06   "  ?id "  baMxa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;Salience reduced by Meena(19.7.11)
(defrule stop1
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop1   "  ?id "  ruka )" crlf))
)

(defrule stop3
(declare (salience 4700))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop3   "  ?id "  ruka )" crlf))
)

;default_sense && category=noun	virAma	0
;"stop","N","1.virAma"
;A sentence should always end with a full stop.
(defrule stop5
(declare (salience 4500))
(id-root ?id stop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stop.clp 	stop5   "  ?id "  virAma )" crlf))
)

