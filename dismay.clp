
(defrule dismay0
(declare (salience 4000))
(id-root ?id dismay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wraswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismay.clp 	dismay0   "  ?id "  wraswa )" crlf))
)

;"dismay","N","1.wraswa"
;She expressed her dismay at his failure to secure top rank.
;
(defrule dismay1
(declare (salience 4900))
(id-root ?id dismay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirASa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismay.clp 	dismay1   "  ?id "  nirASa_kara )" crlf))
)

;"dismay","VT","1.nirASa_karanA"
;We were dismayed at Indian teams poor performance.
;

;@@@ Added by Pramila(banasthali University) on 22-01-2014
;The crowd threw up their hands in dismay.
;भीड ने  निराशा में उनके हाथ उपर उठाए .
(defrule dismay2
(declare (salience 5000))
(id-root ?id dismay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirASA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismay.clp 	dismay2   "  ?id "  nirASA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (13-12-2014) --- meaning changed from 'BayaBIwa_kara' to 'nirASa_ho'
;@@@ Addeed by 14anu26  [18-06-14]
;They were dismayed by the U-turn in policy.
;वे नीति में यू टर्न से भयभीत किए गये थे .
;वे नीति में यू टर्न से निराश हो गये थे.          translation changed by 14anu-ban-04 (13-12-2014) 
(defrule dismay3
(declare (salience 5000))
(id-root ?id dismay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirASa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismay.clp 	dismay3   "  ?id " nirASa_ho)" crlf))
)

;[NOTE] this rule is commented by 14anu-ban-04 (13-12-2014) because the root of 'dismayingly' is not 'dismay'] 
;@@@ Added by 14anu26  [26-06-14]
;Neverthless,his message is still dismayingly mixed.
;तथापि, उसका सन्देश अभी भी निराशा मिश्रित है . 
;(defrule dismay4
;(declare (salience 5000))
;(id-root ?id dismay)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id dismayingly)
;(viSeRya-viSeRaka  ?  ?id)
;(id-cat_coarse ?id adverb)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id  nirASA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  dismay.clp 	dismay4   "  ?id "   nirASA )" crlf))
;)

