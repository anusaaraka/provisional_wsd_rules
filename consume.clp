;$$$ Modified by Manasa.(Arsha sodha sansthan) 14-09-2015
;$$$ Modified by 14anu-ban-03 (17-04-2015)
;The Hotel was consumed by fire.  [same clp file]
;होटल आग से नष्ट हो गया था .  [manual]
(defrule consume1
(declare (salience 4900))
(id-root ?id consume)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 fire|rain|storm|tsunami) ; added by Manasa.(Arsha sodha sansthan) 14-09-2015
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_ho))  ;meaning changed from 'naRta' to 'naRta_ho' by 14anu-ban-03 (17-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consume.clp 	consume1   "  ?id "  naRta_ho )" crlf))
)


;@@@ Added by 14anu-ban-03 (17-04-2015)
;Before he died he had consumed a large quantity of alcohol. [oald]
;मरने से पहले उसने मद्य की अधिक मात्रा पी ली थी .  [manual]
(defrule consume2
(declare (salience 4900))
(id-root ?id consume)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 quantity)
(viSeRya-of_saMbanXI ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pI_le))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consume.clp 	consume2   "  ?id "  pI_le )" crlf))
)


;@@@ Added by 14anu-ban-03 (17-04-2015)
;Carolyn was consumed with guilt. [oald]
;केरलिन पाप से भर गया था .  [manual]
(defrule consume3
(declare (salience 4900))
(id-root ?id consume)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consume.clp 	consume3   "  ?id "  Bara )" crlf))
)

;------------------------------------------Default rule--------------------------------------------

;$$$ Modified by 14anu-ban-03 (17-04-2015)
;The electricity industry consumes large amounts of fossil fuels. [oald]
;विद्युत उद्योग जीवाश्म ईंधनों की अधिक मात्रा उपभोग करता है . [manual]
;;By Ritesh Srivastava
;;This process consumes enormous amounts of energy.
(defrule consume0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (17-04-2015)
(id-root ?id consume)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)'
(assert (id-wsd_root_mng ?id upaBoga_kara))  ;meaning changed from 'Karca' to 'upaBoga_kara' by 14anu-ban-03 (17-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consume.clp 	consume0   "  ?id "  upaBoga_kara )" crlf))
)


