;@@@ Added by 14anu-ban-10 on (11-10-2014)
;Recent developments have, however, indicated that protons and neutrons are built out of still more elementary constituents called quarks.
;[ncert corpus]  
;waWApi, hAla hI meM hue vikAsoM ne yaha sUciwa kiyA hE ki protoYna waWA nyUtroYna Ora BI kahIM aXika mUla avayavoM, jinheM 'kvArka' kahawe hEM, se milakara bane hEM.[ncert corpus]
;तथापि, हाल ही में हुए विकासों ने यह सूचित किया है कि प्रोटॉन तथा न्यूट्रॉन और भी कहीं अधिक मूल अवयवों, जिन्हें 'क्वार्क' कहते हैं, से मिलकर बने हैं.
(defrule recent0
(declare (salience 0000))
(id-root ?id recent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAla_hI_meM_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recent.clp 	recent0   "  ?id "  hAla_hI_meM_huA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-10-2014)
;Recent decades have seen much progress on this front.  [ncert corpus]
;piCale kuCa xaSakoM meM isa kRewra ne bahuwa pragawi xeKI hE.[ncert corpus]
(defrule recent1
(declare (salience 200))
(id-root ?id recent)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 decade)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recent.clp 	recent1  "  ?id "  piCalA)" crlf))
)
