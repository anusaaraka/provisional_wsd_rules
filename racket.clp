
;$$$ Modified by 14anu-ban-05 on (17-04-2015)
;Try adjusting your grip on the racket.[OALD]
;रैकेट  पर पकड को ठीक करने का प्रयास कीजिए . [MANUAL]
;@@@ Added by 14anu-ban-10 on (10-03-2015)
;Why dont you strike the ball with your racket.[hinkhoj]
;आप  क्यों नही गेंद को अपने रैकेट से  मारते हैं  . [manual]
(defrule racket1
(declare (salience 200))
(id-word ?id racket)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-with_saMbanXI  ? ?id)(kriyA-on_saMbanXI  ?kri ?id))	;added 'kriyA-on_saMbanXI' by 14anu-ban-05 on (17-04-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id rEketa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  racket.clp 	racket1   "  ?id "  rEketa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (10-03-2015)
;Stop making that terrible racket.[hinkhoj]
;वह घोर शोर कोलाहल मचाना बनाना बन्द कीजिए .[manual]
(defrule racket2
(declare (salience 300))
(id-word ?id racket)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id  ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Sora_kolAhala_macAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  racket.clp 	racket2   "  ?id " Sora_kolAhala_macAnA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (10-03-2015)
;He is involved in the drugs racket.[hinkhoj]
;वह ड्रग के बेईमानी से कमाया धन में सम्मिलित  है . [manual]
(defrule racket0
(declare (salience 100))
(id-word ?id racket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id beImAnI_se_kamAyA_Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  racket.clp   racket0   "  ?id "  beImAnI_se_kamAyA_Xana)" crlf))
)

