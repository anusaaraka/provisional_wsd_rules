
(defrule nasty0
(declare (salience 5000))
(id-root ?id nasty)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apriya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nasty.clp 	nasty0   "  ?id "  apriya )" crlf))
)

;Commented by 14anu21 on 07.07.2014 by deleting rule nasty1
;Rules nasty1 and nasty0 differ only by salience .
;(defrule nasty1
;(declare (salience 4900))
;(id-root ?id nasty)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat ?id adjective|adjective_comparative|adjective_superlative)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id burA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nasty.clp 	nasty1   "  ?id "  burA )" crlf))
;)

;"nasty","Adj","1.burA/hqxayahIna"
;He passed a nasty remark on her. 
;This is a nasty stain on my shirt.
;He is a nasty boy.
; 
;"natal","Adj","1.janma_saMbaMXI"
;The child had this problem right from the pre-natal stage. 
;
;

;@@@ Added by 14anu21 on 07.07.2014
;She had a nasty brush with her boss this morning.[oxford]
;;उसकी आज सुबह उसके बॉस के साथ एक अप्रिय बुरुश था .
;उसकी आज सुबह उसके बॉस के साथ एक अप्रिय झगडा था . 
;Added rule brush14
;आज सुबह उसका बॉस के साथ बुरी तरह से झगडा हुआ था .
(defrule nasty2
(declare (salience 5010))
(id-root ?id nasty)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 brush)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burI_waraha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nasty.clp 	nasty2   "  ?id "  burI_waraha )" crlf))
)
