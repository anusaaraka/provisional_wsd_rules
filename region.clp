;@@@ Added by 14anu-ban-10 on (09-10-2014)
;After the decline of the guptas , several small kingdoms ruled this hilly state and established their power in its different regions . [parallel corpus] 
;गुप्तों के पतन के बाद , असंख्य छोटे राज्यों ने इस पहाडी राज्य पर शासन किया और इसके विभिन्न प्रदेश में अपनी शक्ति को स्थापित किया . [parallel corpus]
;Settled amidsts five rivers - Ravi  , Jhelum  , Sutlej  , Vyas and Chenab the province of Punjab is extremely green and fertile region  .[tourism corpus];added by 14anu-ban-10 on (05-02-2015)
;पाँच नदियों - रावी , झेलम , सतलज , व्यास और चेनाब के बीच बसा प्रांत पंजाब अत्यंत ही हरा - भरा एवं उपजाऊ प्रदेश है ।[tourism corpus];added by 14anu-ban-10 on (05-02-2015)
(defrule region1
(declare (salience 5000))
(id-root ?id region)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 different|fertile) ;added by 14anu-ban-10 on (27-11-2014);fertile added by 14anu-ban-10 on (05-02-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  region.clp 	region1  "?id "   praxeSa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-02-2015)
;The natural beauty of this region located in the lap of the Himalayas is diverse  , charming and incomparable  .[tourism corpus]
;हिमालय की गोद में स्थित इस प्रदेश का प्राकृतिक सौंदर्य विविध , मनोहारी व अतुलनीय है ।[tourism corpus]
(defrule region2
(declare (salience 5100))
(id-root ?id region)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  region.clp 	region2  "?id "   praxeSa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-02-2015)
;Different types of fauna are found in this region among which black buck  , mountain deer  , beer  , panther  , mynah  , cuckoo  , moal  , kaliz etc . are chief  .[tourism corpus]
;इस प्रदेश में विभिन्न प्रकार के पशु - पक्षी पाए जाते हैं जिनमें काला पहाड़ी हिरण , भालू , तेंदुआ , मैना , कोयल , मोनाल , कालीज़ आदि प्रमुख हैं ।[tourism corpus]
(defrule region3
(declare (salience 5300))
(id-root ?id region)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  region.clp 	region3  "?id "   praxeSa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (07-03-2015)
;Geological studies of basalt containing such pieces of magnetised region have provided evidence for the change of direction of earth's magnetic field, several times in the past.[ncert corpus]
;Ese cumbakIya kRewroM se yukwa asiwASma BaNdAroM ke BUvEjFAnika aXyayanoM se isa bAwa ke pramANa mile hEM ki pqWvI ke cumbakIya kRewra kI xiSA awIwa meM kaI bAra ulata cukI hE.[ncert corpus]
(defrule region4
(declare (salience 5400))
(id-root ?id region)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 piece)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  region.clp 	region4   "?id" kRewra)" crlf))
)

;-------------------- Default Rules -----------------

;@@@ Added by 14anu-ban-10 on (09-10-2014)  
;The components prependicular to this line cancel out when summing over all regions of the shell leaving only a resultant force along the line joining the point to the center. [ncert corpus]
;Kola ke saBI kRewroM ke baloM ke GatakoM kA yoga karawe samaya isa reKA ke lambavaw xiSA ke Gataka niraswa ho jAwe hEM waWA kevala Kola ke kenxra se binxu xravyamAna ko milAne vAlI reKA ke anuxiSa pariNAmI bala bacA rahawA hE. [ncert corpus]
(defrule region0
(declare (salience 000))
(id-root ?id region)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  region.clp 	region0   "?id" kRewra)" crlf))
)

