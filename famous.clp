;@@@ Added by 14anu-ban-05 on (12-01-2015)
;Unwas is famous for the Durga Temple .[TOURISM]
;unavAsa xurgA maMxira ke lie prasixXa hE .[TOURISM]
(defrule famous0
(declare (salience 100))
(id-root ?id famous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasixXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  famous.clp  	famous0   "  ?id "  prasixXa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (12-01-2015)
;The famous Chinese traveller Huen Tsang has mentioned the Allahabad city in his travel memoir .[TOURISM]
;ilAhAbAxa Sahara kA maSahUra cInI yAwrI hvenasAMga ne apane yAwrA vqwAMwa meM jikra kiyA hE .[TOURISM]
(defrule famous1
(declare (salience 100))
(id-root ?id famous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maSahUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  famous.clp  	famous1   "  ?id "  maSahUra )" crlf))
)

