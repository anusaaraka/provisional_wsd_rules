;-------------------------------------------------------DEFAULT RULE------------------------------------------------
;@@@ Added by 14anu-ban-01 on 25-08-2014.
;20370:If it comes up , some of them will be submerged .[Karan Singla]
;यदि यह ऊपर आता है , तो  उनमें से कुछ डूब जायेंगे। [improvised: changed 'कुछ डूब जाती हैं ' to 'उनमें से कुछ डूब जायेंगे ']
;52197:Extending over Gujarat - likely to be its greatest beneficiary - Madhya Pradesh and Rajasthan , it will also displace 40 , 000 families and submerge 245 villages .
;परियोजना से सबसे ज्यादा लभ गुजरात को मिलेगा , जबकि मध्य प्रदेश और राजस्थान भी प्रभावित होंगे . इसके चलते 40 , 000 परिवार विस्थापित होंगे और 245 गांव ड़ूब जाएंगे .
(defrule submerge0
(declare (salience 0))
(id-root ?id submerge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id dUba_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " submerge.clp  	submerge0   "  ?id "   dUba_jA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-02-2015):Run it on parse no. 14
;When an object is submerged in a fluid at rest, the fluid exerts a force on its surface.[NCERT corpus]
;जब कोई पिंड किसी शान्त तरल में डूबा हुआ है, तो तरल उस पिंड पर बल आरोपित करता है[NCERT corpus]
(defrule submerge3
(declare (salience 0))
(id-word ?id submerged)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id dUbA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " submerge.clp  	submerge3   "  ?id "   dUbA_huA)" crlf))
)

;---------------------------------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on 25-08-2014.
;The village got submerged in a couple of seconds.
;The flood submerged the whole village.
(defrule submerge1
(declare (salience 1000))
(id-root ?id submerge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?obj)
(id-cat_coarse ?obj noun)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id dubA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " submerge.clp  	submerge1   "  ?id "   dubA_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on 25-08-2014.
;After this Dwarkapuri was submerged under sea. (dUba_gayA)
;37202:The conventional trade cycle was submerged under the impact of the rising prices all round .
;पारस़्परिक व़्यापार चक्र चारों तरफ बढ़ती कीमतों के प्रभाव में दब कर रह गया .
;Doubts that had been submerged in her mind suddenly resurfaced.
(defrule submerge2
(declare (salience 1000))
(id-root ?id submerge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?obj)
(id-root ?obj doubt|cycle)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id dabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " submerge.clp  	submerge2   "  ?id "   dabA)" crlf))
)

