;@@@Added by 14anu-ban-08 (31-03-2015)     
;He nibbled at the idea, but would not make a definite decision.  [oald]
;उसने सुझाव में दिलचस्पी दिखायी पर उसने कोई निश्चित निर्णय नहीं लिया.  [self]
(defrule nibble2
(declare (salience 200))
(id-root ?id nibble)
?mng <-(meaning_to_be_decided ?id)  
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI ?id ?id1)
(id-root ?id1 idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilacaspI_xiKA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nibble.clp 	nibble2   "  ?id "  xilacaspI_xiKA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-08 (31-03-2015)
;I took a nibble from the biscuit.  [oald]
;मैनें बिस्कुट का एक टुकड़ा लिया.  [self]
(defrule nibble0
(declare (salience 0))
(id-root ?id nibble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nibble.clp   nibble0   "  ?id "  tukadZA )" crlf))
)

;@@@Added by 14anu-ban-08 (31-03-2015)     ;Run on parser 122
;We sat drinking wine and nibbling olives.  [oald]
;हमनें बैठकर जैतून का छोटा कौर खाया और वाइन पी.  [self]
(defrule nibble1
(declare (salience 100))
(id-root ?id nibble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA_kOra_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nibble.clp   nibble1   "  ?id "  CotA_kOra_KA )" crlf))
)

