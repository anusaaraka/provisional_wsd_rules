;@@@ Added by 14anu-ban-04 (13-04-2015)
;A murder enquiry.                        [oald]             ;run on parse no. 7
;हत्या की जाँच.                               [self]
(defrule enquiry1
(declare (salience 20))
(id-root ?id enquiry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " enquiry.clp 	enquiry1   "  ?id "  jAzca)" crlf)
)
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (13-04-2015)
;Ask at enquiries to see if your bag has been handed in.                       [oald]
;पूछताछ पर पूछिए  यह देखने के लिए कि आपका थैला  दिया गया है या नहीं  .                              [self]
(defrule enquiry0
(declare (salience 10))
(id-root ?id enquiry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUCawACa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " enquiry.clp 	enquiry0   "  ?id "  pUCawACa)" crlf)
)
)
