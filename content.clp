;@@@ Added by 14anu23 on 27/6/14
;Its sugar content is slightly less and ash content more than in human milk . 
;बकरी के दूध में शर्करा की मत्रा कुछ कम और राख  की मत्रा मनुष्य के दूध से कुछ अधिक होता है . 
(defrule content3
(declare (salience 4900))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id  mawrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  content.clp  	content3   "  ?id " mawrA )" crlf))
)

;@@@ Added by 14anu23 on 27/6/14
;The content of the course depends on what the students would like to study.
;पाठ्यक्रम की विषय वस्तु पर निर्भर करती है विद्यार्थी जिसे अध्ययन करना पसन्द करेंगे . 
(defrule content4
(declare (salience 4901))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 course|book|novel|publication|journal|article|essay|column)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRaya_vaswu)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  content.clp 	content4   "  ?id "  viRaya_vaswu )" crlf))
)

;@@@ Added by 14anu23 on 27/6/14
; A table of contents.
;विषय सूचि. 
(defrule content5
(declare (salience 4901))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-word ?id1 table)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viRaya_sUcI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " content.clp	content5  "  ?id "  " ?id1 "  viRaya_sUcI  )" crlf))
)

;@@@ Added by 14anu23 on 27/6/14
;Her poetry has a good deal of political content.
;उसकी काव्य में राजनैतिक विषयों का एक अच्छा समावेश है .
(defrule content6
(declare (salience 4901))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 political|romantic|unique)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viRaya ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  content.clp  	content6   "  ?id "  viRaya )" crlf))
)

;@@@ Added by 14anu23 on 27/6/14
;She was content to step down after four years as chief executive. 
;वह चार वर्षों के बाद मुख्य प्रशासक का पद छोङ कर सन्तुष्ट थी . 
(defrule content7
(declare (salience 4900))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(saMjFA-to_kqxanwa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sanwuRta )) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  content.clp  	content7   "  ?id "  sanwuRta )" crlf))
)

;@@@ Added by 14anu18 (16-06-14)
;What are the contents of the menu.
;मेनू की सामग्री क्या हैं .
(defrule content8
(declare (salience 4800))
(id-root ?id content)
(id-word ?id contents)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmagrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  content.clp 	content8   "  ?id "  sAmagrI )" crlf))
)

;@@@ Added by 14anu-ban-03 (15-12-2014)
;Its sugar content is slightly less and ash content more than in human milk . [same clp file]
;बकरी के दूध में शर्करा की मत्रा कुछ कम और राख  की मत्रा मनुष्य के दूध से कुछ अधिक होता है .
(defrule content9
(declare (salience 4900))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 less)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id  mawrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  content.clp  	content9   "  ?id " mawrA )" crlf))
)

;--------------------- Default Rules -----------------------
(defrule content0
(declare (salience 5000))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id contented )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMwqRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  content.clp  	content0   "  ?id "  saMwqRta )" crlf))
)

;"contented","Adj","1.saMwqRta/wqpwa"
;She is leading a contented life.
;Are you content with your present job?
;
(defrule content1
(declare (salience 00))  ;reduced by 14anu-ban-03 (15-12-2014)
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwoRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  content.clp 	content1   "  ?id "  sanwoRa )" crlf))
)

;"content","N","1.sanwoRa"
;He is living in peace && content.
;--"2.nihiwa_vaswu"
;He emptied the contents of his pockets
;Food with high calorie fat content is not good for health.
;--"3.viRaya-vaswu"
;She didn't read the article that is why she is unaware of its contents.
;The content of your essay is excellent, but it is not well written.
;
(defrule content2
(declare (salience 4800))
(id-root ?id content)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwuRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  content.clp 	content2   "  ?id "  sanwuRta_kara )" crlf))
)

;"content","VT","1.sanwuRta_karanA"
;These worldly things do not content me.
;There is no butter today,so we must content ourselves with dry bread.
;
