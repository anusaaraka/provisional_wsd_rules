;@@@ Added by 14anu-ban-06 (06-04-2015)
;An indifferent meal.(OALD)
;साधारण भोजन . (manual)
;We didn't like the restaurant much - the food was indifferent and the service rather slow.(cambridge)
;हमें भोजनालय बहुत पसन्द नहीं आया था- आहार साधारण था और सेवा काफी धीमे थी . (manual)
(defrule indifferent1
(declare (salience 2000))
(id-root ?id indifferent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(subject-subject_samAnAXikaraNa ?id1 ?id))
(id-root ?id1 food|meal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indifferent.clp 	indifferent1   "  ?id "  sAXAraNa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (06-04-2015)
;The government cannot afford to be indifferent to public opinion.(OALD)
;सरकार लोकमत के लिए तटस्थ रहना बर्दाश्त नहीं कर सकती है . (manual)
(defrule indifferent0
(declare (salience 0))
(id-root ?id indifferent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id watasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indifferent.clp 	indifferent0   "  ?id "  watasWa )" crlf))
)
