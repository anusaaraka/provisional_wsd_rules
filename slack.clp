
;@@@ Added by 14anu-ban-11 on (19-02-2015)
;Note:-- Working properly on parser no 5.
;Wednesdays are always slack.(oald)
;बुधवार हमेशा मन्द हैं . (anusaaraka)
(defrule slack1
(declare (salience 20))
(id-root ?id slack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 wednesday|monday|friday|tuesday|thursday|saturday)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slack.clp 	 slack1   "  ?id "  manxa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (19-02-2015)
;He's been very slack in his work lately.  (oald)
;वह हाल में अपने  कार्य में अत्यन्त लापरवाह  हो गया है। (self)
(defrule  slack2
(declare (salience 30))
(id-root ?id  slack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAparavAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   slack.clp 	 slack2   "  ?id "  lAparavAha )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (19-02-2015)  ;Note:-- Working properly on parser no 6.
;The rope suddenly went slack.(oald)
;रस्सी अचानक ढीला पड गई . (anusaaraka)
(defrule slack0
(declare (salience 00))
(id-root ?id  slack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlA_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slack.clp 	slack0   "  ?id "  DIlA_padZa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (19-02-2015)
;Ramesh bought slacks for Mahesh on his birthday.(hinkhoj)
;रमेश ने उसके जन्मदिन पर महेश के लिए ढीली पैंट खरीदी . (self)
(defrule  slack3
(declare (salience 00))
(id-root ?id  slack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlI_pEMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   slack.clp 	 slack3   "  ?id "  DIlI_pEMta )" crlf))
)

