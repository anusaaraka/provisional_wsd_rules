
;@@@ Added by 14anu-ban-11 on (15-04-2015)
;I scratch my nose to cover my wince.(coca)
;मैं झिझक छिपाने के लिए अपनी नाक खुरचता हूँ . (self)
(defrule wince1
(declare (salience 10))
(id-root ?id wince)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id2 ?id1)
(id-root ?id1 cover)
(id-root ?id2 nose)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JiJaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wince.clp      wince1   "  ?id "  JiJaka)" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;I still wince when I think about that stupid thing I said.(coca)
;मैं अभी भी झिझक जाता हूँ जब मैं उस मूर्खता के बारे में सोचता हूँ  जिसे मैंने कहा .(self) 
(defrule wince3
(declare (salience 10))
(id-root ?id wince)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id ?id1)
(id-root ?id1 think)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JiJaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wince.clp      wince3   "  ?id "  JiJaka_jA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;I managed not to wince. (coca)
;मैं नहीं घबराने मे सफल हुआ . (self)
(defrule wince4
(declare (salience 10))
(id-root ?id wince)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(id-root ?id1 manage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GabarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wince.clp      wince4   "  ?id "  GabarA)" crlf))
)


;------------------------------ Default Rules -------------------------------------------

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;She gave a wince as the nurse put the needle in.(oald)
;उसने दर्द की शिकन दिया जब परिचारिका ने सुई लगायी . (self)
(defrule wince0
(declare (salience 00))
(id-root ?id wince)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarxa_kI_Sikana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wince.clp      wince0   "  ?id "  xarxa_kI_Sikana)" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;He winced inwardly at her harsh tone.(coca)
;वह उसके कटु लहजे से मन-ही-मन दुखी हुआ .(self) 
(defrule wince2
(declare (salience 00))
(id-root ?id wince)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuKI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wince.clp      wince2   "  ?id "  xuKI_ho)" crlf))
)

