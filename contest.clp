;@@@ Added by 14anu-ban-03 (22-04-2015)
;A contest for the leadership of the party. [oald]
;पार्टी के नेतृत्व के लिए सङ्घर्ष .  [manual]
(defrule contest2
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest2   "  ?id "  saMGarRa )" crlf))
)


;@@@ Added by 14anu-ban-03 (22-04-2015)
;His step brother contested for his right in the property. [same clp]
;उसके सौतेला भाई ने सम्पत्ति में अपने अधिकार के लिए बहस की.  [manual]
(defrule contest3
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest3   "  ?id "  bahasa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (22-04-2015)
;The divorce was not contested. [oald]  ;working on parser no.- 3
;तलाक को अमान्य सिद्ध करने का प्रयत्न नहीं किया था .  [manual]
(defrule contest4
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 divorce)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amAnya_sixXa_karane_kA_prayawna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest4   "  ?id "  amAnya_sixXa_karane_kA_prayawna_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (22-04-2015)
;Three candidates contested for the leadership. [OALD]
;तीन उम्मीदवारों ने नेतृत्व के लिए सङ्घर्ष किया . [manual]
(defrule contest5
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 candidate)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest5   "  ?id "  saMGarRa_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (22-04-2015)
;The party has decided not to contest this election. [oald]
;पार्टी ने  यह चुनाव नहीं लडने का फैसला किया है .  [manual]
(defrule contest6
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 election)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest6   "  ?id "  lada )" crlf))
)

;@@@ Added by 14anu-ban-03 (22-04-2015)
;The contest between rivals to win the game. [self with refrence to oald]
;खेल जीतने के लिए प्रतिद्वन्दियों के बीच होड . [manual]
(defrule contest7
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hoda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest7  "  ?id "  hoda )" crlf))
)


;@@@ Added by 14anu-ban-03 (22-04-2015)
;After some contest a decision was finally taken. [self with refrence to oald]
;कुछ विवाद के बाद अन्ततः निर्णय ले लिया गया था . [manual]
(defrule contest8
(declare (salience 5000))
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-after_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest8  "  ?id "  vivAxa )" crlf))
)


;-------------------------------------------------------Default rules----------------------------------------------

(defrule contest0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (22-04-2015)
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiyogiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest0   "  ?id "  prawiyogiwA )" crlf))
)

;"contest","N","1.prawiyogiwA"
;Her daughter has won the dance contest.
;--"2.saMGarRa"
;There was a tough contest for the leadership.
;
(defrule contest1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (22-04-2015)
(id-root ?id contest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukAbalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contest.clp 	contest1   "  ?id "  mukAbalA_kara )" crlf))
)

;"contest","VT","1.mukAbalA_karanA"
;They contested for the post of speaker in the parliament.
;--"2.warka_karanA"
;His step brother contested for his right in the property.
;
