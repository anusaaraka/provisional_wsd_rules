;### Salience decreased by 14anu5
;Eg : There was one bathroom with a shower stall in the corner.
;Anusaaraka : वहाँ एक स्नानघर था जिसके कोने में एक वर्षा स्टाल था .
;Man : वहाँ एक स्नानघर था जिसके कोने में एक फुहारा स्टाल था .
(defrule shower0
(declare (salience 3500))
(id-root ?id shower)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shower.clp 	shower0   "  ?id "  varRA )" crlf))
)

;"shower","N","1.varRA"
;There was a heavy shower of rain in the afternoon.
;--"2.PuhArA"
;The gardener was watering the plants by a showering cane.
;
(defrule shower1
(declare (salience 4900))
(id-root ?id shower)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shower.clp 	shower1   "  ?id "  nahA )" crlf))
)

;"shower","V","1.nahAnA{PuhAra_meM}"
;She showered, changed && went out.
;--"2.barasanA"
;Ash from the volcano showered on the near by villages.
;

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 26.06.14
;There was one bathroom with a shower stall in the corner.
;वहाँ एक स्नानघर था जिसके कोने में एक फुहारा स्टाल था . 
;वहाँ एक स्नानघर था जिसके कोने में एक शॉवर-स्टॉल था .[Translation improved by 14anu-ban-01 on (30-12-2014)]   
(defrule shower2
(declare (salience 4500))
(id-root ?id shower)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 stall)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id PuhArA)) ;commented by 14anu-ban-01 on (30-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SoYvara_stoYla)) ;added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  shower.clp     shower2   "  ?id " "  ?id1 " SoYvara_stoYla )" crlf))
);added by 14anu-ban-01 on (30-12-2014)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu21 on 25.06.2014
;Volcanic ash showered down on the town after the eruption.
;उदभेदन के बाद  शहर पर ज्वालामुखीय राख की बौछार हुई.
;ज्वालामुखीय राख ने उदभेदन के बाद नगर पर कम नहाया . (Translation before adding rule)
(defrule shower3
(declare (salience 4910))
(id-root ?id shower)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 down)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject  ?id ?idsub)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bOCAra_ho))
(assert (kriyA_id-subject_viBakwi ?id kA));added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  shower.clp     shower3    "  ?id " kA )" crlf);added by 14anu-ban-01 on (30-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  shower.clp     shower3   "  ?id " "  ?id1 " bOCAra_ho )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu21 on 25.06.2014
;They showered rice on the bride.
;उन्होंने नववधू पर चावल नहाया . (Translation before adding rule)
;उन्होंने नववधू पर चावल बौछार की . 
;उन्होंने नववधू पर चावल की बौछार की . [Translation improved by 14anu-ban-01 on (30-12-2014)] 
(defrule shower4
(declare (salience 4900))
(id-root ?id shower)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-on_saMbanXI  ?id ?id1)(kriyA-with_saMbanXI  ?id ?id1))
(kriyA-subject  ?id ?idsub)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bOCAra_kara))
(assert (kriyA_id-object_viBakwi ?id kA));added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  shower.clp 	shower4   "  ?id " kA )" crlf);added by 14anu-ban-01 on (30-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shower.clp 	shower4   "  ?id "  bOCAra_kara )" crlf))
)

