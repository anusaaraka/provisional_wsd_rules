
(defrule snug0
(declare (salience 5000))
(id-root ?id snug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snug.clp 	snug0   "  ?id "  suKaxa )" crlf))
)

;"snug","Adj","1.suKaxa/ArAmaxeha"
;I felt warm && snug in bed.
;--"2.xuruswa/cuswa"
;The jacket's a bit snug across the shoulders.
;
(defrule snug1
(declare (salience 4900))
(id-root ?id snug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id susaMhawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snug.clp 	snug1   "  ?id "  susaMhawa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (07-01-2015)
;;@@@ Added by 14anu23 18/06/2014
;A snug jacket.
;एक  चुस्त जैकेट
;एक आरामदेह जैकेट[Translation improved by 14anu-ban-01 on (07-01-2015)]
(defrule snug2
(declare (salience 5100));salience increased by 14anu-ban-01
(id-root ?id snug)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)	commented by 14anu-ban-01
(viSeRya-viSeRaNa ?id1 ?id)	;interchanged ids by 14anu-ban-01
(id-root ?id1 jacket|hood|jeans|denim|home|house|room);added by 14anu-ban-01
; jacket | pant | shirt | jeans | apparel | tshirt | laggings | skirt | tops) Commented this by Roja. Need to improve this rule 22-11-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAmaxeha));changed "cuswa" to "ArAmaxeha" by 14anu-ban-01 on (07-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snug.clp 	snug2   "  ?id "  ArAmaxeha )" crlf));changed "cuswa" to "ArAmaxeha" by 14anu-ban-01 on (07-01-2015)
)


;"snug","N","1.susaMhawa"
;She was warm && snug in her blanket.
;
