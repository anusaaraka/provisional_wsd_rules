;$$$ Modified by 14anu-ban-06 (21-03-2015)
;He hails from America.(OALD)[parser no. - 43]
;vaha amarIkA se saMbaMXiwa hE.(manual)
(defrule hail0
(declare (salience 5000))
(id-root ?id hail)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 from)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saMbaMXiwa_ho))
(assert (kriyA_id-object_viBakwi ?id se));added by 14anu-ban-06 (21-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hail.clp	hail0  "  ?id "  " ?id1 "  saMbaMXiwa_ho  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hail.clp       hail0   "  ?id " se )" crlf));added by 14anu-ban-06 (21-03-2015)
)

;He hails from America.
;vaha amarIkA se saMbaMXiwa hE


;$$$ Modified by 14anu-ban-06 (10-12-2014)
;@@@ Added by 14anu05 on 20.06.14
;Bansal is being hailed for saving a young child from drowning.
;बांसल की एक तरुण बच्चे को डूबने से बचाने के लिए जयजयकार की जा रही है . 
(defrule hail3
(declare (salience 5000))
(id-root ?id hail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb);added by 14anu-ban-06 (10-12-2014)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id1 PropN);modified 'id-cat_coarse' of ?id1 from 'noun' to 'PropN' by 14anu-ban-06 (10-12-2014) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jayajayakAra_kara));meaning changed from 'jayajayakAra_karanA' to 'jayajayakAra_kara' by 14anu-ban-06 (10-12-2014)
(assert (kriyA_id-subject_viBakwi ?id kA));added by 14anu-ban-06 (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hail.clp 	hail3   "  ?id "  jayajayakAra_karanA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hail.clp      hail3   "  ?id " kA )" crlf)
)
)
;@@@ Added by 14anu-ban-06 (21-03-2015)
;The film was hailed as a masterpiece in its day.(cambridge) [parser no.-763]
;फिल्म अपने दिनो में उत्कृष्ट कृति कही जाती थी. (manual)
(defrule hail4
(declare (salience 5300))
(id-root ?id hail)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 as)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kahA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hail.clp	hail4  "  ?id "  " ?id1 "  kahA_jA  )" crlf))
)

;------------------Default Rules ----------------------------
(defrule hail1
(declare (salience 4900))
(id-root ?id hail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id olAvqRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hail.clp 	hail1   "  ?id "  olAvqRti )" crlf))
)

;"hail","N","1.olAvqRti"
;Hail damages crop during winter .
;SIwakAla ke xOrAna hone vAlI olAvqRti Pasala ko BArI hAni pahuzcAwI hE.
;
(defrule hail2
(declare (salience 4800))
(id-root ?id hail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pukAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hail.clp 	hail2   "  ?id "  pukAra )" crlf))
)

;"hail","V","1.pukAranA"
;He hailed his friend .
;usane apane miwra ko pukArA . 
;
;LEVEL 
;Headword : hail
;
;Examples --
;
;Noun
;--"1.ole"
;..a current of cold air, was congealed into a hail-stone.  
;TaMdI havA eka JoMkA ole ke gole meM CipA WA.
;Why were they not all killed by the shot that fell like hail among them? 
;oloM kI waraha unapara giranevAlI usa goliyoM kI bOCAra se ve saba mare kEse nahIM ?  
;--"2.bOCAra"
;Poulsson....had wriggled out of it on the other side, && was squirming in a hail of bullets towards Ray.
;pulasana .... reMgakara xUsarI ora se nikala gayA WA, Ora goliyoM kI bOCAra meM CatapatAwe hue re kI ora baDZa rahA WA.
;
;Verb
;--"1.pukArA"
;He ran down the stairs into the street && hailed a cab.
;vaha sIDZiyoM se nIce sadZaka kI ora ko xOdA Ora usane eka tEksI pukArA.
;Somebody would be "hailing a ghost" presently, if it wasn't done.
;agara yaha nahIM kiyA gayA wo aBI koI 'BUwa ko pukAregA'.
;A voice was now heard hailing us from the entrance.
;praveSa xvAra se hameM pukArawI eka AvAjZa sunAI xI.
;He had a persuasive, hail-fellow well-met air with him.
;
;--"2.cillAkara_aBivAxana_karanA"
;The crowd hailed the famous star.
;BIdZa ne prasixXa aBinewA kA cillAkara aBivAxana kiyA. 
;
;"hail from","kA_rahanevAlA"
;"I hail from California myself," was Messner's announcement.
;'mEM Kuxa kEliPorniyA kA rahanevAlA hUz' mEsanara kI GoRaNA WI.
;
;sUwra : pukAra^aBivAxana/ole{bOCAra}


