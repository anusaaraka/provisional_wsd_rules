;@@@ Added by 14anu-ban-01 on (05-11-2014).
;It is a highly significant discovery.[self:with reference to oald]
;यह एक बहुत महत्वपूर्ण खोज है.[self]
(defrule significant0
(declare (salience 0))
(id-root ?id significant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawvapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  significant.clp  	significant0  "  ?id " mahawvapUrNa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-11-2014).
;A choice of change of different units does not change the number of significant digits or figures in a measurement.[NCERT corpus]
;किसी मापन में विभिन्न मात्रकों के परिवर्तन के चयन से सार्थक अङ्कों की सङ्ख्या परिवर्तित नहीं होती.[NCERT corpus]
;Clearly, reporting the result of measurement that includes more digits than the significant digits is superfluous and also misleading since it would give a wrong idea about the precision of measurement.[NCERT corpus]
;अतः राशि के मापन के परिणाम में सार्थक अङ्कों से अधिक अङ्क लिखना अनावश्यक एवं भ्रामक होगा, क्योंकि, यह माप की परिशुद्धता के विषय में गलत धारणा देगा.[NCERT corpus]
(defrule significant1
(declare (salience 3700))
(id-root ?id significant)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 figure|digit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArWaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  significant.clp  	significant1  "  ?id " sArWaka )" crlf))
)
