
(defrule quote0
(declare (salience 5000))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxXaraNacihna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote0   "  ?id "  uxXaraNacihna )" crlf))
)

;"quote","N","1.uxXaraNacihna"
;He put his remarks within quotes.
;
(defrule quote1
(declare (salience 4900))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxXqwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote1   "  ?id "  uxXqwa_kara )" crlf))
)

;"quote","VT","1.uxXqwa_karanA"
;He could quote from Shakespeare.
;--"2.xAma_lagAnA"
;Quote your prices for these cars.
;
;

;@@@ Added by 11anu-ban-11 on 08-09-2014
;In Sur's poem , the old sayings are quoted at many places . 
;11 . सूर की कविता में पुराने आख्यानों और कथनों का उल्लेख बहुत स्थानों में मिलता है ।
(defrule quote2
(declare (salience 5100))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote2   "  ?id "  ulleKa )" crlf))
)


;@@@ Added by 14anu-ban-11 on (30-03-2015)
;Quote your prices for these cars.(quote.clp)
;इन गाडियों के लिए अपने मूल्य भाव बताइए .  [manual]
(defrule quote3
(declare (salience 4901))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 price)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAva_bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote3  "  ?id "  BAva_bawA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-04-2015)
;Put the title of the article in quotes.(oald)
;विराम चिह्नों में  लेख का नाम रखिए . (self)
(defrule quote4
(declare (salience 5001))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id1 article)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAma_cihna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote4  "  ?id "  virAma_cihna)" crlf))
)

;@@@ Added by 14anu-ban-11 on (11-04-2015)
;The essay was full of quotes.(oald)
;निबन्ध उदाहरणों से भरा हुआ था . (self)
(defrule quote5
(declare (salience 5002))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(id-root ?id1 full)
(id-root ?id2 essay)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAharaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote5  "  ?id "  uxAharaNa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-04-2015)
;Their quote for the job was way too high. (oald)
;काम के लिए उनका महत्त्व बहुत  था . (self)
(defrule quote6
(declare (salience 5003))
(id-root ?id quote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quote.clp 	quote6  "  ?id "  mahawwva)" crlf))
)


