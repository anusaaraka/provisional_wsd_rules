;@@@ Added by 14anu-ban-06 (26-02-2015)
;If only he would show a little humanity for once.(cambridge)
;अगर वह एक बार के लिए थोड़ी इंसानियत दिखाता . (manual)
;If only he would display a little humanity for once. (cambridge)
;अगर वह एक बार के लिए थोड़ी इंसानियत दिखाता . (manual)
(defrule humanity1
(declare (salience 2000))
(id-root ?id humanity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 display|show)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMsAniyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  humanity.clp 	humanity1   "  ?id "  iMsAniyawa )" crlf))
)

;@@@ Added by 14anu-ban-06 (26-02-2015)
;The judge was praised for his courage and humanity. (OALD)
;न्यायाधीश की उसके साहस और इंसानियत के लिए प्रशंसा की जाती थी . (manual)
(defrule humanity2
(declare (salience 2100))
(id-root ?id humanity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI ?id1 ?id)
(id-root ?id1 praise)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMsAniyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  humanity.clp 	humanity2   "  ?id "  iMsAniyawa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (26-02-2015)
;The massacre was a crime against humanity. (cambridge)
;हत्याकाण्ड मानवजाति के विरुद्ध अपराध था . (manual)
(defrule humanity0
(declare (salience 0))
(id-root ?id humanity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnavajAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  humanity.clp         humanity0   "  ?id "  mAnavajAwi )" crlf))
)

