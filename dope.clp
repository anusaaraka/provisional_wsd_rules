;@@@ Added by 14anu-ban-04 (21-03-2015)
;He is a dope.              [free-dictionary]
;वह मूर्ख व्यक्ति है .               [self]
(defrule dope3
(declare (salience 20))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  mUrKa_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope3  "  ?id " mUrKa_vyakwi)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-03-2015)
;Give me the dope on the new boss.                 [oald]
;मुझे  नये बॉस के बारे में जानकारी  दीजिए .                        [self]
(defrule dope4
(declare (salience 20))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope4  "  ?id "  jAnakArI)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-03-2015)
;The wine was doped.                            [oald]
;वाइन में नशीली दवा मिलाई गयी थी .                           [self]
(defrule dope5
(declare (salience 20))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id meM)) 
(assert (id-wsd_root_mng ?id  naSIlI_xavA_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  dope.clp     dope5  "  ?id " meM  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope5  "  ?id " naSIlI_xavA_milA)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-03-2015)
;They doped his drink before robbing him.            [merriam-webster]
;उसको लूटने से पहले  उन्होंने उसके पेय पदार्थ में नशीली दवा मिलाई .        [self]
(defrule dope6
(declare (salience 20))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id meM)) 
(assert (id-wsd_root_mng ?id  naSIlI_xavA_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  dope.clp    dope6   "  ?id " meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope6  "  ?id " naSIlI_xavA_milA)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-03-2015)
;What a dope he is.                       [merriam-webster]
;वह  कितना  मूर्ख  है .                              [self]
(defrule dope7
(declare (salience 20))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(praSnAwmaka_vAkya)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope7  "  ?id " mUrKa)" crlf))
)


;------------------------------------------------------ DEFAULT RULES ------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (21-03-2015)
;They were arrested for selling dope.                           [cald]
;वे नशीली दवा बेचने के लिए पकड़े गये थे .                                    [self]
(defrule dope0
(declare (salience 10))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naSIlI_xavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope0  "  ?id " naSIlI_xavA)" crlf))
)


;@@@ Added by 14anu-ban-04 (21-03-2015)
;Thieves doped a guard dog and stole $10 000 worth of goods.           [oald]
;चोरों ने एक पहरी कुत्ते को नशीली दवा खिलाई और  $10 000 के  मूल्य हितों की चोरी की .           [self]
(defrule dope1
(declare (salience 10))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naSIlI_xavA_KilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope1 "  ?id " naSIlI_xavA_KilA)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-03-2015)
;That movie was so dope.                [merriam-webster]    
;वह चलचित्र बहुत अच्छा था .                      [self]
(defrule dope2
(declare (salience 10))
(id-root ?id dope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dope.clp    dope2 "  ?id " acCA)" crlf))
)

