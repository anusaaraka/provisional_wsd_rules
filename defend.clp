;@@@ Added by 14anu-ban-04 (12-02-2015)
;How can you defend such behaviour?                                [oald]
;आप ऐसा व्यवहार का कैसे समर्थन कर सकते हैं?                                   [self]
;He defended his decision to punish the boy.                        [olad]
;उसने लड़के को दण्ड देने के लिए उसके निर्णय का समर्थन किया  .                        [self]
(defrule defend1
(declare (salience 20))
(id-root ?id defend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 decision|behaviour) 
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  defend.clp     defend1   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defend.clp 	defend1   "  ?id "  samarWana_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (16-02-2015)
;She will be defending her title at next month's championships.         [oald]
;वह अगले महीने की  प्रतियोगिता में अपने  ख़िताब  को पुनः प्राप्त कर रही होगी .                   [self]
;He intends to defend his seat in the next election.                     [oald]
;वह अगले चुनाव में अपनी सीट को पुनः प्राप्त करने  का इरादा रखता है .                          [self] 
(defrule defend2
(declare (salience 20))
(id-root ?id defend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 title|seat) 
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id punaH_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  defend.clp     defend2   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defend.clp 	defend2   "  ?id "  punaH_prApwa_kara )" crlf))
)

;--------------------- Default Rules --------------

;@@@ Added by 14anu-ban-04 (12-02-2015)
;Troops have been sent to defend the borders.                              [oald                    
;दल सरहदों की  रक्षा करने के लिए भेजे गये हैं .                                            [self]
(defrule defend0
(declare (salience 10))
(id-root ?id defend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defend.clp 	defend0   "  ?id "  rakRA_kara )" crlf))
)

