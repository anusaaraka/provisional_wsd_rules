;@@@ Added by 14anu-ban-06 (15-04-2015)
;Turn right at the next intersection.(cambridge)
;अगले चौराहे पर  दाहिनी ओर मुडिए . (manual)
(defrule intersection1
(declare (salience 2000))
(id-root ?id intersection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-kriyA_viSeRaNa ?id1 ?id2)
(viSeRya-at_saMbanXI ?id2 ?id)
(id-root ?id1 turn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOrAhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intersection.clp 	intersection1   "  ?id "  cOrAhA )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-04-2015)
;A busy intersection. (cambridge)
;व्यस्त चौराहा . (manual)
(defrule intersection2
(declare (salience 2100))
(id-root ?id intersection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 busy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOrAhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intersection.clp 	intersection2   "  ?id "  cOrAhA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (15-04-2015)
;The intersection of the lines on the graph marks the point where we start to make a profit.(cambridge)
;ग्राफ पर रेखाओं का प्रतिच्छेदन उस बिंदु को चिह्नित करता है जहाँ हम लाभ कमाना शुरु करते हैं . (manual)
(defrule intersection0
(declare (salience 0))
(id-root ?id intersection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawicCexana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intersection.clp 	intersection0   "  ?id "  prawicCexana )" crlf))
)

