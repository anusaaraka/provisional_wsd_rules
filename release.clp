;@@@ Added by 14anu-ban-10 on (15-11-2014)
;A stone released from the top of a building accelerates downward due to the gravitational pull of the earth. [ncert corpus]
;kisI Bavana ke SiKara se binA aXomuKI XakkA xiye mukwa kiyA gayA pawWara pqWvI ke guruwvIya KiFcAva ke kAraNa wvariwa ho jAwA hE.[ncert corpus]
(defrule release2
(declare (salience 5000))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release2   "  ?id "  mukwa_kara )" crlf))
)


;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
;The movie's general release is next week. 
;चलचित्र का सामान्य प्रकाशन अगला सप्ताह है . 
(defrule release3
(declare (salience 5000))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 movie|tape|cassete|book|poem|poetry)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release3   "  ?id "  prakASana )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
;The new software is planned for release in April.
;नया सॉफ्टवेयर अप्रैल में रिलीज कर के लिए योजना बनाई गयी है .
(defrule release4
(declare (salience 5000))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rilIja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release4   "  ?id "  rilIja_kara )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
; The release of carbon dioxide into the atmosphere.
;वातावरण के अन्दर कार्बन डाइआक्साइड को छोडा गया. 
(defrule release5
(declare (salience 5000))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 gas|oxygen|carbon|dioxide|nitrogen|argon|helium|hydrogen)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release5   "  ?id "  CodZa_xe )" crlf))
)

;@@@ Added by 14anu17
;Lightening A couplet in the Rigveda says O Rudra ! please ask your thunderbolt , which you have released in the skies , to stop chasing us upon our earth 
(defrule release6
(declare (salience 4900))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse =(+ ?id 1) preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release6   "  ?id "  CodZa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-02-2015)
;The release mechanism of a lock.[hinkhoj]
;एक ताला की खुलने की क्रिया  तंत्र . [manual]
(defrule release7
(declare (salience 5100))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?  ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kulane_kI_kirayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release7   "  ?id "  Kulane_kI_kirayA)" crlf))
)
;@@@ Added by 14anu-ban-10 on (18-02-2015)
;This news has been released to the press.[hinkhoj]
;यह खबर  मोचन करना  के लिए प्रेस मे जारी की गई है ।[manual]
(defrule release8
(declare (salience 5200))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mocana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release8   "  ?id "  mocana_kara)" crlf))
)
;@@@ Added by 14anu-ban-10 on (18-02-2015)
;The release of a new film is delayed.[hinkhoj]
;एक नई फिल्म की  प्रदर्शन में देरी कर दियी है।[manual]
(defrule release9
(declare (salience 5300))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release9   "  ?id "  praxarSana)" crlf))
)
;@@@ Added by 14anu-ban-10 on (18-02-2015)
;The new edition of the book has been released.[hinkhoj]
;पुस्तक के नए संस्करण का विमोचन किया गया है .[manual]
(defrule release10
(declare (salience 5400))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ? )
(id-root ?id1 edition)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimocana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release10   "  ?id "  vimocana_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;"release","N","1.rihAI"
;Jailer got an order for Rahul's release from prison.
(defrule release0
(declare (salience 5000))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rihAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release0   "  ?id "  rihAI )" crlf))
)

;;"release","VT","1.CodZanA"
;The prisoner was released
(defrule release1
(declare (salience 4900))
(id-root ?id release)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  release.clp 	release1   "  ?id "  CodZa )" crlf))
)



;"release","N","1.rihAI"
;Jailer got an order for Rahul's release from prison.
;--"2.praxarSana"
;The release of a new film is delayed.
;--"3.Kulane_kI_kriyA"
;The release mechanism of a lock.
;"release","VT","1.CodZanA"
;The prisoner was released
;--"2.mukwa_karanA"
;She gently released herself from his arms.
;--"3.vimocana/lokArpaNa_karanA"
;The new edition of the book has been released.
;--"4.mocana_karanA"
;This news has been released  to the press. 
;--"5.CodZanA{yaMwra_kA}"
;The bullet is released from the gun at very high speed. 
;--"6.wyAganA"
;He has released his rights in favour of his nephew.     
;
