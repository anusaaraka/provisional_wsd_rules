;@@@ Added by 14anu-ban-06 (30-03-2015)
;A child learns to talk by imitation. (OALD)
;बच्चा प्रतिलिपि के द्वारा बातचीत करना सीखता है . (manual)
(defrule imitation1
(declare (salience 2000))
(id-root ?id imitation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-by_saMbanXI ?id1 ?id)
(id-root ?id1 talk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawilipi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitation.clp 	imitation1   "  ?id "  prawilipi )" crlf))
)

;@@@ Added by 14anu-ban-06 (30-03-2015)
;He does an imitation of Barack Obama.(OALD)
;वह बराक ओबामा की नकल करता है . (manual)
;She can do a wonderful imitation of a blackbird's song. (cambridge)
;वह ब्लैकबर्ड के गाने की बहुत बढ़िया कर सकती है . (manual)
(defrule imitation2
(declare (salience 2100))
(id-root ?id imitation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitation.clp 	imitation2   "  ?id "  nakala )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (30-03-2015)
;Imitation leather.(OALD)[parser no.- 7]
;नकली चमडा . (manual)
(defrule imitation0
(declare (salience 0))
(id-root ?id imitation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitation.clp 	imitation0   "  ?id "  nakalI )" crlf))
)

;@@@ Added by 14anu-ban-06 (30-03-2015)
;An imitation leather jacket. (cambridge)[parser no.- 7]
;कृत्रिम चमडे की जैकेट . (manual)
(defrule imitation3
(declare (salience 0))
(id-root ?id imitation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqwrima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitation.clp 	imitation3   "  ?id "  kqwrima )" crlf))
)
