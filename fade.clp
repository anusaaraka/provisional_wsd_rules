;@@@ Added by 14anu-ban-05 on (05-02-2015)
;Her smile faded.	[oald]
;उसकी मुस्कराहट फीकी पड़ गई. [manual]

(defrule fade0
(declare (salience 100))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id PIkI_padZa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fade.clp  	fade0   "  ?id "  PIkI_padZa_jA )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (05-02-2015)
;The laughter faded away. [oald]
;हँसी गायब हो गई.		  [manual]

(defrule fade1
(declare (salience 101))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gAyaba_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fade.clp  fade1  "  ?id "  " + ?id 1 "  gAyaba_ho_jA  )" crlf))
)


;@@@ Added by 14anu-ban-05 on (05-02-2015)
;His voice faded to a whisper.	[oald]
;उसकी आवाज काना फूसी में तब्दिल हो गई.	[manual]
(defrule fade2
(declare (salience 102))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wabxila_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fade.clp  	fade2   "  ?id "  wabxila_ho_jA )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (05-02-2015)
;Summer was fading into autumn.	[oald]
;ग्रीष्म शरद ऋतु  में विलीन हो जाता है.	[manual]

(defrule fade3
(declare (salience 103))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vilIna_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fade.clp  	fade3   "  ?id "  vilIna_ho_jA )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (05-02-2015)
;All other issues fade into insignificance compared with the struggle for survival. [oald]
;अन्य सभी   मुद्दें  नगण्य  महत्व के हो जाते हैं जब तुलना अस्तित्व के लिए संघर्ष से करते हैं.   			 [manual]
(defrule fade4
(declare (salience 103))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) into)
(id-word =(+ ?id 2) insignificance)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) nagaNya_mahawva_kA_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fade.clp  fade4  "  ?id "  " + ?id 1 "  " + ?id 2 " nagaNya_mahawva_kA_ho_jA  )" crlf))
)


;@@@ Added by 14anu-ban-05 on (05-02-2015)
;Memories have faded, he said, and Wal-Mart did not keep good records.[COCA]
;यादें धुँधली पर चुकी हैं ,उन्होंने कहा, और वाल मार्ट अच्छा रिकॉर्ड नहीं रखा था.			[MANUAL]
(defrule fade5
(declare (salience 104))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 memory)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XuzXalI_para_cukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fade.clp  	fade5   "  ?id " XuzXalI_para_cukA )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (05-02-2015)
;Unfortunately due to the water leaking from the rocks at the top the heads of all figures have faded .[tourism]
;xurBAgya se Upara kI catZtAnoM se pAnI risane ke kAraNa prAya: saBI AkqwiyoM ke sira mita gae hEM .[tourism]
(defrule fade6
(declare (salience 105))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-due_to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mita_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fade.clp  	fade6   "  ?id "  mita_jA )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (05-02-2015)
;With this beauty of the tourist places start to fade .[tourism]
;isake sAWa hI paryatana sWaloM kA yaha sOMxarya lupZwa hone lagawA hE .[tourism]
(defrule fade7
(declare (salience 103))
(id-root ?id fade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(- ?id 1) to)
(id-word =(- ?id 2) start)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (- ?id 2) lupZwa_hone_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fade.clp  fade7  "  ?id "  " - ?id 1 "  " - ?id 2 " lupZwa_hone_laga  )" crlf))
)

