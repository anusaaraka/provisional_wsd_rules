;;$$$ meaning changed from nOkarI_se_nikAla to muzha_moda_le by Prachi Rathore[26-11-13]
;When they show an operation on TV, I have to turn away.
;दूरदर्शन पर जब आपरेशन करते हुए दिखाते है,मैं अपना मुँह दूसरी तरफ मोड़ लेता हूँ.
(defrule turn0
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muzha_moda_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn0  "  ?id "  " ?id1 "  muzha_moda_le  )" crlf))
)

;$$$ Modified by 14anu22
(defrule turn1
(declare (salience 4800))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id  proposal)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TukarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn1  "  ?id "  " ?id1 "  TukarA  )" crlf))
)



;Added by Amba
(defrule turn2
(declare (salience 4700))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 inakAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn2  "  ?id "  " ?id1 "  inakAra_kara  )" crlf))
)




(defrule turn3
(declare (salience 4600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn3  "  ?id "  " ?id1 "  bAhara_nikAla  )" crlf))
)


;$$$ meaning changed from Ulata to palata by Prachi Rathore[26-11-13]
(defrule turn4
(declare (salience 4400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 palata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn4  "  ?id "  " ?id1 "  palata  )" crlf))
)

;@@@ Modified by sudhir wsd participant [10-07-14]
;Changed meaning from 'upasWiwa_ho' as 'mila_jA'
;He is still hoping something will turn up.
;vah aBI BI ASA kar rahA hE ki kuCa mila jAyegA.
;I am sure it will turn up.
;mEM ASvasxa huM ki yah mila jAyegA.
(defrule turn5
(declare (salience 4200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mila_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn5  "  ?id "  " ?id1 "  mila_jA  )" crlf))
)


;The rule can be deleted as it has been taken care of in rule8 (Meena 30.4.10).
; Added by human
(defrule turn6
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 left)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn6   "  ?id "  moda )" crlf))
)

;$$$Modified by 14anu-ban-07,(17-02-2015)
;And when the light turned red at the crosswalk, everyone stopped. (coca)
;क्रासिगं  पर  जब बत्ती लाल हुई  , हर कोई रुका गया. (manual)
;The milk has turned sour
(defrule turn7
(declare (salience 3900))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)  			;added by 14anu-ban-07,(17-02-2015)
(subject-subject_samAnAXikaraNa  ?id1 ?id2) 	;added by 14anu-ban-07,(17-02-2015)
(id-word ?id2 sour|red|blue|green)  		;modified ?id1 as ?id2 by 14anu-ban-07,(17-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn7   "  ?id "  ho )" crlf))
)



;Modified by Meena(25.1.11) ; added (conjunction-components  ?conj  ?id2 ?id) for the cases like the example below.
;The path has many twists and turns. 
;Modified by Meena(30.4.10)
;Go straight and take a right turn . 
(defrule turn8
(declare (salience 3500))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)(conjunction-components  ?conj  ?id ?id2)(conjunction-components  ?conj  ?id1 ?id)) ;Interchanged conjunction components ids  by Roja(15-02-11)
;$$$ (conjunction-components  ?conj  ?id1 ?id) Again added by  Prachi Rathore[25-11-13]
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id modZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn8   "  ?id "  modZa )" crlf))
)




(defrule turn9
(declare (salience 3400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn9   "  ?id "  GumA )" crlf))
)

;I couldn't see Elena's expression, because her head was turned.
;मैं एलेना के हाव-भाव को नहीं देख सका,क्योंकि उसका सिर मुड़ा हुआ था .
(defrule turn10
(declare (salience 3300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muda))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn10   "  ?id "  muda )" crlf))
)

;"turn","VTI","1.GumAnA"
;You turn the wheel.
;
;


;Salience reduced by Meena(30.4.10)
;Added by Veena Bagga (01-01-2010)
(defrule turn11
;(declare (salience 3500))
(id-root ?id turn)
;(Any)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn11 " ?id " bArI)" crlf))
)
;She is waiting for her turn .


;Added by Roja(18-09-10)
;The lights in the street turn on only during night.
(defrule turn12
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga  ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp  turn12  "  ?id "  " ?id1 "  jala)" crlf))
)


;@@@ Added by Prachi Rathore[26-11-13]
;Suddenly she just turned on me and accused me of undermining her.
;अचानक से वह मुझ पर दोष लगाने लगी और उसने मुझे गुप्तरूप से हानि पहुँचाने के लिए अपराधी ठहराया.
(defrule turn13
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn13 " ?id " xoRa_lagA)" crlf))
)


;@@@ Added by Prachi Rathore[26-11-13]
;Now turn the page, please, and start work on Exercise 2.
;अब कृपया पन्ना पलटें,और दूसरे अभ्यास पर काम करना शुरू करें.
(defrule turn14
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 page)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id palata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn14 " ?id " palata)" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;The weather has suddenly turned cold.
;मौसम अचानक ठंडा हो गया
(defrule turn15
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(kriyA-subject  ?id ?id1)
(id-root ?id2 cold|pale|nasty|solemn|sour|bad|good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn15 " ?id " ho_jA)" crlf))
)

;commented by 14anu-ban-07,(20-02-2015)
;Turn off the light.
;बत्ती बन्द कीजिए . (14anu-ban-07)
;बुझा दो and बन्द कीजिए  is same.

;@@@ Added by Prachi Rathore[26-11-13]
;Turn off the light.
;बत्ती बुझा दो
;(defrule turn16
;(declare (salience 4200))
;(id-root ?id turn)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 off)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 buJA_xe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn16  "  ?id "  " ?id1 "  buJA_xe  )" crlf))
;)

;@@@ Added by Prachi Rathore[26-11-13]
;She turned her ankle on the rocks and had to hobble back to camp.
;उसका टखना चट्टान पर मुड गया और उसे वापिस शिविर तक लंगड़ा कर चलना पड़ा.
(defrule turn17
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muda_jA))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn17 " ?id " muda_jA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  turn.clp     turn17   "  ?id "  kA )" crlf))
)



;@@@ Added by Prachi Rathore[26-11-13]
;Surely you won't turn your back on them?
;विश्वास है तुम उनके प्रति अपनी सहायता से मुँह नही मोड़ोगे
(defrule turn18
(declare (salience 4100))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(kriyA-object  ?id ?id2)
(id-root ?id2 back)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 muzha_moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn18  "  ?id "  " ?id2 "  muzha_moda )" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;Spain cannot afford to turn its back on tourism.
;स्पेन पर्यटन के प्रति विमुख नही हो सकता.
(defrule turn19
(declare (salience 4100))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(kriyA-object  ?id ?id2)
(id-root ?id2 back)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 vimuKa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn19  "  ?id "  " ?id2 "  vimuKa_ho )" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;In this game if you give the wrong answer you have to miss a turn.
;इस खेल में अगर तुम गलत उत्तर देते हो तो तुम्हें एक बारी छोड़नी पड़ेगी.
(defrule turn20
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 miss)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn20 " ?id " bArI)" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;The girl's natural father claimed that her stepfather was turning her against him.
;लड़की के जैविक पिता ने दावा किया है कि उसके सौतेले पिता उसे उनके विरूद्ध कर रहें थे.
(defrule turn21
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-against_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn21 " ?id " kara)" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;The girl's natural father claimed that her stepfather was turning her against him.
;लड़की के जैविक पिता ने दावा किया है कि उसके सौतेले पिता उसे उनके विरूद्ध कर रहें थे.
(defrule turn22
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-against_saMbanXI  ?id ?id1)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn22 " ?id " ho)" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;They turned us away at the entrance because we hadn't got tickets.
;हमारे पास टिकिट नही थे इसलिए प्रवेश-द्वार से ही हमे वापिस भेज दिया गया  (अंदर जाने नही दिया)
(defrule turn23
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-root ?id2 away)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 vApasa_Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn23  "  ?id "  " ?id2 "  vApasa_Beja )" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;I usually turn in at about midnight.
; मैं ज्यादातर मध्यरात्रि में बिस्तर में जाकर सोता हूँ.
(defrule turn24
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 biswara_me_so))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn24  "  ?id "  " ?id1 "  biswara_me_so  )" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;Thousands of weapons were turned in during the national gun amnesty.
;हजारों शस्त्र राष्ट्रीयक्षमा के दौरान वापिस कर दिए गए.
(defrule turn25
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_during_saMbanXI  ?id ?)
(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1))
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn25 " ?id " vApasa_kara)" crlf))
)

;@@@ Added by Prachi Rathore[26-11-13]
;$$$ Modified by Vasu Vardhan[4-6-14]  (14anu09)
;Both companies turn in pre-tax profits of over 5.5 million annually.
;दोनों कम्पनियों ने टैक्स के प्रोफिट को छोड़कर करीब ५.५ मिलियन का लाभ कमाया.
(defrule turn26
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) in) ;editted by Vasu Vardhan[4-6-14] (this rule is for "turn in". "in turn" has another meaning.(rules turn39 turn 40)) 
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn26 " ?id " kamA)" crlf))
)

;$$$ Modified by 14anu-ban-07,(20-02-2015)
;@@@ Added by Prachi Rathore[26-11-13]
;I should think the smell of her breath would turn any man off.
;मेरे विचार से उसके मुहँ की दुर्गन्ध से कोई भी व्यक्ति उससे मुख मोड़ लेगा. 
(defrule turn27
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)                           ;spelling changed from 'kriya' to 'kriyA' by 14anu-ban-07 (20-02-2015)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muzKa_moda_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn27  "  ?id "  " ?id1 "  muzKa_moda_le  )" crlf))
)


;@@@ Added by Prachi Rathore[26-11-13]
;Short men really turn me on.
;छोटे कद के आदमी मुझे आकर्षित करते हैं. 
(defrule turn28
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AkarRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn28  "  ?id "  " ?id1 "  AkarRiwa_kara  )" crlf))
)

;@@@ Added by Prachi Rathore[03-12-13]
; If nothing else, the school will turn her into an individual.[03-12-13][cambridge]
;यदि कुछ भी नही अन्य, तो विद्यालय एक विशिष्ट व्यक्ति में उसको बदल देगा . 
(defrule turn29
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn29 " ?id " baxala_xe)" crlf))
)

;Commented by July workshop participants under Aditi and Soma guidance (15-07-14)
;@@@ Added by Prachi Rathore
;To turn the volume up.[oald]
;(defrule turn30
;(declare (salience 5000))
;(id-root ?id turn)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 up)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id2)
;(id-root ?id2 volume)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kara_xe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn30 " ?id " kara_xe)" crlf))
;)


;$$$ Modified by 14anu-ban-07,(20-02-2015)      
;Please turn the volume down. (oald)
;कृपया आवाज धीमा कर दीजिए . (manual)
;@@@ Added by Prachi Rathore
;To turn the volume down.[oald]
; आवाज कम/धीमी कर देने के लिये . 
(defrule turn31
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 volume)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id kara_xe)) ;commented by 14anu-ban-07,(20-02-2015) 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XImA_kara_xe))  ;added by 14anu-ban-07,(20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn31  "  ?id "  " ?id1 "  XImA_kara_xe  )" crlf))      ;changed the meaning from 'kara_xe' to 'XImA_kara_xe'  and added affected id by 14anu-ban-07,(20-02-2015)
)

;@@@ Added by Prachi Rathore
;Two years ago I shot a Royal Bengal Tiger which had turned man-eater. 
;दो वर्ष पहले मैंने एक ऐसे ही रायल बंगाल टाइगर को मारा था, जो आदमखोर हो चुका था।
(defrule turn32
(declare (salience 3400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 man-eater)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn32   "  ?id "  ho_jA )" crlf))
)

;$$$ Modified by 14anu-ban-07,(20-02-2015)
;@@@ Added by Prachi Rathore
;Turn the engine off.[cambridge]
;इंजन बंद कीजिए .
(defrule turn33
(declare (salience 4400))   ;salience  increased from 3400 to 4400 due to turn16 
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
;(kriyA-kriyA_viSeRaNa  ?id ?id1)  ;commented by 14anu-ban-07,(20-02-2015)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)  ;added by 14anu-ban-07,(20-02-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn33  "  ?id "  " ?id1 "  baMxa_kara  )" crlf))
)

;$$$ Modified by July workshop participants under Aditi and Soma guidance (15-07-14) 
;Changed meaning from 'dAla_xe' to 'xabAva_dAla'
;@@@ Added by Prachi Rathore[8-1-14]
;United turned up the heat on their opponents with a second goal.[oald]
;xUsare gola ke sAWa yunAited ne apane prawixvanxiyoM para xabAva dAlA.
(defrule turn34
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 heat)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 xabAva_dAla)) ; added ?id2 to affected_ids by July workshop participants on 15-07-14
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn34  "  ?id "  " ?id1 "  "?id2 "  xabAva_dAla )" crlf))
)

;@@@ Added by Prachi Rathore[16-1-14]
; He turned tail and ran from the fight.[m-w]
;;उसने दुम दबाई और लडाई से भागा . 
(defrule turn35
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object  ?id ?id1)
(id-root ?id1 tail)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn35   "  ?id "  xabA )" crlf))
)

;$$$ Modified by 14anu-ban-07, (21-02-2015)
;Could you turn the TV up? (oald)
;क्या आप टेलीविज़न (का आवाज़) तेज कर सकते है? 
;$$$ Modified by sudhir wsd participant [09-07-14]
;Changed meaning from  'calA_xe' to 'weja_kara' and added 'sound|music|loudspeaker|volume' in the list
;She turned the sound up.
;usane AvAj baDa dI.
;Could you turn up the radio?
;kyA Ap apane redio kA AvAj badAyenge.
;@@@ Added by Prachi Rathore[18-1-14]
;She turned up the radio to drown out the noise from next door.[oald]
(defrule turn36
(declare (salience 6000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(kriyA-object  ?id ?obj)
(id-root ?obj radio|sound|music|loudspeaker|volume|TV) ;added TV by 14anu-ban-07, (21-02-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 weja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp turn36  "  ?id "  " ?id1 "  weja_kara  )" crlf))
)

;@@@ Added by Prachi Rathore[13-2-14]
;$$$ Modified by Vasu Vardhan[4-6-14]  (14anu09)
;This year has turned out a hard one for me .:www.online-literature.com/tolstoy/2891/.........
;यह वर्ष मेरे लिये  मुश्किलों से भरा रहा है . /यह  वर्ष  मेरे लिये  मुश्किल वाला रहा है . 
(defrule turn37
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-out_saMbanXI  ?id ?id2)
(id-root ?id1 out)
(id-word =(+ ?id 1) out)  ;corrected by Vasu Vardhan[3-6-14](otherwise the rule misfires when there is out in some other part of the sentence)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn37  "  ?id "  " ?id1 "  rahA)" crlf))
)


;@@@ Added by Prachi Rathore[13-2-14]
;It turned out that she was a friend of my sister.[oald]
;यह पता चला कि वह मेरी बहन की मित्र थी . 
(defrule turn38
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(id-root =(+ ?id1 1) that) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  pawA_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn38  "  ?id "  " ?id1 "  pawA_calA )" crlf))
)

;@@@ Added by 14anu22 Priya Panthi(30-5-14).
;this in turn increases the pressure.
;इस्के परिणामस्वरूप दबाव की व्रध्दि होती है.

(defrule turn_obj_1
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) in )
(id-word ?id1 in)
(viSeRya-in_saMbanXI ?id2 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pariNAmasvarUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp    turn_obj_1  "  ?id "  " ?id1 "  pariNAmasvarUpa )" crlf))) 

;@@@ Added by Prachi Rathore[13-2-14]
;The job turned out to be harder than we thought.[oald]
;नौकरी हमारी सोच  की तुलना में अधिक कठिन निकली  . 
(defrule turn39
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(to-infinitive  ?id2 ?id3)
(id-root ?id3 be)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id3 nikalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn39  "  ?id " " ?id1" " ?id3 "    nikalA )" crlf))
)

;@@@ Added by sudhir wsd participant [14-07-14]
;She turned up HIV positive.  [COCA]
;vaha eca AI vI pojItiva pAI gaI.
(defrule turn41
(declare (salience 7000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id  ?obj)
(id-root ?obj positive|negative)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAI_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp turn41  "  ?id "  " ?id1 "  pAI_jA  )" crlf))
)

;@@@ Added by sudhir wsd participant (14-07-2014)
;They turned up the missing item.[COCA]
;unhone lApawA cIjoM ko KojA.
(defrule turn42
(declare (salience 6200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(kriyA-object  ?id ?id3)
(id-root ?id3 species|item|sample)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn42  "  ?id "  " ?id1 "  Koja )" crlf))
)



;@@@ Added by July workshop participants under Aditi and Soma guidance (15-07-14)
;We arranged to meet at 7.30, but she never turned up.
;hamane sADe sAwa baje milane kA samaya niSciwa kiyA WA , lekina vo AyI hI nahIM.
(defrule turn43
(declare (salience 4200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?sub)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn43  "  ?id "  " ?id1 "  A  )" crlf))
)

;@@@ Added by Karanveer,Anshika [Banathali Vidyapith] (17.7.14) 
;The company turns over $3.5 million a year. (OALD)
;isa sAla kampanI kI pUrNa bikrI tIna miliyana rahI.
(defrule turn44
(declare (salience 6200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 over)
(kriyA-subject ?id ?id2)
(id-root ?id2 company|organisation)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrNa_bikrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn44  "  ?id "  " ?id1 "  pUrNa_bikrI)" crlf))
)

;@@@ Added By 14anu-ban-06 Karanveer Kaur (Banasthali Vidyapith) (23-7-14)
;We missed the turn off for the airport.  (OALD)
;hama vimAnapawwana ke lie moda cUke gaye.
(defrule turn45
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-upasaraga ?id ?id1)
(id-root ?id1 off)
(kriyA-for_saMbanXI  ?id ?id2)
(id-root ?id2 airport)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn45  "  ?id "  " ?id1 "  moda)" crlf))
)

;@@@ Added by 14anu-ban-07 ,08-09-2014
;We shall turn to dynamics in later sections. (ncert corpus)
;हम अगले अनुभाग में गति विज्ञान की ओर मुखातिब  होङ्गे. (manually)
(defrule turn46
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) to) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  kI_ora_muKAwiba_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	 turn46  "  ?id "  " (+ ?id 1) " kI_ora_muKAwiba_ho )" crlf))
)


;@@@ Added by 14anu-ban-05 on (24-04-2015)
;At the foot of the stairs she turned to face him.	[OALD]
;सीढ़ियों के निचला सिरे पर वह उसका सामना करने के लिए मुखातिब हुई. [MANUAL]
(defrule turn46_1
(declare (salience 5001))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-root ?id1 face)	;more constraints can be added 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKAwiba_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn46_1 " ?id " muKAwiba_ho)" crlf))
)


;@@@ Added byy 14anu20 on 02.07.2014.
;He turned up the collar of his coat.
;उसने उसके कोट की गरदनी मोडी . 
(defrule turn47
(declare (salience 4400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(kriyA-upasarga  ?id ?id2)
(id-root ?id1 blanket|collar|cot)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp  turn47  "  ?id "  " ?id2 "  moda  )" crlf))
)

;$$$Modified by 14anu-ban-07,(17-02-2015)
;@@@ Added byy 14anu20 on 02.07.2014.
;She turned against her old friend.
;वह उसके पुराने मित्र के खिलाफ हुई .
(defrule turn48
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
;(id-cat_coarse ?id2 preposition)	;commented by 14anu-ban-07,(17-02-2015)
;(id-root ?id2 against)		  ;commented by 14anu-ban-07,(17-02-2015)
(kriyA-against_saMbanXI  ?id ?id1)
(pada_info (group_head_id ?id)(preposition ?id2))   ;added  by 14anu-ban-07,(17-02-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ke_KilAPa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp  turn48  "  ?id "  " ?id2 "  ke_KilAPa_ho  )" crlf))
)

;@@@ Added byy 14anu20 on 02.07.2014.
;She will turn 21 in june.
;वह जून में 21 वर्षीय हो जायेगी . 
(defrule turn49
(declare (salience 4300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-object  ?id ?id2)
(id-cat_coarse ?id2 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varRIya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn49 " ?id " varRIya_ho)" crlf))
)



;@@@ Added by 14anu09 Vasu Vardhan[4-6-14].
;she spoke to each of the guests in turn.
;वह बारी बारी से अतिथियों मे से के हर एक से बोली . 
(defrule turn40
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) in )
(id-word ?id1 in)
(kriyA-in_saMbanXI ?id2 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bArI_bArI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn40  "  ?id "  " ?id1 "  bArI_bArI_se )" crlf)))

;@@@Added by 14anu18 (30-06-14)
;He turned towards her.
;वह उसकी ओर मुडा
(defrule turn_towards
(declare (salience 4000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-towards_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mudA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn_towards   "  ?id "  mudA )" crlf))
)

;@@Added by 14anu18 (30-06-14)
;He turned his body towards her.
;उसने उसकी ओर अपना शरीर मोडा.
(defrule turn_towards2
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-towards_saMbanXI ?id ?)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id modA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn_towards2   "  ?id "  modA )" crlf))
)

;@@@Added by 14anu-ban-07,(16-01-2015)
;Let φ be the flux in each turn in the core at time t due to current in the primary when a voltage vp is applied to it.(ncert)
;प्राथमिक कुण्डली के सिरों के बीच वोल्टता vp लगाने से, माना किसी क्षण t पर, इस कुण्डली का प्रत्येक फेरा क्रोड में φ फ्लक्स उत्पन्न करता है.
(defrule turn54
(declare (salience 3700))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id1 flux)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  PerA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn54   "  ?id "   PerA )" crlf))
)

;@@@Added by 14anu-ban-07,(17-02-2015)
;He turned the gun on himself. (oald)
;उसने स्वयं पर बन्दूक तानी . (manual)
(defrule turn50
(declare (salience 4100))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 gun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  wAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn50   "  ?id "   wAna )" crlf))
)

;@@@Added by 14anu-ban-07,(17-02-2015)
;We took a turn around the park.(oald)
;हमने उद्यान के चारों ओर थोडी सैर ली . (manual)
(defrule turn51
(declare (salience 3600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-around_saMbanXI  ?id1 ?id2)
(id-root ?id2 park)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  WodZI_sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp 	turn51   "  ?id "   WodZI_sEra )" crlf))
)

;@@@Added by 14anu-ban-07,(17-02-2015)
;Each of us collects the mail in turn. (cambridge)
;हमें से हर एक बारी बारी से डाक इकट्ठा करता है . (manual)
(defrule turn52
(declare (salience 3700))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 mail)
(pada_info (group_head_id ?id)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 bArI_bArI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn52  "  ?id "  " ?id1 "  bArI_bArI_se )" crlf)))


;@@@Added by 14anu-ban-07,(17-02-2015)
;Give the screw a couple of turns to make sure it's tight.(cambridge)
;पेंच को थोड़ा  घुमा दीजिए  सुनिश्चित करने केलिए कि यह कसा हुआ है . (manual)
(defrule turn53
(declare (salience 3800))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 couple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn53 " ?id " GumA)" crlf))
)

;@@@Added by 14anu-ban-07,(19-02-2015)
;Burglars had turned the house over.(oald)
;चोरों ने घर लूट लिया था . (manual)
(defrule turn55
(declare (salience 4500))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 burglar|thief)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lUta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn55  "  ?id "  " ?id1 "  lUta_le  )" crlf))
)


;@@@Added by 14anu-ban-07,(19-02-2015)
;Snappy sayings, little stories, easy lessons they can take home and turn over in their minds.(coca)
;रोचक कहावतें,छोटी कहानियाँ,सरल पाठ [की पुस्तकें] जो वे घर लेजा सकते हैं  और अपने मन में दोहरा सकते हैं . (manual)
(defrule turn56
(declare (salience 4600))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id2)
(id-root ?id2 mind)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xoharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn56  "  ?id "  " ?id1 "  xoharA  )" crlf))
)

;@@@Added by 14anu-ban-07,(19-02-2015)
;Despite our worries everything turned out well. (oald)
;हमारी चिंताओं के बावजूद सब-कुछ अच्छे से हो गया . (manual)
(defrule turn57
(declare (salience 5100))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-root ?id2 well)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ho_jA))
(assert (id-wsd_viBakwi ?id2 se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn57  "  ?id "  " ?id1 "  ho_jA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  turn.clp turn57  "  ?id2 " se)" crlf)
)
)


;@@@Added by 14anu-ban-07,(19-02-2015)
;The factory turns out 900 cars a week.(oald)
;फैक्टरी सप्ताह में 900 गाडियों का उत्पादन करती है . (manual)
(defrule turn58
(declare (salience 5200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 factory)
(kriyA-object  ?id ?id3)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwpAxana_kara))
(assert (id-wsd_viBakwi ?id3 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn58  "  ?id "  " ?id1 "  uwpAxana_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  turn.clp turn58  "  ?id3 " kA)" crlf)
)
)


;@@@Added by 14anu-ban-07,(19-02-2015)
;Remember to turn out the lights when you go to bed. (oald)
;याद रखिए  बन्द करना बत्ती जब आप सोने के लिये जाते हैं . (manual)
(defrule turn59
(declare (salience 5300))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 light)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn59  "  ?id "  " ?id1 "  baMxa_kara  )" crlf))
)


;@@@Added by 14anu-ban-07,(19-02-2015)
;You must turn in your pass when you leave the building.(oald)
;आपको   अपना पास वापस करना चाहिए जब आप इमारत छोड कर चले जाते हैं . (manual) 
(defrule turn60
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(pada_info  (group_head_id ?id2) (preposition ?id1))
(kriyA-in_saMbanXI  ?id ?id2)
(id-root ?id2 pass)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vApasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn60  "  ?id "  " ?id1 "  vApasa_kara  )" crlf))
)

;@@@Added by 14anu-ban-07,(19-02-2015)
;I turned my chair round to face the fire. (oald)(parser problem)
;मैंने आग की तरफ  मुँह  करने  केलिए मेरा कुर्सी घुमाई . (manual)
(defrule turn61
(declare (salience 3400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(id-word ?id1 round)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp turn61  "  ?id "  " ?id1 "  GumA  )" crlf))
)


;@@@ Added by 14anu-ban-07,(20-02-2015)
;They turned in a petition with 80 000 signatures.(oald)
;उन्होंने 80 000 हस्ताक्षरों के साथ  निवेदनपत्र दिया .                   [self]
(defrule turn62
(declare (salience 5000))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(pada_info  (group_head_id ?id2) (preposition ?id1))
(kriyA-in_saMbanXI  ?id ?id2)
(id-root ?id2 petition|work)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn62  "  ?id "  " ?id1 "  xe  )" crlf))
)

;@@@ Added by 14anu-ban-07,(20-02-2015)
;People had been turned off by both candidates in the election.(oald)
;लोग चुनाव में दोनों उम्मीदवारों से उब गये थे . (manual)
(defrule turn63
(declare (salience 5200))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-by_saMbanXI ?id ?id3)
(id-root ?id3 candidate)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uba_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn63  "  ?id "  " ?id1 "  uba_jA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(20-02-2015)
;The dogs suddenly turned on each other. (oald) (parser no. 5)
;कुत्ते अचानक एक दूसरे  पे झपट पडे . (manual)
(defrule turn64
(declare (salience 4400))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga  ?id  ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-root ?id2 dog|cat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Japata_pada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turn.clp      turn64 " ?id " Japata_pada)" crlf))
)

;@@@ Added by 14anu-ban-07,(20-02-2015)
;I'll turn the television on.(oald)
;मैं  टेलिविजन को चालू करूँगा . (manual)
(defrule turn65
(declare (salience 4500))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga  ?id  ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 television|radio)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cAlU_kara))
(assert (id-wsd_viBakwi ?id2 ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp  turn65  "  ?id "  " ?id1 "  cAlU_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  turn.clp turn65  "  ?id2 " ko)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(21-02-2015)
;He turned the business over to his daughter.(oald)
;उसने उसकी बेटी को उद्योग सौंपा . (manual)
(defrule turn66
(declare (salience 4700))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id2)
(id-root ?id2 daughter|son)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sOMpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn66  "  ?id "  " ?id1 "  sOMpa  )" crlf))
)

;@@@ Added by 14anu-ban-07,(21-02-2015)
;Turn around and let me look at your back. (oald)
;मुडिए और मुझे  अपनी  पीठ दिखाइए . (manual)
(defrule turn67
(declare (salience 3700))
(id-root ?id turn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 around)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mudZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turn.clp	turn67  "  ?id "  " ?id1 "  mudZa )" crlf))
)



