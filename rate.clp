
;"rating","N","1.SreNI_narXAraNa"
;Government must give to the scientific research a high-priority rating 
(defrule rate0
(declare (salience 5000))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id rating )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SreNI_narXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rate.clp  	rate0   "  ?id "  SreNI_narXAraNa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;They traveled at a rate of 55 miles per hour.[hinkhoj]
;उन्होंने यात्रा प्रति घन्टा 55 के वेग  से कि.[manual]
(defrule rate4
(declare (salience 5100))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI   ?id1 ?id)
(id-root ?id1 travel)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vega))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate4   "  ?id "  vega)" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;He was rated as a broker.[hinkhoj]
;वह  दलाल के वर्ग या श्रेणी मे रखा गया था . [manual]
(defrule rate5
(declare (salience 5200))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI_me_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate5   "  ?id "  SreNI_me_raKa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;I am not prepared to marry her at any rate.[hinkhoj]
;मैं  किसी भी हालत मे उससे विवाह करने को तैयार नहीं हूँ . [manual]
(defrule rate6
(declare (salience 5300))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) any)
(id-word =(- ?id 2) at)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)(- ?id 2) kisI_BI_hAlawa_me))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rate.clp  rate6  "  ?id "  "(- ?id 1)(- ?id 2) " kisI_BI_hAlawa_me)" crlf))
)

;$$$ Modified by 14anu-ban-06 (30-04-2015)
;@@@ Added by 14anu-ban-10 on (05-03-2015)
;His pulse rate dropped suddenly.[hinkhoj]
;उसकी  धड़कन अनुपात अचानक गिरी . [manual]
;उसकी  धड़कन की दर अचानक कम हो गई .(manual);added by 14anu-ban-06 (30-04-2015)
(defrule rate7
(declare (salience 5400))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root =(- ?id 1) pulse)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) Xadakana_kI_xara));meaning changed from 'anupAwa' to 'Xadakana_kI_xara' by 14anu-ban-06 (30-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rate.clp  rate7  "  ?id "  " (- ?id 1) " Xadakana_kI_xara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;If you spend at this rate you will soon be bankrupt.[hinkhoj]
;यदि आप  इसी रफ्तार से खर्च करते रहे तो आप शीघ्र दिवालिया हो जाए गेये . 
(defrule rate8
(declare (salience 5500))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 spend)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isI_raPwAra_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate8   "  ?id "  isI_raPwAra_se)" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;They rated the book quite high.[hinkhoj]
;उन्होंने पुस्तक का मूल्यांकन काफी ऊँचा  करा . [manual]
(defrule rate9
(declare (salience 5600))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlyAMkana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate9  "  ?id "  mUlyAMkana_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-03-2015)
;The rate of interest has fallen from 10% to 7%.[hinkhoj]
;सरकार द्वारा निर्धारित कर 10 % से 7 % से गिरी है . [manual]
(defrule rate10
(declare (salience 5700))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root =(+ ?id 1) of)
(id-word =(+ ?id 2) interest)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)(+ ?id 2) sarakAra_xvArA_nirXAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rate.clp  rate10  "  ?id "  " (+ ?id 1) (+ ?id 2)" sarakAra_xvArA_nirXAriwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-03-2015)
;Those tasks rates low on my priority list.[hinkhoj]
;वह काम मेरी प्राथमिकता सूची पर कम  निर्धारित करा गाया है . [manual]
(defrule rate11
(declare (salience 5800))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 task)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  nirXAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate11   "  ?id "   nirXAriwa_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-03-2015)
;He is walking at the rate of 5km.an hour.[hinkhoj]
;वह 5km. घण्टा के माप से चल रहा है . [manual]
(defrule rate12
(declare (salience 5900))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) the)
(id-word =(- ?id 2) at)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)(- ?id 2) mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rate.clp  rate12  "  ?id "  "(- ?id 1)(- ?id 2) " mApa)" crlf))
)

;------------------------ Default Rules ----------------------

;"rate","V","1.mUlya_lagAnA"
(defrule rate2
(declare (salience 4800))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate2   "  ?id "  mUlya_lagA )" crlf))
)

;"rate","VTI","1.BAva_TaharAnA"
;They rated the book quite high. 
(defrule rate3
(declare (salience 4700))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAva_TaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate3   "  ?id "  BAva_TaharA )" crlf))
)

;"rate","N","1.xara"
;He is walking at the rate of 5km.an hour.
(defrule rate1
(declare (salience 4900))
(id-root ?id rate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rate.clp 	rate1   "  ?id "  xara )" crlf))
)


;"rate","VTI","1.BAva_TaharAnA"
;They rated the book quite high. 
;--"2.mUlyAkaMna_karanA"
;They rate him kind && hospitable.  
;--"3.varga_yA_SreNI_me_raKanA"
;He was rated as a broker.
;--"4.nirXAriwa_karanA"
;Those tasks rates low on my priority list.  
;
;"rate","N","1.xara"
;He is walking at the rate of 5km.an hour.
;--"2.kisI_BI_hAlawa_me"
;I am not prepared to marry her at any rate.  
;--"3.mAwrA"
;If you spend at this rate you will soon be bankrupt.
;--"4.anupAwa"
;His pulse rate dropped suddenly.  
;

