;@@@ Added by 14anu-ban-06 (14-03-2015)
;She's very ingenious when it comes to finding excuses.(OALD)
;वह अत्यन्त चतुर है जब यह बात बहाना ढूंढने की आती है .(manual) 
(defrule ingenious1
(declare (salience 2000))
(id-root ?id ingenious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cawura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " ingenious.clp   ingenious1   "   ?id " cawura )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (14-03-2015)
;His plots are always very ingenious.(OALD)
;उसके कथानक हमेशा अत्यन्त चातुर्यपूर्ण हैं . (manual)
;An ingenious idea. (cambridge)
;चातुर्यपूर्ण विचार . (manual)
(defrule ingenious0
(declare (salience 0))
(id-root ?id ingenious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAwuryapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " ingenious.clp   ingenious0   "   ?id " cAwuryapUrNa )" crlf))
)

