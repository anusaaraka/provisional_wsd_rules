
;@@@ Added by 14anu-ban-04 (31-01-2015)
;He was executed for treason.                      [oald]
;उसे देशद्रोह के लिए प्राण दण्ड दिया गया था .                    [self]
(defrule execute1
(declare (salience 20))
(id-root ?id execute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prANa_xaNda_xe)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  execute.clp 	execute1   "  ?id "  prANa_xaNda_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (31-01-2015)
;His will was executed by his lawyers in 2008.                      [oald]
;2008 में उसकी   वसीयत उसके वकीलों के द्वारा वैध की गयी थी .                        [self]
(defrule execute2
(declare (salience 30))
(id-root ?id execute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 will)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vEXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  execute.clp 	execute2   "  ?id "  vEXa_kara )" crlf))
)

;---------------------------- Default Rules --------------------------

;@@@ Added by 14anu-ban-04 (31-01-2015)
;He executed all his plans carefully.                   [hinkhoj]
;उसने सावधानी से अपनी सभी योजनाएँ कार्यान्वित कीं.                    [self]
(defrule execute0
(declare (salience 10))
(id-root ?id execute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryAnviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  execute.clp  execute0   "  ?id "  kAryAnviwa_kara )" crlf))
)

