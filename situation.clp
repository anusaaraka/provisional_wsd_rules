;@@@ Added by 14anu-ban-01 on (17-10-2014)
;He is in a critical situation.[self:with reference to oald]]
;वह गंभीर हालत|स्थिति में है.[self]
(defrule situation1
(declare (salience 1000))
(id-root ?id situation)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi/hAlawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situation.clp      situation1   "  ?id "  sWiwi/hAlawa)" crlf)
))


;@@@ Added by 14anu-ban-01 on (09-02-2015)
;This situation is depicted in Fig.4.1 (a).[NCERT corpus]
;isa sWiwi ko ciwra 4.1(@a) meM xarSAyA gayA hE.[NCERT corpus]
(defrule situation2
(declare (salience 1000))
(id-root ?id situation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-karma  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situation.clp      situation2   "  ?id "  sWiwi)" crlf)
))

;@@@ Added by 14anu-ban-01 on (17-02-2015)
;This is a situation where the principle of conservation of angular momentum is applicable.[NCERT corpus]
;यह एक ऐसी स्थिति है जिसमें कोणीय संवेग का संरक्षण स्पष्ट है. [NCERT corpus]
(defrule situation3
(declare (salience 1000))
(id-root ?id situation)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(viSeRya-jo_samAnAXikaraNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situation.clp      situation3   "  ?id "  sWiwi)" crlf)
))

;------------------------ Default Rules ----------------------
;@@@ Added by 14anu-ban-01 on (17-10-2014)
;To get the law of free fall under gravity, it is better to create a situation wherein the air resistance is negligible.[NCERT Corpus]
;गुरुत्व बल के अधीन मुक्त पतन का नियम प्राप्त करने के लिए यह श्रेयस्कर है कि ऐसी परिस्थिति उत्पन्न की जाए जिसमें वायु-प्रतिरोध उपेक्षणीय हो और ऐसा किया भी जा सकता है.
;[NCERT Corpus]
(defrule situation0
(declare (salience 1))
(id-root ?id situation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parisWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  situation.clp      situation0   "  ?id "  parisWiwi)" crlf)
))

