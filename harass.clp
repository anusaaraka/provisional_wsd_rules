;$$$ Modified by 14anu02 on 3.7.14
;Her poor harassed father had to endure her constant interruptions.	(WordNet) 
;उसके दीन परेशान पिता को उसकी निरन्तर टोंका टोकी सहनी पडीं . 
(defrule harass0
(declare (salience 5000))
(id-root ?id harass)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id harassed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pareSAna))		;changed meaning from 'pareSAnI' to 'pareSAna' by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  harass.clp  	harass0   "  ?id "  pareSAna )" crlf))
)

;"harassed","Adj","1.pareSAnI"
;vaha mele kI BIdZa BAdZa meM"harassed" ho gaI.
;
(defrule harass1
(declare (salience 4900))
(id-root ?id harass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harass.clp 	harass1   "  ?id "  pareSAna_kara )" crlf))
)

;"harass","V","1.pareSAna karanA"
;pulisa riSvawa pAne ke liye aparAXI ko"harass" kara rahI WI.
;
;
