
(defrule smell0
(declare (salience 5000))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_pawA_lagA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " smell.clp smell0 " ?id "  kA_pawA_lagA )" crlf)) 
)

;$$$ Modified by 14anu-ban-01 on (16-03-2015)
;If you really want to smell out a fake claim, you need a file with real papers in it.[self: with reference to coca]
;यदि आप वास्तव में एक जाली दावे का पता लगाना चाहते हैं, तो आपको इसमें असली कागजों वाली फाइल की आवश्यकता है . [self]
(defrule smell1
(declare (salience 4900))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)								;added by 14anu-ban-01 on (16-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_lagA))  ;changed "kA_pawA_lagA" to "pawA_lagA" by 14anu-ban-01 on (16-03-2015)
(assert (kriyA_id-object_viBakwi ?id kA))					;added by 14anu-ban-01 on (16-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smell.clp 	smell1  "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smell.clp	smell1  "  ?id "  " ?id1 "  pawA_lagA  )" crlf))  ;changed "kA_pawA_lagA" to "pawA_lagA" by 14anu-ban-01 on (16-03-2015)
)


;$$$ Modified by 14anu-ban-01 on (13-03-2015):corrected meaning
;@@@ Added by jagriti(15.3.2014)
;He smelt the accident and jumped out from the running train.[rajpal]
;उसने दुर्घटना भांप ली और चलती रेलगाडी से बाहर कूद पडा . 
(defrule smell4
(declare (salience 4800))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 accident|danger)	
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAzpa_le))	;corrected "BAMpa_le" to "BAzpa_le" by 14anu-ban-01 on (13-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell4   "  ?id "  BAzpa_le )" crlf))	;corrected "BAMpa_le" to "BAzpa_le" by 14anu-ban-01 on (13-03-2015)
)


;@@@ Added by 14anu-ban-01 on (13-03-2015)
;The old house smells of damp.[oald]
;पुराना घर नमी से महकता है .  [self]
(defrule smell5
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell5   "  ?id "  mahaka )" crlf))
)


;@@@ Added by 14anu-ban-01 on (13-03-2015)
;That cake smells good.[cald]
;वह केक अच्छा महकता है .[self]
(defrule smell6
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell6   "  ?id "  mahaka )" crlf))
)



;@@@ Added by 14anu-ban-01 on (13-03-2015)
;Your feet smell .[cald]
;आपके पाँवों से दुर्गन्ध आती है .[self]
(defrule smell7
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 hand|foot|sock|glove)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurganXa_A))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  smell.clp 	smell7  "  ?id " se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell7   "  ?id " xurganXa_A )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;Can you smell something burning?[cald]
;क्या आपको कुछ जलने की बू आ रही है? [self]
(defrule smell8
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 burning)	;changed 'something' to 'burning' by 14anu-ban-01 on (16-03-2015)
(kriyA-object  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?)	;changed '?id2' to '?' by 14anu-ban-01 on (16-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bU_A))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smell.clp 	smell8  "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell8   "  ?id " bU_A )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-03-2015)
;The marketplace was filled with delightful smells.[cald]
;चौक सुहावनी सुगन्ध से भर गया था .[self]
;There's a delicious smell in here.[cald]
;यहां पर  मनोहर/रुचिकर सुगन्ध है . [self]
(defrule smell9
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 delicious|delightful|good|pleasant)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suganXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell9   "  ?id " suganXa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-03-2015)
;I wish we could get rid of that smell in the bathroom. [cald]---parse no. 18
;काश हम स्नानघर में [आ रही] उस दुर्गन्ध से मुक्ति पा सकते . [self]
(defrule smell10
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 rid)
(kriyA-of_saMbanXI  ?id1 ?id)
(viSeRya-in_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurganXa))
(assert (id-wsd_viBakwi ?id2 meM_A_rahI))	;NOTE by 14anu-ban-01 : "A_rahI" because smell is feminine and 'meM' because it will always check (viSeRya-in_saMbanXI  ?id ?id2) condition 					
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  smell.clp 	smell10  "  ?id2 " meM_A_rahI)" crlf)	
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell10   "  ?id "  xurganXa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;She's still enjoying the sweet smell of success after her victory in the world championships.[cald]
;वह  विश्व-स्तरीय प्रतियोगिता में अपनी विजय के बाद अभी भी सफलता की सुखद अनुभूति का आनंद ले रही है . [self] 
(defrule smell11
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 sweet)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id2 success)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBUwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell11   "  ?id " anuBUwi)" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;Once you enter the countryside, you can smell the pollution free sweet air.[sweet.clp]
;जैसे ही आप ग्रामीण क्षेत्र में प्रवेश करते  हैं, वैसे ही आप प्रदूषण मुक्त ताजी हवा में साँस ले सकते हैं .  [self] 
(defrule smell12
(declare (salience 200))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 air)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAzsa_le))
(assert (id-wsd_viBakwi ?id1 meM))					
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  smell.clp 	smell12  "  ?id1 " meM)" crlf)	
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell12   "  ?id "  sAzsa_le )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-03-2015)
;These are sweet smelling flowers.[self: with reference to cald]
;ये खुशबूदार फूल हैं .[self] 
(defrule smell13
(declare (salience 200))
(id-word ?id smelling)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) sweet)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (- ?id 1) KuSabUxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " smell.clp 	smell13  "  ?id "  " (- ?id 1) "  KuSabUxAra  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;It is a very foul smelling rubbish.[self: with reference to cald]
;यह अत्यन्त बदबूदार कचरा है . [self] 
(defrule smell14
(declare (salience 200))
(id-word ?id smelling)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) foul)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (- ?id 1) baxabUxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " smell.clp 	smell14  "  ?id "  " (- ?id 1) "  baxabUxAra  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;Brenda can smell trouble a mile off . [cald]
;ब्रेन्डा आनेवाली समस्याओं को काफी पहले ही भाँप सकती है. [self]
(defrule smell15
(declare (salience 4800))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 trouble)	
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAzpa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell15  "  ?id "  BAzpa )" crlf))	
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;The prophetic task is to smell out death in a situation.[coca]
;किसी परिस्थिति में मृत्यु को भाँप लेना ही भविष्यसूचक कार्य है  . [self]
(defrule smell16
(declare (salience 5000))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-root ?id2 death|trouble)
(kriyA-object ?id ?id2)								
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAzpa_le))	
(assert (kriyA_id-object_viBakwi ?id ko))					
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smell.clp 	smell16  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smell.clp	smell16  "  ?id "  " ?id1 "  BAzpa_le  )" crlf))							
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;That aftershave of yours is smelling out the whole house.[cald]
;आप का वह आफ्टरशेव लोशन पूरे घर को तीखी महक से भर रहा है . [self]
(defrule smell17
(declare (salience 5000))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 aftershave|deodorant|perfume)
(kriyA-subject  ?id ?id1)								
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wIKI_mahaka_se_Bara))	
(assert (kriyA_id-object_viBakwi ?id ko))					
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smell.clp 	smell17  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smell.clp	smell17  "  ?id "  " ?id1 "  wIKI_mahaka_se_Bara )" crlf))							
)


;...Default rule....
(defrule smell2
(declare (salience 100))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUzGa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell2   "  ?id "  sUzGa )" crlf))
)

(defrule smell3
(declare (salience 100))
(id-root ?id smell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ganXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smell.clp 	smell3   "  ?id "  ganXa )" crlf))
)

;"smell","N","1.ganXa"
;Rose's smell is captivating.
;
;
