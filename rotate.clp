; @@@ Added by Anita-05-06-2014
;We rotate the night shift so no one has to do it all the time. [oxford learner's dictionary]
;हमने रात की पारी को घुमा दिया जिससे किसी को इसे हर समय नहीं करना पड़े ।
(defrule rotate0
(declare (salience 1000))
(id-root ?id rotate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate0   "  ?id "  GumaA_xe )" crlf))
)

; @@@ Added by Anita-05-06-2014
;The EU presidency rotates among the members. [oxford learner's dictionary]
;यूरोपीय संघ की अध्यक्षता सदस्यों के बीच घूमती है । 
(defrule rotate1
(declare (salience 2000))
(id-root ?id rotate)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-among_saMbanXI  ?id ?)(kriyA-kriyA_viSeRaNa  ?id ?)(kriyA-kriyArWa_kriyA  ? ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate1   "  ?id "  GUma )" crlf))
)

; @@@ Added by Anita-05-06-2014
;When I joined the company, I rotated around the different sections. [oxford learner's dictionary]
;जब मैंने कंपनी का पद-भार ग्रहण किया , मैंने विभिन्न भागों के चक्कर लगाए । 
(defrule rotate2
(declare (salience 4000))
(id-root ?id rotate)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 around)
(id-root ?sam section)
(kriyA-around_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cakkara_lagA))
(assert (id-wsd_viBakwi ?sam ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate2   "  ?id " " ?id1 " cakkara_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  rotate.clp 	rotate2   "  ?sam "  ke )" crlf))
)

; @@@ Added by Anita-05-06-2014
;Rotate the handle by 180° to open the door.  [cambridge dictionary] [Parse prob.]
;दरवाज़ा खोलने के लिए हैंडल को १८० डिग्री तक घुमाइये।
;Rotate the wheel through 180 degrees. [oxford learner's dictionary]
;पहिया १८० डिग्री तक घुमाइये ।
;180 aMSoM waka pahiyA GumAie. [verified sentence]
(defrule rotate3
(declare (salience 500))
(id-root ?id rotate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate3   "  ?id "  GumA )" crlf))
)

; @@@ Added by Anita-05-06-2014
;We rotate the night shift so no one has to do it all the time. [oxford learner's dictionary]
;हमने रात की पारी में फेर-बदल कर दी जिससे किसी एक को हर समय इसे न करना पड़े ।
;(defrule rotate4
;(declare (salience 5000))
;(id-root ?id rotate)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 shift)
;(id-root ?id2 night|day)
;(id-word ?id3 we)
;(and(kriyA-object  ?id ?)(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2))
;(kriyA-subject  ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Pera_baxala_kara_xe))
;(assert (id-wsd_viBakwi ?id3 ne))
;(assert (id-wsd_viBakwi ?id1 meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate4   "  ?id "  Pera_baxala_kara_xe )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  rotate.clp 	rotate4   "  ?id3 "  ne )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  rotate.clp 	rotate4   "  ?id1 "  meM )" crlf))
;)

;##################################default-rule##########################

;@@@ Added by Anita-05-06-2014
;The wheel rotates around an axle. [cambridge dictionary]
;पहिया धुरी के चारों ओर घूमता है ।
;While the fan rotates and its axis moves sidewise this point is fixed. [By mail] [parse problem]
;जब पंखा घूमता है और उसकी धुरी साथ-साथ चलती है तो इसका बिन्दु निश्चित होता है ।
(defrule rotate_default-rule
(declare (salience 0))
(id-root ?id rotate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotate.clp     rotate_default-rule   "  ?id "  GUma )" crlf))
)
