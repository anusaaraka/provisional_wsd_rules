
;$$$ Modified by 14anu-ban-03 (19-03-2015)   ;changed word meaning to root meaning
;@@@ Added by 14anu02 on 21.6.14
;He belongs to a closed family.   ;working on parser no.- 2
;वह एक बंधे हुए परिवार का सदस्य है . 
;"closed","Adj","1.banXA_huA"
(defrule closed0
(declare (salience 5000))
(id-root ?id close|closed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) family)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banXA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  closed.clp  	closed0   "  ?id "  banXA_huA )" crlf))
)


;$$$ Modified by 14anu-ban-03 (19-03-2015)   ;changed word meaning to root meaning
;@@@ Added by 14anu02 on 21.6.14
;A closed society.
;He has a closed mind.
;उसके एक सङ्कीर्ण मन है . 
(defrule closed2
(declare (salience 5000))	
(id-root ?id close|closed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) society|mind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkIrNa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  closed.clp  	closed2   "  ?id "  saMkIrNa )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)   ;changed word meaning to root meaning
;@@@ Added by 14anu02 on 21.6.14
;The club has a closed membership.
;क्लब की एक सीमित सदस्यता है . 
(defrule closed3
(declare (salience 5000))	
(id-root ?id close|closed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) membership)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImiwa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  closed.clp  	closed3   "  ?id "  sImiwa )" crlf))
)

;---------------------------------Default rule------------------------------------

;$$$ Modified by 14anu-ban-03 (19-03-2015)   ;changed word meaning to root meaning 
;@@@ Added by 14anu02 on 21.6.14
;Keep the door closed.
;दरवाजा बन्द रखिए.
;The museum is closed on Mondays.
(defrule closed1
(declare (salience 4800))	
(id-root ?id close|closed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(or(object-object_samAnAXikaraNa ? ?id)(subject-subject_samAnAXikaraNa ? ?id))  ;commented by 14anu-ban-03 (19-03-2015) as there is no default rule in file
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  closed.clp  	closed1   "  ?id "  banxa )" crlf))
)

