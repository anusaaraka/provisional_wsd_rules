;##############################################################################
;#  Copyright (C) 2014-2015 14anu03 Akshay Singh (akubuzz@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu03 on 26-june-14
;Stay loyal and I will share my kingdom with you.
;स्वामिभक्त रहो और मैं आपके साथ मेरा राज्य साझा करुँगा . 
(defrule kingdom1
(declare (salience 0))
(id-root ?id kingdom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_course ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " kingdom.clp  kingdom1  " ?id "   rAjya  )" crlf))
)

;$$$ Modified by 14anu-ban-07,(11-12-2014)
;@@@ Added by 14anu03 on 26-june-2014
;A bomb that could blow us to kingdom come.
;जो दूसरी दुनिया में ले जाये एक ऐसा बम है . 
;एक ऐसे बम जो  हमें  स्वर्ग में पहुँचा दे . ;added by 14anu-ban-07 (11-12-2014)
(defrule kingdom2
(declare (salience 5500))
(id-root ?id kingdom)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 come)
(test (=(+ ?id 1) ?id1))
(kriyA-to_saMbanXI ?id2 ?id)
(id-word ?id2 blow)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 svarga_meM_pahuzcA_xe))  ;meaning changed from xUsarI_xuniyA_meM_le_jAye to svarga_meM_pahuzcA_xe and added id2 in affected ids by 14anu-ban-07 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  kingdom.clp     kingdom2   "  ?id "  " ?id1 " " ?id2 "  xUsarI_xuniyA_meM_le_jAye )" crlf))
)

;$$$ Modified by 14anu-ban-07 (12-12-2014)
;@@@ Added by 14anu03 on 26-june-2014
;You can complain till kingdom come.
;जबतक राज्य अा नहीं हो जाता आप शिकायत कर सकते हैं.
;प्रलय काल तक आप शिकायत कर सकते हैं.   ;aded by 14anu-ban-07 (12-12-2014)
(defrule kingdom3
(declare (salience 5400))
(id-root ?id kingdom)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 come)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pralaya_kAla));meaning changed from rAjya_A_nahIM_jAwA to pralaya kAla ,by 14anu-ban-07 (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  kingdom.clp     kingdom3   "  ?id "  " ?id1 "  pralaya_kAla )" crlf))
)
