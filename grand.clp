;@@@ Added by 14anu-ban-05 on (18-02-2015)
;A year and a half later, Kilian returned to Houston with the grand idea of opening a serious literary bookstore.[COCA]
;देढ साल के बाद, किलीअन एक  साहित्यिक किताबों की दुकान खोलने की उच्च विचार के साथ ह्यूस्टन में लौट आए.	[manual]

(defrule grand2
(declare (salience 5001))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 idea)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand2   "  ?id "  ucca )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-02-2015)
;I had a grand day out at the seaside. [OALD]
;समुद्र के किनारे पर मेरा  एक  दिन शानदार था . 	[manual]
;Qutub Minar is one of the most grand memories of Delhi .[tourism]
;kuwuba mInAra , xillI kI sabase SAnaxAra yAxagAroM meM se eka hE .[tourism]

(defrule grand3
(declare (salience 5001))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 day|party|show|memory|history|attempt|enhancement)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand3   "  ?id "  SAnaxAra )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-02-2015)
;You've done a grand job.[CALD]
;तुमने  बढ़िया काम किया है.	[manual]

(defrule grand4
(declare (salience 5001))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 job|work|weather|option)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand4   "  ?id "  baDZiyA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-02-2015)
;My grandson is a grand little chap.[cald]
;मेरा पोता एक सुंदर छोटा लडका है. [manual]
;Note kindly remove (id-root ?id1 chap) after adding 'chap' to animate.gdbm 

(defrule grand5
(declare (salience 5001))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-root ?id1 chap))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand5   "  ?id "  suMxara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-02-2015)
;From simple to grand, each type of food providing restaurants are available .[tourism]
;sAmAnya se SAnaxAra waka, hara waraha kA Bojana karAne vAle reswrAz hEM .[tourism]

(defrule grand6
(declare (salience 5002))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand6   "  ?id "  SAnaxAra )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-02-2015)
;Due to being famous for grand woollen clothes and hand made clothes , Panipat city of Haryana has been given the name of the city of the weavers .[TOURISM]
;SAnaxAra UnI vaswroM evaM hAWa se bane kapadZoM ke lie prasixXa hone kI vajaha se hariyANA ke Sahara pAnIpawa ko `` julAhoM kI nagarI `` kI saMjFA xI gaI hE .[TOURISM]

(defrule grand7
(declare (salience 5003))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id2 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand7   "  ?id "  SAnaxAra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-02-2015)
;This was the decision of a grand jury.[COCA]
;यह एक  प्रसिद्ध   न्यायपीठ का निर्णय था .      [Manual]

(defrule grand8
(declare (salience 5001))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 jury)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasixXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand8   "  ?id "  prasixXa )" crlf))
) 

;------------------------ Default Rules ----------------------

(defrule grand0
(declare (salience 5000))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bavya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand0   "  ?id "  Bavya )" crlf))
)

;"grand","Adj","1.badZA"
;The opening ceremony was a grand show.
(defrule grand1
(declare (salience 4900))
(id-root ?id grand)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grand.clp 	grand1   "  ?id "  badZA )" crlf))
)

