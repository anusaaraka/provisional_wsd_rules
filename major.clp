;@@@ Added by 14anu-ban-08 (27-11-2014)
;We have yet to answer one major question in this connection ; What is the analog of mass in rotational motion?     [NCERT]
;इस सम्बन्ध में एक मुख्य प्रश्न का उत्तर देना अभी शेष है कि घूर्णी गति में द्रव्यमान के समतुल्य राशि क्या है?     [NCERT]
(defrule major0
(declare (salience 0))
(id-root ?id major)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  major.clp 	major0   "  ?id "  muKya )" crlf))
)

;@@@Added by 14anu-ban-08 (27-11-2014)
;Table1.1 lists some of the great physicists, their major contribution and the country of origin.    [NCERT]
;सारणी 1.1 में कुछ महान भौतिक विज्ञानियों, उनके प्रमुख योगदानों तथा उनके मूल देशों की सूची दी गई है.    [NCERT]
(defrule major1
(declare (salience 100))
(id-root ?id major)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 contribution)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  major.clp 	major1   "  ?id "  pramuKa )" crlf))
)
