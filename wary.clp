
;@@@ Added by 14anu-ban-11 on (20-04-2015)
;He gave her a wary look. (oald)
;उसने उसको चौकन्नी नजर दी . (self)
(defrule wary1
(declare (salience 10))
(id-root ?id wary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 look)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOkannA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wary.clp 	wary1   "  ?id "  cOkannA)" crlf))
)

;------------------------------ Default Rules -------------------------------

;@@@ Added by 14anu-ban-11 on (20-04-2015)
;Be wary of strangers who offer you a ride.(oald)
;अजनबीओ से होशियार रहिए जो आपको  सवारी प्रस्ताव देते हैं . (self)
(defrule wary0
(declare (salience 00))
(id-root ?id wary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hoSiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wary.clp     wary0   "  ?id "  hoSiyAra)" crlf))
)


