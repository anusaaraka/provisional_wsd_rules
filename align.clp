;@@@Added by 14anu-ban-02(09-03-2015)
;In the presence of an external field B0, which is strong enough, and at low temperatures, the individual atomic dipole moment can be made to align and point in the same direction as B0.[ncert 12_05]
;पर्याप्त शक्तिशाली बाह्य चुम्बकीय क्षेत्र B0 की उपस्थिति में एवं निम्न तापों पर अलग-अलग परमाणुओं के द्विध्रुव आघूर्ण सरल रेखाओं में और B0 की दिशा के अनुदिश
; संरेखित किए जा सकते हैं.[ncert]
;चुम्बकीय क्षेत्र B0 की उपस्थिति में,जो कि पर्याप्त शक्तिशाली है,एवं निम्न तापों पर अलग-अलग परमाणुओं के द्विध्रुव आघूर्ण सरल रेखाओं में और B0 की दिशा के अनुदिश 
;संरेखित किए जा सकते हैं.[self]

(defrule align1 
(declare (salience 100)) 
(id-root ?id align) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-to_saMbanXI  ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id saMreKiwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  align.clp  align1  "  ?id "  saMreKiwa_kara )" crlf)) 
)
 
;@@@Added by 14anu-ban-02(09-03-2015)
;The top and bottom line of each column on the page should align.[oald]
;पृष्ठ पर हर एक खण्ड  की ऊपर और नीचे वाली रेखा को सीध में होना चाहिए . [self]
(defrule align2 
(declare (salience 100)) 
(id-root ?id align) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 column)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sIXa_meM_ho)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  align.clp  align2  "  ?id "  sIXa_meM_ho )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(09-03-2015)
;Sentence: The two parts of the machine are not properly aligned.[mw]
;Translation:मशीन के दो भाग उचित ढङ्ग से पड्क्ति में नहीं लगे हैं . [self]
(defrule align0
(declare (salience 0))
(id-root ?id align)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id padkwi_meM_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  align.clp  align0  "  ?id "  padkwi_meM_laga )" crlf))
)

