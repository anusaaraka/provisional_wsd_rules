
(defrule flicker0
(declare (salience 5000))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buJa_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flicker.clp flicker0 " ?id "  buJa_jA )" crlf)) 
)

(defrule flicker1
(declare (salience 4900))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buJa_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flicker.clp flicker1 " ?id "  buJa_jA )" crlf)) 
)

(defrule flicker2
(declare (salience 4800))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buJa_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flicker.clp flicker2 " ?id "  buJa_jA )" crlf)) 
)

(defrule flicker3
(declare (salience 4700))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 buJa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flicker.clp	flicker3  "  ?id "  " ?id1 "  buJa_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-03-2015)
;She felt a flame of anger flicker and grow. [OALD] 
;उसने क्रोध के आवेग को झलकता हुआ और बढ़ता हुआ  महसूस किया. [manual]  ;run on parser 3 of multiparse
(defrule flicker6
(declare (salience 4601))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flicker.clp 	flicker6   "  ?id "  Jalaka )" crlf))
)


;@@@ Added by 14anu-ban-05 on (26-03-2015)
;Her eyelids flickered as she slept.[OALD]
;उसकी पलकें  फड़फड़ाई जैसे ही वह सोई . 	[MANUAL]
(defrule flicker7
(declare (salience 4602))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 eyelid)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PadZaPadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flicker.clp 	flicker7   "  ?id "  PadZaPadZA )" crlf))
)

;------------------------ Default Rules ---------------------

(defrule flicker4
(declare (salience 4600))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flicker.clp 	flicker4   "  ?id "  camaka )" crlf))
)

;"flicker","N","1.ASA kI kiraNa"
;There was a flicker of hope in him for the job that he had recently applied for.
(defrule flicker5
(declare (salience 4500))
(id-root ?id flicker)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASA_kI_kiraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flicker.clp 	flicker5   "  ?id "  ASA_kI_kiraNa )" crlf))
)

