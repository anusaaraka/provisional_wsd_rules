;@@@Added by 14anu-ban-02(20-02-2015)
;He's the baby of the team.[oald]
;वह दल का अनुभवहीन खिलाडी है . [anusaaraka]
(defrule baby2
(declare (salience 100)) 
(id-root ?id baby) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1) 
(id-root ?id1 team)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anuBavahIna_KilAdZI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  baby.clp  baby2  "  ?id "  anuBavahIna_KilAdZI )" crlf)) 
) 

;------------------------ Default Rules ----------------------
;Modified (SiSU  as SiSu) by Shirisha Manju (Suggested by Sukhada 07-06-13)
(defrule baby0
(declare (salience 0))	;salience reduce from 5000 to 0 by 14anu-ban-02(20-02-2015)
(id-root ?id baby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiSu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  baby.clp 	baby0   "  ?id "  SiSu )" crlf))
)

;"baby","VT","1.ke_sAWa_bacce_jEsA_vyavahAra_karanA"
;Even though Rakesh is now six year old his parents still baby him.
(defrule baby1
(declare (salience 4900))
(id-root ?id baby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa_bacce_jEsA_vyavahAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  baby.clp 	baby1   "  ?id "  ke_sAWa_bacce_jEsA_vyavahAra_kara )" crlf))
)
