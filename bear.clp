;@@@ Added by 14anu05 GURLEEN BHAKNA on 25.06.14
;He can't bear to be laughed at.
;वह खुद पर हँसा जाना बरदाश्त नहीं कर सकता है .
(defrule bear19
(declare (salience 4000))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraxASwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear19   "  ?id "  baraxASwa_kara )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 25.06.14
;I can't bear having cats in the house.
;मैं घर में बिल्लियों का होना बरदाश्त नहीं कर सकता हूँ . 
(defrule bear16
(declare (salience 4000))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_karma  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraxASwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear16   "  ?id "  baraxASwa_kara )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 25.06.14
;I can't bear him sleeping all day.
;मैं उसको पूरा दिन सोते हुऐ बरदाश्त नहीं कर सकता हूँ .
(defrule bear17
(declare (salience 4000))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraxASwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear17   "  ?id "  baraxASwa_kara )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 25.06.14
;His pain was a lot more than he could bear.
;उसका दर्द सहन करने से बहुत ज्यादा था.
(defrule bear18
(declare (salience 5500))
(id-word ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
;(id-cat_coarse ?id1 noun)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear18   "  ?id "  sahana_kara )" crlf))
)

(defrule bear0
(declare (salience 5000))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id born )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janma))
(assert (id-H_vib_mng ?id yA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bear.clp  	bear0   "  ?id "  janma )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  bear.clp       bear0   "  ?id " yA )" crlf))
)

(defrule bear1
(declare (salience 5500))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sahayoga_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear1  "  ?id "  " ?id1 "  sahayoga_xe  )" crlf))
)

;If you put in an action against him,i will bear you out.
;yaxi wuma usake virUxXa kAryavAhI karoge wo mEM BI wumhe sahayoga xUzgA
(defrule bear2
(declare (salience 4800))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-down_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraxASwa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bear.clp bear2 " ?id "  baraxASwa_kara )" crlf)) 
)

(defrule bear3
(declare (salience 4700))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baraxASwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear3  "  ?id "  " ?id1 "  baraxASwa_kara  )" crlf))
)

(defrule bear4
(declare (salience 4600))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bear.clp bear4 " ?id "  sambanXa_kara )" crlf)) 
)

(defrule bear5
(declare (salience 4500))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sambanXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear5  "  ?id "  " ?id1 "  sambanXa_kara  )" crlf))
)

(defrule bear6
(declare (salience 4400))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAbiwa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bear.clp bear6 " ?id "  sAbiwa_kara )" crlf)) 
)

(defrule bear7
(declare (salience 4300))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAbiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear7  "  ?id "  " ?id1 "  sAbiwa_kara  )" crlf))
)

(defrule bear8
(declare (salience 4200))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAhasa_raKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bear.clp bear8 " ?id "  sAhasa_raKa )" crlf)) 
)

(defrule bear9
(declare (salience 4100))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAhasa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear9  "  ?id "  " ?id1 "  sAhasa_raKa  )" crlf))
)

(defrule bear10
(declare (salience 4000))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-with_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahana_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bear.clp bear10 " ?id "  sahana_kara )" crlf)) 
)

(defrule bear11
(declare (salience 3900))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sahana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bear.clp	bear11  "  ?id "  " ?id1 "  sahana_kara  )" crlf))
)

;Added by Aditya and Hardik(8.7.13),IIT(BHU)
;They will have to bear him.
;She can not bear the smell of petrol.
(defrule bear15
(declare (salience 3750))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(or(id-word ?id1 pain|burden|smell|pressure|load|weight|trouble|cost|threats|consequences|allegations|blame|harshness|anger|loss|shame|sock)(id-cat_coarse ?id1 pronoun))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear15   "  ?id "  sahana_kara )" crlf))
)

;@@@ Added by 14anu-ban-02 (08-12-2014)
;The grubs bore into the wood of trees or sometimes into the roots or pith of herbaceous plants.[agriculture]
;सूण्डी पेडों की लकडी में या कभी कभी औषधेय पौधों के मूल  या मज्जा मे  बेधन करती है.[manual]
(defrule bear20
(declare (salience 3750))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 wood|tree|root)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id beXana_kara))
(assert (id-wsd_root ?id bore))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear20   "  ?id "  beXana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root   " ?*prov_dir* "  bear.clp     bear20   "  ?id " bore )" crlf))
)
;@@@Added by 14anu-ban-02(25-02-2015)
;It may be borne in mind that if an equation fails this consistency test, it is proved wrong, but if it passes, it is not proved right.[ncert 11_02]
;यह बात भी हमें स्पष्ट करनी चाहिए कि यदि कोई समीकरण सङ्गति परीक्षण में असफल हो जाती है तो वह गलत सिद्ध हो जाती है, परन्तु यदि वह परीक्षण में सफल हो जाती है तो इससे वह सही सिद्ध नहीं हो जाती.[ncert]
(defrule bear21
(declare (salience 3750))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 mind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear21   "  ?id "  spaRta_kara )" crlf))
)

;----------------------------- Default rules ---------------------
(defrule bear12
(declare (salience 3800))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAlU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear12   "  ?id "  BAlU )" crlf))
)

;passive_-_- && category=verb	janma_le	0
(defrule bear13
(declare (salience 3700))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear13   "  ?id "  janma )" crlf))
)

;He was born: janmA_WA && not janma_liyA_WA
(defrule bear14
(declare (salience 3600))
(id-root ?id bear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bear.clp 	bear14   "  ?id "  XAraNa_kara )" crlf))
)


;"bear","VT","1.XAraNa karanA"
;She bears the title of Duchess.
;--"2.barxASwa_karanA"
;I cannot bear his constant criticism.
;--"3.vahana_karanA"
;She is bearing his child.
;
;
;LEVEL 
;Headword : bear
;
;Examples --
;
;"bear","N","1.BAlU"
;I saw a bear in the zoo.
;mEMne cidZiyAGara meM eka BAlU xeKA.
;
;"bear","VT","1.XAraNa karanA"
;She bears the title of Duchess.
;vaha 'dacesa' kA KiwAba XAraNa kiye hue hE. <---apane Upara lenA <-- vahana karanA
;--"2.JelanA"
;I cannot bear his constant criticism.
;mEM lagAwAra honevAlI AlocanaWawawa Jela nahIM sakawI. <--apane BIwara vahana karanA
;--"3.vahana_karanA"
;Ram bore the expenses of his brother's daughter.
;apane BAI kI betI kI SAxI ke Karca kA BAra rAma ne vahana kiyA.
;--"4.janma_xenA"
;Sita bore hari three daughters.
;sIwA ne hari ko wIna betiyAz janIM. <--baccoM ko apane anxara vahana kara janma xenA
;
;sUwra : BAlU/janana^JelanA`
;
