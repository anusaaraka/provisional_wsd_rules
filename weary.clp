;@@@ Added by 14anu-ban-11 on (12-03-2015)
;She suddenly felt old and weary.(oald)
;उसने अचानक वृद्ध और थका हुआ महसूस किया .(self) 
(defrule weary2
(declare (salience 5001))
(id-root ?id weary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weary.clp 	weary2   "  ?id "  WakA_ho)" crlf))
)

;@@@ Added by 14anu-ban-11 on (12-03-2015)
;A weary journey.(oald)
;एक उबाऊ यात्रा .(self)
(defrule weary3
(declare (salience 5002))
(id-root ?id weary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 journey)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ubAU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weary.clp 	weary3   "  ?id "  ubAU)" crlf))
)

;@@@ Added by 14anu-ban-11 on (12-03-2015)
;She soon wearied of his stories. (oald)
;वह शीघ्र उसकी कहानियों से ऊब गई . (self)
(defrule weary4
(declare (salience 4901))
(id-root ?id weary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI  ?id ?id1)
(id-root ?id1 story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Uba_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weary.clp 	weary4   "  ?id "  Uba_jA)" crlf))
)

;------------------------ Default Rules ----------------------


;"weary","Adj","1.klAnwa"
;He looked weary after the day's hard work.
;People are weary of change of governments.
(defrule weary0
(declare (salience 5000))
(id-root ?id weary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weary.clp 	weary0   "  ?id "  klAnwa )" crlf))
)

;;"weary","V","1.WakAnA"
;The boy was wearied of pedalling the cycle.
(defrule weary1
(declare (salience 4900))
(id-root ?id weary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weary.clp 	weary1   "  ?id "  WakA )" crlf))
)



;"weary","V","1.WakAnA"
;The boy was wearied of pedalling the cycle.
;"weary","Adj","1.klAnwa"
;He looked weary after the day's hard work.
;People are weary of change of governments.
;--"2.ubAU"
;It was a weary journey by train for two days.


 
 

