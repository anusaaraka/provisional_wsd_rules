;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################

;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Addded by 14anu26  [27-06-14]
;the stigma of mental disorder.
;मानसिक रोग का कलंक.
;मानसिक रोग का चिह्न.[Translation improved by 14anu-ban-01 ]
(defrule stigma0
(declare (salience 0))
(id-root ?id stigma)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  cinha))                ;changed " kalaMka" to "cinha" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stigma.clp 	stigma0   "  ?id "  cinha )" crlf))
)                                     ;changed " kalaMka" to "cinha" by 14anu-ban-01 on (12-01-2015)

;@@@ Addded by 14anu26  [27-06-14]
;A given pollen grain blowing in the wind is thus unlikely to land on a receptive stigma. 
;इस प्रकार एक दिया हुआ पराग कण हवा में उड़ते हुए एक ग्रहणशील वर्तिकाग्र पर उतरना असम्भव है .
(defrule stigma1
(declare (salience 400))
(id-root ?id stigma)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-on_saMbanXI ? ?id)(kriyA-to_saMbanXI ? ?id)(kriyA-on_saMbanXI ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  varwikAgra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stigma.clp 	stigma1   "  ?id "   varwikAgra )" crlf))
)

;commented by  14anu-ban-01 on (12-01-2015) because "stigmatised" has root "stigmatise" and not "stigma".Also the required meaning for the word is coming from the dictionary.
;@@@ Addded by 14anu26  [27-06-14]
;The institution was stigmatised due to the corruption.
;संस्था भ्रष्टाचार के कारण  कलंकित करी गयी थी .
;(defrule stigma2
;(declare (salience 500))
;(id-root ?id stigma)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id stigmatised)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id kalaMkiwa_kara ))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  stigma.clp 	stigma2   "  ?id "  kalaMkiwa_kara )" crlf))
;)
