;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;Parallel experiments are being conducted in both countries.
;दोनो देशों में समान प्रयोग चलाए जा रहे हैं . 
(defrule parallel0
(declare (salience 3000)) ;salience decreases by 14anu-ban-09
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel0   "  ?id " samAna )" crlf))
)


;There are a number of parallels between our two situations.
;हमारी दो परीस्थितियों के बीच बहुत सारी समानता हैं . 
(defrule parallel1
(declare (salience 8000))
(id-word ?id parallels)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-between_saMbanXI ?id ?id1)(viSeRya-with_saMbanXI  ?id ?id1))
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel1   "  ?id " samAnawA )" crlf))
)

;By symmetry, the electric field will not depend on y and z coordinates and its direction at every point must be parallel to the x-direction.
;सममिति के अनुसार विद्युत क्षेत्र y तथा z निर्देशाङ्कों पर निर्भर नहीं करेगा तथा इसकी प्रत्येक बिन्दु पर दिशा x - दिशा के समानान्तर होनी चाहिए . 
(defrule parallel2
(declare (salience 7000))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel2   "  ?id " samAnAnwara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
;My ideas are parallel to yours. [rajpal]
;मेरे विचार आपके समान हैं .
(defrule parallel3
(declare (salience 7000))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?verb ?id)
(kriyA-subject  ?verb ?id1)
(id-root ?id1 idea|plan|view|goal|opinion|concept|proposal|objective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel3   "  ?id " samAna )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
;Mr Rajiv is without a parallel. [rajpal]
;मिस्टर राजीव अतुलनीय हैं . 
(defrule parallel4
(declare (salience 7000))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-without_saMbanXI  ? ?id)
(id-root ?id1 without)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 awulanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " parallel.clp  parallel4  "  ?id "  " ?id1 "  awulanIya  )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
;You can not parallel his success. [rajpal]
;अाप उसकी सफलता की तुलना नहीं कर सकते .
(defrule parallel5
(declare (salience 7000))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 success|victory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel5   "  ?id " wulanA )" crlf))
)

;NOTE-There is some problem regarding the symbol (b) in the 2nd sentence which Roja Mam gona see on Wednesday. She mentioned the symbol in her program. After correction, even in the 2nd sentence(2nd occurance) of "parallel" the rule will fire. Otherwise the rule is ok!
;@@@ Added by 14anu-ban-09 on (18-08-2014)
;Then we draw a line from the head of A parallel to B and another line from the head of B parallel to A to complete a parallelogram OQSP. [NCERT CORPUS] 
;फिर हम A के शीर्ष से B के समान्तर एक रेखा खीँचते हैं और B के शीर्ष से A के समान्तर एक दूसरी रेखा खीँचकर समान्तर चतुर्भुज OQSP पूरा करते हैं . [NCERT CORPUS]

(defrule parallel10
(declare (salience 4500))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel10 "  ?id " samAnwara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-08-2014)
;Notice that this is unlike the force due to an electric field, qE, which can have a component parallel (or antiparallel) to motion and thus can transfer energy in addition to momentum. [NCERT CORPUS] Added by 14anu-ban-09 on (24-11-2014)
;XyAna xIjie, yaha vixyuwa kRewra ke kAraNa bala, @qE, se Binna hE, jisakA gawi ke samAnwara (aWavA prawisamAnwara) avayava ho sakawA hE Ora isa prakAra saMvega ke sAWa-sAWa UrjA ko BI sWAnAnwariwa kara sakawA hE. [NCERT CORPUS] Added by 14anu-ban-09 on (24-11-2014)
;This means that if a force is not parallel to the velocity of the body, but makes some angle with it, it changes only the component of velocity along the direction of force. [NCERT CORPUS] Added by 14anu-ban-09 on (19-11-2014)
;isakA arWa yaha huA ki yaxi koI bala piNda ke vega ke samAnwara nahIM hE, varan usase koI koNa banAwA hE, waba vaha kevala bala kI xiSA meM vega ke Gataka ko parivarwiwa karawA hE. [NCERT CORPUS] ;Added by 14anu-ban-09 on (19-11-2014)
;The component parallel to the surfaces in contact is called friction. [NCERT CORPUS]
;samparka - pqRToM ke samAnwara Gataka ko GarRaNa bala kahawe hEM. [NCERT CORPUS]
;Friction, by definition, is the component of the contact force parallel to the surfaces in contact, which opposes impending or actual relative motion between the two surfaces.  [NCERT CORPUS]
;pariBARA ke anusAra, GarRaNa bala samparka bala kA samparka pqRToM ke samAnwara Gataka howA hE, jo xo pqRToM ke bIca samupasWiwa aWavA vAswavika ApekRa gawi kA viroXa karawA hE. [NCERT CORPUS]

(defrule parallel11
(declare (salience 4800))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(viSeRya-to_saMbanXI ?id ?id1)
;(id-cat_coarse ?id noun) ;commented by 14anu-ban-09 on (19-11-2014)
(id-root ?id1 surface|velocity|motion) ;added 'velocity' by 14anu-ban-09 on (19-11-2014) ;added 'motion' by 14anu-ban-09 on (24-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel11 "  ?id " samAnwara )" crlf))
)


;@@@ Added by 14anu-ban-09 on (20-08-2014)
;Let this velocity have a component vp parallel to the magnetic field and a component vn normal to it. [NCERT CORPUS] ;added by 14anu-ban-09 on (28-11-2014) 
;mAna lIjie isa vega kA cumbakIya kRewra ke samAnwara avayava @vp waWA isa kRewra ke aBilambavawa avayava @vn hE. [NCERT CORPUS] ;added by 14anu-ban-09 on (28-11-2014) 
;We have already mentioned that we are developing the study of rotational motion parallel to the study of translational motion with which we are familiar. [NCERT CORPUS]
;hama pahale hI yaha ulleKa kara cuke hEM ki GUrNI gawi kA aXyayana hama sWAnAnwaraNa gawi ke samAnwara hI calAyefge.

(defrule parallel12
(declare (salience 4800))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 motion|component) ;Added 'motion' by 14anu-ban-09 on (19-11-2014);added 'component' by 14anu-ban-09 on (28-11-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel12 "  ?id " samAnwara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-08-2014)
;Figure 9.3 shows what happens when a parallel beam of light is incident on (a) a concave mirror, and (b) a convex mirror. [NCERT CORPUS] ;added by 14anu-ban-09 on (15-11-2014)
;ciwra 9.3 meM xarSAyA gayA hE ki jaba koI samAnwara prakASa-puFja kisI (@a) avawala xarpaNa waWA (@b) uwwala xarpaNa, para Apawiwa howA hE wo kyA howA hE. [NCERT CORPUS] ;added by 14anu-ban-09 on (15-11-2014)
;ciwra 9.3 meM xarSAyA gayA hE ki jaba koI samAnAnwara prakASa-puFja kisI (@a) avawala xarpaNa waWA (@b) uwwala xarpaNa, para Apawiwa howA hE wo kyA howA hE. [Self] ;added by 14anu-ban-09 on (15-11-2014)
;As shown in the Fig. 7.31, z and z′ are two parallel axes separated by a distance a.  [NCERT cORPUS]
;jEsA ki ciwra 7.31 meM xarSAyA gayA hE @z evaM @z′ xo sAmAnAnwara akReM hEM jinake bIca kI xUrI @a hE .
;It allows to find the moment of inertia of a body about any axis, given the moment of inertia of the body about a parallel axis through the center of mass of the body.  [NCERT CORPUS]
;yaxi kisI piNda kA jadawva AGUrNa usake guruwva kenxra se gujarane vAlI akRa ke pariwaH jFAwa ho, wo usa akRa ke sAmAnAnwara kisI xUsarI akRa ke pariwaH jadawva AGUrNa hama isa prameya kI sahAyawA se jFAwa kara sakawe hEM.

(defrule parallel13
(declare (salience 4900))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 axis|beam) ;added 'beam' 14anu-ban-09 on (15-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel13 "  ?id " samAnAnwara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-08-2014)
;However, if two equal and opposite deforming forces are applied parallel to the cross-sectional area of the cylinder, as shown in Fig. 9.2(b), there is relative displacement between the opposite faces of the cylinder. [NCERT CORPUS]
;yaxi xo barAbara Ora viroXI virUpaka bala belana ke anuprasWa paricCexa ke samAMwara lagAe jAez, jEsA ciwra 9.2(@b) meM xiKAyA gayA hE, wo belana ke sammuKa PalakoM ke bIca sApekRa visWApana ho jAwA hE.

(defrule parallel14
(declare (salience 5000))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 apply)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAMwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel14 "  ?id " samAMwara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-08-2014)
;This force will cause the fluid to flow parallel to the surface.[NCERT CORPUS]
;यह बल तरल को पृष्ठ के समांतर बहने के लिए बाध्य करता है.

(defrule parallel15
(declare (salience 4500))
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 fluid)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAMwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel15 "  ?id " samAMwara )" crlf))
)

;############################################## Default Rules ##########################################################
;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
(defrule parallel6
(declare (salience 4000)) ;Salience reduced because of default rule by 14anu-ban-09 on (19-11-2014)
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel6   "  ?id " wulanA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
(defrule parallel7
(declare (salience 3000)) ;Salience reduced because of default rule by 14anu-ban-09 on (19-11-2014)
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel7   "  ?id " wulanIya )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 27-3-2014
(defrule parallel8
(declare (salience 3500)) ;Salience reduced because of default rule by 14anu-ban-09 on (19-11-2014)
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barAbarI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel8   "  ?id " barAbarI_kara )" crlf))
)


;The road and the canal are parallel to each other.
;सडक और नहर एक दूसरे के समानान्तर हैं . 
(defrule parallel9
(id-root ?id parallel)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAnAnwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  parallel.clp 	parallel9   "  ?id " samAnAnwara )" crlf))
)
