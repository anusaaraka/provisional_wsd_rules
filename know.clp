
(defrule know0
(declare (salience 5000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id knowing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAnakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  know.clp  	know0   "  ?id "  jAnakAra )" crlf))
)

;"knowing","Adj","1.jAnakAra"
;He is a knowing collector of rare books.
;It was a knowing attempt to defraud.
;She gave me a knowing look when I mentioned about him.



; 
(defrule know1
(declare (salience 4900))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id knowing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAnanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  know.clp  	know1   "  ?id "  jAnanA )" crlf))
)

;"knowing","N","1.jAnanA"
;There is a big difference in knowing && understanding.



;
(defrule know2
(declare (salience 4800))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id knowing )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAnawe_huye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  know.clp  	know2   "  ?id "  jAnawe_huye )" crlf))
)

;"knowing","V","1.jAnawe_huye"
;Knowing all the facts I will  not hold this against you.
;



;Added by Meena(24.9.09) salience in know3 should be higher than know4 
; At once he knew that they were thieves.
(defrule know3
(declare (salience 4900))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id knew)
(id-root =(- ?id 2) once)
(id-root =(- ?id 3) at)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know3   "  ?id "  pahacAna_jA )" crlf))
)



;Added by Meena(24.9.09)
;They knew him very well.
(defrule know4
(declare (salience 4800))
;(declare (salience 1500))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp 	know4   "  ?id "  jAna )" crlf))
)

;commented by 14anu-ban-02
;correct meaning is coming from know4
;Added by Meena(22.01.10)
;I want to know about it.
;(defrule know5
;(declare (salience 1000))
;(id-root ?id know)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-kqxanwa_karma  ?id1 ?id)
;(kriyA-about_saMbanXI  ?id ?id2)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mAlUma_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know5   "  ?id "  mAlUma_kara )" crlf))
;)



;Added by Meena(22.01.10)
;I want her to know about it.
(defrule know6
(declare (salience 3000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id2 =(- ?id 2))
(kriyA-about_saMbanXI  ?id ?id1)
(kriyA-aBiprewa_kriyA  ?id2 ?id)
(kriyA-karwA  ?id =(- ?id 2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAlUma_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know6   "  ?id "  mAlUma_ho )" crlf))
)


(defrule know7
(declare (salience -1000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know7   "  ?id "  jAna )" crlf))
)


;"know","V","1.jAnanA"
;I want to know who is winning the game.
;I know that the President lied to the people.
;She knows how to knit.
;Galileo knew that the earth moves around the sun.
;I know that I left the key on the table.
;We know this movie.
;His greed knew no limits.
;This student knows her irregular verbs.
;I know Latin.
;Adam knew Eve.
;The child knows right from wrong.
;I know this voice.
;
;

;Added by Prachi Rathore[2-12-13]
;Indeed, our many questions about the heavens have received reasonably satisfactory answers from the laws of science [known] to us today.
;वास्तव में, विज्ञान के जो सिद्धांत हमें आज मालूम हैं, उनसे हमें आकाश संबंधी कई प्रश्नों के संतोषजनक उत्तर मिल गये हैं।
(defrule know8
(declare (salience 5000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAlUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know8   "  ?id "  mAlUma )" crlf))
)

;Added by Prachi Rathore[3-12-13]
;Paradoxically, scientists seem to [know] more about the stars which are far away than about the planets of our own solar system.
;यह अजीब बात है कि वैज्ञानिकों को हमारे अपने सौरमंडल के ग्रहों के मुकाबले उन तारों के बारे में ज्यादा जानकारी है जो बहुत दूरी पर स्थित हैं।
(defrule know9
(declare (salience 5000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp      know9   "  ?id "  jAnakArI )" crlf))
)

;@@@ Added by Prachi Rathore[9-1-14]
;If you want to call a meeting or anything, just let me know.
(defrule know10
(declare (salience 5000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(kriyA-vAkyakarma  ?id2 ?id)
(id-root ?id2 let)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 bawA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " know.clp 	know10  "  ?id "  " ?id2 "  bawA_xe  )" crlf))
)

;@@@ Added by 14anu-ban-07,(26-08-2014)
;Since D is known, the diameter d of the planet can be determined using Eq. (2.2).  (ncert corpus)
;क्योङ्कि D का मान ज्ञात है, अतः ग्रह के व्यास d का मान समीकरण (2.2) की सहायता से ज्ञात किया जा सकता है.
(defrule know11
(declare (salience 4900))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jFAwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp 	know11   "  ?id "  jFAwa_ho )" crlf))
)
;$$$Modified by 14anu-ban-02(22-02-2016)
;###[COUNTER EXAMPLE]He is known as the Einstein of India .[sd_verified]
;###[COUNTER EXAMPLE]वह भारत के आइंस्टाइन के रूप में जाना जाता है . [self]
;@@@ Added by 14anu-ban-07 (03-11-2014)
;A complete set of these units, both the base units and derived units, is known as the system of units. (ncert)
;मूल-मात्रकों और व्युत्पन्न मात्रकों के सम्पूर्ण समुच्चय को मात्रकों की प्रणाली (या पद्धति) कहते हैं.(ncert)
(defrule know12
(declare (salience 5000))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 system)	;Added by 14anu-ban-02(22-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp 	know12   "  ?id "  kaha )" crlf))
)

;@@@ Added by 14anu-ban-07 (03-12-2014)
;These larvae, which are known as white grubs, are easily recognized by their broad and fleshy appearance, white or grayish white color together with the C-shaped body, having well-developed thoracic legs rarely used for locomotion.[agriculture]
;ये लार्वा, जो सफेद grubs के रूप में जाना जाता है, आसानी से अपनी व्यापक और मांसल  रूप द्वारा पहचाना जा सकता हैं ,इसका शरीर सफेद या भूरे सफेद रंग का सी आकार का होता है ,इसके पास अच्छी तरह से विकसित थोरैसिक पैर होते है जो कभी कभार  संचलन के लिए इस्तेमाल किये जाते हैं . [manual]
(defrule know13
(declare (salience 5100))
(Domain agriculture)
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnA_jA))
(assert (id-domain_type  ?id agriculture))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp 	know13   "  ?id "  jAnA_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  know.clp 	know13   "  ?id "  agriculture )" crlf)
)
)

;@@@ Added by 14anu-ban-07 (10-03-2014)
;We did not come to know when sleep engulfed us after daylong fatigue in the comfortable warm room.( tourism)
;हमें पता नहीं चला  कि दिन  भर  की  थकान  के बाद  हमें आरामदायक  गर्म  कमरे  में  नींद कब आई   . (self)    
(defrule know14
(declare (salience 4900))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id1 ?id)
(kriyA-kriyA_niReXaka  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  know.clp 	know14   "  ?id "  pawA )" crlf))
)


;@@@Added by 14anu-ban-07,(09-04-2015)
;Computer expert? He doesn't know a mouse from a modem! (cambridge)(parser problem)
;कम्प्यूटर विशेषज्ञाम्पलृ?वह मोडम माउस के बीच फर्क नहीं जानता है! (manual)
(defrule know15
(declare (salience 5100))
(id-root ?id know)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 from)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_bIca_PZarka_jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " know.clp	know15  "  ?id "  " ?id1 "  ke_bIca_PZarka_jAna  )" crlf))
)

