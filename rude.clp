;$$$ Modified by 14anu-ban-10 on (21-03-2015)
(defrule rude1
(declare (salience 4900))
(id-root ?id rude)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 behaviour|joke) ;Added by 14anu-ban-10 on (21-03-2015)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rude.clp 	rude1   "  ?id "  aSiRta )" crlf))
)

;"rude","Adj","1.aSiRta"
;He got annoyed with her rude behaviour.                   
;I don't like rude jokes.


;@@@ Added by 14anu-ban-10 on (21-03-2015)
;A rude comment.[oald]
;एक रूखा  आलोचन हैं . [manual]
(defrule rude3
(declare (salience 5200))
(id-root ?id rude)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 comment)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  rUKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rude.clp 	rude3   "  ?id "   rUKA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-03-2015)
;Why are you so rude to your mother?.[oald]
;आप आपनी माँ से इतना असभ्य क्यों हैं.?[manual] 
(defrule rude4
(declare (salience 5300))
(id-root ?id rude)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI  ?id ? )
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  asaBya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rude.clp 	rude4   "  ?id "    asaBya)" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by Shirisha Manju 02-09-14 
(defrule rude0
(declare (salience 5000))
(id-root ?id rude)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBaxra));modified 'aBaxra-' as 'aBaxra' by Shirisha Manju 02-09-14
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rude.clp 	rude0   "  ?id "  aBaxra )" crlf))
)

