
;@@@ Added by 14anu19 (18-06-14)
;Its outlook and interests were from the beginning all india in character .
;उसका दृष्टिकोण और हित प्रकृति में अखिल भारतीय आरम्भ से थे
(defrule interest4
(declare (salience 4900))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp  interest4   "  ?id "  hiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-12-2014)
;The money was repaid with interest. (OALD)
;पैसे को ब्याज के साथ चुका दिया गया था . (manual)
(defrule interest5
(declare (salience 5000))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI ?id1 ?id)
(id-root ?id1 repay)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id byAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp  interest5   "  ?id "  byAja )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-12-2014)
;Interest rates have risen by 1%.(OALD)
;ब्याज दरें 1 % से बढ़ गयी हैं . (manual)
(defrule interest6
(declare (salience 5000))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 rate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id byAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp  interest6   "  ?id "  byAja )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-02-2015)
;You should put the money in a savings account where it will earn interest. (cambridge)
;आपको एक बचत खाता में पैसा डालना चाहिए जहाँ यह ब्याज कमाएगा .  (manual)
(defrule interest7
(declare (salience 5100))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 earn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id byAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp  interest7   "  ?id "  byAja )" crlf))
)

(defrule interest0
(declare (salience 5000))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id interesting )
(or (id-cat_coarse ?id adjective) (subject-subject_samAnAXikaraNa ?i ?id) (viSeRya-viSeRaNa ?x ?id)) ;It was interesting to hear about school life in Britain. It is biochemically an interesting experiment.
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id rucikara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  interest.clp  	interest0   "  ?id "  rucikara )" crlf))
)

;given_word=interesting && word_category=adjective	$rocaka)

;"interesting","Adj","1.rocaka"
;Arthur Hailey's novels are interesting.

;------------------- Default Rules ----------------------

(defrule interest1
(declare (salience 4900))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id interested )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ruci_raKanevAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  interest.clp  	interest1   "  ?id "  ruci_raKanevAlA )" crlf))
)

;"interested","Adj","1.ruci_raKanevAlA"
;The interested people can join the group..
;
;
(defrule interest2
(declare (salience 4800))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUci))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp 	interest2   "  ?id "  rUci )" crlf))
)

(defrule interest3
(declare (salience 4700))
(id-root ?id interest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruci_uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interest.clp 	interest3   "  ?id "  ruci_uwpanna_kara )" crlf))
)

;"interest","VTI","1.ruci_uwpanna_karanA"
;After retirement he began to interest himself in voluntary work.
;
;
