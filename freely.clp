
;@@@ Added by 14anu-ban-05 on (29-11-2014)
;Thus, in case charges were moving freely through the conductor under the action of electric field, their kinetic energy would increase as they move.[NCERT]
;awaH cAlaka ke anxara vixyuwa kRewra ke praBAva se agara AveSa mukwa rUpa se gawimAna rahawe wo unakI gawija UrjA baDa jAwI.[NCERT]
(defrule freely0
(declare (salience 1000))
(id-root ?id freely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  freely.clp  	freely0   "  ?id "  mukwa_rUpa_se )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-11-2014)
;A thin long piece of a magnet, when suspended freely, pointed in the north-south direction.[NCERT]
;cumbaka kA eka pawalA lambA tukadA, svawanwrawApUrvaka latakAe jAne para, hameSA uwwara-xakRiNa xiSA ke anuxiSa TaharawA WA.[NCERT]

(defrule freely1
(declare (salience 1000))
(id-root ?id freely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 suspend|speak|work|communicate|express)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svawanwrawApUrvaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  freely.clp  	freely1   "  ?id "  svawanwrawApUrvaka )" crlf))
)

