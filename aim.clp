
(defrule aim0
(declare (salience 5000))
(id-root ?id aim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aim.clp 	aim0   "  ?id "  lakRya )" crlf))
)

;"aim","N","1.lakRya"
;Her aim is to become a pilot.
;
(defrule aim1
(declare (salience 4900))
(id-root ?id aim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aim.clp 	aim1   "  ?id "  lakRiwa_kara )" crlf))
)

;$$$Modified by 14anu-ban-02(15-01-2015)
;meaning changed from niSAnA_lagAnA to niSAnA_lagA.
;He aimed his gun at the target. 
;उसने लक्ष्य में अपनी बन्दूक से निशाना लगाया. 
;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 02.07.2014 email-id:sahni.gourav0123@gmail.com
;He aimed his gun at the target. 
;उसने लक्ष्य में अपनी बन्दूक से निशाना लगाया. 
(defrule aim2
(declare (salience 4900))
(id-root ?id aim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 gun|pistol|revolver|rifle|handgun|cannon)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSAnA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aim.clp 	aim2   "  ?id "  niSAnA_lagA )" crlf))
)

;@@@ Added by Shreya Singhal on 21.09.19
;Strong AI aims to build machines that can truly reason and solve problems.  [AI Corpus]
;मजबूत ए. आई. मशीनें जो समस्याएँ सचमुच तर्क कर सकतीं हैं और हल कर सकतीं हैं बनाने के लिए उद्देश्य है .
(defrule aim3
(declare (salience 5000))
(id-root ?id aim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 noun|PropN)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  aim.clp      aim3   "  ?id "  uxxeSya )" crlf))
)


;"aim","V","1.lakRiwa_karanA"
;She's aiming at a scholarship.
;--"2.niSAnA_banAnA"
;My remarks were not aimed at you.
;
;LEVEL 
;
;
;Headword : aim
;
;Examples --
;
;"aim","N","1.lakRya"
;Her aim is to become a pilot.
;usakA lakRya pAyaleta banane kA hE
;
;--"2.uxxeSya"
;Our aim is to increase sales in other countries. 
;hamArA uxxeSya xUsare xeSoM meM bikrI baDZAnA hE -manuRya apane lakRya waka pahuzcane ke liye kuCa uxxeSyoM ko banAkara unakA pAlana karawA hE-yahAz usakA lakRya bikrI baDZAnA hE.
;
;"aim","V","1.lakRiwa_karanA"
;She's aiming at a scholarship.
;vaha CAwravqwwi pAne para lakRiwa kara rahI hE.-CAwravqwi pAnA hI usakA lakRya hE-lakRya
;
;--"2.niSAnA_banAnA"-merI tippaNI ne use lakRya nahIM banAyA-lakRya banAnA-lakRya
;My remarks were not aimed at you.
;merI tippaNiyAz wuma para niSAnA nahIM banI.
;
;"3.niSAnA lagAnA"-usane lakRya sAXa kara niSAnA lagAyA-lakRya sAXanA-lakRya
;He aimed his gun at the target.
;usane lakRayAMka para niSAnA lagAyA.
;
;         nota:--uparyukwa 'aim' Sabxa ke liye saBI vAkyoM kA avalokana karane para  
;            yaha niRkarRa nikAlA jA sakawA hE ki saMjFA Ora kriyA ke saBI                        vAkyoM kA arWa eka hI nABikIya arWa 'lakRya' se nikAlA jA sakawA 
;             hE
;
;             sUwri : lakRya 
;
;
;
;
;
;
;
;
;
;
