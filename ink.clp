;@@@ Added by 14anu-ban-06 (16-03-2015)
;The group has just inked a $10 million deal.(OALD)
;समूह ने एक $ 10 दस-लाख के समझौते पर अभी दस्तखत किया है . (manual)
(defrule ink2
(declare (salience 5100))
(id-root ?id ink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 deal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaswaKawa_kara))
(assert  (id-wsd_viBakwi   ?id1  para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ink.clp 	ink2   "  ?id "  xaswaKawa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  ink.clp      ink2   "  ?id1 " para )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-03-2015)
;The date for the presentation should have been inked in by now. (OALD)[parser no. - 2]
;व्याख्यान के लिए तिथि को अब से  चिन्हित करना चाहिये. (manual)
(defrule ink3
(declare (salience 5200))
(id-root ?id ink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cinhiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  ink.clp 	ink3  "  ?id "  " ?id1 " cinhiwa_kara)" crlf)
)
)
;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
;"ink","N","1.syAhI"
;The boy bought ink from the shop.
(defrule ink0
(declare (salience 5000))
(id-root ?id ink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id syAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ink.clp 	ink0   "  ?id "  syAhI )" crlf))
)

;"ink","VT","1.syAhI_PeranA"
;Ink the roller of the zerox machine.
(defrule ink1
(declare (salience 4900))
(id-root ?id ink)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id syAhI_Pera))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ink.clp 	ink1   "  ?id "  syAhI_Pera )" crlf))
)

