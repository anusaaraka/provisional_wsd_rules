;@@@ Added by 14anu04 on 27-June-2014
;She made a knot in her hair.
;उसने अपने केश में जूडा बनाया . 
(defrule knot_tmp5
(declare (salience 5100))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id3 ?id)
(kriyA-in_saMbanXI  ?id3 ?id2)
(id-root ?id2 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jUdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot_tmp5   "  ?id "  jUdA )" crlf))
)

;@@@ Added by 14anu04 on 27-June-2014
;She tied her hair in a knot.
;उसने जूडे में अपने केश बाँधे. 
(defrule knot_tmp4
(declare (salience 5100))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id3 ?id2)
(kriyA-in_saMbanXI  ?id3 ?id)
(id-root ?id2 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jUdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot_tmp4   "  ?id "  jUdA )" crlf))
)

;@@@ Added by 14anu04 on 27-June-2014
;She had her hair in a knot.
;उसके केश जूडे में  थे. 
(defrule knot_tmp3
(declare (salience 5100))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id2 ?id)
(id-root ?id2 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jUdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot_tmp3   "  ?id "  jUdA )" crlf))
)

;@@@ Added by 14anu04 on 27-June-2014
;Small knots of people came out on streets.
;लोगों के छोटे समूह सडकों पर आ गए . 
(defrule knot_tmp2
(declare (salience 5100))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samUha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot_tmp2   "  ?id "  samUha )" crlf))
)

;@@@ Added by 14anu04 on 27-June-2014
;The speed of the ship was 20 knots per hour.
;जहाज की रफ्तार 20 समुद्री मील प्रति घण्टा थी . 
(defrule knot_tmp
(declare (salience 5100))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id knots)
(id-word =(+ ?id 1) per)
(id-word =(+ ?id 2) hour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samuxrI_mIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot_tmp   "  ?id "  samuxrI_mIla )" crlf))
)


;commented by 14anu-ban-07 (13-12-2014) samUha and tolI is same knot_tmp2
;@@@ Added by Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;Little knots of people had gathered at the entrance.
;लोगों की छोटी टोलियों ने प्रवेश में इकठ्ठा किया था . 
;(defrule knot2
;(declare (salience 5000))
;(id-root ?id knot)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(kriyA-subject ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id tolI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot2   "  ?id "  tolI )" crlf))
;)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;I could feel a knot of fear in my throat. 
;मैं मेरे कण्ठ में भय की उलझन महसूस कर सका . 
(defrule knot3
(declare (salience 5000))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ? ?id)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot3   "  ?id "  ulaJana )" crlf))
)

;$$$ Modified by 14anu-ban-07 (13-12-2014)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;My stomach was in knots
;मेरा पेट उलझनों में था.
(defrule knot4
(declare (salience 5000))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id2 ?id)  ;?id2 by 14anu-ban-07, due to Connectivity missing (13-12-2014)
(kriyA-subject  ?id2 ?id1) ;by 14anu-ban-07 (13-12-2014)
(id-root ?id1 stomach)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot4   "  ?id "  ulaJana )" crlf))
)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;Sailors had to know lots of different knots. 
;नाविकों विभिन्न समुद्री मील के बहुत से पता करने के लिए किया था.
(defrule knot5
(declare (salience 5000))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ? ?id)
(id-root ?id1 sailor|captain|Helmsman|coxswain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samuxrI_mIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot5   "  ?id "  samuxrI_mIla )" crlf))
)

;-------------------------- Default rules ----------------------
(defrule knot0
(declare (salience 5000))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAzTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot0   "  ?id "  gAzTa )" crlf))
)

;"knot","N","1.gAzTa"
;She is having number of knots in her ribbon.
;His stomach was in knots.
;Their muscles stood out in knots.
;--"2.samuxrI_mIla"
;Ship is moving at the speed of 30 knots per hour.
;--"3.samUha"
;A small knot of women listened to his sermon.
;


;$$$ Modified by 14anu-ban-07 (13-12-2014)
;$$$ Modified by 14anu04 on 27-June-2014
;The children knotted themselves together.
;previous rule translation: बच्चों ने एक साथ खुद को गाँठ लगाया .  
;new rule translation: बच्चों ने एक साथ खुद को बाँधा. 
(defrule knot1
(declare (salience 4900))
(id-root ?id knot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXa)) ;meaning changed from gAzTa_laga to bAzXa , by 14anu-ban-07 (13-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knot.clp 	knot1   "  ?id "  gAzTa_laga )" crlf))
)


;"knot","V","1.gAzTa_laganA"
;Tie a knot to the string.
;--"2.bAzXanA"
;Knot the strings tightly to one another.
;--"3.ulaJAnA"
;The strange question had him all in knots.
;
