
;@@@ Added by 14anu09 and kokila eflu [19-6-14]
(defrule commission2
(declare (salience 4900))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission2   "  ?id "  niyukwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-12-2014)
;One 66 cm Cyclotron donated by the Rochester University of USA was commissioned in Panjab University, Chandigarh. [ncert]
;amerikA ke roSestara viSvavixyAlaya xvArA praxAna kie gae 66 @cm sAiklotroYna ko paFjAba viSvavixyAlaya, caNdIgaDa meM sWApiwa kiyA gayA. [ncert]
(defrule commission3
(declare (salience 4000))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp    commission3   "  ?id "  sWApiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (21-03-2015)
;He has secured commission to design buildings for the government.  [hinkhoj]
;उसने सरकार के लिए इमारतों का नक्शा बनाने का अधिकार प्राप्त कर लिया है . [manual]
(defrule commission4
(declare (salience 5000))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 secure)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission4   "  ?id "  aXikAra )" crlf))
)


;@@@ Added by 14anu-ban-03 (21-03-2015)
;He is receiving 20% commission.  [hinkhoj]
;वह 20 % दलाली लेता है .  [manual]
(defrule commission5
(declare (salience 5000))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xalAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission5   "  ?id "  xalAlI )" crlf))
)


;@@@ Added by 14anu-ban-03 (21-03-2015)
;The commission of a crime. [oald]
;अपराध का कृत्य . [manual]
(defrule commission6
(declare (salience 5000))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1 crime|offence|murder)          
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqwya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission6   "  ?id "  kqwya )" crlf))
)


;@@@ Added by 14anu-ban-03 (21-03-2015)
;She's just got a commission to paint Sir Ellis Pike's wife. [oald]
;उसे सर एलिस पाइक की पत्नी का शृंगार करने का कार्यभार मिला है . [manual]
(defrule commission7
(declare (salience 5000))
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryaBAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission7   "  ?id "  kAryaBAra )" crlf))
)


;@@@ Added by 14anu-ban-03 (21-03-2015)
;She has been commissioned to write a new national anthem. [oald]
;उसे एक नया राष्ट्र गान लिखने का कार्य सौंपा गया है . [manual]
(defrule commission8
(declare (salience 4900))   
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya_sOMpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission8   "  ?id "  kArya_sOMpa )" crlf))
)

;------------------ Default rules ----------------------

(defrule commission0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (21-03-2015)
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission0   "  ?id "  Ayoga )" crlf))
)

;"commission","N","1.Ayoga"
;The Srikrishna commission investigated the Bombay riot case.
;--"2.aXikAra"
;He has secured commission to design buildings for the government.
;--"3.xalAlI"
;

(defrule commission1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (21-03-2015)
(id-root ?id commission)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commission.clp 	commission1   "  ?id "  aXikAra_xe )" crlf))
)

;"commission","VT","1.aXikAra_xenA/niyukwa_karanA"
;Vinoth Kumar has been commissioned as a pilot officer.
;
