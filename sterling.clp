
(defrule sterling0
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSvasanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sterling.clp 	sterling0   "  ?id "  viSvasanIya )" crlf))
)

;"sterling","Adj","1.viSvasanIya"
;Her work is sterling as compared to other.
;

;$$$ Modified by 14anu-ban-01 on (06-04-2015)
(defrule sterling1
(declare (salience 4900))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAuNdZa))	;corrected spelling by 14anu-ban-01 on (06-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sterling.clp 	sterling1   "  ?id "  pAuNdZa )" crlf))	;corrected spelling by 14anu-ban-01 on (06-04-2015)
)

;"sterling","N","1.pAuMdZa"
;British people use sterling for purchasing things.
;--"2.cAnxI_kA_ABURaNa"
;Rita collected much sterling silver jewellery for her marriage.
;


;@@@ Added by 14anu-ban-01 on (06-04-2015)
;You can be paid in pounds sterling or American dollars. [oald]
;आपको पाउण्ड या अमरीकी डौलर में पैसे दिए जा सकते हैं . [self]
(defrule sterling2
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 pound)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAuNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sterling.clp 	sterling2  "  ?id "  " ?id1 "  pAuNda  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (06-04-2015)
;Rita collected much sterling silver jewellery for her marriage.[sterling.clp]
;रीटा ने अपनी शादी के लिए बहुत शुद्ध चाँदी/खालिस चाँदी के गहने  इकट्ठे किए . [self]
(defrule sterling3
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 silver)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SuxXa_cAzxI/KAlisa_cAzxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sterling.clp 	sterling3  "  ?id "  " ?id1 "  SuxXa_cAzxI/KAlisa_cAzxI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;He has done sterling work on the finance committee.[oald]
;उसने व्त्ति समिति पर शानदार कार्य किया है . [self]
(defrule sterling4
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 work)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sterling.clp 	sterling4   "  ?id "  SAnaxAra)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;She has sterling qualities. [oald]
;उसमें उत्कृष्ट गुणवत्ताएँ हैं . [self]
(defrule sterling5
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 quality)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkqRta))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sterling.clp 	sterling5   "  ?id "  uwkqRta)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;Thanks to all your sterling efforts, we’ve raised over £12000. [oald]
;आपके सभी वास्तविक प्रयासों के कारण हमने १२००० पाउण्ड से ज्यादा बढ़ा लिया है  [self]
(defrule sterling6
(declare (salience 5000))
(id-root ?id sterling)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 effort)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sterling.clp 	sterling6   "  ?id "  vAswavika)" crlf))	
)
