;@@@ Added by 14anu-ban-04 (28-04-2015)
;The Ottoman Empire disintegrated into lots of small states.          [cald]
;मचिया एम्पाइअर बहुत सारे छोटे राज्यों   में विभाजित हुआ .                              [self]
;The country has disintegrated into separate states.                 [oald]
;देश अलग-अलग राज्यों में विभाजित हुआ है .                                       [self]
(defrule disintegrate1
(declare (salience 40))
(id-root ?id disintegrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_ho))      
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disintegrate.clp 	disintegrate1   "  ?id "  viBAjiwa_ho)" crlf))
)

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The laser can disintegrate most kinds of rock.                  [merriam-webster]
;लेजर अधिकांश प्रकार के  पत्थरों को तोड़ सकता है .                                 [self]
(defrule disintegrate2
(declare (salience 30))
(id-root ?id disintegrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id woda))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disintegrate.clp     disintegrate2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disintegrate.clp 	disintegrate2   "  ?id "  woda)" crlf))
)

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The social fabric of this country is slowly disintegrating.                 [oald]
;इस देश का सामाजिक निर्माण  धीरे से अशक्त हो रहा है .                                        [self]
(defrule disintegrate3
(declare (salience 20))
(id-word ?id disintegrating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSakwa_ho))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disintegrate.clp 	disintegrate3  "  ?id "  aSakwa_ho)" crlf))
)

;--------------------------------------------------------DEFAULT RULE---------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The plane disintegrated as it fell into the sea.                 [oald]
; विमान विघटित हो गया जब वह समुद्र में गिरा .                                  [self]
(defrule disintegrate0
(declare (salience 10))
(id-root ?id disintegrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viGatiwa_ho_jA))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disintegrate.clp 	disintegrate0   "  ?id "  viGatiwa_ho_jA)" crlf))
)


