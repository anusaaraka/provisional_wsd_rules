;@@@ Added by 14anu-ban-06 (28-01-2015)
;They either honestly imbibe the frolicking waves of the sea or capture them in the pictures .(parallel corpus)
;समंदर की इठलाती लहरों को ईमानदारी से आत्मसात करते हैं या तस्वीरों में कैद करते हैं ।(parallel corpus)
(defrule imbibe1
(declare (salience 2000))
(id-root ?id imbibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  AwmasAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imbibe.clp 	imbibe1   "  ?id "  AwmasAwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-01-2015)
;They did not realise that modern ideas and culture could be best imbibed by integrating them into Indian culture .(parallel corpus)
;उन्होंने यह महसूस नहीं किया कि आधुनिक विचार और संस्कृति को भारतीय संस्कृति में सुगंधित करके ही सब से अच्छी तरह अपनाया जा सकता है .
;(parallel corpus)

(defrule imbibe2
(declare (salience 2500))
(id-root ?id imbibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 idea|culture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  apanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imbibe.clp 	imbibe2   "  ?id "  apanA )" crlf))
)

;--------------------- Default Rule ----------------------
;@@@ Added by 14anu-ban-06 (28-01-2015)
;Have you been imbibing again? (cambridge)
;क्या आप फिर से पी रहे (manual)al)
(defrule imbibe0
(declare (salience 0))
(id-root ?id imbibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  pI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imbibe.clp   imbibe0   "  ?id "  pI )" crlf))
)

