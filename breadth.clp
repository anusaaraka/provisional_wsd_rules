;@@@Added by 14anu-ban-02(05-02-2015)
;Sentence: For a given material, increasing the depth d rather than the breadth b is more effective in reducing the bending, since δ is proportional to d-3 and only to b-1 (of course the length l of the span should be as small as possible).[ncert 11_09]
;Translation: किसी दिये हुए द्रव्य के लिए बङ्कन कम करने के लिए चौडाई b की बजाय मोटाई d को बढाना अधिक प्रभावी होता है क्योंकि δ, d-3 लेकिन b-1 के अनुक्रमानुपाती होता है (यद्यपि दण्ड की लम्बाई यथासम्भव कम होनी ही चाहिए).[ncert]
(defrule breadth0 
(declare (salience 0)) 
(id-root ?id breadth) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id cOdZAI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breadth.clp  breadth0  "  ?id "  cOdZAI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(05-02-2015)
;Sentence: The breadth of her knowledge is amazing.[cambridge]
;Translation: उसके ज्ञान का विस्तार अद्धभुत है.[self]
(defrule breadth1 
(declare (salience 100)) 
(id-root ?id breadth) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ? ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 knowledge)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id viswAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breadth.clp  breadth1  "  ?id "  viswAra )" crlf)) 
) 

;@@@Added by 14anu-ban-02(05-02-2015)
;Sentence: A teacher must have a breadth of knowledge of the subject.[wordnet]
;Translation: शिक्षक के पास विषय के ज्ञान की गहराई होनी चाहिये.[self]
(defrule breadth2 
(declare (salience 100)) 
(id-root ?id breadth) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ? ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 knowledge)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id gaharAI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breadth.clp  breadth2  "  ?id "  gaharAI )" crlf)) 
) 
