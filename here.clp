;@@@ Added by 14anu01 on 21-06-2014
;Turn here.
;इधर मुडिए . 
(defrule here0
(declare (salience 5500))
(id-root ?id here)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(- ?id 1) turn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  iXara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  here.clp 	here0   "  ?id "   iXara)" crlf))
)

;@@@ Added by 14anu-ban-06  (14-08-2014)
;There is a hearsay regarding this that to get rid of killing of the same gotra the five Pandavas had done an intense penance for twelve years here and went for the ascension of the heaven from here itself .(tourism corpus)
;इसके  विषय  में  जनश्रुति  है  कि  गोत्र  हत्या  के  पाप  से  छुटकारा  पाने  के  लिए  यहाँ  पाँच  भाई  पाण्डवों  ने  बारह  वर्ष  शिव  की  घोर  तपस्या  की  तथा  यहीं  से  स्वर्गारोहण  को  चले  गए  थे  ।
;Returning from Gangotri the travelleres used to take the route to Kedarnath from here itself .(tourism corpus)
;गंगोत्री  से  लौटकर  यात्री  यहीं  से  केदारनाथ  का  मार्ग  पकड़ते  थे  ।
;The Pantagan is here itself .(tourism corpus)
;यही  पंटागण  है  ।
(defrule here01
(declare (salience 2500))
(id-root ?id here)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(+ ?id 1) itself)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) yahIM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " here.clp	 here01  "  ?id "  " (+ ?id 1)  " yahIM  )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by 14anu-ban-06 (15-12-2014)
;@@@ Added by 14anu01 on 21-06-2014
;Come here.
;यहां आइए . 
(defrule here1
(declare (salience 0));salience reduced from '4500' to '0' by 14anu-ban-06 (15-12-2014)
(id-root ?id here)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
;(id-cat_coarse =(- ?id 1) verb);commented by 14anu-ban-06 (15-12-2014) to make this rule as 'default rule'
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  yahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  here.clp 	here1   "  ?id "   yahAz)" crlf))
)


