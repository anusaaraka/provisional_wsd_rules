;$$$Modified by 14anu-ban-02(14-01-2015)
;There were many bugs in the code.
;संहिता में बहुत गलतियाँ थीं. 
;@@@ Added by 14anu04 on 17-June-2014
;There were many bugs in the code.
;संहिता में बहुत गलतियाँ थीं. 
(defrule bug_tmp
(declare (salience 1200))
(id-root ?id bug)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id2 code)	;commented by 14anu-ban-02(14-01-2015)
;(test (!= ?id2 0))
(viSeRya-in_saMbanXI  ?id ?id1)	;added by 14anu-ban-02(14-01-2015)
(id-root ?id1 code)		;added by 14anu-ban-02(14-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bug.clp 	bug_tmp   "  ?id "  galawI )" crlf))
)


;@@@ Added by 14anu04 on 17-June-2014
;He was bugged by the detective.
;वह जासूस द्वारा तङ्ग किया गया था. 
(defrule bug1
(declare (salience 900))
(id-root ?id bug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wafga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bug.clp 	bug1   "  ?id "  wafga_kara )" crlf))
)

;@@@Added by 14anu-ban-02(13-04-2015)
;There's a stomach bug going round. [oald]
;यहाँ  पेट सङ्क्रमित बीमारी फैल रही है.[self]
(defrule bug3
(declare (salience 2000))
(id-root ?id bug)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 stomach|eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkramiwa_bImArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bug.clp 	bug3   "  ?id "  saMkramiwa_bImArI )" crlf))
)


;--------------default_rules---------------------------------

;@@@ Added by 14anu04 on 17-June-2014
;There were many bugs on the bed.
;बिस्तर पर बहुत कीडे थे. 
(defrule bug0
(declare (salience 1000))
(id-root ?id bug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bug.clp      bug0   "  ?id "  kIdA )" crlf))
)


;@@@ Added by 14anu04 on 17-June-2014
;The rooms were bugged.
;कमरों की छानबीन की गयी थी.
(defrule bug2
(declare (salience 800))
(id-root ?id bug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_CAnabIna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bug.clp 	bug2   "  ?id "  kI_CAnabIna_kara )" crlf))
)
