;@@@Added by 14anu-ban-02(09-04-2015)
;The job would suit someone with a business background.[oald]
;नौकरी  उद्योग अनुभव वाले  किसी व्यक्ति के लिये  उपयुक्त होगी .[self] 
(defrule background1 
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) 
(id-root ?id1 business)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anuBava)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background1  "  ?id "  anuBava )" crlf)) 
) 

;@@@Added by 14anu-ban-02(09-04-2015)
;In the background is the sound of smooth jazz.[coca]
;वातावरण में  मधुर जाज की ध्वनि है. [self]
(defrule background2 
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-in_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 sound)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id vAwAvaraNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background2  "  ?id "  vAwAvaraNa )" crlf)) 
) 


;@@@Added by 14anu-ban-02(09-04-2015)
;He was from a poor background.[cald]
;वह दीन वर्ग से था .[self] 
(defrule background3 
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 poor)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id varga)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background3  "  ?id "  varga )" crlf)) 
) 
;@@@Added by 14anu-ban-02(09-04-2015)
;Can you give me some background on the issue?[cald]
;क्या आप मुझे इस विषय पर  कुछ जानकारी दे सकते हैं? [self]
(defrule background4 
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-on_saMbanXI  ?id ?id1)
(id-root ?id1 issue)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id jAnakArI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background4  "  ?id "  jAnakArI )" crlf)) 
) 


;@@@Added by 14anu-ban-02(09-04-2015)
;He is a background singer.[self]
;वह पार्श्व गायक है . [self]
(defrule background5 
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 singer)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pArSva)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background5  "  ?id "  pArSva )" crlf)) 
) 


;@@@Added by 14anu-ban-02(09-04-2015)
;If you listen carefully to this piece of music, you can hear a flute in the background.[cald]
;यदि आप सङ्गीत को ध्यान से सुनते हैं, तो आप पीछे (बज रही)बाँसुरी सुन सकते हैं .[self] 
(defrule background6
(declare (salience 100)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id1 flute|piano)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pICe)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background6  "  ?id "  pICe )" crlf)) 
) 


;-------------------Default_rules------------------------------------------


;@@@Added by 14anu-ban-02(09-04-2015)
;Sentence: The historical background to the war.[oald]
;Translation: युद्ध को ऐतिहासिक पृष्ठभूमि . [self]
(defrule background0 
(declare (salience 0)) 
(id-root ?id background) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pqRtaBUmi)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  background.clp  background0  "  ?id "  pqRtaBUmi )" crlf)) 
) 


