;@@@ Added by 14anu-ban-06 (15-04-2015)
;The town's prosperity is inextricably intertwined with the fortunes of the factory. (cambridge)[parser no.- 731]
;नगर की समृद्धि फैक्टरी के भाग्य के साथ घनिष्ठ रूप से जुडी हुई है . (manual)
(defrule intertwine1
(declare (salience 2000))
(id-root ?id intertwine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intertwine.clp 	intertwine1   "  ?id "  judZA_ho )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-04-2015)
;A necklace of rubies intertwined with pearls. (OALD)
;मोतियों से जड़ी हुई लाल मणि की एक माला  . (manual)
(defrule intertwine2
(declare (salience 2000))
(id-root ?id intertwine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 pearl)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jadZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intertwine.clp 	intertwine2   "  ?id "  jadZA_ho )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-04-2015)
;Their political careers had become closely intertwined. (OALD)[parser no.- 26]
;उनके राजनैतिक कैरियर घनिष्ठ रूप से जुडे हुए बन गए थे . (manual)
(defrule intertwine3
(declare (salience 2200))
(id-root ?id intertwine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 career)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intertwine.clp 	intertwine3   "  ?id "  judZA_ho )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (15-04-2015)
;Intertwining branches.(OALD)
;गुँथी हुईं शाखाएँ.(manual)
(defrule intertwine0
(declare (salience 0))
(id-root ?id intertwine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guzWA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intertwine.clp 	intertwine0   "  ?id "  guzWA_ho )" crlf))
)


