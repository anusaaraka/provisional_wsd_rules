;"remove","N","1.hatAva_yA_xUra_hone_kI_kriyA"
;Her account of the incident was several removes from the truth. [Hinkhoj Dictionary, Old file]
;उसकी घटना का खाता सच्चाई से कई हटाव पर है ।
;$$$ Modified by Anita-27.11.2013 as Suggested by Chaitanya ji (mng 'hatAva_yA_xUra_hone_kI_kriyA' to 'hatAva')
(defrule remove0
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id removes) ;Suggested by Chaitanya ji 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove0   "  ?id "  hatAva )" crlf))
)

;$$$ Modified by Anita-27.11.2013 
;This detergent will remove even old stains. [Cambridge Dictionary] 
;यह साबुन पुराने धब्बे भी निकाल देगा ।
(defrule remove1
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 stain|spot)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove1   "  ?id "  nikAla_xe )" crlf))
)

;@@@ Added by Anita-27.11.2013 
;The men came to remove the rubbish from the backyard. [Cambridge Dicionary]
;पिछवाड़े का कचरा ले जाने के लिए आदमी आया था ।
(defrule remove3
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 rubbish)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove3   "  ?id "  le_jA )" crlf))
)

;@@@ Added by Anita-27.11.2013 
;It got so hot that he removed his tie and jacket. [Cambridge Dictionary ]
;इतनी गर्मी हो गयी कि उसे अपनी टाई और जैकेट भी उतारनी पड़ी ।
(defrule remove4
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove4   "  ?id "  uwAra_xe )" crlf))
)

;@@@ Added by Anita-27.11.2013 
;They decided to remove their son from the school. [Cambridge Dictionary]
;उन्होंने अपने बेटे को स्कूल से निकालने का निश्चय किया ।
(defrule remove5
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 school|college|hostel|class|house|home)
(kriyA-from_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove5   "  ?id "  nikAla )" crlf))
)

;@@@ Added by Anita-27.11.2013 
;Hearing your opinion has removed my last doubts about her. [Cambridge Dictionary]
;तुम्हारे विचार को सुनकर मेरा आखिरी संदेह भी उसके बारे में खत्म हो गया ।
;Hearing your opinion has removed my last suspicions about her.
(defrule remove6
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id  ?id1)
(id-root ?id1 doubt|suspicion)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kawma_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove6   "  ?id "  Kawma_ho )" crlf))
)

;@@@ Added by Anita-27.11.2013 
;Remove your hand from my shoulder.
;mere kanXe se ApakA hAWa hatAie. 
(defrule remove7
(declare (salience -10));Reduced salience to stop firing for this sentence. Ex: Remove these spots from your shirt. 
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_kriyA  ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove7   "  ?id "  hatA )" crlf))
)

;@@@ Added by Anita-30.6.2014 
;The police were able to remove Ganga and Chandra Kiran from the damaged car with some difficulty and took them to the ;Sundernagar civil hospital. [ news-dev]
;पुलिस ने गंगा व चंद्र किरण को कड़ी मशक्कत के बाद क्षतिग्रस्त टैक्सी से बाहर निकाला और नागरिक अस्पताल सुंदरनगर ले गए ।
(defrule remove8
(declare (salience 100))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove8   "  ?id "  nikAla )" crlf))
)

;@@@ Added by 14anu-ban-10 on (14-10-2014)
;Systematic errors can be minimized by improving experimental techniques, selecting better instruments and removing personal bias as far as possible.  [ncert corpus]
;suXarI huI prAyogikI wakanIkoM ke upayoga, prayoga ke lie apekRAkqwa acCe mApana yanwroM kA cayana evaM yaWAsamBava vyakwigawa pUrvAgrahoM ko xUra karake kramabaxXa wrutiyoM ko kama kiyA jA sakawA hE. [ncert corpus]
(defrule remove9
(declare (salience 200))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove9   "  ?id "  xUra_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (15-11-2014)
;When the deforming force is removed, the interatomic forces tend to drive them back to their original positions.  [ncert corpus]
;jaba virUpaka bala ko hatA liyA jAwA hE wo aMwarA - paramANavika bala unheM vApasa apanI prAraMBika sWiwiyoM meM le jAwe hEM.[ncert corpus]
(defrule remove10
(declare (salience 300))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove10   "  ?id "  hatA_le)" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-11-2014)
;To remove such ambiguities in determining the number of significant figures, the best way is to report every measurement in scientific notation (in the power of 10).[ncert corpus]
;sArWaka afkoM ke nirXAraNa meM isa prakAra kI sanxigXawA ko xUra karane ke lie sarvowwama upAya yaha hE ki prawyeka mApa ko vEjFAnika safkewa (10 kI GAwoM ke rUpa meM) meM praswuwa kiyA jAe.[ncert corpus]
(defrule remove11
(declare (salience 400))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove11   "  ?id "  xUra_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-11-2014)
;As mentioned above, roughly it is half the energy required to remove it entirely from the liquid i.e., half the heat of evaporation.[ncert corpus]
;जैसा कि ऊपर वर्णन किया है कि अणु को पूर्ण रूप से द्रव से बाहर निकालने के लिए लगभग आधी ऊर्जा की जरूरत होती है अर्थात् वाष्पन की ऊष्मा की आधी.[manual]
(defrule remove12
(declare (salience 500))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ? ?id )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove12   "  ?id " bAhara_nikAla)" crlf))
)

;$$$ Modified by 14anu-ban-06 (30-04-2015)
;@@@ Added by 14anu-ban-10 on (19-03-2015)
;The decay of the wood will spread if it is not removed.[oald]
;लकड़ी की सड़न फैलेगी यदि यह हटाई नहीं गयी .[manual]
(defrule remove13
(declare (salience 600))
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatAI_jA));meaning changed from 'hatAI' to 'hatAI_jA' by 14anu-ban-06 (30-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove13   "  ?id " hatAI_jA)" crlf))
)
;--------------------------- Default Rules -------------------------
;Refer to Sabdasutra
;default_sense && category=verb	hatA_xe	0
;"remove","V","1.hatA_xe"
;The company's shareholders have voted to remove the executive board
;कंपनी के शेयर होल्डर्स ने एग्जीक्यूटिव बोर्ड को हटाने के लिए वोट दिए .
;Several opposition groups are fighting to remove the president from power .
;बहुत से विरोधी दल प्रेसिडेंट को हटाने के लिए लड़ाई कर रहें हैं
(defrule remove_default
(declare (salience -20)) ;Reduced salience by Anita 
(id-root ?id remove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remove.clp 	remove_default   "  ?id "  hatA_xe )" crlf))
)
;--------------------------- Old Examples -------------------------
;"remove","VTI","1.hatA_xenA"
;Remove your hand from my shoulder. 
;--"2.xUra_karanA"
;Remove these spots from your shirt. 
;--"3.alaga_karanA"
;He has been removed from his service. 
;--"4.anwa_karanA"
;We should remove terrorism from our country.  
;--"5.eka_jagaha_se_xUsarI_jagaha_jAnA"
;We removed to Mumbai early in June. 
;Above sentence is not valid. Sentence can be like this
;We re moved to Mumbai early in June. (Suggested by Anita ji 27-11-13) 
;
