;$$$ Modified by 14anu-ban-09 on 21-7-14
;A folk dance called Theyyam can also be seen in Muthappan. [Parallel Corpus]
;muwrappana meM weyyama mAmaka loka nrawya BI xeKjA sakawA hE.
;In the festivals , functions of Bundelkhand the folk culture of the Arya and the Dravida are equally seen. [Parallel Corpus]
;bunxelakaMda meM loka saMskrawi ke SIrRa evaM vIrawA spaRta rUpa se xiKAE xewI hEM.
;In Bundelkhand a folk singer named Isuri is remembered with deep respect. [Parallel Corpus]
;bunxelakaMda meM IsurI nAmaka loka nAyaka ko awyanwa sammAna ke sAWa yaxa kiyA jANA hE.

;@@@ Added by 14anu11
;The big nagasvaram is usually called the bari nagasvaram and is typical of the folk ensembles nayyandi melam of Tamil Nadu and the 
;karanga mela of Karnataka , it is also the only oboe now in use in Karnatak classical music 
;विशाल नागस्वरम् को प्राय : बडी नागस्वरम् कहा जाता है और तमिलनाडु के नयनन्दी मेलम तथा कर्नाटक के कर्गा मेला जैसे लोक मेलों में यह एक खास वाद्य है .
(defrule folk2
(declare (salience 5000))
(id-root ?id folk)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) 
(id-root ?id1 music|dance|musician|singer|dancer|art|tale|culture|artist|style); added by 14anu-ban-09
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  folk.clp 	folk2   "  ?id "  loka )" crlf))
)


;---------------------- Default rules -------------------------
(defrule folk0
(declare (salience 4500))
(id-root ?id folk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga_viSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  folk.clp 	folk0   "  ?id "  varga_viSeRa )" crlf))
)


(defrule folk1
(declare (salience 4900))
(id-root ?id folk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  folk.clp 	folk1   "  ?id "  loka )" crlf))
)

;"folk","Adj","1.loka"
;folk music, folk dances, folk art of a country or community.
;
;
