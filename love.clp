
(defrule love0
(declare (salience 5000))
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id loving )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pyAra_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  love.clp  	love0   "  ?id "  pyAra_BarA )" crlf))
)

;$$$ Modified by 14anu26  [26-04-16]
;He was the love of my life.
;वह मेरे जीवन का  प्यार  था.
(defrule love1
;(declare (salience 4900))
(declare (salience 0)) ;salience changed by 14anu26  
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pyAra)) ;meaning changed from prema to  pyAra
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  love.clp 	love1   "  ?id "  pyAra )" crlf))
)

;$$$ Modified by Soma (18-9-16)
;"love","VTI","1.pasanxa_karanA"
;I love French food
;I love cooking
(defrule love2
(declare (salience 4800))
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))) ; Added to specify that the equivalent pasanxa_kara is applicable only when the object is non-human
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasanxa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  love.clp 	love2   "  ?id "  pasanxa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  love.clp      love2   "  ?id " ko )" crlf)
)
)

;@@@ Added by Soma (18-9-16) - to specify the equivalent of love as 'pyAra_kara' when the object is human
;--"2.pyAra_karanA"
;She loves her husband deeply
(defrule love3
(declare (salience 5000))
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pyAra_kara))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  love.clp 	love3   "  ?id "  pyAra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  love.clp      love3   "  ?id " se )" crlf)
)
)

;@@@Added by 14anu-ban-08 (14-04-2015)
;He's in love with his work.  [oald]
;उसे अपने काम में रूची हैं.  [self]
(defrule love4
(declare (salience 100))
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI ?id ?id1)
(id-root ?id1 work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUcI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  love.clp 	love4   "  ?id "  rUcI )" crlf))
)

;@@@Added by 14anu-ban-08 (14-04-2015)
;I haven't been to Brazil, but I'd love to go.  [oald]
;मैं कभी ब्राज़ील नहीं गया, पर मैं जाना चाहता हूँ. [self]
(defrule love5
(declare (salience 4900))
(id-root ?id love)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 go)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAha)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  love.clp 	love5   "  ?id "  cAha )" crlf))
)
