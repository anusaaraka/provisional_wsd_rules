
(defrule indent0
(declare (salience 5000))
(id-root ?id indent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAzgapawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indent.clp 	indent0   "  ?id "  mAzgapawra )" crlf))
)

;"indent","N","1.mAzgapawra"
;I placed an indent for new equipments.
;
(defrule indent1
(declare (salience 4900))
(id-root ?id indent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAMwexAra_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indent.clp 	indent1   "  ?id "  xAMwexAra_banA )" crlf))
)

;"indent","V","1.xAMwexAra_banAnA"
;--"2.gadDA_banAnA"
;

;@@@ Added by 14anu-ban-06 (02-03-2015)
;Each new paragraph should be indented about two centimetres from the margin. (cambridge)
;हर एक नया अनुच्छेद मार्जिन से दो सेंटीमीटर के लगभग जगह छोड कर लिखा जाना चाहिए . (manual)
(defrule indent2
(declare (salience 5100))
(id-root ?id indent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 paragraph)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jagaha_Coda_kara_liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indent.clp 	indent2   "  ?id "  jagaha_Coda_kara_liKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-03-2015)
;We indented for the engine spares last month.(cambridge)
;हमने पिछले महीने इंजनों के अतिरिक्त पुरजों के लिए अनुरोध किया . (manual)
(defrule indent3
(declare (salience 5100))
(id-root ?id indent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indent.clp 	indent3   "  ?id "  anuroXa_kara )" crlf))
)
