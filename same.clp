
(defrule same0
(declare (salience 5000))
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) very )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp 	same0   "  ?id "  vahI )" crlf))
)

;$$$ Modified by kokila eflu and 14anu09[23-06-14]
;Those same employees registered again.
;उन्हीं कर्मचारियों ने फिर से रजिस्टर किए . 
;उन्हीं कर्मचारियों ने फिर से रजिस्टर किया . [Translation improved by 14anu-ban-01 on (12-12-2014)]
(defrule same1
(declare (salience 4800));salience reduced from 4900 to 4800 by 14anu-ban-01 on (16-10-2014)
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) the|those|these )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id vahI)) ; commented by  kokila eflu and 14anu09
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) vahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " same.clp	same1  "  ?id "  " =(- ?id 1) "  vahI  )" crlf))
)

;@@@Added by Manasa ( 23-02-2016 )
;For example, the same law of gravitation (given by Newton) describes the fall of an apple to the ground, the motion of the moon around the earth and the motion of planets around the sun
;उदाहरण के लिए , वही  गुरुत्वाकर्षण का नियम ( जिसे न्यूटन ने प्रतिपदित किया ) पृथ्वी पर किसी सेब का गिरना , पृथ्वी के परितः चन्द्रमा की परिक्रमा तथा सूर्य के परितः ग्रहों की गति जैसी परिघटनाओं की व्याख्या करता है .
(defrule same7
(declare (salience 4800))
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp 	same7   "  ?id "  vahI )" crlf))
)

(defrule same2
(declare (salience 4800))
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp     same2   "  ?id "  samAna )" crlf))
)

;"same","Adj","1.samAna/saxqSa"
;Handwriting of Priya && Vidushi is just same.
;
(defrule same3
(declare (salience 4700))
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp 	same3   "  ?id "  vahI )" crlf))
)

;"same","Pron","1.vahI"
;I will do the same again.
;

(defrule same4
(declare (salience 4800))
(id-root ?id same)
(id-word =(- ?id 1) the)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp    same4   "  ?id "  yaha )" crlf))
)

;Commented by 14anu-ban-01 on (15-01-2015) because meaning for this sentence is coming from default rule same2
;@@@ Added by 14anu01
;It will always be the same.
;(defrule same05
;(declare (salience 5500))
;(id-root ?id same)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) the )
;(id-cat_coarse ?id adjective)
;(viSeRya-viSeRaka  ?id ?)
;(subject-subject_samAnAXikaraNa  ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id samAna))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp 	same05   "  ?id "  samAna )" crlf))
;)

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;The same is true in the design of bridges, automobiles, ropeways etc.[NCERT corpus]
;pula, svacAliwa vAhana, rajjumArga Axi kI dijAina ke lie BI yahI bAwa sawya hE.[NCERT corpus]
(defrule same5
(declare (salience 4800))
(id-root ?id same)
(id-word =(- ?id 1) the)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp    same5   "  ?id "  yahI )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (11-02-2015)
;@@@ Added by 14anu-ban-11 on (27-11-2014)
;Around the same time, in 1887, it was found that certain metals, when irradiated by ultraviolet light, emitted negatively charged particles having small speeds. (Ncert)
;लगभग उसी समय, 1887 में, यह पाया गया कि जब कुछ निश्चित धातुओं को पराबैंगनी प्रकाश द्वारा किरणित करते हैं तो कम वेग वाले ऋण-आवेशित कण उत्सर्जित होते हैं.(Ncert)
(defrule same6
(declare (salience 4850))
(id-root ?id same)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 time|location)	;added "location" by 14anu-ban-01 on (11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  same.clp 	same6   "  ?id "  usI )" crlf))
)


