;@@@ Added by 14anu-ban-03 (17-03-2015)   
;There was a lot of comment about his behaviour. [oald]  ;working on parser- 8
;उसके बर्ताव के बारे में बहुत सारी आलोचना हुई थी. [manual]
(defrule comment2
(declare (salience 100))
(id-root ?id comment)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comment.clp 	comment2  "  ?id "  AlocanA_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (17-03-2015)
;A spokesperson commented that levels of carbon dioxide were very high. [oald]
;प्रवक्ता ने व्याख्या की कि कार्बन डाइ आक्साइड का स्तर बहुत ऊँचा था . [manual]
(defrule comment3
(declare (salience 200))    
(id-root ?id comment)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)
(id-root ?id1 high)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAKyA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comment.clp 	comment3  "  ?id "  vyAKyA_kara )" crlf))
)

;$$$Modified by 14anu-ban-02(08-02-2016)
;###[COUNTER STATEMENT] The official refused to comment on the matter.[sd_verified]
;###[COUNTER STATEMENT] अधिकारी ने विषय पर टिप्पणी करने से मना किया . [self]
;The results are a clear comment on government education policy. [oald]
;परिणाम सरकारी शिक्षा नीति पर एक स्पष्ट आलोचना हैं . [manual]
;@@@ Added by 14anu-ban-03 (17-03-2015)   
;The results are a clear comment on government education policy. [oald]   ;working on parser- 62
;परिणाम सरकारी शिक्षा नीति पर एक स्पष्ट आलोचना करते हैं . [manual]
(defrule comment4
(declare (salience 300))
(id-root ?id comment)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-on_saMbanXI  ?id ?id1)
;(id-cat_coarse ?id verb)
(viSeRya-on_saMbanXI  ?id ?id1)
(id-root ?id1 policy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comment.clp 	comment4  "  ?id "  AlocanA )" crlf))
)


;-----------------------------------------Default rules--------------------------

(defrule comment0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (17-03-2015)
(id-root ?id comment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tippaNI));changed 'tIkA-tippaNI' with 'tippaNI' by sheetal(29-09-09).
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comment.clp 	comment0   "  ?id "  tippaNI )" crlf))
)
;He made negative comments to the press .
;"comment","N","1.tIkA-tippaNI"
;From time to time she contributed a personal comment on his account
;

;$$$ Modified by 14anu-ban-03 (17-03-2015)
(defrule comment1
(declare (salience 00))    ;salience reduced by 14anu-ban-03 (17-03-2015)
(id-root ?id comment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tippaNI_kara));changed 'tIkA-tippaNI_kara' with 'tippaNI_kara' by sheetal(29-09-09).
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comment.clp 	comment1   "  ?id "  tippaNI_kara )" crlf))  ;changed meaning in printout from 'tIkA-tippaNI_kara' to 'tippaNI_kara' by 14anu-ban-03 (17-03-2015)
)

;"comment","V","1.tIkA-tippaNI_karanA"
;From time to time she commented on his account.
;


