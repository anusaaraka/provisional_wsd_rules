;@@@ Added by 14anu-ban-07,2-8-14
;The area SBAC bounded by the ellipse and the radius vectors SB and SC is larger than SBPC in (Fig. 8.1a).(ncert corpus)
;दीर्घवृत्त तथा त्रिज्या सदिशों SB एवं SC द्वारा घेरा गया क्षेत्रफल SBPC की तुलना में अधिक है (चित्र 8.1a).
;Acceleration and velocity are both vectors.(oald)
;त्वरण और वेग दोनों सदिश हैं. 
(defrule vector0
(id-root ?id vector)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxiSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vector.clp 	vector0   "  ?id "  saxiSa )" crlf))
)

;@@@ Added by 14anu-ban-07,2-8-14
;Mosquitoes are the vectors in malaria.(oald)
;macCar maleriyA ke rogavAhaka howe hEM.
(defrule vector1
(declare (salience 1000))
(id-root ?id vector)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rogavAhaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vector.clp 	vector1   "  ?id "  rogavAhaka )" crlf))
)

