;@@@ Added by 14anu-ban-01 on (05-02-2015)
;He seemed stunned that the Princess Adrina had even heard of him.[collins dictionary]
;वह अवाक्/दंग था कि राजकुमारी अड्रीना ने भी उसके बारे में सुना था. [self]  
(defrule stunned0
(declare (salience 0))
(id-word ?id stunned)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id avAk/xaMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  stunned.clp 	stunned0   "  ?id "  avAk/xaMga )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-02-2015)
;They stood in stunned silence beside the bodies.[oald]
;वे शरीरों के पास  निस्तब्ध विस्मृति में खडे हुए . [self]
(defrule stunned1
(declare (salience 1000))
(id-word ?id stunned)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 silence)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id niswabXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  stunned.clp 	stunned1   "  ?id "  niswabXa )" crlf))
)
