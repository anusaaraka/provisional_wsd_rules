
;@@@ Added by 14anu-ban-02(05-09-2014)
;Adnan Sami's popularity might take a beating with this one .[karan singla]
;अदनान सामी की लकप्रियता को इस वाकये से धक्का लग सकता है .[karan singla]
(defrule beating2
(declare (salience 100))
(id-root ?id beating)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-word ?id1 take)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XakkA_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beating.clp 	beating2   "  ?id " XakkA_laga )" crlf))
)

;@@@ Added by 14anu-ban-02(31-01-2015)
;Sound beating.[rajpal]
;खूब पिटाई .(sound17)
(defrule beating3
(declare (salience 100))
(id-root ?id beating)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 sound)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pitAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beating.clp 	beating3   "  ?id " pitAI )" crlf))
)

;--------------------- Default Rules ------------
;@@@ Added by 14anu-ban-02(05-09-2014)
;I felt his heart beating like the heart of a dying bird , shot with someone's rifle.[karan singla]
;मुझे उसके दिल की धड़कन , बंदूक का शिकार हुए किसी मरते हुए पक्षी की जैसी महसूस हुई ।[karan singla]
(defrule beating1
(declare (salience 0))
(id-root ?id beating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XafaZkana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beating.clp 	beating1   "  ?id " XafaZkana )" crlf))
)

