;@@@ Added by 14anu-ban-06 (03-03-2015)
;Her youngest child died in infancy.(cambridge)
;उसका जवान बच्चा किशोरावस्था में मर गया . (manual)
(defrule infancy0
(declare (salience 0))
(id-root ?id infancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiSorAvasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infancy.clp 	infancy0   "  ?id "  kiSorAvasWA )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-03-2015)
;The system is still in its infancy.(cambridge)
;प्रणाली अभी भी उसकी प्रारम्भिक अवस्था में  है . (manual)
(defrule infancy1
(declare (salience 2000))
(id-root ?id infancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 cinema|system)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAraMBika_avasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infancy.clp 	infancy1   "  ?id "  prAraMBika_avasWA )" crlf))
)
