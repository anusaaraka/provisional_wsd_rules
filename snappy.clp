;@@@ Added by 14anu-ban-01 on (20-02-2015)
;He's a snappy dresser.[cald]
;वह सजीले कपड़े पहनने वाला व्यक्ति है.[self]
(defrule snappy1
(declare (salience 100))
(id-root ?id snappy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 dresser|attire|suit|apparel|cloth|outfit)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sajIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snappy.clp     snappy1   "  ?id "  sajIlA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (20-02-2015)
;Interruptions make her snappy and nervous.[oald]
;अवरोध/हस्तक्षेप उसे चिड़चिड़ा और अशांत बना देते हैं . 
(defrule snappy2
(declare (salience 100))
(id-root ?id snappy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(object-object_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidacidA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snappy.clp     snappy2   "  ?id "  cidacidA)" crlf))
)



;@@@ Added by 14anu-ban-01 on (20-02-2015)
;I started singing along with that snappy tune.[coca]
;मैंने उस (जीवंतता से पूर्ण/मस्ती भरी) धुन के साथ-साथ गाना शुरु कर किया . [self]
(defrule snappy3
(declare (salience 100))
(id-root ?id snappy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 tune|music)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIvanwawA_se_pUrNa/maswI_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snappy.clp     snappy3   "  ?id "  jIvanwawA_se_pUrNa/maswI_BarA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (20-02-2015)
;Snappy sayings, little stories, easy lessons they can take home and turn over in their minds.(coca)
;रोचक कहावतें,छोटी कहानियाँ,सरल पाठ [की पुस्तकें] जो वे घर लेजा सकते हैं  और अपने मन में दोहरा सकते हैं . (manual)
(defrule snappy0
(declare (salience 0))
(id-root ?id snappy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rocaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snappy.clp     snappy0   "  ?id "  rocaka)" crlf))
)

