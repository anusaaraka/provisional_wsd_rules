;@@@ Added by 14anu-ban-07, (25-03-2015)
;In 1948, Gandhi fell victim to a member of a Hindu gang.(oald)
;1948 में, गान्धी एक हिंदू दल के सदस्य के शिकार हुए थे . (manual)
(defrule victim0
(id-root ?id victim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  victim.clp      victim0   "  ?id "  SikAra )" crlf))
)

;@@@ Added by 14anu-ban-07, (25-03-2015)
; Relatives of the victim watched from the public gallery as the murder charge was read out in court.(oald)
; पीडित व्यक्ति के रिश्तेदारों ने जनता दीर्घा से देखा जैसा हि  हत्या का इल्जाम  अदालत में घोषित किया गया था .   (manual)
(defrule victim1
(declare (salience 1000))
(id-root ?id victim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 relative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIdiwa_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  victim.clp      victim1   "  ?id "  pIdiwa_vyakwi )" crlf))
)

