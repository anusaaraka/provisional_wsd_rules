;@@@Added by 14anu-ban-02(26-02-2015)
;The prosecution alleges that she was driving carelessly.[oald]
;अभियोग ने आरोप लगाया कि वह लापरवाही से गाड़ी चला रही थी . [self]
(defrule allege2
(declare (salience 1000))
(id-root ?id allege)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 prosecution)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aropa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allege.clp 	allege2   "  ?id "  Aropa_lagA )" crlf))
)

;------------------------ Default Rules ----------------------

;"alleged","Adj","1.Aropiwa{kaWiwa}"
;He is an alleged criminal.
(defrule allege0
(declare (salience 5000))
(id-root ?id allege)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id alleged )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Aropiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  allege.clp  	allege0   "  ?id "  Aropiwa )" crlf))
)

;"allege","V","1.binA_sabUwa_ke_Aropa_lagAnA"
; He alleged that he was not the culprit in the  crime.
(defrule allege1
(declare (salience 0))	;salience reduce by 14anu-ban-02(26-02-2015)
(id-root ?id allege)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binA_sabUwa_ke_Aropa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allege.clp 	allege1   "  ?id "  binA_sabUwa_ke_Aropa_lagA )" crlf))
)
