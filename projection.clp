;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;Spiky projections on top of a fence.
;बाढ के ऊपर नुकीले प्रक्षेप .
(defrule projection0
(declare (salience 4700))	;Default Rule
(id-root ?id projection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakRepa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  projection.clp 	projection0   "  ?id " prakRepa )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;According to data projections, sales will increase.
;डेटा पूर्वानुमान के अनुसार, बिक्री मे वृद्धि होगी 
(defrule projection1
(declare (salience 5000))
(id-root ?id projection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 current|data|sales|market|future|past)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvAnumAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  projection.clp 	projection1   "  ?id " pUrvAnumAna )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 25/6/2014
;Facilities are vital to the projection of U.S. force.
;सुविधाएँ यू.एस. बल की योजना के लिए जीवन सम्बन्धी हैं .
(defrule projection2
(declare (salience 4900))
(id-root ?id projection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 comapany|force|enterprise|government)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  projection.clp 	projection2   "  ?id " yojanA )" crlf))
)
