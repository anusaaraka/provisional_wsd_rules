;##############################################################################
;#  Copyright (C) 2013-2014 Prachi Rathore (prachirathore02 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;After using the cream for a month, I could see no visible difference. [oald]
;महीने से मलाई का उपयोग करने के बाद, मैं स्पष्ट अन्तर को नहीं देख सका . 
(defrule visible0
(declare (salience 5000))
(id-root ?id visible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visible.clp 	visible0   "  ?id "  spaRta)" crlf))
)

;A highly visible politician [m-w]
;एक सशक्त राजनीतिज्ञ .
(defrule visible1
(declare (salience 5000))
(id-root ?id visible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(- ?id 1) highly)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) saSakwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " visible.clp	visible1  "  ?id "  " (- ?id 1) " saSakwa )" crlf))
)

;@@@ Added by 14anu-ban-07 , 26-8-14
;An optical microscope uses visible light to ' look ' at the system under investigation. (ncert corpus)
;एक प्रकाशीय सूक्ष्मदर्शी द्वारा किसी निकाय की जाँच के लिए दृश्य - प्रकाश का उपयोग किया जाता है.
;Instead of visible light, we can use an electron beam. (ncert corpus)
;दृश्य प्रकाश के स्थान पर हम, इलेक्ट्रॉन - पुञ्ज का उपयोग कर सकते हैं. 
(defrule visible3
(declare (salience 5000))
(id-root ?id visible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) light)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) xqSya_prakASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " visible.clp	 visible3  "  ?id "  " =(+ ?id 1)" xqSya_prakASa  )" crlf))
)

;xxxxxxxxxxxxxxxxDEFAULT RULExxxxxxxxxxxxxxxxx
;$$$Modified by 14anu-ban-07,(04-02-2015)
;The house is clearly visible from the beach. (oald)
;घर समुद्रतट से स्पष्टतः दृष्टिगोचर / दृष्टिगोचर होता /दिखायी देता है . (self)
;An amazing city encircled with curved mountains, beautiful architecture of churches and houses and dense pine trees is visible after reaching Shillong .(tourism corpus)
;शिलांग  पहुँचते  ही  घुमावदार  पहाड़ों  ,  खूबसूरत  गिरजाघरों  ,  मकानों  की  शिल्पकला  तथा  सघन  चीड़  वृक्षों  से  घिरा  एक  अद्भुत  शहर   दृष्टिगोचर / दृष्टिगोचर होता /दिखायी देता  है  ।(tourism corpus)

;In the early stages the star was so bright that it was visible during the day.
;शुरू में तारे में इतनी चमक थी कि यह दिन में भी दिखायी देता था।
;शुरू में तारे में इतनी चमक थी कि यह दिन में भी  दृष्टिगोचर / दृष्टिगोचर होता /दिखायी देता था। (by 14anu-ban-07)
(defrule visible2
(declare (salience 4000))
(id-root ?id visible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id xiKa))   ;note : category of visible will always be adjective but in these sentences it's meaning is like a verb so meaning changed from xiKa to xqRtigocara.suggested by Vineet Chaitanya sir. ;by 14anu-ban-07,(04-02-2015)
(assert (id-wsd_root_mng ?id xqRtigocara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visible.clp 	visible2   "  ?id "  xqRtigocara)" crlf))
)
