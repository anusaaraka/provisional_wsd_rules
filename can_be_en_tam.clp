;@@@ Added by 14anu-ban-02(11-02-2016)
;Infectious diseases can be acquired in several ways. [oald]
;सङ्क्रामक बीमारियाँ कई कारणो से हो सकतीं हैं . [self]
(defrule can_be_en_tam1
(declare (salience 100))
(id-TAM ?id can_be_en)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 disease)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id can_be_en 0_sakawA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  can_be_en_tam.clp        can_be_en_tam1  "  ?id " 0_sakawA_hE )" crlf))
)

;---------------------Default_rule-------------

;@@@ Added by 14anu-ban-02(11-02-2016)
;There are so many things that can be done. [COCA]
;वहाँ बहुत सारी चीजें हैं जिन्हें किया जा सकता है[self]
(defrule can_be_en_tam0
(id-TAM ?id can_be_en)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id can_be_en yA_jA_sakawA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  can_be_en_tam.clp        can_be_en_tam0  "  ?id " yA_jA_sakawA_hE )" crlf))
)
