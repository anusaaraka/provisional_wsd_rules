;@@@ Added by 14anu-ban-04 (24-02-2015)
;It is difficult to assess the full extent of the damage.                [oald]
;क्षति की पूरी गंभीरता का आकलन करना मुश्किल है।                                                                                       [self]
(defrule extent2
(declare (salience 20))
(id-root ?id extent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 damage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMBIrawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  extent.clp 	extent2   "  ?id "  gaMBIrawA )" crlf))
)

;@@@ Added by 14anu-ban-04 (27-01-2015)
;You can't see the full extent of the beach from here.              [oald]
;तुम यहां से समुद्रतट की पूरी सीमा को नहीं देख सकते हो .                             [self]    
(defrule extent0
(declare (salience 10))
(id-root ?id extent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  extent.clp 	extent0   "  ?id "  sImA )" crlf))
)


;@@@ Added by 14anu-ban-04 (27-01-2015)
;The reason is that Jharkhand is such a state which is untouched to a large extent by the ill effects of urbanization .    [tourism corpus]
;कारण  यह  है  कि  ,  झारखंड  एक  ऐसा  राज्य  है  जो  शहरीकरण  के  दुष्प्रभाव  से  काफी  हद  तक  अछूता  है  ।                            [tourism corpus]
(defrule extent1     
(declare (salience 20))
(id-root ?id extent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 large) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  extent.clp 	extent1   "  ?id "  haxa )" crlf))
)


