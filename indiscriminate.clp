;@@@ Added by 14anu-ban-06 (08-04-2015)
;She's always been indiscriminate in her choice of friends. (OALD)
;वह मित्रों के चयन में हमेशा अविवेकी रही है . (manual)
(defrule indiscriminate1
(declare (salience 2000))
(id-root ?id indiscriminate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 choice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avivekI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indiscriminate.clp 	indiscriminate1   "  ?id "  avivekI )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (08-04-2015)
;The indiscriminate use of fertilizers can cause long-term problems.(cambridge)
;खाद का अन्धाधुन्ध उपयोग दीर्घावधि समस्याओं का कारण बन सकता है . (manual)
(defrule indiscriminate0
(declare (salience 0))
(id-root ?id indiscriminate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anXAXunXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indiscriminate.clp 	indiscriminate0   "  ?id "  anXAXunXa )" crlf))
)
