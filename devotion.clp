;@@@ Added by 14anu-ban-04  (10-11-2014)
;For Tibbetans this place is very important and of religious devotion .       [tourism-corpus]
;तिब्बतियों के लिए यह स्थल बहुत महत्त्वपूर्ण व धार्मिक आस्था का है ।                                  [tourism-corpus]
(defrule devotion0    
(declare (salience 10))
(id-root ?id devotion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsWA/SraxXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devotion.clp  devotion0   "  ?id "  AsWA/SraxXA )" crlf))
)


;@@@ Added by 14anu-ban-04  (10-11-2014)
;Faithfuls follow the custom of circumambulating with the belief that Lord Rama will fulfil the wishes of the devotees having been happy with their devotion  .               [tourism-corpus]
;श्रद्धालु  परिक्रमा  की  प्रथा  का  पालन  करते  हैं  इस  विश्वास  से  कि  भगवान  राम  उनकी  भक्ति  से  खुश  होकर  भक्तों  की  मनोकामना  पूर्ण  करेंगे  ।                 [tourism-corpus]
(defrule devotion1
(declare (salience 20))
(id-root ?id devotion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devotion.clp  devotion1   "  ?id "  Bakwi )" crlf))
)

;@@@ Added by 14anu-ban-04  (10-11-2014)
;When you will see the large Shani statue located here then you will yourself get drenched in the devotion of Suraputra Shanidev.[tourism-corpus]
;जब  आप  यहाँ  स्थित  विशाल  शनि  प्रतिमा  के  दर्शन  करेंगे  तो  आप  स्वयं  सूर्यपुत्र  शनिदेव  की  भक्ति  में  रम  जाएँगे  ।           [tourism-corpus]
(defrule devotion2
(declare (salience 30))
(id-root ?id devotion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 drench)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devotion.clp  devotion2   "  ?id "  Bakwi )" crlf))
)


;@@@ Added by 14anu-ban-04  (10-11-2014)
;Programs start at 5 in the evening in which Hare Krishna mantra chant , singing of devotion songs and hymns , and idol worship are included .  [tourism-corpus]
;कार्यक्रम  शाम  के  पाँच  बजे  शुरू  होते  हैं  जिसमें  हरे  कृष्ण  मंत्रोच्चार  भक्ति के गीतों और भजनों  का  गायन  और  मूर्ति  की  पूजा  शामिल  है  ।   [tourism-corpus]

(defrule devotion3
(declare (salience 40))
(id-root ?id devotion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 song|path)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bakwi))
(assert (id-wsd_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  devotion.clp     devotion3  "  ?id " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devotion.clp  devotion3  "  ?id "  Bakwi )" crlf))
)


