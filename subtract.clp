
(defrule subtract0
(declare (salience 0));salience reduced to 0 from 5000 by 14anu-ban-01 on 31-8-14.
(id-root ?id subtract)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtract.clp 	subtract0   "  ?id "  GatA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (31-08-2014).
;405:Hence the rule: When two quantities are added or subtracted, the absolute error in the final result is the sum of the absolute errors in the individual quantities.  [NCERT corpus]
;अतः, नियम यह है: जब दो राशियों को सङ्कलित या व्यवकलित किया जाता है, तो अन्तिम परिणाम में निरपेक्ष त्रुटि उन राशियों की निरपेक्ष त्रुटियों के योग के बराबर होती है.[NCERT corpus]
(defrule subtract1
(declare (salience 1000)) 
(id-root ?id subtract)
?mng <-(meaning_to_be_decided ?id)
(Domain physics);added on (27-10-2014)
(conjunction-components  ?id1 ?id2 ?id)
(id-root ?id1 or)
(id-root ?id2 add)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavakaliwa_kara))
(assert (kriyA_id-subject_viBakwi ?id ko));added on (30-10-2014)
(assert (id-domain_type ?id physics));added on (30-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtract.clp 	subtract1   "  ?id "  vyavakaliwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  subtract.clp 	subtract1    "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  subtract.clp 	subtract1   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
; How to subtract, add and multiply vectors?        [NCERT corpus: exchanged 'subtract' and 'add']
;सदिशों को कैसे घटाया ,जोडा  या गुणा किया जाता है.                 [NCERT corpus:exchanged 'subtract' and 'add']
(defrule subtract2
(declare (salience 100))
(id-root ?id subtract)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)	
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* " subtract.clp 	subtract2   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtract.clp 	subtract2   "  ?id "  GatA )" crlf))
)
