;@@@ Added by 14anu-ban-11 on (12-02-2015)
;Reynolds dosed his voice with warmth again.(coca)
;रेनल्ड्ज ने फिर से जोश के साथ अपनी आवाज तेज की.(self)
(defrule warmth1
(declare (salience 10))
(id-root ?id warmth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-root ?id1 dose)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  warmth.clp 	warmth1   "  ?id "  joSa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (12-02-2015)
;An agreeable warmth welcome in the house.(hinkhoj)
;सुखद मित्रभाव से घर में स्वागत. (self)
(defrule warmth2
(declare (salience 20))
(id-root ?id warmth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 welcome)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miwraBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  warmth.clp 	warmth2   "  ?id "  miwraBAva )" crlf))
)

;------------------ Default Rules -------------

;@@@ Added by 14anu-ban-11 on (12-02-2015)
;The blanket provided warmth in the cold weather.(hinkhoj)
;कम्बल ने ठण्डा के मौसम में गर्माहट प्रदान की . (self)
(defrule warmth0
(declare (salience 0))
(id-root ?id warmth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garmAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  warmth.clp   warmth0   "  ?id "  garmAhata )" crlf))
)


