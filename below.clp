
;@@@ Added by 14anu-ban-02(27.08.14)
;There is a kunda below the hill .
;पहाड़ी  के  नीचे  एक  कुण्ड  है  ।
(defrule below0
(declare (salience 0))
(id-root ?id below)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_nIce))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  below.clp 	below0   "  ?id "  ke_nIce )" crlf))
)



;@@@ Added by 14anu-ban-02(27.08.14)
;A simple method for estimating the molecular size of oleic acid is given below. 
;ओलीक अम्ल अणु के साइज का आकलन करने की एक सरल विधि नीचे दी गई है. 
(defrule below1
(declare (salience 0))
(id-root ?id below)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIce))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  below.clp 	below1   "  ?id "  nIce )" crlf))
)

;@@@ Added by 14anu-ban-02(28.08.14)
;Children's Park - This park is appropriate for the entertainment of children below 14 years .
;बच्चों  का  पार्क  -  14  वर्ष  से  कम  आयुवाले  बच्चों  के  विनोद  के  लिए  यह  पार्क  उपयुक्त  है  ।
(defrule below2
(declare (salience 10))
(id-root ?id below)
?mng <-(meaning_to_be_decided ?id)
(id-word  =(+ ?id 2) years|year)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  below.clp 	below2   "  ?id "  se_kama )" crlf))
)

