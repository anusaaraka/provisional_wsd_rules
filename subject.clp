;##############################################################################
;#  Copyright (C) 2014-2015 Archit Bansal (archit.bansal18@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;Commented by 14anu-ban-01 on (12-01-2015) because this sentence has parsing problem and correct meaning is coming from default rule subject_tmp on running the sentence on parse no. 2
;@@@ Added by 14anu04 on 25-June-2014
;Maths is a boring subject. 
;गणित एक उबाऊ विषय है . 
;(defrule subject_tmp3
;(declare (salience 3000))
;(id-root ?id subject)
;?mng <-(meaning_to_be_decided ?id)
;(subject-subject_samAnAXikaraNa ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id viRaya))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject_tmp3  " ?id "   viRaya  )" crlf))
;)


;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu04 on 25-June-2014
;The noble king cares for his subjects.
;कुलीन राजा अपनी प्रजा के लिए परवाह करता है. 
(defrule subject_tmp4
(declare (salience 3200))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id1 ?id)           ;added '?id1' by 14anu-ban-01 on (12-01-2015)
(kriyA-subject ?id1 ?id2)                ;Added by 14anu-ban-01 on (12-01-2015)
(id-root ?id2 king|ruler|dictator|instructor|queen|prince|princess)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject_tmp4  " ?id "   prajA  )" crlf))
)

;@@@ Added by 14anu04 on 25-June-2014
;The subjects of the empire obeyed the king. 
;साम्राज्य की प्रजा ने राजा की आज्ञा का पालन किया .  
(defrule subject_tmp5
(declare (salience 3400))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 empire|state|kingdom)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject_tmp5  " ?id "   prajA  )" crlf))
)

;@@@ Added by 14anu-ban-11 on (09-03-2015)
;Focus the camera on the subject.(oald) 
;वस्तु पर कैमरा का केंद्र बिंदु   .   (self)
(defrule subject1
(declare (salience 3100))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id1 ?id)
(id-root ?id1 camera)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject1  " ?id "  vaswu)" crlf))
)


;@@@ Added by 14anu-ban-11 on (09-03-2015)
; We need male subjects between the ages of 18 and 25 for the experiment.  (oald)
;हमें प्रयोग के लिए 18 और 25 उम्र के बीच के नर व्यक्तियों की आवश्यकता है .   (self)
(defrule subject2
(declare (salience 3500))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 male)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject2  " ?id "  vyakwi )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu04 on 25-June-2014
;Focus on the subject of the experiment.
;प्रयोग के विषय पर ध्यान दीजिए. 
(defrule subject_tmp
(declare (salience 3000))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)                ;corrected 'course' to 'coarse'by 14anu-ban-01 on (12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject_tmp  " ?id "   viRaya  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu04 on 25-June-2014
;The Roman Empire subjected most of Europe to its rule.
;रोम साम्राज्य ने ज्यादातर यूरोप को अपने नियमों से शासित किया.  
(defrule subject_tmp2
(declare (salience 3000))
(id-root ?id subject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)                    ;corrected 'course' to 'coarse'by 14anu-ban-01 on (12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  SAsiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " subject.clp  subject_tmp2  " ?id "   SAsiwa_kara  )" crlf))
)

