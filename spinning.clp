;---------------------------Default Rule-----------------------------------
;@@@ Added by 14anu-ban-01 on (21-10-2014)
;This is a small-scale cotton spinning firm.[self:with reference to oald] <Parser problem in this sentence,it is considering 'spinning' as a verb instead of a noun>
;यह एक लघु कपास कर्तन  व्यवसाय संघ है. [self]     
;Spinning is one of my hobbies.[spin.clp]
;कर्तन/कताई मेरी रुचियों/अभिरुचियों में से एक है.[self]                                                
(defrule spinning0
(declare (salience 0))
(id-root ?id spinning)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karwana/kawAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " spinning.clp 	spinning0   "  ?id "  karwana/kawAI)" crlf))
)
;----------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;A prominent example of this kind of rotation is a top spinning in place (Fig. 7.5 (a)).[NCERT corpus]
;इस प्रकार के घूर्णन के मुख्य उदाहरणों में एक है, एक ही स्थान पर घूमता लट्टू ( चित्र 7.5(a)).[NCERT corpus]

(defrule spinning1
(declare (salience 0))
(id-root ?id spinning)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) top)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " spinning.clp 	spinning1   "  ?id " GUmawA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;We know from experience that the axis of such a spinning top moves around the vertical through its point of contact with the ground, sweeping out a cone as shown in Fig. 7.5(a).[NCERT corpus]
;apane anuBava ke AXAra para hama yaha jAnawe hEM ki isa prakAra GUmawe lattU kI akRa, BUmi para isake samparka-biMxu se gujarawe aBilamba ke pariwaH eka Safku banAwI hE jEsA ki ciwra 7.5(@a) meM xarSAyA gayA hE.[NCERT corpus]
(defrule spinning2
(declare (salience 0))
(id-root ?id spin)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " spinning.clp 	spinning2  "  ?id " GUmawA)" crlf))
)

(defrule spinning3
(declare (salience 5100))
(id-root ?id spinning)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) firm|company)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " spinning.clp 	spinning3  "  ?id " karwana)" crlf))
)
