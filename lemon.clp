;$$$ Modified by 14anu02 on 19.6.14
;@@@ Added by Nandini(16-12-13)
;Pick up some lemons when you go to the stores.[hinkoja-dict]
;कुछ नींबू ले लीजियेगा जब आप दुकान जाये.
(defrule lemon0
(declare (salience 200))
(id-root ?id lemon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-object  ? ?id)	;Commented by 14anu02
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIMbU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lemon.clp 	lemon0   "  ?id "  nIMbU)" crlf))
)

;Commented by 14anu02 on 19.06.14 since 'lemon yellow' is a multiword expression.Added lemon_yellow in multiword dictionary 
;@@@ Added by Nandini (16-12-13)
;Glass object with lemon yellow look nice.
;(defrule lemon1
;(declare (salience 100))
;(id-root ?id lemon)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nIbuI_raMga))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lemon.clp 	lemon1   "  ?id "  nIbuI_raMga)" crlf))
;)

;$$$ Modified by 14anu02 on 19.6.14
;@@@ Added by Nandini(16-12-13)
;She was wearing a lemon saree.[hinkoja-dict]
;उसने एक हल्की पीली रङ्ग की साडी पहनी हुई थी . 
(defrule lemon2
(declare (salience 300))
(id-root ?id lemon)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA_pIlA_raMga_kA)) ;meaning changed from 'halkA_pIlA_raMga' to 'halkA_pIlA_raMga_kA' by 14anu2
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lemon.clp 	lemon2   "  ?id "  halkA_pilA_raMga_kA)" crlf))
)

;@@@ Added by 14anu05 on 28.06.14
;Add a dash of lemon juice.
;थोडा सा नींबू का रस मिलाओ . 
(defrule lemon3
(declare (salience 3500))
(id-root ?id lemon)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 juice)
(test(=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nIMbU_kA_rasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  lemon.clp     lemon3   "  ?id "  " ?id1 "  nIMbU_kA_rasa)" crlf))
)
