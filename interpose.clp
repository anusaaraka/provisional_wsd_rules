;@@@ Added by 14anu-ban-06 (13-04-2015)
;‘Just a minute,’ Charles interposed. ‘How do you know?’(OALD)
;'सिर्फ एक मिनट,' चार्ल्स ने बीच में टोका .'आप कैसे जानते हैं?'  (manual)
(defrule interpose1
(declare (salience 2000))
(id-root ?id interpose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bIca_meM_toka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpose.clp 	interpose1   "  ?id "  bIca_meM_toka )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (13-04-2015)
;He quickly interposed himself between Mel and the doorway. (OALD)
;उसने फटाफट मेल और द्वारपथ के बीच स्वयं को डाला . (manual)
(defrule interpose0
(declare (salience 0))
(id-root ?id interpose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpose.clp 	interpose0   "  ?id "  dAla )" crlf))
)
