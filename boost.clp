
(defrule boost0
(declare (salience 5000))
(id-root ?id boost)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boost.clp 	boost0   "  ?id "  baDZAvA )" crlf))
)

;"boost","N","1.baDZAvA"
;This small gift as a token will be a boost for the child's improvement. 
;
(defrule boost1
(declare (salience 4900))
(id-root ?id boost)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZAvA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boost.clp 	boost1   "  ?id "  baDZAvA_xe )" crlf))
)

;"boost","V","1.baDZAvA_xenA"
;The unexpected increase in share prices helped to boost the market.
;

;@@@ Added by 14anu-ban-02 (10-11-2014)
;Research published in the journal Food Chemistry shows that peanuts contain high concentrations of antioxidant polyphenols, primarily a compound called p-coumaric acid, and that roasting can increase peanuts' p-coumaric acid levels, boosting their overall antioxidant content by as much as 22percent.[agriculture]
;फूड कैमिस्ट्री  में प्रकाशित हुआ शोध पत्र दर्शाता है कि मूँगफली में एंटीऑक्सीडेंट polyphenolsकी उच्च सांद्रता होती हैं जिसे मुख्य रूप से पी -coumaric अम्ल कहते  है , भूनने से  पी -coumaric अम्ल का स्तर बढ जाता हैं,और उनके सभी एंटीऑक्सीडेंट तत्व   २२% तक बढ जाते हैं.[manual]
(defrule boost2
(declare (salience 4900))
(id-root ?id boost)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 content)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boost.clp 	boost2   "  ?id "  baDZa_jA )" crlf))
)
