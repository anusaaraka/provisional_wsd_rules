;@@@ Added by 14anu03 on 16-june-2014
;The English Wikipedia comprises of more than two million articles.
;अङ्ग्रेजी विकिपेडीया से ज्यादा दो दस-लाख वस्तुओं का समाविष्ट करता है . 
(defrule more100
(declare (salience 5500))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 than)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 se_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  more.clp     more100   "  ?id "  " ?id1 "  se_jyAxA )" crlf))
)

;Added by Meena(5.02.10)
;How much more spilled?
(defrule more0
(declare (salience 5001))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(or (viSeRaNa-viSeRaka ?id ?id1) (kriyA-kriyA_viSeRaNa ?k ?id)) ;How much more should we work on this?
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp      more0   "  ?id "  Ora )" crlf))
)

(defrule more1
(declare (salience 5000))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp 	more1   "  ?id "  aXika )" crlf));;inconsistency in the mng in assert & print statement has been corrected by Sukhada (15.3.10)).
)

(defrule more2
(declare (salience 4900))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp 	more2   "  ?id "  aXika )" crlf))
)

;@@@ Added by Nandini(4-12-13)
;However, there is more to a star than just its shine.[from mail]
;लेकिन तारे में चमक के अलावा और भी बहुत कुछ होता है।
(defrule more3
(declare (salience 5005))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-word ?id1 star)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora_BI_bahuwa_kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp 	more3   "  ?id "  Ora_BI_bahuwa_kuCa )" crlf))
)


;@@@ Added by 14anu24 
;Arbitration is more like going to court , and is very straightforward . 
;मध्यस्थता करवाना अदालत जाने से मिलता जुलता है और बहुत सरल होता है .
(defrule more4
(declare (salience 5800))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) like)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp     more4   "  ?id "  - )" crlf))
)

;default_sense && category=adjective	aXika	0
;"more","Adj","1.aXika/jyAxA"
;I have more pens than he has.
;
;

;@@@ Added by 14anu-ban-08 (04-12-2014)
;This example justifies the idea to retain one more extra digit (than the number of digits in the least precise measurement) in intermediate steps of the complex multi-step calculations in order to avoid additional errors in the process of rounding off the numbers.[NCERT]
;उपरोक्त उदाहरण, जटिल बहुपदी परिकलन के मध्यवर्ती पदों में (कम से कम परिशुद्ध माप में अङ्कों की सङ्ख्या की अपेक्षा) एक अतिरिक्त अङ्क रखने की धारणा को न्यायसङ्गत ठहराता है, जिससे कि सङ्ख्याओं की पूर्णाङ्कन प्रक्रिया में अतिरिक्त त्रुटि से बचा जा सके.    [NCERT]
(defrule more5
(declare (salience 5501))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 extra)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 awirikwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  more.clp     more5  "  ?id "  " ?id1 " awirikwa )" crlf))
)


;@@@ Added by 14anu-ban-08 (04-12-2014)
;The resolution of such an electron microscope is limited finally by the fact that electrons can also behave as waves ; (You will learn more about this in class XII).    [NCERT]
;इस प्रकार के इलेक्ट्रॉन-सूक्ष्मदर्शी का विभेदन भी अन्ततः इसी तथ्य द्वारा सीमित होता है कि इलेक्ट्रॉन भी तरङ्गों की तरह व्यवहार कर सकते हैं (इस विषय में विस्तार से आप कक्षा XII में पढेंगे).   [NCERT]
;You will learn more about the significant figures in section 2.7.   [NCERT]
;नुभाग 2.7 में आप सार्थक अङ्कों के विषय में और विस्तार से सीखेंगे.    [NCERT]
(defrule more6
(declare (salience 5009))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-kriyA_viSeRaNa ?id1 ?id)(kriyA-object ?id1 ?id))
(id-root ?id1 learn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viswAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp     more6   "  ?id "  viswAra )" crlf))
)


;@@@ Added by 14anu-ban-08 (04-12-2014)
;The intermediate case where the deformation is partly relieved and some of the initial kinetic energy is lost is more common and is appropriately called an inelastic collision.    [NCERT]
;इसके अतिरिक्त मध्यवर्ती स्थिति आमतौर पर देखने को मिलती है जब विकृति आंशिक रूप से कम हो जाती है और प्रारम्भिक गतिज ऊर्जा की आंशिक रूप से क्षति हो जाती है; इसे समुचित रूप से अप्रत्यास्थ सङ्घट्ट कहते हैं.       [NCERT]
(defrule more7
(declare (salience 5500))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 common)
(test (=(+ ?id 1) ?id1))
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AMSika_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  more.clp     more7  "  ?id "  " ?id1 " AMSika_rUpa_se )" crlf))
)

;@@@Added by 14anu-ban-08 (10-03-2015)
;She prefers to answer any fan mail herself for a more personal touch.  (oald)
;वह ज़्यादा वैयक्तिक रूप देने लिए किसी प्रशंसक की डाक को स्वयं उत्तर देना पसन्द करती है .  (manual)
(defrule more8
(declare (salience 5000))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ?id1 ?id)
(viSeRya-viSeRaNa ?id1 ?id2)
(id-root ?id1 touch)
(id-root ?id2 personal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp 	more8   "  ?id "  jyAxA )" crlf))
)

;@@@Added by 14anu-ban-08 (25-03-2015)
;I only need one more card to complete the set. [oald]
;मुझे  समूह तैयार करने के लिए केवल एक और कार्ड की आवश्यकता है . [manual]
(defrule more9
(declare (salience 5009))
(id-root ?id more)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 card)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  more.clp     more9   "  ?id "  Ora )" crlf))
)

