
;Added by sheetal(23-03-10)
;He is apparently an expert on dogs .
(defrule on01
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) expert)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp        on01   "  ?id "  kA )" crlf))
)


;(defrule on0
;(declare (salience 4999))
;(id-root ?id on)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(- ?id 1) noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id para))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on0   "  ?id "  para )" crlf))
;)

(defrule on1
(declare (salience 4900))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) charge|attack|complaint|cost|electric charge|care|guardianship|rush|thrill|excitement|emotion|feeling|billing|asking|accusation|explosive charge)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on1   "  ?id "  meM )" crlf))
)

(defrule on2
(declare (salience 4800))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) panel|commiittee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on2   "  ?id "  para )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (09-04-2015)
;She was keen to take on the role of producer. 	[oald]
;वह निर्माता की भूमिका को लेकर इच्छुक थी . 	[Manual]
;$$$ Modified by Shirisha Manju -- used 'word' fact instead 'root' and added sundays|mondays|tuesdays|wednesdays|thursdays|fridays to list
;I go to school on Mondays, Tuesdays, Wednesdays, Thursdays and Fridays.
;mEM somavAra, mafgalavAra, buXavAra, guruvAra Ora SukravAra ko vixyAlaya jAwA hUz.
;On Saturdays I play games at school. 
;SanivAra ko mEM vixyAlaya meM Kela KelawA hUz.
;Modified by Aditya and Hardik(5.7.13),IIT(BHU) (Added month names and number category)
;I completed my college on 23rd july 2013.
;Modified by Meena(21.7.11)
;The Dow fell 22.6% on Black Monday.
(defrule on3
(declare (salience 4700))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-on_saMbanXI ?kri ?id1)(viSeRya-on_saMbanXI  ?viSeRya  ?id1))
;(id-root  ?id1 Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday)
(or (id-word  ?id1 sunday|monday|tuesday|wednesday|thursday|friday|saturday|january|february|march|april|may|june|july|august|september|october|november|december|sundays|mondays|tuesdays|wednesdays|thursdays|fridays|saturdays|role)(id-cat_coarse ?id1 number));Modified to lowcase by Roja(13-06-13). As now we are using NER to get PropN info , NER doesnt recognize weekdays as Named Entities. So changed to lowcase. Ex:They are playing an important match against Liverpool on Saturday. 	;added 'role' by 14anu-ban-09 on (09-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on3   "  ?id "  ko )" crlf))
)

(defrule on4
(declare (salience 4600))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) cash|money|gun|drugs)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on4   "  ?id "  ke_pAsa )" crlf))
)

(defrule on5
(declare (salience 4500))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagAwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on5   "  ?id "  lagAwAra )" crlf))
)

;"on","Adv","1.lagAwAra"
;She talked on for two hours without stopping.




;Added by Meena(7.3.11)
;Golconda fort is situated on the western outskirts of Hyderabad.
;The big question on everybody's mind is who killed OJ.{ "The big question on my mind is who killed OJ." for openlogos as it takes the wrong relation (viSeRya-on_saMbanXI question everybody's) }
(defrule on6
(declare (salience 4400))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(or(and(kriyA-on_saMbanXI =(- ?id 1) ?id1)
   (id-word ?id1 outskirts))
   (and(viSeRya-on_saMbanXI  =(- ?id 1) ?id1)
       (id-word ?id1 mind))
)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp        on6   "  ?id "  meM )" crlf))
)



;Salience reduced by Meena(7.3.11)
(defrule on7
(declare (salience 0))
;(declare (salience 4400))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on7   "  ?id "  para )" crlf))
)


;$$$ Modified by Sonam Gupta MTech IT Banasthali 11-3-2014 (silence reduced)
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Grandpa cooked their meal on the stove and the three of them sat down to eat. [Gyannidhi]
;दादा जी ने स्टोव पर भोजन पकाया और तीनों ने साथ बैठकर खाया. 
(defrule on8
(declare (salience 4350))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI  ? ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  on.clp 	on8   "  ?id "  para )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;From then on the plan they had formulated was carried out with smooth precision.  [Gyannidhi]
;तब से उन्होंने जो योजना बनाई थी वह बिना किसी बाधा के बहुत जल्द कामयाब हुई।
(defrule on9
(declare (salience 4850))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-on_saMbanXI  ?id1 ?)
(id-root ?id1 then)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 waba_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " on.clp  on9  "  ?id "  " ?id1 "  waba_se )" crlf))
)

;@@@ Added by Preeti(9-5-14)
;He is cheating on his wife. [Oxford Advanced Learner's Dictionary]
;vaha apanI pawnI ke alAvA nAjAyaja_saMbaMXa_raKa rahA hE.
(defrule on10
(declare (salience 4350))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
;(kriyA-on_saMbanXI   =(- ?id 1) ?)
(id-root =(- ?id 1) cheat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ke_alAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on10  "  ?id "  ke_alAvA )" crlf))
)


;"on","Prep","1.para"
;The book is on the table.
;
;@@@ Added by 14anu-ban-05 on 16-08-2014
;Tourists can also roam on foot in Ooty .[PARALLEL CORPUS]
;UtI meM sElAnI pExala BI GUma sakawe hEM
(defrule on011
(declare (salience 4500))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(id-root =(+ ?id 1) foot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on011  "  ?id "  - )" crlf))
)



;@@@ Added by 14anu-ban-09 on (29-01-2015)
;He slid on his back.				[collins dictionary]
;वह पीठ के बल गिर/फिसल गया.			[Manual]

(defrule on11
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI   ? ?id1)
(id-root ?id1 back)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ke_bala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on11  "  ?id "  ke_bala )" crlf))
)

;@@@ Added by 14anu-ban-05 on 11-08-2014
;;You must give references on the foot of a page.[from  foot.clp file]
;wuma peja ke nicale BAga meM saMxarBa sUcI avaSya xo.
(defrule on012
(declare (salience 4500))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ? ?id1)
(viSeRya-of_saMbanXI  ?id1 ?)
(id-root ?id1 foot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on012  "  ?id "  meM )" crlf))
)


;@@@ Added by 14anu-ban-09 on (12-02-2015)
;Our grandmother would in no time prepare us a glass of warm milk with saffron sprinkled on top.  [e-mail]
;हमारी दादी बिना समय लगाये गर्म दूध के गिलास ऊपर से केसर छिड़क कर तैयार करती हैं .               			  [Manual]

(defrule on12
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(viSeRya-on_saMbanXI   ? ?id1)
(id-root ?id1 top)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on12  "  ?id "  - )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-02-2015)
;She was acquitted on all charges.		[oald]
;वह सभी आरोपों से बरी की गयी थी .  			[self]

(defrule on13
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI   ?id1 ?id2)
(id-root ?id1 acquit)
(id-root ?id2 charge)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on13  "  ?id "  se )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-02-2015)
;She passes by my house on her way.	[pass.clp]
;वह अपने रास्ते में मेरे घर से होकर गुजरती हैं.	[Self]

(defrule on14
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))	;modified '?id2' to '?id1' by 14anu-ban-09 on (04-03-2015)	
(id-cat_coarse ?id preposition)
(viSeRya-on_saMbanXI   ? ?id1)
(id-root ?id1 way)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on14  "  ?id "  meM )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (09-04-2016): restricted the rule
;### Counter Examples### He spoke last on this issue. 	[sd_verified]
;वह इस विषय पर आखिर में बोला	[sd_verified]
;It is true that you are my friend but I can not go along with you on this issue. [sd_verified]
;आप मेरे मित्र हैं यह सत्य है परन्तु मैं इस विषय पर आपके साथ सहमत_नहीं_हो सकता हूँ	[sd_verified]
;He opened a conversation on the issue of animal rights. [sd_verified]
;उसने पशुओं_के अधिकारों के विषय पर वार्तालाप आरम्भ किया.[sd_verified]
;@@@ Added by 14anu-ban-09 on (04-03-2015)
;He plays on defence.	;Added by 14anu-ban-09 on (03-04-2015)
;वह बचाव में खेलता  है . 		;Added by 14anu-ban-09 on (03-04-2015)
;The government has not moved on this issue.  [oald]
;सरकार इस समस्या में बदलाव नहीं ला रही हैं.  [self]
(defrule on15
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI ?id2 ?id1)	;changed ? to ?id2 by 14anu-ban-01
(id-root ?id1 issue|defence)	;added 'defence' by 14anu-ban-09 on (03-04-2015)
(id-root ?id2 ?root&~speak&~go&~open)	;added by 14anu-ban-01
(pada_info (group_head_id ?id1)(preposition ?id))	;added by 14anu-ban-01
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on15  "  ?id "  meM )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-03-2015)
;How can parents capitalize on the computer's potential for spurring their child's development? [Report Set 4]
;कैसे माँ-बाप उनके बच्चों के विकास को प्रेरित करने के लिए संगणक के सामर्थ्य का लाभ उठा सकते हैं. 			[Manual]
(defrule on16
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI   ?id1 ?id2)
(id-root ?id1 capitalize)
(id-root ?id2 potential)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on16  "  ?id "  kA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-03-2015)
;Give me the dope on the new boss. 	[oald]
;मुझे नये बास के बारे में जानकारी दीजिए.		[manual]

(defrule on17
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(id-cat_coarse ?id preposition)
(viSeRya-on_saMbanXI   ?id1 ?id2)
(id-root ?id1 dope)
(id-root ?id2 TV|boss|teacher|computer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on17  "  ?id "  ke_bAre_meM )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;Every year the school puts on a musical production. 	[oald]
;प्रत्येक वर्ष विद्यालय में एक संगीत प्रस्तुति रखी जाती है .	[Manual] 
(defrule on18
(declare (salience 5000))
(id-root ?id on)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-on_saMbanXI   ? ?id1)
(id-root ?id1 production)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " on.clp  on18 "  ?id "  - )" crlf))
)


