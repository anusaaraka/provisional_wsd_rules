;@@@ Added by 14anu-ban-05 on (17-04-2015)
;You need to keep a good grip on reality in this job.[OALD]
;आपको इस काम मे वास्तविकता पर अच्छी  समझ  रखने की जरूरत है . [MANUAL]
(defrule grip3
(declare (salience 4001))
(id-root ?id grip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grip.clp 	grip3   "  ?id "  samaJa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (17-04-2015)
;The book grips you from start to finish. [OALD]
;पुस्तक शुरु से अन्त तक  आपकी  दिलचस्पी बनाये रखती हैं . [MANUAL]
(defrule grip4
(declare (salience 4900))
(id-root ?id grip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 book)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilacaspI_banAye_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grip.clp 	grip4   "  ?id "  xilacaspI_banAye_raKa )" crlf))
)


(defrule grip0
(declare (salience 5000))
(id-root ?id grip)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id gripping )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id uwsAhiwa_karane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grip.clp  	grip0   "  ?id "  uwsAhiwa_karane_vAlA )" crlf))
)

;"gripping","Adj","1.uwsAhiwa_karane_vAlA"
;The film had gripping storyline.
;
(defrule grip1
(declare (salience 4900))
(id-root ?id grip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kasa_kara_pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grip.clp 	grip1   "  ?id "  kasa_kara_pakadZa )" crlf))
)

;default_sense && category=verb	pakadZa	0
;"grip","VT","1.pakadZanA"
;I gripped the overhead rod in the bus.
;
;

;@@@ Added by 14anu-ban-05 on (17-04-2015)
;Keep a tight grip on the rope.	[OALD]
;रस्सी पर मजबूत पकड़ बनाये रखिए . 	[MANUAL]
(defrule grip2
(declare (salience 4000))
(id-root ?id grip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grip.clp 	grip2   "  ?id "  pakadZa )" crlf))
)


