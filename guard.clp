;@@@ Added by 14anu-ban-05 on (09-02-2015)
;Guards have been posted along the border.[oald]
;रक्षक सीमा के पास नियुक्त किए गये हैं . [MANUAL]

(defrule guard2
(declare (salience 5001))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 post)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard2   "  ?id "  rakRaka )" crlf))
)


;@@@ Added by 14anu-ban-05 on (09-02-2015)
;The police officers guarded the prisoner.	[wordreference forum]
;पुलिस अधिकारी कैदी की निगरानी कर रहे थे			[manual]

(defrule guard3
(declare (salience 5002))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 prisoner)		;more constraints can be added
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id nigarAnI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard3   "  ?id "  nigarAnI_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  guard.clp 	guard3   "  ?id " kI )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-02-2015)
;This ingredient guards your teeth against decay.	[wordreference forum]
;यह घटक क्षय के विरुद्ध आपके दाँतों की रक्षा करते हैं.			[manual] 

(defrule guard4
(declare (salience 5003))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-against_saMbanXI  ?id ?id1)
(id-root ?id1 decay)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard4   "  ?id "  rakRA_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (09-02-2015)
;There was a man on guard, wearing leather armor and a helmet that didn't match.[COCA]
;वहाँ पहरे पर एक आदमी था, जिसने चमड़े से बना कवच और हैल्मेट पहन रखा था जो कि मेल नहीं खा रहे थे.		[MANUAL]

(defrule guard5
(declare (salience 5004))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard5   "  ?id "  paharA )" crlf))
)

;--------------------- Default Rules ---------------
(defrule guard0
(declare (salience 5000))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paharexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard0   "  ?id "  paharexAra )" crlf))
)

;"guard","VT","1.raKavAlI_karanA"
;I am  guarding the field against any cattle intrusion.
(defrule guard1
(declare (salience 4900))
(id-root ?id guard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKavAlI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guard.clp 	guard1   "  ?id "  raKavAlI_kara )" crlf))
)

