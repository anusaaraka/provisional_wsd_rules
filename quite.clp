
(defrule quite0
;(declare (salience 5000)) ; Commented salience by Anita
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niwAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite0   "  ?id "  niwAnwa )" crlf))
)

;"quite","Adj","1.niwAnwa"


;Added by Meena(7.9.10)
;We meet the hero quite early in the film .
(defrule quite1
(declare (salience 4900))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 early)
(viSeRaNa-viSeRaka  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bilkula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp     quite1   "  ?id "  bilkula )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (22-02-2016)
;John is quite certainly a better choice.	[sd_verified]
;जॉन सचमुच निश्चित रूप से एक अधिक बेहतर विकल्प है	[sd_verified]
;Salience reduced by Meena(7.9.10)
(defrule quite2
;(declare (salience 0))		;commented by 14anu-ban-01 
;(declare (salience 4900))	;commented by 14anu-ban-01
(declare (salience 5000))	;added by 14anu-ban-01 
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka  ?id1 ?id)	;added by 14anu-ban-01 
(id-root ?id1 certainly)				;added by 14anu-ban-01
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sacamuca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite2   "  ?id "  sacamuca )" crlf))
)

;@@@ Added by Anita-12.12.2013
;The ladder will not quite reach the top of the wall. [by mail sentence]
;सीढी दीवार के सर्वोच्च स्थान तक पूरी तरह से नहीं पहुँचेगी . [verified sentence]
(defrule quite4
(declare (salience 5000))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ladder)
(kriyA-subject  ?kriya ?id1)
(kriyA-kriyA_viSeRaNa  ?kriya ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrI_waraha_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite4   "  ?id "  pUrI_waraha_se )" crlf))
)

;default_sense && category=adverb	kAPI_haxa_waka	0
;"quite","Adv","1.kAPI_haxa_waka"
;He writes quite well.
;You're quite right.
;--"2.pUrI_wOra_se"
;Are you quite sure?  
;--"3.sacamuca_hI"
;The food was quite delicious.
;--"4.TIka_hE_!"
;They should talk about it like two adults. `Quite?'.
;
(defrule quite3
(declare (salience 4800))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite3   "  ?id "  kAPI )" crlf))
)

;"quite","Det","1.kAPI"
;The new actress is quite a beauty.
;It was quite a sudden change.
;It is quite small a house.

;@@@ Added by 14anu-ban-11
;The forces are not quite known fully but the state of motion is known. 
;bala pUrNa rUpa se pUNrwayA jFAwa nahIM hEM paranwu gawi kI avasWA jAnI jAwI hE.
(defrule quite5
(declare (salience 5000))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ?id1 ?id)
(id-root ?id1 known) ;Added by 14anu-ban-11 on (08-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNawayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite5   "  ?id "  pUrNawayA )" crlf))
)



;@@@ Added by 14anu-ban-11 on (31-10-2014)
;Because SI units used decimal system, conversions within the system are quite simple and convenient. [ncert]
; क्योँकि एस आई मात्रक ने दशमलव प्रप्राली का उपयोग किया, इस निकाय के भीतर परिवर्तन पूर्णतया सरल और सुविधाजनक हैं . [ncert]
(defrule quite6
(declare (salience 5000))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ?id1 ?id)
(id-root ?id1 simple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyanwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite6   "  ?id "  awyanwa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (22-02-2016) 
;You're quite right.	[quite.clp]
;आप काफी हद तक सही हैं . 	[self]
(defrule quite8
(declare (salience 5000))	
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 right)	
(viSeRya-viSeRaka  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI_haxa_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp     quite8  "  ?id "  kAPI_haxa_waka )" crlf))
)

;@@@ Added by 14anu17
;Bacterial meningitis is quite rare.
;जीवाण्विक गर्दन तोड बुखार काफी दुर्लभ है.
(defrule quite7
(declare (salience 4900))
(id-root ?id quite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quite.clp 	quite7   "  ?id "  kAPI )" crlf))
)

