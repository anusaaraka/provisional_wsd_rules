
(defrule acute0
(declare (salience 5000))
(id-root ?id acute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIvra)); Replaced 'wivra' with 'wIvra' by Shirisha Manju Suggested by Chaitanya Sir (14-10-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acute.clp 	acute0   "  ?id "  wIvra )" crlf))
)

(defrule acute1
(declare (salience 4900))
(id-root ?id acute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIkRNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acute.clp 	acute1   "  ?id "  wIkRNa )" crlf))
)

;"acute","Adj","1.wIkRNa"
;Dogs have an acute sense of smell.
;--"2.kuSAgra"
;Her judgment is acute.
;--"3.vikata"
;There is an acute shortage of water in our area.
;
;

;@@@ Added by 14anu07 Karishma Singh MNNIT Allahabad on 14-6-2014
(defrule acute2
(declare (salience 5500))
(id-root ?id acute)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) shortage|deficiency)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyaXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acute.clp 	acute2   "  ?id "  awyaXika )" crlf))
)

;@@@ Added by 14anu07 Karishma Singh MNNIT Allahabad on 14-6-2014
(defrule acute3
(declare (salience 5200))
(id-root ?id acute)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 judgement)
(or(subject-subject_samAnAXikaraNa  ?id1 ?id)(id-word =(+ ?id 1) judgement))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuSAgra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  acute.clp 	acute3   "  ?id "  kuSAgra )" crlf))
)

