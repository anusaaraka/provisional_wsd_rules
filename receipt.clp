;$$$ Modified by 14anu22
(defrule receipt0
(declare (salience 5000))
(id-root ?id receipt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receipt.clp 	receipt0   "  ?id "  pAnA )" crlf))
)

;"receipt","N","1.pAnA"
;He got happy on receipt of the goods.
;$$$ Modified by 14anu22 priya panthi  3/6/2014 priyapanthi@hotmail.com mnnit,allahabad
;He swore he was in St. Louis and had a receipt to prove it.
;वह स्त्. लुइस में था उसने कसम खाई और यह सिद्ध करने के लिए रसीद थी . 
(defrule receipt1
(declare (salience 4900))
(id-root ?id receipt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receipt.clp 	receipt1   "  ?id "  rasIxa )" crlf))
)


;"receipt","VT","1.rasIxa_xenA"
;We got receipts for each thing we bought.

;@@@Added by 14anu24
;If you have the receipt or other proof of purchase , take this with you . 
;अगर आपके पास रसीद या खरीदने का कोई अन्य सबूत है , तो उसे साथ लेते जाईए .
(defrule receipt2
(declare (salience 5100))
(id-root ?id receipt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receipt.clp  receipt2   "  ?id "  rasIxa )" crlf))
)

