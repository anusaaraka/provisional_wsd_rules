
;@@@Added by 14anu-ban-07,(09-04-2015)
;She’s an absolute knockout.(oald)
;वह एक बेहद आकर्षक व्यक्ति है .(manual) 
(defrule knockout1
(declare (salience 1000))
(id-root ?id knockout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 absolute|real)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behaxa_AkarRaka_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knockout.clp 	knockout1   "  ?id "  behaxa_AkarRaka_vyakwi )" crlf))
)

;---------------------------------- Default Rules -----------------------------

;@@@Added by 14anu-ban-07,(09-04-2015)
;The match ended in knockout in the fifth round.(oald)
;मैच पाँचवे दौरा में पछाडने से समाप्त हुआ . (manual)
(defrule knockout0
(id-root ?id knockout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paCAdZanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knockout.clp         knockout0   "  ?id "  paCAdZanA )" crlf))
)

