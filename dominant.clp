;@@@ Added by 14anu-ban-04 (18-03-2015)    
;The dominant feature of the room was the large fireplace.          [cald]
;कमरे की मुख्य विशेषता बड़ी  अंगीठी थी .                   [self]                
(defrule dominant1
(declare (salience 20))
(id-root ?id dominant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 role|feature)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " dominant.clp 	 dominant1   "  ?id " muKya )" crlf))
)


;@@@ Added by 14anu-ban-04 (18-03-2015)  
;The gene for brown eyes is dominant.                   [cald]
;भूरी आँखों के लिए जीन प्रमुख  है .                                 [self]
(defrule dominant2
(declare (salience 20))
(id-root ?id dominant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 gene)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " dominant.clp 	 dominant2   "  ?id " pramuKa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (18-03-2015)    
;A dominant military power.                       [cald]
;एक प्रबल सैनिक शक्ति .                                [self]
(defrule dominant0
(declare (salience 10))
(id-root ?id dominant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prabala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " dominant.clp 	dominant0   "  ?id "  prabala )" crlf))
)
