;@@@Added by 14anu-ban-08 (27-02-2015)
;The beauties of nature. [oald]
;प्रकृति की सुन्दरता.  [self]
(defrule nature0
(declare (salience 0))
(id-root ?id nature)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakqwi))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nature.clp 	nature0   "  ?id "  prakqwi )" crlf))
)

;@@@Added by 14anu-ban-08 (27-02-2015)
;She is very sensitive by nature.   [oald]
;वह स्वभाव से बहुत भावुक हैं.  [self]
(defrule nature1
(declare (salience 470))
(id-root ?id nature)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-by_saMbanXI ?id1 ?id)
(id-root ?id1 sensitive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svaBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nature.clp 	nature1   "  ?id "  svaBAva )" crlf))
)

;@@@Added by 14anu-ban-08 (27-02-2015)
;The changing nature of society.   [oald]
;समाज का स्वरुप बदल रहा हैं.  [self]
(defrule nature2
(declare (salience 472))
(id-root ?id nature)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 society)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svarupa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nature.clp 	nature2   "  ?id "  svarupa  )" crlf))
)

;@@@Added by 14anu-ban-08 (27-02-2015)
;Books of a scientific nature.  [oald]
;वैज्ञानिक प्रकार की किताबें.  [self]
(defrule nature3
(declare (salience 474))
(id-root ?id nature)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 scientific)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nature.clp 	nature3   "  ?id "  prakAra )" crlf))
)
