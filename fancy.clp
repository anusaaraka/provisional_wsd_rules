
;@@@ Added by 14anu-ban-05 on (05-12-2014)
;The prices here will be at least 25 percent less than what the fancy emporia in vijaywada and hyderabad charge .[KARAN SINGLA]
;यहाँ पर कीमतें विजयवाडा एवं हैदराबाद की  आकर्षक बड़ी दुकानें जो मूल्य लगाती हैं , उससे कम से कम 25 प्रतिशत कम होगी .[MANUAL]
(defrule fancy3
(declare (salience 4900))
(id-root ?id fancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fancy.clp 	fancy3   "  ?id "  AkarRaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-12-2014)
;If you are a fancier of eating various varieties of food then understand that you will be made available fabulous tasting sea food.[TOURISM]
;agara Apa viBinna vErAyatI kA KAnA KAne ke SOkIna hEM wo mAna lIjie ki Apako sI PUda lAjavAba svAxa upalabXa karAezge .[TOURISM]
(defrule fancy4
(declare (salience 4900))
(id-root ?id fancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 eat|food|drink|meal|music)	;more constraints can be added	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SOkIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fancy.clp 	fancy4   "  ?id "  SOkIna )" crlf))
)

;----------------------- Default rules ----------------------------
(defrule fancy0
(declare (salience 4500))  ;decreased salience from 5000 to 4500 by 14anu-ban-05 on (05-12-2014)
(id-root ?id fancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asAmAnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fancy.clp 	fancy0   "  ?id "  asAmAnya )" crlf))
)

;"fancy","Adj","1.asAmAnya"
;She has a fancy calculator.
;--"2.baDZe_caDZe"
;The store sells items neatly packed with fancy price tags.
;Rita loves making fancy statements at every oppurtunity.
;
(defrule fancy1
(declare (salience 4900))
(id-root ?id fancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fancy.clp 	fancy1   "  ?id "  kalpanA )" crlf))
)

;"fancy","N","1.kalpanA"
;He is having silly teenage fancies.
;--"2.CotA_sajA_huA_keka"
;She bought fancies on her birthday.
;
(defrule fancy2
(declare (salience 4800))
(id-root ?id fancy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasanxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fancy.clp 	fancy2   "  ?id "  pasanxa_kara )" crlf))
)

;"fancy","VT","1.pasanxa_karanA"
;I fancy colorful stamps.
;--"2.icCA_honA"
;She fancies a strong hot cup of tea after a long working session.
;--"3.socanA"
;In the dark she fancied that she saw a large animal figure move behind the trees.
;He fancies that India will win this match but I find it impossible.
;LEVEL 
;Headword  : fancy
;
;Examples --
;
;"fancy","N","1.pasanxa"
;She has a fancy for hotel food
;use hotela kA KAnA pasanxa hE
;--"2.kalpanA" 
;He is having teenaged fancies
;vaha kiSorAvasWA vAlI kalpanAez karawA hE.
;
;"fancy","V","pasanxa_karanA"
;I fancy colorful dress
;muJe raMgIna poSAka pasanxa hE
;--"2.socanA" <--pasanxa_karanA
;Which horse do you fancy in the next race?
;agale xOda meM kOna sA GodA(jIwane ko)socawe ho?
;
;"fancy","Adj","asAmAnya" <--sajAvatI<--kalpanA para AXAriwa
;I have a fancy calculator
;mere pAsa eka asAmAnya gaNiwra hE
;--"2.baDZe_caDZe"  <--manamAnA_mUlya <--pasanxa kA
;He always gives fancy prices for his goods 
;vaha hameSA baDZA caDZAkara vaswuoM kA xAma xewA hE
;
;
;vyAKyA --
;
;uparyukwa viSeRaNa vAkya 1.meM `fancy calculator' aWavA gaNiwra asAmAnya hone   ke kAraNa `kalpanA' se janI vaswuoM kA BAsa howA hE. 'fancy' kA bIjArWa 'kalpanA' hI lagawA hE. kalpanA se 'pasanxa' kA BAva AyA prawIwa howA hE.
;
;
;anwarnihiwa sUwra ;
;
;kalpanA - kalpanA se AyA - pasanxa_karanA
;            |--baDZA_caDZA
;
;sUwra  : kalpanA`
;
