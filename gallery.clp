
;@@@ Added by 14anu-ban-05 on (25-03-2015)
;Some of his work has been exhibited by local art galleries.	[OALD]
;उसके  कुछ काम  स्थानीय कला चित्रशालाओं के द्वारा प्रदर्शित किया गया है.		[manual]
(defrule gallery1
(declare (salience 101))
(id-root ?id gallery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwraSAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gallery.clp 	gallery1   "  ?id "  ciwraSAlA )" crlf))
)


;------------------------------ Default rules ---------------------------------
;@@@ Added by 14anu-ban-05 on (25-03-2015)
;Relatives of the victim watched from the public gallery as the murder charge was read out in court.[OALD]
;पीडित-व्यक्ति  के रिश्तेदारों ने  दर्शक दीर्घा से देख रहे थे जब हत्या के इल्जाम  अदालत में पढे जा रहे थे.               [manual]
(defrule gallery0
(declare (salience 100))
(id-root ?id gallery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xIrGA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gallery.clp  gallery0   "  ?id "  xIrGA )" crlf))
)

