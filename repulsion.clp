;@@@ Added by 14anu-ban-10 on (07-03-2015)
;Thus, the substance develops a net magnetic moment in direction opposite to that of the applied field and hence repulsion.[ncert corpus]
;isa prakAra paxArWa meM pariNAmI cumbakIya AGUrNa Aropiwa kRewra ke viparIwa xiSA meM vikasiwa howA hE Ora isa kAraNa yaha prawikarRiwa howA hE.[ncert corpus]
(defrule repulsion0
(declare (salience 100))
(id-root ?id repulsion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prawikarRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "   repulsion.clp  	 repulsion0   "  ?id "  prawikarRiwa)" crlf))
)
;@@@ Added by 14anu-ban-10 on (07-03-2015)
;She feels repulsion for him.[ncert corpus]
;वह उसके लिए विरोध अनुभव करती है . [ncert corpus]
(defrule repulsion1
(declare (salience 200))
(id-root ?id repulsion)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "   repulsion.clp  	 repulsion1   "  ?id "  viroXa)" crlf))
)
