;@@@ Added by 14anu-ban-06 (03-02-2015)
;All the family felt that Stephen had been hustled into the engagement by Claire.(OALD)
;सभी परिवार ने महसूस किया कि स्टीवन को क्लेर के द्वारा सगाई के लिए मजबूर किया गया था .(manual) 
(defrule hustle1
(declare (salience 2000))
(id-root ?id hustle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 engagement|marriage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majabUra_kara))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hustle.clp 	hustle1   "  ?id "  majabUra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hustle.clp      hustle1   "  ?id " ko )" crlf)
)
)
;--------------------- Default Rule ----------------------

;@@@ Added by 14anu-ban-06 (03-02-2015)
;After giving his speech, Johnson was hustled out of the hall by bodyguards.(OALD)
;उसके भाषण देने के बाद, जॉन्सन को अङ्गरक्षको के द्वारा हॉल के बाहर धक्का दिया गया था . (manual)
(defrule hustle0
(declare (salience 0))
(id-root ?id hustle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XakkA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hustle.clp 	hustle0   "  ?id "  XakkA_xe )" crlf))
)


;@@@ Added by 14anu-ban-06 (03-02-2015)
;The team showed a lot of determination and hustle.(cambridge)
;दल ने बहुत सारी दृढता और ऊधम दिखाया . (manual)
(defrule hustle2
(declare (salience 0))
(id-root ?id hustle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UXama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hustle.clp 	hustle2   "  ?id "  UXama )" crlf))
)
