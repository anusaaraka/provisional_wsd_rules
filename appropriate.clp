(defrule appropriate0
(declare (salience 0));Salience changed from 5000 to 0 by 14anu-ban-02
(id-root ?id appropriate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appropriate.clp 	appropriate0   "  ?id "  uciwa )" crlf))
)

;"appropriate","Adj","1.uciwa"
;The alliance will be declared at an appropriate time.
;
(defrule appropriate1
(declare (salience 0));Salience changed from 5000 to 0 by 14anu-ban-02
(id-root ?id appropriate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciwa_rUpa_se_apanA_banA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appropriate.clp 	appropriate1   "  ?id "  anuciwa_rUpa_se_apanA_banA_le )" crlf))
)

;"appropriate","VT","1.anuciwa_rUpa_se_apanA_banA_lenA"
;It is alleged that Mr. Kumar appropriated the Institute Funds.
;--"2.alaga_raKanA"
;Rs.20000/- have been appropriated for the Conference.
;


;@@@ Added by 14anu-ban-02 (27-08-2014)
;Children's Park - This park is appropriate for the entertainment of children below 14 years .
;बच्चों  का  पार्क  -  14  वर्ष  से  कम  आयुवाले  बच्चों  के  विनोद  के  लिए  यह  पार्क  उपयुक्त  है  ।
(defrule appropriate2
(declare (salience 5000))
(id-root ?id appropriate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appropriate.clp 	appropriate2   "  ?id "  upayukwa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (26-11-2014)
;Light of different frequencies can be used by putting appropriate colored filter or colored glass in the path of light falling on the emitter C.(Ncert)
;उत्सर्जक C पर पडने वाले प्रकाश के मार्ग में उपयुक्त फिल्टर अथवा रङ्गीन काँच रखकर भिन्न तरङ्गदैर्घ्य के प्रकाश का उपयोग कर सकते हैं.(Ncert)
(defrule appropriate3
(declare (salience 500))
(id-root ?id appropriate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 filter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appropriate.clp 	appropriate3   "  ?id "  upayukwa )" crlf))
)
