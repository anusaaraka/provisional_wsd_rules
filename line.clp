
(defrule line0
(declare (salience 5000))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id lining )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aswara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  line.clp  	line0   "  ?id "  aswara )" crlf))
)

;"lining","N","1.aswara"
;Use a thin lining for the curtains to give them a neat fall.
;
;
(defrule line1
(declare (salience 4900))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 paMkwi_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp	line1  "  ?id "  " ?id1 "  paMkwi_banA  )" crlf))
)

;The children lined up for the prayer.
;baccoM ne prArWanA ke lie paMkwi banAI
(defrule line2
(declare (salience 4800))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ikatTA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp	line2  "  ?id "  " ?id1 "  ikatTA_kara  )" crlf))
)

;The books were lined up neatly in the almirah.
;alamArI meM kiwAbeM sahI warIke se ikatTI kI huIM WIM
;Commented by Nandini
;(defrule line3
;(declare (salience 4700))
;(id-root ?id line)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 up)
;(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by ;Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id paMkwi_banA));Automatically modified ;'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's ;program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " line.clp line3 ;" ?id "  paMkwi_banA )" crlf)) 
;)

(defrule line4
(declare (salience 4600))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 paMkwi_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp	line4  "  ?id "  " ?id1 "  paMkwi_banA  )" crlf))
)

(defrule line5
(declare (salience 4500))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line5   "  ?id "  lAina )" crlf))
)

;"line","N","1.lAina/reKA"
;A horizontal line.
;--"2.sImA-reKA"
;The crossed the line && so he was declared out.
;--"3.surakRA-paMkwiyAz"
;In battle fields there are different lines of defence.
;--"4.paMkwi"
;Stand in a line. There are ten words in this line.
;--"5.vaMSAvalI"
;He comes from a family with a line of sportsmen.
;--"6.dorI/wAra"
;Fishing line; telephone line.
;--"7.patarI"
;Line of train.
;--"8.praNAlI"
;We are not sure which line of action he is going to adopt in this matter.
;--"9.parivahana_kampanI"
;A shipping line.
;--"10.vyavasAya"
;He has chosen business as his line.
;--"11.kRewra"
;He is joining a technical line.
;--"12.viSeRa_xUrasaMcAra_sevA"
;He talked on the hotline.
;
(defrule line6
(declare (salience 4400))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reKA_KIMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line6   "  ?id "  reKA_KIMca )" crlf))
)

;"line","V","1.reKA_KIMcanA"
;Draw a line joining A && B.
;--"2.aswara_lagAnA"
;A woollen blanket gives more warmth when lined up with a cotton sheet.
;

;@@@--- Added by Nandini(29-4-14)
;He has fine lines around his eyes. [OLAD]
;usake AzKoM ke chAroM ora JurriyAz hEM.
(defrule line7
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-around_saMbanXI  ?id ?id1)
(id-root ?id1 eye)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JurrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line7   "  ?id "  JurrI )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;He was convicted of illegally importing weapons across state lines.[OLAD]
;usako avEXa rUpa se rAjya kI sImA ke pAra aswra AyAwa karanA kA aparAXI TaharAyA gayA WA.
(defrule line8
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 state|district|county)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line8   "  ?id "  sImA )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;He is joining a technical line.[hinKoj]
;usane eka wakanIkI- kRewra meM praveSa kiyA hE.
(defrule line9
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 join)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line9   "  ?id "  kRewra )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;He traced the line of her jaw with his finger.[OLAD]
;usane apanI ufgalIyoM se usake jabade kI rupa reKA banAI.
(defrule line10
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 trace)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rupa_reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line10   "  ?id "  rupa_reKA )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;He has chosen business as his line.[OLAD]
;usane uxyoga ko vyavasAya kI waraha cunA hE.
(defrule line11
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id1 ?id)
(id-root ?id1 choose)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line11   "  ?id "  vyavasAya )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;I do not follow your line of reasoning.[OLAD]
;mEM warka viwarka kI ApakI xiSA kA anusaraNa_nahIM karawA .
(defrule line12
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-root ?id2 reasoning|argument|attack|defence|enquiry|inquiry)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line12   "  ?id "  xiSA )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;We are starting a new line in casual clothes.
;hama sahaja vaswroM meM eka nayA uwpAxa Suru_kara rahe hEM.
(defrule line13
(declare (salience 4580))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(viSeRya-in_saMbanXI  ?id ?id2)
(id-root ?id1 start)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line13   "  ?id "  uwpAxa )" crlf))
)

;@@@--- Added by Nandini(29-4-14)
;A woollen blanket gives more warmth when lined up with a cotton sheet . 
;jaba suwI cAxara kA aswara lagA howA hE waba UnI kambala Ora garmI xewA hE .
(defrule line14
(declare (salience 4950))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-with_saMbanXI  ?id ?id2)
(id-root ?id2 sheet)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aswara_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp	line14  "  ?id "  " ?id1 "  aswara_lagA  )" crlf))
)

;@@@ Added by Anita - 24.5.2014 [By mail]
;Fishing with rod and line. 
;मच्छली की बंसी और तार से मच्छली पकड़ना ।
(defrule line16
(declare (salience 5100))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line16   "  ?id "  wAra )" crlf))
)


;@@@ Added by 14anu24
;The young camel should be taught as to how to keep to the line of march .
;कम उम्र के ऊंट को चलने की दिशा में सीधी रेखा में रहना सिखाया जाना चाहिए .
(defrule line15
(declare (salience 5555))        ;Salience increased from 5000 to 5555 by 14anu-ban-08 (13-01-2015)
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 keep)
(kriyA-to_saMbanXI  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-root ?id2 march)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp	line15  "  ?id "  " ?id1 "  reKA  )" crlf))
)

;@@@ Added by 14anu-ban-08 on (05-08-2014)
;The lengths of the line segments representing these vectors are proportional to the magnitude of the vectors.    [NCERT]
;ina saxisoM ko vyakwa karane vAlI reXA KaNdoM kI lambAiyAz saxisoM ke parimANa ke saMAnupAwI hEM.
(defrule line17
(declare (salience 6000))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) segment)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) reKA_KaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " line.clp  line17  "  ?id "  " (+ ?id 1) "  reKA_KaNda  )" crlf))
)

;@@@ Added by 14anu-ban-08 on (22-08-2014)   Adding Relation
;An arrow is marked at the head of this line.     [NCERT]
;isa reKA ke sire para eka wIra kA niSAna lagA xewe hEM .
;Then we draw a line from the head of A parallel to B and another line from the head of B parallel to A to complete a parallelogram OQSP.  [NCERT]
;फिर हम A के शीर्ष से B के समान्तर एक रेखा खीँचते हैं और B के शीर्ष से A के समान्तर एक दूसरी रेखा खीँचकर समान्तर चतुर्भुज OQSP पूरा करते हैं .
(defrule line18
(declare (salience 5100))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id1 ?id)(viSeRya-from_saMbanXI  ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line18   "  ?id "  reKA )" crlf))
)

;@@@ Added by 14anu-ban-08 on (31-08-2014)   
;Gravitational forces caused by the various regions of the shell have components along the line joining the point mass to the center as well as along a direction prependicular to this line.         [NCERT]
;खोल के विभिन्न क्षेत्रों के कारण गुरुत्वीय बलों के, खोल के केन्द्र को बिन्दु द्रव्यमान से मिलाने वाली रेखा के अनुदिश तथा इसके लम्बवत्, दोनों दिशाओं में घटक होते हैं.
(defrule line19
(declare (salience 5200))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line19   "  ?id "  reKA )" crlf))
)

;@@@ Added by 14anu-ban-08 on (31-08-2014)
;A pair of equal and opposite forces with different lines of action is known as a couple.   [NCERT]
;दो बराबर परिमाण के, विपरीत दिशाओं में लगे बलों का जोडा जिनकी क्रिया रेखाएँ एक न हों बलयुग्म कहलाता है. 
(defrule line20
(declare (salience 5300))
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line20   "  ?id "  reKA )" crlf))
)

;$$$Modified by 14anu-ban-08 (17-01-2015)      ;commented constraint
;@@@ Added by 14anu11
;He could never be ' turned aside from his chosen line of defence or attack or be tempted into side issues . 
;बचाव या अभियोजन की उनकी चुनी हुई दिशा से न तो उन्हें विचलित किया जा सकता था और न ही उन्हें गौण प्रश्नों की ओर आकर्षित किया जा सकता था .
(defrule line21
(declare (salience 5500))
(id-root ?id line)
;(id-root ?id1 of)             ;commented by 14anu-ban-08 (17-01-2015)
;(id-root ?id2 defence)        ;commented by 14anu-ban-08 (17-01-2015)
(id-root =(+ ?id 1) of)
(id-root =(+ ?id 2) defence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rkSaA_reKA))
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 rkSaA_reKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp 	line21   "  ?id "  rkSaA_reKA )" crlf))
)

;@@@Added by 14anu18 (03-07-14)
;Whatever line of engineering you choose, work hard on it.
;आप जो भी क्षेत्र कल बनाने की विद्या का चुनते हैं,उसपर कठोर परिश्रम कीजिए .
(defrule line9_2
(declare (salience 5301))           ;Salience is increased from 4580 to 5301 by 14anu-ban-08 (16-01-2015)
(id-root ?id line)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id2)  
(id-root ?id2 business|work|medicine|engineering|interest|study)       
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  line.clp     line9_2   "  ?id "  kRewra )" crlf))
)
