;@@@ Added by 14anu-ban-03 (05-03-2015)
;I'm never going to guess the answer if you don't give me a clue. [oald]
;मैं उत्तर का अन्दाजा कभी नहीं लगा सकता हूँ यदि आप मुझे सूत्र नहीं देते . [manual]
(defrule clue3
(declare (salience 10))
(id-root ?id clue)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object_2 ?id1 ?id)
(id-root ?id1 give)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clue.clp 	clue3   "  ?id "  sUwra )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-03-2015)
;I don't have a clue where she lives. [oald]
;मुझे नहीं पता है वह कहाँ रहती है . [manual]
(defrule clue4
(declare (salience 20))
(id-root ?id clue)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-jo_samAnAXikaraNa ?id ?id1)
(kriyA-aXikaraNavAcI ?id2 ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clue.clp 	clue4   "  ?id "  pawA )" crlf))
)


;----------------------Default Rules---------------------

;@@@ Added by 14anu-ban-03 (04-03-2015)
;The police tried to solve the case with the help of little clue it had. [hinkhoj]
;पुलिस ने कुछ सुराग की सहायता से मामले को हल करने का प्रयास किया  था . [manual]
(defrule clue0
(declare (salience 00))
(id-root ?id clue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clue.clp 	clue0   "  ?id "  surAga )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-03-2015)
;Can you clue me in?  [hinkhoj]
;क्या आप मुझे जानकारी दे सकते हैं?  [manual]
(defrule clue1
(declare (salience 00))
(id-root ?id clue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clue.clp 	clue1   "  ?id "  jAnakArI_xe )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-03-2015)
;Sania is more clued up about the cinema than I am. [oald]
;सान्या चलचित्र के बारे में मुझसे अधिक जानकार है . [manual]
(defrule clue2
(declare (salience 00))
(id-root ?id clue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clue.clp 	clue2   "  ?id "  jAnakAra )" crlf))
)



