;@@@ Added by 14anu-ban-11 on (19-02-2015)
;On my cheek is a smudge of lipstick.(oald)
;मेरे गाल पर लिपस्टिक का दाग है . (self)
(defrule smudge2
(declare (salience 5001))
(id-root ?id smudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 lipstick)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smudge.clp 	smudge2   "  ?id "  xAga )" crlf))
)

;@@@ Added by 14anu-ban-11 on (19-02-2015)
;Tears had smudged her mascara.(oald)
;आँसू ने उसका  मस्कारा फैल दिया था . (anusaaraka)
(defrule smudge3
(declare (salience 4910))
(id-root ?id smudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 mascara)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smudge.clp 	smudge3   "  ?id "  PEla_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (19-02-2015)
;Areas feels like a smudge.(coca)
;क्षेत्र गन्दा जैसा महसूस हो रहा है . (self)
(defrule smudge4
(declare (salience 5002))
(id-root ?id smudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-like_saMbanXI  ?id1 ?id)
(id-root ?id1 feel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMxA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smudge.clp 	smudge4   "  ?id "  gaMxA  )" crlf))
)

;------------------------ Default Rules ----------------------

;"smudge","N","1.XabbA"
;He entered with a smudge on his face.
(defrule smudge0
(declare (salience 5000))
(id-root ?id smudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XabbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smudge.clp 	smudge0   "  ?id "  XabbA )" crlf))
)

;"smudge","V","1.XabbA_dZAlanA"
;You smudged your new t-shirt.
(defrule smudge1
(declare (salience 4900))
(id-root ?id smudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XabbA_dZAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smudge.clp 	smudge1   "  ?id "  XabbA_dZAla )" crlf))
)

;"smudge","V","1.XabbA_dZAlanA"
;You smudged your new t-shirt.
;--"2.ragadZane_para_CUtane_vAlA"
;You have got a smudge of dust on your cheek.
