;@@@ Added by 14anu-ban-06 (09-04-2015)
;Poor farmers have little choice but to try to grow food in this infertile soil.(cambridge)
;गरीब किसानों के पास इस बंजर मिट्टी में आहार उपजाने का प्रयास करने के न के बराबर विकल्प है . (manual)
(defrule infertile1
(declare (salience 2000))
(id-root ?id infertile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 soil)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMjara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infertile.clp 	infertile1   "  ?id "  baMjara )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (09-04-2015)
;An infertile couple. (OALD)
;जनन-अक्षम युगल . (manual)
;It has been estimated that one in eight couples is infertile. (cambridge)
;यह अन्दाजा लगाया गया है कि आठ युगलो में एक जनन-अक्षम है . (manual)
(defrule infertile0
(declare (salience 0))
(id-root ?id infertile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janana-akRama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infertile.clp 	infertile0   "  ?id "  janana-akRama )" crlf))
)
