;@@@ Added by 14anu-ban-06 (04-03-2015)
;He was hauled up in court front of a judge.(cambridge)
;वह अदालत में न्यायधीश के सामने घसीटा गया था . (manual)
(defrule haul2
(declare (salience 5100))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GasItA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " haul.clp	haul2  "  ?id "  " ?id1 "  GasItA_jA  )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-03-2015)
;He was hauled off to jail. (OALD)
;वह कारागार में बलपूर्वक डाला गया था . (manual)
(defrule haul3
(declare (salience 4900))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 jail)
(id-root =(+ ?id 1) off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  balapUrvaka_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " haul.clp	 haul3  "  ?id "  " (+ ?id 1)  " balapUrvaka_dAla  )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-03-2015)
;Fishermen have been complaining of poor hauls all year. (cambridge)
;मछुआरा पूरे वर्ष कम जाल में फँसी हुई मछलियों की संख्या के बारे में शिकायत करते रहे हैं . (manual)
(defrule haul4
(declare (salience 5100))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-of_saMbanXI ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 fisherman)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAla_meM_PazsI_huI_maCaliyoM_kI_saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haul.clp 	haul4   "  ?id "  jAla_meM_PazsI_huI_maCaliyoM_kI_saMKyA )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-03-2015)
;A number of suspects have been hauled in for questioning. (OALD)
;कई सन्दिग्धो को प्रश्न करने के लिए जबरदस्ती बुलाया गया है . (manual)
(defrule haul5
(declare (salience 5200))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 question)
(id-root =(+ ?id 1) in)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  jabaraxaswI_bulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " haul.clp	 haul5  "  ?id "  " (+ ?id 1)  " jabaraxaswI_bulA  )" crlf)
)
)

;------------------------ Default Rules ----------------------

;"haul","N","1.pAyA_huA_mAla"
;The bandits came back with a rich haul
;dAkU awyaXika pAye hue mAla ke sAWa lOte.
(defrule haul0
(declare (salience 5000))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAyA_huA_mAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haul.clp 	haul0   "  ?id "  pAyA_huA_mAla )" crlf))
)

;"haul","V","1.KIMca_nikAlanA{badZI_mAwrA_meM}"
;With great difficulty we could haul our car out of the ravine .
;badZI pareSAnI ke sAWa hama apanI gAdZI ko gadDe se bAhara Xakela pAye.
(defrule haul1
(declare (salience 4900))
(id-root ?id haul)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KIMca_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haul.clp 	haul1   "  ?id "  KIMca_nikAla )" crlf))
)

