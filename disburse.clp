;@@@ Added by 14anu-ban-04 (08-04-2015)
;The funds were to be disbursed in two instalments.                 [oald]
;पूँजी को दो किस्तों में चुकाया जाना था .                                        [self]
(defrule disburse1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disburse)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id cukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  disburse.clp     disburse1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disburse.clp       disburse1  "  ?id "  cukA )" crlf))
)

;@@@ Added by 14anu-ban-04 (08-04-2015)
;The regional council disburses grants to local writers.                [oald]
;क्षेत्रीय परिषद स्थानीय लेखकों को आर्थिक मदद देता है .                                  [self]
(defrule disburse2
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disburse)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disburse.clp       disburse2  "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (08-04-2015)
;His medical bill was disbursed by the company.                 [hinkhoj]
;उसके चिकित्सा बिल का पैसा  कम्पनी के द्वारा दिया गया था .                         [self]
(defrule disburse3
(declare (salience 30)) 
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disburse)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 bill)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id pEsA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  disburse.clp     disburse3   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disburse.clp       disburse3  "  ?id "  pEsA_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (08-04-2015)
;The company disbursed his medical bill.                  [hinkhoj] [;active form of above sentence]
;कम्पनी ने उसके चिकित्सा बिल का पैसा दिया .                           [self]
(defrule disburse4
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disburse)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 bill)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id pEsA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disburse.clp     disburse4   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disburse.clp       disburse4  "  ?id "  pEsA_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (08-04-2015)
;The government has disbursed millions of dollars in foreign aid.         [merriam-webster]
;सरकार ने विदेशी सहायता में  लाखों  डौलर  व्यय किए हैं .                                        [self]
(defrule disburse0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disburse)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disburse.clp       disburse0  "  ?id "  vyaya_kara )" crlf))
)

