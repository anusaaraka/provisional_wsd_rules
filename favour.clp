;@@@ Added by 14anu24
;If they have , then an arbitrator should find in your favour .
;अगर उन्होंने ऐसा किया है , तो किसी मध्यस्थ को आपकी पक्ष में चाहिए.
(defrule favour3
(declare (salience 4900))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  favour.clp 	favour3   "  ?id "  pakRa )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (10-12-2014)
;@@@ Added by 14anu13 on 30-06-14
;This Veda is less in favour with the Hindus than the others .
;यह वेद हिन्दुओं में अन्य वेदों की अपेक्षा कुछ कम लोकप्रिय है .
(defrule favour03
(declare (salience 5100))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI ?id ?)		;removed or by 14anu-ban-05 on (10-12-2014)
(viSeRya-in_saMbanXI ?id1  ?id) ;added ?id1 by 14anu-ban-05 on (10-12-2014)
(id-root ?id1 less|much|more)	;added by 14anu-ban-05 on (10-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lokapriya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  favour.clp 	favour03   "  ?id "  lokapriya )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (10-12-2014)
;@@@ Added by 14anu13 on 30-06-14
;Can You do me a favour?
;क्या आप मेरी सहायता कर सकते हैं?
(defrule favour4
(declare (salience 5100))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?  ?id)
;(viSeRya-det_viSeRaNa ?id  ?)   	;commented by 14anu-ban-05 on (10-12-2014)	
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  favour.clp 	favour4   "  ?id "  sahAyawA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-01-2015)
;You're not asking for a favor but engaging in a business transaction.         [COCA]
;तुम सहायता के लिए नहीं पूछ रहे हो, लेकिन व्यापारिक लेनदेन में संलग्न हो रहे हो.                         [manual]
(defrule favour5
(declare (salience 4950))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  favour.clp  	favour5   "  ?id "  sahAyawA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-01-2015)
;By the nineteenth century, enough evidence had accumulated in favor of atomic hypothesis of matter.          [ncert]
;unnIsavIM SawAbxI waka paxArWa kI paramANvIya parikalpanA ke samarWana meM kAPI sAkRya ekawriwa ho gae We.       [ncert]
(defrule favour6
(declare (salience 4950))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarWana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  favour.clp  	favour6   "  ?id "  samarWana  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-01-2015)
;Experimental observations depict that some transitions are more favored than others.            [ncert]
;prAyogika prekRaNa xarSAwe hEM ki kuCa safkramaNa xUsaroM kI apekRA aXika svIkArya hEM.         [ncert]
(defrule favour7
(declare (salience 4950))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  favour.clp  	favour7   "  ?id "  svIkArya  )" crlf))
)

;--------------------- Default rules ------------------
(defrule favour0
(declare (salience 5000))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id favoured )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id anugraha_prApwa_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  favour.clp  	favour0   "  ?id "  anugraha_prApwa_karanA )" crlf))
)

;"favoured","Adj","1.anugraha prApwa karanA"
;China is the most favoured nation in American trade list.
;
(defrule favour1
(declare (salience 4900))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqpA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  favour.clp 	favour1   "  ?id "  kqpA )" crlf))
)

(defrule favour2
(declare (salience 4800))
(id-root ?id favour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  favour.clp 	favour2   "  ?id "  samarWana_kara )" crlf))
)



;default_sense && category=noun	kqpA	0
;"favour","N","1.kqpA"
;Due to her favour I was able to pass the exam.
; 
;"favour","VT","1.kqpA_xqRti_raKanA"
;He favoured me in the debate.
;
;LEVEL 
;Headword  : favour
;
;Examples --
;
;"favour","N","1.kqpA"
;Ram did him a favour by offering him a job
;nOkarI xekara rAma ne usapara kqpA kI.
;
;"favour","V","1.pakRapAwa_karanA" <--kisI_para_kqpA_karanA
;The examiner should not favour the students
;parIkRaka ko CAwroM ke sAWa pakRapAwa nahIM karanA cAhie
;--"2.pasanxa_karanA" <--pakRa_meM_honA
;I favour your proposal
;mEM wumhAre praswAva ko pasanxa karawA hUz
;
;
;anwarnihiwa sUwra ;
;
;kqpA -viSeRa kqpA karanA (pakRa lenA)- pakRa_meM_honA
;
;sUwra : kqpA-pakRa
;
