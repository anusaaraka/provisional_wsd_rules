
;@@@ Added by 14anu-ban-05 on (07-04-2015)
;She moves with the natural grace of a ballerina.	[OALD]
;वह बैले नर्तकी की प्राकृतिक  मोहकता के साथ चलती है . 		[MANUAL]

(defrule grace2
(declare (salience 5002))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mohakawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace2   "  ?id "  mohakawA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (07-04-2015)
;He conducted himself with grace and dignity throughout the trial.[OALD]
;वह परीक्षण के दौरान शिष्टता और गरिमा के साथ पेश आया.			[MANUAL]

(defrule grace3
(declare (salience 5001))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-root ?id1 conduct)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRtawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace3   "  ?id "  SiRtawA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (07-04-2015)
;They've given me a month's grace to get the money.	[OALD]
;वे पैसा प्राप्त करने के लिए मुझे एक महीने की  रियायत  दे चुके हैं . 		[MANUAL]

(defrule grace4
(declare (salience 5001))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id riyAyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace4   "  ?id "  riyAyawa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (07-04-2015)
;She is one of the finest players ever to have graced the game. 	[OALD]
;वह  खेल की शोभा बढ़ाने वाले  बेहतरीन खिलाड़ियों में से एक है.				[MANUAL]

(defrule grace5
(declare (salience 4901))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoBA_baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace5   "  ?id "  SoBA_baDA )" crlf))
)

;------------------------ Default Rules ----------------------

;"grace","N","1.kqpA"
;By the grace of God I am well.
(defrule grace0
(declare (salience 5000))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqpA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace0   "  ?id "  kqpA )" crlf))
)

;"grace","V","1.suSoBiwa_karanA"
;A beautiful rose plant graces my garden.
(defrule grace1
(declare (salience 4900))
(id-root ?id grace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suSoBiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grace.clp 	grace1   "  ?id "  suSoBiwa_kara )" crlf))
)

