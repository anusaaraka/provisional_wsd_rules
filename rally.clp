;@@@ Added by 14anu-ban-10 on (02-03-2015)
;A mass rally in support of the strike.[oald]
;हड़ताल के समर्थन में एक सामूहिक जनसभा।[self]
(defrule rally2
(declare (salience 5100))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janasaBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally2   "  ?id "  janasaBA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;Rally driving.[oald]
;गाड़ियों की प्रतियोगिता का ड्राइव।[self]
(defrule rally3
(declare (salience 5200))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAdZiyoM_kI_prawiyogiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally3   "  ?id "  gAdZiyoM_kI_prawiyogiwA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;He has organized a public rally.[hinkhoj]
;उसने एक जनता  शक्ति प्रदर्शन के लिये जमाव सङ्गठित किया है . [manual]
(defrule rally4
(declare (salience 5300))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 public)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sakwi_praxarSana_ke_liye_jamAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally4   "  ?id "  Sakwi_praxarSana_ke_liye_jamAva )" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;The pound rallied today against the German mark[hinkhoj]
;पाउन्ड ने जर्मनी से चिह्न के विरुद्ध आज  लाभ किया .  [manual]
(defrule rally5
(declare (salience 5400))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 pound)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  lABa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally5   "  ?id "   lABa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;There is a car rally from Delhi to Bombay via Jaipur.[hinkhoj]
;गाड़ियों की प्रतियोगिता है दिल्ली से  बॉम्बे जयपुर की राह से.[manual]
(defrule rally6
(declare (salience 5500))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id  ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiyogiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally6   "  ?id "  prawiyogiwA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;she got unexpected rally of life after a prolong illness.[hinkhoj]
; उसने एक लम्बे रोग के बाद जीवन का अनपेक्षित  स्वास्थ मे सुधार  प्राप्त किया . [manual]
(defrule rally7
(declare (salience 5600))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 life)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  svAsWa_me_suXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally7   "  ?id "   svAsWa_me_suXAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;That was a great tennis rally!.[oald]
;वह एक बहुत बढ़िया  टैनिस का दौर था.[manual]
(defrule rally8
(declare (salience 5800))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 tennis)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  xOra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally8   "  ?id "   xOra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-03-2015)
;A rally in shares on the stock market.[oald]
;फिर से उछाल आना   शेयर बाज़ार  के शेयर  मे.[manual]
(defrule rally9
(declare (salience 5900))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 share)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pira_se_uCAla_AnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally9   "  ?id " Pira_se_uCAla_AnA)" crlf))
)

;------------------------ Default Rules ----------------------

;"rally","N","1.jamaGata{Sakwi_praxarSana_kA}"
;He has organized a public rally.   
(defrule rally0
(declare (salience 5000))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamaGata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally0   "  ?id "  jamaGata )" crlf))
)

;"rally","VTI","1.ekawriwa_karanA"
;The general rallied his scattered army.
(defrule rally1
(declare (salience 4900))
(id-root ?id rally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rally.clp 	rally1   "  ?id "  ekawriwa_kara )" crlf))
)



;"rally","N","1.jamaGata{Sakwi_praxarSana_kA}"
;He has organized a public rally.   
;--"2.gAdZiyoM_kI_prawiyogiwA"
;There is a car rally from Delhi to Bombay via Jaipur.      
;--"3.svAsWa_me_suXAra"
;she got unexpected rally of life after a prolong illness.  
;
;"rally","VTI","1.ekawriwa_karanA"
;The general rallied his scattered army.
;The party rallied to the support of the Prime Minister at the Boat Club.  
;--"2.lABa_karanA"
;The pound rallied today against the German mark

