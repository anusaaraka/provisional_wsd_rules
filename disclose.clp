;@@@ Added by 14anu-ban-04 (06-02-2015)
;The report discloses that human error was to blame for the accident.           [oald]
;रिपोर्ट स्पष्ट करती है कि दुर्घटना के लिए मानवीय भूल को दोष देना चाहिए .                                [self]
(defrule disclose2
(declare (salience 20))
(id-root ?id disclose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject   ?id ?id1)
(id-root ?id1 report)                                               ;added by 14anu-ban-04 on (17-02-2015)
;(id-root ?id1 ?str)                                                ;commented by 14anu-ban-04 on (17-02-2015)
;(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))           ;commented by 14anu-ban-04 on (17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disclose.clp 	disclose2  "  ?id "  spaRta_kara )" crlf))
)
 
;@@@ Added by 14anu-ban-04 (17-02-2015)
;The company has disclosed profits of over £200 million.                    [oald]
;कम्पनी ने  £ 200 दस-लाख के ऊपर के लाभ का खुलासा किया है .                              [self]
(defrule disclose3
(declare (salience 15))
(id-root ?id disclose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject   ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id KulAsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disclose.clp     disclose3  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disclose.clp 	disclose3  "  ?id "   KulAsA_kara )" crlf))
)

;---------------------- Default Rules ----------------------
;@@@ Added by 14anu-ban-04 (06-02-2015)
;The spokesman refused to disclose details of the takeover to the press.        [oald]               
;प्रवक्ता ने  प्रेस  को अधीनीकरण की जानकारी बताने  से  मना कर दिया.                                  [self]
(defrule disclose1
(declare (salience 10))                           ;salience decreased from '20' to '10'  by 14anu-ban-04 on (17-02-2015)
(id-root ?id disclose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id1)                          ;commented by 14anu-ban-04 on (17-02-2015)
;(id-root ?id1 detail)                              ;commented by 14anu-ban-04 on (17-02-2015)
;(kriyA-to_saMbanXI ?id ?id2)                       ;commented by 14anu-ban-04 on (17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disclose.clp 	disclose1  "  ?id "  baWA )" crlf))
)

