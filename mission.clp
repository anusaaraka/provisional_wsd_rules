;@@@Added by 14anu-ban-08 (12-02-2015)
;The planes were on a bombing mission.   [hindkhoj]
;विमान बमबारी करने के  लक्ष्य पर थे.   [self]
(defrule mission0
(declare (salience 0))
(id-root ?id mission)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mission.clp 	mission0   "  ?id "  lakRya )" crlf))
)

;@@@Added by 14anu-ban-08 (12-02-2015)
;Mission accomplished.  [oald]
;कार्य  पूरा हुआ .  [self]
(defrule mission1
(declare (salience 0))
(id-root ?id mission)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 accomplish)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mission.clp 	mission1   "  ?id "  kArya )" crlf))
)

;@@@Added by 14anu-ban-08 (09-03-2015)
;The mission returned successfully and confidently.  [hindkhoj]
;शिष्टमंडल सफलतापूर्वक और विश्वास के साथ लौटे.  [self]
(defrule mission2
(declare (salience 10))
(id-root ?id mission)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 return)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRtamaMdala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mission.clp 	mission2   "  ?id "  SiRtamaMdala )" crlf))
)
