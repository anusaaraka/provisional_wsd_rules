;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Pramila(BU) 
;Under these assumptions, the trajectory of an alpha-particle can be computed employing Newton's second law of motion and the Coulomb's law for electrostatic force of repulsion between the alpha-particle and the positively charged nucleus. 
;इन अभिधारणाओं के आधार पर ऐल्फा - कण और धनावेशित नाभिक के मध्य स्थिर वैद्युत प्रतिकर्षण बल के कूलॉम - नियम तथा न्यूटन के गति के द्वितीय नियम का उपयोग करके ऐल्फा - कण के प्रक्षेप पथ का अभिकलन किया जा सकता है. 
;ina aBiXAraNAoM ke AXAra para ElPA - kaNa Ora XanAveSiwa nABika ke maXya sWira vExyuwa prawikarRaNa bala ke kUloYma - niyama waWA nyUtana 
;ke gawi ke xviwIya niyama kA upayoga karake ElPA - kaNa ke prakRepa paWa kA aBikalana kiyA jA sakawA hE. 

;He employed the computers to solve the problem.
;उसने समस्या को हल करने के लिए संगणक का उपयोग किया.
;usane samasyA ko hala karane ke lie saMgaNaka kA upayoga kiyA


;You should employ your time better.
;तुम्हे अपने समय का अच्छी तरह उपयोग करना चाहिए.
;wumhe apane samaya kA acCI waraha upayoga karanA cAhie.

;It is necessary to employ secret measures to get one's ends.
;किसी के परिणाम को प्राप्त करने के लिए गुप्त उपायों का उपयोग करना आवश्यक है.
;yaha eka ke siroM ko prApwa karane ke lie gupwa upAyoM kA upayoga karanA AvaSyaka hE.

;It is necessary to employ the hammer to drive the nail.
;कील को लगाने के लिए हथौडे का उपयोग करना आवश्यक है.
;kIla ko lagAne ke lie haWOde ko upayoga karanA AvaSyaka hE.

;$$$ Modified by 14anu-ban-04 (29-11-2014)
;###[COUNTER EXAMLPE]### Once again we employ Ampere's law in the form of Eq. (4.17(a)).        [NCERT-CORPUS]
;###[COUNTER EXAMLPE]### एक बार फिर हम ऐंपियर के नियम का उपयोग [समीकरण [4.17(a)] के रूप में करते हैं.            [NCERT-CORPUS]
;@@@ Added by 14anu24
;If your opponent employs a solicitor , you can not be charged the cost even if you lose your case . 
;अगर आपका विरोध एक सालिसिटर नियुक्त करता है , तो आपके केस हार जाने पर भी आपसे इसकी कीमत वसूल नहीं की जा सकती .
(defrule employ_tmp
(declare (salience 5000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))        ;added by 14anu-ban-04 (29-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp   employ_tmp   "  ?id " niyukwa_kara )" crlf))
)


(defrule employ0
(declare (salience 4900))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-vAkyakarma  ?id ?id1)(kriyA-aXikaraNavAcI  ?id ?id1)(kriyA-kriyA_viSeRaNa  ?id ?id1)(kriyA-kqxanwa_karma  ?id1 ?id)(saMjFA-to_kqxanwa  ?id2 ?id)) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ0   "  ?id " upayoga_kara  )" crlf))
)



;@@@ Added by Pramila(BU) 

;One does not employ hands for all kind of work .
;सभी प्रकार के कार्यो के लिये हाथ का उपयोग नहीं करते हैं.
;saBI prakAra ke kAryo ke liye hAWa kA prayoga nahIM karawe hEM.

;The same can be done over widespread areas by employing the drought weapon . 
;यही बात अनावृष्टि शस्त्र के उपयोग  करने से विस्तृत क्षेत्रों में की जा सकती है | 
;yahI bAwa anAvqRti Saswra ke upayoga karane se viswqwa kRewroM meM kI jA sakawI hE.

;Having determined D we can employ a similar method to determine the size or angular diameter of the planet.    ;ncert
;डी के निर्धारण के पश्चात् हम ग्रह का आमाप अथवा कोणीय व्यास भी निर्धारित करने के लिए इसी विधि का उपयोग कर सकते है .
;Modified by Pramila [condition '(kriyA-kriyArWa_kriyA  ?id ?id1)' added] on 04-03-2014
(defrule employ1
(declare (salience 5000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-for_saMbanXI  ?id ?id1)(kriyA-by_saMbanXI  ?id1 ?id)(kriyA-kriyArWa_kriyA  ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ1   "  ?id " upayoga_kara  )" crlf))
)

;@@@ Added by Pramila(BU) on 04-03-2014
;Our time could be usefully employed in attending to business matters.
;अपना समय व्यापार विषयों को समझने में उपयोग करना चाहिए.
(defrule employ2
(declare (salience 5000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ2   "  ?id " upayoga_kara  )" crlf))
)

;@@@ Added by Pramila(BU) on 04-03-2014
;In doing so, [employ] Newton's third law. 
;ऐसा करने के लिए न्यूटन का तृतीय नियम उपयोग कीजिए.
(defrule employ3
(declare (salience 5000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(kriyA-kriyA_viSeRaNa  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ3   "  ?id " upayoga_kara  )" crlf))
)


;In this stage , the teacher stands at the front of the class , employing books as the sole teaching device , verbally explaining and physically directing lessons . 
;इस अवस्था में , शिक्षक कक्षा के समझ खड़ा होता ( या होती ) है - केवल पुस्तकों को ही शिक्षण सामग्री की तरह उपयोग करते हुए , मौखिक रूप से स्पष्टीकरण देते हुए और शारीरिक रूप से पाठ निदेशित करते हुए |
;(defrule employ13
;(declare (salience 5000))
;(id-root ?id employ)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-as_saMbanXI  ?id ?id1)
;(kriyA-kqxanwa_karma  ?id2 ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id upayoga_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ13   "  ?id " upayoga_kara  )" crlf))
;)
;

;@@@ Added by Pramila(BU) 
;This readjustment requires the help of vocational council , social worker , employment exchange and original employing authority . 
;फिर से इस समंजन के लिए उसे व्यावसायिक परिषद सामाजिक कार्यकर्ता , रोजगार दफ्तर और पहले के नियोक्‍ता दफ्तर की आवश्यकता होती है | 
;Pira se isa samaMjana ke lie use v yAvasAyika pariRaxa sAmAjika kAryakarwA , rojagAra xaPwara Ora pahale kI niyokZwA xaPwara kI 
;AvaS yakawA howI hE |
(defrule employ4
(declare (salience 4500))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id employing|employed)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyokZwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ4   "  ?id " niyokZwA  )" crlf))
)

;@@@ Added by Pramila(BU)
;He has employed the students into the project.
;उसने छात्रों को प्रोजैक्ट में नियुक्त किया.
;usane CAwroM ko projEkta meM niyukwa kiyA.

;He agreed to employ the job applicant.
;वह नौकरी आवेदक को नियुक्त करने के लिये सहमत हुआ.
;vaha nOkarI Avexaka ko niyukwa karane ke liye sahamawa huA.
(defrule employ5
(declare (salience 4800))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-into_saMbanXI  ?id ?id1)(kriyA-kriyArWa_kriyA  ?id1 ?id)(kriyA-as_saMbanXI  ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ5   "  ?id " niyukwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (29-11-2014)
;The spare time was employed in reading.            [same clp file sentence]
;खाली समय पढने में लगाया.
;KAlI samaya paDane meM lagAyA.

(defrule employ6
(declare (salience 4010))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)                  
(id-root ?id1 time)     
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ6   "  ?id " lagA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (29-11-2014)
;###[COUNTER EXAMPLE] ### The principle is also employed in Mass Spectrometer — a device that separates charged particles, usually ions, according to their charge to mass ratio.        [NCERT-CORPUS]
;###[COUNTER EXAMPLE] ### इस सिद्धान्त का उपयोग द्रव्यमान स्पेक्ट्रोमीटर में भी किया जाता है ; यह ऐसी युक्ति है जो आवेशित कणों को, प्रायः आयनों, उनके आवेश-द्रव्यमान अनुपात के अनुसार पृथक करती है.              [NCERT-CORPUS]
;@@@ Added by Pramila(BU)
;She is employed as a teacher.
;उसको अध्यापक के रूप में नियुक्त किया.

;The spare time was employed in reading.            
;खाली समय पढने में लगाया.
;KAlI samaya paDane meM lagAyA.

;She employed several months in learning Swahili.
;उसने स्वाहिली सीखने में कई महीने लगाए.
;usane suhAlI sIKane meM kaI mahIne lagAe.

(defrule employ7
(declare (salience 4950))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kAlavAcI ?id ?id1)                    ;added by 14anu-ban-04 (29-11-2014)
(id-root ?id1 month|year|day|week)      ; added by 14anu-ban-04 (29-11-2014)
;(kriyA-in_saMbanXI  ?id ?id2)               ; commented by 14anu-ban-04  (29-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ7   "  ?id " lagA )" crlf))
)

;@@@ Added by Pramila(BU) on 04-03-2014
;My father employed me into business.
;मेरे पिता ने मुझे व्यापार में लगाया.
;mere piwA ne muJe vyApAra ke XanXe meM lagAyA.
(defrule employ8
(declare (salience 4950))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-word ?id1 business)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ8   "  ?id " lagA )" crlf))
)

;@@@ Added by Pramila(BU) 
;Collecting stamps employs a lot of his time.
;मोहरें इकट्ठे करने में उसका बहुत समय लगता है.
;mohareM ikatTe karanA bahuwa samaya lewI hE.
(defrule employ9
(declare (salience 4900))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(viSeRya-viSeRaNa  ?id1 ?id2)
(AjFArWaka_kriyA  ?id2)
(id-cat_coarse ?id verb)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ9   "  ?id " laga )" crlf))
)

;@@@ Added by 14anu-ban-04 on 22-08-2014
;We employ units for some physical quantities that can be derived from the seven base units (Appendix A 6).   [NCERT-CORPUS]
;हम ऐसी भौतिक राशियों के मात्रकों का भी उपयोग करते हैं जिन्हें सात मूल राशियों से व्युत्पन्न किया जा सकता है (परिशिष्ट A 6).                     [NCERT-CORPUS]
;Having determined D, we can employ a similar method to determine the size or angular diameter of the planet.      [NCERT-CORPUS]
;D के निर्धारण के पश्चात् हम इसी विधि द्वारा ग्रह का आमाप अथवा कोणीय व्यास भी निर्धारित कर सकते हैं.   [NCERT-CORPUS]
;D के निर्धारण के पश्चात्, हम  ग्रह का आमाप अथवा कोणीय व्यास निर्धारित  करने के लिए इसी विधि  का उपयोग कर सकते हैं.      [self (14anu-ban-04)] 

(defrule employ14
(declare (salience 4100))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE))) ;added by 14anu-ban-04 (03-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ14   "  ?id " upayoga_kara  )" crlf))
)



;@@@ Added by Shreya Singhal on 24.09.19
;Today's successful AI systems operate in well-defined domains and employ narrow, specialized knowledge. [AI corpus]
;आज की सफल ए. आई. प्रणाली सुसङ्केत क्षेत्रों और प्रयोग करती सङ्कीर्ण, विशेष तरह के ज्ञान में चलती हैं .
(defrule employ15
(declare (salience 4100))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp employ15   "  ?id " prayoga_kara )" crlf))
)



;---------------------------------------------------------default rules---------------------------------------------------------------------
;@@@ Added by Pramila(BU) 
;How many people has she employed?
;वह कितने लोगो को काम पर लगा चुकी है?
(defrule employ10
(declare (salience 4000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_para_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ10   "  ?id " kAma_para_lagA )" crlf))
)

;@@@ Added by Pramila(BU) 
(defrule employ11
(declare (salience 4000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI_xe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ11   "  ?id " nOkarI_xe )" crlf))
)

;@@@ Added by Pramila(BU) 
(defrule employ12
(declare (salience 4000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ12   "  ?id " nOkarI )" crlf))
)

;@@@ Added by Pramila(BU) 
(defrule employ13
(declare (salience 3000))
(id-root ?id employ)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rojagAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  employ.clp 	employ13   "  ?id " rojagAra )" crlf))
)

