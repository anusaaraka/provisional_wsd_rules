;Added by Sukhada. (8-2-13)
;Ex. What is the probability of tossing a fair coin twice in a row and getting heads both times? 
;She has been voted Best Actress three years in a row.
(defrule toss_in_a_row
(declare (salience 5000))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 toss|vote); $$$ Added vote in the list by Anita 
(id-word =(- ?id 2) in)
(kriyA-in_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 2)  lagAwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " row.clp	toss_in_a_row "  ?id "  " (- ?id 2) " lagAwAra)   )" crlf))
)

;"row","N","1.JagadZA"
;They had a row again.
; उन्होंने फिर से झगड़ा किया ।
;$$$Modified by 14anu18 (18-06-14)
;Added condition kriyA-object
(defrule row0
(declare (salience 3000))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ? ?id)  ;Added by 14anu18 to avoid over-generalisation.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JagadZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row0   "  ?id "  JagadZA_ho)" crlf))
)

;$$$ Modified by 14anu-ban-10 on (13-12-2014) 
;$$$ Modified by 14anu05 on 21.06.14
;The rule didn't work for the already given example.
;@@@ Added by Anita
;He rowed us across the hussain sagar lake.
(defrule row1
(declare (salience 5500))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?id ?id1) ;added by 14anu05 ;commented out by 14anu-ban-10 on (13-12-2014)
(id-root ?id1 lake)   ;Commented by 14anu05 ;uncommented out by  14anu-ban-10 on (13-12-2014)
(kriyA-across_saMbanXI  ?id ?id1) ;Commented by 14anu05 ;uncommented out by  14anu-ban-10 on (13-12-2014) as condition is working fine for given example.
(id-cat_coarse ?id verb)
;(id-word ?id1 boat) ;added by 14anu05 ;commented out by 14anu-ban-10 on (13-12-2014)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id nOkA_meM_le_jA)) ;Commented by 14anu05
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nAva_calA)) ;added by 14anu05
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " row.clp     row1   "  ?id "  " ?id1 "  nAva_calA  )" crlf))
)

;They've gone for a row to the island.
;वे नौका विहार के लिए आईलैंड जा चुके हैं ।
;@@@ Added by Anita-6.12.2013
(defrule row3
(declare (salience 4550))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkA_vihAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row3   "  ?id " nOkA_vihAra )" crlf))
)

;--"2.JagadZanA"
;@@@ Added by Anita-6.12.2013
;She has being rowing again over money with her husband. (old clp sentence)
;वह अपने पति के साथ पैसों के फिर से झगड़ा कर रही है ।
;My parents are always rowing about money. (parser problem)
;मेरे माता-पिता पैसों के लिए हमेशा झगड़ा करते रहते हैं ।
(defrule row4
(declare (salience 4900))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 money)
(or(kriyA-with_saMbanXI  ?id ?)(subject-subject_samAnAXikaraNa  ? ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JagadZa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row4   "  ?id "  JagadZa_kara)" crlf))
)

;@@@ Added by Anita-6.12.2013
;The wind dropped, so we had to row back home. [cambridge dictionary]
;हवा मन्द हो गई थी , इसलिए हमें नौका घर की ओर वापस खेनी पड़ी ।
(defrule row5
(declare (salience 4650))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 back)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkA_vApasa_Ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row5   "  ?id "  nOkA_vApasa_Ke )" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-11-2014)
;The cover cropped cotton fields were planted to clover, which was left to grow in between cotton rows throughout the early cotton growing season (stripcover cropping).[agriculture domain]
;कपास क्षेत्रो की झाडी वाली फसल में तिपतिया घास बोई गई, जिन्हें कपास की पंक्ति के चारों ओर श्रवण कपास के उपजने वाली/बढती/बढने वाले ॠतु में बढने के लिए छोड दिया गया था.[manual]
(defrule row6
(declare (salience 5600))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMkwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row6   "  ?id " paMkwi )" crlf))
)

;@@@ Added by 14anu-ban-10 on (07-02-2015)
;Our heart started jumping with excitement on seeing the herd (row) of deers of 'Impala' species just as we were heading for the exit gate .[tourism corpus]
;हम निकास द्वार की तरफ बढ़ रहे थे कि ’इंपाला’ प्रजाति के हिरणों की डार (कतार) देखकर मन बल्लियों उछलने लगा ।[tourism corpus]
(defrule row7
(declare (salience 5700))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-saMjFA_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kawAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row7   "  ?id " kawAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-02-2015)
;There is a row of houses immediately adjacent to the factory.[oald]
;वहाँ कारखाने के लिए तुरंत सन्निकट घरों की एक कतार है।[self]
(defrule row8
(declare (salience 5800))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 house)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kawAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row8   "  ?id " kawAra )" crlf))
)

;------------------------ Default Rules ------------------

;"row","VT","1.KenA"
;He rowed us across the hussain sagar lake.
(defrule row_default_verb_rule
;(declare (salience 4900))
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row_default_verb_rule   "  ?id "  Ke )" crlf))
)

;--"2.paMkwi"
;$$$ Modified by Anita-6.12.2013
;$$$ Modified Rule 14anu18 (18-06-14)
;decreased salience (14anu18 18-06-14), ;Modified Rule 14anu18 (18-06-14), commented condition.
;Ex.The second row is far away.
;दूसरी पङ्क्ति बहुत दूर है . 
;She has arranged all her books in rows. 
;उसने अपनी सारी पुस्तकों को पँक्ति में रखा ।
;The machine punches a row of holes in the metal sheet.
;मशीन धातु की चद्दर पर छेदों को एक पँक्ति में करती है ।  
(defrule row2
(declare (salience 2000))    ;decreased salience (14anu18 18-06-14)
(id-root ?id row)
?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-in_saMbanXI ? ?id)(viSeRya-of_saMbanXI ?id ?))  
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMkwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  row.clp 	row2   "  ?id " paMkwi )" crlf))
)


;"row","VT","1.KenA"
;He rowed us across the hussain sagar lake.
;--"2.JagadZanA"
;She's being rowing again over money with her husband.
;
