
(defrule smash0
(declare (salience 5000))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id smashing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id uwwama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  smash.clp  	smash0   "  ?id "  uwwama )" crlf))
)

;"smashing","Adj","1.uwwama"
;We had a smashing time on holiday.
;
(defrule smash1
(declare (salience 4900))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id smashed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id piye_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  smash.clp  	smash1   "  ?id "  piye_hue )" crlf))
)

;"smashed","Adj","1.piye_hue{SarAba}"
;Drinking is not bad but smashed up people create problem of law && order.
;
(defrule smash2
(declare (salience 4800))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarAne_kI_Xvani))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash2   "  ?id "  takarAne_kI_Xvani )" crlf))
)

;"smash","N","1.takarAne_kI_Xvani"
;The plate hit the floor with a smash. 
;--"2.takarAva"
;The impact of smash was so great that he crushed many people in the crowd with his car.
;--"3.xurGatanA"
;Due to technical defects car smash took place at work.
;
(defrule smash3
(declare (salience 4700))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZorase_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash3   "  ?id "  jZorase_mAra )" crlf))
)

;"smash","V","1.jZorase_mAranA"
;Street children throwing stones on the road smash the passing vehicles.
;--"2.parAjiwa_karanA/aMwa_karanA"
;World powers today are uniting to smash terrorism.
;


;$$$ Modified by 14anu-ban-01 on (07-01-2015)
;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014**********
;The player hit a smash.
;खिलाड़ी ने समैश मारा.
;खिलाड़ी ने स्मैश मारा.[Translation improved by 14anu-ban-01 on (07-01-2015)]
(defrule smash4
(declare (salience 5000))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 tennis|badminton|play|player|point)
;(test(!= ?id1 0)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smESa ));changed "samESa" to "smESa" by 14anu-ban-01 on (07-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash4   "  ?id "  smESa )" crlf));changed "samESa" to "smESa" by 14anu-ban-01 on (07-01-2015)
)

;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014**********
;The movie was a smash hit.
;फिल्म ज़बर्दस्त हिट रही थी.
(defrule smash5
(declare (salience 5000))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 hit|success)
(test (= (+ ?id 1) ?id1)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZabarxaswa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash5   "  ?id "  jZabarxaswa )" crlf))
)

;;@@@ Added by 14anu-ban-11 on (16-03-2015)
;Due to technical defects car smash took place at work. (hinkhoj)
;तकनीकी- दोषों के कारण गाडी दुर्घटना  ने स्थान ले लिया . (self)(confused in translation)
(defrule smash6
(declare (salience 4801))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 car)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurGatanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash6   "  ?id "  xurGatanA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (16-03-2015)
;The impact of smash was so great that he crushed many people in the crowd with his car. (hinkhoj)
;टक्कर का प्रभाव इतना ज़बर्दस्त था कि उसने अपनी गाडी से भीड में बहुत सारे लोगों को मसल दिया . (self)
(defrule smash7
(declare (salience 4802))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 impact)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash7   "  ?id "  takkara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (16-03-2015)
;She has smashed the world record.(oald)
;उसने विश्व रिकार्ड तोडा दिया है . (self)
(defrule smash8
(declare (salience 4701))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 record)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wodZA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash8  "  ?id "  wodZA_xe)" crlf))
)



;@@@ Added by 14anu-ban-11 on (16-03-2015)
;The glass bowl smashed into a thousand pieces. (oald)
;काँच की कटोरी हजार टुकडों में टूट गयी . (self)
(defrule smash9
(declare (salience 4702))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 piece)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tUta_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash9   "  ?id "  tUta_jA)" crlf))
)



;@@@ Added by 14anu-ban-11 on (16-03-2015)
;The car smashed into a tree.(oald)
;गाडी पेड से टकरा गई . (self)
(defrule smash10
(declare (salience 4703))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 tree)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash10   "  ?id "  takarA_jA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (16-03-2015)
;He smashed the ball into the goal. (oald)
;उसने गोल में गेंद फेंकी .(self) 
(defrule smash11
(declare (salience 4704))
(id-root ?id smash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 goal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smash.clp 	smash11  "  ?id "  PeMka)" crlf))
)



