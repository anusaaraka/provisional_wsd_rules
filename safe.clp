;@@@ Added by 14anu-ban-11 on (11-02-2015)
;Its safe to assume that there will always be a demand for new software.(hinkhoj)
;यह मानना  विश्वसनीय है कि वहाँ पर नये सॉफ्टवेयर के लिए  हमेशा माँग रहेगी . (self)
;Note:- working properly on parser no. 10.
(defrule safe1
(declare (salience 10))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-anaBihiwa_subject  ?id1 ?id)
(id-root ?id1 assume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSvasanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe1   "  ?id " viSvasanIya)" crlf))
)

;@@@ Added by 14anu-ban-11 on (12-02-2015)
;The missing child was found safe and sound.(hinkhoj)
;लापता बच्चा सकुशल पाया गया था . (anusaaraka)
(defrule safe3
(declare (salience 50))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-subject  ?id ?)
(conjunction-components  ?id1 ?id ?id2)
(id-root ?id1 and)
(id-root ?id2 sound)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 sakuSala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir*  "   safe.clp  safe3  "  ?id "  " ?id1 " " ?id2 "   sakuSala )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;Nobody is safe from suspicion at the moment.[oald]
;कोई भी इस समय शक से  अहानिकर/निरापद नहीं है . [self]
(defrule safe4
(declare (salience 50))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 suspicion|doubt)
(viSeRya-from_saMbanXI  ?id ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ahAnikara/nirApaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe4   "  ?id " ahAnikara/nirApaxa)" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;Is the water here safe to drink?[oald]
;क्या यहाँ का पानी पीने (के योग्य/लिए उपयुक्त) है?  [self]                  
(defrule safe5
(declare (salience 50))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 water|milk)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yogya/upayukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe5   "  ?id " yogya/upayukwa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;He is a safe driver.[oald]
;वह एक सतर्क/होशियार चालक है.   [self]  
(defrule safe6
(declare (salience 50))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sawarka/hoSiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe6   "  ?id " sawarka/hoSiyAra)" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;It is a safe verdict.[oald]
;यह एक कुशल/अखंडनीय निर्णय है.[self]
(defrule safe7
(declare (salience 50))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 verdict|decision|answer|conclusion|judgement|adjudication)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aKaNdanIya/kuSala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe7   "  ?id " aKaNdanIya/kuSala)" crlf))
)
;--------------------------- Default Rules -------------------

;@@@ Added by 14anu-ban-11 on (11-02-2015)
;Here's your passport Now keep it safe.(oald)
;यहां है आपका पासपोर्ट अब इसको सुरक्षित रखना . 
(defrule safe0
(declare (salience 00))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surakRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp     safe0   "  ?id " surakRiwa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (11-02-2015)
;All have cable TV, telephone, safe, bathrobes, slippers and hairdryer. (oald)
;सभी में केबल दूरदर्शन, टेलीफोन, तिजोरी, बाथरोब, हवाई चप्पल और hairdryer हैं . (anusaaraka)
;;Note:- working properly on parser no.3.
(defrule safe2
(declare (salience 00))
(id-root ?id safe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wijorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  safe.clp 	safe2   "  ?id " wijorI )" crlf))
)

