;@@@ Added by Anita--23-06-2014
;Note that in this type of representation the magnitudes are not considered. [ncert]
;ध्यान दीजिए, इस प्रकार के प्रस्तुतीकरण में परिमाणों पर विचार नहीं किया जाता ।
(defrule representation0
(declare (salience 5000))
(id-root ?id representation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 type)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwikaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  representation.clp 	representation0   "  ?id "  praswuwikaraNa )" crlf))
)


;@@@ Added by Anita--23-06-2014
;This statue is a representation of Hercules.[cambridge dictionary]
(defrule representation1
(declare (salience 4900))
(id-root ?id representation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 statue)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  representation.clp 	representation1   "  ?id "  mUrwi )" crlf))
)


;#####################################default-rule####################################
;@@@ Added by Anita--23-06-2014
;Can he afford legal representation? [cambridge dictionary]
;क्या वह कानूनी प्रतिनिधित्व कर सकता है ?
;He gave a talk on the representation of women in 19th-century art. [cambridge dictionary]
;उसने १९ शताब्दी की कला में स्त्रियों के प्रतिनिधित्व पर भाषण दिया ।
(defrule representation_default-rule
(declare (salience 0))
(id-root ?id representation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiniXiwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  representation.clp 	representation_default-rule   "  ?id "  prawiniXiwva )" crlf))
)
