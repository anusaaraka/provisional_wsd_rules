;@@@Added by 14anu-ban-02(16-03-2015)
;Tonight,they will banter for hours.[coca]
;आज की रात को, वे घण्टों से मजाक करेंगे . [self]
(defrule banter2
(declare (salience 100))
(id-root ?id banter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id majZAka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  banter.clp  	banter2   "  ?id "  majZAka_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$Modified by 14anu-ban-02(16-03-2015)
;He bantered with reporters and posed for photographers.[oald]
;उसने संवाददाता के साथ दिल्लगी की और फोटोग्राफरों के लिए मुद्रा बनाई . [self]
;"bantering","V","1.xillagI_karanA"
;We saw officers && subordinates bantering with each other.
(defrule banter0
(declare (salience 0))	;reduce to 0 from 5000 by 14anu-ban-02(16-03-2015)
(id-root ?id banter)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id bantering )	;commented by 14anu-ban-02(16-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xillagI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  banter.clp  	banter0   "  ?id "  xillagI_kara )" crlf))
)

;"banter","N","1.hAsya/upahAsa"
;We saw the traditional banter between officers && subordinates on Labour Day.
(defrule banter1
(declare (salience 4900))
(id-root ?id banter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAsya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  banter.clp 	banter1   "  ?id "  hAsya )" crlf))
)

