;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;Which team do you support?
;आप कौनसी टीम को प्रोत्साहित करते हैं?
(defrule support2
(declare (salience 4400))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 team|player|club)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prowsAhiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))               ;added by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  support.clp   support2   "  ?id " ko )" crlf)   ;added by 14anu-ban-01 on (12-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support2   "  ?id "  prowsAhiwa_kara )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;This digital audio player supports multiple formats.
;यह डिजिटल श्राव्य प्लेअर बहुत फॉर्मेट चलाने में समर्थ है . 
;यह डिजिटल श्राव्य प्लेअर बहुत से फॉर्मेट चलाता है.[Translation improved by 14anu-ban-01 on (12-01-2015)]
(defrule support3
(declare (salience 4400))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 format|extension)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))         ;changed "calAne_meM_samarWa" to "calA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support3   "  ?id "  calA )" crlf))                               
)


;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 02.07.14
;Ram has to support his family after his father's death.
;राम को उसके पिता की मृत्यु के बाद उसके परिवार का भरण-पोषण करना है .
;राम को ही उसके पिता की मृत्यु के बाद उसके परिवार का भरण-पोषण करना है .[Translation improved by 14anu-ban-01 on (12-01-2015)]
(defrule support4
(declare (salience 4400))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 brother|family|friend|mother|sister)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaraNa-poRaNa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))    ;added by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support4   "  ?id "  BaraNa-poRaNa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  support.clp   support4   "  ?id " kA )" crlf))
)

;@@@ Added by 14anu21 on 27.06.2014
;The temple is supported on pillars.
;मन्दिर खम्भों पर सहारा दिया हुआ है .(Translation before adding rule)
;मन्दिर खम्भों पर टिका हुआ है .
;The temple is supported by pillars.
; मन्दिर खम्भों से सहारा दिया गया है .(Translation before adding rule)
;मन्दिर खम्भों से टिका हुआ है .
;The temple is supported by columns.
; मन्दिर स्तम्भों से सहारा दिया गया है . (Translation before adding rule)
;मन्दिर स्तम्भों से टिका हुआ है. 
(defrule support5
(declare (salience 5000))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-by_saMbanXI  ?id ?id1)(kriyA-on_saMbanXI  ?id ?id1))
(kriyA-subject  ?id ?id2)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
(not(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support5   "  ?id "  tikA )" crlf)
)
)


;@@@ Added by  14anu-ban-01 on (05-02-2015)
;She was stunned by the amount of support she received from well-wishers.[cald]
;शुभचिंतकों से मिले प्रोत्साहन से वह  चकित हुई/भौंचक्की रह गयी .[self] 
(defrule support6
(declare (salience 2000)) 
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 amount)
(kriyA-by_saMbanXI  ? ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id prowsAhana))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id1 1) ?id1 prowsAhana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " support.clp 	support6   "  ?id "  "(+ ?id1 1) "    " ?id1 "  prowsAhana )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support6   "  ?id "  prowsAhana )" crlf))
)

;@@@ Added by  14anu-ban-01 on (09-04-2015)
;It will support a smooth transition for students with severe disabilities.[self:with reference to coca]
;यह कष्टमय विकलाङ्गता वाले विद्यार्थियों के सरल पारगमन में सहायक होगा . [self]
(defrule support7
(declare (salience 4000))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 transition|operation)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyaka_ho))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support7   "  ?id "  sahAyaka_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  support.clp   support7   "  ?id " meM )" crlf)
)
)

;----------------------- Default Rules --------------
;The rule fired for : This digital audio player supports multiple formats.
;Anusaaraka : यह अङ्कीय श्राव्य प्लेअर बहुत रूपाकार को सहारा देता है .
;Man : यह डिजिटल श्राव्य प्लेअर बहुत फॉर्मेट चलाने में समर्थ है .
;"support","N","1.sahArA"
;--"2.AXAra/sahArA"
;He was their only means of support.
(defrule support0
(declare (salience 2000)) ;Salience reduced by 14anu05 as it only gives default meaning. It acts like default rule.
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support0   "  ?id "  sahArA )" crlf))
)


;"support","V","1.sahArA_xenA"
;He supported his friend in his difficulties.
(defrule support1
(declare (salience 3900))
(id-root ?id support)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahArA_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  support.clp 	support1   "  ?id "  sahArA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  support.clp   support1   "  ?id " ko )" crlf)
)
)

;"support","V","1.sahArA_xenA"
;He supported his friend in his difficulties.
;--"2.BaraNa_poRaNa_karanA"
;He supported his family.
;

