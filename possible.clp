;$$$ Modified by 14anu-ban-09 on (27-11-2014)
;NOTE-Changed meaning from 'SAyaxa_samBava' to 'SAyaxa'
;Added by sheetal(11-03-10)
;Grace may not be possible to fix the problem .
;शायद दया किसी समस्या को सुलझाना नहीं हो सकता. [Self] ;translation by 14anu-ban-09 on (27-11-2014)

(defrule possible0
(declare (salience 5000))
(id-root ?id possible) ;removed 'able' by 14anu-ban-09 on (27-11-2014)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id possible) ;commented by 14anu-ban-09 on (27-11-2014)
;(id-root =(+ ?id 2) fix) ;commented by 14anu-ban-09 on (27-11-2014)
(saMjFA-to_kqxanwa  ?id ?id1) ;added by 14anu-ban-09 on (27-11-2014)
(id-root ?id1 fix) ;added by 14anu-ban-09 on (27-11-2014)
;(id-word ?id1 may) ;commented by 14anu-ban-09 on (27-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAyaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  possible.clp      possible0   "  ?id "  SAyaxa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-04-2015)
;I asked him in the nicest possible way to put his cigarette out.  [oald]
;मैंने उसको सिगरेट बुझाने को अच्छे तरीके में कहा .                                 [manual]
(defrule possible3
(declare (salience 1001))
(id-root ?id possible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id2 ?id)
(id-root ?id2 way)
(viSeRya-viSeRaNa  ?id2 ?id1)
(id-root ?id1 nice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  possible.clp      possible3   "  ?id "   -)" crlf))
)

;-------------------- Default rules --------------------
;@@@ Added by 14anu01 on 24-06-2014
;Possible uses of nuclear power
;परमाणु ऊर्जा के संभव उपयोगों
(defrule possible1
(declare (salience 1000))
(id-root ?id possible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  possible.clp      possible1   "  ?id "   samBava)" crlf))
)

;@@@ Added by 14anu01 on 24-06-2014
;I have marked five possibles with an asterisk.
(defrule possible2
(declare (salience 1000))
(id-root ?id possible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAvya_prawyASI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  possible.clp      possible2   "  ?id "   samBAvya_prawyASI)" crlf))
)
