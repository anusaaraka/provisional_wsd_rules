
;@@@ Added by Anita--28.5.2014
;Finishing early was a welcome change.
;जल्दी समाप्त करना एक सुखद परिर्तन था ।
(defrule welcome3
(declare (salience 5100))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 change)
(or(viSeRya-viSeRaNa  ?id1 ?id)(viSeRya-viSeRaka  ?id ?))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome3   "  ?id "  suKaxa )" crlf))
)

;@@@ Added by Anita--22.7.2014
;The chuckle was as welcome as it was unexpected. [By Mail]
;दबी हुई हँसी सुखद थी जितनी कि अनपेक्षित थी ।
(defrule welcome4
(declare (salience 5200))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome4   "  ?id "  suKaxa )" crlf))
)


;"welcome","VT","1.svAgawa_karanA"
;The Principal welcomed the gathering.
;--"2.XanyavAxa_sahiwa_lenA_[kisI_vaswu_ko]"
;Gifts are always welcomed by children.
;--"3.viSeRa_rUpa_se_praBAviwa_honA"
;The manager welcomed the suggestion warmly. .
;

;@@@ Added by 14anu-ban-11 on (30-03-2015)
;Gifts are always welcomed by children.(hinkhoj)
;उपहार हमेशा बच्चों के द्वारा धन्यवाद सहित लिए जाते हैं . (self)
(defrule welcome5
(declare (salience 4801))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XanyavAxa_sahiwa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome5   "  ?id "  XanyavAxa_sahiwa_le )" crlf))
)

;------------------------------------ Default Rules -------------------------------
;"welcome","Adj","1.svAgawa"
;The chief guest gave a welcome address.
;You are welcome to use my lab.
(defrule welcome0
(declare (salience 5000))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome0   "  ?id "  svAgawa )" crlf))
)

;"welcome","N","1.svAgawa"
;We were touched by the warmth of their welcome.
(defrule welcome1
(declare (salience 4900))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome1   "  ?id "  svAgawa )" crlf))
)

(defrule welcome2
(declare (salience 4800))
(id-root ?id welcome)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  welcome.clp 	welcome2   "  ?id "  svAgawa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  welcome.clp   welcome2   "  ?id " kA )" crlf))
)
