;@@@ Added by 14anu-ban-06 (04-12-2014)
;Kaul has implanted the device in more than 200 patients .(parallel corpus)
;कौल ने यह उपकरण 200 से ज्यादा रोगियों में लगाया है .(parallel corpus)
;At present in place of the severed breast fake breast can be implanted through surgery .(parallel corpus)
;आजकल कटे स्तन की जगह नकली स्तन सर्जरी द्वारा लगाया जा सकता है ।(parallel corpus)
(defrule implant0
(declare (salience 0))
(id-root ?id implant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implant.clp 	implant0   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-12-2014)
;Prejudices can easily become implanted in the mind.(OALD)
;पक्षपात मन में  आसानी से विकसित हो जाता हैं . (manual)
(defrule implant1
(declare (salience 2000))
(id-root ?id implant)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 prejudice|idea|attitude)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implant.clp 	implant1   "  ?id "  vikasiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-12-2014)
;During operation lens is implanted at front of the iris , the frontal part of the eye .(parallel corpus)
;ऑपरेशन के दौरान आँख के अगले भाग , आइरिस के आगे लैन्स का प्रत्यारोपण किया जाता है ।(parallel corpus)
(defrule implant2
(declare (salience 2100))
(id-root ?id implant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 lens)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyAropiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implant.clp 	implant2   "  ?id "  prawyAropiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-12-2014)
;It is also used to implant ions into solids and modify their properties or even synthesise new materials.(NCERT)
;इसका उपयोग ठोसों में आयनों को रोपित करके उनके गुणों में सुधार करने और यहाँ तक कि नए पदार्थों को संश्लेषित करने में भी किया जाता है.
;(NCERT)
(defrule implant3
(declare (salience 2200))
(id-root ?id implant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ropiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implant.clp 	implant3   "  ?id "  ropiwa_kara )" crlf))
)

