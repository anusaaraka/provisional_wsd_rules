;$$$Modified by 14anu-ban-02(15-04-2016)
;Sentence: Both defendants were acquitted.              [oald]
;Translation:दोनों प्रत्यर्थी रिहा किए गये थे .                     [self]
;@@@ Added by 14anu-ban-04 (23-02-2015)
;He is a defendant in the property case.             [hinkhoj] 
;वह सम्पत्ति मामले में प्रतिवादी है .                               [self]
(defrule defendant1 
(declare (salience 20)) 
(id-root ?id defendant) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
;(viSeRya-in_saMbanXI ?id ?id1)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
;(id-root ?id1 case)
;(id-root ?id2 property)
(kriyA-subject  ?id1 ?id)	;Added by 14anu-ban-02(15-04-2016)
(kriyA-karma  ?id1 ?id)		;Added by 14anu-ban-02(15-04-2016)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prawyarWI)) 	;meaning changed from 'prawivAxI' to 'prawyarWI' by 14anu-ban-02(15-04-2016)
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  defendant.clp  defendant1  "  ?id "   prawyarWI )" crlf)) 
) 

;Modified by 14anu-ban-02(15-04-2016)
;Who is acting for the defendant?[sd_verified]
;prawivAxI ke lie kOna prawiniXiwva_kara rahA hE?
;Who is acting on behalf of the defendant? [sd_verified]
;prawivAxI kI ora se kOna prawiniXiwva_kara rahA hE?[sd_verified]
;Added by 14anu-ban-04 (23-02-2015)
;Sentence: Both defendants were acquitted.              [oald]
;Translation:दोनों मुलज़िम रिहा किए गये थे .                     [self]
(defrule defendant0 
(declare (salience 10)) 
(id-root ?id defendant) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prawivAxI))	;meaning changed from 'mulajiZma' to 'prawivAxI' by 14anu-ban-02(15-04-2016)
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  defendant.clp  defendant0  "  ?id "  prawivAxI )" crlf)) 
) 
