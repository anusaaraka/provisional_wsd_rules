;@@@ Added by Manasa (26-8-15)Arsha sodha Sansthan.
;He was a devoted leader.
;vaha eka samarpiwa nAyaka WA.
(defrule devoted1
(declare (salience 4900))
(id-root ?id devoted)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 teacher|doctor|manager|engineer|officer|leader|member|father|brother|husband)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarpiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devoted.clp   devoted1   "  ?id "  samarpiwa )" crlf))
)

;--------------------------------------default rule----------------
;@@@ Added by Manasa (26-8-15)Arsha sodha Sansthan.
;He was a devoted servant.
;waha vaPAxAra nOkara WA.
(defrule devoted0
(declare (salience 4900))
(id-root ?id devote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaPAxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devoted.clp   devoted0   "  ?id "  vaPAxAra )" crlf))
)


