
(defrule poorly0
(declare (salience 5000))
(id-root ?id poorly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asvasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poorly.clp 	poorly0   "  ?id "  asvasWa )" crlf))
)

;"poorly","Adj","1.asvasWa"
;He used to be a very poorly student while studying in a school.
;
(defrule poorly1
(declare (salience 4900))
(id-root ?id poorly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lastamapastama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poorly.clp 	poorly1   "  ?id "  lastamapastama )" crlf))
)

;@@@ Added by 14anu-ban-09 on 5-8-14
;But the earth does receive heat from the sun across a huge distance and we quickly feel the warmth of the fire nearby even though air conducts poorly and before convection can set in. [NCERT CORPUS]
;paranwu viSAla xUrI hone para BI pqWvI sUrya se URmA prApwa kara lewI hE, waWA hama pAsa kI Aga kI uRNawA SIGra hI anuBava kara lewe hEM, yaxyapi vAyu alpa cAlaka hE waWA iwane kama samaya meM saMvahana XArAez BI sWApiwa nahIM ho pAwIM.

(defrule poorly2
(declare (salience 5000))
(id-root ?id poorly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 conduct)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poorly.clp 	poorly2   "  ?id "  alpa )" crlf))
)

;@@@ Added by 14anu-ban-09 on 5-8-14
;Molecules in gases are very poorly coupled to their neighbors.[NCERT CORPUS]
;gEsoM ke aNu apane pAsa ke aNuoM se bahuwa halake se yugmiwa howe hEM.

(defrule poorly3
(declare (salience 5000))
(id-root ?id poorly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id1 couple)
(id-root ?id2 molecule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_halke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poorly.clp 	poorly3   "  ?id "  bahuwa_halke )" crlf))
)

;"poorly","Adv","1.lastamapastama"
