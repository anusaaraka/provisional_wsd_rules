
(defrule one0
(declare (salience 5000))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) must)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hara_eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one0   "  ?id "  hara_eka )" crlf))
)

;One must go there at least once.
;Added by human

(defrule one1
(declare (salience 4900))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id ones )
(id-cat_coarse =(- ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAlA))
(assert (id-wsd_number ?id p))  ;Modified number by Roja (09-11-10)The little ones are hopping and jumping merrily.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one1   "  ?id "  vAlA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  one.clp       one1   "  ?id " p )"crlf))
)

(defrule one2
(declare (salience 4800))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id ones )
(id-word =(- ?id 1) which)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAlA))
(assert (id-wsd_number ?id s))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one2   "  ?id "  vAlA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  one.clp       one2   "  ?id "  s )" crlf))
)

(defrule one22
(declare (salience 4980))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) which)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAlA))
(assert (id-wsd_number ?id s))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp    one22   "  ?id "  vAlA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  one.clp    one22   "  ?id " s )" crlf))
)

;
;Added by human
(defrule one3
(declare (salience 4700))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) adjective)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one3   "  ?id "  eka )" crlf))
)

;At last one of them ... last is marked as an adj. 
;To handle this case, above rule is added.
(defrule one4
(declare (salience 4600))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one4   "  ?id "  vAlA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (30-08-2014)
;Changed meaning from "hameM" to "koI"
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;In these ashrams one has a sense of homecoming. [Gyannidhi]
;इन आश्रमों में एक अपने पन का अनुभव होता हैं।
;ena ASramoM meM koI BI gara jEsA anuBava karawA hE. [Own Manual] ;Translation by 14anu-ban-09
(defrule one16
(declare (salience 4550))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
(id-cat_coarse ?id1 verb)
(id-root ?id2 sense|point|idea|reason|view|feeling|meaning|purpose|opinion|sensation|implication)
(kriyA-object  ?id1 ?id2)
(kriyA-subject  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI)) ;[hameM added by 14anu3] ;modified hameM as koI by 14anu-ban-09
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one16   "  ?id "  eka )" crlf))
)


(defrule one5
(declare (salience 4500))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one5   "  ?id "  koI_BI )" crlf))
)

; Order of the previous two rules is important
(defrule one6
(declare (salience 4400))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) twenty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikkIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one6   "  ?id "  ikkIsa )" crlf))
)

(defrule one7
(declare (salience 4300))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) thirty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikwIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one7   "  ?id "  ikwIsa )" crlf))
)

(defrule one8
(declare (salience 4200))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) forty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikwAlIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one8   "  ?id "  ikwAlIsa )" crlf))
)

(defrule one9
(declare (salience 4100))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) fifty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikAvana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one9   "  ?id "  ikAvana )" crlf))
)

(defrule one10
(declare (salience 4000))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) sixty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iksaTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one10   "  ?id "  iksaTa )" crlf))
)

(defrule one11
(declare (salience 3900))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) seventy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikhawara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one11   "  ?id "  ikhawara )" crlf))
)

(defrule one12
(declare (salience 3800))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) eighty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikAsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one12   "  ?id "  ikAsI )" crlf))
)

(defrule one13
(declare (salience 3700))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) ninety)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikkAnnava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one13   "  ?id "  ikkAnnava )" crlf))
)

;I liked the red one; The blue one is good.
(defrule one14
(declare (salience 3600))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id avy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one14   "  ?id "  eka )" crlf))
)

(defrule one15
(declare (salience 3500))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one15   "  ?id "  eka )" crlf))
)

;@@@ Added by 14anu01 on 21-06-2014
;sadly, it is a short one.
; दुर्भाग्यवश, यह छोटा है . 
(defrule one17
(declare (salience 6700)) ;salience increased '5000' to '6700' by 14anu-ban-09 on (22-01-2015)
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) short|long|another)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one17   "  ?id "  - )" crlf))
)
;@@@ Added by 14anu-ban-09 on (25-09-2014)
;The artist & conservationist says he visits each one twice every year - in the spring to prune and in late summer to graft - for three years, until the tree is established.[Anusaaraka_4th_september]
;kalAkAra Ora saMrakRaNavAxI kahawe hE ki vaha hara eka prawyeka sAla meM wIna sAloM ke lie kaTora pariSrama karege jaba waka ki peda nIMva dAlawe hE. [Own Manual]

(defrule one18
(declare (salience 3500))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 visit)
(id-word =(- ?id 1) each)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) hara_eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " one.clp  one18  "  ?id "  " (- ?id 1) "  hara_eka  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-10-2014)
;Indian government introduced a new one rupee note. 	[Anusaaraka-Agama observation]	;added by 14anu-ban-09 on (16-03-2015)
;भारतीय सरकार ने एक रुपय का नोट जारी किया . 			[self]	;added by 14anu-ban-09 on (16-03-2015)
;A lot has changed in Delhi in the last one year.	[Report-Set 7]	;added by 14anu-ban-09 on (12-03-2015)
;आखिरी एक वर्ष में दिल्ली में बहुत बदलाव आया है . 			[manual]	;added by 14anu-ban-09 on (12-03-2015)
;In our study, however, we mostly deal with the simpler and special case of rotation in which one line (i.e. the axis) is fixed. [NCERT CORPUS] ;Added by 14anu-ban-09 on (05-11-2014)
;waWApi, apane aXyayana meM, aXikAMSawaH, hama EsI sarala evaM viSiRta GUrNana gawiyoM waka sImiwa raheMge jinameM eka reKA (yAni akRa) sWira rahawI hE. [NCERT CORPUS] ;Added by 14anu-ban-09 on (05-11-2014)
;A can be expressed as a sum of two vectors — one obtained by multiplying a by a real number and the other obtained by multiplying b by another real number.  [NCERT CORPUS]
;@A ko xo saxiSoM ke yoga ke rUpa meM viyojiwa kiyA jA sakawA hE ;eka saxiSa @a ke kisI vAswavika safKyA ke guNanaPala ke rUpa meM Ora isI prakAra xUsarA saxiSa @b ke guNanaPala ke rUpa meM hE . [NCERT CORPUS]

(defrule one19
(declare (salience 5000))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 vector|line|year|note) ;Added 'line' by 14anu-ban-09 on (05-11-2014)	;Added 'year' by 14anu-ban-09 on (12-03-2014) 	;Added 'note' by 14anu-ban-09 on (16-03-2015) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one19   "  ?id "  eka )" crlf))
)

;@@@ Added by 14anu20 on 23/06/2014.
;I would like to take that one.
;मैं वह लेना पसन्द करूँगा .
(defrule one20
(declare (salience 4500))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(viSeRya-det_viSeRaNa  ?id =(- ?id 1))
(id-word =(- ?id 1) this|that)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one20   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu20 on 19/06/2014
;My position is a secure one.
;मेरी स्थिति सुरक्षित है . 
;मेरा पद सुरक्षित है . [Improved Translation] 	;added by 14anu-ban-09 on (22-01-2015)
(defrule one21
(declare (salience 6600))
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 2) a)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse =(- ?id 1) adjective)

=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 2) -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " one.clp  one21  "  ?id "  " (- ?id 2) " -  )" crlf))
)

;Removed by 14anu-ban-09 on (27-01-2015)
;NOTE-The meaning given for "young ones" which is not the only meaning. In the counter example, the meaning is 'बच्चे'. Even the constraints not proper.
;### [Counter Example] ### They have five young ones and another on the way. [http://dictionary.reference.com/browse/young+ones]
;### [Counter Example] ### उनके पाँच बच्चे हैं और  एक और आने वाला हैं. 	[Self]
; Happily , there are still a handful of young ones who are determined to follow up on their big dreams because at the
;moment they probably know no other way 
;उनके कौशल की परीक्षा जळी ही होगी , और वह भी आनंद के खिलफ , परंपरा और इतिहास के खिलफ .
;@@@ Added by 14anu11
;(defrule one23
;(declare (salience 6000))
;(id-root ?id one)
;(id-root ?id1 young)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id ones )
;(id-root =(- ?id 1) young)
;(id-cat_coarse =(- ?id 1) adjective)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  yuvA_log))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one23   "  ?id "  yuvA_log)" crlf)
;)
;)

;$$$ Modified by 14anu-ban-09 on (27-01-2015)
;### [Counter Example] ### In these ashrams one has a sense of homecoming.	[Gyannidhi]
;### [Counter Example] ### ena ASramoM meM koI BI gara jEsA anuBava karawA hE. [Own Manual]
;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 06.06.2014 email-id:sahni.gourav0123@gmail.com
;sentence:One was with seven strings and the other with nine.
;hindi translation:एक नौ वालियों सात रस्सियों वाले और दूसरे वाला था . 
;एक सात तारो वाला था और दूसरा नौ वाला .
(defrule one24
(declare (salience 4600))	;Salience reduced '5000' to '4600' by 14anu-ban-09 on (27-01-2015)
(id-root ?id one)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
;(kriyA-subject ?id1 ?id)	;Commented by 14anu-ban-09 on (27-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  one.clp 	one24   "  ?id "  eka )" crlf))
)

;"one","Det","1.eka"
;I have one nice pen.
;
;
