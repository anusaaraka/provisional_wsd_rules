
;@@@ Added by 14anu-ban-11 on (24-01-2015)
;That's a sad quirk of nature.(coca)
;वह प्रकृति का,एक  दुःखद मोङ है.(manual)
(defrule quirk1
(declare (salience 100))
(id-root ?id quirk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 sad)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mofa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quirk.clp 	quirk1   "  ?id "  mofa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (24-01-2015) 
;Her lips quirked suddenly.(oald)
;उसके ओंठ अचानक हीले.(manual)
(defrule quirk2
(declare (salience 200))
(id-root ?id quirk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id ?id1)
(kriyA-subject  ?id ?id1)
(id-root ?id1 lip)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quirk.clp 	quirk2   "  ?id "  hIla )" crlf))
)

;-------------------------------- Default Rules -----------------

;@@@ Added by 14anu-ban-11 on (24-01-2015)
;That was the quirk in my particular deal.(coca)
;वह मेरे विशिष्ट डील में अनोखी चाल थी .(manual) 
(defrule quirk0
(declare (salience 0))
(id-root ?id quirk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anoKI_cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quirk.clp    quirk0   "  ?id "  anoKI_cAla )" crlf))
)

