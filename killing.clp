;@@@Added by 14anu-ban-07,(06-02-2015)
;We had to do some killing stomach exercises last night.(cambridge)(parser no. 34)
;हमें पिछली रात कुछ थकाऊ पेट के व्यायाम करने पडे . (manual)
(defrule killing1
(declare (salience 1000))
(id-root ?id killing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 exercise|schedule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakAU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  killing.clp      killing1   "  ?id "  WakAU)" crlf))
)

;@@@Added by 14anu-ban-07,(06-02-2015)
;She told us a killing story about her wedding day.(cambridge)(parser no. 12)
;उसने उपने विवाहोत्सव दिन के बारे में हमें  एक बेहद मजेदार किस्सा सुनाया. (manual)
(defrule killing2
(declare (salience 1100))
(id-root ?id killing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behaxa_majexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  killing.clp      killing2   "  ?id "  behaxa_majexAra)" crlf))
)

;------------------ Default Rules -----------

;@@@Added by 14anu-ban-07,(06-02-2015)
;Two puny men armed only with swords against a twenty-ton killing machine.(coca)(parser no. 94)
;दो शक्तिहीन आदमी एक बीस-टन घातक मशीन के विरुद्ध  सिर्फ़ तलवारों से सशस्त्र थे . (manual)
(defrule killing0
(id-root ?id killing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAwaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  killing.clp      killing0   "  ?id "  GAwaka)" crlf))
)

