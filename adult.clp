;@@@ Added by 14anu-ban-02 (03-12-2014)
;The adults feed on the fronds midribs and the rachis of inflorescence.[agriculture]
;मध्यनाली से जुडे लम्बे पडे पत्ते और  पुष्पक्रम के मेरुदण्ड पर व्यसक-कीडे  भोजन करते हैं.[manual]
(defrule adult2
(declare (salience 5000))
(id-root ?id adult)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 feed)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vayaska))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adult.clp 	adult2   "  ?id "  vayaska )" crlf))
)

;--------------------- Default rules -------------------

(defrule adult0
(declare (salience 5000))
(id-root ?id adult)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vayaska))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adult.clp 	adult0   "  ?id "  vayaska )" crlf))
)

;"adult","Adj","1.vayaska"
;Children are not allowed to see adult movies.
;
(defrule adult1
(declare (salience 4900))
(id-root ?id adult)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAliga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adult.clp 	adult1   "  ?id "  bAliga )" crlf))
)

;"adult","N","1.bAliga"
;Adults too like children's movies.
;
