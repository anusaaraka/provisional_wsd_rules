;@@@ Added by 14anu02 and Kusumika on 20.6.14
;News circulation is decreasing for the print media.
;समाचार सञ्चार छपाई सञ्चार माध्यम के लिए घट रहा है . 
(defrule circulation0
(declare (salience 5000))
(id-root ?id circulation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) news|blood|water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circulation.clp circulation0   "  ?id "  saMcAra )" crlf))
)
;------------------- Default rule ------------------------

;@@@ Added by 14anu-ban-03 (15-12-2014)
;A number of forged tickets are in circulation. [oald]
;जाली टिकटों की संख्या प्रचलन में हैं । 
(defrule circulation1
(declare (salience 00))
(id-root ?id circulation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracalana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circulation.clp   circulation1  "  ?id "  pracalana )" crlf))
)
