
(defrule whole0
(declare (salience 100))
(id-root ?id whole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whole.clp 	whole0   "  ?id "  pUrA )" crlf))
)

;"whole","Adj","1.samaswa"
;--"2.pUrA"
;The whole village was washed away by the floods.
;
(defrule whole1
(declare (salience 4900))
(id-root ?id whole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whole.clp 	whole1   "  ?id "  pUrNa )" crlf))
)

;default_sense && category=noun	samaswa	0
;"whole","N","1.samaswa"
;They all together make a whole.
;

;@@@ Added by Pramila(BU) on 14-03-2014
;He was whole after illness.   ;shiksharthi
;वह बीमारी के बाद भला-चंगा था.
(defrule whole2
(declare (salience 5000))
(id-root ?id whole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BalA-caMgA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whole.clp 	whole2   "  ?id "  BalA-caMgA )" crlf))
)

;@@@ Added by Anita--28.5.2014
;This gives us a preliminary sense of how sociology studies human society as an interconnected whole.
;इससे हमें यह प्रारम्भिक ज्ञान प्राप्त होता है कि समाजशास्र किस प्रकार से समाज का एक परस्पर सम्बद्ध समष्टि के रूप में अध्ययन करता है । 
(defrule whole3
(declare (salience 5000))
(id-root ?id whole)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?kri ?id)
(id-cat_coarse ?id noun)
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whole.clp 	whole3   "  ?id "  samaRti )" crlf))
)
