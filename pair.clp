
(defrule pair0
(declare (salience 0))
(id-root ?id pair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pair.clp 	pair0   "  ?id "  jodZA )" crlf))
)

;"pair","N","1.jodZA"
(defrule pair1
(declare (salience 4900))
(id-root ?id pair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pair.clp 	pair1   "  ?id "  jodZA_banA )" crlf))
)

;"pair","VTI","1.jodZA_banAnA"
;She will be paired with her senior in a doubles match.
;dabalsa mEca meM usakI jodZI jyeRTa KilAdZI ke sAWa banegI.
;

;@@@ Added by 14anu-ban-09 on 18-8-14
;It is different at interfaces of different pairs of liquids and solids. [NCERT CORPUS]
;xravoM waWA TosoM ke viBinna yugmoM ke aMwarApqRToM para yaha Binna - Binna howA hE.

(defrule pair2
(declare (salience 4500))
(id-root ?id pair)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 liquid|force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yugma))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pair.clp 	pair2   "  ?id "  yugma )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  pair.clp 	pair2   "  ?id "  physics )" crlf)
)

;@@@ Added by 14anu-ban-09 on 18-8-14
;We know from Newton's third law that these internal forces occur in equal and opposite pairs and in the sum of forces of Eq. (7.10), their contribution is zero.  [NCERT CORPUS]
;nyUtana ke wqwIya niyama se hama jAnawe hEM ki ye Anwarika bala saxEva barAbara parimANa ke Ora viparIwa xiSA meM kAma karane vAle jodoM ke rUpa meM pAe jAwe hEM Ora isalie samIkaraNa (7.10) meM baloM ko jodane meM inakA yoga SUnya ho jAwA hE.
;The net contribution of every such pair to the integral and hence the integral ∫xdm itself is zero.[NCERT CORPUS]
;samAkala meM hara jode kA yogaxAna SUnya hE Ora isa kAraNa svayaM ∫@xdm kA mAna SUnya ho jAwA hE.

(defrule pair3
(declare (salience 4600))
(id-root ?id pair)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(or(and(viSeRya-viSeRaNa ?id ?id1)(id-root ?id1 opposite))(and(viSeRya-to_saMbanXI ?id ?id1)(id-root ?id1 integral)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodA))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pair.clp 	pair3   "  ?id "  jodA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  pair.clp 	pair3   "  ?id "  physics )" crlf)
)

;@@@ Added by 14anu-ban-09 on 18-8-14
;Thus, for the pair of masses, rotating about the axis through the center of mass perpendicular to the rod. [NCERT CORPUS]
;awaH, xravyamAnoM ke isa jode kA, xravyamAna kenxra se gujarawI Cada ke lambavaw akRa ke pariwaH jadawva AGUrNa.

(defrule pair4
(declare (salience 4600))
(id-root ?id pair)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 mass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pair.clp 	pair4   "  ?id "  jodA )" crlf))
)

