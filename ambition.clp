;@@@Added by 14anu-ban-02(20-04-2015)
;He's always had a burning ambition to start his own business.[oald]
;उसे  हमेशा  अपना उद्योग शुरु करने की अधिक लालसा  थी . [self]
(defrule ambition1	;rule does'nt get activated because 'burning' is not treated as adjective by any of the parsers.
(declare (salience 100))
(id-root ?id ambition)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 burning)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAlasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ambition.clp 	ambition1   "  ?id "  lAlasA )" crlf))
)
;@@@Added by 14anu-ban-02(20-04-2015)
;He has already achieved his main ambition in life - to become wealthy.[cald]
;उसने अपने जीवन की प्रमुख आकाङ्क्षा पहले से ही पूरी कर ली- धनवान बनने की . [self]
(defrule ambition2
(declare (salience 100))
(id-root ?id ambition)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 main)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkAMkRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ambition.clp 	ambition2   "  ?id "  AkAMkRA)" crlf))
)
;------------------------------default_rules-------------------------------
;@@@Added by 14anu-ban-02(20-04-2015)
;She doubts whether she'll ever be able to fulfil her ambition.[oald]
;उसे सन्देह है कि वह कभी भी  अपनी महत्वाकाङ्क्षा को पूरा करने में समर्थ होगी .[self] 
(defrule ambition0
(declare (salience 0))
(id-root ?id ambition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawvAkAMkRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ambition.clp 	ambition0   "  ?id "  mahawvAkAMkRA )" crlf))
)
