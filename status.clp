;@@@Added by Manasa (31-8-2015) Arsha sodha sansthan.
;The party denied the legal status.(oxf)
;pArtI ne kAnUnI xarjA kA inkAra kiyA.
(defrule status1
(declare (salience 0))
(id-root ?id status)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarjA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  status.clp   status1   "  ?id "  xarjA  )" crlf))
)

;@@@ Added by Manasa (31-8-2015) Arsha sodha sansthan.
;You will learn more about the present status of varioius infrastructure.
;wuma aBi AXArika sMracanA ke  wAwkAik sWwi ke bAre me ora BI sIKoge.
(defrule status2
(declare (salience 100))
(id-root ?id status)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  status.clp     status2   "  ?id "  sWiwi  )" crlf))
)


