;@@@ Added by 14anu03 on 26-june-14
;She stayed in her tree, waiting for Elise.
;वह एलीसे के लिए प्रतीक्षा करती हुई अपने घर में,रही .
(defrule tree102
(declare (salience 5400))
(id-root ?id tree)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) her)
(id-word =(- ?id 2) in)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tree.clp 	tree102   "  ?id "  gara )" crlf))
)

;$$$ Modified by 14anu-ban-07 (09-12-2014),use parser no.2
;@@@ Added by 14anu03 on 26-june-14
;She likes to tree.
;उसे पेड लगाना पसंद है.
(defrule tree101
(declare (salience 5400))
(id-root ?id tree)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peda_lagA))  ; changed from peda_lagAnA to peda_lagA,by 14anu-ban-07 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tree.clp 	tree101   "  ?id "  peda_lagA )" crlf))
)

;$$$ Modified by 14anu-ban-07 (09-12-2014),use parser no.2
;@@@ Added by 14anu03 on 26-june-14
;Her dog likes to tree squirrels.
;उसके कुत्ते को गिलहरी का पीछा करना पसन्द है . 
(defrule tree100
(declare (salience 5500))
(id-root ?id tree)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)  ;added by 14anu-ban-07 (09-12-2014)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)  ;commented by 14anu-ban-07 (09-12-2014) as ?id is verb
(id-word ?id1 squirrels|dog|bird)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pICA_kara))  ; changed from pICA_karanA to pICA_kara,by 14anu-ban-07 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tree.clp 	tree100   "  ?id "  pICA_kara )" crlf))
)

(defrule tree0
(declare (salience 5000))
(id-root ?id tree)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id treed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vqkRoM_se_pUriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  tree.clp  	tree0   "  ?id "  vqkRoM_se_pUriwa )" crlf))
)

;"treed","Adj","1.vqkRoM_se_pUriwa"
;A treed area is a feast to the eyes.
;
(defrule tree1
(declare (salience 4900))
(id-root ?id tree)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tree.clp 	tree1   "  ?id "  peda )" crlf))
)

;"tree","N","1.peda"
;Trees give shade.
;
;
