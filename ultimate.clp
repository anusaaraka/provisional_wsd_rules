

;@@@ Added by Anita-1.7.2014 
;Of course the ultimate responsibility for the present conflict without doubt lies with the aggressor . [cambridge ;dictionary] 
;वास्तव में वर्तमान संघर्ष की पूरी जिम्मेदारी निश्चत रूप से आक्रमण कर्ता की है ।
;We will accept ultimate responsibility for whatever happens. [oxford learner's dictionary]
;जो कुछ भी होता है , उसकी हम पूरी जिम्मेदारी स्वीकार करेंगे ।
(defrule ultimate0
(declare (salience 100))
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 responsibility)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate0   "  ?id "  pUrI )" crlf))
)

;@@@ Added by Anita-1.7.2014 
;My manager will make the ultimate decision about who to employ . [cambridge dictionary]
;अंतिम निर्णय मेरा प्रबंधक लेगा कि किस को रोजगार दिया जाए ।
;The ultimate decision lies with the parents. [oxford learner's dictionary]
;अंतिम निर्णय माता - पिता का होता है ।
;This race will be the ultimate test of your skill. [oxford learner's dictionary]
;यह प्रतियोगिता आपके हुनर का अंतिम परीक्षण होगी ।
;Nuclear weapons are the ultimate deterrent. [oxford learner's dictionary]
;परमाणु हथियार अंतिम निवारक हैं ।
(defrule ultimate1
(declare (salience 200))
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 decision|authority|test|deterrent)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate1   "  ?id "  anwima )" crlf))
)

;@@@ Added by Anita-1.7.2014 
;Infidelity is the ultimate betrayal . [cambridge dictionary]
;बेवफाई परम विश्वासघात है ।
;This is the ultimate luxury cruiser . [cambridge dictionary]
;यह परम आरामदायक क्रूजर है ।
;Silk sheets are the ultimate luxury. [oxford learner's dictionary]
;रेशमी चादरें परम आरामदायक है ।
(defrule ultimate2
(declare (salience 300))
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 betrayal|cruiser|luxury)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate2   "  ?id "  parama )" crlf))
)

;@@@ Added by Anita-11.8.2014 
;The hotel is described as 'the ultimate in luxury'. [cambridge dictionary] [Parse problem]
;होटल का 'सुखसाधन में सर्वश्रेष्ठ' के रूप में वर्णन किया गया है ।
(defrule ultimate3
(declare (salience 400))
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(viSeRya-in_saMbanXI  ?id ?)
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvSreRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate3   "  ?id "  sarvSreRTa )" crlf))
)

;@@@ Added by Anita-11.8.2014 
;I mean, tackling six men single-handedly - that really is the ultimate in stupidity! [Parse problem]
;मेरा तात्पर्य है ,अकेले छः आदमियों से निपटना - वह वास्तव में मूर्खता की चरम सीमा है!
(defrule ultimate4
(declare (salience 500))
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 stupidity|idiotic)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id carama_sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate4   "  ?id "  carama_sImA )" crlf))
)


;######################################default-rule############################
;@@@ Added by Anita-11.8.2014 
;Our company is the ultimate in Interior designing. [By Krithika][Parse problem]
;हमारी कंपनी इंटीरियर डिजाइनिंग में सर्वश्रेष्ठ है ।
(defrule ultimate_default-rule
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvSraRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate_default-rule   "  ?id "  sarvSraRTa )" crlf))
)

;@@@ Added by Anita-1.7.2014 
;We could not trace the ultimate source of the rumours. [oxford learner's dictionary]
;हम अफवाहों का मूतभूत स्रोत नहीं ढूँढ सके ।
(defrule ultimate_default-rule1
(id-root ?id ultimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlaBUwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ultimate.clp 	ultimate_default-rule1   "  ?id "  mUlaBUwa )" crlf))
)
