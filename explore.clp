;@@@ Added by 14anu-ban-04 (19-02-2015)
;She explored the sand with her toes.               [olad]
;उसने अपने पाँव के अँगूठों से  रेत को महसूस किया .                  [self]
(defrule explore1
(declare (salience 20))
(id-root ?id explore)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 toe|hand|leg|foot)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id mahasUsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  explore.clp    explore1   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explore.clp  explore1   "  ?id " mahasUsa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (19-02-2015)
;They explored the land to the south of the Murray River.                [oald]
;उन्होंने दक्षिण की ओर मरी नदी की भूमि का  अन्वेषण किया .                                 [self]
(defrule explore0
(declare (salience 10))
(id-root ?id explore)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))  
(assert (id-wsd_root_mng ?id anveRaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  explore.clp    explore0   "  ?id " kA  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explore.clp  explore0   "  ?id " anveRaNa_kara )" crlf))
)

