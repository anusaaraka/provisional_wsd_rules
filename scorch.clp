;@@@ Added by 14anu-ban-11 on (17-02-2015)
;The leaves will scorch if you water them in the sun.(oald)
;पत्तियाँ झुलस जाएँगी यदि आप धूप में उनको सींचते हैं . (anusaaraka) 
(defrule scorch1
(declare (salience 20))
(id-root ?id scorch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 leave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Julasa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorch.clp 	scorch1   "  ?id "  Julasa_jA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (17-02-2015)
;She scorched to victory in the sprint final. (oald)
;वह लघु दौड अन्तिम में जीत के लिये तेजी से गई .(self) 
(defrule scorch2
(declare (salience 30))
(id-root ?id scorch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 victory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorch.clp 	scorch2  "  ?id "  wejI_se_jA )" crlf))
)

;------------------- Default Rules ----------------
;@@@ Added by 14anu-ban-11 on (17-02-2015)
;I scorched my dress when I was ironing it. (oald)
;मैंने मेरा लिबास जला दिया जब मैं  इस्त्री कर रहा था . (anusaaraka)
(defrule scorch0
(declare (salience 10))
(id-root ?id scorch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorch.clp   scorch0   "  ?id "  jalA_xe )" crlf))
)


