;$$$ Modified by 14anu-ban-02 -- added Eng and man translations
;Small drops of ice in the extremely beautiful lakes named Visansar - Kisansar seem very alluring .
;विसंसर  -  किसंसर  नामक  बेहद  सुंदर  झीलों  में  बर्फ  के  नन्हें-नन्हें  कण  तैरते  हुए  बड़े  मोहक  लगते  हैं  ।
(defrule allure0
(declare (salience 5000))
(id-root ?id allure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id alluring )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mohaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  allure.clp  	allure0   "  ?id "  mohaka )" crlf))
)

;"alluring","Adj","1.mohaka"
;Cricket is an alluring game.


;$$$ Modified by 14anu-ban-03 (30-07-2014)
; Meaning change from mAyA to moha_le 
;Passing through the road between tea estates allure hearts in first glimpse.
;cAya bAgAnoM ke bIca sadaka se gujaranA pahalI hI bAra meM mana moha lewA hE.
(defrule allure1
(declare (salience 4900))
(id-root ?id allure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) ; parser category problem (treating it as noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moha_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allure.clp 	allure1   "  ?id "  moha_le )" crlf))
)


;"allure","N","1.mAyA"
;Villagers get easily carried away by the false allure of city-life.
;
(defrule allure2
(declare (salience 4800))
(id-root ?id allure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  allure.clp 	allure2   "  ?id "  luBA )" crlf))
)

;"allure","VT","1.luBAnA"
;Villagers are easily allured to the glitter of city-life.
;
