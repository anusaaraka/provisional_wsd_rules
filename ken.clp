;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Addded by 14anu26  [05-07-14]
;it was beyond my ken.
;यह मेरी पहुँच के बाहर था . 
(defrule ken0
(declare (salience 0))
(id-root ?id ken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ken.clp 	ken0   "  ?id "  pahuzca  )" crlf))
)

;@@@ Addded by 14anu26  [05-07-14]
;do you ken him?
;क्या आप उसको पहचानते है ?
(defrule ken1
(declare (salience 100))
(id-root ?id ken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ken.clp 	ken1   "  ?id "  pahacAna  )" crlf))
)

;@@@ Addded by 14anu26  [05-07-14]
;He was unable to ken about the matter.
;वह विषय के बारे में समझने मे असमर्थ था . 
(defrule ken2
(declare (salience 100))
(id-root ?id ken)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?id  ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ken.clp 	ken2   "  ?id "  samaJa  )" crlf))
)

