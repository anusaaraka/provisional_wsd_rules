;@@@ Added by 14anu-ban-06 (05-02-2015)
;The Court of Appeal exercised its jurisdiction to order a review of the case.(OALD)
;कोर्ट ऑफ अपील  ने मामले के पुनरावलोकन के लिए अपने न्याय अधिकार का प्रयोग करते हुए आदेश दिया . (manual)
(defrule jurisdiction0
(declare (salience 0))
(id-root ?id jurisdiction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nyAya_aXikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jurisdiction.clp 	jurisdiction0   "  ?id "  nyAya_aXikAra )" crlf))
)

;@@@ Added by 14anu-ban-06 (05-02-2015)
;These matters do not fall within our jurisdiction. (OALD)
;ये विषय हमारे अधिकार क्षेत्र में नहीं आते हैं .(manual)
(defrule jurisdiction1
(declare (salience 2000))
(id-root ?id jurisdiction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-within_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAra_kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jurisdiction.clp 	jurisdiction1   "  ?id "  aXikAra_kRewra )" crlf))
)
