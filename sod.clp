;@@@ Added by 14anu-ban-11 on (25-02-2015)
;It was a real sod of a job.(oald)
;यह काम की वास्तव  दिक्कत थी . (self)
(defrule sod2
(declare (salience 5001))
(id-root ?id sod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xikkawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sod.clp 	sod2   "  ?id "  xikkawa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (25-02-2015)
;The area under the sod is eaten up by the domestic animals. (hinkhoj)
;घास के मैदान का क्षेत्र घरेलू पशुओं के द्वारा  खाया हुआ है . (self)
(defrule sod3
(declare (salience 5002))
(id-root ?id sod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-under_saMbanXI  ?id1 ?id)
(id-root ?id1 area)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAsa_kA_mExAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sod.clp 	sod3   "  ?id "  GAsa_kA_mExAna)" crlf))
)


;@@@ Added by 14anu-ban-11 on (25-02-2015)
;You lucky sod. (oald)
;आप  भाग्यशाली आदमी. (self)
(defrule sod4
(declare (salience 5003))
(id-root ?id sod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 lucky)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sod.clp 	sod4  "  ?id "  AxamI)" crlf))
)

;------------------------ Default Rules ----------------------

;"sod","N","1.samasyAkAraka_vyakwi"
;That stupid sod always created problems for me.
(defrule sod0
(declare (salience 5000))
(id-root ?id sod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samasyAkAraka_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sod.clp 	sod0   "  ?id "  samasyAkAraka_vyakwi )" crlf))
)

;;"sod","V","1.GAsa_lagAnA"
;Sod the fields.
(defrule sod1
(declare (salience 4900))
(id-root ?id sod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAsa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sod.clp 	sod1   "  ?id "  GAsa_lagA )" crlf))
)



;"sod","N","1.samasyAkAraka_vyakwi"
;That stupid sod always created problems for me.
;--"2.GAsa_kA_mExAna"
;The area under the sod is eaten up by the domestic animals.
;;"sod","V","1.GAsa_lagAnA"
;Sod the fields.

