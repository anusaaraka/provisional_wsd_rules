;@@@ Added by 14anu-ban-02 (14-11-2014)
;Sentence: Soy-based infant formula (SBIF) is sometimes given to infants who are not being strictly breastfed.[agriculture]
;Translation: सोया आधारित शिशु फार्मूला ( SBIF ) कभी कभी उन शिशुओं को  दिया जाता है जिन्हे  पूरी तरह से  स्तनपान नहीं कराया जाता हैं .[manual]
(defrule breastfeed0 
(declare (salience 0)) 
(id-word ?id breastfed) ;breastfed is the past form of breastfeed.since breastfeed has a root problem,thereforebreastfed will not be recognized in breastfeed.clp.so the rule will not fire unless breastfeed has a root.
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id swanapAna_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  breastfeed.clp  breastfeed0  "  ?id "  swanapAna_kara )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (14-11-2014)
;It is a law that you can breastfeed your child in public .[coca]
;यह एक कानून है कि आप जनता में अपने शिशु को स्तनपान करा सकती हैं.[manual]
(defrule breastfeed1 
(declare (salience 100)) 
(id-word ?id breastfeed) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject  ?id ?id1)
(id-word ?id1 you|she|lady|mother)
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id swanapAna_karA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  breastfeed.clp  breastfeed1  "  ?id "  swanapAna_karA )" crlf)) 
) 
