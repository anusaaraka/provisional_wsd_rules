;##############################################################################
;#  Copyright (C) 2014-2015 Komal Singhal (komalsinghal1744@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu06(Vivek Agarwal) on 19/6/2014
;Channel your skills
;अपनी काबीलियत का आयोजन करो.

(defrule channel00
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 curiosity|interset|skill|effort|labour|work )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  channel.clp 	channel00   "  ?id "  Ayojana_kara)" crlf))
)

;@@@ Added by 14anu-ban-03 on (10-09-2014)
;Watch a video of Ellie preparing this flavorful dish,along with other delicious recipes,at the Almond Boards You Tube channel,or at www. [google anusaaka report]
;bAxAma boVrdja para yA www para anya svAxiRta vyaFjana viXiyoM ke sAWa,yaha PleVvarPZala WAlI wEyAra karawe hue elI ke,vIdiyo ko yUtUba cEnala para xeKie. 
(defrule channel1
(declare (salience 200))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-word ?id1 youtube)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cEnala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  channel.clp 	channel1   "  ?id " cEnala  )" crlf))
) 

;@@@ Added by 14anu19 (17-06-2014)
;She had established channels of communication with the countries of Asia.  
;उसने एशिया के देशों के साथ सूचना के माध्यम स्थापित किए थे .
(defrule channel2
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id =(+ ?id 2))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mAXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  channel.clp     channel2   "  ?id "  mAXyama )" crlf))
)

;@@@ Added by 14anu19 (17-06-2014)
;There are twenty TV channels in India now. 
;अब भारत में बीस दूरदर्शन चैनल् हैं 
(defrule channel3
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id =(- ?id 1))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cEnala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  channel.clp     channel3   "  ?id "  cEnala )" crlf))
)


;@@@ Added by 14anu19 (17-06-2014)
;A channel is typically what you rent from a telephone company.
;चैनल आम तौर पर है आप जो एक टेलीफोन कम्पनी से किराये पर लेते हैं
(defrule channel4
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id =(- ?id 1))
(kriyA-subject  =(+ ?id 1) ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cEnala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  channel.clp     channel4   "  ?id "  cEnala )" crlf))
)

;@@@ Added by 14anu-ban-03 (26-03-2015)
;Flicking through the channels, I came across an old war movie. [oald]
;चैनलों को बदलते हुए,मैं एक पुराने युद्ध फिल्म के पार आया. [manual]
(defrule channel5
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-through_saMbanXI ?id1 ?id)
(id-root ?id1 flick)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cEnala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  channel.clp     channel5   "  ?id "  cEnala )" crlf))
)


;@@@ Added by 14anu-ban-03 (26-03-2015)
;The application must go through official channels. [hinkhoj]
;आवेदन आधिकारिक माध्यम से जाना चाहिए । [manual]
(defrule channel6
(declare (salience 5000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mAXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  channel.clp     channel6   "  ?id "  mAXyama )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 on (10-09-2014)
;The water channels crisscrossing the garden used to be full of colorful fish of various species . [karan singla]
;वे नहरे जो कि बाग में तानाबाना बनाती है , की प्रकार की रंगीली मछलियों से भरा रहता है .
(defrule channel0
(declare (salience 000))
(id-root ?id channel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  channel.clp 	channel0   "  ?id " nahara  )" crlf))
)




