
(defrule intensive0
(declare (salience 5000))
(id-root ?id intensive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saGana_saMrakRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intensive.clp 	intensive0   "  ?id "  saGana_saMrakRaNa )" crlf))
)


;$$$ Modified by 14anu-ban-06 (02-02-2015)
;Intensive methods are implemented in the field of agriculture.(intensive.clp)
;कृषि वर्ग के क्षेत्र में वृद्धिकर विधियाँ कार्यान्वयन की जातीं हैं . (manual)
(defrule intensive1
(declare (salience 5100))
(id-root ?id intensive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)	;added by 14anu-ban-06 (02-02-2015)
(id-root ?id1 method)		;added by 14anu-ban-06 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXikara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intensive.clp 	intensive1   "  ?id "  vqxXikara )" crlf))
)

;@@@ Added by 14anu24[16-6-14]
;Under the intensive system , the hens are kept confined either on deep litter in a room or under a shed or in individual cages.
;श्रमप्रधान प्रणाली में मुर्गियों को कमरे में अथवा छप्पर के नीचे काफी घासफूस डालकर रखा जाता है अथवा इन्हें अलग अलग पिंजरों में रखा जाता है .
(defrule intensive2
(declare (salience 5100));salience increased from '3000' to '5100' by 14anu-ban-06 (11-12-2014)
(id-root ?id intensive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) system)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SramapraXAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intensive.clp        intensive2   "  ?id "  SramapraXAna )" crlf))
)


;"intensive","Adj","1.vqxXikara"
;Intensive methods are implemented in the field of agriculture.
;--"2.wIvra"
;An intensive search helped him to regain his possessions.
;
;

;@@@ Added by 14anu-ban-06 (31-01-2015)
;After becoming the Culture and Tourism minister when I visited the Fort for an intensive study of the Red Fort I saw several dilapidated constructions .(parallel corpus)
;संस्कृति व पर्यटन मंत्री बनने के बाद जब मैंने लाल किले का गहन अध्ययन करने के लिए किले का दौरा किया तो वहाँ मैंने अनेक जीर्ण-शीर्ण संरचनाएँ देखीं ।(parallel corpus)
;The clinical depiction of it is such that deep intensive decline , contractions of bladder and atrophy in hands - feet arises .(parallel corpus)
;इसका नैदानिक चित्रण इस प्रकार होता है कि गहन संवेदना ह्रास , ब्लैडर का संकुचन और हाथों - पैरों में क्षीणता आ जाती है ।(parallel corpus)
;After considerable intensive research and experimentation , the Company succeeded , in a matter of three months , in manufacturing such a critical ortinance material up to English specifications .(parallel corpus)
;काफी गहन शोध और प्रयोग के बाद कंपनी को इस प्रकार की महत़्वपूर्ण युद्ध सामग्री के निर्माण में जो अंग्रेजी की आवश़्यकता के अनुसार थी तीन महीने के समय में सफलता मिल सकी .(parallel corpus)
(defrule intensive3
(declare (salience 5100))
(id-root ?id intensive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 study|decline|research)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intensive.clp 	intensive3   "  ?id "  gahana )" crlf))
)

