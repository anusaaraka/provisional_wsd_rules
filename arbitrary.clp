;@@@ Added by 14anu-ban-02 (04-12-2014)
;The grades given by the teacher appeared to be arbitrary.[karan singla]
;शिक्षक द्वारा दिए गए ग्रेड मनमाने हुए लगे.[manual]
(defrule arbitrary0 
(declare (salience 0)) 
(id-root ?id arbitrary) 
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id manamAnA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrary.clp  arbitrary0  "  ?id "  manamAnA)" crlf)) 
)


;@@@ Added by 14anu-ban-02 (04-12-2014)
;Sentence: The office timings were same for all but the breaks were arbitrary.[englishleap]
;Translation: दफ्तर का समय सभी के लिये समान था लेकिन अंतराल  अनियमित थे.[manual]
(defrule arbitrary1 
(declare (salience 0)) 
(id-root ?id arbitrary) 
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aniyamiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrary.clp  arbitrary1  "  ?id "  aniyamiwa )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (04-12-2014)
;The court gave an arbitrary ruling in his favor.[englishleap]
;न्यायाधीश ने उसके पक्ष में एकपक्षीय निर्णय दिया.[manual]
(defrule arbitrary2 
(declare (salience 100)) 
(id-root ?id arbitrary) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(viSeRya-in_saMbanXI  ?id1 ?id2) 
(id-root ?id1 ruling)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ekapakRIya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrary.clp  arbitrary2  "  ?id "  ekapakRIya )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (04-12-2014)
;Now, for any other arbitrary point G (Fig. 10.10) let the phase difference between the two displacements be φ.[ncert]
;अब, किसी दूसरे यथेच्छ बिंदु G (चित्र 10.10 ) के लिए मान लीजिए दो विस्थापनों के बीच कलान्तर φ है.[ncert]
(defrule arbitrary3 
(declare (salience 100)) 
(id-root ?id arbitrary) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 point)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id yaWecCa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrary.clp  arbitrary3  "  ?id "  yaWecCa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(19-02-2015)
;If we consider points at arbitrary distance from the surface of the earth, the result just derived is not valid since the assumption that the gravitational force mg is a constant is no longer valid.[ncert 11_01]
;यदि हम पृथ्वी के पृष्ठ से यादृच्छिक दूरियों के बिंदुओं पर विचार करें तो उपरोक्त परिणाम प्रामाणिक नहीं होते क्योंकि तब यह मान्यता कि गुरुत्वाकर्षण बल mg अपरिवर्तित रहता है वैध नहीं है.[ncert]
(defrule arbitrary4 
(declare (salience 100)) 
(id-root ?id arbitrary) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 distance)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id yAxqcCika)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrary.clp  arbitrary4  "  ?id "  yAxqcCika )" crlf)) 
) 
