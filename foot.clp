;@@@ Added by 14anu13 on 19-06-14
; The players then shoot arrows at spots marked near the feet .
;तब खिलाडी पैरों के निकट चिन्हित निशान पर तीर मारते है |
(defrule foot03
(declare (salience 5000))
(id-root ?id foot)
(id-root ?id1 the)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foot.clp 	foot03   "  ?id "  pEra )" crlf))
)


;$$$ Modified by 14anu13 on 19-06-14 (commented previous conditions and added new one because I think it is now sufficient '(viSeRya-det_viSeRaNa ?id ?id1)' was not suitable for the sentences like example given below)
; The players then shoot arrows at spots marked near the feet .
;तब खिलाडी पैरों के निकट चिन्हित निशान पर तीर मारते है |
;Modified by Meena(14.9.09); added(viSeRya-saMKyA_viSeRaNa ..) and commented (id-cat_coarse ..)
;He was about six feet tall.
;It is 800 feet above the sea level.
;It is about eight hundred feet above the sea level.
(defrule foot0
(declare (salience 5000))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
;(or(viSeRya-saMKyA_viSeRaNa  ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id1))
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
;(id-cat_coarse =(- ?id 1) cardinal)
(id-cat_coarse ?id noun) ;added by 14anu13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foot.clp 	foot0   "  ?id "  PUta )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (24-04-2015)
;At the foot of the stairs she turned to face him.	[OALD]
;सीढ़ियों के निचला सिरे पर वह उसका सामना करने के लिए मुखातिब हुई. [MANUAL]
;@@@ Added by 14anu05 GURLEEN BHAKNA on 21.06.14
;When he reached at the foot of the hill, his face was pale with terror.
;जब वह पहाडी के पैरों में पहुंचा, उसका मुख आतंक के साथ पीला था .
(defrule foot4
(declare (salience 5500))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 stair|pyramid|tower|building)	;added 'stair' by 14anu-ban-05 and removed 'hill', 'cliff', 'mountain' by 14anu-ban-05 on (24-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nicalA_sirA)); meaning changed "pEra" to "nicalA_sirA" by 14anu-ban-05 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foot.clp 	foot4   "  ?id "  nicalA_sirA )" crlf))
)


(defrule foot1
(declare (salience 4900))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id footing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  foot.clp  	foot1   "  ?id "  sWiwi )" crlf))
)

;given_word=footing && word_category=noun	$AXAra)

;"footing","N","1.AXAra"
;His company is now on a firm footing in the market.
;--"2.saMwulana"
;She lost her footing in the slippery bathroom && fell down.
;--"3.paxavI"
;The workers wanted to be on equal footing with their officers.
;
(defrule foot2
(declare (salience 5000))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(or (viSeRya-det_viSeRaNa  ?id  ?id2) (viSeRya-RaRTI_viSeRaNa ?id ?)); added RaRTI relation by Roja(02-02-11)When you stand on this rock and face the east, the waves of the bay of bengal lap your feet.
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAzva))
(assert (kriyA_id-object_viBakwi ?id1 se));Added by Roja(02-02-11)When you stand on this rock and face the east, the waves of the bay of bengal lap your feet.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foot.clp 	foot2   "  ?id "  pAzva )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  foot.clp   foot2   "  ?id1 " se )" crlf)
)
)

;@@@Added by 14anu-ban-05 Prajna Jha on 05-08-2014
;Our feet stop due to the friction which does not allow relative motion between the feet and the floor of the bus.[NCERT Physics]  
;hamAre pEra GarRaNa ke kAraNa ruka jAwe hEM, kyofki GarRaNa bala pEroM waWA basa ke ParSa ke bIca ApekRa gawi nahIM hone xewA.
(defrule foot3
(declare (salience 2000))
(id-root ?id foot)
(id-word ?id feet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  foot.clp  	foot3   "  ?id "  pEra )" crlf))
)

;@@@ Added by 14anu-ban-05 on 07-08-2014
;You must give references on the foot of a page.
;wuma peja ke nicale BAga meM saMxarBa sUcI avaSya xo.
(defrule foot04
(declare (salience 3000))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1 )
(id-root ?id1 page|stair|bed)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nicale_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  foot.clp  	foot04   "  ?id "  nicale_BAga )" crlf))
)



;@@@ Added by 14anu-ban-05 on 11-08-2014
;On the Western foot of the hill near the way up an ancient pond is famous by the name of Patal Ganga .[PARALLEL CORPUS]
;पहाड़ी की दक्षिणी तलहटी में गुफा के चढ़ाई मार्ग के समीप पर प्राचीन तालाब पाताल गंगा के नाम से प्रसिद्ध है ।
(defrule foot5
(declare (salience 2000))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 hill|cliff|mountain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walahatI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  foot.clp  	foot5   "  ?id "  walahatI )" crlf))
)


;@@@ Added by 14anu-ban-05 on 11-08-2014
;Tourists can also roam on foot in Ooty .[PARALLEL CORPUS]
;UtI meM sElAnI pExala BI GUma sakawe hEM
(defrule foot6
(declare (salience 2000))
(id-root ?id foot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExala ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  foot.clp  	foot6   "  ?id "  pExala )" crlf))
)

;LEVEL 
;Headword : foot
;
;Examples --
;"foot","N","1.pEra/pAzva"
;He kicked the ball with his foot. 
;usane apane pEra se bAla ko Tokara mArI.
;--"2.nicalA BAga"  <---pAxa
;You must give references on the foot of a page.
;wuma peja ke nicale BAga meM saMxarBa (sUcI) avaSya xo. 
;--"3.pAxa tippaNI"
;A foot note is given at the end of a page.
;pqRTa ke aMwa meM pAxa tippadZI xI gayI hE.
;--"4.pahAdZa kI walahatI"-pahAdZa kA nicalA BAga yA pAxa
;We camped at the foot of the hill.
;hamane pahAdZa kI walahatI meM kEmpa lagAyA
;--"5.Puta{mApa}"
;This wall is 12feet high.
;yaha xIvAra bAraha Puta UzcI hE.
;
;"on foot","pExala"
;Some people came on foot,others came by car.
;kuCa loga pExala Ae Ora kuCa kAra se.
;
;vyAKyA --
;ukwa uxaHraNoM meM 1 se 4 waka paraspara sambanXiwa lagawe hEM. 'foot' kA mUla arWa 'pAzva' hE. isase viswAra hokara isameM anya arWa 'nicalA_BAga' A gayA. uxAharaNa 5 meM AnevAlA
;arWa inase asambaxXa lagawA hE. ho sakawA hE ki jaba waka nApane ke anya sAXana nahIM Ae We, 
;'pAzva' nApane kA eka sAXana rahA ho Ora isIliye 'foot' kA isa arWa meM prayoga hone lagA ho. paranwu kyoMki aba inameM sIXA sambanXa nahIM xiKawA hE isaliye inako alaga alaga mAna
;lenA hI uciwa hogA. 
;
;anwarnihiwa sUwra ; 
;
; pAzva --SarIra kA nicalA BAga --kisI BI vaswu kA nicalA BAga --pAxa, walahatI  
;He hurt his foot ....                               ...footnote, foot of the hill
;
;
;sUwra : pAzva`/Puta  
;
;
;
;
