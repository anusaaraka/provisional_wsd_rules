;@@@ Added by 14anu-ban-06 (20-03-2015)
;He laughed hollowly.(COCA)
;वह दिखावटी तरीके से हँसा . (manual)
(defrule hollowly0
(declare (salience 0))
(id-root ?id hollowly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKAvatI_warIke_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollowly.clp 	hollowly0   "  ?id "  xiKAvatI_warIke_se )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-03-2015)
; Her footsteps echoed hollowly in the quiet streets. (cambridge)
;उसके कदम शान्त सडकों में खोखली आवाज से प्रतिध्वनित हुए .(manual)
;His footsteps echo hollowly on the cold stone ground. (OALD)
;उसके कदम ठण्डे पत्थर क्षेत्र पर खोखली आवाज से प्रतिध्वनित हुए . (manual) 
(defrule hollowly1
(declare (salience 2000))
(id-root ?id hollowly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 echo)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KoKalI_AvAja_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollowly.clp 	hollowly1   "  ?id "  KoKalI_AvAja_se )" crlf))
)
