;@@@Added by 14anu-ban-02(18-02-2015)
;Sentence: For example, the bending of red component of white light is least while it is most for the violet.[ncert12_09]
;Translation: उदाहरण के लिए, श्वेत प्रकाश का लाल घटक सबसे कम मुडता है जबकि बैंगनी घटक अधिक मुडता है.[ncert]
;Translation: उदाहरण के लिए, श्वेत प्रकाश का लाल घटक का मुड़ना सबसे कम  है जबकि बैंगनी घटक अधिक  है.[self]
(defrule bending0 
(declare (salience 0)) 
(id-root ?id bending) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id mudZanA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bending.clp  bending0  "  ?id "  mudZanA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-02-2015)
;From Eq. (9.16), we see that to reduce the bending for a given load, one should use a material with a large Young's modulus Y.[11_09]
;समीकरण (9.16) से हम देखते हैं कि किसी दिये हुए भार के लिए बङ्कन कम करने के लिए ऐसे द्रव्य का उपयोग करना चाहिए जिसका यङ्ग गुणाङ्क Y अधिक हो.[ncert]
(defrule bending1 
(declare (salience 100)) 
(id-root ?id bending) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 load)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id badZkana)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bending.clp  bending1  "  ?id "  badZkana )" crlf)) 
) 


