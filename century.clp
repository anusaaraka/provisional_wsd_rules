;########################################################################
;#  Copyright (C) 02.07.2014 14anu21
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`

;@@@ Added by 14anu21 on 02.07.2014
;He scored a century.[self]
;उसने शताब्दी प्राप्त की . (Translation before adding rule)
;Added rule score9 and this rule to correct the translation
;उसने शतक बनाया . 
;He made a century.[self]
;उसने शताब्दी बनाई . (Translation before adding rule)
;उसने शतक बनाया . 
(defrule century0
(declare (salience 100))   ;added by 14anu-ban-03 (10-12-2014)
(id-root ?id century)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object ?id1 ?id)(kriyA-subject ?id1 ?id))
(id-root ?id1 score|make)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sawaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  century.clp 	century0  "  ?id "  Sawaka )" crlf))
)

;@@@ Added by 14anu-ban-03 (10-12-2014)
;Eighteenth century writers. [oald]
;अठ्ठारहवी शताब्दी के लेखक. [manual]
(defrule century1
(declare (salience 00))
(id-root ?id century)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SawAbxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  century.clp 	century1  "  ?id "  SawAbxI )" crlf))
)



