;Commented by 14anu-ban-01 on (10-04-2015) because this rule should be in 'stilted.clp'
;(defrule stilt0
;(declare (salience 5000))
;(id-root ?id stilt)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id stilted )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id ruKAI_se))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stilt.clp  	stilt0   "  ?id "  ruKAI_se )" crlf))
;)

;"stilted","Adj","1.ruKAI_se"
;She said it in a stilted tone.

;$$$ Modified by 14anu-ban-01 on (10-04-2015):corrected meaning
;Stilts are used by men in the circus to entertain people.[stilt.clp]
;सर्कस में लोगों का मनोरञ्जन करने के लिए लोगों वयक्तियों द्वारा चलने_के_लिये_बडे _डण्डे उपयोग किए जाते हैं. [self]
(defrule stilt1
(declare (salience 4900))
(id-root ?id stilt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calane_ke_liye_badZA_daNdA))	;changed "calane_ke_liye_badZe_daMde" to "calane_ke_liye_badZA_daNdA" by 14anu-ban-01  on (10-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stilt.clp 	stilt1   "  ?id "  calane_ke_liye_badZA_daNdA)" crlf))	;changed "calane_ke_liye_badZe_daMde" to "calane_ke_liye_badZA_daNdA" by 14anu-ban-01 on (10-04-2015)
)


;@@@ Added by  14anu-ban-01 on (10-04-2015)
;She had to walk on stilts because of her injured legs.[self:with respect to cald]
;उसे अपने घायल पैरों की वजह से पाँव_रखकर_चलने_वाली_लकडियों पर चलना पडा .[self]
(defrule stilt2
(declare (salience 5000))
(id-root ?id stilt)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 walk|stand|wobble|sit|run|dance|balance|move)
(kriyA-on_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAzva_raKakara_calane_vAlI_lakadI))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stilt.clp 	stilt2   "  ?id "  pAzva_raKakara_calane_vAlI_lakadI)" crlf))	
)

;"stilt","N","1.calane ke liye badZe daMde"
;Stilts are used by men in the circus to entertain people.
;--"2.bAzsa jisa para Gara sWira ho"      
;The boat houses built on stills && situated in the middle of the lake look very beatiful.
;
;


;@@@ Added by  14anu-ban-01 on (13-04-2015)
;The houses are built on stilts to protect them from the annual floods.[cald]
;घरों को वार्षिक बाढ से बचाने के लिए  उन्हें बाँस पर बनाया जाता है.[self]
(defrule stilt3
(declare (salience 5000))
(id-root ?id stilt)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 house|room)
(kriyA-subject ?id1 ?id2)
(kriyA-on_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzsa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stilt.clp 	stilt3   "  ?id "  bAzsa)" crlf))	
)

