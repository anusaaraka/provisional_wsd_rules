;@@@ Added by 14anu-ban-06  (14-10-2014)
;Her father's death impacted greatly on her childhood years.(OALD)
;उसके पिता की मृत्यु ने उसके बचपन के वर्षों पर अत्यन्त गहरा असर डाला . (manual)
(defrule impact0
(declare (salience 0))
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaharA_asara_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impact.clp 	impact0   "  ?id "  gaharA_asara_dAla )" crlf))
)

;@@@ Added by 14anu-ban-06  (14-10-2014)
;By introducing many innovative ideas inside the building , the impact of climate in cold and hot seasons can be minimized .(parallel corpus)
;भवन के अन्तर्गत बहुत सी अभिनव विशिष्टताओं को समाहित कर जाड़े व गर्मी दोनों ऋतुओं में जलवायु के प्रभाव को कम किया जा सकता है ।(parallel corpus)
(defrule impact1
(declare (salience 0))
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impact.clp 	impact1   "  ?id "  praBAva )" crlf))
)

;@@@ Added by 14anu-ban-06 (14-10-2014)
;We also experience the impact of forces on us, like when a moving object hits us or we are in a merry-go-round.(NCERT)
;hama apane Upara baloM ke safGAwa, jEse kisI gawiSIla vaswu ke hamase takarAwe samaya aWavA "mErI go rAuNda JUle" meM gawi karawe samaya,anuBava karawe hEM .(NCERT)
(defrule impact2
(declare (salience 2500))
(Domain physics)
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safGAwa))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impact.clp 	impact2   "  ?id "  safGAwa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  impact.clp 	impact2   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (14-10-2014)
;The steam engine, as we know, is inseparable from the Industrial Revolution in England in the eighteenth century, which had great impact on the course of human civilisation.(NCERT)
;jEsA ki hama jAnawe hEM ki BApa kA iMjana, iMglEMda meM aTAharavIM SawAbxI meM huI Oxyogika krAnwi, jisakA mAnava saByawA para awyaXika praBAva huA, se apqWakkaraNIya hE.(manual)
(defrule impact3
(declare (salience 2550))
(Domain physics)
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impact.clp 	impact3   "  ?id "  praBAva )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  impact.clp 	impact3   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu03 on 18-june-2014
;The impact between cars was deadly.
;गाडियाँ के बीच टक्कर प्राणनाशक था .
(defrule impact100
(declare (salience 5500))
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 between|with)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_bIca_takkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  impact.clp     impact100   "  ?id "  " ?id1 "  ke_bIca_takkara )" crlf))
)

;$$$ Modified by 14anu-ban-06 (10-12-2014)
;@@@ Added by 14anu03 on 18-june-14
;The court ruling will impact the education of minority students.
;अदालत निर्णय अल्पसङ्ख्यक विद्यार्थियों की शिक्षा पर प्रभाव डालेगा.
(defrule impact101
(declare (salience 6500))
(id-root ?id impact)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 education)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva_dAla));meaning changed from 'para_praBAva_dAlegI' to 'praBAva_dAla' by 14anu-ban-06 (10-12-2014)
(assert (kriyA_id-object_viBakwi ?id para));added by 14anu-ban-06 (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impact.clp      impact101   "  ?id " praBAva_dAla)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  impact.clp       impact101   "  ?id " para )" crlf)
)
)
