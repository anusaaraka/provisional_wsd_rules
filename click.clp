;$$$ Modified by 14anu-ban-03 (17-02-2015)
;Click on the file and drag it across. [oald]
;फ़ाइल पर क्लिक करें और इसे सरकाए. [manual]
(defrule click1
(declare (salience 4900))
(id-root ?id click)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ?id ?) (kriyA-on_saMbanXI ?id ?))   ;added (kriyA-on_saMbanXI ?id ?) by 14anu-ban-03 (17-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klika_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  click.clp 	click1   "  ?id "  klika_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (17-02-2015)
;His mind clicks very fast. [same clp file]
;उसका दिमाग अत्यन्त तेज चलता है . [manual]
(defrule click3
(declare (salience 5000))
(id-root ?id click)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 mind)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  click.clp    click3   "  ?id "  cala )" crlf))
)

;------------------ Default Rules ----------------------
;"click","N","1.KataKata_kI_AvAjZa"
;The door was shut with a click.
(defrule click0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (17-02-2015)
(id-root ?id click)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KataKata_kI_AvAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  click.clp 	click0   "  ?id "  KataKata_kI_AvAjZa )" crlf))
)

(defrule click2
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (17-02-2015)
(id-root ?id click)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klika_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  click.clp 	click2   "  ?id "  klika_ho )" crlf))
)

;"click","V","1.klika_kara[ho]"
;--"2.klika_karanA[honA]"
;He clicked the camera just then.
;Click the button on the right.
;The window clicked shut.
;--"3.yakAyaka_samaJa_meM_AnA"
;His mind clicks very fast.
;


