;@@@ Added by 14anu-ban-07,28-07-2014
; के बीच में  :
;They strolled among the crowds.(oald)
;ve BIda ke bIca meM tahala rahe We.(manually)
(defrule among0
(declare (salience 0))
(id-root ?id among)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bIca_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  among.clp 	among0   "  ?id "  ke_bIca_meM )" crlf))
)



;@@@ Added by 14anu-ban-07,28-07-2014
;में   :
;Among the highest buildings of Thailand 'Ushakal-ka-mandir' is also worth watching.(parallel corpus)
;थाईलैंड  की  सबसे  ऊँची  इमारतों  में  ’  उषाकाल  का  मंदिर  ’  भी  देखने  लायक  है  ।
;Among the visiting places of Pithoragarh is the hill located 7 kilometres away from Chandak city at a height of 2 thousand metres .(parallel corpus)
;पिथौरागढ़ के दर्शनीय स्थलों में ’चंडाक’ शहर से 7 किलोमीटर दूर 2 हजार मीटर की ऊँचाई पर स्थित पहाड़ी है ।
;Among the holiday packages announced so far this is very economical package .(parallel corpus)
;अब तक घोषित किए गए हॉलिडे पैकेजों में यह काफी किफायती पैकेज है ।

(defrule among1
(declare (salience 2000))
(id-root ?id among)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-among_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  among.clp 	among1   "  ?id "  meM )" crlf))
)



;@@@ Added by 14anu-ban-07,28-07-2014
;इनमें  से   :
;Koh Lan' is the longest beach among them.(parallel corpus)
;इनमें  से  ’  कोह  लेन  ’  सब  से  लंबा  बीच  है  ।
(defrule among2
(declare (salience 2100))
(id-root ?id among)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-among_saMbanXI  ? ?id1)
(id-cat_coarse ?id preposition)
(id-root ?id1 them)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id enameM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  among.clp 	among2   "  ?id "  enameM_se )" crlf))
)

