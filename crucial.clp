
;$$$ Modified by 14anu-ban-03 (04-02-2015)
;"crucial","Adj","1.saMkatapUrNa"
;At a crucial moment he rescued me. [hinkhoj]
;एक सङ्कटपूर्ण क्षण में उसने मुझे बचाया . [self]
(defrule crucial1
(declare (salience 5050))
(id-root ?id crucial)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)  ;added by 14anu-ban-03 (04-02-2015)
(id-root ?id1 moment)  ;added by 14anu-ban-03 (04-02-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkatapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crucial.clp 	crucial1   "  ?id "  saMkatapUrNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-02-2015)
;A crucial moment in his career. [hinkhoj]
;उसके कैरियर में एक अतयन्त महत्त्वपूर्ण क्षण . [self]
(defrule crucial2
(declare (salience 5050))
(id-root ?id crucial)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 moment) 
(viSeRya-in_saMbanXI ?id1 ?id2) 
(id-root ?id2 career)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awayanwa_mahawwvapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crucial.clp 	crucial2   "  ?id " awayanwa_mahawwvapUrNa )" crlf))
)

;-------------------- Default Rules -----------------

(defrule crucial0
(declare (salience 5000))
(id-root ?id crucial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirNAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crucial.clp 	crucial0   "  ?id "  nirNAyaka )" crlf))
)

