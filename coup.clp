
; NOTE- when root problem is solved word meaning is changed to root meaning.

;@@@ Added by 14anu-ban-03 (31-03-2015)
;Getting this contract has been quite a coup for us. [oald]
;यह अनुबन्ध प्राप्त करना काफी हद तक हमारे लिए अप्रत्याशित सफलता  है . [manual]
(defrule coup1
(declare (salience 10))
(id-word ?id coup)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id ?id1)
(id-root ?id1 quite)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aprawyASiwa_saPalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  coup.clp  	coup1   "  ?id "  aprawyASiwa_saPalawA )" crlf))
)


;@@@ Added by 14anu-ban-03 (31-03-2015)
;Two players are out of the team because of coup. [oald]
;दो खिलाडी चोट की वजह से  टीम  के बाहर हैं . [manual]
(defrule coup2
(declare (salience 10))
(id-word ?id coup)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cota))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  coup.clp  	coup2   "  ?id "  cota )" crlf))
)


;@@@ Added by 14anu-ban-03 (31-03-2015)
;She had set a coup for him. [oald]
;उसने उसके लिए चाल योजित की थी . [manual]
(defrule coup3
(declare (salience 10))
(id-word ?id coup)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 set)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  coup.clp  	coup3   "  ?id "  cAla )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (31-03-2015)
;A military coup. [oald]
;एक सैनिक आकस्मिक शासन परिवर्तन . [manual]
(defrule coup0
(declare (salience 00))
(id-word ?id coup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasmika_SAsana_parivarwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  coup.clp      coup0   "  ?id "  Akasmika_SAsana_parivarwana )" crlf))
)

