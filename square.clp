;##############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################

;$$$ Modified by 14anu-ban-01 on (10-01-2015)
;@@@ Added by 14anu04 on 1-7-2014
;Can I leave you to square up with the waiter?(oxford)
;क्या मैं आपको उसके साथ हिसाब करने के लिए छोड सकता हूँ? 
;क्या मैं वेटर का ठीक ठीक हिसाब चुकता करने का जिम्मा आप पर छोड सकता हूँ?[Tanslation improved by 14anu-ban-01 ,Suggested by Chaitanya Sir]
(defrule square_tmp6
(declare (salience 5100))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga  ?id ?id1)
(id-root ?id2 waiter|shopkeeper);added by 14anu-ban-01 on (10-01-2015)
(kriyA-with_saMbanXI  ?id ?id2)
;(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)));changed animate.gdbm to human.gdbm by 14anu-ban-01
;NOTE by 14anu-ban-01 -->The condition above can be restricted later because this sense will generally come only when the ?id2 is waiter|shopkeeper etc.depending upon the context.
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_TIka_hisAba_cukawA_kara));changed "hisAba_kara" to "TIka_TIka_hisAba_cukawA_kara" by 14anu-ban-01
(assert (make_verbal_noun ?id1));added by 14anu-ban-01 on (10-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun   " ?*prov_dir* "  square.clp	square_tmp6   "  ?id " )" crlf);added by 14anu-ban-01 on (10-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square_tmp6  "  ?id "  " ?id1 " TIka_TIka_hisAba_cukawA_kara  )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (10-01-2015)
;@@@ Added by 14anu04 on 1-7-2014
;Bruno squared himself to face the waiting journalists.
;ब्रुनो ने स्वयं को प्रतीक्षा करते हुये पत्रकार का सामना करने के लिए तैयार किया . 
;ब्रुनो ने स्वयं को प्रतीक्षा करते हुये पत्रकारों का सामना करने के लिए तैयार किया . [Translation improved by 14anu-ban-01 on (10-01-2015)]
(defrule square_tmp5
(declare (salience 4500))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb);corrected 'course' to 'coarse' by 14anu-ban-01
(kriyA-object  ?id ?id1);added by 14anu-ban-01
(id-cat_coarse ?id1 pronoun);added by 14anu-ban-01
;(id-root =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))commented by 14anu-ban-01 on (10-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square_tmp5   "  ?id " wEyAra_kara)" crlf))
)

;@@@ Added by 14anu04 on 1-7-2014
;He ran square into me.(shabdkosh)
;वह सीधा मेरी तरफ भागा. 
(defrule square_tmp4
(declare (salience 4500))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id2 ?id)
(kriyA-into_saMbanXI  ?id2 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square_tmp4   "  ?id " sIXA)" crlf))
)

;$$$ Modified by 14anu-ban-01 on (10-01-2015)
;@@@ Added by 14anu04 on 1-7-2014
;It was a square deal. (oxford)
;यह एक समान डील थी. 
;यह एक खरा सौदा था . [Translation improved by 14anu-ban-01 on (10-01-2015)]
;It was a square partnership.[square.clp]
;यह एक बराबरी की भागीदारी थी.[self]
(defrule square_tmp3
(declare (salience 4500))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) partnership);removed 'deal' from the list and changed 'word' to 'root' by 14anu-ban-01 on (10-01-2015) because correct meaning is coming from Database_compound_phrase_root_mng
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id samAna)); Commented by 14anu-ban-01 on (10-01-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) barAbarI_kI_BAgIxArI)) ;added by 14anu-ban-01 on (10-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square_tmp3  "  ?id "  " =(+ ?id 1) " barAbarI_kI_BAgIxArI )" crlf))
)

;@@@ Added by 14anu04 on 1-7-2014
;The teams were all square at half-time. (oxford)
;टीमें मध्यान्तर पर बराबरी पर थीं . 
(defrule square_tmp2
(declare (salience 5000))
(id-word ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 all)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 barAbarI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square_tmp2  "  ?id "  " ?id1 " barAbarI  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (10-01-2015):Run this sentence on parser no.7
;@@@ Added by 14anu04 on 1-7-2014
;Are you being square with me? (oxford)
;क्या आप मेरे साथ ईमानदार हैं? 
(defrule square_tmp
(declare (salience 4500))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) being)
(kriyA-subject  =(- ?id 1) ?id1);added by 14anu-ban-01 on (10-01-2015)
;(id-cat_coarse ?id verb)	;commented by 14anu-ban-01 on (10-01-2015) because it is 'adjective' here.
;(kriyA-with_saMbanXI  ?id ?id1);commented by 14anu-ban-01 on (10-01-2015) because it is 'adjective' here.
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ImAnaxAra));changed "ImAnaxAra_ho" to "ImAnaxAra" by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square_tmp   "  ?id " ImAnaxAra)" crlf));changed "ImAnaxAra_ho" to "ImAnaxAra" by 14anu-ban-01
)

;@@@ Added by Jagrithi
;The players squared up to each other and started shouting. 
;खिलाड़ी एक दूसरे से लड़ने के लिए तैयार हो गए और चिल्लाना आरंभ कर दिया.[cambridge advanced learner]
(defrule square0
(declare (salience 5000))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lafAI_ke_liye_wEyAra_ho)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square0  "  ?id "  " ?id1 " lafAI_ke_liye_wEyAra_ho  )" crlf))
)

;@@@ Added by Jagrithi
;If you pay for both tickets now,I will square up with you later.  
;यदि आप अब दोनों टिकट के लिए भुगतान करते हैं, तो मैं बाद में आपके साथ चुकता करूँगा .[cambridge advanced learner]
(defrule square1
(declare (salience 5000))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(id-cat_coarse ?id verb)
(kriyA-samakAlika_kriyA ?id ?id1)
(id-root ?id1 pay)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cukawA_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square1  "  ?id "  " ?id1 "  cukawA_kara  )" crlf))
)

;@@@ Added by Jagrithi
;India squared off the series.[english to hindi wordnet]
;भारत ने श्रृंखला को बराबर किया.
(defrule square2
(declare (salience 4800))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 off)
(kriyA-object ?id ?id2)
(id-word ?id2 series)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 barAbarI_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square2  "  ?id "  " ?id1 "  barAbarI_kara )" crlf))
)

;@@@ Added by Jagrithi
;The two candidates will square off on this issue in a debate tomorrow.
 ;दो उम्मीदवार कल बहस के इस मुद्दे पर स्पर्धा करेंगे.[oxford]
(defrule square3
(declare (salience 4700))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 off)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sparXA_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " square.clp	square3  "  ?id "  " ?id1 " sparXA_kara )" crlf))
)

;@@@ Added by Jagrithi
;Total area is 15 square meter.
;कुल क्षेत्रफल 15 वर्ग मीटर है.[cambridge advanced learner]
(defrule square4
(declare (salience 4600))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word ?id1 meter|centimeter|milimeter|miles|kilometer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square4   "  ?id " varga)" crlf))
)

;@@@ Added by Jagrithi
;We squared the bill.[english to hindi wordnet]
;हमने बिल चुकता कर दिया.
(defrule square5
(declare (salience 4500))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriya-object ?id ?id1)
(id-word ?id1 bill)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cukawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square5   "  ?id " cukawA_kara)" crlf))
)

;@@@ Added by Jagrithi
;You may leave but it would be better if you square it with the principal first.
;आप छोड सकते हैं परन्तु यह अधिक बेहतर होगा यदि आप प्रमुख  के साथ पहले इजाजत ले . 
;I think I'll be able to come, but I'll square it with my parents first.
;मैं सोचता हूँ कि मैं आने के लिए सक्षम हो जाऊँगा, लेकिन मैं पहली बार अपने माता पिता से इजाज़त लूँगा.                        [oxford advance learner]
(defrule square6
(declare (salience 4450))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 principal|parent|brother|sister|uncle|grandfather)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ijAjawa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square6   "  ?id " ijAjawa_le)" crlf))
)

;@@@ Added by Jagrithi
;One idea squares with another.
(defrule square7
(declare (salience 4400))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUla_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square7   "  ?id " anukUla_ho)" crlf))
)

;@@@ Added by Jagrithi
;Hit square in the jaw.
; जबड़े में सीधे चोट पहुंचाअो.[english to hindi wordnet]
(defrule square8
(declare (salience 4300))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-word ?id1 hit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square8   "  ?id " sIXe)" crlf))
)

;..........default rule..............
;@@@ Added by Jagrithi
;Square of 7 is 49 .
;7 का वर्ग 49 है.[cambridge advanced learner]
;;She moved her castle forward three squares. [cambridge advance ]
;उसने हाथी को तीन वर्ग  अागे बढाया.
(defrule square9
(declare (salience 0))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square9   "  ?id " varga)" crlf))
)

;@@@ Added by Jagrithi
;Do not square the corners but round them .
;कोने  वर्गाकार मत बनाइए परन्तु उनको गोल बनाइए . [english to hindi wordnet]
(defrule square10
(declare (salience 0))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vargAkAra_banA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square10   "  ?id " vargAkAra_banA)" crlf))
)

;@@@ Added by Jagrithi
;A square jaw .
;एक  वर्गाकार जबड़ा.[english to hindi wordnet]
(defrule square11
(declare (salience 0))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vargAkAra ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square11   "  ?id " vargAkAra)" crlf))
)

;@@@ Added by Jagrithi
;Hit square in the jaw.
;जबड़े में सीधे चोट पहुंचाअो.[english to hindi wordnet]
(defrule square12
(declare (salience 0))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square12   "  ?id " sIXe)" crlf))
)

;$$$ Modified by 14anu-ban-01 on (10-01-2015)
;@@@ Added by Jagrithi
(defrule square13
(declare (salience 0))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun);added by 14anu-ban-01 on (10-01-2015) as  it was interfering in square_tmp5
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  square.clp 	square13   "  ?id " varga)" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_square4
(declare (salience 4600))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 meter|centimeter|milimeter|miles|kilometer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " square.clp   sub_samA_square4   "   ?id " varga)" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_square4
(declare (salience 4600))
(id-root ?id square)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 meter|centimeter|milimeter|miles|kilometer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " square.clp   obj_samA_square4   "   ?id " varga)" crlf))
)
