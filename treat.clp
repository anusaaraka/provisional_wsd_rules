;@@@ Added by 14anu03 on 02-july-14.
;They gave us treat at hotel.
;उन्होनें हमें होटल में दावत दी.
(defrule treat100
(declare (salience 5500))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id1 ?id)
(id-word ?id1 gave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvawa_xI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat100   "  ?id "  xAvawa_xI)" crlf)
)
)
(defrule treat0
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ananxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat0   "  ?id "  Ananxa )" crlf))
)

;"treat","N","1.Ananxa"
;The concert was a real treat for the music lovers.
;--"2.KarcA"
;This is her treat. Let's go.
;
(defrule treat1
(declare (salience 4900))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra_kara))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat1   "  ?id "  vyavahAra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  treat.clp     treat1   "  ?id " se )" crlf)
)
)

;@@@ Added by Prachi Rathore
;Mr. Barua summoned the garden doctor to[ treat] the injured gang-leader.[gyan-nidhi]
;-मि. बरूआ ने बागान के डाक्टर को घायल गेंग लीडर की चिकित्सा/इलाज करने के लिए बुलवाया।
(defrule treat2
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id2 injured)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IlAja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat2   "  ?id "  IlAja_kara )" crlf)
)
)


;@@@ Added by Prachi Rathore[12-3-14]
;In our discussions we shall treat the objects in motion as point objects.[ncert]
;इस अध्ययन में हम सभी गतिमान वस्तुओं को अतिसूक्ष्म मानकर बिन्दु रूप में निरूपित करेङ्गे ..
(defrule treat3
(declare (salience 5050))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(kriyA-as_saMbanXI  ?id ?)
(id-root ?id1 object)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirupiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat3   "  ?id "  nirupiwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  treat.clp     treat3   "  ?id " ko )" crlf)
)
)

;@@@ Added by Prachi Rathore[12-3-14]
;I decided to treat his remark as a joke. [oald]
;मैंने  उसकी टिप्पणी को मज़ाक  मानने का फैसला किया . 
(defrule treat4
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat4   "  ?id "  mAna )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  treat.clp     treat4   "  ?id " ko )" crlf)
)
)

;@@@ Added by Prachi Rathore[12-3-14]
;--"5.saMsAXiwa_karanA"
;Treat the crops with pesticide.
;She was treated for sunstroke.[oald]
;
(defrule treat5
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-with_saMbanXI  ?id ?)(kriyA-for_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upacAriwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat5   "  ?id "  upacAriwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  treat.clp     treat5   "  ?id " ko )" crlf)
)
)

;@@@ Added by Prachi Rathore[12-3-14]
;He treated his friends to an icecream.
;usane apane xoswoM ko AisakrIma KilAI.
(defrule treat6
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvawa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat6   "  ?id "  xAvawa_xe)" crlf)
)
)

;@@@ Added by Prachi Rathore[12-3-14]
;This is her treat. Let's go.
;yaha usakI xAvawa hE.calo caleM
(defrule treat7
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat7   "  ?id "  xAvawa)" crlf)
)
)



;@@@ Added by Prachi Rathore[27-3-14]
;The fabric has been treated to repel water.[oald]
;कपडा पानी हटाने के लिये संसाधित किया गया है. 
(defrule treat8
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(kriyA-object  ?id1 ?id2)
(id-root ?id2 water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMsAXiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat8   "  ?id "  saMsAXiwa_kara)" crlf)
)
)


;@@@ Added by 14anu-ban-07,25-09-14
;As a simple case of motion in a plane, we shall discuss motion with constant acceleration and treat in detail the projectile motion.(ncert corpus)
;किसी समतल में गति के सरल उदाहरण के रूप में हम एकसमान त्वरित गति का अध्ययन करेङ्गे तथा एक प्रक्षेप्य की गति के विषय में विस्तार से पढेङ्गे  पढ़ेंगे.
(defrule treat9
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat9   "  ?id "  paDa)" crlf)
)
)

;@@@ Added by 14anu23 24/06/2014
;When a large number of animals have to be treated , they should be driven slowly through a foot - bath containing some antiseptic solution . 
;जब बहुत जानवरों का इलाज करना हो तो उनके पांवों को धोने के लिए उन्हें ऐसे द्रव में से धीमे धीमे गुजारा जाना चाहिए जिसमें रोगाणुरोधक घोल मिला हो .
(defrule treat10
(declare (salience 4900))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat10   "  ?id "  ilAja_kara)" crlf)
)
)

;@@@ Added by 14anu20 on 03.07.2014.
;He treat crops with insecticides.
;वह  पैदावार के साथ कीटनाशक पदार्थ प्रयोग करता है . 
(defrule treat11
(declare (salience 4950))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 crop|wood|fruit|vegetable|eatable|food)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 insecticide|preservative|pesticide|medicine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat11   "  ?id "  pryayoga_kara )" crlf)
))

;@@@ Added by 14anu20 on 03.07.2014.
;The doctor treated four patients last week.
;डाक्टर ने पिछले सप्ताह चार मरीजों का उपचार किया . 
(defrule treat12
(declare (salience 4950))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 hospital|nurse|doctor|health_center)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upacAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat12   "  ?id "  upacAra_kara )" crlf)
))

;@@@ Added by 14anu20 on 03.07.2014.
;They treat workers like dirt.
;वे  कार्यकर्ताओं को निरादर करते हैं . 
(defrule treat13
(declare (salience 4990))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-like_saMbanXI  ?id ?id1)
(id-root ?id1 dirt)
(id-root ?id2 like)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 nirAxara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " treat.clp	treat13  "  ?id "  " ?id1 " " ?id2 "  nirAxara_kara  )" crlf))
)

;@@@Added by Gourav Sahni MNNIT Allahabad 14anu15 on 13/06/2014
;Glaucoma can be treated by eye drops , tablets and surgery.
;ग्लकोमा आँखों बूँदों से, गोलियों और शल्य चिकित्सा से इलाज किया जा सकता है . 
(defrule treat14
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja_kiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat14   "  ?id "  ilAja_kiyA)" crlf)
)
)


;@@@ Added by 14anu-ban-07,(18-02-2015)
;Slaves treated as chattel. [merriam-webster]
;सेवको से गुलाम की तरह व्यवहार किया गया. [manual]
(defrule treat15
(declare (salience 5100))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 chattel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra_kiyA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat15   "  ?id "  vyavahAra_kiyA_jA )" crlf)
)
)

;@@@ Added by 14anu-ban-07,(18-02-2015)
;The crowd were treated to a superb display of tennis.(oald)
;भीड का टेनिस के एक उत्कृष्ट प्रदर्शन से मनोरञ्जन किया गया था .  [manual]
(defrule treat16
(declare (salience 5100))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 display)
(kriyA-subject  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoraMjana_kara))
(assert (id-wsd_viBakwi ?id2 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat16   "  ?id "  manoraMjana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  treat.clp treat16  "  ?id2 " kA)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(18-02-2015)
;I'm going to treat myself to a new pair of shoes.(oald)
;मैं  स्वयं के लिए एक नये जोडे जूते खरीदने जा रहा हूँ . (manual)
(defrule treat17
(declare (salience 5200))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id1 ?id2)
(viSeRya-of_saMbanXI  ?id2 ?id3)
(id-root ?id3 shoe)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZarIxanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  treat.clp 	treat17   "  ?id "  KZarIxanA)" crlf)
)
)

;"treat","VI","1.vyavahAra_karanA"
;We have to treat animals in a human way .
;--"2.mAnanA"
;Krishna treated Sudama as his close friend.
;--"3.vivecana_karanA"
;The subject is treated vividly in the following chapter .
;--"4.cikiwsA_karanA"
;She was treated for Jaundice.
;--"5.saMsAXiwa_karanA"
;Treat the crops with pesticide.
;--"6.kA_Karca_karanA"
;She treated her friends to an icecream.
;
;LEVEL 
;Headword : treat
;
;Examples --
;"treat","VI","1.vyavahAra_karanA"
;She hates people who treat animals badly.
;vaha una logoM se naParawa karawI hE jo paSuoM se burA vyavahAra karawe hEM
;
;"2.mAnanA"<--ke jEsA vyavahAra karanA 
;Krishna treated Sudama as his close friend.
;kqRNa suxAmA ko apanA karIbI miwra mAnawe We
;
;"3.vivecana_karanA"<--viRaya ko spaRta karanA<--viRaya ke sAWa uciwa vyavahAra karanA
;The subject is treated vividly in the previous class .
;viRaya kA vivecana pUrva kakRA meM sajIvawA se kiyA gayA WA.
;                                                   
;"4.cikiwsA_karanA"<--upacAra karanA<-- upacAra ke liye uciwa vyavahAra karanA 
;Last year she was treated for typhoid.
;piCale varRa usakI cikiwsA tAiPAyada ke liye kI gayI WI.
;
;"5.upacAriwa karanA/saMsAXiwa_karanA"<--upacAra karanA
;Treat the crops with pesticide.
;Pasala ko kItANunASaka xavA se upacAriwa karo.
;
;"6.kA_Karca_karanA/KilAnA"<--KarcA uTAkara Ananxiwa honA yA karanA<--Ananxa_xene_ke_liye_vyavahAra karanA 
;He treated his friends to an icecream.
;usane apane xoswoM ko AisakrIma KilAI.
;
;"treat","N","1.Ananxa"
;The concert was a real treat for the music lovers.
;samAroha saMgIwa premiyoM ko vAswavika Ananxa xene vAlA WA.
;
;"2.xAvawa/KarcA"<--muJe xAvawa KilA kara Ananxa milegA
;This is her treat. Let's go.
;yaha usakI xAvawa hE.calo caleM
;                        
;       nota:--uparyukwa'treat'Sabxa ke saBI vAkyoM para gOra kareM wo kriyA Ora saMjFA              ke samaswa vAkyoM ke viBinna arWoM ko sUwra kI sahAyawA se jodZA
;            jA sakawA hE. 
;                       sUwra : vyavahAra`[>Ananxa`]
;
;Discussion on Feedback -
;
;purAnA sUwra - uciwa_vyavahAra[>Ananxa]   5/11/2001
;
;sUwra meM 'uciwa' kI mOjUxagI ke viruxXa uxAharaNa -
;
;  ill treated
;
;* rugNa uciwa_vyavahAra`[>Ananxa]_{ed/en}
;
;yaxi sUwra meM 'uciwa_vyavahAra_ rahA wo Upara xiye uxAharaNa jEsA Autaputa A
;sakawA hE.  'vyavahAra' apane Apa meM 'treat' kA bIjArWa xe sakawA hE. yaxi sUwra
;'vyavahAra`[>Ananxa]' ho wo Upara xiye aMgrejZI paxa kA Autaputa nimna AegA -
;
;  ill treated
;
; rugNa vyavahAra`[>Ananxa]_{ed/en}
;
;yAni 'anuciwa_vyavahAra'. awaH isa pramANa ke AXAra para sUwra meM se 'uciwa'
;Sabxa ko nikAla  sakawe hEM.
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_treat2
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 injured)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IlAja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " treat.clp   sub_samA_treat2   "   ?id " IlAja_kara )" crlf)
)
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_treat2
(declare (salience 5000))
(id-root ?id treat)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 injured)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IlAja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " treat.clp   obj_samA_treat2   "   ?id " IlAja_kara )" crlf)
)
)
