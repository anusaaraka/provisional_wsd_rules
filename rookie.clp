;@@@ Added by 14anu-ban-10 on (28-02-2015)
;The officer gave a difficult task to the rookie.[hinkhoj]
;अधिकारी ने अनाड़ी को एक कठिन काम दिया  . [manual]
(defrule rookie0
(declare (salience 100))
(id-root ?id rookie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anAdI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rookie.clp 	rookie0   "  ?id "  anAdI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-02-2015)
;The transition from rookie to fighter pilot starts with selection day. [oald]
;योद्धा विमान चालक को नौसिखिया से स्वर-सन्धान चयन दिन के साथ शुरु करता है.[manual] 
(defrule rookie1
(declare (salience 200))
(id-root ?id rookie)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOsiKiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rookie.clp 	rookie1   "  ?id "  nOsiKiyA)" crlf))
)

