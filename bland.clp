;@@@Added by 14anu-ban-02 (01-04-2015)
;The vegetable soup was rather bland.[mw]
;शाक सूप थोड़ा बेस्वाद था . [self]
(defrule bland1 
(declare (salience 100)) 
(id-root ?id bland) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 soup|chiken)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id besvAxa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bland.clp  bland1  "  ?id "  besvAxa )" crlf)) 
) 



;-------------------default_rules-------------------------


;@@@Added by 14anu-ban-02 (01-04-2015)
;Sentence: Pop music these days is so bland.[cald]
;Translation:आजकल का पॉप सङ्गीत  बहुत  नीरस है . [self]
(defrule bland0 
(declare (salience -1)) 
(id-root ?id bland) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nIrasa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bland.clp  bland0  "  ?id "  nIrasa )" crlf)) 
) 


