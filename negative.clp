
(defrule negative0
(declare (salience 5000))
(id-root ?id negative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakArAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  negative.clp 	negative0   "  ?id "  nakArAwmaka )" crlf))
)

;"negative","Adj","1.nakArAwmaka"
;He gave a negative reply.
;He is very negative in his approach.
;
(defrule negative1
(declare (salience 4900))
(id-root ?id negative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  negative.clp 	negative1   "  ?id "  nakAra )" crlf))
)

;"negative","N","1.nakAra"
;He spoke in the negative.
;--"2.nigetiva{Poto_kA}"
;I do not know where I have kept the negatives of these photographs.
;

;Added by sheetal(1-10-09).
(defrule negative2
(declare (salience 4950))
(id-root ?id negative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 comment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakArAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " negative.clp negative2 " ?id " nakArAwmaka )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (31-03-2016): restricted the rule
;### Counter example### He made negative comments to the press. 	[sd_verified]
;उसने प्रैस को नकारात्मक टिप्पणियाँ दीं.	[sd_verified]
;@@@ Added by 14anu-ban-10 on (20-8-2014)
;Another way of putting it is that mass comes only in one variety (there is no negative mass), but charge comes in two varieties: positive and negative charge.  [ncert corpus]
;isako isa prakAra BI kaha sakawe hEM ki xravyamAna kevala eka hI prakAra (qNAwmaka xravyamAna jEsA kuCa nahIM hE ) kA howA hE, jabaki AveSa xo prakAra ke howe hEM: XanAveSa waWA qNAveSa.
(defrule negative3
(declare (salience 5000))
(id-root ?id negative)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 charge)	;added by 14anu-ban-01
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id qNAveSa ))	;commented by 14anu-ban-01
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 qNAveSa))	;added by 14anu-ban-01
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " negative.clp negative3 " ?id " qNAveSa  )" crlf))	;commented by 14anu-ban-01
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  negative.clp  negative3  "  ?id "  " ?id1 "  qNAveSa  )" crlf))	;added by 14anu-ban-01
)

