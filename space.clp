
(defrule space0
(declare (salience 5000))
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id spacing )
(viSeRya-between_saMbanXI ?id ?);Added by Shirisha Manju Suggested by Chaitanya Sir (19-09-13)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aMwarAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  space.clp  	space0   "  ?id "  aMwarAla )" crlf))
)

;"spacing","N","1.aMwarAlana"
;There is a lot of space between sun && moon.
;



(defrule space1
(declare (salience 4900))
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id spaced )
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anwara_para_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " space.clp	space1  "  ?id "  " ?id1 "  anwara_para_raKa  )" crlf))
)




(defrule space3
(declare (salience 4700))
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwara_para_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space3   "  ?id "  anwara_para_raKa )" crlf))
)


;Added by Meena(29.5.10)
;And 21 could stand for the 21 cm radio frequency of hydrogen in space .
;Was it a message from some intelligent beings in outer space ?
;Added kriyA-through_saMbanXI by Shirisha Manju Suggested by Chaitanya Sir (19-09-13) 
;In communication using radio waves, an antenna at the transmitter radiates the Electromagnetic waves (em waves), which travel through the space and reach the receiving antenna at the other end.
(defrule space4
(declare (salience 4600))
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-in_saMbanXI  ?id1 ?id)(kriyA-in_saMbanXI ?id1  ?id)(kriyA-through_saMbanXI ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwarikRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp     space4   "  ?id "  aMwarikRa )" crlf))
)

;Salience reduced by Meena(29.5.10)
(defrule space5
(declare (salience 5200)) ;silience increased by 14anu-ban-10
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space5  "  ?id "  jagaha )" crlf))
)



;default_sense && category=noun	jagaha	0
;"space","N","1.jagaha"
;To play cricket we need a big space.
;--"2.anwarikRa"
;Niel Amstronge was the first person to go in to the space.
;


;Commented by 14anu-ban-01 on (08-01-2015) because there is no example for this sentencecorrect meaning will come from default rule space5
;@@@ Added by 14anu09
;
;(defrule space06
;(declare (salience 5000))
;(id-root ?id space)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id =(- ?id 1))
;(id-root =(- ?id 1) this|his|that|my|her|thier) remove the comment if it is overgeneralized
;(id-cat_coarse ?id noun )
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id jagaha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space06   "  ?id "  jagaha )" crlf))
;)


;Commented by 14anu-ban-01 on (08-01-2015) because correct meaning is coming from default rule space5
;@@@ Added by 14ban-anu-10 on (20-8-2014)
;In a turbulent flow the velocity of the fluids at any point in space varies rapidly and randomly with time.  
;vikRubXa pravAha meM kisI biMxu para jagaha kA vega xruwa waWA yAxqcCika rUpa se samaya meM baxalawA rahawA hE.
;(defrule space6
;(declare (salience 5400)) 
;(id-root ?id space)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-in_saMbanXI ?id1 ?id)
;(id-cat_coarse ?id noun )
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id jagaha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space6  "  ?id "  jagaha )" crlf))
;)


;@@@ Added by 14anu-ban-11 on (04-12-2014)
;It all happened in the space of ten minutes. (oald)
;यह सब दस मिनटों की अवधि में हुआ (manual)
(defrule space7
(declare (salience 5300)) 
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space7  "  ?id "  avaXi )" crlf))
)


;@@@ Added by 14anu-ban-11 on (05-12-2014)
;The first woman in space.(oald)
;अन्तरिक्ष में प्रथम स्त्री .(anusaaraka) 
(defrule space8
(declare (salience 5500)) 
(id-root ?id space)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun )
;(id-root ?id1 woman)    ;commented by 14anu-ban-11 on (29-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwarikRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  space.clp 	space8  "  ?id "  anwarikRa )" crlf))
)






