;@@@ Added by 14anu-ban-06 (03-04-2015)
;In the phrase "Leave him alone!", the verb "leave" is in the imperative form. (cambrideg)        [parser no.12]
;वाक्यांश में "उसको अकेला छोड़ो!", क्रिया "छोड़" आदेश सूचक रूप में है . (manual)
;An imperative sentence. (OALD)
;आदेश सूचक वाक्य . (manual)
(defrule imperative1
(declare (salience 2000))
(id-root ?id imperative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 sentence|form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa_sUcaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperative.clp 	imperative1   "  ?id "  AxeSa_sUcaka )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (03-04-2015)
;In the phrase "Leave him alone!", the verb "leave" is an imperative.(cambridge)
;वाक्यांश में "उसको अकेला छोड़ो!", क्रिया "छोड़" आदेशक क्रिया है . (manual)
(defrule imperative3
(declare (salience 2000))
(id-root ?id imperative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id2 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSaka_kriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperative.clp 	imperative3   "  ?id "  AxeSaka_kriyA )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-04-2015)
;In the phrase "Leave him alone!", the verb "leave" is in the imperative. (cambridge)
;वाक्यांश में "उसको अकेला छोड़ो!", क्रिया "छोड़" आदेशक रूप में है . (manual)
(defrule imperative4
(declare (salience 2100))
(id-root ?id imperative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSaka_rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperative.clp 	imperative4   "  ?id "  AxeSaka_rUpa )" crlf))
)
;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

;@@@ Added by 14anu-ban-06 (03-04-2015)
;It's imperative to act now before the problem gets really serious.(cambridge)
;अभी कार्य करना अनिवार्य है इससे पहले की समस्या वास्तव में गम्भीर हो जाए . (manual)
(defrule imperative0
(declare (salience 0))
(id-root ?id imperative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anivArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperative.clp 	imperative0   "  ?id "  anivArya )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-04-2015)
;Getting the unemployed back to work, said the mayor, is a moral imperative.(cambridge)
;महापौर कहते हैं,बेरोजगारो को वापिस काम पर लौटाना ,एक नैतिक अनिवार्यता है . (manual)
(defrule imperative2
(declare (salience 0))
(id-root ?id imperative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anivAryawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperative.clp 	imperative2   "  ?id "  anivAryawA )" crlf))
)
