;@@@ Added by 14anu-ban-03 (17-02-2015)
;Gill chortled with delight. [cald]
;गिल खुशी के साथ हँसा . [manual]
;She chortled with glee at the news. [cald]
;वह समाचार सुन कर  उल्लास के साथ हँसी . [manual]
(defrule chortle2
(declare (salience 100))
(id-root ?id chortle)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 delight|glee)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hazsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " chortle.clp  chortle2 " ?id "  hazsa )" crlf))
)

;------------------------------ Default Rules --------------------

;@@@ Added by 14anu-ban-03 (17-02-2015)
;She chortled at the news. [self: refrenced by cald] 
;वह समाचार सुन कर खुशी से हँसी . [manual]
(defrule chortle0
(declare (salience 00))
(id-root ?id chortle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuSI_se_hazsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " chortle.clp  chortle0 " ?id "  KuSI_se_hazsa )" crlf)) 
)

;@@@ Added by 14anu-ban-03 (17-02-2015)
;I heard a chortle at the back of the room. [cald]
;मैंने कमरे के पीछे धीमी हँसी की आवाज सुनी . [manual] 
(defrule chortle1
(declare (salience 00))
(id-root ?id chortle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XImI_hazsI_kI_AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " chortle.clp   chortle1 " ?id "  XImI_hazsI_kI_AvAja )" crlf)) 
)

