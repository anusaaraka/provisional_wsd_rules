
(defrule militant0
(declare (salience 5000))
(id-root ?id militant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkrAmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  militant.clp 	militant0   "  ?id "  AkrAmaka )" crlf))
)

;@@@ Added by 14anu07 on 2/06/2014
;The people took to the streets each time a militant was killed.
;जब कभी कोई उग्रवादी मारा जाता तब वे सडकों पर उतर आते थे .
(defrule militant2
(declare (salience 5100))
(id-root ?id militant)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) a|the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ugravAxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  militant.clp 	militant2   "  ?id "  ugravAxI )" crlf))
)


;"militant","Adj","1.AkrAmaka"
;The militant elements in the organization triggered the controvery.
;
(defrule militant1
(declare (salience 4900))
(id-root ?id militant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ugravAxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  militant.clp 	militant1   "  ?id "  ugravAxI )" crlf))
)

;"militant","N","1.ugravAxI"
;Ten militants were killed in police firing.
;
