;@@@ Added by 14anu-ban-09 Anshika Sharma (CS) 19-7-2014 Banasthali Vidyapeeth
;He belongs to a royal family.
;vaha SAhI parIvAra se saMbaMXa raKawA hE.
;Royal ladies used it for drinking water. [Parallel Corpus]
;SAhI mahilAyeM isa bAbafI kA upayoga pAnI pIne ke lie karawI WIM.
;This has also been summer royal palace once. [Parallel Corpus]
;kaBI yaha grIRma SAhI mahala BI rahA hE.
(defrule royal1
(declare (salience 4900))
(id-root ?id royal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  royal.clp 	royal1   "  ?id "  SAhI )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@ Added by 14anu-ban-09 on 21-7-2014
;There he sacrificed the royal robes and jewelleries.
;vahAz unhoMne rAjasI pariXAnoM Ora ABURaNM kA wyAga kara xiyA.
;Mysore Palace gives the feeling of royal wealth.
;mEsUra pElesa rAjasI vEBava kA ehasAsa xilAwa hE.
;You can relish the royal luxury at 'Oberoi Udaivilas'.
;Apa 'oborAya uxayavilAsa' meM rAjasI TAta-bAta kA AnaMxa gOrava kI jIMvawa misAla hE.
(defrule royal2
(declare (salience 5000))
(id-root ?id royal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 journey|lifestyle|luxury|kitchen|wealth|robe|pride) ;modified 'robes' as 'robe' by 14anu-ban-10 on (11-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjasI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  royal.clp 	royal2   "  ?id  "  rAjasI ) " crlf))
)

;@@@ Added by 14anu-ban-09 on 22-07-2014
;In this mascot with the royal look two actors riding an elephant have been shown greeting.
;isa SAhI aMxAja vAle isa prawIka cinha meM xo hAWIyoM para savAra nAyaka-nAyikA ko maMgalavAxana karawe hue xiKAyA gayA hE.
;Ajabgadh resident Raambabu used to work in Jaipur before this and now in this unique aman Bagh with royal style.
;rAjake ke SAhI aMxAja meM se kiwane logoM ne BUwoM ke BAnagaDa ki sEra kI hogI! xUra kyoM jAUz, jayapura vAloM meM se BI kuCeka ne hI SAyaxa usa ilAke meM rAwa biwAI hogI.

(defrule royal3
(declare (salience 6000))
(id-root ?id royal)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) style|look|way)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) SAhI_aMxAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " royal.clp  royal3  "  ?id "  " + ?id 1 "  SAhI_aMxAja  )" crlf))
)

(defrule royal0
(declare (salience 000))
(id-root ?id royal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  royal.clp 	royal0   "  ?id "  SAhI )" crlf))
)

;"royal","Adj","1.SAhI"
;He belongs to a royal family.
;Her uncle had a royal visit to America.
;His father is a member of royal society of arts.
;King of jorden got a royal welcome when he came to india.  
;
;Commented by 14anu-ban-09
;(defrule royal1
;(declare (salience 5000))
;(id-root ?id royal)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id SAhI_KAnaxAna_kA_vyakwi))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  royal.clp 	royal1   "  ?id "  SAhI_KAnaxAna_kA_vyakwi )" crlf))
;)

;"royal","N","1.SAhI_KAnaxAna_kA_vyakwi"
;The young royals enjoy life.
;
