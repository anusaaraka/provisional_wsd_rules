;##############################################################################
;#  Copyright (C) 2013-2014 Prachi Rathore (prachirathore02 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-06 (16-12-2014)
;$$$ Modified by 14anu24 ;added the condition (id-cat_coarse =(+ ?id 1) noun);and changed the meaning "alaga_alaga" to "prawyeka"
;@@@ Added by Prachi Rathore [22-1-14]
;When you look at a tree in the distance you cannot make out its individual leaves.
;जब आप दूर के किसी पेड़ को देखते हैं, तो आपको उसके अलग-अलग पत्ते दिखाई नहीं देते।
(defrule individual0	
(declare (salience 5500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse =(+ ?id 1) noun)
(viSeRya-viSeRaNa  =(+ ?id 1) ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga-alaga));meaning changed from 'prawyeka' to 'alaga-alaga' by 14anu-ban-06 (16-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual0   "  ?id "  alaga-alaga )" crlf))
)

;$$$ Modified by 14anu-ban-06 (16-12-2014)
;@@@ Added by 14anu24 Pratibha Verma (MNNIT) IT[9-06-2014]
;An individual sheep adds 0.5 to 0.7 tonne of mature to the soil every year.
;कृषि भूमि को प्रत्येक भेड से प्रतिवर्ष 0.5 से 0.7 मीट्रिक टन खाद मिलती है .
(defrule individual1 	
(declare (salience 5500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective) 
(id-cat_coarse =(+ ?id 1) noun) 
(viSeRya-viSeRaNa  =(+ ?id 1) ?id) 
(id-root =(+ ?id 1) sheep);added by 14anu-ban-06 (16-12-2014) to avoid clash with 'individual0'
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyeka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual1   "  ?id "  prawyeka )" crlf))
)

;@@@ Added by Prachi Rathore [22-1-14]
;Highly individual style of dress.[oald]
;लिबास की अत्यधिक विशेष शैली . 
(defrule individual2	
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 style|need)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual2   "  ?id "  viSeRa )" crlf))r
) 

;@@@ Added by Prachi Rathore[22-1-14]
;We deal with each case on an individual basis. [cambridge]
;हम व्यक्तिगत आधार पर हर एक मामले के साथ देते हैं . 
(defrule individual3	
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 basis)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwigawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual3   "  ?id "  vyakwigawa )" crlf))
)

;@@@ Added by Prachi Rathore[22-1-14]
; If nothing else, the school will turn her into an individual.[cambridge]
;यदि कुछ भी नही अन्य, तो विद्यालय एक विशिष्ट व्यक्ति में उसको बदल देगा . 
(defrule individual4	
(declare (salience 2500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-into_saMbanXI  ?id1 ?id)(kriyA-as_saMbanXI  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual4   "  ?id "  viSiRta_vyakwi )" crlf))
)

;;XXXXXXXXXXXXXXXXXXXXXXXXXXX DEFAULT RULE XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX;;
;@@@ Added by Prachi Rathore[22-1-14]
(defrule individual5	
(declare (salience 1500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwigawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual5   "  ?id "  vyakwigawa )" crlf))
)

;@@@ Added by Prachi Rathore[22-1-14]
;An odd-looking individual.[oald]
(defrule individual6	
(declare (salience 1500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual6   "  ?id "  vyakwi )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_individual3
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 style|need)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " individual.clp   sub_samA_individual3   "   ?id " viSeRa )" crlf))
) 

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_individual3
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 style|need)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " individual.clp   obj_samA_individual3   "   ?id " viSeRa )" crlf))
) 

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_individual4
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 basis)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwigawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " individual.clp   sub_samA_individual4   "   ?id " vyakwigawa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_individual4
(declare (salience 3000))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 basis)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwigawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " individual.clp   obj_samA_individual4   "   ?id " vyakwigawa )" crlf))
)

;@@@ Added by 14anu-ban-06  (18-08-14)
;For the system of n particles, the linear momentum of the system is defined to be the vector sum of all individual particles of the system. (NCERT)
;n कणों के इस निकाय का कुल रेखीय संवेग, एकल कणों के रेखीय संवेगों के सदिश योग के बराबर है .
;To get the total angular momentum of a system of particles about a given point we need to add vectorially the angular momenta of individual particles. (NCERT)
;कणों के किसी निकाय का, किसी दिए गए बिन्दु के परितः कुल कोणीय संवेग ज्ञात करने के लिए हमें एकल कणों के कोणीय संवेगों के सदिश योग की गणना करनी होगी.
(defrule individual7	
(declare (salience 5500))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)  
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 particle) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual7   "  ?id "  ekala )" crlf))
)

;@@@ Added by 14anu-ban-06  (17-10-2014)
;This is because, as explained earlier, it is reasonable to suppose that individual measurements are as likely to overestimate as to underestimate the true value of the quantity.(NCERT)
;क्योङ्कि जैसा पहले स्पष्ट किया जा चुका है कि यह मानना युक्तिसङ्गत है कि किसी राशि की व्यष्टिगत माप उस राशि के वास्तविक मान से उतनी ही अधिआकलित हो सकती है, जितनी उसके अवआकलित होने की सम्भावना होती है.(NCERT)
;Hence the rule: The relative error in a physical quantity raised to the power k is the k times the relative error in the individual quantity.(NCERT)
;अतः, नियम यह है: किसी भौतिक राशि जिस पर k घात चढाई गई है, की आपेक्षिक त्रुटि उस व्यष्टिगत राशि की आपेक्षिक त्रुटि की k गुनी होती है.(NCERT)
(defrule individual8	
(declare (salience 5550))
(id-root ?id individual)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 quantity|measurement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaRtigawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  individual.clp 	individual8   "  ?id "  vyaRtigawa )" crlf))
)
