;---------------------Default Rule-----------------------------------

;@@@ Added by 14anu-ban-09 on (30-07-2014)
;A classically trained singer. [OALD] 
;pAramparika praSikRiwa gAyaka. [Self] 
(defrule classically0
(declare (salience 0000))
(id-root ?id classically)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAramparika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  classically.clp 	classically0   "  ?id "  pAramparika )" crlf))
)

;--------------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on 30-7-14
;Her face is classically beautiful.
;usakA caharA prawiRTiwa suMxara hE.

(defrule classically1
(declare (salience 1000))
(id-root ?id classically)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-cat_coarse ?id1 adjective)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiRTiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  classically.clp 	classically1   "  ?id "  prawiRTiwa )" crlf))
)



