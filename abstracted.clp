;@@@Added by 14anu-ban-02(25-02-2015)
;Sentence: He gave her an abstracted glance, then returned to his book.[cambridge]
;Translation: उसने उसको  एक अन्यमनस्क रूप दिया और वापस अपनी किताब में लौट गया. [self]
(defrule abstracted0 
(declare (salience 0)) 
(id-root ?id abstracted) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anyamanaska)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abstracted.clp  abstracted0  "  ?id "  anyamanaska )" crlf)) 
) 

;@@@Added by 14anu-ban-02(25-02-2015)
;He was listening to music and had an abstracted expression.[oald]
;वह ध्यान से सङ्गीत सुन रहा था और खोया-खोया भाव लिये था . [self]
(defrule abstracted1 
(declare (salience 100)) 
(id-root ?id abstracted) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 expression)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KoyA-KoyA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abstracted.clp  abstracted1  "  ?id "  KoyA-KoyA )" crlf)) 
) 
