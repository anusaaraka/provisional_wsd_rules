;@@@ Added by 14anu-ban-04 (04-12-2014)
;He donated thousands of pounds to charity.     [oald]
;उसने हजारों पाउंड  धर्मार्थ संगठन  को दान किए ।                            [self]
(defrule donate0
(declare (salience 20))
(id-root ?id donate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  donate.clp 	donate0   "  ?id " xAna_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (04-12-2014)
;One 66 cm Cyclotron donated by the Rochester University of USA was commissioned in Panjab University, Chandigarh.          [NCERT-CORPUS]
;अमेरिका के रोशेस्टर विश्वविद्यालय द्वारा प्रदान किए गए 66 cm साइक्लोट्रॉन को पंजाब विश्वविद्यालय, चण्डीगढ में स्थापित किया गया.             [NCERT-CORPUS]
(defrule donate1
(declare (salience 30))
(id-root ?id donate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 Cyclotron) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  donate.clp 	donate1   "  ?id " praxAna_kara )" crlf))
)
