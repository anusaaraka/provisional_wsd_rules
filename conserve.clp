;@@@ Added by 14anu-ban-03 (10-04-2015)
;Help to conserve energy by insulating your home. [oald]
;अपने  घर  को रोधक के द्वारा ऊर्जा संरक्षित करने में सहायता करें।  [manual]
(defrule conserve2
(declare (salience 4900))
(id-root ?id conserve)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 energy)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMrakRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conserve.clp 	conserve2  "  ?id "  saMrakRiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (10-04-2015)
;Strawberry conserve. [cald]             ;working on parser no-27
;स्ट्रोबेरी जैम .  [manual]
(defrule conserve4
(declare (salience 5000))
(id-root ?id conserve)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jEma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conserve.clp 	conserve4   "  ?id "  jEma )" crlf))
)


;----------------------------------------------Default rules-------------------------------------------

(defrule conserve0
(declare (salience 00))           ;salience reduced by 14anu-ban-03 (10-04-2015)
(id-root ?id conserve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id murabbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conserve.clp 	conserve0   "  ?id "  murabbA )" crlf))
)

;"conserve","N","1.murabbA"
;I like the conserve of gooseberries.
;
(defrule conserve1
(declare (salience 00))              ;salience reduced by 14anu-ban-03 (10-04-2015)
(id-root ?id conserve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surakRiwa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conserve.clp 	conserve1   "  ?id "  surakRiwa_raKa )" crlf))
)

;"conserve","VT","1.surakRiwa_raKanA"
;Wild life should be conserved.
;


;@@@ Added by 14anu-ban-03 (09-10-2014)
;But all conserved quantities are not necessarily scalars. [ncert corpus]
;paranwu saMrakRiwa hone vAlI saBI rASiyAz axiSa hI hoM yaha AvaSyaka nahIM hE. [ncert corpus]
;The total linear momentum and the total angular momentum (both vectors) of an isolated system are also conserved quantities.  NOTE:(In this sentence parser is taking conserved as 'verb' but it is 'adjective') [ncert corpus]
;kisI viyukwa nikAya kA kula rEKika saMvega, waWA kula koNIya saMvega (xonoM saxiSa) xonoM BI saMrakRiwa rASiyAz hEM.[ncert corpus]
(defrule conserve3
(declare (salience 4900))
(id-root ?id conserve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMrakRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conserve.clp 	conserve3   "  ?id "  saMrakRiwa )" crlf))
)
