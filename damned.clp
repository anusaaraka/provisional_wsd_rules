;@@@Added by 14anu20 on 18/06/2014
;I am damned to hell for sin.
;मैं  पाप के लिए शापित हूँ.
(defrule damned0
(declare (salience 15))
(id-root ?id damned)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SApiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damned.clp 	damned0   "?id "  SApiwa )" crlf))
)

;@@@Added by 14anu20 on 18/06/2014.
;The people were damned.
;लोग आलोचित थें . 
(defrule damned1
(declare (salience 15))
(id-root ?id damned)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Alociwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damned.clp 	damned1   "?id "  Alociwa )" crlf))
)




