;@@@ Added by Preeti(16-12-13)
;The character of the village has changed since the road was built. 
;gAzva kI sWiwi baxala cukI hE kyoMki sadaka banAI gayI WI.
(defrule character1
(declare (salience 1050))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character1   "  ?id "  sWiwi )" crlf))
)

;@@@ Added by Preeti(16-12-13)
;Everyone admires her strength of character and determination. 
;saba usakI yogyAwA kI wAkawa Ora xqDza niScaya kI praSaMsA karawe hEM.
(defrule character2
(declare (salience 1100))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(and(viSeRya-of_saMbanXI  ? ?id2) (id-root ?id2 strength))
(and(kriyA-object  ?id1 ?id) (id-root ?id1 show)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yogyAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character2   "  ?id "  yogyAwA )" crlf))
)

;@@@ Added by Preeti(16-12-13)
;Clint Eastwood's character is the most sympathetic in the movie. 
;klinta estavuda kA cariwra calaciwra meM sabase aXika sahAnuBUwiSIla hE.
(defrule character3
(declare (salience 1150))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(or(id-cat_coarse ?id1 PropN)(id-cat_coarse ?id1 noun))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cariwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character3   "  ?id "  cariwra )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-11-2014)
;Using this fact a small scale laboratory model can be set up to study the character of fluid flow. [ncert]
;isa sawya kA upayoga karawe hue hama prayogaSAlA meM eka laGuswarIya moYdala sWApiwa karake warala pravAha ke aBilakRaNoM kA aXyayana kara sakawe hEM. [ncert]
(defrule character5
(declare (salience 1050))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 flow)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBilakRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp    character5   "  ?id " aBilakRaNa )" crlf))
)

;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 19.06.2014 email-id:sahni.gourav0123@gmail.com
;Generosity is part of the American character.
;उदारता अमरीकी चरित्र का भाग है . 
(defrule character6
(declare (salience 5000))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cariwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character6   "  ?id "  cariwra )" crlf))
)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 19.06.2014 email-id:sahni.gourav0123@gmail.com
;There were some really strange characters hanging around the bar. 
;शराबघर के चारों ओर आसपास मण्डराते हुए कुछ वास्तव में विचित्र अक्षर थे . 
(defrule character7
(declare (salience 5000))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-aBihiwa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id akRara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character7   "  ?id "  akRara )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-03-2015)
;Most of the Shakespearean characters wore cloaks. [cloak.clp]
;शेक्सपियर के पात्रों में से अधिकांश ने लबादे पहने थे। [manual]
(defrule character9
(declare (salience 5001))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 most)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character9   "  ?id "  pAwra )" crlf))
)


;#############################Defaults rule#######################################

;@@@ Added by Preeti(16-12-13)
;It is not in her character to be jealous. 
;IrRyAlu honA usake svaBAva meM nahIM hE.
(defrule character0
(declare (salience 1000))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svaBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character0   "  ?id "  svaBAva )" crlf))
)

;@@@ Added by 14anu26 [16-06-14]
;A line 30 characters long.
;एक लाइन 30 अङ्क लम्बा
(defrule character4
(declare (salience 1150))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp 	character4   "  ?id "  aMka)" crlf))
)

;Its outlook and interests were from the beginning all india in character .
;उसका दृष्टिकोण और हित प्रकृति में अखिल भारतीय आरम्भ से थे
;@@@Added by 14anu19 (18-06-14)
(defrule character8
(declare (salience 1000))
(id-root ?id character)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  character.clp        character8   "  ?id "  prakqwi )" crlf))
)
