;Rule is commented by 14anu-ban-03 (15-12-2014)  because it is unnecessary and firing from rule count1 that gives a correct meaning for this sentence.
;@@@ Added by 14anu04 on 30-June-2014
;He insisted on counting him out for the task.
;उसने कार्य में उसको शामिल न करने पर जोर दिया. 
;(defrule count_tmp3
;(declare (salience 4800))
;(id-root ?id count)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 2) out)
;(id-root =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nA_SAmila_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " count.clp 	count_tmp3  " ?id " nA_SAmila_kara )" crlf))
;)

;@@@ Added by 14anu04 on 30-June-2014
;He insisted on counting him in for the task.
;उसने कार्य में उसको शामिल करने पर जोर दिया. 
(defrule count_tmp2
(declare (salience 4800))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 2) in)
(id-root =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count_tmp2   "  ?id "  SAmila_kara )" crlf))
)

;Rule commented by 14anu-ban-03 (15-12-2014)  because it is unnecessary and firing from rule count2 that gives a correct meaning for this sentence.
;@@@ Added by 14anu04 on 30-June-2014
;I can always count on him.
;मैं उसपर  हमेशा भरोसा कर सकता हूँ . 
;(defrule count_tmp
;(declare (salience 4800))
;(id-root ?id count)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) on)
;(id-root =(+ ?id 2) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id BarosA_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count_tmp   "  ?id "  BarusA_kara )" crlf))
;)


(defrule count0
(declare (salience 5000))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SAmila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " count.clp	count0  "  ?id "  " ?id1 "  SAmila_kara  )" crlf))
)

;We can count her in for the party.
;hama use BI pArtI meM SAmila kara sakawe hEM
(defrule count1
(declare (salience 4900))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SAmila_na_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " count.clp	count1  "  ?id "  " ?id1 "  SAmila_na_kara  )" crlf))
)

;If you are going for party, you can count me out.
;yaxi wuma pArtI meM jA rahe ho wo muJe SAmila mawa karanA

;$$$ Modified by 14anu02 on 30.06.14
;counter example-You can count on Richard; he'll keep his word.(http://dictionary.reference.com)
;आप रिचार्ड पर भरोसा कर सकते हैं; वह अपना वचन निभायेगा . 
;You can count upon her for this work.
;आप इस कार्य के लिए उसके ऊपर भरोसा कर सकते हैं . 
(defrule count2
(declare (salience 4800))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) upon|on)	;added by 14anu02 since 'count on/upon' always has this meaning
;(id-word ?id1 upon)		;commented by 14anu02 
;(kriyA-upasarga ?id ?id1)	;commented by 14anu02 since the parser was not reconizing upon and on as particles here
;(kriyA-object ?id ?)		;commented by 14anu02 since this relation was not satisfied for any of the sentences above
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarosA_kara)) ;changed from 'wsd_group_root_mng' to 'wsd_root_mng' by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count2   "  ?id "  BarosA_kara )" crlf))
)

;You can count upon her for this work.
;wuma isa kAma ke lie usapara viSvAsa kara sakawe ho

;@@@ Added by 14anu-ban-03(18-8-2014)
;The smallest value that can be measured by the measuring instrument is called its least count. [NCERT-CORPUS]
;kisI mApaka yanwra xvArA mApA jA sakane vAlA Cote se CotA mAna usa mApaka yanwra kA alpawamAfka kahalAwA hE.
(defrule count5
(declare (salience 4800))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) least)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) alpawamAfka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  count.clp    count5  "  ?id "  " =(- ?id 1) "   alpawamAfka )" crlf))
)

;@@@Added by 14anu18 (26-06-14)
;He is the count of England.
;वह इंग्लैंड का काउन्ट है . 
(defrule count10
(declare (salience 4900))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAunta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count10   "  ?id "  kAunta)" crlf))
)

;----------------------- Default Rules --------------------------
(defrule count3
(declare (salience 4700))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ginawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count3   "  ?id "  ginawI )" crlf))
)

;default_sense && category=noun	gaNanA	0
;"count","N","1.gaNanA/ginawI"
;The food rich in fat will boost the calorie count.
;--"2.eka_badZA_kulIna_jana"
;His father is a count.
;--"3.aBiyoga_iljZAma"
;The judge said to the criminal,'The counts against you are many'. 
;
(defrule count4
(declare (salience 4600))
(id-root ?id count)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  count.clp 	count4   "  ?id "  gina )" crlf))
)

;"count","VT","1.ginanA"
;Can you count the books on your shelf?
;--"2.mAna_karanA"
;The academy counts several Nobel Prize winners among its members.
;--"3.BarosA_karanA"
;I can count on my friends during.
;
