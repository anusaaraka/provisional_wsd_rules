
;$$$ Modified by Anita-4.3.2014
;His response to the accusation was rapid. [cambridge dictionary]
;अभियोग को उसका उत्तर शीघ्र था । 
(defrule rapid1
(declare (salience 4900))
(id-root ?id rapid)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ? ?id) ;Added relation by Anita
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIGra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapid.clp 	rapid1   "  ?id "  SIGra )" crlf))
)

; rule commented out by 14anu-ban-10 on (12-12-2014) as root is rapidly, so to be handled in rapidly.clp. 
;@@@ Added by Anita--24.2.2014
;The world's resources are rapidly diminishing. [By mail]
;विश्व के संसाधन शीघ्रता से कम हो रहे हैं ।
;(defrule rapid2
;(declare (salience 4800))
;(id-root ?id rapid)
;(id-word ?id rapidly)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-kriyA_viSeRaNa  ? ?id)
;(id-cat_coarse ?id adverb)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id SIGrawA_se))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  rapid.clp 	rapid2   "  ?id "  SIGrawA_se )" crlf))
;)

;"rapid","Adj","1.SIGra"
;@@@ Added by Anita--24.2.2014
;There has been a rapid increase in the prices of gold & silver recently. [old clp. sentence]
;सोने और चांदी की कीमतों में त्वरित वृद्धि हुई है ।
(defrule rapid3
(declare (salience 4700))
(id-root ?id rapid)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wvariwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapid.clp 	rapid3   "  ?id "  wvariwa )" crlf))
)

;@@@ Added by 14anu20 datec 16/06/2014
;Rapid development was seen.
;तेज विकास देखा गया था . 

(defrule rapid4
(declare (salience 4700))
(id-root ?id rapid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapid.clp 	rapid4   "  ?id "  weja )" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-10-2014)
;By the middle of the twentieth century, science had become a truly international enterprise, with many cultures and countries contributing to its rapid growth.  [ncert corpus]
;bIsavIM SawAbxI ke maXya waka vijFAna, vAswavika rUpa meM, eka mahAna xruwa kArya bana gayA, jisake anwarrARtrIya vikAsa ke lie aneka saByawAoM evaM xeSoM ne apanA yogaxAna xiyA. [ncert corpus]
(defrule rapid5
(declare (salience 5000))
(id-root ?id rapid)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 growth)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  xruwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapid.clp 	rapid5   "  ?id "   xruwa )" crlf))
)
;################################default-rule#######################################

(defrule rapid0
(id-root ?id rapid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapid.clp 	rapid0   "  ?id "  wejZa )" crlf))
)

