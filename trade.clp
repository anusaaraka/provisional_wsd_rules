
(defrule trade0
(declare (salience 5000))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id trading )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lena-xena))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  trade.clp  	trade0   "  ?id "  lena-xena )" crlf))
)

;"trading","N","1.lena-xena"
;Trading is his profession.
;
;
(defrule trade1
(declare (salience 4900))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-in_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAnI_vaswu_xekara_nayI_vaswu_KarIxa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trade.clp trade1 " ?id "  purAnI_vaswu_xekara_nayI_vaswu_KarIxa )" crlf)) 
)

(defrule trade2
(declare (salience 4800))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 purAnI_vaswu_xekara_nayI_vaswu_KarIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trade.clp	trade2  "  ?id "  " ?id1 "  purAnI_vaswu_xekara_nayI_vaswu_KarIxa  )" crlf))
)

(defrule trade3
(declare (salience 4700))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciwa_lABa_uTA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trade.clp trade3 " ?id "  anuciwa_lABa_uTA )" crlf)) 
)

(defrule trade4
(declare (salience 4600))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciwa_lABa_uTA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trade.clp trade4 " ?id "  anuciwa_lABa_uTA )" crlf)) 
)

(defrule trade5
(declare (salience 4500))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciwa_lABa_uTA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " trade.clp trade5 " ?id "  anuciwa_lABa_uTA )" crlf)) 
)

(defrule trade6
(declare (salience 4400))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anuciwa_lABa_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trade.clp	trade6  "  ?id "  " ?id1 "  anuciwa_lABa_uTA  )" crlf))
)




;Added by Meena(20.5.11)
;At the end of the day, 251.2 million shares were traded.
(defrule trade7
(declare (salience 4300))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?subj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApAra_kara))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trade.clp     trade7   "  ?id "  vyApAra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  trade.clp      trade7   "  ?id " kA )" crlf))
)

;@@@ Added by 14anu07 Karishma Singh MNNIT Allahabad on 17/06/2014
(defrule trade10
(declare (salience 5000))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApAra_kara))
;(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trade.clp     trade10   "  ?id "  vyApAra_kara )" crlf))
)


;Salience reduced by Meena(20.5.11)
(defrule trade8
(declare (salience 0))
;(declare (salience 4300))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trade.clp 	trade8   "  ?id "  vyApAra_kara )" crlf))
)

;default_sense && category=verb	vyApAra_kara	0
;"trade","V","1.vyApAra_karanA"
;The businessmen have to trade with other countries.
;--"2.vaswu_vinimaya_karanA"
;He traded his stamp-collection for a coin-collection.
;
;

;@@@ Added by Prachi Rathore[10-2-14]
;They were attempting to trade off inflation against unemployment.[oald]
;वे बेरोजगारी के विरुद्ध मुद्रा स्फीति का समझोता करने का प्रयास कर रहे थे . 
(defrule trade9
(declare (salience 4800))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJOwA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trade.clp	trade9  "  ?id "  " ?id1 "  samaJOwA_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(11-03-2015)
;My car is costing me too much to run, so I'm going to trade down to a cheaper model. (cambridge)
;मेरी गाडी  को दौडने के लिए मुझे ज्यादा ही  कीमत चुकानी पड रही है, इसलिए मैं बेच कर नयी खरीदने जा रहा हूँ एक अधिक सस्ते नमूना( माडेल ) को  . [manual]
(defrule trade11
(declare (salience 4800))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 beca_kara_nayI_KarIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trade.clp	trade11  "  ?id "  " ?id1 "  beca_kara_nayI_KarIxa  )" crlf))
)

;@@@ Added by 14anu-ban-07,(11-03-2015)
;We're going to trade up to a larger house.(oald)
;हम बेच कर नयी खरीदने जा रहे हैं  एक अधिक विशाल घर  . (manual)
(defrule trade12
(declare (salience 4800))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 beca_kara_nayI_KarIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trade.clp	trade12  "  ?id "  " ?id1 "  beca_kara_nayI_KarIxa  )" crlf))
)

;@@@ Added by 14anu-ban-07,(11-03-2015)
;The two politicians didn't really discuss the issues, they just traded insults.(cambridge)
;दो राजनीतिज्ञों ने वास्तव में समस्याओं के विषय में  चर्चा नहीं की थी, वे केवल अपमान कर रहे थे. (manual)
(defrule trade13
(declare (salience 4800))
(id-root ?id trade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 insult)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " trade.clp	trade13  "  ?id "  kara_raha  )" crlf))
)


