
(defrule where0
(declare (salience 5000))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where0   "  ?id "  kahAz )" crlf))
)

(defrule where1
(declare (salience 4900))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) tell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where1   "  ?id "  kahAz )" crlf))
)

(defrule where2
(declare (salience 4800))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where2   "  ?id "  kahAz )" crlf))
)

;I did not know where to go.
(defrule where3
(declare (salience 4700))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where3   "  ?id "  kahAz )" crlf))
)

(defrule where4
(declare (salience 4600))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id wh_adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where4   "  ?id "  kahAz )" crlf))
)

;"where","Interro","1.kahAz"
;Where do you stay?.
;

;$$$ Modified, uncommented and splitted the rule by 14anu-ban-01 on (15-04-2016) because where6 is modified and now this rule is required
;The place where you are staying is very congested.
;जहाँ आप रह रहे हैं वह स्थान अत्यन्त सङ्कुलित है .[added by 14anu3]
;Commented this rule by 14anu-ban-11 on (09-12-2014).
;Note:-- Bcoz rule 'where6' is working.
;$$$ Modified by 14anu18 (1-07-14)  
;14anu18 (1-07-14) added condition (kriyA-kriyA_viSeRaNa ? ?id)
;Example : I don't prefer places where people smoke.	[this sentence will be handled in different rule]
;मैं वे स्थान पसन्द नहीं करता हूँ जहाँ लोग धूम्रपान करते है.
;Modified by sheetal(08-03-10)
;I will show you the house where I met your mother .
(defrule where5
(declare (salience 4500))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
;(or (kriyA-vAkya_viBakwi ?sub ?id)(viSeRya-jo_samAnAXikaraNa  ?sub ?id)(kriyA-kriyA_viSeRaNa ? ?id));added by sheetal	;14anu18 (1-07-14) added condition (kriyA-kriyA_viSeRaNa ? ?id)
(viSeRya-jo_samAnAXikaraNa  ? ?id)	;added by 14anu-ban-01
;(not (or (yes_no_question)(kriyA-from_saMbanXI  ?kri ?id)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAz));changed 'jisa_sWAna_para' as "jahAz" by sheetal
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp     where5   "  ?id "  jahAz )" crlf))
)

;"where","Rel Pron","1.jisa_sWAna_para"

;$$$ Modified by 14anu-ban-01 on (15-04-2016): changed  jahAz => kahAz again because the example given is isappropriate for the default rule.
;The place where you are staying is very congested.
;जहाँ आप रह रहे हैं वह स्थान अत्यन्त सङ्कुलित है .[added by 14anu3]
;$$$ Modified by 14anu03 .kahAz =>jahAz on 19-june-14
(defrule where6
(declare (salience 4400))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAz))	;changed jahAz => kahAz by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where6   "  ?id "  kahAz )" crlf))	;changed jahAz => kahAz by 14anu-ban-01
)
;Modified by Sukhada. jahAz => kahAz
;The most famous tamrind tree in India is in Gwaliar, where it stand near the ..


;Some progress is possibly being made in the UK, where researchers at Cambridge are studying the effectiveness of the desensitization technique.(agriculture)
;कुछ प्रगति सम्भवतः  यूके में बनाई जा रही है जहाँ  केंब्रिज  में  शोधक desensitization तकनीक के प्रभावकारिता का अध्ययन कर रहे हैं . (agriculture)
;A hypothesis of the development of peanut allergy has to do with the way peanuts are processed in North America versus other countries, such as Pakistan and China, where peanuts are widely eaten.(agriculture)
;$$$ Modified by 14anu-ban-11 on (30-10-2014) -- added 'ionise' to the list
;Electric force manifests itself in atmosphere where the atoms are ionised and that leads to lightning.(ncert) 
;vExyuwa bala svayaM vAwAvaraNa, jahAz paramANu AyanIkqwa howe hEM, meM prakata howA hE Ora isI ke kAraNa wadiwa xamakawI hE.
;$$$ Modified by 14anu-ban-06 Karanveer Kaur 23-7-14 -- meaning changed from 'jahAz_se' to 'jahAz'
;Nadala Basin , where the elephant research camp is located .   (Parallel Corpus)
;नडाला नदी क्षेत्र , जहाँ हाथी शोध कैंप स्थित है ।
;@@@ Added by Pramila(BU) on 21-02-2014
;After a whole hour of driving, we fetched up back where we started.        ;cald
;पूरे एक घंटे गाडी चलाने के बाद हम वापिस वहीं पहुँच गए जहाँ से हमने शुरू किया था.
(defrule where7
(declare (salience 4800))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id wh-adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 start|locate|ionise|eat|study) ; added 'locate' by 14anu-ban-06 23-7-14 ;added 'ionise' by 14anu-ban-11 on (30-10-2014)
;added 'eat' by 14anu-ban-11 on (07-11-2014) ; added 'study' by 14anu-ban-11 on (08-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where7   "  ?id "  jahAz)" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-10-2014)
;Where v is the instantaneous velocity when the force is F. (ncert)
;jahAz @V wAwkRaNika vega hE jabaki bala @F hE.(ncert)
(defrule where8
(declare (salience 5000))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id wh-adverb)
(viSeRya-viSeRaka ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where8   "  ?id "  jahAz)" crlf))
)

;@@@ Added by 14anu-ban-11 on (14-11-2014)
;If the secondary is an open circuit or the current taken from it is small, then to a good approximation Îµs = vs where vs is the voltage across the secondary. (ncert)
;यदि द्वितीयक कुण्डली के सिरे मुक्त हों अथवा इससे बहुत कम धारा ली जा रही हो तो पर्याप्त सन्निकट मान तक εs = vs यहाँ vs द्वितीयक कुण्डली के सिरों के बीच वोल्टता है.(ncert)
(defrule where9
(declare (salience 5000))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id wh-adverb)
(viSeRya-viSeRaka ?id1 ?id)
(id-root ?id1 voltage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp 	where9   "  ?id "  yahAz)" crlf))
)

;@@@ Added  by 14anu-ban-01 on (15-04-2016) 
;I don't prefer places where people smoke.	[sentence of rule where5]
;मैं वे स्थान पसन्द नहीं करता हूँ जहाँ लोग धूम्रपान करते है.
(defrule where10
(declare (salience 4500))
(id-root ?id where)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  where.clp     where10   "  ?id "  jahAz )" crlf))
)

