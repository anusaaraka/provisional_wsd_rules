
(defrule ruin0
(declare (salience 5000))
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp 	ruin0   "  ?id "  vinASa )" crlf))
)

;"ruin","N","1.vinASa"
(defrule ruin1
(declare (salience 4900))
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp 	ruin1   "  ?id "  naRta_kara )" crlf))
)

;"ruin","VT","1.naRta_kara"
;The crops were ruined by the delayed monsoon.            
;If she loses the interest in job it will ruin her.
;

;Added by sheetal(3-12-09).
(defrule ruin2
(declare (salience 4950))
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 lecture)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIrasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin2   "  ?id "  nIrasa_kara )" crlf))
)
;The sleeping of students can ruin a lecture .

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 27/6/2014****
;His life was ruined.
;उसका जीवन बर्बाद हो गया था . 
(defrule ruin03
(declare (salience 5000))
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 he|life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barbAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp 	ruin03   "  ?id "  barbAxa )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (09-12-2014)
;@@@ Added by 14anu-ban-02 Shivani Pathak Banasthali(23-07-14).
;We visited the ruins of a Norman castle .(oald)
;hama nAzramana xurga ke WvaMsAvaSeRa xeKane gaye.
;Ruins of old temples and forts can also be seen in Sariska National Park  .(corpora)
;सरिस्का  राष्ट्रीय  उद्यान  में  पुराने  मंदिरों  और  किलों  के  भग्नावशेष  भी  देखे  जा  सकते  हैं  ।
;Two bodies were found among the charred ruins of the house .(corpora)
;Gara ke Julase huye WvaMsAvaSeRa meM xo Sava mile .
;In 1638 AD , Mughal Emperor Shahjahan had taken the decision of changing his headquarters from Agra and settling a new capital near the old ruins of Firozabad beside the Yamuna river  .(corpora)
;सन्  1638  में  मुगल  बादशाह  शाहजहाँ  ने  अपना  मुख्यालय  आगरा  से  बदलने  और  दिल्ली  में  यमुना  किनारे  फिरोज़ाबाद  के  पुराने  खंडहरों  के  पास  नया  राजधानी  शहर  बसाने  का  निर्णय  लिया  था  ।
(defrule ruin3
(declare (salience 5000))
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 of) ;commented out by 14anu-ban-10 on (09-12-2014)
;(kriyA-upsarga ?id ?id1);commented out by 14anu-ban-10 on (09-12-2014)
(or(viSeRya-viSeRaNa  ?id ? )(viSeRya-det_viSeRaNa  ?id )(viSeRya-of_saMbanXI  ?id ? )) ;added by 14anu-ban-10 on (09-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WvaMsAvaSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin3   "  ?id "  WvaMsAvaSeRa )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (09-12-2014)
;@@@ Added by 14anu-ban-02 Shivani Pathak Banasthali(24-07-14).
;The old mill is now little more than a ruin.(oald)
;purAnA kAraKAna aba eka KaMdahara se aXika kuCa nahI he .[manual]
(defrule ruin4
(declare (salience 5100)) ;salience increased by 14anu-ban-10 on (09-12-2014)
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ?id ? ) ;added by 14anu-ban-10 on (09-12-2014)
;(subject-subject_samAnAXikaraNa ?id ?id1) ;commented by 14anu-ban-10 on (09-12-2014)
;(id-root ?id1 mill)  ;commented by 14anu-ban-10 on (09-12-2014)
;(id-word ?id1 of)
;(kriyA-upsarga ?id ?id1)
(id-cat_coarse ?id noun) ;uncommented by 14anu-ban-10 on (09-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaMdahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin4   "  ?id "  KaMdahara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-01-2015)
;Situated south - east of Patna the ruins of Nalanda appear to be narrating the tale of its glorious past even today .[tourism corpus]
;पटना  के  दक्षिणपूर्व  में  स्थित  नालंदा  के  खंडहर  आज  भी  अपने  गौरवशाली  इतिहास  की  कहानी  बताते  प्रतीत  होते  हैं  ।[tourism corpus]
(defrule ruin5
(declare (salience 5200)) 
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(id-root ?id1  narrate) 
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaMdahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin5   "  ?id "  KaMdahara )" crlf))
)

;@@@ Added by  by 14anu-ban-10 on (29-01-2015)
;The drainage system of the university complex was of first class , even today rain water does not log in the ruins of the buildings .[tourism corpus]
;विश्वविद्यायलय  परिषर  की  जलनिकासी  व्यवस्था  अव्वल  दर्जे  की  थी  ,  आज  भी  भवनों  के  खंडहरों  में  बारिश  का  पानी  जमा  नहीं  होता  है  ।[tourism corpus]
(defrule ruin7
(declare (salience 5400)) 
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1 )
(id-root ?id1   building)
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaMdahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin7   "  ?id "  KaMdahara )" crlf))
)
;@@@ Added by  by 14anu-ban-10 on (29-01-2015)
;In ancient times on the northern peak of Kausani the  Kartikeypur state was settled the ruins of which are seen even today .[tourism corpus]
;प्राचीन  समय  में  कौसानी  की  उत्तरी  चोटी  पर  कार्तिकेयपुर  राज्य  बसा  था  ,  जिसके  अवशेष  आज  भी  दिखाई  देते  हैं  ।[tourism corpus]
(defrule ruin8
(declare (salience 5500)) 
(id-root ?id ruin)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1   see)
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruin.clp      ruin8   "  ?id "  avaSeRa)" crlf))
)


