;$$$ Modified by by 14anu-ban-06 (16-02-2015)
;War has left its imprint on the strained faces of these people.(cambridge)
;युद्ध ने इन लोगों के तनावपूर्ण चेहरो पर अपनी छाप छोडी . (manual)
(defrule imprint0
(declare (salience 5000))
(id-root ?id imprint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id);added by 14anu-ban-06 (16-02-2015)
(kriyA-subject ?id1 ?id2);added by 14anu-ban-06 (16-02-2015)
(id-root ?id2 war|fight|tragedy);added by 14anu-ban-06 (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imprint.clp 	imprint0   "  ?id "  CApa )" crlf))
)

;"imprint","N","1.CApa"
;The tragedy left a lasting imprint on the community.
;

;@@@ Added by by 14anu-ban-06 (16-02-2015)
;Their footprints were imprinted in the snow. (OALD)
;उनके पैरो के निशान बरफ में छप गये थे . (manual)
(defrule imprint3
(declare (salience 5000))
(id-root ?id imprint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 footprint)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Capa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imprint.clp 	imprint3   "  ?id "  Capa_jA )" crlf))
)
;-------------------------- Default Rules -------------------
;@@@ Added by 14anu-ban-06 (04-02-2015)
;The button had left an imprint on my arm. (cambridge)
;बटन ने मेरी बाजु पर निशान छोड दिया. (manual)
;The guide on the jeep and the driver predict the location of the tiger by these danger signals and the paws imprints on the tracks .(parallel corpus)
;जीप  में  चढ़े  गाइड  और  ड्राइवर  इन्हीं  चेतावनी  संकेतों  और  ट्रैक  पर पंजों  के निशानो से  बाघ  की  स्थिति  का  अनुमान  लगाते  हैं  ।(parallel corpus)
(defrule imprint2
(declare (salience 0));salience reduced to '0' from '5100' by 14anu-ban-06 (16-02-2015)
(id-root ?id imprint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-on_saMbanXI ?id ?id1);commented to make this as default rule by 14anu-ban-06 (16-02-2015)
;(id-root ?id1 arm|hand|neck|track);commented to make this as default rule by 14anu-ban-06 (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imprint.clp 	imprint2   "  ?id "  niSAna )" crlf))
)

;$$$ Modified by by 14anu-ban-06 (16-02-2015)
;The terrible scenes were indelibly imprinted on his mind.(OALD)
;भयानक दृश्य स्थाई रूप से उसके मन में बैठ गये थे . (manual)
;The terrible scenes were indelibly imprinted on his memory.(imprint.clp)
;भयानक दृश्य स्थाई रूप से उसकी स्मृति में बैठ गये थे . (manual)
(defrule imprint1
(declare (salience 4900))
(id-root ?id imprint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bETa_jA));meaning changed from 'mana_meM_bETA_xe' to 'bETa_jA' by 14anu-ban-06 (16-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imprint.clp 	imprint1   "  ?id "  bETa_jA )" crlf))
)

;"imprint","V","1.mana_meM_bETA_xenA"
;The terrible scenes were indelibly imprinted on his memory.
;

