;@@@ Added by 14anu07 on 21/06/2014
(defrule circle2
(declare (salience 5000))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 work|live)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maNdalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle2   "  ?id "  maNdalI)" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;He is well known in theatrical circles. [Oxford Advanced Learner's Dictionary]
;vaha nAtakIya maNdalI_yA samuxAyoM meM prasiXxa hE.
(defrule circle02
(declare (salience 4600))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa  ?id ?id1) (samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1))
(id-root ?id1 friend|banking|medical|theatrical|dark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMdalI_yA_samuxAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle02   "  ?id "  maMdalI_yA_samuxAya)" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;She has a large circle of friends. [merriam-webster.com]
;usakI  eka bahuwa badZI miwra maNdalI hE.
(defrule circle03
(declare (salience 1000))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 friend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMdalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle03   "  ?id "  maMdalI )" crlf))
)

;@@@ Added by 14anu07 on 21/06/2014
(defrule circle3
(declare (salience 4500))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GerA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle3   "  ?id "  GerA)" crlf))
)

;@@@ Added by Preeti(31-7-14)
;We formed a circle around the campfire. [merriam-webster.com]
;hamane kEMpaPAyara ke cAroM ora GerA banAyA.
(defrule circle05
(declare (salience 4600))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GerA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle05   "  ?id "  GerA )" crlf))
)


;@@@ Added by 14anu07 on 21/06/2014
(defrule circle4
(declare (salience 4500))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqwwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle4   "  ?id "  vqwwa)" crlf))
)

;@@@ Added by 14anu-ban-03 (17-02-2015)
;He found that the alignment of the needle is tangential to an imaginary circle which has the straight wire as its center and has its plane perpendicular to the wire. [ncert]
;unhoMne pAyA ki cumbakIya suI wAra ke aBilambavawa wala meM wAra kI sWiwi ke keMxrawaH vqwwa kI sparSa reKA ke samAnwara saMreKiwa howI hE.[ncert]
;Iron filings sprinkled around the wire arrange themselves in concentric circles with the wire as the center.[ncert]
;wAra ke cAroM ora yaxi lOha cUrNa CidakeM wo isake kaNa wAra ke cAroM ora safkeMxrI vqwwoM meM vyavasWiwa ho jAwe hEM.[ncert]
(defrule circle5
(declare (salience 4500))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqwwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle5   "  ?id "  vqwwa)" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;We had seats in the circle. [Oxford Advanced Learner's Dictionary]
;hamArI kursiyAz vqwwAkAra meM lagI huI We.
(defrule circle6
(declare (salience 1000))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id1 seat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqwwAkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle6   "  ?id "  vqwwAkAra )" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;She circled the correct answer. [merriam-webster.com]
;usane sahI uwwara para  golA banAyA..
(defrule circle7
(declare (salience 4600))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject  ?id ?id1)(kriyA-object  ?id ?id1))
(id-root ?id1 mistake|answer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golA_banA))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle7   "  ?id "  golA_banA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  circle.clp  circle7   "  ?id " para )" crlf)
)

;------------------------ Default Rules ----------------------

;$$$ Modified by 14anu07 on 21/06/2014
(defrule circle0
(declare (salience 4000));salience reduced by 14anu07
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maNdalI)) ;changed mng from 'vqwwa' to 'maNdalI' by 14anu07
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle0   "  ?id "  maNdalI)" crlf))
)

(defrule circle1
(declare (salience 4900))
(id-root ?id circle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara_lagA)); Modified cakKara_lagA to cakkara_lagA by Roja. Suggested by Chaitanya Sir (20-11-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  circle.clp 	circle1   "  ?id "  cakkara_lagA )" crlf))
)

;default_sense && category=verb	cakkara_mAra	0
;"circle","V","1.cakkara_mAranA"
;The aircraft circled the airport before landing.
;--"2.GeranA"
;The security forces circled the enemy camp.
;
;LEVEL 
;
;
;Headword : circle
;
;Examples --
;
;"circle","N"
;--1.GerA
;School children were standing in a circle.
;skUla ke bacce eka Gere meM KadZe We.
;
;--2.maMdalI
;I worked for seven years in that circle.
;mEneM usa maMdalI meM sAwa sAla kAma kiyA WA.
;Your friends circle is very large.
;wumhArI miwra maMdalI bahuwa badZI hEM.
;
;--3.vqwa 
;Use your compasses to draw a circle.
;vqwa AkAra KIcaneM ke lie apane kampAsa kA iswemAla karo.
;
;
;"circle","V"
;
;--1.cakkara lagAnA
;I circled her home twice, but I didn't meet her.
;mEneM xo bAra usake Gara ke cakkara lagAye Pira BI usase mila nahI pAyI.
;
;--2.parikramA karanA
;The moon circles the earth every 28 days.
;aTAisa xina caMxramA XarawI kI parikramA karawA rahawA hEM.
;
;--3.GeranA
;Himalayas are circled by hills.
;himAlaya parvawoM se Gire hue hE. 
;
;yahAz para "circle"Sabxa ke kaI arWa nikala rahe hE,paranwu ina saba me eka bAwa
;sAmAne AwI hEki yahAz eka Ese Sabxa kA prayoga ho sakawA hE, jo sabameM samAna
;hEM.yahAz isa Sabxa kelie AkAra kA arWa sAmane AwA hEM.isalie yahAz sUwra banegA
;
;sUwra : vqwwa`
;
;
;
