
(defrule hull0
(declare (salience 5000))
(id-root ?id hull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAjZa_kA_DAzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hull.clp 	hull0   "  ?id "  jahAjZa_kA_DAzcA )" crlf))
)

;"hull","N","1.jahAjZa_kA_DAzcA"
;tEtAnika jahAja kA DAzcA samuxra meM dUba gayA.
;
(defrule hull1
(declare (salience 4900))
(id-root ?id hull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CilakA_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hull.clp 	hull1   "  ?id "  CilakA_uwAra )" crlf))
)

;"hull","V","1.CilakA_uwAranA"
;matara ko 'hull' karake raKanA hE.
;
;$$$ Modified by 14anu-ban-06 (21-11-2014)
;@@@ Added by 14anu24 [17-6-14]
;The materials commonly used for the purpose are rice husk , groundnut or cottonseed hulls , wheat bhoosa , finely broken corn cobs , rice strw;,sugar - cane fibres , saw dust and wood shavings .
;इस उद्देश्य से प्राय : चावल की भूसी , मूंगफली अथवा तिलहन की खली , गेहूं का भूसा , बारीक पिसी अनाज की गुल्ली , चावल के तिनके , गन्ने के तन्तु , लकडी का बुरादा तथा लकडी की छोटी छोटी कतरनें इस्तेमाल की जाती हैं .
(defrule hull2
(declare (salience 5000))
(id-root ?id hull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun);Modified category from verb to noun commented by 14anu-ban-06 (21-11-2014)
(id-root ?id1 cottenseed|groundnut)  ;added 'groundnut' by 14anu-ban-06 (21-11-2014)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hull.clp     hull2   "  ?id "  KalI )" crlf))
)

