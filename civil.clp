;commented by Preeti(31-07-2014)
;(defrule civil0
;(declare (salience 5000))
;(id-root ?id civil)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nAgarIka))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  civil.clp 	civil0   "  ?id "  ;nAgarIka )" crlf))
;)

;@@@ Added by Preeti(31-07-2014)
;She was barely civil to me. [merriam-webster.com]
;vaha mere sAwA muSkila se SiRta WI.
(defrule civil2
(declare (salience 1000))
(id-root ?id civil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  civil.clp 	civil2   "  ?id "  SiRta )" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;He and his ex-wife can not even have a civil conversation. [Cambridge Learner’s Dictionary]
;usake Ora usakI pUrva pawnI eka SiRta vArwAlApa BI nahIM  kara sakawe hEM.
(defrule civil3
(declare (salience 1050))
(id-root ?id civil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 conversation|relationship|behaviour|tone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  civil.clp 	civil3   "  ?id "  SiRta )" crlf))
)

;------------------------ Default Rules ----------------------

;"civil","Adj","1.nAgarika"
;The country faced civil disorder during partition.
(defrule civil1
(declare (salience 900)) ;salience reduced from 4900 to 900 by Preeti(31-07-2014)
(id-root ?id civil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAgarika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  civil.clp 	civil1   "  ?id "  nAgarika )" crlf))
)


;"civil","Adj","1.nAgarika"
;The country faced civil disorder during partition.
;--"2.saBya"
;You should have a civil behaviour in public.
;--"3.xIvAnI{kAnUna_saMbaMXI}"
;He is a civil lawyer,not a criminal lawyer.
;--"4.asEnika"
;The civil government of the country was overthrown in a military coup.
;
;
