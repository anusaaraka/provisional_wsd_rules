
;@@@ Added by 14anu-ban-03 (03-03-2015)
;Most of the Shakespearean characters wore cloaks. [same clp]
;शेक्सपियर के पात्रों में से अधिकांश ने लबादे पहने थे।  [manual]
(defrule cloak2
(declare (salience 5000))
(id-root ?id cloak)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 wear)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id labAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cloak.clp 	cloak2   "  ?id "  labAxA )" crlf))
)


;@@@ Added by 14anu-ban-03 (03-03-2015)
;The meeting was cloaked in mystery. [oald]
;बैठक रहस्य से आच्छादित थी . [manual]
(defrule cloak3
(declare (salience 5000))  
(id-root ?id cloak)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 mystery)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AcCAxiwa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cloak.clp 	cloak3   "  ?id " AcCAxiwa )" crlf))
)


;-------------------------------Default Rules----------------------------------
;$$$ Modified by 14anu-ban-03 (03-03-2015)
;The author prefers to hide behind a cloak of anonymity.[oald]
;लेखक अनामिकता की आड के पीछे छिपना पसन्द करते है . [manual]
(defrule cloak0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (03-03-2015)
(id-root ?id cloak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AdaZ))  ;meaning changed from 'labAxA' to 'AdaZ' by 14anu-ban-03 (03-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cloak.clp 	cloak0   "  ?id "  AdaZ )" crlf))
)

;"cloak","N","1.labAxA"
;Most of the Shakespearean characters wore cloaks.
;

;$$$ Modified by 14anu-ban-03 (03-03-2015)
;The hills were cloaked in thick mist. [oald]
;पहाडियाँ घनी धुन्ध में ढकी हुई थीं . [manual]
(defrule cloak1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (03-03-2015)
(id-root ?id cloak)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DakA_ho))  ;meaning changed from 'DakA_huA_ho' to 'DakA_ho' by 14anu-ban-03 (03-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cloak.clp 	cloak1   "  ?id "  DakA_ho )" crlf))
)

;"cloak","V","1.DakA_huA_honA"
;The investigation is cloaked in mystery.
;
