;##############################################################################
;#  Copyright (C) 2014-2015 Komal Singhal (komalsinghal1744@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Anita - 12-05-2014
;First rub the baking tray well with butter. [cambridge dictionary] [tam-problem]
;सबसे पहले मक्खन से बेकिंग ट्रे को अच्छी तरह से चिकना कर लें .
(defrule rub_butter
(declare (salience 8000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 butter)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikanA_kara_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub_butter  "  ?id "  cikanA_kara_le )" crlf))
)

;@@@ Added by Anita - 14-05-2014
;The chair legs have rubbed holes in the carpet.
;कुर्सी के पाओं ने कालीन में छेद बना दिए ।
(defrule rub003
(declare (salience 2000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 carpet)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub003  "  ?id "  banA_xe )" crlf))
)


;@@@ Added by Anita - 14-05-2014
;She rubbed her hair dry quickly with a towel. [oxford learner's dictionary]
;उसने जल्दी से अपने बाल सुखाने के लिए तौलिया से पोंछे ।
(defrule rub06
(declare (salience 3500))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 dry)
(kriyA-vAkyakarma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id poMCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub06  "  ?id "  poMCa )" crlf))
)

;@@@ Added by Anita - 14-05-2014
;In some cultures, people traditionally greet each other by rubbing noses. [oxford learner's dictionary]
;कुछ संस्कृतियों में, पारंपरिक रूप से लोग एक दूसरे से नाक रगड़ कर अभिवादन करते हैं ।
(defrule rub09
(declare (salience 5000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 nose)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ragadXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  rub.clp     rub09  "  ?id "  ragadXa_kara )" crlf))
)

;@@@ Added by Anita - 14-05-2014
;He gave her hair a good rub to dry it. [oxford learner's dictionary]
;उसने उसके बालों को सुखाने के लिए अच्छी मालिश की ।
;She gave her knee a quick rub. 
;उसने अपने घुटने की फटाफट मालिश की ।
(defrule rub07
(declare (salience 4000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAliSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub07  "  ?id "  mAliSa )" crlf))
)

;@@@ Added by Anita - 14-05-2014
;She rubbed the lotion into her skin. [oxford learner's dictionary]
;उसने अपनी त्वचा पर लोशन लगाया ।
;We rubbed some polish into the surface of the wood. [cambridge dictionary]
;हमने लकड़ी की सतह पर थोड़ी पॉलिश लगाई ।
;Rub salt over the fish before cooking. [oxford learner's dictionary]
;मच्छली को पकाने से पहले उस पर नमक लगाइए ।
(defrule rub010
(declare (salience 5350))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-into_saMbanXI  ?id ?)(kriyA-over_saMbanXI  ?id ?sam))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub010  "  ?id "  laga )" crlf))
)

;@@@ Added by Anita - 15-05-2014
;The wheel is rubbing on the mudguard. [oxford learner's dictionary]
;पहिया मडगार्ड पर रगड़ रहा है ।
(defrule rub10
(declare (salience 4500))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ragadZa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub10  "  ?id "  ragadZa_raha )" crlf))
)


;@@@ Added by 14anu-ban-01 on (04-08-2014)
;246245:Keep the hands of the child clean and stop them from rubbing their eyes .[Karan Singla]
;बच्चे का हाथ साफ रखें और उन्हें आँखों को मलने से रोकें ।[improvised manually:changed masala to mala]
;290535:Prevent rubbing eyes to keep the eyes fresh .[Karan Singla] {parsing problem:parser is tagging 'rubbing' as an adjective}
;आँखों को तरोताजा बनाए रखने के लिए आँखों को मलने से बचें ।[improvised manually:changed masala to mala]
;290661:When there is a poisonous insect bite poison gets removed with decrease in pain by rubbing lemon juice on that spot .[Karan Singla]
;जहरीले कीड़े के काटने पर नींबू का रस उस स्थान पर मलने से जहर दूर होकर दर्द भी कम हो जाता है । [Karan Singla]

(defrule rub001
(declare (salience 3000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 eye|hand|lemon|medicine)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp 	rub001   "  ?id "  mala)" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-08-2014)
;286995:Toothache ends on rubbing teeth on grinding Indian madder Rubia cordifolia .[Karan Singla]
;मंजीठ को पीसकर मंजन करने से दाँतों की पीड़ा दूर हो जाती है ।[Karan Singla]
(defrule rub02
(declare (salience 3000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 tooth)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maFjana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp 	rub02   "  ?id "  maFjana_kara)" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-08-2014)
;97974:He sat down beside her and rubbed his dry face with his hand .[Karan Singla]
;वह उसके निकट बैठ गया , और अपने सूखे चेहरे पर हाथ फेरने लगा ।[Karan Singla]
;94224:He would get up and light a cigarette uneasily , rubbing his face .[Karan Singla]
;वह उठ खड़ा होता , बेचैनी से सिगरेट सुलगाकर अपने चेहरे पर हाथ फेरने लगता ।[Karan Singla]

(defrule rub03
(declare (salience 3000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 face|hair)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAWa_Pera))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp 	rub03   "  ?id "  hAWa_Pera)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-08-2014)
;While the right hand beats the rhythm with a straight stick ,the left hand rubs the membrane on the other side with curved twig .[Karan Singla]
;दायें हाथ से जहां सीधी छड़ी से तान दी जाती है वहीं बायीं ओर एक मुड़ी हुई डंडी से इसे रगड़ा जाता है .[Karan Singla]
(defrule rub04
(declare (salience 4900))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 membrane)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ragada ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp 	rub04   "  ?id "  ragada )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@ added by 14anu22
;She rubbed out everything
;उसने सब मिटा दिया.
(defrule rub2
(declare (salience 4200)) ;salience incresed by 14anu-ban-10 on (11-12-2014)
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mitA_xe)) ;meaning changed from mitA to mitA_xe by 14anu-ban-10 on (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp	rub2  "  ?id "  " ?id1 "  mitA_xe )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@ added by 14anu22
;Dont rub off your mistakes on others.
;अपनी गलती दूसरों पे मत मढ़ो.
;अपनी गलती दूसरों पे मत डालो. ;translation modified by 14anu-ban-10 on (11-12-2014) 
(defrule rub3
(declare (salience 4300));salience incresed by 14anu-ban-10 on (11-12-2014)
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off) ; Removed 'out' by 14anu-ban-10 on (11-12-2014)
(kriyA-upasarga  ?id ?id1)
;(id-word =(+ ?id1 1) on) ;commented out by 14anu-ban-10 on (11-12-2014
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  dAlo)) ;meaning changed from maDZa to dAlo and removed =(+ ?id 1) from ids
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp	rub3  "  ?id "  " ?id1 "   dAlo)" crlf))
)

;@@@Added by 14anu19 (02-07-2014)
;He himself had stepped out and rubbed shoulders with people of all religion . [corpus]
;उन्होंने स्वयं बाहर निकलकर सभी धर्मों और समुदायों के लोगों के साथ मेल जोल रखा 
(defrule rub4
(declare (salience 4900))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id =(+ ?id 1))
(id-word =(+ ?id 1) shoulders)
(kriyA-with_saMbanXI  ?id ?id2)
;(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) mela_jola_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp rub4  "  ?id "  " (+ ?id 1) "  mela_jola_raKa  )" crlf))
)



;@@@Added by 14anu19 (02-07-2014)
;Mohan has rubbed the answers off the blackboard. 
;मोहन ने ब्लैकबोर्ड पर से उत्तर मिटाया
(defrule rub5
(declare (salience 4900))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-off_saMbanXI  ?id ?id1)
(kriyA-object  ?id ?id2)
(kriyA-subject  ?id ?id3)
;(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mitA))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp         rub5   "  ?id "  mitA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  rub.clp    rub5   "  ?id " ne )" crlf))

;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@Added by 14anu19 (02-07-2014)
;Her sense of fun has rubbed off on her children. [oald]
;मजे की उसकी संवेदना उसके बच्चों में आ गयी 
(defrule rub6
(declare (salience 5000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 off)
(kriyA-on_saMbanXI  ?id ?id2)
(id-root ?id2 child) ;added by 14anu-ban-10 on (11-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 A_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp rub6  "  ?id "  " ?id1 "  A_jA  )" crlf))
)


;@@@Added by 14anu19 (02-07-2014)
;A cat was rubbing against my leg.
;बिल्ली मेरी टाँग से रगड रही थी . 
(defrule rub7
(declare (salience 4900))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-against_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ragada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp         rub7   "  ?id "  ragada )" crlf))
)



;@@@Added by 14anu19 (02-07-2014)
;Rub the wood down well with fine sandpaper before doing anything.
;कुछ भी करने से पहले अच्छी तरह बढिया रेगमाल से लकडी को चिकना कीजिए
(defrule rub8
(declare (salience 4900))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka  ?id1 ?id2)
(id-word ?id2 down)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 cikanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  rub.clp    rub8   "  ?id " ko )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp rub8  "  ?id "  " ?id2 "  cikanA_kara  )" crlf))

;@@@ Added by Anita - 15-05-2014
;The back of my shoe is rubbing. [oxford learner's dictionary] [verified sentence]
;मेरे जूते का पिछला भाग काट रहा है ।
(defrule rub11
(declare (salience 6000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 back)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub11  "  ?id "  kAta )" crlf))
)

;@@@ Added by Anita - 16-05-2014
;We manage to rub along together fairly well. [oxford learner's dictionary]
;हम काफी अच्छी तरह से साथ-साथ रहने में समर्थ हुए ।
(defrule rub012
(declare (salience 6500))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 along)
(kriyA-upasarga  ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  sAWa_sAWa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp  rub012 "  ?id "  " ?id1 "  sAWa_sAWa_raha )" crlf))
)


;@@@ Added by Anita - 16-05-2014
;I know I was stupid; you don't have to rub it in. [oxford learner's dictionary]
;मुझे मालुम है कि मैं बेवकूफ था; आपको इसे याद दिलाने की जरूरत नहीं है ।
(defrule rub12
(declare (salience 7700))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-word  ?id1 it)
(id-word  =(+ ?id 2) in)
(kriyA-object  ?id ?id1)
(kriyA-kriyArWa_kriyA  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 =(+ ?id 2) yAxa_xilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp  rub12 " ?id "  " ?id1 " " (+ ?id 2) " yAxa_xilA )" crlf))
)


;@@@ Added by Anita - 16-05-2014
;Her sense of fun has rubbed off on her children. [oxford learner's dictionary]
;उसके आनन्द की भावना का उसके बच्चों पर प्रभाव पड़ा ।
;Let us hope some of his good luck rubs off on me! [oxford learner's dictionary] (parse-problem)
;चलिए उम्मीद करें कि उसकी अच्छी किस्मत का मुझ पर प्रभाव  पड़े ।
;His enthusiasm is starting to rub off on the rest of us. [cambridge dictionary]
;उनका उत्साह का बाकी हम सभी पर प्रभाव पड़ना शुरू हो रहा है ।
(defrule rub13
(declare (salience 7800))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-word  ?id1 off)
(id-word  =(+ ?id 2) on)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 =(+ ?id 2) praBAva_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp  rub13 " ?id "  " ?id1 " " (+ ?id 2) " praBAva_dAla )" crlf))
)

;@@@ Added by Anita - 16-05-2014
;The horse's neck was rubbed raw where the rope had been.
; घोड़े की गर्दन में जहाँ रस्सी पड़ी थी वहाँ रगड़ से चमड़ी उधड़ गई । 
(defrule rub14
(declare (salience 7000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 raw)
(viSeRya-viSeRaka  ?id1 ?id)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ragadXa_se_camadXI_UXadXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rub.clp  rub14 "  ?id "  " ?id1 "  ragadXa_se_camadXI_UXadXa )" crlf))
)

;@@@ Added by Anita - 20-05-2014
;He rubbed at the stain on his trousers and made it worse. [cambridge dictionary]
;उसने अपने पतलून के दाग को रगड़ा और उसे और भी खराब बना दिया ।
;She rubbed at the stain on her saree and made it worse.
;उसने अपनी साड़ी के दाग को रगड़ा और उसे और भी खराब बना दिया ।
(defrule rub15
(declare (salience 7980))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 stain)
(kriyA-at_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ragadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub15  "  ?id "  ragadZa )" crlf))
)

;@@@ Added by Anita - 21-05-2014
;The hotel is in the middle of nowhere and there lies the rub. We don't have a car. [oxford learner's dictionary];
;होटल सुनसान जगह में है इसलिए वहाँ मुश्किल है । हमारे पास कार नहीं है ।
(defrule rub16
(declare (salience 7750))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp     rub16  "  ?id "  muSkila )" crlf))
)


;------------------------------ Default rules -----------------------

;Same rule written by 14anu22
;Dont rub it.
;इसे मत रगड़ो.
;@@@ Added by 14anu19 ,14anu22 and 14anu-ban-01 (02-07-2014)
;She rubbed the oil into her skin.
;उसने उसकी त्वचा पर तेल मला . 
;282405:If there is headache then before sun rise apply a paste of raw guava after rubbing on stone .[Karan Singla]
;यदि सिरदर्द हो तो सूर्योदय से पूर्व कच्चा अमरुद पत्थर पर घिस कर लेप लगाएँ ।[improvised manually:changed 'kareM' to 'lagAeM']
(defrule rub0
(declare (salience 4000))
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gisa/mala/ragadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp         rub0   "  ?id "  Gisa/mala/ragadZA)" crlf))
)

;Same rule written by 14anu22
;@@@ Added by 14anu19 and 14anu22
;It happened due to constant rubbing.
;ऐसा निरंतर रगड़ने से हुआ.
(defrule rub01
(id-root ?id rub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id poFCA/ragadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rub.clp         rub01   "  ?id "  poFCA/ragadZa))" crlf))
)

