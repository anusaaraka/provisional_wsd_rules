;@@@ Added by 14anu-ban-11 on (11-02-2015)
;Duck with sage and onion stuffing.(oald)
;तेजपत्ता और प्याज मसाले के साथ बतख . (anusaaraka)
(defrule sage1
(declare (salience 20))
(id-root ?id sage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 stuffing)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wejapawwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sage.clp   sage1  "  ?id "  wejapawwA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-02-2015)
;My sage old grandfather. (cambridge)
;मेरा ज्ञानी  वृद्ध दादा .(self)
(defrule sage2
(declare (salience 30))
(id-root ?id sage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 father|mother|sister|brother|grandfather|grandmother)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jFAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sage.clp   sage2  "  ?id "  jFAnI )" crlf))
)

;------------------------- Default Rules ---------------------

;@@@ Added by 14anu-ban-11 on (11-02-2015)
;He look like a sage. (hinkhoj)
;वह साधु की तरह दिखता है . (anusaaraka)
(defrule sage0
(declare (salience 10))
(id-root ?id sage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sAXu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sage.clp   sage0  "  ?id "  sAXu )" crlf))
)



























