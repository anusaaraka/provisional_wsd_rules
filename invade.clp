;@@@ Added by 14anu-ban-06 (11-02-2015)
;Troops invaded on August 9th that year. (OALD)
;दलों ने उस वर्ष  9th अगस्त को आक्रमण किया .  (manual)
(defrule invade0
(declare (salience 0))
(id-root ?id invade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkramaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invade.clp 	invade0   "  ?id "  AkramaNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-02-2015)
;Do the press have the right to invade her privacy in this way? (OALD)
;क्या प्रैस के पास इस तरह उसकी गोपनीयता में हस्तक्षेप करने का अधिकार है?  (manual)
(defrule invade1
(declare (salience 2000))
(id-root ?id invade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 privacy|secrecy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haswakRepa_kara))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invade.clp 	invade1   "  ?id "  haswakRepa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  invade.clp      invade1   "  ?id1 " meM )" crlf))
)
