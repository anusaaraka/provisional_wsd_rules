
(defrule wish0
(declare (salience 100)) ;salience reduced by 14anu01
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id icCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish0   "  ?id "  icCA )" crlf))
)

;"wish","N","1.icCA"
;Her wish was to serve the needy.
;--"2.ASIrvAxa_xenA"
;I sent my new year wish to my daughter.
;
(defrule wish1
(declare (salience 100))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id icCA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish1   "  ?id "  icCA_kara )" crlf))
)

;default_sense && category=verb	icCA_honA[karanA]	0
;"wish","VTI","1.icCA_honA[karanA]"
;I wished to be a doctor.
;--"2.SuBakAmanA_karanA"
;I wish you success.
;

;@@@ Added by Pramila(BU) on 20-02-2014
;I do not wish to diminish the importance of their contribution.
;मैं उनके योगदान का महत्व कम नहीं करना चाहता हूँ .
(defrule wish2
(declare (salience 4900))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(kriyA-object  ?id1 ?id2)
(viSeRya-of_saMbanXI  ?id2 ?id3)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish2   "  ?id "  cAha )" crlf))
)


;@@@ Added  by 14anu01 
;wish you a happy birthday.
;आपको जन्मदिन की बधाई.
(defrule wish3
(declare (salience 4000))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) you)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baXAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish3   "  ?id "   baXAI)" crlf))
)

;@@@ Added  by 14anu01 
;wish him happy birthday.
;उसे  जन्मदिन की बधाई देनाा
(defrule wish4
(declare (salience 4500))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) him|her|them|his|their)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBakAmanA_xenA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish4   "  ?id "   SuBakAmanA_xenA)" crlf))
)

;@@@ Added by 14anu01 
;The Queen had sent her best wishes for his examination.
;रानी उसकी परीक्षा के लिए उसे शुभकामनाएं भेजी थी.
(defrule wish5
(declare (salience 5550))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBakAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish5   "  ?id "   SuBakAmanA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (28-03-2015)
;I sent my new year wish to my daughter. (hinkhoj)
;मैंने अपनी बेटी को नये वर्ष की शुभकामना भेजी है . (self)
(defrule wish6
(declare (salience 150))
(id-root ?id wish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBakAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wish.clp 	wish6   "  ?id "   SuBakAmanA)" crlf))
)


