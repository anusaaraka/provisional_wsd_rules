
;"rigging","N","1.jahAjZa_kI_rassI"
;Rigging of the ship broke due to friction.
(defrule rig0
(declare (salience 5000))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id rigging )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jahAjZa_kI_rassI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rig.clp  	rig0   "  ?id "  jahAjZa_kI_rassI )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 24/6/2014
;Please rig up a tent for the night. 
;कृपया रात के लिए एक तम्बू लगा दें .
(defrule rig3
(declare (salience 5000))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 up) 
(test (= (+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig3   "  ?id "  lagA )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 24/6/2014
;The match was rigged. 
;वह मैच फिक्स था.
(defrule rig4
(declare (salience 5000))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Piksa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig4   "  ?id "  Piksa )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 24/6/2014
;He rigged the match. 
;उसने मैच फिक्स किया. 
(defrule rig5
(declare (salience 5000))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Piksa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig5   "  ?id "  Piksa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-03-2015)
;The ship was rigged properly.[hinkhoj]
;जहाज को ठीक से उपकरण से लेस करा  गया था।[manual]
(defrule rig6
(declare (salience 5100))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upakaraNa_se_lesa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig6   "  ?id "  upakaraNa_se_lesa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-03-2015)
;Opposition party claimed that the election had been rigged.[hinkhoj]
;विपक्षी दल ने दावा किया है कि चुनाव चालाकी से संचालित किया गया था ।[manual]
(defrule rig7
(declare (salience 5200))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 election)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAlAkI_se_saMcAliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig7   "  ?id " cAlAkI_se_saMcAliwa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;"rig","N","1.viSeRa_upakaraNa"
;I have provided all rigs for your ship. 
(defrule rig1
(declare (salience 4900))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa_upakaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig1   "  ?id "  viSeRa_upakaraNa )" crlf))
)

;"rig","VT","1.upakaraNa_se_lesa_karanA"
;The ship was rigged properly.
(defrule rig2
(declare (salience 4800))
(id-root ?id rig)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upakaraNa_se_lesa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rig.clp 	rig2   "  ?id "  upakaraNa_se_lesa_kara )" crlf))
)

;"rig","VT","1.upakaraNa_se_lesa_karanA"
;The ship was rigged properly.
;--"2.cAlAkI_se_saMcAliwa_karanA"
;Opposition party claimed that the election had been rigged.   
;
