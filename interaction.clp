;@@@ Added by 14anu-ban-06  (09-09-2014)
;After all , every one of the thousands of strands which makes up the web of human interaction and activity has to be governed by some law .(Parallel corpus)
;सब हुछ होते हुए भी , मनुष़्य के पारस़्परिक संबंधों और क्रियाओं के जाल के हजारों रेशों में से प्रत़्येक को किसी न किसी कानूनके अंतर्गत हीह नियंत्रित करना पड़ता है .
(defrule interaction0
(declare (salience 0))
(id-root ?id interaction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pArasparika_vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interaction.clp 	interaction0   "  ?id "  pArasparika_vyavahAra )" crlf))
)

;@@@ Added by 14anu-ban-06  (09-09-2014)
;This'life'began through an interaction between certain chemical substances called nucleic acids and proteins .(Parallel corpus)
;यह जीवन कुछ विशेष न्यूक्लिक अम्लों और प्रोटीनों जैसे रासायनिक पदार्थों के बीच परस्पर क्रिया से आरंभ हुआ होगा .
;All life is the outcome of a complicated interaction between heredity and environment .(Parallel corpus)
;जीवन उसी आपसी क्रिया का ही नाम है जो आनुवंशिकता तथा परिवेश के बीच होती रहती है .
(defrule interaction1
(declare (salience 2000))
(id-root ?id interaction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-between_saMbanXI ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paraspara_kriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interaction.clp 	interaction1   "  ?id "  paraspara_kriyA )" crlf))
)

;@@@ Added by 14anu-ban-06  (09-09-2014)
;Synthesis of polynucleotides , even before'life'began , may have been influenced by the interaction of nucleotides with peptides .(Parallel corpus)
;जीवन का आरंभ होने से भी पहले , पॉलीन्यूक्लिओटाइडों का निर्माण , न्यूक्लिओटाइडों और पेप्टाइडों की परस्पर क्रिया से प्रभावित हुआ होगा .
(defrule interaction2
(declare (salience 2100))
(id-root ?id interaction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paraspara_kriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interaction.clp 	interaction2   "  ?id "  paraspara_kriyA )" crlf))
)
