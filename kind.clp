
(defrule kind0
(declare (salience 5000))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id kinds)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  kind.clp  	kind0   "  ?id "  prakAra )" crlf))
)

(defrule kind1
(declare (salience 4900))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) a )
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind1   "  ?id "  prakAra )" crlf))
)

;Helping us was so kind of you.
(defrule kind2
(declare (salience 4800))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) ; Added by Aditya and Hardik(9.7.13),IIT(BHU)
(id-cat_coarse ?id1 pronoun|PropN) ; Added by Aditya and Hardik(9.7.13),IIT(BHU)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xayAlupana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind2   "  ?id "  xayAlupana )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (14-02-2016)
;One kind of response from the earliest times has been to observe the physical environment carefully, look for any meaningful patterns and relations in natural phenomena, and build and use new tools to interact with nature.	[ncert corpus]
;प्राचीन काल से मानव की एक प्रकार की प्रतिक्रिया यह रही है कि उसने अपने भौतिक पर्यावरण का सावधानीपूर्वक प्रेक्षण किया है, प्राकृतिक परिघटनाओं में अर्थपूर्ण पैटर्न तथा सम्बन्ध खोजे हैं, तथा प्रकृति के साथ क्रिया-प्रतिक्रिया कर सकने के लिए नए औजारों को बनाया तथा उनका उपयोग किया है.	[translation improved by Chaitanya Sir]
;Added by Aditya and Hardik(9.7.13),IIT(BHU)
;What kind of person is Rama?
(defrule kind5
(declare (salience 4750))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) of)		;commented by 14anu-ban-01 on (14-02-2016)
(viSeRya-of_saMbanXI  ?id )	;added by 14anu-ban-01 on (14-02-2016)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) prakAra_kA))	;commented by 14anu-ban-01 on (14-02-2016)
(assert (id-wsd_root_mng ?id prakAra))											;added by 14anu-ban-01 on (14-02-2016)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kind.clp	  kind5  "  ?id "  " (+ ?id 1) "  prakAra_kA  )" crlf))	;commented by 14anu-ban-01 on (14-02-2016)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind5   "  ?id "  prakAra )" crlf))	;added by 14anu-ban-01 on (14-02-2016)
)

;@@@Added by 14anu-ban-07,(07-02-2015)
;Either pay in cash or in kind.(same file)
;या तो  भुगतान करो  नकद या वस्तु में. (self)
(defrule kind6
(declare (salience 4800))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 pay)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind6   "  ?id "  vaswu )" crlf))
)


;@@@Added by 14anu-ban-07,(07-02-2015)
;This soap is kinder to the skin. (cambridge)
;यह साबुन त्वचा के लिए अधिक नम्र है .(self) 
(defrule kind7
(declare (salience 4850))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 skin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id namra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind7   "  ?id "  namra )" crlf))
)

;---------------------------- Default Rules -----------------
(defrule kind3
(declare (salience 4700))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xayAlu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind3   "  ?id "  xayAlu )" crlf))
)


;default_sense && category=noun	prakAra	0
;"kind","N","1.prakAra"
;He is a very different kind of person.
(defrule kind4
(declare (salience 4600))
(id-root ?id kind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kind.clp 	kind4   "  ?id "  prakAra )" crlf))
)


;default_sense && category=noun	prakAra	0
;"kind","N","1.prakAra"
;He is a very different kind of person.
;--"2.vaswu"
;Either pay in cash or in kind.
;
;
