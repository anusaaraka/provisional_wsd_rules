;@@@ Added by 14anu-ban-02 (14-11-2014)
;Sentence: The government wanted to avoid giving the facility help from advocates or jury to the accused and arbitrarily shifted the venue of the trial to Bombay High Court to the prejudice of Tilak .[karan singla]
;Translation: सरकार उन्हें कोई भी सुविधा वकील या जूरी की मदद देने तक को तैयार न हुई और तिलक के विरूद्ध उन्होंने मुकदमे को मनमाने ढंग से बंबई हाई कोर्ट में स्थानांतरित कर दिया .[karan singla]
(defrule arbitrarily0 
(declare (salience 0)) 
(id-root ?id arbitrarily) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adverb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id manamAne_DaMga_se)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrarily.clp  arbitrarily0  "  ?id "  manamAne_DaMga_se )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (14-11-2014)
;Measurement of any physical quantity involves comparison with a certain basic, arbitrarily chosen, internationally accepted reference standard called unit.[ncert]
;किसी भौतिक राशि का मापन, एक निश्चित, आधारभूत, यादृच्छिक रूप से चुने गए मान्यताप्राप्त, सन्दर्भ-मानक से इस राशि की तुलना करना है ; यह सन्दर्भ-मानक मात्रक कहलाता है.[ncert]

(defrule arbitrarily1 
(declare (salience 100)) 
(id-root ?id arbitrarily)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id) 
(id-cat_coarse ?id adverb) 
(id-root ?id1 choose)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id yAxqcCika_rUpa_se)) ;Modified 'yAxracCika' as 'yAxqcCika' by Shirisha Manju 07-08-2015
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arbitrarily.clp  arbitrarily1  "  ?id "  yAxqcCika_rUpa_se )" crlf)) 
)  


