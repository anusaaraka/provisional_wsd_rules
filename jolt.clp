;@@@ Added by 14anu-ban-06 (10-03-2015)
;He was jolted forwards as the bus moved off.(OALD)
;उसे आगे की ओर झटका लगा था जैसे बस ने प्रस्थान  किया  . (manual)
(defrule jolt2
(declare (salience 5100))
(id-root ?id jolt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 forward)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JatakA_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jolt.clp 	jolt2   "  ?id "  JatakA_laga )" crlf))
)

;@@@ Added by 14anu-ban-06 (10-03-2015)
;At 12:55 A.M. the train jolted to a stop.(COCA)
;12:55 ए. एम. पर रेलगाडी स्टाप पर हल्के झटके के साथ रूकी. (manual) 
(defrule jolt3
(declare (salience 5200))
(id-root ?id jolt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 stop|halt)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halke_Jatake_ke_sAWa_rUka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jolt.clp 	jolt3   "  ?id "  halke_Jatake_ke_sAWa_rUka )" crlf))
)

;---------------------- Default Rules -------------------
(defrule jolt0
(declare (salience 5000))
(id-root ?id jolt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JatakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jolt.clp 	jolt0   "  ?id "  JatakA )" crlf))
)

;"jolt","VT","1.hilAnA"
;The bomb explosion jolted the entire building.
(defrule jolt1
(declare (salience 4900))
(id-root ?id jolt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jolt.clp 	jolt1   "  ?id "  hilA )" crlf))
)

