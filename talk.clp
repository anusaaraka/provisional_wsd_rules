;Added by Meena(19.5.10)
;Paul, in typically rude fashion, told him he was talking rubbish.
(defrule talk_rubbish1
(declare (salience 4900))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 rubbish)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bakavAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp  talk_rubbish1  "  ?id "  " ?id1 "  bakavAsa_kara  )" crlf))
)

;Added by Shirisha Manju (20-8-13) Suggested by Chaitanya Sir
;Henceforth, every Sunday, I shall be giving a talk on the teaching of the Gita, who is verily our mother.
(defrule talk_give
(declare (salience 4900))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?kri ?id)
(id-root ?kri give)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BASaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  talk.clp      talk_give   "  ?id "  BASaNa )" crlf))
)


;@@@ Added by Prachi Rathore[19-2-14]
;Peace talks were held to try to heal the growing rift between the two sides.[cambridge]
;शांति वार्ता दोनों तरफ के बीच बढती हुई दरार मिटाने का प्रयास करने के लिये आयोजित की गयी थी . 
(defrule talk3
(declare (salience 4900))
(id-word ?id talks)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vArwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  talk.clp 	talk3   "  ?id "  vArwA )" crlf))
)

;@@@ Added by 14anu-ban-07,(24-02-2015)
;What discussion? You weren't talking to me, you were talking at me!(cambridge)(parser problem)
;क्या चर्चा? आप मुझसे बातचीत नहीं कर रहे थे, आप मुझपर बोले जा रहे थे! (manual)
(defrule talk4
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 at)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bole_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk4  "  ?id "  " ?id1 " bole_jA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;If we talk up the event, people will surely come.(cambridge)
;यदि हम कार्यक्रम को सराहते हैं, तो लोग निश्चित रूप से आएँगे . (manual)
(defrule talk5
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 up)
(kriyA-object  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sarAha))
(assert (id-wsd_viBakwi ?id2 ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk5  "  ?id "  " ?id1 " sarAha  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  talk.clp talk5  "  ?id2 " ko)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;Children who talk back are regarded as cheeky and disrespectful.(cambridge)(parser no. 2)
;बच्चे  जो जबान चलाते हैं   गुस्ताख और अनादरणीय माने जाता हैं . (manual)
(defrule talk6
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 back)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jabAna_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk6  "  ?id "  " ?id1 " jabAna_calA  )" crlf))
)


;@@@ Added by 14anu-ban-07,(25-02-2015)
;The policeman talked the girl down after she had been on the roof for two hours.(cambridge)(parser no. 157)
;सिपाही ने लडकी को समझाया जब  वह दो घण्टों तक छत पर रही थी . (manual)
(defrule talk7
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk7  "  ?id "  " ?id1 " samaJA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;You shouldn't talk down your own achievements.(oald)
;आपको आपकी अपनी उपलब्धियाँ कम नहीं समझानी चाहिए . (manual)
(defrule talk8
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 down)
(kriyA-object  ?id ?id2)
(not(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kama_samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk8  "  ?id "  " ?id1 " kama_samaJA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;I didn't want to move abroad but Bill talked me into it.(oald)
;मैंने विदेश  नहीं  जाना चाहाता था परन्तु बिल ने  मुझे मनाया . (manual)
(defrule talk9
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 into)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 manA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk9  "  ?id "  " ?id1 " manA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;I'd like to talk it over with my wife first.(cambridge)
;मैं पहले मेरी पत्नी के साथ यह विचार विमर्श करना पसन्द करूँगा . (manual)
(defrule talk10
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 over)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_vimarSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk10  "  ?id "  " ?id1 " vicAra_vimarSa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(25-02-2015)
;We spent a whole hour talking around the problem before looking at ways of solving it.(oald)
;हमने  हल करने का मार्गों  सोचने से पहले  समस्या के बाहर बातचीत करते हुए एक पूरा घण्टा बिताया . (manual)
(defrule talk11
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 around)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_bAwacIwa_kara))
(assert (id-wsd_viBakwi ?id2 ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk11  "  ?id "  " ?id1 " viRaya_ke_bAhara_bAwacIwa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  talk.clp talk11  "  ?id2 " ke)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(26-02-2015)
;She's not keen on the idea but we think we can talk her round.(cambridge)(parser problem)
;उसे सुझाव पर दिलचस्पी  नहीं है परन्तु हमरा विचारना हैं कि हम उसको  मना सकते हैं .(manual) 
(defrule talk12
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 round)
(kriyA-object ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 manA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk12  "  ?id "  " ?id1 " manA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(26-02-2015)
;I felt that he just talked round the subject and didn't tackle the main issues.(cambridge)(parser problem) 
;मुझे लगा कि उसने प्रमुख समस्यों को हल नहीं किया और विषय के बाहर बातचीत कर रहा था  .(manual) 
(defrule talk13
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 round)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_bAwacIwa_kara))
(assert (id-wsd_viBakwi ?id2 ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk13  "  ?id "  " ?id1 " viRaya_ke_bAhara_bAwacIwa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  talk.clp talk13  "  ?id2 " ke)" crlf)
)
)


;@@@ Added by 14anu-ban-07,(26-02-2015)
;Can you talk me through the various investment options?(oald)(parser no. 85)
;क्या आप विभिन्न विनीयुक्त धन विकल्पो का मुझे विवरण दे सकते है? (manual)
(defrule talk14
(declare (salience 5100))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 through)
(kriyA-object ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vivaraNa_xe))
(assert (kriyA_id-object_viBakwi ?id2 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk14  "  ?id "  " ?id1 " vivaraNa_xe  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  talk.clp     talk14   "  ?id2 "  kA )" crlf))
)

;@@@ Added by 14anu-ban-07,(26-02-2015)
;It sounds like a good idea but we’ll need to talk it through.(oald)
;यह एक अच्छा सुझाव की तरह लगता है परन्तु हमे इस पर विचार-विमर्श करना पडेंगे . (manual)
(defrule talk15
(declare (salience 5000))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 through)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra-vimarSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " talk.clp	talk15  "  ?id "  " ?id1 " vicAra-vimarSa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(26-02-2015)
;I attended an interesting talk on local history.(same file)
;मैं स्थानीय इतिहास पर रोचक वाद-विवाद में उपस्थित  था.(manual)
(defrule talk16
(declare (salience 4800))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 attend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAxa-vivAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  talk.clp 	talk16   "  ?id "  vAxa-vivAxa )" crlf))
)

;------------------------ Default Rules ----------------------

;salience reduced by Manju(21-08-13)
;"talk","N","1.bAwacIwa"
;Let's have more work && less talk around here.
(defrule talk0
(declare (salience 4700))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwacIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  talk.clp 	talk0   "  ?id "  bAwacIwa )" crlf))
)

;Salience reduced by Meena(19.5.10)
;"talk","VT","1.bAwacIwa_karanA"
;We talked for a long time.
(defrule talk2
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id talk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwacIwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  talk.clp 	talk2   "  ?id "  bAwacIwa_kara )" crlf))
)

;"talk","VT","1.bAwacIwa_karanA"
;We talked for a long time.
;--"2.bola_sakanA"
;Children learn to talk faster.
;
;"talk","N","1.bAwacIwa"
;Let's have more work && less talk around here.
;--"2.gapaSapa"
;There has been talk about you.
;--"3.vAxa-vivAxa"
;I attended an interesting talk on local history.
;


