;@@@ Added by 14anu-ban-04 (25-03-2015)
;She handled the situation with great sensitivity and delicacy.            [oald]
;उसने बड़े भावुकता और भद्रता से परिस्थिति  सम्भाली .                                    [self]
(defrule delicacy1
(declare (salience 30))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?kri ?id)
(kriyA-object ?kri ?id1)
(id-root ?id1 situation|matter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaxrawA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy1  "  ?id "  BaxrawA)" crlf))
)

;@@@ Added by 14anu-ban-04 (25-03-2015)
;The exquisite delicacy of the embroidery.           [oald]
;कशीदाकारी की उत्कृष्ट बारीकी .                               [self]
(defrule delicacy2
(declare (salience 30))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id  ?id1)
(id-root ?id1  embroidery)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArIkI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy2  "  ?id "  bArIkI)" crlf))
)

;@@@ Added by 14anu-ban-04 (25-03-2015)
;In some parts of the world, sheep's eyes are considered a great delicacy.               [cald]
;विश्व के कुछ भागों में, भेड़ों की आँखें एक बड़ा स्वादिष्ट भोजन मानी जाती हैं .                                       [self] 
(defrule delicacy3
(declare (salience 20))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?kri ?id)
(kriyA-subject ?kri ?id1)
(id-root ?id1 eye|egg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta_Bojana/svAxiRta_KAxya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy3  "  ?id "  svAxiRta_Bojana/svAxiRta_KAxya)" crlf))
)

;@@@ Added by 14anu-ban-04 (25-03-2015)
;Crabs are a delicacy in this region.                   [hinkhoj]
;केकड़े इस क्षेत्र में स्वादिष्ट भोजन हैं .                               [self]
(defrule delicacy4
(declare (salience 30))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?kri ?id1)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta_Bojana/svAxiRta_KAxya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy4  "  ?id "  svAxiRta_Bojana/svAxiRta_KAxya)" crlf)) 
)

;@@@ Added by 14anu-ban-04 (25-03-2015)
;I don't think you fully appreciate the delicacy of the situation.                    [cald]
;मैं नहीं सोचता हूँ कि आप  परिस्थिति की नजाकत को  पूर्ण रूप से समझते हैं .                                     [self]
(defrule delicacy5
(declare (salience 30))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 matter|situation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najAkawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy5  "  ?id "  najAkawa )" crlf)) 
)

;;@@@ Added by 14anu-ban-04 (25-03-2015)
;The delicacy of their game has led them to the number one position in the world.              [hinkhoj]
;उनके खेल कि कुशलता उनको विश्व में पहले नंबर  के स्थान    की ओर ले गयी है .                                          [self]
(defrule delicacy6
(declare (salience 30))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(viSeRya-RaRTI_viSeRaNa ?id1 ?id2)
(id-root ?id1 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuSalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy6  "  ?id "  kuSalawA )" crlf)) 
)
 

;--------------------------------------------------------DEFAULT RULES --------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (25-03-2015)
;The delicacy of his touch.                           [oald]
;उसके स्पर्श की कोमलता .                                     [self]
(defrule delicacy0
(declare (salience 10))
(id-root ?id delicacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id komalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicacy.clp    delicacy0  "  ?id "  komalawA)" crlf))
)

;------------------------------------------------------------------------------------------

