;Commented the whole rule as it should be handled in 'shocking.clp' by 14anu-ban-01 on (10-11-2014) 
;(defrule shock0
;(declare (salience 5000))
;(id-root ?id shock)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id shocking )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id xahalAnevAlA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  shock.clp  	shock0   "  ?id "  xahalAnevAlA )" crlf))
;)

;"shocking","Adj","1.xahalAnevAlA"
;She went to see the shocking sight after the disastrous flood.
;--"2.bahuwa KarAba"
;The food here is shocking.
;
;Added by Human
(defrule shock1
(declare (salience 4900))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOMkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shock.clp 	shock1   "  ?id "  cOMkA )" crlf))
)

(defrule shock2
(declare (salience 4800))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shock.clp 	shock2   "  ?id "  saxamA )" crlf))
)

(defrule shock3
(declare (salience 4700))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shock.clp 	shock3   "  ?id "  cOMka )" crlf))
)

;"shock","V","1.cOMkanA"
;He was shocked to see the results.
;He was shocked to hear his child swearing.
;
;
;@@@ Added by 14anu04 on 24-June-2014
;He got an electric shock.
;उसको बिजली का झटका लगा . 
(defrule shock_tmp
(declare (salience 4900))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 electric)
(test (=(- ?id 1) ?id1)) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bijalI_kA_JatakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shock.clp 	shock_tmp  "  ?id "  " ?id1 " bijalI_kA_JatakA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (10-11-2014)
;Some people (0.6%[26] of the United States population) report that they experience mild to severe allergic reactions to peanut exposure; symptoms can range from watery eyes to anaphylactic shock, which can be fatal if untreated. [agriculture corpus]
;कुछ लोग (यूनाइटड स्टॆट्स की जनसङ्ख्या का 0.6 %) शिकायत करते हैं कि वे मूँगफली खाने पर मृदु से गंभीर प्रत्यूर्ज प्रतिक्रियाओं का अनुभव करते हैं;लक्षण आँखों से पानी बहने  से लेकर तीव्रगाहिता संबंधी आघात तक हो सकते हैं जो कि अनुपचारित रहें तो घातक हो सकते हैं.[Self]
(defrule shock4
(declare (salience 4900))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-word ?id1 anaphylactic)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AGAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shock.clp 	shock4   "  ?id "  AGAwa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-12-2014)
;Don't touch that wire or you'll get a shock. (oald)
;वह तार स्पर्श मत कीजिए  अन्यथा आपको झटका लग सकता है . (manual)
(defrule shock5
(declare (salience 4900))
(id-root ?id shock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 get)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JatakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " shock.clp 	shock5  " ?id1 " JatakA )" crlf))
)

