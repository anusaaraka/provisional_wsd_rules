;@@@ Added by 14anu-ban-04 (10-03-2015)
;Robinson and his cohorts were soon ejected from the hall.       [oald]
;रॉबन्सन  और उसके  साथियों को हॉल से शीघ्र ही बाहर निकाला गया था .                           [manual]
;He was forcibly ejected from the restaurant.                      [oald]
;उसको भोजनालय से बलपूर्वक बाहर निकाला गया था .                                  [self]
;They were summarily ejected by the security guard.                  [oald]
;उनको पहरेदार के द्वारा फौरन ही बाहर निकाला गया था .                               [self]
(defrule eject4
(declare (salience 4020))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)       
(id-cat_coarse ?id verb)
(or(kriyA-from_saMbanXI ?id ?id1)(kriyA-by_saMbanXI ?id ?id1))
(kriyA-subject ?id ?id2)
(or(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) (id-cat_coarse ?id2 PropN))
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id bAhara_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  eject.clp     eject4   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject4   "  ?id " bAhara_nikAla)" crlf))
)

;@@@ Added by 14anu-ban-04 (10-03-2015)
;How do you eject the tape?                     [cald]
;आप पट्टी कैसे निकालते हैं?                             [self]
(defrule eject5
(declare (salience 4910))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject5   "  ?id "  nikAla )" crlf))
)


(defrule eject0
(declare (salience 4900))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id balapUrvaka_bAhara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject0   "  ?id "  balapUrvaka_bAhara_kara )" crlf))
)

;@@@ added by pramila(BU) on 05-12-2013
;So your stainless steel spoon was made from a material which was processed deep inside some star and finally ejected into outer space in 
;a supernova explosion.
;इसलिए स्टेनलैस स्टील का चम्मच उस सामग्री से बना जो किसी तारे के अंदर तैयार हुई और फिर किसी सुपरनोवा विस्फोट में वहां से निकलकर अंतरिक्ष में फेंक दी गयी।
(defrule eject1
(declare (salience 5000))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject1   "  ?id "  PeMka_xe )" crlf))
)

(defrule eject2
(declare (salience 4000))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id balapUrvaka_bAhara_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject2   "  ?id "  balapUrvaka_bAhara_ho )" crlf))
)

;"eject","VT","1.balapUrvaka_bAhara_honA[karanA]"
;He managed to eject from the crashing helicopter.
;


;@@@ Added by 14anu-ban-04 (28-11-2014)
;During a solar flare, a large number of electrons and protons are ejected from the sun.             [NCERT-CORPUS]
;सौर प्रज्वाल के समय सूर्य से विशाल सङ्ख्या में इलेक्ट्रॉन तथा प्रोटॉन बाहर उत्सर्जित होते हैं.                     [NCERT-CORPUS]
(defrule eject3
(declare (salience 4010))
(id-root ?id eject)
?mng <-(meaning_to_be_decided ?id)       
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara_uwsarjiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  eject.clp 	eject3   "  ?id "  bAhara_uwsarjiwa_ho)" crlf))
)
