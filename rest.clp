
;Added by human
(defrule rest0
(declare (salience 5000))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 eye)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Tahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest0   "  ?id "  Tahara )" crlf))
)

(defrule rest1
(declare (salience 4900))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest1   "  ?id "  SeRa )" crlf))
)

;@@@ Added by Anita-20.1.2014
;May her soul rest in peace.  [old clp sentence]
;उसकी आत्मा को शान्ति मिले ।
(defrule rest4
(declare (salience 4850))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 peace)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwi_mila))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest4   "  ?id "  SAnwi_mila )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  rest.clp      rest4   "  ?id " ko )" crlf)
)
)

;@@@ Added by Anita-20.1.2014
;He rested his head on a pillow. [old clp sentence]
;उसने अपना सिर तकिये पर टिकाया ।
(defrule rest5
(declare (salience 4950))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 pillow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest5   "  ?id "  tikA )" crlf))
)

;@@@ Added by Anita-20.1.2014
;I will never rest until I know the truth. [old clp sentence]
;जब तक सच नहीं जान लूँगा तब तक मैं शान्त नहीं होऊँगा । 
(defrule rest6
(declare (salience 5050))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_niReXaka  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest6   "  ?id "  SAnwa_ho )" crlf)
)
)

;@@@ Added by Anita-20.1.2014
;India's hope of a gold medal rested on P.T.Usha. [old clp sentence]
;भारत की गोल्ड मेडल की आशा पी.टी. ऊषा पर निर्भर करती है ।
(defrule rest7
(declare (salience 5150))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 hope)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 medal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest7   "  ?id "  nirBara_kara )" crlf)
)
)



;@@@ Added by Anita-20.1.2014
;His eys rested on her face. [old clp sentence]  [tam problem] 
;उसकी आँखें उसके चेहरे पर टिक गयीं ।
(defrule rest8
(declare (salience 4000))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tika_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest8   "  ?id "  tika_jA )" crlf))
)

;@@@ Added by Anita-20.1.2014
;Let the Bofors case rest.  [old clp sentence] [4th parse]
;चलो बोफॉर्स मामले की कार्यवाही छोड़ दें ।
;I rest my case. 
; मैंने अपने मामले की कार्यवाही छोड़ दी ।
(defrule rest9
(declare (salience 5270))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-object  ?id ?id1)(kriyA-subject ?id ?id1))
(id-root ?id1 case)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryavAhI_CodZa_xe))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest9   "  ?id " kAryavAhI_CodZa_xe)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  rest.clp      rest9  "  ?id " kA )" crlf)
)
)

;@@@ Added by Anita-20.1.2014 
;Let this field rest for a year. [old clp sentence] [6th parse]
;चलो एक साल के लिए यह खेत खाली छोड़ दें ।
(defrule rest10
(declare (salience 5250))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(viSeRya-det_viSeRaNa  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAlI_CodZa_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest10   "  ?id "  KAlI_CodZa_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  rest.clp      rest10  "  ?id " ko )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (05-08-2014)
;Consider the motion of a car starting from rest, picking up speed and then moving on a smooth straight road with uniform speed. [ncert corpus]
;gElIliyo kA jadawva kA niyama usakA AramBa binxu WA jisakA nyUtana ne 'gawi ke prraWama niyama ' ke rUpa meM saMrUpaNa kiyA: "prawyeka piNda waba waka apanI virAmAvasWA aWavA sarala reKA meM ekasamAna gawi kI avasWA meM rahawA hE jaba waka koI bAhya bala use anyaWA vyavahAra karane ke lie vivaSa nahIM karawA. 
;If the object is released from rest, the initial potential energy is completely converted into the kinetic energy of the object just before it hits the ground. [ncert corpus]
;yaxi piNda ko virAmAvasWA se mukwa kiyA jAwA hE, wo BUmi se takarAne se TIka pahale piNda kI sampUrNa sWiwija UrjA gawija UrjA meM parivarwiwa ho jAwI hE. 
;@@@ Added by Anita-20.3.2014
;In the simpler case when charges are at rest the force is given by Coulombs law attractive for unlike ;charges and repulsive for like charges. [ncert]
;सरल प्रकरण में, जब आवेश विरामावस्था में होते हैं, तो इस बल को कूलॉम - नियम द्वारा व्यक्त किया जाता है: "सजातीय आवेशों में ;प्रतिकर्षण तथा विजातीय आवेशों में आकर्षण"
(defrule rest11
(declare (salience 5350))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-at_saMbanXI  ? ?id)(viSeRya-of_saMbanXI ?id1 ?id)(kriyA-from_saMbanXI ?id2 ?id)) ;( 2 relation added )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAmAvasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest11   "  ?id "  virAmAvasWA )" crlf))
)
;@@@ Added by Anita--2.4. 2014
;The decision regarding the rest of the candidates for the Assembly would only be made after Diwali.
;बाकी सीटों पर फैसला दीपावली बाद जिले की बाकी विधानसभा सीटों पर प्रत्याशियों की घोषणा दीपावली के बाद होने की संभावना ;व्यक्त की जा रही है।
(defrule rest12
(declare (salience 5450))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest12   "  ?id "  SeRa )" crlf))
)
;@@@ Added by Anita--23.5. 2014
;The onus rests upon the individual. [SOCIOLOGY TEXT BOOK CLASS XI]
;xAyiwva vyakwi ke Upara nirBara karawA hE.
;दायित्व व्यक्तिगत पर निर्भर करता है ।
(defrule rest13
(declare (salience 5550))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upon_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest13   "  ?id "  nirBara_kara )" crlf)
)
)
;@@@ Added by Anita--30.5. 2014
;C. Wright Mills rests his vision of the sociological imagination precisely in the unravelling of how the ;personal and public are related.
;सी. राइट मिल्स समाजशास्त्रीय कल्पनाओं पर अपनी  दृष्टि मुख्यतः इन बातों को सुलझाने पर टिकाते हैं कि किस तरह वैयक्तिक और ;जनहित परस्पर सम्बन्धित हैं  ।
(defrule rest14
(declare (salience 5600))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tikA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest14   "  ?id "  tikA_xe )" crlf))
)

;@@@ Added by 14anu01 
; The rest for her, was history.
;उसके लिए शेष, इतिहास था . 
(defrule rest15
(declare (salience 5500))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) for )
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1 )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest15   "  ?id "  SeRa ) " crlf))
)

;@@@ Added by 14anu-ban-10 on (25-08-2014)
;When an object is submerged in a fluid at rest, the fluid exerts a force on its surface.[ncert corpus]
;jaba koI piNda kisI SAMwa warala meM dUbA huA hE, wo warala usa piNda para bala Aropiwa karawA hE.
(defrule rest16
(declare (salience 5560))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest16  "  ?id " SAMwa  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (31-03-2016)
;NOTE: Parser Problem: 'resting' is tagged as noun instead of 'verb'.So, this rule will get fired when its POS will be correct.
;@@@ Added by 14anu-ban-10 on (08-09-2014)
;When bodies are in contact (e.g. a book resting on a table, a system of rigid bodies connected by rods, hinges and other types of supports), there are mutual contact forces (for each pair of bodies) satisfying the third law.  
;jaba kaI piNda samparka meM howe hEM, (uxAharaNArWa, meja para raKI koI puswaka, CadoM, kabjoM waWA anya prakAra ke AXAroM se sambaxXa xqDa piNdoM kA koI nikAya), waba vahAz wqwIya niyama ko sanwuRta karane vAle (piNdoM ke prawyeka yugala ke lie) pArasparika samparka bala howe hEM.
(defrule rest17
(declare (salience 6000))
(id-root ?id rest)
(id-word ?id resting)	;added by 14anu-ban-01
?mng <-(meaning_to_be_decided ?id)
;(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ? )(viSeRya-det_viSeRaNa ?id ? )) 	;commented by 14anu-ban-01
(viSeRya-kqxanwa_viSeRaNa ? ?id )	;added by 14anu-ban-01
;(id-cat_coarse ?id noun)	;commented by 14anu-ban-01
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id raKA_huA))	;changed "id-wsd_root_mng to "id-wsd_word_mng"""raKI"	to "raKA_huA" by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest17   "  ?id " raKA_huA )" crlf))	;changed "raKI"	to "raKA_huA" by 14anu-ban-01
)


;@@@ Added by 14anu-ban-10 on (15-11-2014)
;If it releases the string, it comes to rest.[ncert corpus]
;yaxi vaha dorI ko Coda xewI hE wo kuCa kRaNa bAxa kAra ruka jAwI hE.[ncert corpus]
(defrule rest18
(declare (salience 6100))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?  ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest18   "  ?id " ruka_jA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-11-2014)
;Fig. 6.7 shows a block attached to a spring and resting on a smooth horizontal surface.[ncert corpus]
;ciwra 6.7 sprifga se saMlagna kisI gutake ko xarSAwA hE jo kisI cikane kREwija pqRTa para virAmAvasWA meM hE. [ncert corpus]
(defrule rest19
(declare (salience 6200))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAmAvasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest19   "  ?id "  virAmAvasWA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-11-2014)
;If we observe the decay from the frame of reference in which the center of mass is at rest, the motion of the particles involved in the decay looks particularly simple; the product particles move back to back with their center of mass remaining at rest as shown in Fig.7.13 (b).[ncert corpus]
;yaxi hama eka Ese sanxarBa Prema se isa kRaya prakriyA ko xeKeM jisameM xravyamAna kenxra sWira ho, wo isameM SAmila kaNoM kI gawi viSeRakara sarala xiKAI padawI hE; uwpanna hue xonoM kaNa eka xUsare kI viparIwa xiSA meM isa prakAra gawimAna howe hEM ki unakA xravyamAna kenxra sWira rahe, jEsA ciwra 7.13 (@b) meM xarSAyA gayA hE.[ncert corpus]
(defrule rest20
(declare (salience 6200))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 be)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest20   "  ?id " sWira)" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-11-2014)
;If we observe the decay from the frame of reference in which the center of mass is at rest, the motion of the particles involved in the decay looks particularly simple; the product particles move back to back with their center of mass remaining at rest as shown in Fig.7.13 (b).[ncert corpus]
;yaxi hama eka Ese sanxarBa Prema se isa kRaya prakriyA ko xeKeM jisameM xravyamAna kenxra sWira ho, wo isameM SAmila kaNoM kI gawi viSeRakara sarala xiKAI padawI hE; uwpanna hue xonoM kaNa eka xUsare kI viparIwa xiSA meM isa prakAra gawimAna howe hEM ki unakA xravyamAna kenxra sWira rahe, jEsA ciwra 7.13 (@b) meM xarSAyA gayA hE.[ncert corpus]
(defrule rest21
(declare (salience 6300))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest21   "  ?id "  sWira)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-01-2015)
;After night rest you can start your adventurous journey in the morning .[tourism corpus]
;रात्रि  विश्राम  के  बाद  सुबह  अपनी  रोमांचक  यात्रा  शुरू  कर  सकते  हैं  ।[tourism corpus]
(defrule rest22
(declare (salience 6400))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 night)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest22   "  ?id "  viSrAma )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-01-2015)
;160 lac acres of land gets irrigated in the wetlands of the Narmada out of which 144 lac acres is in Madhya Pradesh alone , the rest is in Maharashtra and Gujarat .[tourism corpus]
;नदी  के  कछार  में  160  लाख  एकड़  भूमि  सिंचित  होती  है  जिसमें  144  लाख  एकड़  अकेले  मध्यप्रदेश  में  है  ,  शेष  महाराष्ट्र  और  गुजरात  में  है  ।[tourism corpus]
(defrule rest23
(declare (salience 6500))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?id1 )
(id-root ?id1 the)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest23   "  ?id "  SeRa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (03-02-2015)
;On looking at the solidified beauty of stones in this middle age city it appears as if time is resting here .[tourism corpus]
;इस  मध्ययुगीन  नगर  में  पाषाण  के  घनीभूत  सौन्दर्य  को  देख  कर  लगता  है  कि  जैसे-समय  यहाँ  विश्राम  कर  रहा  हो  ।[tourism corpus]
(defrule rest24
(declare (salience 5450))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1 )
(id-root ?id1 time)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest24   "  ?id "  viSrAma )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-02-2015)
;On being afflicted with the mountain sickness disease one should take total rest  .[tourism corpus]
;माऊंटेन सिकनेस बीमारी से ग्रस्त होने पर पूरी तरह से आराम करना चाहिए ।[tourism corpus]
(defrule rest25
(declare (salience 5460))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 take)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest25   "  ?id "  ArAma )" crlf))
)


;--------------------- Default Rules -----------------------

(defrule rest2
(declare (salience 100))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest2   "  ?id "  viSrAma )" crlf))
)


;"rest","VTI","1.ArAma_karanA"
;He is tired & he is resting now.
(defrule rest3
(declare (salience 100))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rest.clp 	rest3   "  ?id "  ArAma_kara )" crlf))
)

;"rest","VTI","1.ArAma_karanA"
;He is tired & he is resting now.
;--"2.SAnwi_praxAna_karanA"   
;May her soul rest in peace.  
;--"3.teka_para_raKanA"   
;He rested his head on a pillow. 
;--"4.suswAnA"    
;I will never rest until I know the truth.
;--"5.kisI_para_nirBara_rahanA"   
;India's hope of a gold medal rested on P.T.Usha.
;--"6.GUra_kara_xeKanA"    
;His eys rested on her face.
;--"7.kAryavAhI_CodZa_xenA"   
;Let the Bofors case rest.
;I rest my case. 
;--"8.Kewa_KAlI_CodZa_xenA"    
;Let this field rest for a year.
;
;



;LEVEL 
;
;
;                 sUwra (nibanXa)
;
;"rest","N","1.viSrAma"
;Sunday is a day of rest for many.
;--"2.teka/sahArA"      ------ < viSrAma
;                       (viSrAma meM isakI AvaSyakawA)
;An arm rest.
;A rest for a telephone receiver.
;--"3.virAma"   ----------- < viSrAma
;                  (--ke samaya anya kAryoM ko virAma lagawA hE)
;The trumpets have six bar's rest.
;--"4.gawihIna"  ------- < viSrAma
;                 (--ke samaya gawihInawA howI hE)
;The football is lying at rest in the field.
;
;--"5.xaPanAnA" ------ < viSrAma
;        (mqwa vyakwi ko sampUrNa viSrAma meM gayA huA mAnA jAwA hE)
;The king was laid to rest beside her late Queen.
;
;"rest","N","1.SeRa"  ----- < viSrAma
;     (--ke samaya saBI kAryoM ko SeRa raKA jAwA hE)                
;Eat what you wish && leave the rest.
;He did the rest of the work.
;
;"rest","VTI","1.ArAma_karanA"  
;He is tired && he is resting now.
;--"2.SAnwi_praxAna_karanA" -------- < SAnwi kA milanA < viSrAma
;           (viSrAma se SAnwi milawI hE)
;May her soul rest in peace.
;--"3.teka_para_raKanA"  --------- < teka kI AvaSyakawA < viSrAma
;         (viSrAma meM kisI ko AXAra banAyA jAwA hE)
;He rested his head on a pillow.
;
;--"4.suswAnA"  ------ < viSrAma
;I will never rest until I know the truth.
;--"5.kisI_para_nirBara_rahanA"   ------- < AXAra < viSrAma
;          (--meM kisI ko AXAra banAyA jAwA hE,usase nirBarawA)
;India's hope of a gold medal rested on P.T.Usha.
;--"6.GUra_kara_xeKanA"   ------ < tikanA < viSrAma
;      (--se kisI para tikanA <--GUrane meM kisI para najara kA tikanA)
;His eys rested on her face.
;--"7.kAryavAhI_CodZa_xenA"  ---- < viSrAma
;        (---meM kiye jAwe hue kriyA ko CodA jAwA hE)
;Let the Bofors case rest.
;I rest my case.
;--"8.Kewa_KAlI_CodZa_xenA"   ---- < viSrAma
;      (pahale prAyaH saBI KewI kare We,---ke samaya Kewa KAlI CodA jAwA WA)
;Let this field rest for a year.
;--------------------------------------------------------
;
;sUwra : viSrAma`
;--------
;
;arWa-viswAra ke saMGaTaka sUwra--
;-- pUrA arWa-viswAra viSrAma se hI huA hE . viSrAma meM sAXana ke rUpa meM 
;prayukwa vaswuoM ke rUpakoM xvArA, viSrAma meM honevAlI kriyAoM xvArA, 
;viSrAma se praBAviwa vaswuoM va kriyAoM ke AXAra para arWa-viswAra ko xeKA jA 
;sakawA hE .
;-- Upara kI tippaNiyoM xvArA waWya spaRta howe hEM . 
;-- anya BARAoM meM BI isa prakAra ke prayoga sAmAnya hEM . jEse-
;  --Kewa KAlI CodanA yA kAryavAhI Codane ke sanxarBa meM-
;praSna- kAma nahIM ho rahA hE kyA ? yA are Kewa KAlI kEse xIKa rahA hE ?
;uwwara- karmacArI, kisAna yA majaxUra ArAma ParamA(kara) rahe hEM .
;           yA Kewa ko KAlI xeKakara-
;   kisAna ArAma kara rahA hE . 
;Ese prayoga Ama hEM .
; anya sanxarBoM ko BI isI waraha samaJA jA sakawA hE .
;
