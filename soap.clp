
(defrule soap0
(declare (salience 5000))
(id-root ?id soap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAbuna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soap.clp 	soap0   "  ?id "  sAbuna )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;Example and translation completed by 14anu-ban-01 on (07-01-2014)
;Successful soaps like Dallas, Bold and Beautiful and Hum Log hyped the impossible and dramatised human frailties while sticking to the age-old formulas of love, power and betrayal.[http://indiatoday.intoday.in/story/tv-mothers-in-law-seem-modern-and-caring-but-beneath-the-superscript-is-a-jaded-stereotype/1/220774.html]
;"डैलस", "बोल्ड ऐंड ब्यूटिफल" और "हम लॊग" जैसे सफल चलचित्रों ने असंभव बातों का अतिशयोक्ति पूर्ण प्रचार करते हुए मानवीय कमजोरियों का नाटकीय चित्रण किया किन्तु साथ ही वे प्रेम, शक्ति और धोखेबाजी के कई दशक पुराने सूत्रों से चिपके रहे.[Self: suggested by Chaitanya Sir]
;Successful soaps like Dallas , Bold and Beautiful and Hum Log hyped the 
;डलस , बोल्ड ऐंड यूटीफुल और हम लग मानवीय कमजोरियों का कल्पनातीत - नाटकीय चित्रण ही थे , साथ ही उनमें प्रेम , शैक्त और छल के आजमाए फार्मूले का प्रयोग था .
;@@@ Added by 14anu11
(defrule soap2
(declare (salience 5000))
(id-root ?id soap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-like_saMbanXI  ?id ?id1)
(kriyA-subject  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calaciwra));changed "nAtakIya_ciwraNa" to "calaciwra" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soap.clp 	soap2   "  ?id "  calaciwra )" crlf));changed "nAtakIya_ciwraNa" to "calaciwra" by 14anu-ban-01 on (12-01-2015)
)


;"soap","N","1.sAbuna"
;Every one in a family should have a separate soap.
;
(defrule soap1
(declare (salience 4900))
(id-root ?id soap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAbuna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soap.clp 	soap1   "  ?id "  sAbuna_lagA )" crlf))
)

;"soap","V","1.sAbuna_lagAnA"
;Ram soaped his hand when a insect touch the hand.
;
