;giving complete attention, or showing complete involvement, or (of attention) complete: 
;-----------------------------------------------------------------------
;She sat with a wrapped expression reading her book. [cambridge dictionary]
;The children watched with wrapped attention. [cambridge dictionary]

;extremely pleased
;---------------
;The minister declared that he was wrapped. [oxford learner's dictionary]

;@@@ Added by Anita-31.5.2015
;She sat with a wrapped expression reading her book. [cambridge dictionary]
;वह लीन अभिव्यक्ति के साथ बैठ कर अपनी किताब पढ़ रही  थी ।
;The children watched with wrapped attention. [cambridge dictionary]
;बच्चों ने ध्यान में लीन होकर देखा ।
(defrule wrapped0
(declare (salience 1000))
(id-root ?id wrapped)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrapped.clp 	wrapped0   "  ?id "  lIna )" crlf))
)
;@@@ Added by Anita-31.5.2015
;The minister declared that he was wrapped. [oxford learner's dictionary]-parse prob.
;मंत्री ने घोषित किया कि वह बहुत खुश था ।
(defrule wrapped1
(declare (salience 2000))
(id-root ?id wrapped)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?kri ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_KuSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrapped.clp 	wrapped1   "  ?id " bahuwa_KuSa )" crlf))
)

;@@@ Added by 14anu-ban-11 on 3.9.2014
;he province of five major rivers Punjab and contiguous Haryan also have several lakes wrapped with that .(Tourism corpus)
;पाँच  प्रमुख  नदियों  का  प्रांत  पंजाब  और  उनसे  जुड़ा  हरियाणा  भी  अपनी  गोद  में  कई  झीलें  समेटे  हुए  है  
(defrule wrapped2
(declare (salience 2100))
(id-root ?id wrapped)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samete))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrapped.clp 	wrapped2   "  ?id " samete )" crlf))
)
