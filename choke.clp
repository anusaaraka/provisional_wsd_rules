;@@@FILE ADDED BY 14anu18 (25-6-14)

;He choked.
;उसकी साँस रुकी.
;@@@ Added by 14anu18 (25-06-14)
(defrule choke0
(declare (salience 5000))
(id-root ?id choke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAMsa_ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  choke.clp    choke0   "  ?id " sAMsa_ruka   )" crlf))
)

;$$$Modified by 14anu-ban-03 (15-01-2015)
;NOTE: there is a parser problem the is correct.
;@@@ Added by 14anu18 (25-06-14)
;The roads were choked due to the traffic.
;सडकें यातायात से जाम हुई थी. 
(defrule choke1
(declare (salience 5000))
(id-root ?id choke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-subject ?id ?) 		;commented by 14anu-ban-03 (15-01-2015)
(viSeRya-viSeRaNa  ?id1 ?id) 	;added by 14anu-ban-03 (15-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAma_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  choke.clp    choke1   "  ?id " jAma_ho   )" crlf))
)

;Default rule
;@@@ Added by 14anu18 (25-06-14)
;The choke was to be changed.
;चोक बदला जाना था . 
(defrule choke2
(declare (salience 5000))
(id-root ?id choke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id coka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  choke.clp    choke2   "  ?id " coka   )" crlf))


)


