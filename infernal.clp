;@@@ Added by 14anu-ban-06 (09-04-2015)
;Stop that infernal noise! (OALD)
;वह खिझाने वाला शोर रोकिए! (manual)
(defrule infernal1
(declare (salience 2000))
(id-root ?id infernal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 noise)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KiJAne_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infernal.clp 	infernal1   "  ?id "  KiJAne_vAlA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (09-04-2015)
;He described a journey through the infernal world. (cambridge)
;उसने नारकीय विश्व में यात्रा के बारे में बताया . (manual)
(defrule infernal0
(declare (salience 0))
(id-root ?id infernal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nArakIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infernal.clp 	infernal0   "  ?id "  nArakIya )" crlf))
)
