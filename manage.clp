


;Added by Meena(1.9.09)
;I finally managed to get a word in.
;I managed to go.
(defrule manage0
(declare (salience 5000))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp     manage0   "  ?id "  pA )" crlf)
)
)


(defrule manage1
(declare (salience 5000))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBAla))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp     manage1   "  ?id "  saMBAla )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  manage.clp       manage1   "  ?id " ko )" crlf)
)
)

;@@@ Added by Nandini(1-1-14)
;Sohan and a few friends just managed to escape with their lives.[via mail]
;sohana Ora kuCa miwra apanI jAna bacAne meM saPala hue.
(defrule manage2
(declare (salience 5050))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp     manage2   "  ?id "  saPala_ho )" crlf)
)
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 24.07.2014
;The tourism department has marked thirty tourist spots like these , which will be managed by these NGOs and voluntary organizations.[PARALLEL CORPUS]
;paryatana viBAga ne Esa wIsa paryatana sWaloM ko cinhita kiya hE, jo ina enajio Ora svayaMsevI saMsWAoM xvAra saMcAliwa kiye jAyeMge.

;Places of boarding are available in Traveller Boarding and Rest House managed by the Shrine Board in Katra  and Dak Bunglows and Rest centres managed by Jammu and Kashmir tourism development corporation at suitable rates.[PARALLEL CORPUS]
;कटरा  में  स्थापन  बोर्ड  (  Shrine  Board  )  द्वारा  संचालित  यात्री  निवास  और  विश्राम  गृह  और  जम्मू  और  कश्मीर  पर्यटन  विकास  निगम  द्वारा  संचालित  डाक  बंगलों  और  विश्राम  केंद्रों  पर  उचित  मूल्य  पर  ठहरने  का  स्थान  उपलब्ध  है  ।
(defrule manage3
(declare (salience 5000))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(or(id-root ?id1 agency|organisation|NGOs|Board|centre|corporation)(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcAliwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp 	manage3   "  ?id "  saMcAliwa )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 24.07.2014
;I cannot think that I can be there by six but I will try to manage.
;mEM nahIM samaJawa ki mEM vahAz Caha baje waka raha pAUMgA para mEM koI upAya nikAlane kI koSiSa karuMgA 
(defrule manage4
(declare (salience 5000))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
( or(id-root ?id1 I|we|he|she)(id-cat_coarse ?id1 PropN))
(kriyA-kriyArWa_kriyA  ?id2 ?id)
(id-root ?id2 try)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upAya_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp 	manage4   "  ?id "  upAya_nikAla)" crlf))
)

;@@@Added by 14anu-ban-08 (23-02-2015)
;They managed to patch up a deal. [oald]
;वे डील तय करने में सफल रहे.  [self]
(defrule manage5
(declare (salience 5051))
(id-root ?id manage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 patch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPala_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manage.clp 	manage5   "  ?id "  saPala_raha )" crlf))
)
