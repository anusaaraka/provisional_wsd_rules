
(defrule groom0
(declare (salience 5000))
(id-root ?id groom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  groom.clp 	groom0   "  ?id "  saIsa )" crlf))
)

;"groom","N","1.saIsa"
;Call the groom for taking the horse to the field.
;
;Modified by 14anu21 on 04.07.2014 by deleting the rule groom1
;And added rule groom2 
;The horses are all well fed and groomed.[oxford]
;घोडे खाना खिलाए गये हैं सब अच्छा और घोडे की देखरेख किए गये हैं . 
;;घोड़ो को अच्छे से खाना खिलाया गया और उनको साफ़ किय गया|
;The dogs are all well fed and groomed.[self]
;;कुत्ते खाना खिलाए गये हैं सब अच्छा और घोडे की देखरेख किए गये हैं . 
;कुत्तों को अच्छे से खाना खिलाया गया और उनको साफ़ किय गया|
;(defrule groom1
;(declare (salience 4900))
;(id-root ?id groom)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id GodZe_kI_xeKareKa_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " 
; groom.clp 	groom1   "  ?id "  GodZe_kI_xeKareKa_kara )" crlf))
;)

;@@@ Added by 14anu21 on 04.07.2014 but either ?id1 or ?id2 has to be animal
;need for animals.gdbm
;animate.gdbm is not sufficient.

;The horses are all well fed and groomed.[oxford]
;घोडे खाना खिलाए गये हैं सब अच्छा और घोडे की देखरेख किए गये हैं .
;घोड़ो को अच्छे से खाना खिलाया गया और उनको साफ़ किय गया|
;The dogs are all well fed and groomed.[self]
;कुत्ते खाना खिलाए गये हैं सब अच्छा और घोडे की देखरेख किए गये हैं . 
;कुत्तों को अच्छे से खाना खिलाया गया और उनको साफ़ किय गया|

(defrule groom2
(declare (salience 5000))
(id-root ?id groom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id2)(kriyA-object ?id ?id1))
;either ?id1 or ?id2 has to be  animal
;need for animals.gdbm
;animate.gdbm is not sufficient.
;
(or(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))  
(or(id-root ?id1 monkey|dog|ape|horse)(id-root ?id2 monkey|dog|ape|horse))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAPa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  groom.clp 	groom2   "  ?id "  sAPa_kara )" crlf))
)


;@@@ Added by 14anu21 on 04.07.2014
;Our junior employees are being groomed for more senior roles. [oxford]
;हमारे जूनियर कर्मचारी अधिक वरिष्ठ भूमिकाओं के लिए घोडे की देखरेख किए जा रहे हैं . [Rule groom1 was getting fired before adding rule groom3]
;हमारे जूनियर कर्मचारी अधिक वरिष्ठ भूमिकाओं के लिए तैयार किए जा रहे हैं . 
;The eldest son is being groomed to take over when his father dies.[oxford]
;बडा लडका ले लेना घोडे की देखरेख किया जा रहा है जब उसका पिता मर जाता है . [Rule groom1 was getting fired before adding rule groom3]
;ज्येष्ठ पुत्र  को अपने पिता की मृत्यू के बाद उनकी जगह लेने के लिए तैयार किया जा रहा है.

(defrule groom3
(id-root ?id groom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id2)(kriyA-object ?id ?id1))
(or(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  groom.clp 	groom3   "  ?id "  wEyAra_kara )" crlf))
)
;"groom","VT","1.GodZe_kI_xeKareKa_karanA"
;The horses were groomed for the race.
;

;@@@ Added by 14anu-ban-05 on (27-02-2015)
;The bride and groom walked down the aisle together.	[cald]
;दूल्हा और दुल्हन एक साथ गलियारे  में नीचे चले गये.			[manual]
(defrule groom4
(declare (salience 5005))
(id-root ?id groom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 walk)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUlhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  groom.clp 	groom4   "  ?id "  xUlhA )" crlf))
)
