;$$$ Modified by 14anu-ban-04 (08-11-2014)
;### [COUNTER EXAMPLE]  Recent (2008) studies comparing age of peanut introduction in Great Britain with introduction in Israel appear to show that delaying exposure to peanuts can dramatically increase the risk of developing peanut allergies.    [agriculture]
;### [COUNTER EXAMPLE] हाल ही  हुए (2008) अध्ययन में ग्रॆट ब्रिटन में मूँगफली के परिचय की तुलना इस्रैल  के साथ (में परिचय ) करते हुए यह दिखाता  है कि मूँगफली  के अरक्षितता में देरी करना प्रभावशाली तरीके से मूँगफली एलर्जि बढ़ने के खतरे को बढा सकता है |           [self]
;Added by Meena(16.11.10)
(defrule develop00
(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id developing )
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 country|nation)           ;added by 14anu-ban-04 (08-11-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vikAsaSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  develop.clp    develop00   "  ?id "  vikAsaSIla )" crlf))
)



(defrule develop0
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id developing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vikAsaSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  develop.clp  	develop0   "  ?id "  vikAsaSIla )" crlf))
)




;"developing","Adj","1.vikAsaSIla"
;India is a developing country.
(defrule develop1
(declare (salience 4900))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id developed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vikasiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  develop.clp  	develop1   "  ?id "  vikasiwa )" crlf))
)



;@@@ Added by Pramila(Banasthali University)
;Over time, their acquaintance developed into a lasting friendship.                     ;cald
;समय के साथ ,उनकी जान पहचान एक लम्बी दोस्ती में बदल गयी .
;The fear is that these minor clashes may develop into all-out confrontation.          ;cald
;इस बात का डर है कि छोटे झगड़े कही बड़े विवादों में ना बदल जाए .
(defrule develop2
(declare (salience 4800))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop2   "  ?id "  baxala_jA )" crlf)
)
)

;@@@ Added by Pramila(Banasthali University)
;The company is spending $650 million on developing new products.
;कंपनी ६५० मिलियन नये माल को बनाने के लिए खर्च कर रहीं है.
(defrule develop3
(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 product)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop3   "  ?id "  banA )" crlf)
)
)


;@@@ Added by Pramila(Banasthali University)  on 04-02-2014
;Sadler developed the highest opinion about Asutosh and perhaps it would be correct to say that he benefited by Asutosh's vast knowledge 
;of the educational field in this country.           ;gyannidhi
;सैडलर ने उनके बारे में बहुत अज्छी राय कायम की और शायद यह कहना सही होगा कि देश में शिक्षा के क्षेत्र में आशुतोष की विस्तृद जानकारी से उन्होंने बाभ उठाया।।।
(defrule develop4
(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 opinion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAyama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop4   "  ?id "  kAyama_kara )" crlf)
)
)

;$$$ Modified by 14anu-ban-06 (20-10-2014)
;### COUNTER EXAMPLE For example, the subject of thermodynamics, developed in the nineteenth century, deals with bulk systems in terms of macroscopic quantities such as temperature, internal energy, entropy, etc.(NCERT)
;### COUNTER EXAMPLE उदाहरण के लिए, उन्नीसवीं शताब्दी में विकसित विषय ऊष्मा गतिकी बृहदाकार निकायों के साथ ताप, आन्तरिक ऊर्जा, एन्ट्रापी आदि जैसी स्थूल राशियों के पदों में व्यवहार करता है.(NCERT)
;@@@ Added by Pramila(Banasthali University)  on 12-03-2014
;The equations developed in this chapter for motion in a plane can be easily extended to the case of three dimensions.  ;ncert
; इस अध्याय में प्राप्त समीकरणों को आसानी से त्रिविमीय गति के लिए विस्तारित किया जा सकता है . .... transformed translation
;हम इस अध्याय में जिन समीकरणों को प्राप्त करेंगे उन्हें आसानी से त्रिविमीय गति के लिए विस्तारित किया जा सकता है . ....   ;original translation
(defrule develop5
(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 equation);added by 14anu-ban-06 (20-10-2014)
=>
(retract ?mng)
;(assert (id-wsd_word_mng ?id prApwa))
(assert (id-wsd_word_mng ?id prApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  develop.clp 	develop5   "  ?id "  prApwa)" crlf)
)
)
;@@@ Added by 14anu-ban-01 on 1-08-14. ;to begin to have something such as a disease or a problem.
;Her son developed asthma when he was two.[oald]
;usake bete ko SvAsaroga SurU ho gayA jaba vaha 2 sAla kA WA.
;The car developed engine trouble and we had to stop.[oald]
;gAdI meM iMjana kI pareSAnI SurU huI Ora hameM rukanA padA.
;Most people have relatively mild symptoms , but a sizable minority goes on to develop persistent pain and severe limitation in jaw motion .[Karan Singla]
;aXikawara logoM ke roga lakRaNa bahuwa gaMBIra nahiM hote,magara kuCa ko jZixxI xarxa SurU ho jAwA hE Ora jabadoM ko calAne meM bahuwa xikkawa hone lagawI hE.[manual]
(defrule develop7
(declare (salience 5000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 pain|asthma|trouble);to begin to have something such as a disease or a problem.
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SurU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop7  "  ?id "  SurU_ho )" crlf)
)
)

;@@@ Added by Vivek Agarwal(14anu06), MNNIT Allahabad on 1/7/2014*****
;Develop a taste for opera.	(Source: thefreedictionary.com )
;ओपेरा के लिए पसंद का विकास कीजिए . 
;ओपेरा के लिए रुचि विकसित कीजिए .           ;tanslation corrected by 14anu-ban-04 (10-01-2015)
(defrule develop07
(declare (salience 4900))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_kriyA  ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop07   "  ?id "  vikasiwa_ho )" crlf)
)
)

;$$$ Modified by 14anu-ban-04 (10-01-2015) 
;@@@ Added by Vivek Agarwal(14anu06), MNNIT Allahabad on 1/7/2014*****
;In between them developed a friendship.	(Source: thefreedictionary.com )
;उनके बीच मैत्री विकिसत हुई . 
(defrule develop08
(declare (salience 4950))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 friendship|hardship)         ; modified ?id as 'id1' by 14anu-ban-04 (10-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop08   "  ?id "  vikasiwa_ho )" crlf)
)
)

;This rule is commented by 14anu-ban-04 on (10-01-2015) because it is unecessary as the correct meaning is given by the rule develop7
;@@@ Added by Vivek Agarwal(14anu06), MNNIT Allahabad on 1/7/2014*****
;The car developed engine trouble and we had to stop.	[oald]
;गाडी में इंजन परेशानी हुई और हमें रुकना पडा .
;(defrule develop09
;(declare (salience 4950))
;(id-root ?id develop)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-subject  ?id ?id1)
;(id-root ?id1 car|engine|automobile|bike|truck)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ho))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop09   "  ?id "  ho )" crlf)
;)
;)


;@@@ Added by 14anu-ban-06 (20-10-2014)
;For example, the subject of thermodynamics, developed in the nineteenth century, deals with bulk systems in terms of macroscopic quantities such as temperature, internal energy, entropy, etc.(NCERT)
;उदाहरण के लिए, उन्नीसवीं शताब्दी में विकसित विषय ऊष्मा गतिकी बृहदाकार निकायों के साथ ताप, आन्तरिक ऊर्जा, एन्ट्रापी आदि जैसी स्थूल राशियों के पदों में व्यवहार करता है.(NCERT)
(defrule develop8
(declare (salience 4800))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-in_saMbanXI ?id ?id1)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 subject)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop8   "  ?id "  vikasiwa )" crlf)
)
)


;@@@ Added by 14anu-ban-04 (08-11-2014)
;The SI, with standard scheme of symbols, units and abbreviations, was developed and recommended by General Conference on Weights and Measures in 1971 for international usage in scientific, technical, industrial and commercial work.   [NCERT-CORPUS]
;SI प्रतीकों, मात्रकों और उनके सङ्केताक्षरों की योजना 1971 में, मापतोल के महा सम्मेलन द्वारा विकसित किया था, वैज्ञानिक, तकनीकी, औद्योगिक एवं व्यापारिक कार्यों में अन्तर्राष्ट्रीय स्तर पर उपयोग हेतु अनुमोदित की गई.        [NCERT-CORPUS]
;1971 में मापतोल के महा सम्मेलन द्वारा SI प्रतीकों, मात्रकों और  सङ्केताक्षरों को  विकसित किया था  और अनुमोदित   किया था अन्तर्राष्ट्रीय स्तर पर वैज्ञानिक, तकनीकी, औद्योगिक एवं व्यापारिक कार्यों में उपयोग हेतु |    [SELF (14anu-ban-04)]   

(defrule develop10
(declare (salience 4100))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-in_saMbanXI ?id ?id1)(kriyA-by_saMbanXI ?id ?id1)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop10   "  ?id "  vikasiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (08-11-2014)
;If the load is increased further, the stress developed exceeds the yield strength and strain increases rapidly even for a small change in the stress. [NCERT CORPUS]
;यदि भार को और बढा दिया जाए तो उत्पन्न  प्रतिबल पराभव सामर्थ्य से अधिक हो जाता है और फिर प्रतिबल में थोडे से अन्तर के लिए भी विकृति तेजी से बढती है. [NCERT CORPUS]
;The restoring force per unit area developed due to the applied tangential force is known as tangential or shearing stress.[NCERT CORPUS]
;लगाए गए स्पर्शी बल के कारण एकांक क्षेत्रफल पर उत्पन्न  प्रत्यानयन बल को स्पर्शी या अपरूपण प्रतिबल कहते हैं.      [NCERT CORPUS]

(defrule develop11
(declare (salience 4800))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 stress|area)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop11 "  ?id "  uwpanna )" crlf)
)
)

;@@@ Added by 14anu-ban-04 (08-01-2015)
;It might cause someone to develop the slimmest suspicion about our true activities.   [slim.clp (slim2)]
;यह किसी भी व्यक्ति के मन में हमारी वास्तविक गतिविधियों को लेकर शक पैदा कर सकता है.    [manual]
;यह किसी को भी मज़बूर कर सकता है हमारी वास्तविक गतिविधियों के बारे में शक पैदा करने के लिए.        [manual]
(defrule develop12
(declare (salience 4800))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object   ?id ?id1)
(id-root ?id1 suspicion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop12 "  ?id "  pExA_kara )" crlf)
)
)


;---------------------default rules----------------------------------
;"developed","Adj","1.vikasiwa"
;The USA is a developed country.
;
;
(defrule develop6
(declare (salience 4000))
(id-root ?id develop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikAsa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  develop.clp 	develop6   "  ?id "  vikAsa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  develop.clp   develop6   "  ?id " kA )" crlf)
)
)

;default_sense && category=verb	vikasiwa_kara	0
;"develop","VT","1.vikasiwa_karanA"
;Her company developed a new kind of building material that withstands all kinds of weather.
;
;


