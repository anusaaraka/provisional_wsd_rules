;@@@ Added by 14anu-ban-06 (09-04-2015)
;Only a small amount of the rainwater actually infiltrates into the soil.(OALD)
;वर्षा का सिर्फ थोडा जल वास्तव में मिट्टी में छन-छन कर अन्दर जाता है .(manual) 
(defrule infiltrate1
(declare (salience 2000))
(id-root ?id infiltrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 soil)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cana-Cana_kara_anxara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infiltrate.clp 	infiltrate1   "  ?id "  Cana-Cana_kara_anxara_jA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (09-04-2015)
;The CIA agents successfully infiltrated into the terrorist organizations. (OALD)
;सीआईऐ प्रतिनिधियों ने सफलतापूर्वक आतंकवादी संगठनों में घुसपैठ की . (manual)
(defrule infiltrate0
(declare (salience 0))
(id-root ?id infiltrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GusapETa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infiltrate.clp 	infiltrate0   "  ?id "  GusapETa_kara )" crlf))
)
