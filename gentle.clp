
(defrule gentle0
(declare (salience 5000))
(id-root ?id gentle)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gentle.clp 	gentle0   "  ?id "  maMxa )" crlf))
)

(defrule gentle1
(declare (salience 4900))
(id-root ?id gentle)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gentle.clp 	gentle1   "  ?id "  suSIla )" crlf))
)

;"gentle","Adj","1.suSIla"
;Ram is a very gentle person.
;--"2.sOmya"
;A gentle breeze is blowing across the beach.
;
;


;@@@ Added by 14anu-ban-01 pn 02-08-2014.
;78765:She should be gentle and mild in temperament .
;स़्वभाव से उसे शाऩ्त व विनम्र होना चाहिए .
(defrule gentle2
(declare (salience 5100))
(id-root ?id gentle)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 temperament)
(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gentle.clp 	gentle2   "  ?id "  SAnwa)" crlf))
)

;@@@ Added by 14anu-ban-01 on 02-08-2014:in anusaaraka-->meaning mentioned is 'suSIla' but i found 'saBya' to be a better choice.
;Ram is a very gentle person.[Anusaaraka]
;RAma eka awyanwa saBya vyakwi hE.[manual]
;78111:Today also world's millions of Musalmans believe that Kuran and Hazarat mohammad have built a gentle community .[Karan Singla]
;आज भी दुनिया के करोड़ों मुसलामान मानते है कि कुरान और हज़रत मुहम्मद सल्ल . ने एक सभ्य समाज की रचना की ।[changed 'AxrSa' to 'saBya']

(defrule gentle3
(declare (salience 5000))
(id-root ?id gentle)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 person|man|boy|community)
(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saBya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gentle.clp 	gentle3   "  ?id "  saBya)" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-01-2015)
;The path has a gentle slope.  [Cambridge]
;पथ में थोड़ी ढाल है.		       [manual]
(defrule gentle4
(declare (salience 5000))
(id-root ?id gentle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 slope)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gentle.clp 	gentle4   "  ?id "  WodZA)" crlf))
)	

