
;@@@ Added by 14anu-ban-03 (28-04-2015)
;They contrived a plan to defraud the company. [oald]
;उन्होने कम्पनी को धोखा देने  की योजना बनाई .  [manual] 
(defrule contrived1
(declare (salience 100))
(id-root ?id contrived)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 plan)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrived.clp 	contrived1   "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu-ban-03 (28-04-2015)
;They contrived to murder their boss. [contrive.clp]
;उन्होने अपने बॉस को मारने की कोशिश की .  [manual] 
(defrule contrived2
(declare (salience 100))
(id-root ?id contrived)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 murder)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koSiSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrived.clp 	contrived2   "  ?id "  koSiSa_kara )" crlf))
)

;--------------------------------- Default Rules ----------------------------------

;@@@ Added by 14anu-ban-03 (28-04-2015)
;She contrived to spend a couple of hours with him every Sunday evening. [oald]
;उसने प्रत्येक रविवार की सन्ध्या को उसके साथ कुछ घण्टे बिताने का उपाय निकाला . [manual] 
(defrule contrived0
(declare (salience 00))
(id-root ?id contrived)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upAya_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrived.clp        contrived0  "  ?id "  upAya_nikAla )" crlf))
)

