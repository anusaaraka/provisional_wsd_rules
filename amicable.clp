;@@@Added by 14anu-ban-02(03-03-2015)
;Sentence: An amicable relationship.[oald]
;Translation: मैत्रीपूर्ण रिश्ता.[self]
(defrule amicable0 
(declare (salience 0)) 
(id-root ?id amicable) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id mEwrIpUrNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amicable.clp  amicable0  "  ?id "  mEwrIpUrNa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(03-03-2015)
;An amicable settlement was reached.[oald]
;एक सौहार्दपूर्ण समझौता हो गया. [self]	;suggested by chaitanaya sir.
(defrule amicable1 
(declare (salience 100)) 
(id-root ?id amicable) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 settlement) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sOhArxapUrNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amicable.clp  amicable1  "  ?id "  sOhArxapUrNa )" crlf)) 
) 
