
(defrule think0
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id thinking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vicAra_karawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  think.clp  	think0   "  ?id "  vicAra_karawA_huA )" crlf))
)

;"thinking","N","1.vicAra_karane_kI_kriyA"
;My way of thinking is totally different.
;

;@@@ Added by Prachi Rathore[24-1-14]
;I keep thinking back to the day I arrived here.[oald]
;मैं याद करना जारी रखता हूँ वो दिन जब मैं यहाँ आया . 
(defrule think3
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 back)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 yAxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think3  "  ?id "  " ?id1 " yAxa_kara )" crlf))
)


;@@@ Added by Prachi Rathore[24-1-14]
;He'd like more time to think things over.[oald]
;वह चीजों पर विचार करने के लिए अधिक समय चाहेगा . 
(defrule think4
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 over)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think4  "  ?id "  " ?id1 " vicAra_kara )" crlf))
)


;@@@ Added by Prachi Rathore[24-1-14]
;Can't you think up a better excuse than that?[oald]
;क्या आप उसकी अपेक्षा एक अधिक बेहतर बहाना नहीं खोज सकते हैं ? 
(defrule think5
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think5  "  ?id "  " ?id1 " Koja )" crlf))
)

;@@@ Added by Prachi Rathore[24-1-14]
;This new building accommodated the post-graduate classes including the classes of the University Law College, the University Library, and the administrative offices of the University.Asutosh Mukherjee pointed out the impropriety of the proposal to the Chancellor, Lord Minto, and asked that the University might be permitted to deal with this matter as it thought proper. [gyan-nidhi]
;आशुतोष मुकर्जी ने प्रस्ताव के अनुपयुक्तता की ओर कुलाधिपति लार्ड मिंटो का ध्यान आकर्षित किया और अग्रतह के इस मामले को जैसा विश्वविद्यालय उचित समझे वैसे ही निपटाने की अनुमति दी जाये।
(defrule think6
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(subject-subject_samAnAXikaraNa  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  think.clp 	think6   "  ?id "  samaJa )" crlf))
)

;@@@ Added by Prachi Rathore[24-1-14]
;I've been thinking over what you said.[oald]
;आपने जो कहा  मैं उस पर विचार कर रहा हूँ . 
(defrule think7
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI  ?id ?)
(id-root ?id1 over)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think7  "  ?id "  " ?id1 " vicAra_kara )" crlf))
)
;given_word=thinking && word_category=noun	$vicAra_karane_kI_kriyA)

;@@@ Added by 14anu11
;Basava compared them to a devotee who left his shoes outside and entered the temple . He stood before God , no doubt ; but he 
;worried only about the safety of his shoes and never thought of God .
;बसव ने उनकी तुलना उस उपासक से की जो अपने जूते मंदिर के बाहर उतारकर देवता के सम्मुख खडा तो है मगर मन में चिंता जूतों की ही बनी हुई है कि कहीं चोरी न चले जाएं .
(defrule think8
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(subject-subject_samAnAXikaraNa  ?id1 ?)
(kriyA-of_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XayAn))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  think.clp 	think8   "  ?id "  XayAn )" crlf))
)

;@@@ Added by 14anu20 on 23/06/2014
;There is no need to be thinking about it.
;इसके बारे में लम्बे समय तक सोचने की जरूरत नहीं है . 
;इसके बारे में सोचते रहने कोई नहीं जरूरत है . (anusaaraka)
(defrule think23_1
(declare (salience 4900)) ;salience increased from 4800 because of think2 by 14anu-ban-07(22-01-2015)
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 to)
(id-root =(+ ?id1 1) be)
(to-infinitive  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id1 1) socawe_rahane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think23_1  "  ?id "  " (+ ?id1 1) " socawe_rahane )" crlf))
)


;@@@ Added by 14anu-ban-07 (10-10-2014)
;The law of conservation of energy is thought to be valid across all domains of nature, from the microscopic to the macroscopic. [ncert]
;ऊर्जा संरक्षण नियम को प्रकृति के सभी प्रभाव क्षेत्रों, सूक्ष्म से स्थूल तक, के लिए वैध माना गया है. [ncert]
; The efficiency of nutrient uptake by crops from fertilizers or residue release is generally thought to be similar.(agriculture)
;उत्पादक या बचे हुए  अवशेष से फसलों से पोषक तत्व तेज की दक्षता आम तौर पर समान माना जाता है ।(manual)
(defrule think9
(declare (salience 6000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-root ?id1 valid|similar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  think.clp 	think9   "  ?id "  mAna )" crlf))
)

;@@@ Added by 14anu-ban-07,(04-03-2015)
;I need some time to think it through - I don't want to make any sudden decisions.(cambridge)
;मुझे  यह विचार करने के लिए  कुछ समय की आवश्यकता है - मैं कोई जल्द निर्णय बनाना नहीं चाहता हूँ . (manual)
(defrule think10
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 through)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think10  "  ?id "  " ?id1 " vicAra_kara )" crlf))
)
;@@@ Added by 14anu-ban-07,(04-03-2015)
;It's a very well thought out plan.(oald)(parser no. 18)
;यह  एक बहुत अच्छा से  विचार किया हुअा योजना  है .  (manual)
(defrule think11
(declare (salience 5000))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " think.clp  think11  "  ?id "  " ?id1 " vicAra_kara )" crlf))
)


;xxxxxxxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxx
(defrule think1
(declare (salience 4900))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id soca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  think.clp 	think1   "  ?id "  soca )" crlf))
)

(defrule think2
(declare (salience 4800))
(id-root ?id think)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id soca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  think.clp 	think2   "  ?id "  soca )" crlf))
)

;"think","VTI","1.soca"
;Think before you act.
;I think he is very smart.
;--"2.kalpanA_karanA"
;Just think-you could be rich one day.
;--"3.manana_karanA"
;Think of good thoughts.
;
;
