
;"siding","N","1.bagalI relapaWa"
(defrule side0
(declare (salience 5000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id siding )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bagalI_relapaWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  side.clp  	side0   "  ?id "  bagalI_relapaWa )" crlf))
)

;@@@ Added by jagriti(2.12.2013)
;The ingredients are listed on the side of the box. [cambridge dictionary]
;सामग्री बॉक्स की सतह पर सूचीबद्ध हैं.
(defrule side1
(declare (salience 4800))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(or(kriyA-on_saMbanXI ? ?id)(viSeRya-saMKyA_viSeRaNa ?id ?)):commented by 14anu-ban-01 on (17-10-2014)
(kriyA-on_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sawaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side1   "  ?id "  sawaha )" crlf))
)




;@@@ Added by 14anu-ban-01 on (21-10-2014)
;The two vectors and their resultant form three sides of a triangle, so this method is also known as triangle method of vector addition.[NCERT corpus]
;दोनों सदिश तथा उनका परिणामी सदिश किसी त्रिभुज की तीन भुजाएं बनाते हैं;इसलिए इस विधि को सदिश योग के त्रिभुज नियम भी कहते हैं.[NCERT corpus]
;There are four sides in a square.[side.clp]
; समचतुर्भुज में चार भुजाएँ होती हैं.[self]
(defrule side4
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id ?id1)(viSeRya-in_saMbanXI ?id ?id1))
(id-root ?id1 triangle|square|rectangle|polygon);list can be long.
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BujA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side4   "  ?id "  BujA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (21-10-2014)
;There was large crowd on the other side of the road.[side.clp]
;सड़क के उस/दूसरे किनारे पर बहुत  ज्यादा भीड थी.[self]
(defrule side5
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 road)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side5   "  ?id "  kinArA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;All the six sides of a cube are equal.[side.clp]
; क्यूब के छः के छः/सारे फलक बराबर होते हैं.[self]
(defrule side6
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 cube|cuboid)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Palaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side6   "  ?id "  Palaka)" crlf))
)



;@@@ Added by 14anu-ban-01 on (21-10-2014)
;There are faults on both sides.[side.clp]
;दोनों पक्षों/दलों में खोट है.[self]
;दोष दोनों पक्षों/दलों का है.
(defrule side7
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id1 ?id)
(id-root ?id1 fault);list can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa/xala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side7   "  ?id "  pakRa/xala )" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-10-2014)
;The plate is balanced by weights on the other side, with its horizontal edge just over water.[NCERT corpus]
;प्लेट के क्षैतिज निचले किनारे को पानी से थोडा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर सन्तुलित कर लेते हैं.[NCERT corpus]
;In Fig. 9.2(d), a solid sphere placed in the fluid under high pressure is compressed uniformly on all sides.[NCERT corpus]
;चित्र 9.2(d) में, अधिक दाब के किसी द्रव के अन्दर रखा एक ठोस गोला सभी ओर से समान रूप से सम्पीडित हो जाता है..[NCERT corpus]
(defrule side8
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id1 ?id)
;(id-root ?id1 weight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side8   "  ?id "  ora )" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-10-2014)
;For example, if the length and breadth of a rectangle are 1.0 m and 0.5 m respectively, then its perimeter is the sum of the lengths of the four sides, 1.0 m + 0.5 m +1.0 m + 0.5 m = 3.0 m.[NCERT corpus]
;uxAharaNa ke lie, yaxi kisI Ayawa kI lambAI Ora cOdAI kramaSaH 1.0 @m waWA 0.5 @m hE wo usakI parimApa cAroM BujAoM ke yoga, 1.0 @m + 0.5 @m +1.0 @m + 0.5 @m = 3.0 @m hogA.[NCERT corpus]
;The length of each side is a scalar and the perimeter is also a scalar.[NCERT corpus]
;हर भुजा की लम्बाई एक अदिश है तथा परिमाप भी एक अदिश है .[NCERT corpus]

(defrule side9
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-of_saMbanXI ?id1 ?id2)
;(id-root ?id1 sum|difference|product)
(viSeRya-of_saMbanXI ?id2 ?id)
(id-root ?id2 length|breadth|width)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BujA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side9   "  ?id "  BujA )" crlf))
)

;NOTE: 'kA' vibhakti is fired in 'debug_file.dat' but not visible in output because pareser is unable to recognize the relation "kriyA-object" between 'sided with' and 'mother' -->by 14anu-ban-01 on (02-01-2015)
;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu07 0n 03/07/2014
;The kids always sided with their mother against me.[oald]:Added by 14anu-ban-01 on (02-01-2015)
;बच्चे ने मेरे विरुद्ध हमेशा अपनी माँ का पक्ष लिया. [self] 
;They side with whoever they think is powerful:Run sentence on parser no.28-->14anu-ban-01 on (02-01-2015)
;वे उसी का पक्ष लेते हैं जो उनकी नजर में ताकतवर है .
(defrule side10
(declare (salience 100))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) with)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pakRa_le));changed "kA_pakRa_le" to "pakRa_le"  by 14anu-ban-01 on (02-01-2015)
(assert (kriyA_id-object_viBakwi ?id kA));added by 14anu-ban-01 on (02-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  side.clp    side10   "  ?id " kA )" crlf);added by 14anu-ban-01 on (02-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " side.clp    side10  "  ?id "  " (+ ?id 1) "  pakRa_le)" crlf));changed "kA_pakRa_le" to "pakRa_le"  by 14anu-ban-01 on (02-01-2015)
)

;@@@ Added by 14anu-ban-01 on (18-02-2015)
;The deal must be agreeable to both sides.[oald]
;सौदा दोनों पक्षों से स्वीकार्य होना चाहिये.[self]
(defrule side11
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id1 agreeable|acceptable|enjoyable|gratifying|satisfying|delightful)
(viSeRya-to_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side11   "  ?id "  pakRa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015)
;She spoke to both sides in the dispute in an attempt to smooth things over.[oald]
;उसने चीजों को ठीक करने के प्रयास में  विवाद में [डूबे] दोनों पक्षों से बात की . [self]
(defrule side12
(declare (salience 1000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 speak|talk)
(kriyA-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side12   "  ?id "  pakRa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (15-04-2015)
;He lives on the farther side of the town.[side.clp]
;वह शहर के दूर वाले भाग/हिस्से में रहता है.[side.clp]
(defrule side13
(declare (salience 5000))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 live|stay|sit|stand|park|write)
(kriyA-on_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAga/hissA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side13   "  ?id "  BAga/hissA )" crlf))
)

;------------------------ Default Rules ----------------------

;"side","N","1.waraPZa"
;While driving both sides should be watched carefully to avoid accidents.
(defrule side2
(declare (salience 1))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waraPZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side2   "  ?id "  waraPZa )" crlf))
)

;"side","V","1.kA_pakRa_lenA/kA_samarWana_karanA"
;I always sides with my mother against my father.
(defrule side3
(declare (salience 1))
(id-root ?id side)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_pakRa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  side.clp 	side3   "  ?id "  kA_pakRa_le )" crlf))
)


;"side","V","1.kA_pakRa_lenA/kA_samarWana_karanA"
;I always sides with my mother against my father.
;"side","N","1.waraPZa"
;While driving both sides should be watched carefully to avoid accidents.
;--"2.bagZala"
;He was lying on his side.
;--"3.ora"
;All the friends were my side.
;--"4.kinArA"
;There was large crowd on the either side of the road.
;--"5.DZAla"
;All sides of mountains were covered with snow.
;--"6.Palaka"
;All the six sides of a cube are equal.
;--"7.BujA"
;There are four sides in a square.
;--"8.pakRa/xala"
;There are faults on both sides.
;--"9.pahalU"
;We must study all the side of a problem && then try to solve.
;-- "10. Baga/hissA"
;He lives on the farther side of the town.
;vaha Sahara ke xUra vAle Baga/hisse meM rahawA hE.;
