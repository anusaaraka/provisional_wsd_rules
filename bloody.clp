;@@@Added by 14anu-ban-02(31-03-2015)
;That was a bloody good meal![oald]
;वह बहुत ही अच्छा भोजन था! [self]
(defrule bloody1  
(declare (salience 100)) 
(id-root ?id bloody) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 good|bad|awful)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bahuwa_hI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloody.clp  bloody1  "  ?id "  bahuwa_hI )" crlf)) 
)


;@@@Added by 14anu-ban-02(31-03-2015)
;He doesn't bloody care about anybody else.[oald]
;वह किसी के बारे में बिलकुल  चिन्ता नहीं करता है . [self]
(defrule bloody2  
(declare (salience 100)) 
(id-root ?id bloody) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 care)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bilakula)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloody.clp  bloody2  "  ?id "  bilakula )" crlf)) 
)


;-----------------default_rules------------------


;@@@Added by 14anu-ban-02(31-03-2015)
;Sentence: A bloody nose.[cald]
;Translation: लहूलुहान नाक . [self]
(defrule bloody0  
(declare (salience 0)) 
(id-root ?id bloody) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id lahUluhAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloody.clp  bloody0  "  ?id "  lahUluhAna )" crlf)) 
)



