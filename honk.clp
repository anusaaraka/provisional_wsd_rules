;@@@ Added by 14anu-ban-06 (20-03-2015)
;He gave us a honk on his horn as he drove off. (cambridge)
;उसने हमें उसके भोंपू की आवाज  दी जैसे ही वह गाडी से रवाना हुआ. (manual)
(defrule honk2
(declare (salience 5200))
(id-root ?id honk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 horn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honk.clp 	honk2   "  ?id "  AvAja )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-03-2015)
;People honked their horns as they drove past. (OALD)
;लोग अपना भोंपू बजाते हैं जब वे  गाडी चलाते हैं. (manual)
(defrule honk3
(declare (salience 5200))
(id-root ?id honk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 horn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honk.clp 	honk3   "  ?id "  bajA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

(defrule honk0
(declare (salience 5000))
(id-root ?id honk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haMsa_ke_kAzva_kAzva_karane_kI_AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honk.clp 	honk0   "  ?id "  haMsa_ke_kAzva_kAzva_karane_kI_AvAja )" crlf))
)

;"honk","N","1.haMsa_ke_kAzva_kAzva_karane_kI_AvAja"
;AsamAna meM 'honk'(haMsa ke kAzva kAzva karane kI AvAja sunAI xe rahI WI.
;
(defrule honk1
(declare (salience 4900))
(id-root ?id honk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BOMpU_bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  honk.clp 	honk1   "  ?id "  BOMpU_bajA )" crlf))
)

;"honk","V","1.BOMpU_bajAnA"
;purAnI motaroM meM'honk'(BoMpU bajA kara AvAja kI jAwI WI.)
;

