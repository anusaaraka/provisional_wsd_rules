;$$$ Modified by 14anu-ban-09 on (04-08-2014)
; What is the result of adding two equal and opposite vectors? [NCERT CORPUS]
;xo samAna Ora viparIwa saxiSoM ko jodane para kyA pariNAma milawA hE ?
;It is the angle between the two directions when two diametrically opposite points of the planet are viewed through the telescope. [NCERT COPRUS]
;यह ग्रह के दो व्यासतः विपरीत (व्यास के विपरीत सिरों पर स्थित) बिन्दुओं को दूरदर्शक द्वारा देखने पर प्राप्त दो दिशाओं के बीच बना कोण है.
;The answer according to Newton is: Yes, the stone does exert an equal and opposite force on the earth. [NCERT CORPUS] 
;परन्तु न्यूटन के अनुसार इस प्रश्न का उत्तर है: हाँ, पत्थर भी पृथ्वी पर परिमाण में समान तथा दिशा में विपरीत बल लगाता है.
;To every action, there is always an equal and opposite reaction.[NCERT CORPUS]  
;प्रत्येक क्रिया की सदैव समान एवं विपरीत दिशा में प्रतिक्रिया होती है.
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;On the opposite page. [Cambridge]
;विपरीत पृष्ठ पर .
(defrule opposite3
(declare (salience 5500))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse ?id1 noun)
(id-root ?id1 page|corner|effect|end|conclusion|direction|side|end|pair|point|force|reaction|vector) ;added 'side|end|pair|point|force|reaction|vector' by 14anu-ban-09
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite3   "  ?id "  viparIwa )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;The boat was used to ferry the loot across to the opposite bank. [Gyannidhi]
;नाव का उपयोग उन पेटियों को नदी के दूसरी ओर के किनारे तक पहुंचाने के लिए किया गया होगा।
(defrule opposite0
(declare (salience 5000))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse ?id1 noun)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite0   "  ?id "  xUsarI_ora )" crlf))
)



(defrule opposite1
(declare (salience 4900))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite1   "  ?id "  viroXI )" crlf))
)

(defrule opposite2
(declare (salience 4800))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite2   "  ?id "  ke_sAmane )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Is there a bakery opposite your house?
;क्या एक बेकरी के सामने आपका घर है? 
(defrule opposite4
(declare (salience 4600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
(kriyA-kAlavAcI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite4   "  ?id "  ke_sAmane )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;They're complete opposites. 
;वे पूर्णतयाः विपरीत हैं .  
(defrule opposite5
(declare (salience 4500))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite5   "  ?id "  viparIwa )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;We live on opposite sides of the city. [Cambridge] 
;हम शहर की दूसरी तरफ रहते हैं . 
(defrule opposite6
(declare (salience 5600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 side)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xUsarI_waraPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " opposite.clp	opposite6  "  ?id "  " ?id1 "  xUsarI_waraPa )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;They represent opposite sides of the issue. [MW] 
;वे विषय के विपरीत पहलू  का प्रतिनिधित्व करते हैं .  
(defrule opposite7
(declare (salience 5800))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 side)
(id-root ?id2 issue|matter|problem|subject|question|result|concern|topic)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viparIwa_pahalU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " opposite.clp	opposite7  "  ?id "  " ?id1 "  viparIwa_pahalU )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;See opposite for further details. [OALD]
;और अधिक जानकारी के लिये सम्मुख को देखिए . 
(defrule opposite8
(declare (salience 4200))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 see)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite8   "  ?id "  sammuKa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-08-2014)
;Multiplying a vector A by a negative number λ gives a vector λA whose direction is opposite to the direction of A and whose magnitude is -λ times |A|. [NCERT CORPUS]
;सदिश A को यदि एक ऋणात्मक सङ्ख्या λ से गुणा करें तो सदिश λA प्राप्त होता है जिसकी दिशा A की दिशा के विपरीत है और जिसका परिमाण |A| का - λ गुना होता है .

(defrule opposite9
(declare (salience 5000))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 direction)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite9   "  ?id "  viparIwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-08-2014)
;The opposite is true for energy absorbing (endothermic) reactions. [NCERT CORPUS]
;ऊष्मा अवशोषी अभिक्रियाओं में इसका विलोम सत्य है.

(defrule opposite10
(declare (salience 5600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 true)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viloma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite10   "  ?id "  viloma )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-08-2014)
;Force on a negative charge is opposite to that on a positive charge. [NCERT CORPUS] ;added by 14anu-ban-09 on (24-11-2014)
;qNAveSa para lagane vAlA bala XanAveSa para lagane vAle bala ke viparIwa howA hE. [NCERT CORPUS] ;added by 14anu-ban-09 on (24-11-2014)
;Since the magnitudes of the two vectors are the same, but the directions are opposite, the resultant vector has zero magnitude and is represented by 0 called a null vector or a zero vector.[NCERT CORPUS] 
;क्योङ्कि दो सदिशों का परिमाण वही है किन्तु दिशा विपरीत है, इसलिए परिणामी सदिश का परिमाण शून्य होगा और इसे O से व्यक्त करते हैं.

(defrule opposite11
(declare (salience 5600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 direction|force)   ;added 'force' by 14anu-ban-09 on (24-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite11   "  ?id "  viparIwa )" crlf))
)


;@@@ Added by 14anu-ban-09 on (14-08-2014)
;The answer according to Newton is: Yes, the stone does exert an equal and opposite force on the earth.[NCERT CORPUS]
;परन्तु न्यूटन के अनुसार इस प्रश्न का उत्तर है: हाँ, पत्थर भी पृथ्वी पर परिमाण में समान तथा दिशा में विपरीत बल लगाता है.

(defrule opposite12
(declare (salience 5700))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(conjunction-components  ?id1 ?id2 ?id)
(viSeRya-viSeRaNa  ?id3 ?id)
(id-root ?id2 equal)
(id-root ?id1 and)
(id-root ?id3 force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite12   "  ?id "  viparIwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-08-2014)
;Further, the mutual forces between two bodies are always equal and opposite. [NCERT CORPUS] 
;साथ ही, दो पिण्डों के बीच परस्पर बल सदैव समान  एवं विपरीत दिशा में होते हैं.
;To every action, there is always an equal and opposite reaction.[NCERT CORPUS]
;प्रत्येक क्रिया की सदैव समान एवं विपरीत दिशा में प्रतिक्रिया होती है.

(defrule opposite13
(declare (salience 5600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(conjunction-components  ?id1 ?id2 ?id)
(id-root ?id2 equal)
(id-root ?id1 and)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 samAna_eMva_viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " opposite.clp  opposite13  "  ?id "  " ?id1 " " ?id2 " samAna_eMva_viparIwa  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-10-2014)
;This restoring force is equal in magnitude but opposite in direction to the applied force. [NCERT CORPUS]
;yaha prawyAnayana bala mAna meM prawyAropiwa bala ke barAbara waWA xiSA meM usake viparIwa howA hE. [NCERT CORPUS]

(defrule opposite14
(declare (salience 4600))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 direction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite14   "  ?id "  viparIwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-10-2014)
;However, if two equal and opposite deforming forces are applied parallel to the cross-sectional area of the cylinder, as shown in Fig. 9.2 (b),there is relative displacement between the opposite faces of the cylinder. [NCERT CORPUS]
;yaxi xo barAbara Ora viroXI virUpaka bala belana ke anuprasWa paricCexa ke samAMwara lagAe jAez, jEsA ciwra 9.2(@b) meM xiKAyA gayA hE, wo belana ke sammuKa PalakoM ke bIca sApekRa visWApana ho jAwA hE. [NCERT CORPUS]

(defrule opposite15
(declare (salience 5700))
(id-root ?id opposite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(conjunction-components  ?id1 ?id2 ?id)
(viSeRya-viSeRaNa  ?id3 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id3 ?id4)
(id-root ?id2 equal)
(id-root ?id1 and)
(id-root ?id3 force)
(id-root ?id4 deform)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opposite.clp 	opposite15   "  ?id "  viroXI )" crlf))
)

;"opposite","Prep","1.ke_sAmane"
;I sat opposite to him during the meal.
;
;
