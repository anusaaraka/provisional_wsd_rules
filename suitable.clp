;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;We regret that latecomers cannot be admitted until a suitable break in the performance.[cald]
;हम खेद करते हैं कि देर से आने वाले [व्यक्ति] प्रदर्शन में एक मुनासिब विराम तक  दाखिल नहीं किए जा सकते . [self]
(defrule suitable1
(declare (salience 1000))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 break)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-in_saMbanXI  ?id1 ?)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id munAsiba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp 	suitable1   "  ?id " munAsiba )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;Keep the dog confined in a suitable travelling cage. [oald]
;कुत्ते को एक सुविधाजनक सफरी  पिन्जरे में बन्द करके रखिए . [manual]
(defrule suitable2
(declare (salience 1000))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 cage)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suviXAjanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp 	suitable2   "  ?id " suviXAjanaka )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;I don't have anything suitable to wear for the party.[cald]
;मेरे पास पार्टी में पहनने के लिए कुछ भी शोभनीय नहीं है . [self]
(defrule suitable3
(declare (salience 1000))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 wear)
(saMjFA-to_kqxanwa  ?id ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoBanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp 	suitable3   "  ?id " SoBanIya )" crlf))
)


;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;Would now be a suitable moment to discuss my report?[oald]
;क्या  वर्तमान मेरी रिपोर्ट के विषय में चर्चा करके एक उचित समय/क्षण रहेगा?  [self]
(defrule suitable4
(declare (salience 1000))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 moment)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa/mAkUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp 	suitable4   "  ?id " uciwa/mAkUla )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;These conditions are suitable to their development.[self: with respect to oald]
;ये स्थितियाँ उनके विकास के अनुकूल हैं .  [self]
(defrule suitable5
(declare (salience 1000))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 condition|situation)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp 	suitable5   "  ?id " anukUla )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;The film is suitable for children.[cald]
;सिनेमा बच्चों के लिए उपयुक्त है . [self]
(defrule suitable0
(declare (salience 0))
(id-root ?id suitable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayukwa/sahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suitable.clp         suitable0   "  ?id "  upayukwa/sahI )" crlf))
)

