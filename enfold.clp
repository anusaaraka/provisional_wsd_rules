;@@@ Added by 14anu-ban-04 (02-03-2015)
;He enfolded her in his arms.                 [cald]
;उसने अपनी बाँहों में  उसको  भर लिया .                   [self]   
(defrule enfold1
(declare (salience 20))
(id-root ?id  enfold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-in_saMbanXI ?id ?id2)
(id-root ?id2 arms)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   enfold.clp 	enfold1  "  ?id "  Bara_le)" crlf))
)

;@@@ Added by 14anu-ban-04 (03-03-2015)
;The summit was enfolded in a circle of white cloud.                    [oald]
;शिखर सफेद बादल के घेरे में छिप गया था .                                           [self]   
(defrule enfold2
(declare (salience 30))
(id-root ?id  enfold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cipa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   enfold.clp 	enfold2   "  ?id " Cipa)" crlf))
)


;---------------------------------------------------------DEFAULT RULES---------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (02-03-2015)
;Darkness spread and enfolded him.                     [oald]
;अन्धकार फैला और उसको ढक लिया .                             [self]   
(defrule enfold0
(declare (salience 10))
(id-root ?id  enfold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   enfold.clp 	enfold0   "  ?id " Daka_le)" crlf))
)

