;@@@ Added by 14anu-ban-10 on (27-02-2015)
;The enemy forces were repulsed.[hinkhoj]
;दुश्मन की सेना को हटा दिया गया था . [manual]
(defrule repulse2
(declare (salience 5100))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse2   "  ?id "  hatA_xe)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-02-2015)
;She repulsed his advances.[hinkhoj]
;उसने उसकी उधार  ठुकरा दिया . [manual]
(defrule repulse3
(declare (salience 5200))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 advance)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TukarA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse3   "  ?id "   TukarA_xe)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-02-2015)
;I was repulsed by the horrible smell.[oald]
;मुझे खराब बू से  घृणा पैदा हो गयी थी . [manual]
(defrule repulse4
(declare (salience 5300))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GQNA_pExA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse4   "  ?id "   GQNA_pExA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-02-2015)
;Each time I tried to help I was repulsed.[oald]
;हर बार जब भी मैंने सहायता करने का प्रयास किया मुझे  भगा दिया गया था . [manual]
(defrule repulse5
(declare (salience 5400))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BagA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse5   "  ?id "   BagA_xe)" crlf))
)

;------------------------ Default Rules ----------------------
;"repulse","N","1.iMkAra"
;My request for a donation met with a repulse.
(defrule repulse0
(declare (salience 5000))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse0   "  ?id "  iMkAra )" crlf))
)

;"repulse","VT","1.KaxedZa_xenA"
;The enemy forces were repulsed.  
(defrule repulse1
(declare (salience 4900))
(id-root ?id repulse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaxedZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repulse.clp 	repulse1   "  ?id "  KaxedZa_xe )" crlf))
)

;"repulse","VT","1.KaxedZa_xenA"
;The enemy forces were repulsed.  
;--"2.TukarA_xenA"
;Rita repulsed Ravi's advances.
;
