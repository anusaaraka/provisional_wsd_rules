;@@@ Added by 14anu-ban-06 (11-04-2015)
;We just didn't jell as a group. (OALD)
;हमने बिल्कुल समूह के जैसे काम नहीं किया था . (manual)
(defrule jell1
(declare (salience 2000))
(id-root ?id jell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI ?id ?id1)
(id-root ?id1 group)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jell.clp 	jell1   "  ?id "  kAma_kara )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (11-04-2015)
;Ideas were beginning to jell in my mind. (OALD)
;विचार मेरे मन में निश्चित रूप लेना शुरु कर रहे थे . (manual)
(defrule jell0
(declare (salience 0))
(id-root ?id jell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_rUpa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jell.clp 	jell0   "  ?id "  niSciwa_rUpa_le )" crlf))
)
