;@@@ Added by 14anu19
;This invaluable tool of the English language fell into Mohan's hands when he went to school in England . 
;अंग्रेजी  भाषा का अमूल्य  स्त्रोत मोहन के हाथ लगा जब वह इंग्लैड मे विद्यालय गया

(defrule fell4
(declare (salience 1350))
(id-root ?id fall)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) into)
(kriyA-into_saMbanXI  ?id ?id1)
(id-word ?id1 hands)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) laga))
;(assert (id-wsd_root_mng ?id gira))
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fell.clp    fell4   "  ?id "  laga )" crlf);commented by Shirisha Manju 17-12-14
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  fell.clp    fell4   "  ?id "  "(+ ?id 1) " laga )" crlf)); added by Shirisha Manju 17-12-14
)

;$$$ Modified by Shirisha Manju 17-12-14 Suggested by Chaitanya Sir
;@@@ Added by 14anu19 (27-06-2014)
; He was worn out and fell asleep . 
;वह थक गया और सो गया
(defrule fell5
(declare (salience 1350))
(id-root ?id fall)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id ?id1)
(id-word ?id1 asleep)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 so_jA)) ; changed meaning 'so' as 'so_jA' and modified =(+ ?id 1) as ?id1 by Shirisha Manju 17-12-14
;(assert (id-wsd_root_mng ?id so_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  fell.clp    fell5   "  ?id "  "?id1 " so_jA )" crlf)); added by Shirisha Manju 17-12-14
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fell.clp    fell5   "  ?id "  so )" crlf));commented by Shirisha Manju 17-12-14
)

;@@@ Added by 14anu-ban-05 on (04-02-2015)
;I fell apart under the pressure.[wordrefernce forum]
;मैं दबाब में आकर टूट गया	[Manual]
;Their marriage finally fell apart.[OALD]
;उनकी शादी अन्ततः टूट गई 	[MANUAL]
(defrule fell3
(declare (salience 2003))
(id-root ?id fall)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) apart)
;(kriyA-subject  ?id ?id1)
;(id-root ?id1 I|marriage|deal)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root ?id fall))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) tUta_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fell.clp  fell3  "  ?id "  " + ?id 1 "  tUta_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root   " ?*prov_dir* "  fell.clp  fell3   "  ?id " leaf )" crlf))
)


;---------------------- Default rules --------------------------

(defrule fell0
(declare (salience 5000))
(id-root ?id fell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahAdZI_ilAkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fell.clp 	fell0   "  ?id "  pahAdZI_ilAkA )" crlf))
)

;"fell","N","1.ú
;Lakeland fells.
;
(defrule fell1
(declare (salience 4900))
(id-root ?id fell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta_girA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fell.clp 	fell1   "  ?id "  kAta_girA )" crlf))
)

(defrule fell2
(declare (salience 1300))
(id-root ?id fall)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fell.clp    fell2   "  ?id "  gira )" crlf))
)

;"fell","V","1.kAta_girAnA"
