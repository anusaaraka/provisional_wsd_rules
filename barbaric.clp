;@@@Added by 14anu-ban-02(18-03-2015)
;Barbaric tribes invaded the area.[mw]
;असभ्य जनजातियों ने क्षेत्र पर आक्रमण किया . [self]
(defrule barbaric1
(declare (salience 100)) 
(id-root ?id barbaric) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 tribe)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id asaBya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbaric.clp  barbaric1  "  ?id "  asaBya )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-03-2015)
;His table manners are barbaric.[mw]
;उसके मेज पर बैठकर खाने के शिष्टाचार असभ्य हैं .[self] 
(defrule barbaric2
(declare (salience 100)) 
(id-root ?id barbaric) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 manner )
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id asaBya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbaric.clp  barbaric2  "  ?id "  asaBya )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(18-03-2015)
;Sentence: A barbaric act.[oald]
;Translation: एक क्रूर कॄत्य.[self]
(defrule barbaric0 
(declare (salience 0)) 
(id-root ?id barbaric) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id krUra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbaric.clp  barbaric0  "  ?id "  krUra )" crlf)) 
) 
