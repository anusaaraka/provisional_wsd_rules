;@@@ Added by 14anu-ban-04 (18-02-2015)
;An elegant solution to the problem.                     [olad]
;समस्या का   सहज उपाय।                                                                                   [self]
(defrule elegant1   
(declare (salience 20))
(id-root ?id elegant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 solution|plan|idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elegant.clp 	elegant1   "  ?id "  sahaja )" crlf))
)


;@@@ Added by 14anu-ban-04 (18-02-2015)
;This is an absolutely elegant wine.                      [reference.com]
;यह एक पूर्णतः अच्छी वाइन है .                                   [self]
(defrule elegant2   
(declare (salience 20))
(id-root ?id elegant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 wine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elegant.clp 	elegant2   "  ?id "  acCA )" crlf))
)

;@@@ Added by 14anu-ban-04 (18-02-2015)
;An elegant wave of the hand.                        [reference.com]
;हाथ की सुडौल लकीर .                                          [self]
(defrule elegant3   
(declare (salience 20))
(id-root ?id elegant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sudOla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elegant.clp 	elegant3   "  ?id "  sudOla )" crlf))
)

;------------------------ Default Rules ----------------------
;@@@ Added by 14anu-ban-04 (18-02-2015)
;She was tall and elegant.                       [oald]
;वह लम्बी और सुन्दर थी .                                [self]
(defrule elegant0
(declare (salience 10))
(id-root ?id elegant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sunxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elegant.clp  elegant0   "  ?id "  sunxara )" crlf))
)

