;@@@ Added by 14anu-ban-03 (12-02-2015)
;There are chasms in rocks. [hinkhoj]
;चट्टानों में दरारें हैं . [manual]
(defrule chasm0
(declare (salience 00))
(id-root ?id chasm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chasm.clp 	chasm0   "  ?id "  xarAra )" crlf))
)


;@@@ Added by 14anu-ban-03 (12-02-2015)
;There is still a vast economic chasm between developed and developing countries. [cald]
;विकसित और विकासशील देशों के बीच एक विशाल अर्थशास्त्रीय मतभेद अभी भी है . [manual]
(defrule chasm1
(declare (salience 100))
(id-root ?id chasm)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawaBexa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chasm.clp 	chasm1   "  ?id "  mawaBexa )" crlf))
)

