;########################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`
;@@@ Added by 14anu03 on 02-july-14
;Check the shaft for scores.
;डण्डे को खंरोच के लिए जाँचिये .  
(defrule score103
(declare (salience 5500))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id1 ?id)
(id-root ?id1 shaft|box|surface|table|mobile|computer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaroMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score103   "  ?id "  KaroMca )" crlf))
)
;@@@ Added by 14anu03 on 02-july-14
;While Carpenter is known as a great director, he's also very good at creating atmospheric music scores for his films.
;जब कि चरपेन्टर एक बढिया निदेशक के रूप में जाना जाता है,वह उसके सिनेमा के लिए वायुमण्डलीय  स्वर लिपि बनाने पर भी अत्यन्त अच्छा है .
(defrule score102
(declare (salience 5500))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 music)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 svara_lipi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  score.clp     score102   "  ?id "  " ?id1 "  svara_lipi )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (26-12-2014)
;@@@ Added by 14anu03 on 02-july-14
;A score of men lost their lives in the battle.
;आदमियों के एक समूह ने लडाई में अपना जीवन खोया . 
(defrule score101
(declare (salience 5500))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-word ?id1 men|women|people|humans);changed 'id-root' to 'id-word' by 14anu-ban-01 on (26-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samUha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score101   "  ?id "  samUha )" crlf))
)
;@@@ Added by 14anu03 on 02-july-14
; I hope that in this House, at least, we shall not try to score off each other.
;कम से कम मैं इस घर में उम्मीद करता हूँ कि हम एक दूसरे को मूर्ख नहीं बनायेंगे.
(defrule score100
(declare (salience 5500))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mUrKa_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  score.clp     score100   "  ?id "  " ?id1 "  mUrKa_banA )" crlf))
)
;@@@  Added by jagriti(31.12.2013)
;She scored 120 in the history test. 
;उसने इतिहास की परीक्षा में 120 अंक प्राप्त किए.
(defrule score0
(declare (salience 5000))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score0   "  ?id "  aMka_prApwa_kara )" crlf))
)
;@@@ Added by jagriti(31.12.2013)
;He scored with her last night.
;उसने उसके साथ कल रात को संभोग किया था. 
(defrule score1
(declare (salience 4900))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 night)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score1   "  ?id "  saMBoga_kara )" crlf))
)
;@@@ Added by jagriti(14.2.2014) 
;He made a score on the table.[rajpal]
;उसने मेज पर खरोंच लगा दी . 
(defrule score2
(declare (salience 4800))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-on_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaroMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score2   "  ?id "  KaroMca )" crlf))
)
;@@@ Added by jagriti(14.2.2014) 
;To pay one's score.[rajpal]
;हिसाब देना . 
(defrule score3
(declare (salience 4700))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 pay)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hisAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score3   "  ?id "  hisAba )" crlf))
)
;@@@ Added by jagriti(14.2.2014) 
;He made a good score.[rajpal]
;उसने अच्छे अंक प्राप्त किए . 
(defrule score4
(declare (salience 4600))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 make|get|keep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score4   "  ?id "  aMka )" crlf))
)
;@@@ Added by jagriti(14.2.2014) 
;The student scored the desk with his knife.[rajpal]
;विद्यार्थी ने अपने चाकू से डेस्क खरोंचा . 
(defrule score5
(declare (salience 4700))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-on_saMbanXI  ?id ?)(kriyA-with_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KaroMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score5   "  ?id "  KaroMca )" crlf))
)
;@@@ Added by jagriti(14.2.2014)
;He was scored by the public for his evil remarks.[rajpal]
;उसे उसकी दुष्ट टिप्पणियों के लिये जनता द्वारा बुरा-भला कहा गया था .
(defrule score6
(declare (salience 4600))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burA-BalA_kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score6   "  ?id "  burA-BalA_kaha )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (26-12-2014)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 30.06.14
;He scored his first goal of the season on Saturday.
;उसने शनिवार को सीज़न का उसका प्रथम गोल किया . 
(defrule score09
(declare (salience 4700))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 goal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara));changed 'kiyA' to 'kara' by 14anu-ban-01 on (26-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp     score09   "  ?id "  kara )" crlf));changed 'kiyA' to 'kara' by 14anu-ban-01 on (26-12-2014)
)

;@@@ Added by 14anu21 on 02.07.2014
;He scored forty runs on ten balls.[self]
;उसने दस गोले पर चालीस घूमना खरोंचे .(Transaltion before adding rule)
;उसने दस गेंद पर चालीस रन बनाए . 

;He scored a century.[self]
;उसने शताब्दी प्राप्त की .(Added rule century0 ; Translation before adding rule) 
;उसने शतक बनाया . 
(defrule score9
(declare (salience 5000))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object ?id ?id1)(kriyA-subject ?id ?id1))
(id-root ?id1 run|century)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score9   "  ?id "  banA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (04-02-2015)
;He scored with a brilliant 25-yard drive. [drive.clp]
;उसने एक शानदार 25-yard के शाट के साथ  गोल बनाया . [self]
(defrule score10
(declare (salience 5000))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 drive)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gola_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score10   "  ?id "  gola_banA )" crlf))
)

;------------------ Default Rules ---------------
;@@@ Added by jagriti(31.12.2013)
;The movie scored an instant success. 
;फिल्म ने तात्कालिक सफलता प्राप्त की.
(defrule score7
(declare (salience 1))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score7   "  ?id "  prApwa_kara )" crlf))
)
;@@@ Added by jagriti(14.2.2014)
;The score of India was 250 runs.[rajpal]
;इंडिया का स्कोर 250 रन था . 
(defrule score8
(declare (salience 1))
(id-root ?id score)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id skora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  score.clp 	score8   "  ?id "  skora )" crlf))
)

