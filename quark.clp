;@@@ Added by Anita-21-06-2014
; Recent developments have however indicated that protons and neutrons are built out of still more elementary constituents ;called quarks. [ncert]
;तथापि, हाल ही में हुए विकासों ने यह सूचित किया है कि प्रोटॉन तथा न्यूट्रॉन और भी कहीं अधिक मूल अवयवों, जिन्हें 'क्वार्क' कहते हैं, से मिलकर बने हैं.
(defrule quark0
(declare (salience 4000))
(id-root ?id quark)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kvArka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quark.clp 	quark0   "  ?id "  kvArka )" crlf))
)

;###################################default-rule#################################################
;@@@ Added by Anita-21-06-2014
;A quark was eaten throughout northern, central, and eastern Europe. 
;क्वार्क पनीर पूरे उत्तरी ,मध्य एवं पूर्वी यूरोप खाया गया था ।
(defrule quark_default-rule
(declare (salience 0))
(id-root ?id quark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kvArka_panIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quark.clp 	quark_default-rule   "  ?id "  kvArka_panIra )" crlf))
)
