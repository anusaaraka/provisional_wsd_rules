
(defrule beef0
(declare (salience 5000))
(id-root ?id beef)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bala_baDZa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " beef.clp beef0 " ?id "  bala_baDZa )" crlf)) 
)

(defrule beef1
(declare (salience 4900))
(id-root ?id beef)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bala_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " beef.clp	beef1  "  ?id "  " ?id1 "  bala_baDZa  )" crlf))
)

;$$$Modified by 14anu-ban-02(12-01-2015)
;He beefed about me to the teacher. 
;उसने  शिक्षक से  मेरी  शिकायत की.
;@@@ Added bt 14anu23 on 25/6/14
;He beefed about me to the teacher. 
;उसने  शिक्षक से  मेरी  शिकायत की.
(defrule beef4
(declare (salience 4900))
(id-root ?id beef)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SikAyawa_kara))        ;commented by 14anu-ban-02(12-01-2015)
(assert (id-wsd_root_mng ?id SikAyawa_kara))    ;Added by 14anu-ban-02(12-01-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " beef.clp	beef4  "  ?id "  " ?id1 "  SikAyawa_kara  )" crlf))    ;commented by 14anu-ban-02(12-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " beef.clp beef4 " ?id "  SikAyawa_kara )" crlf))  ;added by 14anu-ban-02(12-01-2015)
)

;$$$Modified by 14anu-ban-02(12-01-2015)
;What's his latest beef?
;उसकी  नवीनतम  शिकायत  क्या है?
;@@@ Added bt 14anu23 on 25/6/14
;What's his latest beef?
;उसकी  नवीनतम  शिकायत  क्या है?
(defrule beef3
(declare (salience 4900))
(id-root ?id beef)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-RaRTI_viSeRaNa ?id ?id1)          ;commented by 14anu-ban-02(12-01-2015)
(viSeRya-viSeRaNa  ?id ?id1)                ;added by 14anu-ban-02(12-01-2015)
(id-root ?id1 late)                         ;added by 14anu-ban-02(12-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SikAyawa))        ;commented by 14anu-ban-02(12-01-2015)
(assert (id-wsd_root_mng ?id SikAyawa))                                           ;added by 14anu-ban-02(12-01-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " beef.clp	 beef3  "  ?id "  " ?id1 "  SikAyawa  )" crlf))                 ;commented by 14anu-ban-02(12-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " beef.clp beef3 " ?id "  SikAyawa )" crlf))  ;added by 14anu-ban-02(12-01-2015)
)


(defrule beef2
(declare (salience 4800))
(id-root ?id beef)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id go_mAMsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beef.clp 	beef2   "  ?id "  go_mAMsa )" crlf))
)

;"beef","N","1.go_mAMsa"
;Some communities don't eat beef.
;
;
