;@@@ Added by 14anu-ban-03 (29-01-2015)
;Here there is the tourist rest house of Haryana Tourism , where is available A and non A rooms apart from camper hut for staying. [tourism]
;यहाँ हरियाणा पर्यटन का टूरिस्ट रेस्ट हाउस है , जहाँ ठहरने के लिए एसी और नॉन एसी रूम के अलावा कैम्पर हट भी उपलब्ध हैं .[tourism]
(defrule camper0
(declare (salience 00))
(id-root ?id camper)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kEmpara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  camper.clp 	camper0   "  ?id "  kEmpara )" crlf))
)



;@@@ Added by 14anu-ban-03 (29-01-2015)
;Shogi is located just 18 kms away from Shimla and is no less than Mecca for campers .[tourism]
;शोगी शिमला से महज 18 कि.मी. दूर स्थित है और कैंप करने वालों के लिए किसी मक्का से कम नहीं .[tourism]
(defrule camper1
(declare (salience 100))
(id-root ?id camper)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kEMpa_karane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  camper.clp 	camper1   "  ?id "  kEMpa_karane_vAlA )" crlf))
)
