
; Modified by 14anu-ban-01 on 10-08-2014.
; meaning changed from 'pareSAna_yA_gussA_xilAne_vAlA' to 'pareSAnI_kA_kAraNa'
;@@@ Added by 14anu-ban-09 on 7-8-14
;In truth, Robert Dudley had been my bane.
;roYbarta dudale saca meM meri pareSAnI_kA_kAraNa rahA TA.
(defrule bane0
(declare (salience 0))
(id-root ?id bane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAnI_kA_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bane.clp 	bane0   "  ?id "  pareSAnI_kA_kAraNa )" crlf))
)

;Modified by 14anu-ban-02(26.08.14)
;Meaning changed from aBiRApa to aBiSApa
;@@@ Added by 14anu-ban-09 on 7-8-14
;Congenital heart disease is one such grotesque state which is a bane. [Parallel Corpus]
;’ जन्मजात हृदय रोग ’ कन्जेनीटल हार्ट एक ऐसी विषम स्थिति है , जो अभिशाप भी है ।

(defrule bane1
(declare (salience 10))
(id-root ?id bane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-jo_samAnAXikaraNa ?id2 ?id3)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(subject-subject_samAnAXikaraNa ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBiRApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bane.clp 	bane1   "  ?id "  aBiRApa )" crlf))
)

;@@@ Added by 14anu-ban-01 on 10-08-2014.
;24542:This has been the bane of Indian cricket .
;यही भारतीय टीम की सबसे ब़ड़ी समस्या रही है .
;89214:Learning by rote without understanding had been for centuries the bane of Indian , in fact all Eastern education .
;शताब़्दियों से बिना समझे , टिप़्पणियों के द्वारा पढ़ना न केवल भारतीयों के लिये बल़्कि यथार्थ में समस़्त पूर्व शिक्षा के लिए समस्या रही.
;92042:Traffic jams are generally the bane of any commuter .
;ट्रैफिक जाम आम तौर पर हर यात्री के लिए समस्या होते हैं ।
(defrule bane2
(declare (salience 10))
(id-root ?id bane)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samasyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bane.clp 	bane2   "  ?id "  samasyA )" crlf))
)

;@@@ Added by 14anu-ban-02 (26.08.14)
;Then as if during the second world war from 1939 to 1945 the black shadow of bane hung over its existence .
;फिर 1939 से 1945 के दूसरे विश्व युद्ध के दौरान तो जैसे अभिशाप की काली छाया इस देश के अस्तित्त्व पर छाई ही रही ।
(defrule bane3
(declare (salience 10))
(id-root ?id bane)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBiSApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bane.clp 	bane3   "  ?id "  aBiSApa )" crlf))
)
