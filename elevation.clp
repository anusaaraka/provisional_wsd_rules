;@@@ Added by 14anu-ban-04 (19-02-2015)
;His elevation to the presidency of the new republic was generally popular.             [cald]
;नये गणतन्त्र के अध्यक्षता के लिए उसकी उन्नति आमतौर पर लोकप्रिय थी .                                      [self]
(defrule elevation1
(declare (salience 20))
(id-root ?id elevation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id unnawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " elevation.clp 	elevation1  "  ?id "  unnawi )" crlf))
)


;@@@ Added by 14anu-ban-04 (19-02-2015)
;The city is at an elevation of 2000 metres.                 [oald]
;शहर 2000 मीटर की ऊँचाई पर है .                                     [self]
(defrule elevation0
(declare (salience 10))
(id-root ?id elevation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " elevation.clp 	elevation0  "  ?id "  UzcAI )" crlf)
)
)


