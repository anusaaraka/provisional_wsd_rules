;@@@ Added by 14anu-ban-04 on (25-04-2015)
;He said that someone had poisoned his coffee.                 [cald]        
;उसने कहा कि किसीने उसकी कॉफी में जहर मिलाया था .                         [self]
(defrule poison4
(declare (salience 30))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id meM)) 
(assert (id-wsd_root_mng ?id jahara_milA/viRa_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   poison.clp      poison4  "  ?id " meM  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison4   "  ?id "  jahara_milA/viRa_milA )" crlf))
)

;@@@ Added by 14anu-ban-04 on (25-04-2015)
;The chemical companies are poisoning our rivers and atmosphere.   [same clp file]
;रसायनिक कम्पनियाँ हमारी नदियों और वातावरण को दूषित कर रहीं हैं .                      [self]
(defrule poison5
(declare (salience 20))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id xURiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   poison.clp      poison5  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison5  "  ?id "  xURiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 on (25-04-2015)
;I won't eat that poison.                       [same clp file]
;मैं वह दूषित आहार नहीं खाऊँगा .                            [self]
(defrule poison6
(declare (salience 30))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 eat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xURiwa_AhAra)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison6   "  ?id "  xURiwa_AhAra )" crlf))
)
 
;@@@ Added by 14anu-ban-04 on (25-04-2015)
;How did the murderer poison the victim?              [merriam-webster]
;हत्यारे ने शिकार को कैसे मारा?                                     [self]
(defrule poison7
(declare (salience 50))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 murderer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison7 "  ?id "  mAra )" crlf))
)

;@@@ Added by 14anu-ban-04 on (25-04-2015)
;The long dispute has poisoned relations between the two countries.       [cald]     ;run on parse no. 6
;लम्बे विवाद ने दो देशों के बीच के सम्बन्धों को बिगाड़ा है .                                     [self]           
(defrule poison8
(declare (salience 30))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 relation|friendship)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id bigAda))                
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   poison.clp      poison8  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison8 "  ?id "  bigAda )" crlf))
)

;$$$ Modified by 14anu-ban-04 on (25-04-2015)
;His so called friend poisoned him.                        [same clp file]
;उसके तथाकथित मित्र ने उसको विष दिया.                                [self]
(defrule poison2
(declare (salience 40))                      ;salience reduced from '4800' to '40' by 14anu-ban-04 on (25-04-2015)
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)                       ;added  by 14anu-ban-04 on (25-04-2015)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))            ;added  by 14anu-ban-04 on (25-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison2   "  ?id "  viRa_xe )" crlf))
)


(defrule poison0
(declare (salience 5000))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id poisoning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viRAkwIkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  poison.clp  	poison0   "  ?id "  viRAkwIkaraNa )" crlf))
)

;"poisoning","N","1.viRAkwIkaraNa"
;He died due to food poisoning.
;


;-------------------------------------------- Default Rules ------------------------------------

(defrule poison1
(declare (salience 10))                         ;salience reduced from '4900' to '10' by 14anu-ban-04 on (25-04-2015)
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison1   "  ?id "  viRa )" crlf))
)

;"poison","N","1.viRa"
;Potassium cyanide is a very strong poison.
;--"2.xURiwa_AhAra"
;I won't eat that poison.
;

;"poison","V","1.viRa_xenA"
;His so called friend poisoned him.
;--"2.xURiwa_kara_xenA"
;The chemical companies are poisoning our rivers && atmosphere. 
;--"3.bigAdanA"
;Don't poison his mind.
;

;@@@ Added by 14anu-ban-04 on (25-04-2015)
;Large sections of the river have been poisoned by toxic waste from factories.                 [cald]        
;नदी के अधिक भाग फैक्टरियों के विषैले अवशेष से दूषित हो गये हैं .                                                    [self]
(defrule poison3
(declare (salience 10))
(id-root ?id poison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xURiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poison.clp 	poison3   "  ?id " xURiwa_ho )" crlf))
)
