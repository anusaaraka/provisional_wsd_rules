
(defrule introduce0
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce0   "  ?id "  paricaya_karA )" crlf))
)

(defrule introduce1
(declare (salience 4900))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce1   "  ?id "  paricaya_kara )" crlf))
)

;"introduce","VT","1.paricaya_karAnA[karanA]"
;Graham was introduced to me  in the function.
;--"2.praswuwa_karanA"
;A new word processor was introduced.
;--"3.upalabXa_karAnA"
;The domestic appliances were introduced in the market .
;
;$$$ Modified by 14anu-ban-06 (12-11-2014)
;### [COUNTER EXAMPLE] ### The soybean was introduced into North America in 1765 but for the next 155 years the crop was grown primarily for forage. (agriculture)
;### [COUNTER EXAMPLE] ### सोयाबीन को   उत्तरी अमेरिका  में 1765 में  लाया गया था, लेकिन अगले 155 सालो तक फसल को मुख्यतः चारे के लिए उगाया गया था. (manual)
;ADDED BY PRACHI RATHORE
;The tube which carries the laser is introduced into the abdomen through a small cut in the skin.[PHYSICS]
;नली लेज़र को चमड़ी में एक छोटे से चीरे के बाद पेट में प्रवेश कराती है .

(defrule introduce2
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 abdomen);added by 14anu-ban-06 (12-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praveSa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce2   "  ?id "  praveSa_karA )" crlf))
)

;ADDED BY PRACHI RATHORE
;The smaller 10 pence coin was introduced in 1992.[CAMBRIDGE]
;दस पेन्स के सिक्के १९९२ में शुरू किए गए थे .

(defrule introduce3
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SurU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce3   "  ?id "  SurU_kara )" crlf))
)


;ADDED BY PRACHI RATHORE
;A haunting oboe solo introduces the third movement of the concerto.[CAMBRIDGE]
;एक अकेले प्रदर्शन ने  बार बार याद आने वाले  पल की बंदिश की शुरुआत की  .
(defrule introduce4
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 movement|act|schedule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuruAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce4   "  ?id "  SuruAwa_kara )" crlf))
)


;ADDED BY PRACHI RATHORE
;They have been slow to introduce changes in procedure.[M-W LEARNERS DICT.]
;वे कार्यविधि में परिवर्तन लाने में धीमे हो चुके हैं . 
(defrule introduce5
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce5   "  ?id "  lA )" crlf))
)


;ADDED BY PRACHI RATHORE
;Such unpopular legislation is unlikely to be introduced before the next election.[CAMBRIDGE]
;इस तरह के अलोकप्रिय लेजिस्लेशन का अगले चुनाव तक पेश करना संभव नहीं है.

(defrule introduce6
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-during_saMbanXI ?id ?)(kriyA-before_saMbanXI ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce6   "  ?id "  peSa_kara )" crlf))
)


;ADDED BY PRACHI RATHORE
;Can I introduce myself? I'm Helen Robins.[OALD]
;क्या मैं खुद का परिचय दे सकता हूँ? मैं हेलेन रोबिन्स् हूँ . 
(defrule introduce7
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
;(id-root id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id1 pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_xe))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce7   "  ?id "  paricaya_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  introduce.clp  introduce7    "  ?id "  kA )" crlf))
)


;ADDED BY PRACHI RATHORE
;The next programme will be introduced by Mary David.[OALD]
;अगला कार्यक्रम मेरी डेवीड के द्वारा पेश किया जाएगा . 
(defrule introduce8
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ?id ?id1)(kriyA-subject ?id ?id1))
(id-cat_coarse ?id verb)
(id-root ?id1 line|range|bill|programme|appliance|processor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce8   "  ?id "  peSa_kara )" crlf))
)


;ADDED BY PRACHI RATHORE
;Further, the value of e / m was found to be independent of the nature of the material/metal used as the cathode (emitter), or the gas introduced in the discharge tube. [PHYSICS]
;यह भी पाया गया कि e / m का मान कैथोड ( उत्सर्जक ) के पदार्थ अथवा धातु या विसर्जन - नलिका में भरी गैस की प्रकृति पर निर्भर नहीं करता .
;The new carpet introduces some color into the living room. 
;The rivalry introduced more drama to/into the competition.
(defrule introduce9
(declare (salience 5300))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-into_saMbanXI ?id ?id1)(kriyA-in_saMbanXI ?id ?id1))
(id-cat_coarse ?id verb)
(id-root ?id1 room|competition|tube)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce9   "  ?id "  Bara )" crlf))
)


;ADDED BY PRACHI RATHORE
;Bands from London introduced the craze for this kind of music.[OALD]
;लन्दन के बैंड  इस प्रकार के सङ्गीत के लिए जुनून लाए . 
(defrule introduce10
(declare (salience 5300))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 craze)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce10   "  ?id "  lA )" crlf))
)


;ADDED BY PRACHI RATHORE
;An Asian plant that has been introduced to America. [M-W LEARNERS DICT.]
;एशियाई वनस्पति जो अमरीका को भेजी गयी है . 
(defrule introduce11
(declare (salience 5300))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce11   "  ?id "  Beja )" crlf))
)


;ADDED BY PRACHI RATHORE
;She introduced her mother to her friends.[M-W LEARNERS DICT.]
;उसने उसके मित्रों को उसकी माँ से मिलवाया . 
(defrule introduce12
(declare (salience 5300))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id2)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milavA))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce12   "  ?id "  milavA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  introduce.clp  introduce12    "  ?id "  se )" crlf))
)

;$$$ Modified by 14anu-ban-06 (03-03-2015)
;### [COUNTER EXAMPLE] ### In Chapters 6 and 8 (Class XI), the notion of potential energy was introduced.(NCERT)
;### [COUNTER EXAMPLE] ### अध्याय 6 तथा 8 (कक्षा 11) में स्थितिज ऊर्जा की धारणा से आपको परिचित कराया गया था.(NCERT)
;### [COUNTER EXAMPLE] ### अध्याय 6 तथा 8 (कक्षा 11) में स्थितिज ऊर्जा की धारणा का परिचित कराया गया था.(NCERT improvised)
;@@@ Added by Prachi Rathore 22-1-14
;In the Faculty of Law the system of teaching by cases and clauses for the discussion of legal problems was introduced.[gyannidhi]
;कानून संकाय में कानूनी समस्याओं से संबंधित विवेचनों में मुकदमों और दफाओं के अनुसार शिक्षा देने की पद्धति शुरू की गई।
(defrule introduce13
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
(kriyA-karma  ?id ?id1);added 'id1' by 14anu-ban-06 (03-03-3015)
(id-root ?id1 system);added by 14anu-ban-06 (03-03-3015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Suru_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce13   "  ?id "  Suru_kara )" crlf))
)

;@@@ Added by Prachi Rathore 22-1-14
;Graham was introduced to me in the function.[old sentence]
;ग्रहाम का कार्यक्रम में मुझसे परिचय करवाया गया था . 
(defrule introduce14
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
(kriyA-karma  ?id ?id1)
(or(id-root id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_kara))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce14   "  ?id "  paricaya_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  introduce.clp  introduce14    "  ?id "  kA )" crlf))
)

;@@@ Added by Prachi Rathore[5-3-14]
;For example if you by habit always hold your head a bit too far to the right while reading the position of a needle on the scale you will introduce an error due to parallax.[ncert]
;उदाहरण के लिए, प्रकाशीय मञ्च पर सुई की स्थिति का पैमाने पर पाठ्याङ्क लेते समय यदि आप स्वभाव के कारण अपना सिर सदैव सही स्थिति से थोडा दाईं ओर रखेङ्गे, तो पाठन में लम्बन के कारण त्रुटि आ जाएगी.
;उदाहरण के लिए, प्रकाशीय मञ्च पर सुई की स्थिति का पैमाने पर पाठ्याङ्क लेते समय यदि आप स्वभाव के कारण अपना सिर सदैव सही स्थिति से थोडा दाईं ओर रखेङ्गे ,तो आप पाठन में लम्बन के कारण त्रुटि आ करेंगे.
;A good strategy is to focus first on the essential features discover the basic principles and then introduce corrections to build a more refined theory of the phenomenon.[ncert]
;.एक अच्छी युक्ति वही है कि पहले किसी परिघटना के परमावश्यक लक्षणों पर ध्यान केन्द्रित करके उसके मूल सिद्धान्तों को खोजा जाए और फिर संशुद्धियों को सन्निविष्ट करके उस परिघटना के सिद्धान्तों को और अधिक परिशुद्ध बनाया जाए..
(defrule introduce15
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 error|correction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce15   "  ?id "  kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (08-09-2014)
;MTNL and tata have introduced broadband service over here .(parallel corpus)
;एम टी एन एल एवं टाटा यहां ब्रॉडबैंड सेवा भी उपलब्ध कराते हैं ।
(defrule introduce16
(declare (salience 5200))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 service)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalabXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce16   "  ?id "  upalabXa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (09-09-2014)
;On the basis of the Report , a bill was prepared which was introduced in the British Parliament on December 19 , 1934 .(parallel corpus)
;इस रिपोर्ट के आधार पर एक विधेयक तैयार किया गया और वह 19 दिसंबर , 1934 को ब्रिटिश संसद में पेश किया गया .
(defrule introduce17
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 parliament|Parliament)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce17   "  ?id "  peSa_kara )" crlf))
)
;@@@ Added by 14anu-ban-06 (13-10-2014)
;Finally, to understand the relative nature of motion, we introduce the concept of relative velocity.(NCERT)
;anwawaH gawi kI ApekRika prakqwi ko samaJane ke lie hama ApekRika gawi kI XAraNA praswuwa kareMge .(NCERT)
(defrule introduce18
(declare (salience 5250))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 concept|phenomenon|technique)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce18   "  ?id "  praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-10-2014)
;As observations improve in detail and precision or experiments yield new results, theories must account for them, if necessary, by introducing modifications.(NCERT)
;जैसे-जैसे प्रेक्षणों के विस्तृत विवरण तथा परिशुद्धता में संशोधन होते जाते हैं, अथवा प्रयोगों द्वारा नए परिणाम प्राप्त होते जाते हैं, वैसे यदि आवश्यक हो तो उन संशोधनों को सन्निविष्ट करके सिद्धान्तों में उनका स्पष्टीकरण किया जाना चाहिए.(NCERT)
;A good strategy is to focus first on the essential features, discover the basic principles and then introduce corrections to build a more refined theory of the phenomenon.(NCERT)
;एक अच्छी युक्ति वही है कि पहले किसी परिघटना के परमावश्यक लक्षणों पर ध्यान केंद्रित करके उसके मूल सिद्धान्तों को खोजा जाए और फिर संशुद्धियों को सन्निविष्ट करके उस परिघटना के सिद्धान्तों को और अधिक परिशुद्ध बनाया जाए.(NCERT)
(defrule introduce19
(declare (salience 5250))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 modification|correction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanniviRta_kara))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce19   "  ?id "  sanniviRta_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  introduce.clp      introduce19   "  ?id1 " ko )" crlf)
)
)
;@@@ Added by 14anu-ban-06 (15-10-2014)
;On the other hand, the concept of antiparticle was first introduced theoretically by Paul Dirac (1902 — 1984) in 1930 and confirmed two years later by the experimental discovery of positron (antielectron) by Carl Anderson.(NCERT)
;दूसरी ओर पॉल डिरेक (1902-1984) द्वारा वर्ष 1930 में सर्वप्रथम सैद्धान्तिक रूप से प्रतिकण की सङ्कल्पना प्रतिपादित की गई जिसे दो वर्ष पश्चात् कार्ल एन्डरसन ने पॉजीट्रॉन (प्रति इलेक्ट्रॉन) की प्रायोगिक खोज द्वारा प्रमाणित किया.(NCERT)
(defrule introduce20
(declare (salience 5250))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 concept|phenomenon|technique)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawipAxiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce20   "  ?id "  prawipAxiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-11-2014)
;The soybean was introduced into North America in 1765 but for the next 155 years the crop was grown primarily for forage.(agriculture)
;सोयाबीन को   उत्तरी अमेरिका  में 1765 में  लाया गया था, लेकिन अगले 155 सालो तक फसल को मुख्यतः चारे के लिए उगाया गया था.(manual)
;Soybeans were first introduced to North America in 1765, by Samuel Bowen, a former East India Company sailor who had visited China in conjunction with James Flint.(agriculture)
;सोयाबीन को   उत्तरी अमेरिका  में 1765 में शमूएल बोवेन के द्वारा पहली  बार  लाया गया था, जो एक पूर्व ईस्ट इंडिया कंपनी नाविक थे जिन्होंने जेम्स फ्लिन्ट के संयोजन में/के साथ चीन का दौरा किया था .(manual)
(defrule introduce21
(declare (salience 5100))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-into_saMbanXI ?id ?id1)(kriyA-to_saMbanXI ?id ?id1))
(id-root ?id1 America|India|China)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce21   "  ?id "  lA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (11-12-2014)
;@@@ Added By 14anu17
;Mumps was the leading cause of viral meningitis in children before the MMR vaccine was introduced .
;कनपडा बच्चों में विषाणुक गर्दन तोड बुखार का मार्ग दिखाता है  पहले MMR टीका लाया गया था . 
;MMR टीका लाने से पहले  कनपडा बच्चों में विषाणुक गर्दन तोड बुखार का मुख्य कारण था.(manual) by 14anu-ban-06 (11-12-2014) 
(defrule introduce22
(declare (salience 4901))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viBakwi  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA));meaning changed from 'lAnA' to 'lA' by 14anu-ban-06 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce22   "  ?id "  lA )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-03-2015)
;In Chapters 6 and 8 (Class XI), the notion of potential energy was introduced.(NCERT)
;अध्याय 6 तथा 8 (कक्षा 11) में स्थितिज ऊर्जा की धारणा से आपको परिचित कराया गया था.(NCERT)
;अध्याय 6 तथा 8 (कक्षा 11) में स्थितिज ऊर्जा की धारणा का परिचित कराया गया था.(NCERT improvised)
(defrule introduce23
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma ?id ?id1)
(id-root ?id1 notion)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_karA))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce23   "  ?id "  paricaya_karA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  introduce.clp  introduce23    "  ?id "  kA )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-03-2015)
;Indian government introduced a new one rupee note.  (Anusaraaka-Agama team observation)
;भारतीय सरकार ने एक रुपये का नोट जारी किया.  (manual)
(defrule introduce24
(declare (salience 5000))
(id-root ?id introduce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 note)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  introduce.clp 	introduce24   "  ?id "  jArI_kara )" crlf))
)

