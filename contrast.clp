;@@@ Added by 14anu07
(defrule contrast2
(declare (salience 5000))
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) in)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast2   "  ?id "  viparIwa)" crlf))
)

;@@@ Added by 14anu07
(defrule contrast3
(declare (salience 5000))
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) with)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast3   "  ?id "  viparIwa)" crlf))
)

;@@@ Added by 14anu-ban-03 (28-04-2015)
;There is an obvious contrast between the cultures of East and West. [oald]
;ईस्ट और वेस्ट की संस्कृतियों के बीच स्पष्ट भेद है .  [manual]
(defrule contrast4
(declare (salience 5000))
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bexa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast4   "  ?id "  Bexa )" crlf))
)


;@@@ Added by 14anu-ban-03 (28-04-2015)
;Her actions contrasted sharply with her promises. [oald]
;उसकी हरकतों से उसके वादों में तेजी से अन्तर दिखा . [manual]
(defrule contrast5
(declare (salience 5000))   
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwara_xiKa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast5  "  ?id "  anwara_xiKa )" crlf))
)


;-------------------------------------------------------Default rules-----------------------------------------

(defrule contrast0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (28-04-2015)
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast0   "  ?id "  viroXa )" crlf))
)

;"contrast","N","1.viroXa"
;There is a remarkable contrast between the twins.
;The contrast of light && dark shade counts a lot in painting.
;

;$$$ Modified by 14anu-ban-03 (28-04-2015)
;It is interesting to contrast the British legal system with the American one. [oald]
;अमरीका वासियों के साथ बर्तानिया कानूनी प्रणाली की तुलना करना रोचक है . [manual]
(defrule contrast1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (28-04-2015)
(id-root ?id contrast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanA_kara))  ;meaning changed from 'vyawireka_kara' to 'wulanA_kara' by 14anu-ban-03 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrast.clp 	contrast1   "  ?id "  wulanA_kara )" crlf))
)

;"contrast","VT","1.vyawireka_karanA"
;His deeds contrasted sharply with his promises.
;It is intresting to contrast the two singers.
;
