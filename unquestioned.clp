;@@@ Added by 14anu-ban-07 (13-10-2014)
;His courage remains unquestioned.(oald)
;उसका साहस असंदिग्ध बनी हुई है.(manual)
(defrule unquestioned0
(id-root ?id unquestioned)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asanxigXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unquestioned.clp 	unquestioned0   "  ?id "  asanxigXa )" crlf))
)

;@@@ Added by 14anu-ban-07 (13-10-2014)
;There is no 'final' theory in science and no unquestioned authority among scientists.	(ncert corpus)
;विज्ञान में कोई भी सिद्धान्त अन्तिम नहीं है तथा वैज्ञानिकों में कोई निर्विवाद विशेषज्ञ अथवा सत्ता नहीं है.(ncert corpus)
(defrule unquestioned1
(declare (salience 1000))
(id-root ?id unquestioned)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 authority)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirvivAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unquestioned.clp 	unquestioned1   "  ?id "  nirvivAxa )" crlf))
)
