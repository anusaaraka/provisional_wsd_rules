
;@@@ Added by 14anu-ban-06 (07-02-2015)
;She felt it incumbent on her to raise the subject at their meeting. (cambridge)
;उसने उनकी बैठक में समस्या उठाना अपने लिए जरूरी समझा .  (manual)
(defrule incumbent1
(declare (salience 2000))
(id-root ?id incumbent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incumbent.clp 	incumbent1   "  ?id "  jarUrI )" crlf))
)

;--------------------- Default Rule ----------------------


;@@@ Added by 14anu-ban-06 (07-02-2015)
;The incumbent president faces problems which began many years before he took office. (cambridge)
;पदस्थ अध्यक्ष समस्याओं का सामना करता है जो उनके  दफ्तर जाने से बहुत वर्षों पहले शुरु हुई  . (manual)
(defrule incumbent0
(declare (salience 0))
(id-root ?id incumbent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incumbent.clp 	incumbent0   "  ?id "  paxasWa )" crlf))
)


;@@@ Added by 14anu-ban-06 (07-02-2015)
;The first incumbent.(cambridge)
;प्रथम पदाधिकारी.(manual)
;The present incumbent is due to retire next month. (cambridge)
;वर्तमान पदाधिकारी अगले महीने रिटायर होने वाला है .  (manual)
(defrule incumbent2
(declare (salience 0))
(id-root ?id incumbent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxAXikArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incumbent.clp 	incumbent2   "  ?id "  paxAXikArI )" crlf))
)

