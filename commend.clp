
;@@@ Added by 14anu-ban-03 (17-03-2015)
;I commend my children in your care. [hinkhoj]
;मैं अपने बच्चों की देखभाल तुम्हे सौंपता हूँ . [manual]
(defrule commend1
(declare (salience 100))
(id-root ?id commend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOMpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commend.clp 	commend1   "  ?id "  sOMpa )" crlf))
)


;@@@ Added by 14anu-ban-03 (17-03-2015)
;She is an excellent worker and I commend her to you without reservation. [oald]
;वह एक उत्तम कार्यकर्ता है और मैं बिना किसी संदेह के आपको उसकी सिफारिश करता हूँ . [manual]
(defrule commend2
(declare (salience 200))
(id-root ?id commend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id siPAriSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commend.clp 	commend2  "  ?id "  siPAriSa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (17-03-2015)
;His paintings commend him to the artistic world. [hinkhoj]
;उसकी कलाकृतियाँ कलात्मक विश्व में उसकी प्रशंसा करती हैं . [manual]
(defrule commend0
(declare (salience 00))
(id-root ?id commend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praSaMsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commend.clp  commend0   "  ?id "  praSaMsA_kara )" crlf))
)

