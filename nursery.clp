;@@@ Added by Nandini(20-1-2014)
;She Started a nursery for infants under two.
;usane xo ke nIce baccoM ke liye xAI KAnA Suru kiyA.
(defrule nursery0
(declare (salience 500))
(id-root ?id nursery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAla_kakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nursery.clp    nursery0   "  ?id "  bAla_kakRa )" crlf))
)

;@@@ Added by Nandini(20-1-2014)
;This is beautiful nursery.
;yaha sunxara pOXA Gara hE.
(defrule nursery1
(declare (salience 100))
(id-root ?id nursery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pOXA_Gara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nursery.clp    nursery1   "  ?id "  pOXA_Gara )" crlf))
)

;@@@ Added by 14anu17
;You should also be able to ask for your baby to be put in the nursery if you want this .
(defrule nursery2
(declare (salience 5000))
(id-root ?id nursery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacce_kA_kamarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nursery.clp    nursery2   "  ?id "  bacce_kA_kamarA )" crlf))
)

