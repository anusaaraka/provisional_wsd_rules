
;"ranking","N","1.paxa_ke_anusAra_SreNI_krama_meM_raKanA"
;Steffi Graf retained her no.1 world ranking.
(defrule rank0
(declare (salience 5000))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id ranking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id paxa_ke_anusAra_SreNI_krama_meM_raKanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rank.clp  	rank0   "  ?id "  paxa_ke_anusAra_SreNI_krama_meM_raKanA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;Ranks of marching infantry looks so organized.[hinkhoj]
; पैदल सेना के रैंक का आवागमन इतना व्यवस्थित दिखाई देता है. [manual]
(defrule rank4
(declare (salience 5100))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rEMka ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank4   "  ?id "  rEMka )" crlf))
)

;@@@ Added by 14anu-ban-10 on  (16-03-2015)
;He is the writer of the highest rank.[hinkhoj]
;वह सबसे उच्चतम श्रेणी का लेखक है . [manual]
(defrule rank5
(declare (salience 5200))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 high)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SreNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank5   "  ?id "  SreNI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;The entrance was guarded by ranks of policemen.[hinkhoj]
;वह प्रवेश द्वार स्थिति सिपाहियों के द्वारा  संरक्षित किया गया था.[manual]
(defrule rank6
(declare (salience 5300))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 policeman)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank6   "  ?id "   sWiwi)" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;This rank grass is likely to produce many weeds.[hinkhoj]
;यह  अधिक मात्रा मे उपज घास  की  बहुत घास फूस  उत्पादन करने की संभावना है । [manual]
(defrule rank7
(declare (salience 5400))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 grass)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika_mAwrA_me_upaja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank7   "  ?id "  aXika_mAwrA_me_upaja  )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;The winner was a rank outsider.[hinkhoj]
;जीतने वाला एक सम्पूर्ण  बाहरी व्यक्ति था . [manual]
(defrule rank8
(declare (salience 5500))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id )
(id-root ?id1 outsider)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank8   "  ?id "  sampUrNa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;You should try to rise from this ranks.[hinkhoj]
;आपको  फौज़ मे मामूली सैनिक से ऊपर उठने की कोशिश करनी चाहिये  .[manual]
(defrule rank9
(declare (salience 5600))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ? ?id )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id POjZa_me_mAmUlI_sEnika ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank9   "  ?id "  POjZa_me_mAmUlI_sEnika )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-03-2015)
;He was ranked among the best dressed men.[hinkhoj]
;वह सर्वोत्तम अच्छे कपडे पहने के तैयार व्यक्ति की विशेष श्रेणी मे रखा गया था .[manual]
(defrule rank10
(declare (salience 5700))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(kriyA-among_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa_SreNI_me_raKanA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank10   "  ?id "  viSeRa_SreNI_me_raKanA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-03-2015)
;You should not eat rank meat.[hinkhoj]
;आपको वह बदबूदार माँस नहीं खाना चाहिए . [manual]
(defrule rank11
(declare (salience 5800))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 meat)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxabUxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank11   "  ?id "  baxabUxAra)" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-03-2015)
;She ranks a good position in her field.[hinkhoj]
;वह उसके क्षेत्र में एक अच्छे  पद मे ऊँची स्थिति मे आती है . [manual]
(defrule rank12
(declare (salience 5900))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 position)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa_meM_UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank12   "  ?id "  paxa_meM_UzcA)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule rank1
(declare (salience 4900))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxavI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank1   "  ?id "  paxavI )" crlf))
)

;"rank","VTI","1.kisI_viReSa_SreNI_me_AnA"
;He ranks well ahead of his classmates in mathematics. 
(defrule rank2
(declare (salience 4800))
(id-root ?id rank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_viReSa_SreNI_me_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rank.clp 	rank2   "  ?id "  kisI_viReSa_SreNI_me_A )" crlf))
)

;"rank","VTI","1.kisI_viReSa_SreNI_me_AnA"
;He ranks well ahead of his classmates in mathematics. 
;--"2.paxa_me_UzcA_honA"   
;She ranks a good position in her field.
;--"3.viSeRa_SreNI_me_raKanA"
;He was ranked among the best dressed men.
;--"4.paxa_me_UzcA_honA"  
;The colonel ranks all other officers in the squadron. 
;

