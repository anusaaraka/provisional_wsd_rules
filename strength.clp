;----------------------------------Default Rule------------------------------------------------------------
;NOTE::Date format corrected in complete file.
;@@@ Added by 14anu-ban-01 on (23-08-2014).
;4558:The stories are little sad episodes from everyday life , mostly dealing with the circumscribed life of woman in a Hindu household , her frustrations and her nobility and strength of suffering .[Karan Singla]
;संकलित कहानियां दैनंदिन जीवन से जुड़ी हैं और तनिक अवसादपूर्ण हैं जो हिंदू परिवारों में स्त्रियों के जीवन के इर्दगिर्द घूमती हैं और उनकी हताशा , उनकी शालीनता और पीड़ा को झेलने की उनकी शक्ति से जुड़ी हैं .[Karan Singla]
;If we reflect a little, the enormous strength of the electromagnetic force compared to gravity is evident in our daily life. [NCERT corpus] 
;यदि हम थोडा चिन्तन करें, तो हम अपने दैनिक जीवन की घटनाओं में स्वयं ही स्पष्ट रूप में यह पायेँगे कि गुरुत्व बल की तुलना में विद्युत चुम्बकीय बल  की  शक्ति अधिक है.[improvised: changed 'अत्यधिक शक्तिशाली' to 'की  शक्ति अधिक'] 

(defrule strength0
(declare (salience 0))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Sakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength0  "  ?id " Sakwi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-08-2014).
;Rule below:strength1 is similar to rule above:strength0 having same conditions except that strength1 is for physics domain. 
;The point B in the curve is known as yield point (also known as elastic limit) and the corresponding stress is known as yield strength (Sy) of the material.[NCERT corpus]  
;वक्र में बिंदु B पराभव बिंदु (अथवा प्रत्यास्थ सीमा) कहलाता है और संगत प्रतिबल को द्रव्य का पराभव सामर्थ्य (Sy) कहते हैं.[NCERT corpus] 

;If the load is increased further, the stress developed exceeds the yield strength and strain increases rapidly even for a small change in the stress.  [NCERT corpus] 
;यदि भार को और बढ़ा दिया जाए तो उत्पन्न प्रतिबल पराभव सामर्थ्य से अधिक हो जाता है और फिर प्रतिबल में थोड़े से अंतर के लिए भी विकृति तेज़ी से बढ़ती है.[NCERT corpus] 

;The point D on the graph is the ultimate tensile strength (Su) of the material. [NCERT corpus]  
;ग्राफ पर बिंदु D द्रव्य की चरम तनन सामर्थ्य (Su) है.[NCERT corpus] 

;If the ultimate strength and fracture points D and E are close, the material is said to be brittle. [NCERT corpus]  
;यदि चरम सामर्थ्य बिंदु D और विभंजन बिंदु E पास - पास हों तो द्रव्य को भंगुर कहते हैं.[NCERT corpus] 
;Table 9.1 gives the values of Young's moduli and yield strengths of some materials. [NCERT corpus] 
;सारणी 9.1 में कुछ द्रव्यों के यंग गुणांक तथा पराभव सामर्थ्य के मान दिए गए हैं.[NCERT corpus] 

(defrule strength1
(declare (salience 0))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(Domain physics);fact added by 14anu-ban-01 on (09-12-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sAmarWya))
(assert (id-domain_type  ?id physics));fact added by 14anu-ban-01 on (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  strength.clp  	strength1   "  ?id "  physics )" crlf);fact added by 14anu-ban-01 on (09-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength1   "  ?id " sAmarWya )" crlf))
)

;-------------------------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (23-08-2014).
;The strong nuclear force is the strongest of all fundamental forces, about 100 times the electromagnetic force in strength.[NCERT corpus] 
;यह प्रबल नाभिकीय बल सभी मूल बलों में प्रबलतम है जोकि प्रबलता में विद्युत - चुम्बकीय बल का लगभग 100 गुना है.  [NCERT corpus] 

(defrule strength2
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id);this condition can be generalized if needed.
(id-root ?id1 force)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prabalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength2  "  ?id " prabalawA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-08-2014).
;35682:The strength of the Legislative Assembly was provisionally fixed by the Act at 140 100 elected , 26 nominated officials and the rest nominated non - officials .[Karan Singla]
;अधिनियम के अधीन विधान सभा की सदस़्य संख़्या अस़्थाई रूप से 140 निर्धारित की गई . इनमें 100 निर्वाचित , 26 मनोनीत सरकारी सदस़्य और शेष मनोनीत गैर - सरकारी सदस़्य होते थे .[Karan Singla]
(defrule strength3
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 Assembly);for an unknown reason,'assembly' was not working here.
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saxasya_saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength3  "  ?id " saxasya_saMKyA )" crlf))
)



;@@@ Added by 14anu-ban-01 on (23-08-2014).
;86765:The combined strength of soldiers in both the armies was eighteen akshauhini.[Karan Singla:improvised]
;दोनों पक्षों की सेनाओं का सम्मिलित संख्याबल भी अठारह अक्षौहिणी था ।[Karan Singla]
(defrule strength4
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 soldier)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMKyA_bala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength4  "  ?id " saMKyA_bala )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-08-2014).
;7101:But as he held Urim and Thummim in his hand , they had transmitted to him the strength and will of the old king .[Karan Singla]
;लेकिन उसके हाथों में उरीम और थुमीम थे , जिनसे उसे बूढ़े बादशाह की ताकत और इच्छा शक्ति का बराबर अहसास हो रहा था ।[Karan Singla]
;लेकिन जैसे ही उसने उरीम और थुमीम को हाथों में पकड़ा, उसके अंदर पुराने बादशाह की सी ताकत और इच्छा शक्ति संचारित हो गई. [self]
;107935:The strength of these movements can be judged from the British reaction to them .
;इन आंदोलनों की ताकत का अनुमान अंग्रेजी हुकूमत की प्रतिक्रिया से लगाया जा सकता है .

(defrule strength5
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 king|movement)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wAkawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength5  "  ?id " wAkawa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-08-2014).
;So the ropes are always made of a number of thin wires braided together, like in pigtails, for ease in manufacture, flexibility and strength. [NCERT corpus]  
;इसलिए व्यापारिक निर्माण में लचक तथा मज़बूती के लिए ऐसे रस्सों को हमेशा वेणी की तरह बहुत से पतले तारों को गुम्फित करके आसानी से बनाया जाता है.[improvised:changed ' प्रबलता ' to 'मज़बूती']

(defrule strength6
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id);this condition can be generalized if needed.(kriyA-in_saMbanXI  11 22)
(id-root ?id1 braided)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id majZabUwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  strength.clp  	strength6  "  ?id " majZabUwI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-08-2014).
;104854:An important offshoot of linkage theory is that the strength or degree of linkage depends upon the distance between the linked genes in the chromosome .[Karan Singla: changed 'degree or strength' to 'strength or degree']
;सहलग़्नता सिद्धांत से प्राप़्त एक महत़्वपवूर्ण तथ़्य यह है कि सहलग़्नता की दृढ़ता अथवा कोटि गुणसूत्र में उपस़्थित जीनों के बीच के अंतर पर निर्भर करती है .[improvised: changed 'कोटि अथवा दृढ़ता' to 'दृढ़ता अथवा कोटि']
(defrule strength7
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 linkage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqDawA));changed 'word_mng' to 'root_mng' on (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  strength.clp  	strength7  "  ?id " xqDawA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-12-2014).
;The field strength due to qm at a distance r from it is given by µ 0qm / 4 π r2; The magnetic field due to the bar magnet is then obtained, both for the axial and the equatorial case, in a manner analogous to that of an electric dipole (Chapter 1).[NCERT corpus]
;r दूरी पर qm के कारण क्षेत्र की तीव्रता µ0qm/4πr2 होगी और तब अक्षीय एवं अनुप्रस्थ दोनों स्थितियों में इस छड चुम्बक के कारण चुम्बकीय क्षेत्र उसी प्रकार ज्ञात किया जा सकता है जैसा कि वैद्युत द्विध्रुव के लिए किया गया था (देखिए अध्याय 1)[NCERT corpus]
(defrule strength8
(declare (salience 1000))
(id-root ?id strength)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 field)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kRewra_kI_wIvrawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strength.clp  	strength8  "  ?id "  " ?id1 "  kRewra_kI_wIvrawA  )" crlf))
)

