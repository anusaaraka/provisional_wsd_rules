;@@@ Added by 14anu-ban-11 on (06-02-2015)
;There was a sting of sarcasm in his voice.(oald) 
;उसकी आवाज में व्यङ्ग्योक्ति का दर्द था . (anusaaraka)
(defrule sting2
(declare (salience 5001))
(id-root ?id sting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 sarcasm)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sting.clp 	sting2   "  ?id "  xarxa )" crlf))
)


;@@@ Added by 14anu-ban-11 on (06-02-2015)
;He was stung by their criticism.(oald)
;वह उनकी आलोचना के द्वारा उकसाया गया था .(anusaaraka) 
(defrule sting3
(declare (salience 5002))
(id-root ?id sting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 criticism)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasAyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sting.clp 	sting3   "  ?id "  ukasAyA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (06-02-2015)
;My legs were stinging due to excess walking.(hinkhoj)
;मेरी टाँगें अतिरिक्त टहलना के कारण दर्द हो रहीं थीं . (anusaaraka)
(defrule sting4
(declare (salience 5003))
(id-root ?id sting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 leg|hand|neck)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sting.clp 	sting4   "  ?id "  xarxa_ho )" crlf))
)


;-------------------- Default Rules ----------------

(defrule sting0
(declare (salience 5000))
(id-root ?id sting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id daMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sting.clp 	sting0   "  ?id "  daMka )" crlf))
)

;"sting","V","1.dZaMka mAranA"
;When you trouble a wasp it stings you.
(defrule sting1
(declare (salience 4900))
(id-root ?id sting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dZaMka_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sting.clp 	sting1   "  ?id "  dZaMka_mAra )" crlf))
)

;"sting","V","1.dZaMka mAranA"
;When you trouble a wasp it stings you.
;--"2.xarxa honA"
;My legs were stinging due to excess walking.
;--"3.ukasAnA"
;Their comments stung him to get into a fight.
;
;
