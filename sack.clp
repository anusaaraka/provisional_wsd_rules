
;@@@Added by 14anu04 on 23-June-2014
;We sacked out on the couch.
;हम खाट पर सो गये. 
(defrule sack_tmp
(declare (salience 5000))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 so))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sack.clp 	sack_tmp  "  ?id "  " ?id1 "  so )" crlf))
)

;@@@Added by 14anu04 on 23-June-2014
;Rome was sacked by the Goths in 1919.
;1919 में रोम गोथ्स के द्वारा लूट लिया गया था. 
(defrule sack_tmp2
(declare (salience 5000))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack_tmp2   "  ?id "  lUta_le)" crlf))
)


;@@@ Added by jagriti(14.2.2014)
;He always wears a sack.[rajpal]
;वह हमेशा ढीला-ढाला कुर्ता पहनता है . 
(defrule sack2
(declare (salience 5000))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 wear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlA-DAlA_kurwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack2   "  ?id "  DIlA-DAlA_kurwA)" crlf))
)
;@@@ Added by jagriti(14.2.2014)
;The robbers sacked his house in night.[rajpal]
;डाकुओं ने रात में उसका घर लूट लिया . 
(defrule sack3
(declare (salience 5000))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 robber|invader)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack3   "  ?id "  lUta_le)" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-12-2014)
;They sacked the town.[sack.clp]
;उन्होंने नगर को लूट लिया. [self] 
(defrule sack4
(declare (salience 5000))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 town|city|village)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta_le))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  sack.clp 	sack4   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack4   "  ?id "  lUta_le)" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-12-2014)
;The sack of troy was brutal.[self:with reference to coca]
;ट्रॉय की लूटपाट बर्बर थी. [self]
(defrule sack5
(declare (salience 100))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUtapAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack5   "  ?id "  lUtapAta)" crlf))
)


;@@@ Added by 14anu-ban-11 on (20-03-2015)
;Four hundred workers face the sack.(oald)
;चार सौ कार्यकर्ताओं को  बरखास्तगी का सामना करना पडा. (self)
(defrule sack6
(declare (salience 10))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraKZAswagI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack6   "  ?id "  baraKZAswagI)" crlf))
)


;@@@ Added by 14anu-ban-11 on (20-03-2015)
;He caught them in the sack together. (oald)
;उसने उनको एक साथ बिस्तर में पकडा .  (self)
(defrule sack7
(declare (salience 20))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 catch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biswara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack7   "  ?id "  biswara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (20-03-2015)
;The army rebelled and sacked the palace.(oald)
;सेना ने विद्रोह किया और महल लूट लिया .  (self)
(defrule sack8
(declare (salience 150))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 palace)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack8   "  ?id "  lUta_le)" crlf))
)


;....default rule....
(defrule sack0
(declare (salience 0));salience reduced from 100 to 0 by 14anu-ban-01 on (12-12-2014)
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id borA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack0   "  ?id "  borA )" crlf))
)

;"sack","N","1.borA/borI"
;The sack split && the rice poured out.
;--"2.lUtapAta"
;The sack of Troy.
;
(defrule sack1
(declare (salience 100))
(id-root ?id sack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sack.clp 	sack1   "  ?id "  nikAla_xe )" crlf))
)

;"sack","V","1.nikAla_xenA"
;He is sacked for incompetence.
;--"2.lUtanA"
;They sacked the town.
;The surface of the room is rough, I want a sander.
;
