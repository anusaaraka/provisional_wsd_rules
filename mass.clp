;Added by Meena(18.11.09)
;It had only a tenth of the sun's mass but showed some wobbling which could be due to planets in its orbit . 
(defrule mass0
(declare (salience 5000))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xravyamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp      mass0   "  ?id "  xravyamAna )" crlf))
)



;Salience reduced by Meena(18.11.09)
(defrule mass1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parimANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp 	mass1   "  ?id "  parimANa )" crlf))
)

;"mass","N","1.parimANa"
;There was masses of dark clouds in the sky.
;--"2.samUha"
;There were masses of people at the funeral.
;--"3.krIswa_yAga"
;You should go to the mass.
;
(defrule mass2
(declare (salience 4900))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp 	mass2   "  ?id "  ekawra_kara )" crlf))
)

(defrule mass3
(declare (salience 4800))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp 	mass3   "  ?id "  ekawra_ho )" crlf))
)

;"mass","V","1.ekawra_honA[karanA]"
;The general massed his troops for final attack.
;

;1.A mass of snow and rocks falling down the mountain. 
;
;@@@ Added by 14anu19
;2.I've eaten masses.
;मैं आधिक्य  में खा चुका हूँ! 
(defrule mass04
(declare (salience 4900))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXikya))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  mass.clp    mass04   "  ?id " meM )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp     mass04   "  ?id "  AXikya)" crlf)
)


;@@@ Added by 14anu24
;at privilege belongs to the masses .
;यह नियति तो जनता की है .
(defrule mass4
(declare (salience 5000))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp     mass4   "  ?id "  janawA )" crlf))
)

;@@@ Added by 14anu19
;3.Government attempts to suppress dissatisfaction among the masses.
;सरकार जनता के बीच में असन्तोष दबाने का प्रयास करती है
(defrule mass5
(declare (salience 4900))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-among_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp     mass5   "  ?id "  janawA )" crlf))
)

;@@@ Added by 14anu19
;The sky was full of dark masses of clouds.
;आसमान बादलों के अन्धेरे समूहों से भरा हुआ था . 
(defrule mass6
(declare (salience 5000))
(id-root ?id mass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samUha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mass.clp     mass6   "  ?id " samUha )" crlf))
)


