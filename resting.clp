;@@@ Added by 14anu-ban-10 on (08-09-2014)
;Therefore Vyasa , in the course of his dictation , dictated such sentences as compelled the writer to ponder over them , and thereby Vyasa gained time for resting awhile .
; लेखक को सोचना पड़ता था और इस प्रकार व़्यास को विश्राम के लिए समय मिल जाता था .
(defrule resting0
(declare (salience 0000))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting0  "  ?id "  viSrAma)" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;Resting means that you can spend your time with friends watching TV , etc .
;आराम करने का मतलब है कि आप अपना वक्‍त अपने दोस्तों के साथ टीवी देखकर आदि में गुजार सकती हैं ।
(defrule resting1
(declare (salience 200))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ? ) 
(id-cat_coarse ?id PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting1   "  ?id "  ArAma)" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;dada hari vava is step - well manifests a unique architectural feature of gujarat . step - wells were built to provide travelers with water and cool resting place .
;दादा हरि वावा कुओं गुजरात के अनोखी शिल्पकला की विशेषता को स्पष्ट करता है . स्टेप कुओःं यात्रियों को ठंडे आरामदायक स्थान तथा पानी प्रदान करने के लिए बनवाये गये थे .
(defrule resting2
(declare (salience 400))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAmaxAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting2   "  ?id "  ArAmaxAyaka)" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;The Yali mandapam obviously served as resting - place of the processional idols or the royalty during festivals .
;स़्पष़्ट है कि यह यलीमडपम उत़्सवों के दौरान शोभा यात्रा में निकली मूर्तियों या राजपुरूषों के लिए विश्राम स़्थल के रूप में काम आता था .
(defrule resting3
(declare (salience 600))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-as_saMbanXI ? ?id)(viSeRya-viSeRaNa ?id ? )) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting3   "  ?id "  viSrAma )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;aurangzeb chose khuldabad as his final resting place because the mausoleum of sayeed zain - ud - din , a muslim holy man lies next door .
;औरंगजेब ने खुलदाबाद को अपना अंतिम विरामस्थल चुना क्योंकि सैय्यद जैनुद्दीन , एक मुस्लिम संत का मकबरा , पडोस में ही है .
(defrule resting4
(declare (salience 800))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAmaswala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting4   "  ?id "  virAmaswala  )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;He sat on in the motionless heat of noon , his chin on his hand and elbows resting on his knees .
;कुहनियों को घुटनों पर रखकर , ठुड्डी को हाथ में टिकाए , वह चुपचाप दुपहर की उनींदी धूप में बैठा रहा ।
(defrule resting5
(declare (salience 1000))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-on_saMbanXI ?id ?) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKakara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting5   "  ?id "  raKakara  )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;This place of the devotion of the people of Pachmarhi and Maharashtra is also called the resting place of Shivji .
;पचमढ़ी  और  महाराष्ट्र  के  लोगों  की  आस्था  के  इस  स्थान  को  शिवजी  की  आश्रय  स्थली  भी  कहा  जाता  है  ।
(defrule resting6
(declare (salience 1200))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ? ?id) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASraya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting6   "  ?id "  ASraya )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;I was resting in the hotel when the phone rang and in chaste Kumauni words my safety was enquired .
;होटल में सुस्ता ही रहा था कि फोन की घंटी बजी और ठेठ कुमाउँनी बोल में कुशलक्षेम पूछी गयी ।
(defrule resting7
(declare (salience 1400))
(id-root ?id rest)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ? ?id) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suswA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting7   "  ?id "  suswA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-11-2014)
;Sit on a swivel chair with your arms folded and feet not resting on, i.e., away from, the ground.[ncert corpus]
;eka GumAva kursI para bETie apanI BujAez mode raKie Ora pEroM ko jamIna se Upara uTAkara raKie.[ncert corpus]
(defrule resting8
(declare (salience 1500))
(id-root ?id resting)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_niReXaka ?id ? )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTAkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resting.clp 	resting8   "  ?id " uTAkara)" crlf))
)


