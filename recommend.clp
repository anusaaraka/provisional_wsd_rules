;$$$ Modified by 14anu-ban-05 on (10-04-2015)
;@@@ Added by Anita 18.3.2014
;I can recommend the chicken in mushroom sauce - it's delicious. [cambridge dictionary]
;मैं मशरूम सॉस में चिकन की सिफारिश कर सकता हूँ- यह स्वादिष्ट है ।
(defrule recommend0
(declare (salience 4800))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?)
;(kriyA-vAkyakarma  ? ?id)	;commented by 14anu-ban-05 on (10-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id siPAriSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend0   "  ?id " siPAriSa_kara )" crlf))
)

;@@@ Added by Anita 12-06-2014
;The doctor has recommended changing my diet. [cambridge dictionary]
;डाक्टर ने मेरा आहार बदलने की सलाह दी है ।
(defrule recommend02
(declare (salience 5000))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 changing)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id salAha_xe))
(assert  (id-wsd_viBakwi   ?id1  kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend02   "  ?id " salAha_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  recommend.clp     recommend02   "  ?id1 "  kI )" crlf))
)


;@@@ Added by 14anu-ban-10 on (19-03-2015)
;The travel agent recommended strongly that we not travel on Thanksgiving Day.[hinkhoj]
;ट्रेवल एजेंट  ने  प्रभावशाली तरीके से  सिफारिश करी  कि हम थैंक्सगिविंग डॆअ पर यात्रा नहीं  करे .[manual] 
(defrule recommend3
(declare (salience 4900))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id siPAriSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend3   "  ?id " siPAriSa_kara )" crlf))
)


;@@@ Added by 14anu-ban-10 on (19-03-2015)
;The hotel is highly recommended for its excellent services.[hinkhoj]
;होटल उसकी उत्तम सेवाओं के लिए अत्यन्त प्रशंसा किया गाया है . [manual]
(defrule recommend4
(declare (salience 5000))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  praSaMsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend4   "  ?id "  praSaMsA_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-04-2015)
;She was recommended for the post by a colleague. [OALD]
;सहकार्यकर्ता के द्वारा पद के लिए उसकी सिफारिश की गयी थी .	[MANUAL]
(defrule recommend5
(declare (salience 5000))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  siPAriSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend5   "  ?id "  siPAriSa_kara )" crlf))
)


;---------------------- Default rule -----------------------------

;@@@ Added by 14anu-ban-05 on (10-04-2015)
;The hotel's new restaurant is highly recommended.[OALD-Modified]
;होटल के नये भोजनालय  की अत्यन्त तारीफ होती है .	[MANUAL] 
(defrule recommend6
(declare (salience 5000))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  wArIPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend6   "  ?id "  wArIPa )" crlf))
)


;@@@ Added by Anita 20.3.2014
;The SI with standard scheme of symbols units and abbreviations was developed and recommended by ;General Conference on Weights and Measures in 1971 for international usage in scientific technical ;industrial and commercial work. [By mail}
;एस आई प्रतीकों, मात्रकों और उनके सङ्केताक्षरों की योजना 1971 में, मापतोल के महा सम्मेलन द्वारा विकसित कर, वैज्ञानिक, तकनीकी, ;औद्योगिक एवं व्यापारिक कार्यों में अन्तर्राष्ट्रीय स्तर पर उपयोग हेतु अनुमोदित की गई ।
(defrule recommend1
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumoxiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend1   "  ?id " anumoxiwa_kara )" crlf))
)

;---------------------------

;She has been recommended for promotion. [cambridge dictionary]
;पदोन्नति के लिए उसकी सिफारिश की गई है ।
;The headmistress agreed to recommend the teachers' proposals to the school governors. [+ (that)] [cambridge dictionary]
;स्कूल की संचालिका स्कूल राज्यपालों से शिक्षकों के प्रस्तावों की सिफारिश करने पर सहमत हुए.
;The doctor recommended that I take more exercise. [+ -ing verb] [cambridge dictionary]
;डॉक्टर ने मुझे सलाह दी कि और अधिक व्यायाम करूँ ।
;I recommend writing your feelings down on paper. [cambridge dictionary]
;मैं कागज पर अपनी भावनाओं को नीचे लिखने की सलाह देते हैं.
;The city has little to recommend it .[cambridge dictionary]
;शहर को इसके बारे में बताने के लिए बहुत थोड़ा  हैं ।

;@@@ Added by 14anu17
;No countries recommend giving the vaccines separately .
;कोई देश अलग-अलग टीकाएँ  देने का सलाह नहीं देगा . 
(defrule recommend2
(declare (salience 100))
(id-root ?id recommend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id salAha_xenA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recommend.clp 	recommend2   "  ?id " salAha_xenA )" crlf))
)
