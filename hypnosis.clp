;@@@ Added by 14anu-ban-06 (16-04-2015)
;Hypnosis helped me give up smoking.  (OALD)
;सम्मोहन ने मुझे धूम्रपान छोडने में सहायता की . (manual)
(defrule hypnosis1
(declare (salience 2000))
(id-root ?id hypnosis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(id-root ?id1 help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammohana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hypnosis.clp 	hypnosis1   "  ?id "  sammohana )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-04-2015)
;He uses hypnosis as part of the treatment. (OALD)
;वह इलाज के भाग के रूप में सम्मोहन का उपयोग करता है . (manual)
(defrule hypnosis2
(declare (salience 2200))
(id-root ?id hypnosis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 use)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammohana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hypnosis.clp 	hypnosis2   "  ?id "  sammohana )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (16-04-2015)
;She only remembered details of the accident under hypnosis. (OALD)
;उसे केवल बेसुधि में दुर्घटना के विवरण याद आए . (manual)
(defrule hypnosis0
(declare (salience 0))
(id-root ?id hypnosis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id besuXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hypnosis.clp 	hypnosis0   "  ?id "  besuXi )" crlf))
)
