(defrule expand0
(declare (salience 5000))
(id-root ?id expand)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 balloon)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expand.clp 	expand0   "  ?id "  PUla )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-02-2015)
;Students number are expanding rapidly.                     [oald]
;विद्यार्थीयों की संख्या शीघ्रता से बढ़ रही हैं .                               [self]
;A child's vocabulary expands through reading.              [oald]
;बच्चे की शब्दावली अध्ययन से बढ़ती है .                                  [self]
(defrule expand3
(declare (salience 4920))
(id-root ?id expand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 number|vocabulary)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expand.clp 	expand3   "  ?id "  baDa )" crlf))
)

;@@@ Added by 14anu-ban-04 (17-02-2015)
;Could you expand on that point, please?                      [oald]
;कृपया क्या आप  इस बिंदु  पर विस्तार कर सकते है?                            [self]
(defrule expand4
(declare (salience 4920))
(id-root ?id expand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 on)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viswAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  expand.clp     expand4   "  ?id " para  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " expand.clp	expand4 "  ?id "  " ?id1 "  viswAra_kara  )" crlf))
)

(defrule expand1
(declare (salience 4900))
(id-root ?id expand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expand.clp 	expand1   "  ?id "  PEla )" crlf))
)

;The Universe is expanding : viSva PEla rahA hE
;"expand","V","1.PEla_jAnA"
;His business expanded rapidly.
;

;$$$ Modified by 14anu-ban-04 (16-02-2015)
;After two years he expanded his business.                  [same clp file]
;दो वर्षों  के बाद उसने अपना उद्योग फैलाया .                                [self]
;"expand","VT","1.PElAnA"
;After two years he expanded his business.
(defrule expand2
(declare (salience 4910))                        ;salience increased from '4800' to 4910' by 14anu-ban-04 on (16-02-2015)
(id-root ?id expand)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?)                      ;added by 14anu-ban-04 on (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expand.clp 	expand2   "  ?id "  PElA )" crlf))
)

;
