;@@@ Added by 14anu-ban-03 (24-02-2015)
;The concept of the potential energy would not be meaningful if the work depended on the path. [ncert]
;sWiwija UrjA kI XAraNA arWapUrNa nahIM rahegI, yaxi kiyA gayA kArya paWa para nirBara ho jAegA. [ncert]
(defrule concept0
(declare (salience 00))
(id-root ?id concept)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAraNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concept.clp 	concept0   "  ?id "  XAraNA )" crlf))
)



;@@@ Added by 14anu-ban-03 (24-02-2015)
;Truth and beauty are abstract concepts.[cambridge]
;सत्य और सुन्दरता काल्पनिक मनोभाव हैं.[self]
(defrule concept1
(declare (salience 10))
(id-root ?id concept)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 abstract)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concept.clp 	concept1   "  ?id "  manoBAva )" crlf))
)
