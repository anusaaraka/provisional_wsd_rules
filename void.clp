;@@@Added by 14anu-ban-07,(31-03-2015)
;The lawyers declared the contract  void.(cambridge)(parser no. 2)
; वकीलों ने अनुबन्ध को व्यर्थ घोषित किया . (manual)
(defrule void3
(declare (salience 5100))
(id-root ?id void)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 contract)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyarWa))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  void.clp 	void3   "  ?id "  vyarWa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  void.clp      void3   "  ?id1 " ko )" crlf)
)
)

;------------------------ Default Rules ----------------------

;"void","Adj","1.rikwa"
;Her face was void of all emotions
(defrule void0
(declare (salience 5000))
(id-root ?id void)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rikwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  void.clp 	void0   "  ?id "  rikwa )" crlf))
)

;"void","N","1.SUnya_sWAna"
;After climbing he found that below him there was nothing but a void
;His wife's death created a void in his life.
(defrule void1
(declare (salience 4900))
(id-root ?id void)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SUnya_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  void.clp 	void1   "  ?id "  SUnya_sWAna )" crlf))
)

;"void","VI","1.avEXAnika_GoRiwa_karanA"
;The authorities had to void his passport
(defrule void2
(declare (salience 4800))
(id-root ?id void)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avEXAnika_GoRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  void.clp 	void2   "  ?id "  avEXAnika_GoRiwa_kara )" crlf))
)

