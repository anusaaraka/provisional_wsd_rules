;@@@ Added by 14anu-ban-04 (19-03-2015)
;The marriage was doomed from the start.         [oald]             
;शादी आरम्भ से बरबाद  हो गयी थी .                         [self]
(defrule doom2
(declare (salience 20))
(id-root ?id doom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barabAxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  doom.clp    doom2  "  ?id " barabAxa_ho)" crlf))
)

;@@@ Added by 14anu-ban-04 (19-03-2015)
;Are we doomed to repeat the mistakes of the past?                  [cald]
;क्या हम भूतकाल की गलतियाँ   दोहराने के लिए  अभिशप्त  किए जाते हैं?                      [self]
(defrule doom3
(declare (salience 20))
(id-root ?id doom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?kri)
(kriyA-object ?kri ?id1)
(id-root ?id1 mistake|error|fault)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBiSapwa_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  doom.clp    doom3  "  ?id " aBiSapwa_kara)" crlf))
)

;@@@ Added by 14anu-ban-04 (19-03-2015)
;He sealed his own doom by having an affair with another woman.               [oald]
;उसने अपने ही भाग्य को  बन्द कर दिया  एक दूसरी स्त्री के साथ प्रेम सम्बन्ध रखकर  .                     [self]
(defrule doom4
(declare (salience 20))
(id-root ?id doom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAgya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  doom.clp    doom4  "  ?id " BAgya)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (19-03-2015)
;A criminal record will doom your chances of becoming a politician.                  [merriam-webster]
;एक आपराधिक रिकार्ड आपकी  राजनीतिज्ञ बनने की सम्भावनाओं को  बरबाद कर देगा .                               [self]
(defrule doom0
(declare (salience 10))
(id-root ?id doom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id barabAxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   doom.clp      doom0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  doom.clp    doom0  "  ?id " barabAxa_kara_xe)" crlf))
)

;@@@ Added by 14anu-ban-04 (19-03-2015)
;The newspapers are always full of doom and gloom these days.            [cald]
;इन दिनों  समाचारपत्र  हमेशा  विनाश  और निराशा से  भरे हुए हैं .                              [self]
(defrule doom1
(declare (salience 10))
(id-root ?id doom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  doom.clp    doom1  "  ?id " vinASa)" crlf))
)

