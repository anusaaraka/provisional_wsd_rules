
;"stressed","Adj","1.xabAva se pIdiwa"
;Students feel stressed under heavy pressure of work.
(defrule stress0
(declare (salience 0));salience reduced from 5000 to 0 by 14anu-ban-01 on (19-10-2014)
(id-root ?id stress)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id stressed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xabAva_se_pIdiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stress.clp  	stress0   "  ?id "  xabAva_se_pIdiwa )" crlf))
)



;@@@ Added by 14anu-ban-01 on (24-10-2014)
;The volume of solid, liquid or gas depends on the stress or pressure acting on it.[Ncert Corpus]
;Tosa, xrava aWavA gEsa kA Ayawana isa para lagane vAle prawibala aWavA xAba para nirBara hE.[Ncert Corpus]
(defrule stress3
(declare (salience 100))
(id-root ?id stress)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id prawibala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stress.clp 	stress3   "  ?id " prawibala)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  stress.clp 	stress3   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (09-02-2015)
;The concept of a field was specially stressed by Faraday and was incorporated by Maxwell in his unification of electricity and magnetism.[NCERT corpus]
;क्षेत्र की अभिधारणा को फैराडे द्वारा विशेष महत्त्व दिया गया तथा मैक्सवेल ने विद्युत तथा चुम्बकत्व को एकीकृत करने में इस अभिधारणा को समावेशित किया.[NCERT corpus]
(defrule stress4
(declare (salience 1000))
(id-root ?id stress)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str)		
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-karma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawva_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stress.clp 	stress4  "  ?id "  mahawva_xe )" crlf))
)


;---------------------- Default Rules -------------------
;$$$ Modified by jagriti(3.4.2014)...default meaning wanAva instead of  balAGAwa
;According to the sources, Rajaram Pandey was under continuous stress after being removed from the post of Minister.
;सूत्रों के हवाले से आ रही खबर के मुताबिक, राजाराम मंत्री पद से हटाए जाने के बाद से तनाव में रहते थे।
(defrule stress1
(declare (salience 0));salience reduced from 5000 to 0 by 14anu-ban-01 on (19-10-2014)
(id-root ?id stress)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wanAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stress.clp 	stress1   "  ?id "  wanAva )" crlf))
)

;"stress","N","1.balAGAwa"
;--"2.bala"
;She put stress on the need of extra security in the border areas.
;--"3.xabAva"
;Due to long working hours Ramu is always under stress.



;"stress","V","1.bala_xenA"
;India should stress more on providing employment.
(defrule stress2
(declare (salience 100))
(id-root ?id stress)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bala_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stress.clp 	stress2   "  ?id "  bala_xe )" crlf))
)

