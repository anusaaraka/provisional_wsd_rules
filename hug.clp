;@@@ Added by 14anu-ban-06 (25-02-2015)
;The track hugs the coast for a mile. (OALD)
;ट्रैक कई मील दूर तक समुद्र तट के साथ साथ चलता है .(manual) 
(defrule hug2
(declare (salience 5100))
(id-root ?id hug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 track|road)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa_sAWa_cala))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hug.clp 	hug2   "  ?id "  sAWa_sAWa_cala )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hug.clp       hug2   "  ?id " ko )" crlf))
)

;------------------------ Default Rules ----------------------

;"hug","N","1.AliMgana"
;miwra ke 'hug se suxAmA ke AzKoM meM AsUz Bara Ae.
(defrule hug0
(declare (salience 5000))
(id-root ?id hug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AliMgana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hug.clp 	hug0   "  ?id "  AliMgana )" crlf))
)

;"hug","V","1.sIne_se_lagAnA"
;kqRNa apane miwra ko 'hug' kiyA.
(defrule hug1
(declare (salience 4900))
(id-root ?id hug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIne_se_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hug.clp 	hug1   "  ?id "  sIne_se_lagA )" crlf))
)

