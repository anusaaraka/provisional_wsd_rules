;$$$ Modified by 14anu-ban-06 (20-02-2015)
;The girl has irregular teeth.(irregular.clp)       [parser no. 2]
;लडकी के दाँत टेढे-मेढे हैं . (manual) 
(defrule irregular1
(declare (salience 5100));salience increased to '5100' by 14anu-ban-06 (20-02-2015)
(id-root ?id irregular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)            ;added by 14anu-ban-06 (20-02-2015)
(id-word ?id1 teeth)                   ;added by 14anu-ban-06 (20-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teDZA_meDZA))       ;meaning changed from 'asama' to 'teDZA_meDZA' by 14anu-ban-06 (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregular.clp 	irregular1   "  ?id "  teDZA_meDZA )" crlf))                                   ;meaning changed from 'asama' to 'teDZA_meDZA' by 14anu-ban-06 (20-02-2015)
)

;@@@ Added by 14anu-ban-06 (20-02-2015)
;Irregular students do not perform well.(irregular.clp)             [parser no. 2]
;अनियमित विद्यार्थी अच्छा प्रदर्शन नहीं करते हैं .(manual) 
(defrule irregular2
(declare (salience 5100))
(id-root ?id irregular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 student)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyamiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregular.clp 	irregular2   "  ?id "  aniyamiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;The irregular troops are taking part in the parade.(irregular.clp)[parser no. 3]
;सशस्त्र सेना से असम्बद्ध दल जलूस में भाग ले रहे हैं . (manual)
(defrule irregular3
(declare (salience 5200))
(id-root ?id irregular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 troop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saSaswra_senA_se_asaMbaxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregular.clp 	irregular3   "  ?id "  saSaswra_senA_se_asaMbaxXa )" crlf))
)

;@@@ Added by 14anu-ban-06 (21-03-2015)
;An irregular verb.(OALD)[parser no. 6]
;अव्यवस्थित क्रिया. (manual)
;There are irregular verbs in grammar.(irregular.clp)[parser no. 2]
;व्याकरण में अव्यवस्थित क्रियाएँ हैं . (manual)
(defrule irregular4
(declare (salience 5200))
(id-root ?id irregular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregular.clp 	irregular4   "  ?id "  avyavasWiwa )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule irregular0
(declare (salience 5000))
(id-root ?id irregular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asAmAnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irregular.clp 	irregular0   "  ?id "  asAmAnya )" crlf))
)


;"irregular","Adj","1.asama"
;The girl has irregular teeth.
;--"2.aniyamiwa"
;Irregular students do not perform well.
;--"3.avyavasWiwa"
;There are irregular verbs in grammar.
;--"4.saSaswra_senA_se_asaMbaxXa"
;The irregular troops are taking part in the parade.
;
;
