
(defrule park0
(declare (salience 5000))
(id-root ?id park)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id parking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gAdZI_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  park.clp  	park0   "  ?id "  gAdZI_sWAna )" crlf))
)

;"parking","N","1.gAdZI sWAna"
;There is no parking between 1 P.M. to 6 P.M.
;
(defrule park1
(declare (salience 4900))
(id-root ?id park)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxyAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  park.clp 	park1   "  ?id "  uxyAna )" crlf))
)

;"park","N","1.uxyAna"
;There is a big park in our colony.
;

;$$$ Modified meaning by Sonam Gupta MTech IT Banasthali 31-12-2013 (from gAdZI_KadA_kara to KadA_kara)
;$$$ Modified by 14anu01 on 27-06-2014
;Where did you park the car I can not remember.
;आपने गाडी कहाँ खडी की मैं याद नहीं कर पा रहा हूँ . 
(defrule park2
(declare (salience 4800))
(id-root ?id park)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  park.clp 	park2   "  ?id "  KadA_kara )" crlf))
);replaced 'gAdZI_KadA_kara' by 'KadA_kara' by 14anu01 

;"park","V","1.gAdZI_KadA_karanA"
;Park the car under the tree.
;

;$$$ Modified by 14anu-ban-09 on (16-01-2015)
;Changed meaning from 'gAdZI_KadA_kara' to 'gAdZI_KadI_kara'
;###[COUNTER EXAMPLE]### Where did you park the car I can not remember. [Same clp file]  ;added by 14anu-ban-09 on (16-01-2015)
;###[COUNTER EXAMPLE]### आपने गाड़ी कहाँ खडी की मैं याद नहीं कर पा रहा हूँ .             [Manual]      ;added by 14anu-ban-09 on (16-01-2015)
;###[COUNTER EXAMPLE]### There were two cars parked on the drive. [drive.clp]            ;added by 14anu-ban-09 on (16-01-2015)
;###[COUNTER EXAMPLE]### सड़क पर दो गाडियाँ खडी की गई थीं .                                     ;added by 14anu-ban-09 on (16-01-2015)
;@@@ Added by 14anu01 on 27-06-2014
;Someone had parked behind us and boxed us in.
;किसीने हमारे पीछे गाडी खडा किया था और  हमें बक्से में बन्द किया .
;किसीने हमारे पीछे गाड़ी खड़ी कि और हमारा रास्ता रोक दिया. [Self] ;Translation added by 14anu-ban-09 on (16-01-2015)

(defrule park3
(declare (salience 5500))
(id-root ?id park)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object ?id ?id1)) ;added by 14anu-ban-09 on (16-01-2015)
;(not(id-word ?id1 car|bike|vehicle|motor_cycle|cycle|taxicabs|taxis|tractors|trucks|vans|wagons)) ;commented by 14anu-ban-09 on (16-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAdZI_KadI_kara))                            ;modified 'gAdZI_KadA_kara' to 'gAdZI_KadI_kara' by 14anu-ban-09 on (15-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  park.clp 	park3   "  ?id "  gAdZI_KadI_kara )" crlf))                               ;modified 'gAdZI_KadA_kara' to 'gAdZI_KadI_kara' by 14anu-ban-09 on (16-01-2015)
)

;@@@ Added by 14anu-ban-09 on (16-01-2015)
;There were two cars parked on the drive. [drive.clp]
;सड़क पर दो गाडियाँ खडी की गई थीं . [Self]

(defrule park4
(declare (salience 5600))
(id-root ?id park)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)(kriyA-subject ?id ?id1))
(id-root ?id1 car|bike|vehicle|motor_cycle|cycle|taxicab|taxi|tractor|truck|van|wagon)                                                                       
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadA_kara))                            
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  park.clp 	park4   "  ?id "  KadA_kara )" crlf))                               
)
