;@@@ Added by 14anu-ban-10 on (17-03-2015)
;Infantrymen were in the rear.[hinkhoj]
;प्यादे  पिछले भाग  में थे . [manual]
(defrule rear2
(declare (salience 5100))
(id-root ?id rear)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  piCale_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rear.clp 	rear2   "  ?id "   piCale_BAga)" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-03-2015)
;The male tiger helps the female to rear the youngs.[hinkhoj]
;नर शेर पालन पोषण करने के लिए मादा की सहायता करता है . [manual]
(defrule rear3
(declare (salience 5200))
(id-root ?id rear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 young)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  pAlana_poRaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rear.clp 	rear3   "  ?id "   pAlana_poRaNa_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-03-2015)
;The horse reared up in fright.[hinkhoj]
;घोडा भय में  पिछले पैर पर खड़े होना .[manual]
(defrule rear4
(declare (salience 5300))
(id-root ?id rear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  piCale_pEra_para_KadZe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rear.clp 	rear4   "  ?id "    piCale_pEra_para_KadZe_ho)" crlf))
)

;------------------------ Default Rules ----------------------

;"rear","N","1.piCavAdZA"
;He has taken a photograph of the house from the rear.
(defrule rear0
(declare (salience 5000))
(id-root ?id rear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCavAdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rear.clp 	rear0   "  ?id "  piCavAdZA )" crlf))
)

;"rear","VTI","1.uTAnA"
;The snake reared its head. 
(defrule rear1
(declare (salience 4900))
(id-root ?id rear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rear.clp 	rear1   "  ?id "  uTA )" crlf))
)

;"rear","VTI","1.uTAnA"
;The snake reared its head. 
;--"2.pAlana_poRaNa_karanA"
;The male tiger helps the female to rear the youngs.
;--"3.piCale_pEra_para_KadZe_honA"
;The horse reared up in fright.
;
;"rear","N","1.piCavAdZA"
;He has taken a photograph of the house from the rear.
;--"2.pICe_kA_BAga"
;He kicked him on the rear.
;

