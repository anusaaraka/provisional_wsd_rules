;commented by 14anu-ban-06 (16-01-2015) because rule is too general.
;$$$ Modified by 14anu-ban-06 (02-12-2014)
;@@@ Added by 14anu03 on 18-june-14
;His hope was misplaced,so was his head. 
;उसकी आशा भटकी हुयी गयी थी, वैसे ही उसका दिमाग था .
;(defrule head100
;(declare (salience 5500))
;(id-root ?id head)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-RaRTI_viSeRaNa ?id ?id1)
;(id-word ?id1 his|her)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ximAga))      ;spelling corrected from 'xImAga' to 'ximAga' by 14anu-ban-06 (02-12-2014)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp      ;head100   "  ?id "  ximAga)" crlf))
;)

(defrule head0
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id headed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id siravAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  head.clp  	head0   "  ?id "  siravAlA )" crlf))
)

;"headed","Adj","1.siravAlA"
;hipopotomasa badA"headed" jAnavara hE.
;
;
(defrule head1
(declare (salience 4900))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kI_waraPa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head1  "  ?id "  " ?id1 "  kI_waraPa_jA  )" crlf))
)

;###Commented by 14anu02 on 3.7.14 since here 'head-off' means 'kI_waraPa_jA' for which already a rule is added
;He headed off towards the river to have some water.
;vaha pAnI lene ke lie naxI kI waraPa calA gayA
;(defrule head2
;(declare (salience 4800))
;(id-root ?id head)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 off)
;(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Age_baDZakara_modZa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " head.clp head2 " ?id "  Age_baDZakara_modZa )" crlf)) 
;)

(defrule head3
(declare (salience 4700))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZakara_modZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head3  "  ?id "  " ?id1 "  Age_baDZakara_modZa  )" crlf))
)

(defrule head4
(declare (salience 4600))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head4   "  ?id "  sira )" crlf))
)


;default_sense && category=noun	SIrRa	0
; 'sira' is a simple word than 'SirRa'
;default_sense && category=noun	sira	0
;"head","N","1.sira"
;usakI "head" para cota lagI hE.
;
;
;LEVEL 
;Headword : head 
;
;Examples --
;
;--"1.sira"
;He fell && hurt his head.
;vaha gira gayA Ora usake sira ko cota laga gayI. 
;--"2.ximAga/buxXi"
;Use your head to solve this problem.
;isa samasyA ko sulaJAne ke liye apane ximAga kA iswamAla karo.
;Meena had a good head for Mathematics. 
;mInA ke pAsa gaNiwa ke liye acCA ximAga hE.
;mInA kA ximAga gaNiwa meM calawA hE.
;--"3.mana/maswiRka"
;The idea never entered my head.
;yaha vicAra kaBI mere mana meM nahIM AyA.
;--"4.SIrRaka"
;The heading of the seminar is 'Freedom'.
;saMgoRTI kA SIrRaka hE 'svecCA'.
;--"5.praXAna"
;Sonia is the head of the Congress party.
;kAzgresa pArti kI newA soniyA hE.
;--"6.UzcAI{sira ke barAbara kA nApa}"
;That Boy was taller than his father by a head.
;vaha ladZakA apane piwA se eka sira (ke nApa ke barAbara) UzcA WA.  
;--"7.SIrRa"
;China is located on the head of the India.
;BArawa ke SIrRa para cIna xeSa sWiwa hE.    
;--"8.caramabiMxu"             
;Hike of petrol prices brought some tensions to common people but   this latest rise of Electricity charges brought the matters to a head.
;petrola ke xAmoM meM baDowarI ne Ama janawA ko xabAva meM dAlA WA , para hAla hI meM kI gayI vixyuwa xaroM meM vqxXi ne sWiwi ko caramabiMxu para pahuzcA xiyA hE.
;
;
;anwarnihiwa sUwra ; 
;
;  SIrRa - sira--praXAna/SIrRaka -caramabinxu
;          |
;          |-buxXi--mana  
;  
;
;sUwra : SIrRa` 
;
;
;          vyAKyA - yahAz hama xeKa sakawe hE ki "head" Sabxa kA prayoga alaga-alaga 
;                  saMxarBo meM howe hue BI unakA eka sAmAnya arWa nikalawA hE.
;                  uparokwa uxAharaNoM se yaha sAmAnyawA spaRta howA hE.        
;             
;         'sira' yaha Sabxa mUla saMskqwa "SIrRa" se vyuwpanna Sabxa hE. SarIra ke 
;    sabase UparI BAga ko 'SIrRa' kahA gayA. isa Sabxa ko hinxI meM 'sira' kahA gayA.
;    isa sira se saMbaMXiwa waWya 'mana','maswiRka','ximAga','buxXi','jFAna' Axi
;    Sabxa jude.
;         
;    SarIra kA sabase UparI BAga hone ke kAraNa 'agraBAga','SIrRaka' 
;    Ora 'UzcAI' Axi Sabxa ukwa saMxarBo meM prayoga meM Awe hEM .
;
(defrule head5
(declare (salience 4500))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id heading )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SIrRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  head.clp  	head5   "  ?id "  SIrRaka )" crlf))
)

;"heading","N","1.SIrRaka"
;What will be the heading of this story?
;isa kahAnI kA SIrRaka kyA hogA ?
;

;###Commented by 14anu02 on 3.7.14 since the same rule is already added as 'head1'
;(defrule head6
;(declare (salience 4400))
;(id-root ?id head)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 off)
;(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kI_waraPa_jA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head6  "  ?id "  " ?id1 "  kI_waraPa_jA  )" crlf))
;)

;###Commented by 14anu02 on 3.7.14 since here 'head-off' means 'kI_waraPa_jA' for which already a rule is added
;He headed off towards the river to have some water.
;vaha pAnI lene ke lie naxI kI waraPa calA gayA
;(defrule head7
;(declare (salience 4300))
;(id-root ?id head)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 off)
;(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Age_baDZakara_modZa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " head.clp head7 " ?id "  Age_baDZakara_modZa )" crlf)) 
;)

(defrule head8
(declare (salience 4200))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZakara_modZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head8  "  ?id "  " ?id1 "  Age_baDZakara_modZa  )" crlf))
)

(defrule head9
(declare (salience 4100))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head9   "  ?id "  sira )" crlf))
)

;$$$ Modified by 14anu-ban-02(22-02-2016)
;I have been invited to head the department .[sd_verified]
;mEM viBAga kI aXyakRawA karane ke lie Amanwriwa kiyA gayA hUz.[sd_verified]
;$$$ Modified by 14anu-ban-06 (10-01-2015)
;$$$ Modified by 14anu02 on 3.7.14
;counter ex-Where are we heading?
;Anusaaraka translation-हम कहाँ अध्यक्षता कर रहे हैं?
;She has been appointed to head the research team.
;वह शोध टीम की अध्यक्षता करने के लिए नियुक्त की गयी है .
;Added by sheetal(26-10-09).
(defrule head10
(declare (salience 4000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1);added ?id1 by 14anu02 because 'head' in this sense is a transitive verb
(id-root ?id1 team|department);added by 14anu-ban-06 (10-01-2015)	;'department' is added by 14anu-ban-02(22-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXyakRawA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp      head10   "  ?id "  aXyakRawA_kara )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  head.clp      head10   "  ?id " kI )" crlf)
)

;@@@ Added by 14anu02 on 3.7.14
;Where are we heading?
;हम कहाँ जा रहे हैं? 
(defrule head013
(declare (salience 3000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp      head013   "  ?id "  jA )" crlf))
)

;@@@ Added by 14anu02 on 3.7.14
;She headed for the door.
;वह दरवाजा की तरफ गई . 
(defrule head014
(declare (salience 4400))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) toward|out|for)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) kI_waraPa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head014  "  ?id "  " (+ ?id 1) "  kI_waraPa_jA  )" crlf))
)



;@@@ Added by Prachi Rathore[15-1-14]
;Is it heads or tails? [m-w]
;क्या यह हेड्स या टेल्स है? 
(defrule head11
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(conjunction-components  ? ?id ?id1)
(id-root ?id1 tail)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hedsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head11 "  ?id " hedsa )" crlf))
)

;@@@ Added by Prachi Rathore[3-3-14]
; During his tenure as head coach, the team won the championship twice. [m-w]
;मुख्य प्रशिक्षक के रूप में उसके कार्य काल के दौरान, टीम ने दो बार  चैम्पियनशिप जीती . 
(defrule head12
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head12   "  ?id "  muKya )" crlf))
)
;I have been invited to head the department .

;default_sense && category=noun	SIrRa	0
; 'sira' is a simple word than 'SirRa'
;default_sense && category=noun	sira	0
;"head","N","1.sira"
;usakI "head" para cota lagI hE.
;
;
;LEVEL 
;Headword : head 
;
;Examples --
;
;--"1.sira"
;He fell && hurt his head.
;vaha gira gayA Ora usake sira ko cota laga gayI. 
;--"2.ximAga/buxXi"
;Use your head to solve this problem.
;isa samasyA ko sulaJAne ke liye apane ximAga kA iswamAla karo.
;Meena had a good head for Mathematics. 
;mInA ke pAsa gaNiwa ke liye acCA ximAga hE.
;mInA kA ximAga gaNiwa meM calawA hE.
;--"3.mana/maswiRka"
;The idea never entered my head.
;yaha vicAra kaBI mere mana meM nahIM AyA.
;--"4.SIrRaka"
;The heading of the seminar is 'Freedom'.
;saMgoRTI kA SIrRaka hE 'svecCA'.
;--"5.praXAna"
;Sonia is the head of the Congress party.
;kAzgresa pArti kI newA soniyA hE.
;--"6.UzcAI{sira ke barAbara kA nApa}"
;That Boy was taller than his father by a head.
;vaha ladZakA apane piwA se eka sira (ke nApa ke barAbara) UzcA WA.  
;--"7.SIrRa"
;China is located on the head of the India.
;BArawa ke SIrRa para cIna xeSa sWiwa hE.    
;--"8.caramabiMxu"             
;Hike of petrol prices brought some tensions to common people but   this latest rise of Electricity charges brought the matters to a head.
;petrola ke xAmoM meM baDowarI ne Ama janawA ko xabAva meM dAlA WA , para hAla hI meM kI gayI vixyuwa xaroM meM vqxXi ne sWiwi ko caramabiMxu para pahuzcA xiyA hE.
;
;
;anwarnihiwa sUwra ; 
;
;  SIrRa - sira--praXAna/SIrRaka -caramabinxu
;          |
;          |-buxXi--mana  
;  
;
;sUwra : SIrRa` 
;
;
;          vyAKyA - yahAz hama xeKa sakawe hE ki "head" Sabxa kA prayoga alaga-alaga 
;                  saMxarBo meM howe hue BI unakA eka sAmAnya arWa nikalawA hE.
;                  uparokwa uxAharaNoM se yaha sAmAnyawA spaRta howA hE.        
;             
;         'sira' yaha Sabxa mUla saMskqwa "SIrRa" se vyuwpanna Sabxa hE. SarIra ke 
;    sabase UparI BAga ko 'SIrRa' kahA gayA. isa Sabxa ko hinxI meM 'sira' kahA gayA.
;    isa sira se saMbaMXiwa waWya 'mana','maswiRka','ximAga','buxXi','jFAna' Axi
;    Sabxa jude.
;         
;    SarIra kA sabase UparI BAga hone ke kAraNa 'agraBAga','SIrRaka' 
;    Ora 'UzcAI' Axi Sabxa ukwa saMxarBo meM prayoga meM Awe hEM .
; 
 
;@@@ Added by 14anu-ban-06   (13-08-2014)
;To find the sum A+B, we place vector B so that its tail is at the head of the vector A, as in Fig. 4.4(b). (NCERT)
;yoga @A+@B prApwa karane ke lie ciwra 4.4(@b) ke anusAra hama saxiSa @B isa prakAra raKawe hEM kiu sakI pucCa saxiSa @A ke SIrRa para ho .
;Then we draw a line from the head of A parallel to B and another line from the head of B parallel to A to complete a parallelogram OQSP. (NCERT)
;Pira hama @A ke SIrRa se @B ke samAnwara eka reKA KIzcawe hEM Ora @B ke SIrRa se @A ke samAnwara eka xUsarI reKA KIzcakara samAnwara cawurBuja @OQSP pUrA karawe hEM .
(defrule head13
(declare (salience 5100))
(id-root ?id head)
(Domain physics)      ;changed domain from general to physics by 14anu-ban-06 (02-12-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIrRa))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head13   "  ?id "  SIrRa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  head.clp 	head13   "  ?id "  physics )" crlf)
)
)

;@@@ Added by 14anu-ban-06  (14-08-2014)
;Being the head of the State , the duties of the President is to maintain discipline in all bodies .(Parallel corpus)
;राष्ट्रपतिजो कि राष्ट्र का प्रमुख है की भूमिका अधिकतर आनुष्ठानिक ही है ।
;The chief minister , after initial resistance , bowed to his colleagues'pressure to allow the ministers and the head of departments to travel in air - conditioned cars .(Parallel corpus)
;मुयमंत्री ने शुरू में प्रतिरोध दिखाया लेकिन अपने सहयोगियों के दबाव में आकर मंत्रियों और विभाग प्रमुखों को वातानुकूलित कारों में घूमने की इजाजत दे दी .
;Prime Minister is the head of the Government ; all the administrative powers are vested with him .(Parallel corpus)
;प्रधानमन्त्री सरकार का प्रमुख है और कार्यपालिका की सारी शक्तियाँ उसी के पास होती हैं ।
(defrule head14
(declare (salience 5200))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI ?id ?id1)(viSeRya-det_viSeRaNa ?id ?))
(id-root ?id1 Department|department|Government|government|state|State|media|Media|unit|Unit|federation|Federation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head14   "  ?id "  pramuKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (01-09-2014)
;As planned Kothiyal then headed back to Bangalore Airport to land .(Parallel corpus)
;पूर्व निर्धारित योजना के तहत कोइयाल बंगलूर हवाऋ अड्डें की ओर  लौटा .
;Let's head back home.
;चलिये हम वापिस घर की तरफ जाएँ .  translation from head015
;चलिये हम घर की ओर लौटे  . 
(defrule head15
(declare (salience 4350))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kI_ora_lOta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " head.clp	head15  "  ?id "  " ?id1 "  kI_ora_lOta  )" crlf))
)

;Commented by Shirisha Manju 19-12-14 Suggested by Chaitanya Sir --- becoz it is merged in head15 rule 
;It can be uncommented if same translation is needed
;@@@ Added by 14anu02 on 3.7.14
;Let's head back home.
;चलिये हम वापिस घर की तरफ जाएँ . 
;(defrule head015
;(declare (salience 4400))
;(id-root ?id head)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(id-root =(+ ?id 1) back)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kI_waraPa_jA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp      head015   "  ?id "  kI_waraPa_jA )" crlf))
;)


;$$$ Modified by 14anu-ban-06 (02-12-2014)
;@@@ Added by 14anu11
;He was the head of a monastery and had a large following , His place Sonnalige ( Sholapur of Maha - rashtra ) was a small village : 
;he built it into a big town .
;वह एक मठ का मुख्य था और उसके शिष्यों की विशाल संख्या थी . उसकी जगह सोन्नातिगे ( महाराष्ट्र का शोलापुर ) नामक एक छोटा गांव था . जिसे उसने बडा नगर बना दिया था .
(defrule head16
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-det_viSeRaNa  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))        ;spelling corrected from 'muKay' to 'muKya' by 14anu-ban-06 (02-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head16   "  ?id "  muKya )" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;Where are we heading?
;आप कहां जा रहे हैं?
(defrule head17
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA));meaning changed from 'jA_rahe' to 'jA' by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head17   "  ?id "  jA )" crlf));meaning changed from 'jA_rahe' to 'jA' by 14anu-ban-06 (09-12-2014)
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;We headed north.
;हम उत्तरी जा रहे.
(defrule head18
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1)  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA));meaning changed from 'jA_rahe' to 'jA' by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head18   "  ?id "  jA )" crlf));meaning changed from 'jA_rahe' to 'jA' by 14anu-ban-06 (09-12-2014)
)

;@@@Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;Walsh headed the ball into an empty goal. 
;वाल्श एक खाली गोल में गेंद का रूख किया.
(defrule head19
(declare (salience 5000))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUKa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head19   "  ?id "  rUKa_kara )" crlf))
)


;@@@ Added by 14anu-ban-06 (16-01-2015)
;I wish you'd use your head.(OALD)
;मैं इच्छा करता हूँ आप अपने दिमाग का उपयोग करेंगे .(manual)
;Use your head to solve this problem.(head.clp)
;इस समस्या को हल करने के लिए अपने दिमाग का उपयोग कीजिए . (manual)
(defrule head20
(declare (salience 4700))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 use)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head20   "  ?id "  ximAga )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-01-2015)
;Who's been putting such weird ideas into your head. (OALD)
;आपके दिमाग में ऐसे अद्भुत विचार  कौन उत्पन्न कर रहा है . (manual)
;Try to put the exams out of your head for tonight.(OALD)
;आज की रात के लिए परीक्षाएँ अपने दिमाग से बाहर रखने का प्रयास कीजिए .  (manual)
(defrule head21
(declare (salience 4700))
(id-root ?id head)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-of_saMbanXI ?id1 ?id)(kriyA-into_saMbanXI ?id1 ?id))
(id-root ?id1 put)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  head.clp 	head21   "  ?id "  ximAga )" crlf))
)
