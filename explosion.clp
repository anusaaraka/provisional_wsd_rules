;@@@ Added  by 14anu-ban-04 on (10-04-2015)
;An explosion in oil prices.                      [oald]
;तेल मूल्यों में  अचानक वृद्धि .                               [self]
(defrule explosion2
(declare (salience 4910))
(id-word ?id explosion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1) 
(id-root ?id1 price|cost|value) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acAnaka_vqxXi))      
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explosion.clp 	explosion2   "  ?id "   acAnaka_vqxXi )" crlf))                      
)

;$$$ Modified by 14anu-ban-04 (17-12-2014)    ---changed meaning from 'visPotiwa_honA' to 'visPota_honA'
;@@@ Added by 14anu23 on 26/6/14
; The fire was thought to have been caused by a gas explosion.
;सोचा गया था  कि आग  एक गैस विस्फोट होने से कारण  लगी.
;यह सोचा गया था  कि आग  एक गैस विस्फोट होने के कारण  लगी.                    ;translation corrected by 14anu-ban-04 on (17-12-2014)
(defrule explosion0
(declare (salience 4900))
(id-word ?id explosion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id visPota))      ;changed meaning from 'visPota_honA' to 'visPota' by 14anu-ban-04 on (10-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explosion.clp 	explosion0   "  ?id "  visPota )" crlf))                      ;changed meaning from 'visPota_honA' to 'visPota' by 14anu-ban-04 on (10-04-2015)
)

;$$$ Modified by 14anu-ban-04 (17-12-2014)    ---changed meaning  from 'gussA_uwAra' to 'gussA'
;@@@ Added by 14anu23 on 26/6/14
;She was alarmed by his violent explosion.
;वह उसके भयङ्कर गुस्सा उतारने से भयभीत हो गयी थी . 
;वह उसके भयंकर गुस्से  से भयभीत हो गयी थी .              ;translation corrected by 14anu-ban-04 
(defrule explosion1
(declare (salience 4910))           ;salience increased  by 14anu-ban-04 
(id-word ?id explosion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gussA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  explosion.clp 	explosion1   "  ?id "  gussA )" crlf))
)
