
(defrule any0
(declare (salience 5000))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
(niReXAwmaka_vAkya      )
(id-word =(+ ?id 1) ?str)
;Test for ?str is made as argument for the gdbm_lookup_p function passed should be always a string                           (if number passed as argument gdbm_lookup_p causes to Segmentation fault)
(id-word =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "uncountable.gdbm" ?str)))
=> 
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any0   "  ?id "  kuCa_BI )" crlf))
)

(defrule any1
(declare (salience 4900))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
(id-word =(+ ?id 1) ?str)
;Test for ?str is made as argument for the gdbm_lookup_p function passed should be always a string                           (if number passed as argument gdbm_lookup_p causes to Segmentation fault)
(id-word =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "uncountable.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any1   "  ?id "  kuCa )" crlf))
)

(defrule any2
(declare (salience 4800))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) without)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any2   "  ?id "  koI )" crlf))
)

(defrule any3
(declare (salience 4700))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) ?str)
;Test for ?str is made as argument for the gdbm_lookup_p function passed should be always a string                           (if number passed as argument gdbm_lookup_p causes to Segmentation fault)
(id-word =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "uncountable.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any3   "  ?id "  kuCa )" crlf))
)

(defrule any4
(declare (salience 4600))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
(niReXAwmaka_vAkya      )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any4   "  ?id "  koI_BI )" crlf))
)

(defrule any5
(declare (salience 4500))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any5   "  ?id "  koI )" crlf))
)

(defrule any6
(declare (salience 4400))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any6   "  ?id "  koI_BI )" crlf))
)

;Commented by Shirisha Manju 29-10-14 Suggested by Chaitanya Sir 
;### COUNTER EXAMPLE ### The spring is not stretched and does not exert any upward force on the object which is moving down with acceleration g due to gravity.
;### spriMga KIMcA huA nahIM hE Ora vaswu para koI urXva bala nahIM lagAwI hE jo wvaraNa jI guruwvAkarRaNa ke kAraNa se nIce kI ora cala rahI hE.###
;;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)23-jan-2014 -- added light 
;;How do we 'see' a black hole if it can not emit any light?
;;कैसे हम एक कृष्ण विवर 'देखते हैं' यदि यह कोइ  प्रकाश का  उत्सर्जन नहीं कर सकता है?
;;Modified by Meena(3.5.11)
;;The book does not make any mention of his love affair.
;;Added by Meena(21.10.09)
;;It did not resemble any living creature.
;(defrule any7
;(declare (salience 4300))
;(id-root ?id any)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id1 ?id)
;(id-word ?id1 ?wrd)
;=>
;(retract ?mng)
;(if (or(eq ?wrd mention)(eq ?wrd questions)(eq ?wrd light)) then
;        (assert (id-wsd_root_mng ?id koI))
;	(if ?*debug_flag* then
;        (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp      any7   "  ?id "  koI )" crlf))
;else
;	(assert (id-wsd_root_mng ?id kisI))
;	(if ?*debug_flag* then
;	(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp       any7   "  ?id "  kisI )" crlf))
;))
;;note: this rule needs to be modified as the condition "(viSeRya-det_viSeRaNa  ?id1 ?id)" is not sufficient.
;

;Salience reduced in "any8" by Meena(21.10.09) as it is a very general rule. 
(defrule any8
(declare (salience 1000))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any8   "  ?id "  koI )" crlf))
)

;"any","Adj","1.koI_BI"
;Can you give me any book?
;
(defrule any9
(declare (salience 4200))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_BI_sWiwi_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any9   "  ?id "  kisI_BI_sWiwi_meM )" crlf))
)

;"any","Adv","1.kisI_BI_sWiwi_meM"
;Is your mother any better?
;
(defrule any10
(declare (salience 4100))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any10   "  ?id "  kuCa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)30-jan-2014
;I doubt whether there is any educated man in Bengal, who is not grateful to him.
;मुझे संदेह है कि सारे बंगाल में शायद ही कोई ऐसा शिक्षित व्यक्ति होगा, जो उनके प्रति कृतज्ञ न हो।
(defrule any11
(declare (salience 5000))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(kriyA-aBihiwa  ?kri ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any11   "  ?id "  koI )" crlf))
)


;"any","Pron","1.kuCa/kiMciwa"
;Is there any water in the jug?
;
;LEVEL 
;Headword : any
;
;Examples --
;
;'any' Sabxa aMgrejZI meM kaI prakAra se prayoga howA hE.
;isake kuCa sAmAnya prayoga -
;
;--"1.koI"
;I have not read any book written by Prasad.
;mEMne prasAxa xvArA raciwa koI kiwAba nahIM paDZI hE.
;--"2.kisI"
;He did not invite any of his class-mates.
;usane apane kisI BI miwra ko Amanwriwa nahIM kiyA.
;We reached Lucknow without any problems.
;hama binA kisI pareSAnI ke laKanaU pahuzca gaye.
;--"3.kuCa"
;I need some money. Do you have any.
;muJe kuCa pEse cAhiez. wumhAre pAsa kuCa hEM.
;--"4.koI_BI"
;Take any saree.
;koI BI sAdZI le lo.
;--"5.koI_BI_jo"
;Take any saree you like.
;koI BI sAdZI jo wumheM pasanxa ho le lo.
;--"6.[kuCa]_Ora"
;I can't write any faster.
;mEM Ora wejZa nahIM liKa sakawA.
;
;sUwra : kaCa^koI_BI
;
;kuCa viSeRa prayoga --
;
;praSnavAcaka vAkyoM meM Ora nakArAwmaka vAkyoM meM 'some' ke sWAna para
;
;Do you have some sugar ?
;Do you have any sugar ?
;I hae some sugar.
;I don't have any sugar.
;*I have any sugar.
;


;@@@ Added by 14anu-ban-02 (07-11-2014)
;The method of dimensions can only test the dimensional validity, but not the exact relationship between physical quantities in any equation.[ncert]
;विमीय विधि द्वारा किसी भी समीकरण की केवल विमीय वैधता ही जाञ्ची जा सकती है, किसी समीकरण में विभिन्न भौतिक राशियों के बीच यथार्थ सम्बन्ध नहीं जाञ्चे जा सकते.[ncert]
(defrule any12
(declare (salience 5000))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ? ?id1)
(viSeRya-det_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_BI))	;meaning changed from kisI to kisI_BI by 14anu-ban-02(31-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any12   "  ?id "  kisI_BI )" crlf))
)	;meaning changed from kisI to kisI_BI by 14anu-ban-02(31-01-2015)

;commented by 14anu-ban-02(08-01-2015)
;meaning is coming from any8
;@@@ Added by 14anu17
;If you have any concerns about your NHS services.
;(defrule any13
;(declare (salience 4301))
;(id-root ?id any)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id1 ?id)
;(id-cat_coarse ?id determiner)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id koI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " any.clp   any13   "   ?id " koI )" crlf))
;)

;$$$Modified by 14anu-ban-02(08-01-2015)
;@@@ Added by 14anu17
;People of any age can carry these germs for weeks or months without becoming ill .
;किसी भी उम्र के लोगों के बीमार हुए  बिना हफ्तों या महीनों के लिए इन कीटाणुओं ले जा सकता है.
(defrule any14
(declare (salience 4701))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)  ;modified by 14anu-ban-02(08-01-2015)
(viSeRya-det_viSeRaNa  ?id2 ?id)  ;added by 14anu-ban-02(08-01-2015)
(id-root ?id1 person)   ;added by 14anu-ban-02(08-01-2015)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_BI))  ;meaning changed from 'koI' to 'kisI_BI' by 14anu-ban-02(08-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any14   "  ?id "  kisI_BI )" crlf))
)

;@@@ Added By 14anu17
;you can get the immunisation at any time .
;तो आप कोई समय में प्रतिरक्षण प्राप्त कर सकते हैं . 
(defrule any15
(declare (salience 4700))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any15   "  ?id "  koI )" crlf))
)

;@@@ Added by 14anu-ban-02(10-12-2014)
;He is by any account an honest man.[mnit from account 012]
;वो हर पैमाने पर एक ईमांदार व्यक्ति है.[mnit from account 012]
(defrule any16
(declare (salience 4700))
(id-root ?id any)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) account|means)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  any.clp 	any16   "  ?id "  hara )" crlf))
)
