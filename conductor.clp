;@@@ Added by 14anu-ban-03 (25-02-2015)
;The bus conductor collects the fare. [hinkhoj]
;बस चालक किराया लेता है .  [self]
(defrule conductor0
(declare (salience 00))
(id-root ?id conductor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAlaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conductor.clp 	conductor0   "  ?id "  cAlaka )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-02-2015)
;We shall assume one kind of mobile carriers as in a conductor (here electrons).  [NCERT]
;We shall assume one kind of mobile carriers as in a metal conductor (here electrons are mobile carriers).  [self]
;हम किसी चालक (जिसमें इलेक्ट्रॉन गतिशील वाहक हैं) की भाँति एक ही प्रकार के गतिशील वाहक मानेंगे.  [NCERT]
;हम किसी धातु सुचालक(जिसमें इलेक्ट्रॉन गतिशील वाहक हैं) की भाँति एक ही प्रकार के गतिशील वाहक मानेंगे.  [self]
(defrule conductor1
(declare (salience 10))
(id-root ?id conductor)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 metal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sucAlaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conductor.clp 	conductor1   "  ?id "  sucAlaka )" crlf))
)
