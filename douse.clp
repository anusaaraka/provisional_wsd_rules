;@@@ Added by 14anu-ban-04 (24-03-2015)
;He doused the flames with a fire extinguisher.        [oald]
;उसने अग्निशामक से आग को बुझाया .                              [self]
(defrule douse1
(declare (salience 20))
(id-root ?id douse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 flame|light|fire|candle)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id buJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  douse.clp     douse1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  douse.clp    douse1  "  ?id "  buJA)" crlf))
)


;@@@ Added by 14anu-ban-04 (24-03-2015)
;The horses are doused with buckets of cold water.              [oald]
;घोड़ों को ठण्डे पानी की बाल्टियों से भिगोया जाता है .                          [self]
(defrule douse2
(declare (salience 20))
(id-root ?id douse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive) 
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id Bigo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  douse.clp     douse2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  douse.clp    douse2 "  ?id "  Bigo)" crlf))
)

;--------------------------------------------------DEFAULT RULE ----------------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (24-03-2015)
;He doused himself in kerosene oil and lighted fire to commit suicide.                  [hinkhoj]
;आत्महत्या करने के लिए उसने स्वयं को  मिट्टी के तेल में भिगोया और  आग लगाई   .                                   [self]
(defrule douse0
(declare (salience 10))
(id-root ?id douse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bigo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  douse.clp    douse0 "  ?id "  Bigo)" crlf))
)
