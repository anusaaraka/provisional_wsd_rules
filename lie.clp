
;Added by human
(defrule lie0
(declare (salience 5000))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 man)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id letA_WA))
(assert (id-H_vib_mng ?id 0))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie0   "  ?id "  letA_WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  lie.clp       lie0   "  ?id "  0 )" crlf)
))

;The beds in which the two men lay vibrated


(defrule lie1
(declare (salience 4900))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 egg)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(assert (id-H_vib_mng ?id 0))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie1   "  ?id "  xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  lie.clp       lie1   "  ?id " 0 )" crlf))
)

;They lay eggs.

;$$$ Modified by 14anu-ban-08 (13-01-2015)     ;changed meaning from 'letakara' to 'leta'
;$$$ Modified by 14anu13 on 18-06-14
;The blood pressure will have to be recorded , especially if the patient is on medication , in the lying , sitting and standing positions . 
;रक्तचाप दर्ज किया जायेगा ,लेटकर ,बैठे हुए ,खडे हुए , मुख्यत: अगर मरीज दवाइयो पर है |
(defrule lie2
(declare (salience 4800))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) still|positions|position|sitting|standing) ; added 'positions|position|sitting|standing' by 14anu13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leta))  ;changed meaning from letA_WA to letakara    ;changed meaning from 'letakara' to 'leta' by 14anu-ban-08 (13-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie2   "  ?id "  leta )" crlf))                                 ;changed meaning from 'letakara' to 'leta' by 14anu-ban-08 (13-01-2015)
)

;particle_down_-	leta_{0/ed/en}	0



(defrule lie3
(declare (salience 4700))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id lay )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nOsiKiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  lie.clp  	lie3   "  ?id "  nOsiKiyA )" crlf))
)

;"lay","Adj","1.nOsiKiyA"
;A lay musician.
;
;


(defrule lie4
(declare (salience 4600))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ArAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	lie4  "  ?id "  " ?id1 "  ArAma_kara  )" crlf))
)

;You've worked a lot,I think you should lie back now.
;wuma bahuwa kAma kara cuke ho ,aba wumheM ArAma karanA cAhie




(defrule lie5
(declare (salience 4500))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 so))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	lie5  "  ?id "  " ?id1 "  so  )" crlf))
)

;I like to lie in on holidays.
;muJe avakASa meM sonA pasaMxa hE

;$$$ Modified by 14anu-ban-06  (25-08-2014)
; The decision lies with you to do it or not.
;ise karane yA na karane kA PZEsalA wuma para nirBara hE.
(defrule lie6
(declare (salience 4400))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 with); commented by 14anu-ban-06  (25-08-2014)
;(kriyA-upasarga ?id ?id1)); commented by 14anu-ban-06  (25-08-2014)
;(kriyA-object ?id ?)); commented by 14anu-ban-06  (25-08-2014))
(kriyA-subject ?id ?id1);added by 14anu-ban-06
(id-root ?id1 decision);added by 14anu-ban-06
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nirBara_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	lie6  "  ?id "  " ?id1 "  nirBara_ho  )" crlf))
)

; The decision lies with you to do it or not..
;ise karane yA na karane kA PZEsalA wuma para nirBara karawA hE

(defrule lie7
(declare (salience 4300))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 so))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	lie7  "  ?id "  " ?id1 "  so  )" crlf))
)

(defrule lie8
(declare (salience 4100))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) on )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie8   "  ?id "  leta )" crlf))
)



(defrule lie9
(declare (salience 4000))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id lay )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aviSeRajFa-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  lie.clp  	lie9   "  ?id "  aviSeRajFa- )" crlf))
)

(defrule lie10
(declare (salience 3900))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie10   "  ?id "  JUTa )" crlf))
)

;"lie","N","1.JUTa"
;Whatever he has said is a pack of lies.
;

;$$$Modified by 14anu-ban-08 (04-02-2015)    ;constraint added
;A coil of hair was lying on the floor. [hinkhoj]
;बालो का गुच्छा फर्श पर पड़ा हुआ था .  [self]
;$$$ Modified by 14anu-ban-06   (25-08-2014)
;Added by Meena(12.5.11)
;She always leaves her clothes lying about on the floor.
;vaha hameSA ParSa para pade hue apane vaswroM ko Coda jAwI hE. (tanslation added by  14anu-ban-06)
(defrule lie11
(declare (salience 4200))     ;salience increased by 14anu-ban-08 (04-02-2015)
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id lying);commented by 14anu-ban-06
;(kriyA-kqxanwa_karma  ?id1 ?id);commented by 14anu-ban-06
(kriyA-on_saMbanXI ?id ?id1);added by 14anu-ban-06
;(id-root ?id1 floor|ground)       ;added by 14anu-ban-08 (04-02-2015)  ;'ground' added by 14anu-ban-08 (06-02-2015)               ;commented by 14anu-ban-08 (12-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id padA_ho));meaning changed from 'pade_hue' to 'padA_ho' by 14anu-ban-06 (29-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie11   "  ?id "  padA_ho )" crlf))
)


;Salience reduced by Meena(12.5.11)
;$$$ Modified by 14anu07 on 24/06/2014
(defrule lie12
(declare (salience 500));salience changed from 3800 to 500 by 14anu7
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUTa_bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie12   "  ?id "  JUTa_bola )" crlf))
)


;@@@Added by 14anu07 on 24/06/2014
(defrule lie21
(declare (salience 1000))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) at)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp 	lie21   "  ?id "  sWiwa_ho)" crlf))
)

;$$$Modified by 14anu-ban-08 (17-01-2015)     ;modified translation, modify meaning
;@@@ Added by Nandini (4-12-13)
;This is the Pole Star that lies towards the north.
;यह ध्रुव तारा है, जो उत्तर दिशा में दिखाई देता है।    (before modification)
;यह ध्रुव तारा है, जो उत्तर दिशा में स्थित है।      (after modification)
;$$$ Modified by 14anu07 on 24/06/2014
(defrule lie13
(declare (salience 5500))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-towards_saMbanXI  ?id ?id1)
(id-word ?id1 north|south|east|west)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))      ;modify meaning'sWiwa_ho' to 'sWiwa' by 14anu-ban-08 (17-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  lie.clp  	lie13   "  ?id "  sWiwa )" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-08-2014) meaning changed from lahUluhAna_padA_ho to padA_ho 
;We were mesmerized by seeing hundreds of hippopotamuses lying in the Manyara lake. (Parallel Corpus)
;मनयारा झील में पड़े हुए सैकड़ों दरियाई घोड़ों को देखकर हम मंत्रमुग्ध हो गए ।
;@@@ Added by Nandini (12-12-13)
;The body was lying in a pool of blood. (via mail)
;SarIra KUna ke wAla meM lahUluhAna_padA_huA WA.
;SarIra KUna ke wAla meM padA huA WA.;translation added by 14anu-ban-06 (29-10-2014) 
(defrule lie14
(declare (salience 4250));salience increased from 3850 to 4250
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
;(id-word ?id1 body)
;(kriyA-subject  ?id ?id1)
;(id-word ?id lying)
(id-root ?id1 pool|lake);added by 14anu-ban-06 (29-10-2014) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id padA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie14   "  ?id "  padA_ho )" crlf))
)

;$$$ Modified by 14anu-ban-06  (25-08-2014)
;@@@ Added by Nandini (12-12-13)
;I spent most of my holiday lying sunbathing by the pool. (via mail)
;mEMne apanI aXika CuttiyAZ wErane ke wAlAba ke kinAre letakara XUpa-snAna karawe hue  biwAyI.
(defrule lie15
(declare (salience 3860))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-kqxanwa_karma  ?id ?id1);commented by 14anu-ban-06
(viSeRya-kqxanwa_viSeRaNa ?id ?id1);added by 14anu-ban-06
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id letakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie15   "  ?id "  letakara )" crlf))
)

;@@@ Added By 14anu-ban-08 on (19-09-2014)
;Let us consider two vectors A and B that lie in a plane as shown in Fig. 4.4(a).    [NCERT]
;हम चित्र 4.4(a) में दर्शाए अनुसार किसी समतल में स्थित दो सदिशों A तथा B पर विचार करते हैं.
(defrule lie16
(declare (salience 4301))     ;salience increased from 4200 to 4301 by 14anu-ban-08 (28-02-2015)
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)        ;added 'id1' by 14anu-ban-08 (28-02-2015)
(id-root ?id1 plane)                ;added by 14anu-ban-08 (28-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie16   "  ?id "  sWiwa )" crlf))
)

;@@@ Added By 14anu-ban-08 on (19-09-2014)
;The length of the vector r represents the magnitude of the vector and its direction is the direction in which P lies as seen from O.  [NCERT]
;सदिश r की लम्बाई उसके परिमाण को निरूपित करती है तथा सदिश की दिशा वह होगी जिसके अनुदिश p (बिन्दु O से देखने पर) स्थित होगा.
(defrule lie17
(declare (salience 4202))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 p)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie17   "  ?id "  sWiwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (25-10-2014)
;Sometimes the modifications may not be drastic and may lie within the framework of existing theory. [NCERT CORPUS]
;kaBI - kaBI ye saMSoXana prabala na hokara supracaliwa sixXAnwoM ke DAFce meM BI ho sakawe hEM. [NCERT CORPUS]

(defrule lie18
(declare (salience 5600))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-within_saMbanXI  ?id ?id1)
(id-root ?id1 framework)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie18   "  ?id "  ho )" crlf))
)

;@@@ Added by 14anu-ban-06 21-07-2014 
;We were surprised to see the skulls,horns and skeletons lying at several places. (Parallel Corpus)
;kaI jagaha viXamAna KopfiyaM sIMga Ora kaMkAlo ko xeKakara vismaya huA.
(defrule lie19
(declare (salience 3950))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " lie.clp	lie19  "  ?id "    viXamAna )" crlf))
)


;@@@ Added By 14anu-ban-08 (15-11-2014)
;What lies behind the phenomenal progress of physics in the last few centuries?     [NCERT]
;पिछली कुछ शताब्दियों में भौतिकी में हुई असाधारण प्रगति का क्या रहस्य है?     [NCERT]
(defrule lie20
(declare (salience 5500))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) behind)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) piCle))   ;modify printout statement by removing equal sign by 14anu-ban-08 (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	 lie20  "  ?id "  " (+ ?id 1) " piCle )" crlf))   ;modify printout statement by removing equal sign by 14anu-ban-08 (28-02-2015)
)

;@@@ Added by 14anu09 Vasu Vardhan(MNNIT ALLAHABAD) on 29.05.2014 email-id:vasuvardhan1995@gmail.com
;Happiness lies in helping others.
;खुशी अन्य की सहायता करने में रहती है.

(defrule lie016
(declare (salience 4300))           ;Salience is increased from 4200 to 4300 by 14anu-ban-08 (13-01-2015)
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 in)
;(test (> ?id1 ?id))
(id-word =(+ ?id 1) in )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp     lie016   "  ?id "  raha )" crlf)
)
)

;@@@Added by 14anu-ban-08 (04-02-2015)
;There was so much of sand deposited here that no one even expected what is lying beneath .  [Tourism]
;यहाँ  इतनी  रेत  जमा  थी  कि  किसी  को  इस  बात  का  आभास  ही  नहीं  था  कि  नीचे  क्या है  ।   [Tourism]
;यहाँ  इतनी  रेत  जमा  थी  कि  किसी  को  इस  बात  का  आभास  ही  नहीं  था  कि  नीचे  क्या पड़ा है  ।   [self]
(defrule lie22
(declare (salience 5500))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) beneath)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) nIce))   ;modify statement by removing equal sign by 14anu-ban-08 (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lie.clp	 lie22  "  ?id "  " (+ ?id 1) " nIce )" crlf))    ;modify printout statement by removing equal sign by 14anu-ban-08 (28-02-2015)
)

;@@@Added by 14anu-ban-08 (28-02-2015)
;Terrestrial phenomena lie somewhere in the middle of this range.  [NCERT]
;पार्थिव परिघटनाएँ इस परिसर के मध्य में कहीं होती हैं.  [NCERT]
(defrule lie23
(declare (salience 5600))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 phenomenon)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie23   "  ?id "  ho )" crlf))
)

;@@@Added by 14anu-ban-08 (03-03-2015)
;The circle lies in a plane perpendicular to the axis.  [NCERT]
;यह वृत्त अक्ष के लम्बवत् तल में अवस्थित है.  [NCERT]
(defrule lie24
(declare (salience 5600))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 circle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie24   "  ?id "  avasWiwa )" crlf))
)

;@@@Added by 14anu-ban-08 (26-03-2015)
;The town lies on the coast.  [oald]
;शहर समुद्र-तट पर स्थित हैं.  [self]
(defrule lie25
(declare (salience 4202))
(id-root ?id lie)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 coast)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lie.clp       lie25   "  ?id "  sWiwa )" crlf))
)


;"lie","V","1.JUTa_bolanA"
;He lied to me about his experience.
;--"2.Bramiwa_karanA"
;The mirror does not lie.
;--"V","1.letanA"
;Lie down on the bed.
;--"2.pade_rahanA"
;A thick sheet of snow is lying all around.
;--"3.sWiwa_honA"
;The whole future lies before you. I know where my interest lies.
;--"4.sWiwi_meM_honA"
;He lies fourth in the merit list.
;
;LEVEL 
;
;
;Headword : lie
;
;Examples --
;V
;1.The corpse was lying, unattended, on the road.
;Sava rAswe para binA raKavAlI ke padZA huA WA
;
;2.His books always lie open.
;usakI kiwAbeM hameSA KulI padZI howI hE
;
;3.The snow is lying thick on the ground.
;jZamIna para GanA barPa padZA huA hE
;
;4.You are young && your whole life lies ahead of you.
;wuma javAna ho Ora wumhArI sArI jZinxagI wumhAre sAmane padZI hE
;
;5.That village lies on the Southern coast.
;vaha gAzva xakRiNI samuxra wata para sWiwa hE
;
;6.Mohan lies in the second place.
;mohana xUsare sWAna para hE    <--sWAna_para_honA<--sWiwa_honA<--sWiwi_meM_honA
;
;7.The valley lay before us.
;hamAre sAmane GAtI WI   < -- honA <--sWiwa_honA<--kI_sWiwi_meM_honA
;
;Upara xie gae vAkyoM se "lie" kriyA kA "padZA_huA_honA" EsA arWa prApwa kara sakawe 
;hEM. isa arWa ko hama Ora BI uciwa isa prakAra banA sakawe hE "[padZI_huI]sWiwi_meM honA"
;lekina pAzcave, Cate Ora sAwave vAkyo ke sanxarBoM meM yaha arWa nahIM bETa rahA hE. isalie ina sanxarBoM meM hama arWa isa prakAra spaRta kara sakawe hEM : 
;
;"[padZA_huA]_sWiwi_meM_honA"
;
;Upara liKiwa vAkyoM meM "lie" kriyA kA jo arWa A rahA hE, use hama viswqwa karake
;"lie" saMjFA kA arWa BI xe sakawe hEM jEse:
;
;N
;He found his ball in a difficult lie.
;usane apanI geMxa ko kaTina sWiwi meM padZA huA pAyA
;They will investigate the lie of the land before making any decisions.
;ve koI BI nirNaya lene se pahale sWala kI sWiwi meM honA jAzceMge
;
;
;'lie' ke kuCa anya uxAharaNa ;
;
;V
;Ravi lied to Mohan when he told him that he was not present in the room.
;ravi ne mohana se 'JUTa_bolA' jaba usane mohana se kahA ki vaha kamare meM nahIM WA.
;
;N
;It was clear from his face that he was speaking a lie.
;usake cehare se yaha sAPa WA ki vaha 'JUTa' bola rahA WA.
;
;awaH 'lie' kA eka anya arWa 'JUTa' BI howA hE. 'lie' kA sUwra hogA ;
;
;sUwra : JUTa/[[sAmane]padZA_huA]_sWiwi_meM_honA
;
;
;
