;@@@ Added by 14anu-ban-06 (21-01-2015)
;There's heaps of time before the plane leaves.(OALD)
;विमान चलने से पहले ढेर सारा समय है . (manual)
(defrule heap2
(declare (salience 5100))
(id-root ?id heap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) of)
(id-root ?id1 time)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  DZera_sArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " heap.clp	 heap2  "  ?id "  " (+ ?id 1)  " DZera_sArA  )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;He deals well with all the criticism heaped on him.(cambridge)[parser no-8]
;उसने उसकी हुई सभी आलोचना का अच्छा से उत्तर दिया है . (manual)
(defrule heap3
(declare (salience 5300))
(id-root ?id heap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DZera_sArI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " heap.clp	heap3  "  ?id "  " ?id1 "  DZera_sArI_ho  )" crlf))
)
;------------------------------- Default Rules ----------------

(defrule heap0
(declare (salience 5000))
(id-root ?id heap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DZera))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heap.clp 	heap0   "  ?id "  DZera )" crlf))
)

;"heap","N","1.DZera"
;There is a heap of garbage near the house . 
;Gara ke pAsa kUde kA DZera padZA hE.
;
(defrule heap1
(declare (salience 4900))
(id-root ?id heap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dera_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heap.clp 	heap1   "  ?id "  Dera_lagA )" crlf))
)

;"heap","V","1.Dera_lagAnA"
;The farmer heaped the crop in the field .
;kisAna ne Kewa meM Pasala kA DZera lagA xiyA .
;
