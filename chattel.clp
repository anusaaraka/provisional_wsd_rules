;@@@ Added by 14anu-ban-03 (18-02-2015)
;Goods and chattels. [cald]   ;working on parser no.-2
;माल और चल-सम्पत्ति हैं . [manual]
(defrule chattel0
(declare (salience 00))
(id-root ?id chattel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala-sampawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chattel.clp 	chattel0   "  ?id "  cala-sampawwi )" crlf))
)


;@@@ Added by 14anu-ban-03 (18-02-2015)
;Slaves treated as chattel. [merriam-webster]
;सेवको से गुलाम की तरह व्यवहार किया गया. [manual]
(defrule chattel1
(declare (salience 100))
(id-root ?id chattel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id1 ?id)
(id-root ?id1 treat)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gulAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chattel.clp 	chattel1   "  ?id "  gulAma )" crlf))
)
