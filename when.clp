
(defrule when0
(declare (salience 5000))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(or (praSnAwmaka_vAkya      )(wh_question));Suggested by Sukhada(13-10-10)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when0   "  ?id "  kaba )" crlf))
)

(defrule when1
(declare (salience 4900))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) tell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when1   "  ?id "  kaba )" crlf))
)

(defrule when2
(declare (salience 4800))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when2   "  ?id "  kaba )" crlf))
)

;I did not know when to go.
(defrule when3
(declare (salience 4700))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when3   "  ?id "  kaba )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (09-12-2014)
;$$$ Modified by 14anu07 on 21/06/2014
(defrule when4
(declare (salience 4300))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id conjunction);commeted by 14anu-ban-11 on (09-12-2014)
(id-cat_coarse ?id wh-adverb);Added by 14anu-ban-11 on (09-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when4   "  ?id "  kaba )" crlf))
)

;"when","Conj","1.jaba"
;Come to our place when you feel like it.


;$$$ Modified by 14anu07 on 21/06/2014
(defrule when5
(declare (salience 4500))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id wh_adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when5   "  ?id "  jaba )" crlf))
)

;"when","Interro","1.kaba"
;When did you go there?.
;
;$$$ Removed category 'relative_pronoun' and added 'viSeRya-jo_samAnAXikaraNa' relation by Roja(28-12-13). Suggested by Sukada.
(defrule when6
(declare (salience 4400))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-jo_samAnAXikaraNa ? ?id)
;(id-cat_coarse ?id relative_pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when6   "  ?id "  jaba )" crlf))
)

;"when","Rel Pron","1.jaba/jisa_samaya"
;Sunday is the day when I take rest.
;
(defrule when7
(declare (salience 4300))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when7   "  ?id "  jaba )" crlf))
)



;@@@ Added by 14anu-ban-11 on (21-01-2015)
;We didn't come to know when two hours passed by while watching all the animals in this zoo. (tourism)
;इस जू में तमाम प्राणियों को देखते-देखते दो घंटे कब बीत गए ,पता ही नहीं चला ।
(defrule when8
(declare (salience 4500))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id wh-adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-vAkyakarma  ?id2 ?id1)
(kriyA-kriyArWa_kriyA  ?id3 ?id2)
(root-verbchunk-tam-chunkids ? ? ? $? ?aux $? ?id3)
(id-root ?aux do)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when8   "  ?id "  kaba )" crlf))
)



;@@@ Added by 14anu-ban-11 on (21-01-2015)
;However when and how it started , no body knows it for sure .(tourism)
;हालाँकि  यह  कब  व  कैसे  शुरू  हुई ,यह  कोई  ठीक-ठीक  नहीं  जानता  । 
(defrule when9
(declare (salience 4400))
(id-root ?id when)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id wh-adverb)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 how)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  when.clp 	when9   "  ?id "  kaba )" crlf))
)



