;@@@ Added by 14anu-ban-06 (22-04-2015)
;The style is imitative of Basque architecture.(OALD) [parser no.- 2]
;बनावट बैस्क वास्तुकला की नकल है . (manual)
(defrule imitative1
(declare (salience 2000))
(id-root ?id imitative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitative.clp 	imitative1   "  ?id "  nakala )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (22-04-2015)
;He's an imitative artist, with very little originality in his work. (cambridge)
;वह अपने कार्य में अत्यन्त कम मौलिकता से दूसरो की नकल करने वाला कलाकार है . (manual)
(defrule imitative2
(declare (salience 2200))
(id-root ?id imitative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 artist)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsaro_kI_nakala_karane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitative.clp 	imitative2   "  ?id "  xUsaro_kI_nakala_karane_vAlA )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (22-04-2015)
;His work has been criticized for being imitative and shallow. (OALD)
;उसके कार्य की नकली और ओछा होने के कारण आलोचना की गयी है . (manual)
(defrule imitative0
(declare (salience 0))
(id-root ?id imitative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imitative.clp 	imitative0   "  ?id "  nakalI )" crlf))
)
