;@@@Added by 14anu-ban-02(02-03-2015)
;Sentence: One of the country’s most eligible bachelors.[oald]
;Translation:  देश के सबसे अधिक योग्य कुँवारा पुरुषों में से एक . [self]
(defrule bachelor0 
(declare (salience 0)) 
(id-root ?id bachelor) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kuzvArA_puruRa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bachelor.clp  bachelor0  "  ?id "  kuzvArA_puruRa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(02-03-2015)
;This experiment was later performed around 1911 by Hans Geiger (1882 — 1945) and Ernst Marsden (1889 — 1970, who was 20 year-old student and had not yet earned his bachelor's degree).[ncert 12_12]
;यह प्रयोग कुछ समय पश्चात सन् 1911 में हैंस गाइगर (1882 – 1945) तथा अर्नेस्ट मार्सडन (1889 – 1970, जो 20 वर्षीय छात्र थे तथा जिन्होंने अभी स्नातक की उपाधि भी ग्रहण नहीं की थी) ने किया.[ncert]
(defrule bachelor1 
(declare (salience 100)) 
(id-root ?id bachelor) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id)
(id-root ?id1 degree)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id snAwaka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bachelor.clp  bachelor1  "  ?id "  snAwaka )" crlf)) 
) 
