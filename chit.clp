;@@@ Added by 14anu-ban-03 (16-02-2015)
;I had take down his address in a chit. [hinkhoj]
;मैंने उसका पता पर्ची में लिख दिया था. [manual]
(defrule chit0
(declare (salience 00))
(id-root ?id chit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parcI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chit.clp 	chit0   "  ?id "  parcI )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-02-2015)
;She's a saucy chit. [shabdkosh]
;वह एक तेज छोटी लडकी है . [manual]
(defrule chit1
(declare (salience 100))
(id-root ?id chit)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  CotI_ladakI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chit.clp 	chit1   "  ?id "  CotI_ladakI )" crlf))
)
