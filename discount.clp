;@@@ Added by 14anu-ban-04 (17-04-2015)
;We cannot discount the possibility of further strikes.             [oald]
;हम और अधिक स्ट्राइक की सम्भावना को खारिज नहीं कर सकते हैं .                       [self]
(defrule discount2
(declare (salience 4920))
(id-root ?id discount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 possibility)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))    
(assert (id-wsd_root_mng ?id KArija_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  discount.clp     discount2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discount.clp 	discount2   "  ?id "  KArija_kara)" crlf))
)

;@@@ Added by 14anu-ban-04 (17-04-2015)
;The government discounted the air fares.              [same clp file]
;सरकार ने हवाई यात्रा के  किराये पर छूट दी .                          [self]
(defrule discount3
(declare (salience 4910))
(id-root ?id discount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUta_xe))
(assert (kriyA_id-object_viBakwi ?id para))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  discount.clp     discount3   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discount.clp 	discount3  "  ?id "  CUta_xe )" crlf))
)

;------------------------------- Default Rules ----------------------------
(defrule discount0
(declare (salience 5000))
(id-root ?id discount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discount.clp 	discount0   "  ?id "  CUta )" crlf))
)

;"discount","N","1.CUta"
;
(defrule discount1
(declare (salience 4900))
(id-root ?id discount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUta_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discount.clp 	discount1   "  ?id "  CUta_xe )" crlf))
)

;"discount","VT","1.CUta_xenA"
;The government discounted the air fares.
;
