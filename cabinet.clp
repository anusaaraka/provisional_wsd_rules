;@@@ Added by Preeti(31-07-2014)
;He is a member of the President's Cabinet. [Oxford Advanced Learner's Dictionary]
;vaha rARtapawi/aXyakRa ke manwrImaNdala kA saxasya hE.
(defrule cabinet0
(id-root ?id cabinet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMwrImaMdala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cabinet.clp 	cabinet0   "  ?id "  maMwrImaMdala)" crlf))
)

;@@@ Added by Preeti(31-07-2014)
;She left the bowl on the cabinet and settled into the chair. [yourdictionary.com]
;usane alamArI para katorI raKI gaI Ora kursI para jAkara bETa gayI.
(defrule cabinet1
(declare (salience 1000))
(id-root ?id cabinet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-from_saMbanXI  ?id1 ?id)(kriyA-on_saMbanXI  ?id1 ?id)(kriyA-in_saMbanXI  ?id1 ?id))
(or(kriyA-object  ?id1 ?id2)(kriyA-karma  ?id1 ?id2))
(id-root ?id2 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alamArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cabinet.clp 	cabinet1   "  ?id "  alamArI )" crlf))
)
