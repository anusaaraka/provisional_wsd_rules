;@@@ Added by 14anu-ban-10 on (19-02-2015)
;She likes rasberry jam.[hinkhoj]   ;parser problem in raspberry.
;वह रसभरी  जैम  पसंद करती है।[manual]
(defrule raspberry0
(declare (salience 100))
(id-root ?id raspberry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasaBarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raspberry.clp 	raspberry0   "  ?id "  rasaBarI )" crlf))
)
;@@@ Added by 14anu-ban-10 on (19-02-2015)
;The speaker got a raspberry as he turned his back.[hinkhoj]
;स्पीकर को प्राप्त हुइ उपहास मे जीभ और होंठों से निकाली गयी आवाज  जेसे हि उसने पीठ बदली .[manual]
(defrule raspberry1
(declare (salience 200))
(id-root ?id raspberry)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 speaker)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upahAsa_me_jIBa_Ora_hoTo_se_nikAlI_gayI_AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raspberry.clp 	raspberry1   "  ?id "  upahAsa_me_jIBa_Ora_hoTo_se_nikAlI_gaye_AvAja )" crlf))
)

