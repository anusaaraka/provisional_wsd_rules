;@@@Added by 14anu-ban-02(18-02-2015)
;Sentence: She betrayed his trust over and over again.[oald]
;Translation:उसने बारम्बार उसके विश्वास का  विश्वासघात किया.[self]
(defrule betray0 
(declare (salience 0)) 
(id-root ?id betray) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id viSvAsaGAwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  betray.clp  betray0  "  ?id "  viSvAsaGAwa_kara )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-02-2015)
;It dawned on Ravi that Rita had betrayed him.[dawn.clp]
;रवि को यह समझ में आ गया था कि रीता ने उसको धोखा दिया था. [self] 
(defrule betray2 
(declare (salience 100)) 
(id-root ?id betray) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id XoKA_xe)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  betray.clp  betray2  "  ?id " XoKA_xe )" crlf)) 
) 
