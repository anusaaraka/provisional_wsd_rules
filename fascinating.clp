;@@@ Added by 14anu-ban-05 on (29-01-2015)
;Italy passing through the bumpy stairs of rise - fall even today stands with a fascinating form before the world gathering within itself the ancient civilization and culture of Europe .		[tourism]
;uwWAnapawana kI UzcI-nIcI sIDZiyoM se gujarawA italI Aja BI yUropa kI prAcInawama saByawA Ora saMskqwi ko apane meM samete xuniyA ke sAmane AkarRaka rUpa lie KadZA hE .				[tourism]
;Due to the influence of various civilizations and cultures a fascinating confluence of Greek , Roman , Spanish , African and Arabic can be seen in Sicily 's architechture and lifestyle .		[tourism]
;aneka saByawA Ora saMskqwiyoM ke praBAva ke calawe sisalI kI sWApawyakalA Ora jIvanaSElI meM yUnAnI , romana , spenI , aPrIkI Ora arebiyA kA AkarRaka saMgama xeKA jA sakawA hE .			[tourism]

(defrule fascinating1
(declare (salience 101))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 statue|form|confluence)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp 	fascinating1   "  ?id "  AkarRaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-01-2015)
;The heart lifted up on having seen a fascinating flock of birds of Tsomoriri with binoculars .		[tourism]
;xUrabIna se so-morIrI ke luBAvane pakRiyoM ke JuMda ko xeKa kara mana JUma uTA .			[tourism]

(defrule fascinating2
(declare (salience 101))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 flock)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luBAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp 	fascinating2   "  ?id "  luBAvanA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-01-2015)
;A fascinating waterfall forms with the water falling from a height of 1500 metres along with a fierce roar in Kanjar .	[tourism]
;kAMjAra meM 1500 mItara kI UzcAI se Gora garjanA ke sAWa girawe jala se eka manoharI jalaprapAwa banawA hE . 		[tourism]

(defrule fascinating3
(declare (salience 101))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-subject  ?id2 ?id1)
(id-root ?id2 form)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manohara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp 	fascinating3   "  ?id "  manohara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-01-2015)
;Attukal presents a fascinating scene with waterfall , curvy hills . [tourism]
;jalaprapAwa , UzcI-nIMcI pahAdZiyoM se yukwa Attukala manorama xqSya upasWiwa karawA hE . [tourism]

(defrule fascinating4
(declare (salience 101))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 scene)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manorama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp 	fascinating4   "  ?id "  manorama )" crlf))
)


;@@@ Added by 14anu-ban-05 on (29-01-2015)
;It's fascinating to see how different people approach the problem.		[oald]
;यह देखना दिलचस्प होगा कि कैसे विभिन्न लोग समस्या को सुलझाते हैं . 				[manual]

(defrule fascinating5
(declare (salience 102))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ? ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(id-root ?id1 see)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilacaspa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp 	fascinating5   "  ?id "  xilacaspa_ho )" crlf))
)

;------------------------- Default Rules ------------------------

;@@@ Added by 14anu-ban-05 on (29-01-2015)
;The climate here is fascinating and charming .         [tourism]
;yahAz kI jalavAyu manamohaka evaM suhAvanI hE .        [manual]

(defrule fascinating0
(declare (salience 100))
(id-root ?id fascinating)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manamohaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fascinating.clp      fascinating0   "  ?id "  manamohaka )" crlf))
)

