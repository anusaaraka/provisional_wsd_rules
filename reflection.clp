;@@@ Added by Anita-05-07-2014
;The increase in crime is a sad reflection on(= shows something bad about) our society today.
;आज अपराध में वृद्धि समाज की दुःखद झलक है ।
(defrule reflection1
(declare (salience 1000))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-on_saMbanXI  ?id ?sam)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection1   "  ?id " Jalaka )" crlf))
)

;@@@ Added by Anita-05-07-2014
;She decided on reflection to accept his offer after all. [oxford learner's dictionary]
;उसने काफी चिंतन के बाद प्रस्ताव को स्वीकार करने का निर्णय ले ही लिया ।
;A week off would give him time for reflection.
;एक सप्ताह की छुट्टी उसको चिन्तन मनन का समय देगी । 
(defrule reflection2
(declare (salience 2000))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-on_saMbanXI  ?kri ?id)(viSeRya-for_saMbanXI  ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cinwana_manana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection2   "  ?id " cinwana_manana)" crlf))
)

;@@@ Added by Anita-05-07-2014
;A book of her reflections on childhood. [oxford learner's dictionary]
;उसकी किताब बचपन पर पुनरावलोकन है  ।
(defrule reflection3
(declare (salience 3500))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 book)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id punarAvalokana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection3   "  ?id " punarAvalokana)" crlf))
)

;@@@ Added by Anita--05-07-2014
;The article is an accurate reflection of events that day. [oxford learner's dictionary]
;लेख उस दिन की घटनाओं का एक सटीक अभिव्यक्ति है ।
(defrule reflection4
(declare (salience 4500))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 article)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBivyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection4   "  ?id " aBivyakwi)" crlf))
)
;@@@ Added by Anita--06-07-2014
;Unlike commonsensical observations or philosophical reflections or theological commentaries, sociology is bound by ;scientific canons of procedure. [SOCIOLOGY TEXT BOOK CLASS XI]
;समाजशास्त्र प्रचलित सामान्य बौद्धिक प्रेक्षणों या दार्शनिक अनुचिंतनों या ईश्नरवादी व्याख्यानों से हटकरप वैज्ञानिक कार्यविधियों से बंधा हुआ है ।
(defrule reflection5
(declare (salience 5000))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 philosophical)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciMwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection5   "  ?id " anuciMwana)" crlf))
)
;##########################################default-rule##################################

;@@@ Added by Anita--05-07-2014
;He admired his reflection in the mirror. [oxford learner's dictionary]
;वह आईने में अपने प्रतिबिंब की प्रशंसा की ।
;Your clothes are often a reflection of your personality.
;अक्सर आपके कपड़े आपके व्यक्तित्व का प्रतिबिंब होते हैं ।
(defrule reflection0
(declare (salience 0))
(id-root ?id reflection)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibimba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reflection.clp 	 reflection0   "  ?id " prawibimba )" crlf))
)








