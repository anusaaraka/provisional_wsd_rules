;@@@Added by 14anu-ban-02(13-04-2015)
;A brash wood.[self ref:mw]
;नाजुक लकड़ी . [self]
(defrule brash1 
(declare (salience 100)) 
(id-root ?id brash) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 wood)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nAjuka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brash.clp  brash1  "  ?id "  nAjuka )" crlf)) 
) 


;@@@Added by 14anu-ban-02(13-04-2015)
;Don't you think that suit's a bit brash for a funeral? [oald]
;क्या आपको नहीं लगता है कि सूट क्रिया कर्म के लिए थोड़ा सा भड़कीला है? [self]
(defrule brash2 
(declare (salience 100)) 
(id-root ?id brash) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 funeral)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BadZakIlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brash.clp  brash2  "  ?id "  BadZakIlA )" crlf)) 
) 


;------------------ Default Rules-----------------------------------------


;@@@Added by 14anu-ban-02(13-04-2015)
;Sentence: Beneath his brash exterior, he's still a little boy inside.[oald]
;Translation: उसके ढीठ बाह्य रूप के नीचे, वह अन्दर से अभी भी  छोटा लड़का  है . [self]
(defrule brash0 
(declare (salience 0)) 
(id-root ?id brash) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id DITa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brash.clp  brash0  "  ?id "  DITa )" crlf)) 
) 


