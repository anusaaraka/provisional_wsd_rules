;@@@ Added by 14anu-ban-11 on (07-04-2015)
;She deftly wove the flowers into a garland. (oald)
;उसने कुशलता से माला में फूल गूँथे . (self)
(defrule weave2
(declare (salience 11))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 garland)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gUzWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp 	weave2   "  ?id "  gUzWa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (07-04-2015)
;The road weaves through a range of hills.(oald)
;सडक पहाडियों मे विभिन्न प्रकार से घुमावदार मार्ग से हो कर आगे बढती है . (self)(confused)
(defrule weave3
(declare (salience 12))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?id1)
(id-root ?id1 range)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumAvaxAra_mArga_se_ho_kara_Age_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp 	weave3   "  ?id "  GumAvaxAra_mArga_se_ho_kara_Age_baDZa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (07-04-2015)
;To weave a narrative.(oald)
;वर्णणात्मक कहानी गढी . (self)
(defrule weave4
(declare (salience 13))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 narrative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp 	weave4   "  ?id "  gaDZa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (07-04-2015)
;She was weaving in and out of the traffic. (oald)
;वह ट्रैफ़िक के अन्दर और बाहर घूम रह रही थी . (self)
(defrule weave5
(declare (salience 14))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI  ?id ?id1)
(id-root ?id1 traffic)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp 	weave5   "  ?id "  GUma_raha)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (07-04-2015)
;The baskets are woven from strips of willow. (oald)
;डलियाँ विलो के पेड की पट्टियों से बुनी जातीं हैं . (self)
(defrule weave0
(declare (salience 00))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp    weave0   "  ?id "  buna)" crlf))
)

;@@@ Added by 14anu-ban-11 on (07-04-2015)
;A tight weave. (oald)
;एक  महँगी बुनावट . (self)
(defrule weave1
(declare (salience 10))
(id-root ?id weave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bunAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weave.clp    weave1   "  ?id "  bunAvata)" crlf))
)

