;@@@ Added by 14anu-ban-06 (09-04-2015)
;The infiltration of rain into the soil.(OALD)
;मिट्टी में वर्षा का समावेश . (manual) 
(defrule infiltration1
(declare (salience 2000))
(id-root ?id infiltration)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 rain|water|moisture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAveSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infiltration.clp 	infiltration1   "  ?id "  samAveSa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (09-04-2015)
;The infiltration of terrorists from across the border. (OALD)
;सीमा के सभी ओर से आतंकवादियों की घुसपैठ . (manual)
(defrule infiltration0
(declare (salience 0))
(id-root ?id infiltration)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GusapETa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  infiltration.clp 	infiltration0   "  ?id "  GusapETa )" crlf))
)
