;@@@ Added by 14anu-ban-11 on (13-03-2015)
;The factory is shedding a large number of jobs.(oald)
;फैक्टरी  बडी सङ्ख्या मे  नौकरी निकाल रही है . (self)
(defrule shed3
(declare (salience 5001))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed3   "  ?id "  nikAla )" crlf))
)

;@@@ Added by 14anu-ban-11 on (13-03-2015)
;Freedom fighters shed their blood for their country.(hinkhoj)
;स्वतन्त्रता सैनिकों ने अपने देश के लिए अपना खून बहाया . (self)
(defrule shed4
(declare (salience 5002))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 blood)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed4   "  ?id "  bahA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (13-03-2015)
;You should have the will power to shed your bad habits.(hinkhoj)
;आपके अन्दर दृढ इच्छा शक्ति होनी चाहिये  अपनी खराब आदतो कोछोड देनी की . (self)
(defrule shed5
(declare (salience 5003))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 habit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed5  "  ?id "  CodZa_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (13-03-2015)
;The candles shed a soft glow over the room.(hinkhoj)
;मोमबत्तियों ने कमरे मे एक कोमल दीप्ति फैला दी . (self)
(defrule shed6
(declare (salience 5004))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI  ?id ?id1)
(id-root ?id1 room)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed6   "  ?id "  PElA_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (13-03-2015)
;Luke shed his clothes onto the floor. (oald)
;लूक ने फर्श पर अपने वस्त्र गिरा दिए . (self)
(defrule shed7
(declare (salience 5005))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-onto_saMbanXI  ?id ?id1)
(id-root ?id1 floor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id girA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed7   "  ?id "  girA_xe)" crlf))
)

(defrule shed0
(declare (salience 5000))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAyabAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed0   "  ?id "  sAyabAna )" crlf))
)

;"shed","N","1.sAyabAna"
;He kept the cattle shed quite dirty.
;
(defrule shed1
(declare (salience 4900))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed1   "  ?id "  JAdZa )" crlf))
)

;"shed","V","1.JAdZanA"
;Coniferous forests shed their leaves during a particular time.
;--"2.bahAnA"
;Freedom fighters shed their blood for their country.
;--"3.CodZa_xenA"
;You should have the will power to shed your bad habits.
;--"4.PElAnA"
;The candles shed a soft glow over the room.
;

;@@@ Added by 14anu05 GURLEEN BHAKNA on 19.06.14
;Perhaps I should shed a few pounds.
;शायद मुझे कुछ पौंड कम करने चाहिए .
(defrule shed2
(declare (salience 5000))
(id-root ?id shed)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 pound|kilo|gram|weight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shed.clp 	shed2   "  ?id "  kama_karanA )" crlf))
)

