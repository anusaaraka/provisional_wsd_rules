;Commented the whole rule because correct meaning is coming from sigh3(here, 'sigh for'  meant 'ke_liye_warasa' for all examples that I encountered)
;@@@ Added by 14anu20 on 20/06/2014
;She sighed for meeting him.[Source added by 14anu-ban-01 on (02-01-2015)--http://dict.hinkhoj.com/words/meaning-of-SIGH-in-hindi.html]
;vaha usase milane ke lie warasI . 
;(defrule sigh0
;(declare (salience 1800))
;(id-root ?id sigh)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-for_saMbanXI  ?id ?id1) 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id warasa));
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " sigh.clp sigh0 " ?id "  warasa )" crlf)) 
;)


;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 on 20/06/2014
;She heard the sigh of the wind in trees.
;usane pedoM meM havA kI sAzsa sunI 
;उसने पेडों में हवा की सरसराहट सुनी [Transltion improved by 14anu-ban-01 on (02-01-2015)]
(defrule sigh1
(declare (salience 1900))
(id-root ?id sigh)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id  sigh) 
(id-cat_coarse ?id noun)
(kriyA-object  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarasarAhata));changed "sarasarAhawa" to "sarasarAhata"  by 14anu-ban-01 on (02-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " sigh.clp sigh1 " ?id "  sarasarAhata )" crlf)) ;changed "sarasarAhawa" to "sarasarAhata"  by 14anu-ban-01 on (02-01-2015)
)

;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 on 20/06/2014
;She breathed a sigh of relief.
;mEMne rAhawa kA gaharI sAzsa liyA .
;उसने राहत की साँस ली[Transltion improved by 14anu-ban-01 on (02-01-2015)]
(defrule sigh2
(declare (salience 2000))
(id-root ?id sigh)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id  sigh) ;Commented by  14anu-ban-01 on (02-01-2015)
(id-root ?id1 breathe)
(kriyA-object ?id1 ?id);Added ?id1 by 14anu-ban-01 on (02-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -));changed "gaharI" to "-"  by 14anu-ban-01 on (02-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " sigh.clp sigh2 " ?id " - )" crlf)) ;changed "gaharI" to "-"  by 14anu-ban-01 on (02-01-2015)
)

;@@@ Added by 14anu20 on 20/06/2014 
;She sighed for past days
;वह पिछले दिनों के लिये तरसी . 
;Example removed from sigh0 and added here by 14anu-ban-01
;She sighed for meeting him.[Source added by 14anu-ban-01 on (02-01-2015)--http://dict.hinkhoj.com/words/meaning-of-SIGH-in-hindi.html]
;vaha usase milane ke lie warasI . 
(defrule sigh3
(declare (salience 2100))
(id-root ?id sigh)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) ke_liye_warasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sigh.clp	sigh3  "  ?id "  " (+ ?id 1) "  ke_liye_warasa  )" crlf))
)

;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 on 20/06/2014
;He said with sigh.
;उसने समाचार गहरी साँस ले कर   कहे . 
;He said with a sigh.[Sentence improved by 14anu-ban-01 with reference to COCA]
;उसने गहरी साँस ले कर कहा . [self]
(defrule sigh4
(declare (salience 2100))
(id-root ?id sigh)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?id1 ?id)
;(id-root ?id1 say|tell|talk)commented by 14anu-ban-01 on (02-01-2015) to generalize the rule
(id-root =(- ?id 1) a)
(id-root =(- ?id 2) with)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (- ?id 2) gaharI_sAzsa_le_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sigh.clp	sigh4  "  ?id "  " (- ?id 1) "  " (- ?id 2) "  gaharI_sAzas_le_kara  )" crlf));corrected "gaharI_sAzas_le_kara" to "gaharI_sAzsa_le_kara"  by 14anu-ban-01 on (02-01-2015)
)

