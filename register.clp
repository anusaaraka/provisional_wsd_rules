
(defrule register0
(declare (salience 5000))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register0   "  ?id "  bahI )" crlf))
)

;"register","N","1.bahI"
;Sir could you sign the hotel register please?
;--"2.xUrI_jahAz_waka_manuSya_yA_bAje_kI_AvAja_sunAI_padZe"
;The register of a flute is very high.  
;
(defrule register1
(declare (salience 4000));decrease salience by Anita--10.7.2014
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rajistara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register1   "  ?id "  rajistara_kara )" crlf))
)

;"register","VT","1.rajistara_kara"
;--"2.xarja_karanA"
;You should always register the birth of a child.
;--"3.paMjIkqwa_karAnA"
;Get yourself registered in the Employment Exchange.  
;--"4.praxarSiwa_karanA"
;Rain Gauge registered 4.7 cm of rain fall.

;@@@ Added by Anita--10-07-2014
;In the office register the employees had marked themselves present for the next two days. [By mail]
;कार्यालय के रजिस्टर में कर्मचारियों ने खुद को अगले दो दिनों के लिए उपस्थित  चिह्नित किया था 
(defrule register2
(declare (salience 5100))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 office)
(kriyA-in_saMbanXI  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rajistara))
(assert  (id-wsd_viBakwi   ?id1  ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register2   "  ?id "  rajistara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  register.clp     register2   "  ?id1 "  ke )" crlf))
)

;@@@ Added by Anita--10-07-2014
;China has registered a protest over foreign intervention. [oxford learner's dictionary]
;चीन ने विदेशी हस्तक्षेप पर विरोध प्रकट किया है ।
(defrule register3
(declare (salience 4800))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 protest)
(id-root ?id2 intervention)
(kriyA-object  ?id ?id1)
(viSeRya-over_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register3   "  ?id "  prakata_kara )" crlf))
)

;@@@ Added by Anita--11-07-2014
;The thermometer registered 32 SYMBOL-DEGREE-SIGN C. [oxford learner's dictionary]
;थर्मामीटर ने 32 डिग्री सेल्सियस तापमान दर्ज किया ।
(defrule register4
(declare (salience 5200))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 c|C)
(kriyA-object_1  ?id ?num)
(kriyA-object_2  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register4   "  ?id "  xarja_kara )" crlf))
)

;@@@ Added by Anita--11-07-2014
;The earthquake registered 3 on the Richter scale. [oxford learner's dictionary]
;भूकंप की तीव्रता रिक्टर पैमाने पर 3 दर्ज की गई ।
(defrule register5
(declare (salience 5550))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1  scale)
(id-root ?id2  Richter)
(kriyA-on_saMbanXI  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register5   "  ?id "  xarja_kara )" crlf))
)

;@@@ Added by Anita--11-07-2014
;Her face registered disapproval. [oxford learner's dictionary]
;उसके चेहरे ने असहमति प्रकट की ।
;usake cehare ne asammawi prakata kI. [verified sentence]
(defrule register6
(declare (salience 5400))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 disapproval)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register6   "  ?id "  prakata_kara )" crlf))
)

;@@@ Added by Anita--11-07-2014
;Shock registered on everyone's face. [oxford learner's dictionary]
;प्रत्येक के चेहरे पर सदमा दिख रहा था ।
(defrule register7
(declare (salience 5500))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ?)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register7   "  ?id "  xiKa_raha )" crlf))
)

;@@@ Added by Anita--11-07-2014
;He barely registered our presence. [oxford learner's dictionary]
;उसने हमारी उपस्थिति नाममात्र के लिए गौर किया । 
(defrule register8
(declare (salience 5600))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gOra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register8   "  ?id "  gOra_kara )" crlf))
)

;@@@ Added by Anita--11-07-2014
;I told her my name, but it obviously did not register. [oxford learner's dictionary]
;मैंने उसे अपना नाम बताया था, पर वास्तव यह उसके ध्यान में नहीं रहा ।
(defrule register9
(declare (salience 5700))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_niReXaka  ?id ?)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_meM_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register9   "  ?id "  XyAna_meM_raha )" crlf))
)

;@@@ Added by Anita--12-07-2014
;Can I register this, please? [oxford learner's dictionary]
;क्या मैं यह कृपया दर्ज कर सकता हूँ ?
;The stock exchange has registered huge losses this week. [oxford learner's dictionary]
;शेयर बाजार में इस सप्ताह भारी नुकसान दर्ज किया गया है ।
(defrule register10
(declare (salience 5800))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(or(praSnAwmaka_vAkya)(kriyA-kAlavAcI  ?id ?))
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register10   "  ?id "  xarja_kara )" crlf))
)

;@@@ Added by Anita--12-07-2014
;She is officially registered as disabled.
;वह विकलांग के रूप में सरकारी तौर से पंजीकृत की गई है ।
;vaha vikalAfga ke rUpa meM sarakArI wOra se paFjIkqwa kI gayI hE. [verified sentence]
(defrule register11
(declare (salience 5900))
(id-root ?id register)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id ?)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMjIkqwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  register.clp 	register11   "  ?id "  paMjIkqwa_kara )" crlf))
)
