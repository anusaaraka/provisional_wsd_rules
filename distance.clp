;@@@ Added by 14anu-ban-04 (20-03-2015)
;Our parents live some distance away.                       [oald]
;हमारे माँ बाप कुछ दूरी पर रहते हैं .                                     [self]
(defrule distance3
(declare (salience 5010))
(id-root ?id distance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) away )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) xUrI_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " distance.clp	distance3  "  ?id "  " (+ ?id 1) "  xUrI_para )" crlf))
)


(defrule distance0
(declare (salience 100))              ;salience reduced from '5000' to ' 100'  by 14anu-ban-04 on (20-03-2015)
(id-root ?id distance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distance.clp 	distance0   "  ?id "  xUrI )" crlf))
)

;"distance","N","1.xUrI"
;The distance from Indore to Bhopal is around 300 km.
;
;$$$ modified by 14anu26     [01-07-14] 
;Is India's most popular politician distancing himself from popular sentiment ?
;क्या भारत का सबसे अधिक लोकप्रिय राजनीतिज्ञ लोकप्रिय भावना से स्वयं को दूर कर रहा है? 
(defrule distance1
(declare (salience 4900))
(id-root ?id distance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_kara)) ;meaning changed from 'svayaM_ko_kisI_se_xUra_kara' to 'xUra_kara' by 14anu26 [01-07-14] 
(assert (kriyA_id-object_viBakwi ?id ko)) ;added by 14anu26 [01-07-14] 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distance.clp 	distance1   "  ?id "  xUra_kara )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  distance.clp      distance1   "  ?id "  ko )" crlf))                                                           

;"distance","VT","1.svayaM_ko_kisI_se_xUra_karanA"
;He distanced himself from campus politics.
;


;@@@ Added by 14anu-ban-04 (10-10-2014)
;A bar magnet can attract an iron nail from a distance.         [NCERT-CORPUS]
;कोई छड चुम्बक लोहे की कीलों को दूर से ही, अपनी ओर आकर्षित कर सकता है.              [NCERT-CORPUS]
  
(defrule distance2
(declare (salience 5000))
(id-root ?id distance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-from_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distance.clp 	distance2  "  ?id "  xUra )" crlf))
)


