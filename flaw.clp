
(defrule flaw0
(declare (salience 5000))
(id-root ?id flaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flaw.clp 	flaw0   "  ?id "  xoRa )" crlf))
)

;"flaw","N","1.xoRa"
;This casting is full of flaws.
;--"2.wruti"
;Your report is full of flaws.
;--"3.kamajZorI"
;The main flaw in her character is the  habit of telling lies at all times.
;
(defrule flaw1
(declare (salience 4900))
(id-root ?id flaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRayukwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flaw.clp 	flaw1   "  ?id "  xoRayukwa_ho )" crlf))
)

;"flaw","V","1.xoRayukwa_honA"
;His presentation at the conference was flawed.
;

;@@@ Added by 14anu-ban-05 on (01-04-2015)
;Flaws have appeared in the new version of the software. [OALD]
;सॉफ्टवेयर के नए संस्करण में त्रुटियाँ दिखाई दिये है.			[MANUAL]
(defrule flaw2
(declare (salience 5001))
(id-root ?id flaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 appear)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wruti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flaw.clp 	flaw2   "  ?id "  wruti )" crlf))
)

;@@@ Added by 14anu-ban-05 on (01-04-2015)
;Unfortunately, this plate has a slight flaw in it. [OALD]
;दुर्भाग्य से, इस थाली में यह एक मामूली नुक्स है.		[manual]
(defrule flaw3
(declare (salience 5001))
(id-root ?id flaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id2)
(id-root ?id2 slight|major)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nuksa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flaw.clp 	flaw3   "  ?id "  nuksa )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (01-04-2015)
;After a short time, the flaws in his new bride’s character became more apparent.[OALD]
;थोड़े समय के बाद उसकी नई दुल्हन के चरित्र में खामियाँ अधिक स्पष्ट हो गये .			[MANUAL]
(defrule flaw4
(declare (salience 5002))
(id-root ?id flaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 character)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAmI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flaw.clp 	flaw4   "  ?id "  KAmI )" crlf))
)
	

