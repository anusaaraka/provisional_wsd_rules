;##############################################################################
;#  Copyright (C) 2013-2014  Prachi Rathore (prachirathore02@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by Prachi Rathore[5-3-14]
;I had a huge crush on her.
;मै उससे बहुत अधिक सम्मोहित था.
(defrule huge0
(declare (salience 5000))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa   ?id1 ?id)
(id-root ?id1 crush)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huge.clp 	huge0   "  ?id "  bahuwa_aXika )" crlf))
)

;@@@ Added by 14anu-ban-06  (19-08-2014)
;There is full fervour of the fair this time also in Garh Mukteshwar and huge crowd of devotees is gathering for the holy dip . (tourism corpus)
;इस  बार  भी  गढ़  मुक्तेश्वर  में  मेले  की  भरपूर  गहमागहमी  है  और  मुख्य  स्नान  के  लिए  श्रद्धालुओं  की  भारी  भीड़  जुट  रही  है  ।
;Huge crowds of local people are seen at all the hill stations during the summer season .(tourism corpus)
;ग्रीष्म ऋतु में सभी हिल स्टेशनों पर स्थानीय लोगों की भारी भीड़ देखने को मिलती है ।
;On the occasion of Dussehra a huge fair is also held in the field of Kullu  .(tourism corpus)
;दशहरे के अवसर पर कुल्लू के मैदान में एक भारी मेला भी लगता है ।
(defrule huge4
(declare (salience 5000))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa   ?id1 ?id)
(id-root ?id1 crowd|fair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huge.clp 	huge4   "  ?id "  BArI )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-01-2015)
;There is a huge craze among tourists of the aarti to be held in evening at bank of Ganga .(parallel corpus)
;शाम  के  समय  गंगा  किनारे  होने  वाली  आरती  का  पर्यटकों  के  बीच  खासा  क्रेज  है  ।(parallel corpus)
(defrule huge5
(declare (salience 5100))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa   ?id1 ?id)
(id-root ?id1 craze)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huge.clp 	huge5   "  ?id "  KAsA )" crlf))
)


;xxxxxxxxxxxx Default Rules xxxxxxxxxx
;@@@ Added by Prachi Rathore[5-3-14]
 ;Himalayas are huge mountains.[shiksharthi-kosh]
;हिमालय विशाल पर्वत हैं . 
(defrule huge1
(declare (salience 400))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huge.clp 	huge1   "  ?id "  viSAla )" crlf))
)


;@@@ Added by Prachi Rathore[5-3-14]
 ;Himalayas are huge mountains.[shiksharthi-kosh]
;हिमालय विशाल पर्वत हैं . 
(defrule huge3
(declare (salience 200))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huge.clp 	huge3   "  ?id "  viSAla )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_huge0
(declare (salience 5000))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 crush)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " huge.clp   sub_samA_huge0   "   ?id " bahuwa_aXika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_huge0
(declare (salience 5000))
(id-root ?id huge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 crush)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " huge.clp   obj_samA_huge0   "   ?id " bahuwa_aXika )" crlf))
)


