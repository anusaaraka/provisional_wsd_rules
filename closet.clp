;@@@ Added by 14anu-ban-03 (16-04-2015)
;A walk-in closet. [oald]
;एक विशाल कमरा .  [manual]
(defrule closet3
(declare (salience 4900))
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet3   "  ?id "  kamarA )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-04-2015)
;He was closeted with the President for much of the day. [oald]
;वह पूरे दिन के लिए राष्ट्रपति के साथ एकान्त में रहा था . [manual]
(defrule closet4
(declare (salience 5000))   
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekAMwa_meM_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet4   "  ?id "  ekAMwa_meM_raha )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-04-2015)
;She had closeted herself away in her room. [oald]
;उसने अपने कमरे के अन्दर स्वयं को बन्द रखा था . [manual]
(defrule closet5
(declare (salience 5000))   
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet5   "  ?id "  banxa_raKa )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-04-2015)
;I suspect he's a closet fascist. [oald]  ;working on parser no.- 10
;मैं सन्देह करता हूँ वह गुप्त संगठन है . [manual]
(defrule closet6
(declare (salience 5000))   
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet6   "  ?id " gupwa )" crlf))
)


;----------------------------------------Default rules----------------------------------------------

(defrule closet0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (16-04-2015)
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gopanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet0   "  ?id "  gopanIya )" crlf))
)

;"closet","Adj","1.gopanIya"
;He is a closet smuggler.
;
(defrule closet1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (16-04-2015)
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alamArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet1   "  ?id "  alamArI )" crlf))
)

;"closet","N","1.alamArI"
;I kept all my jewelleries in the closet.
;
(defrule closet2
(declare (salience 00))    ;salience reduced by 14anu-ban-03 (16-04-2015)
(id-root ?id closet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_kArya_meM_magna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  closet.clp 	closet2   "  ?id "  kisI_kArya_meM_magna_ho )" crlf))
)

;"closet","V","1.kisI_kArya_meM_magna_honA"
;They were closeted in the classroom with the Professor.
;
