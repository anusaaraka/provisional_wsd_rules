;@@@ Added by Anita-09-06-2014
;I have a document wallet . [oxford learner's dictionary]
;मेरे पास एक दस्तावेज़ पेटी है ।
(defrule wallet0
(declare (salience 5000))
(id-root ?id wallet)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 document)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  xaswAvejZa_petI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallet.clp 	wallet0  " ?id "  " ?id1 " xaswAvejZa_petI )" crlf))
)

;@@@ Added by Anita-09-06-2014
;She picked up the wallet and handed it back to him. [oxford learner's dictionary]
;उसने बटुआ उठाया और उसको वापस दिया ।
(defrule wallet1
(declare (salience 1000))
(id-root ?id wallet)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id batuA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallet.clp 	wallet1   "  ?id " batuA )" crlf))
)

;#############################default-rule##################################

;@@@ Added by Anita-09-06-2014
;He pulled out a big, fat wallet stuffed with bank notes.[cambridge dictionary]
;उसने एक बड़ा, मोटा बैंक के नोटों से भरा बटुआ निकाला ।
;Paper money and credit cards are in my wallet . [oxford learner's dictionary]
;कागज़ी मुद्रा और क्रैडिट- कार्ड मेरे बटुए में हैं ।
(defrule wallet_default-rule
(declare (salience 0))
(id-root ?id wallet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id batuA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wallet.clp 	wallet_default-rule   "  ?id "  batuA )" crlf))
)
