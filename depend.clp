
;$$$ Modidied by 14anu-ban-04 (15-11-2014) 
;### [COUNTER EXAMPLE]### It is defined at each point in space (and can in addition depend on time).                    [NCERT-CORPUS]
;### [COUNTER EXAMPLE]### इसे दिक्स्थान के हर बिंदु पर परिभाषित किया जाता है (और साथ ही समय पर निर्भर कर सकता है).                  [NCERT-CORPUS]
 
;Added by Pramila(Banasthali University) on 19-11-2013
;I'm depending on you to keep your promise.             ;cald
;मैं तुम पर भरोसा कर रहा हूँ कि अपना वायदा पूरा करोंगे.
;You can't always depend on the trains arriving on time.              ;cald
;तुम ट्रेनों का समय पर आने का भरोसा नहीं कर सकते .
(defrule depend0
(declare (salience 4900))
(id-root ?id depend)
?mng <-(meaning_to_be_decided ?id)
(or(and(kriyA-on_saMbanXI  ?id ?id1)(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id2))(kriyA-kriyArWa_kriyA  ?id ?id1))           ;remove 'kriyA-in_saMbanXI' relation by  14anu-ban-04
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarosA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depend.clp 	depend0   "  ?id "  BarosA_kara )" crlf))
)

;$$$ Modified by 14anu-ban-04 (15-11-2014)
;Added by Pramila(Banasthali University) on 19-11-2013
;Charities depend on people supporting their activities.                ;cald
;अनुदान लोगों की मदद पर निर्भर करता है.
(defrule depend1
(declare (salience 5000))
(id-root ?id depend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str)                                                                                   ;added by  14anu-ban-04
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))                ;added by  14anu-ban-04
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "inanimate.gdbm" ?str)))                  ;commented by  14anu-ban-04

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depend.clp 	depend1   "  ?id "  nirBara_kara )"
 crlf))
)

;@@@ Added by 14anu-ban-04 (06-12-2014)
;In addition to depending on each point in space, it can also vary with time, i.e., be a function of time.    [NCERT-CORPUS]
;दिक्स्थान में प्रत्येक बिंदु पर निर्भर होने के साथ-साथ यह समय के साथ भी परिवर्तित हो सकता है, अर्थात यह समय का फलन है.                [NCERT-CORPUS]
(defrule depend4
(declare (salience 4010))
(id-root ?id depend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(viSeRya-in_saMbanXI ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depend.clp 	depend4  "  ?id "  nirBara_ho )" crlf))
)



;-------------------------------------default rules---------------------------------------------
(defrule depend2
(declare (salience 4000))
(id-root ?id depend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depend.clp 	depend2   "  ?id "  nirBara_kara )" crlf))
)

;"depend","V","1.nirBara_kara"
(defrule depend3
(declare (salience 3000))
(id-root ?id depend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarosA_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  depend.clp 	depend3   "  ?id "  BarosA_raKa )" crlf))
)

;"depend","VT","1.BarosA_raKanA"
;I can't depend on you any more.
;--"2.ASriwa_honA"
;Women should not depend on their husbands for their needs.
;
;LEVEL 
;Headword : depend
;
;Examples --
;
;"depend","VT","1.nirBara_karanA"
;Women should not depend on their husbands for their monetary needs.
;apanI ArWika jZarUrawoM ke liye swriyoM ko apane pawiyoM para nirBara nahIM karanA cAhiye.
;--"2. BarosA_raKanA"
;I can't depend on you any more.
;mEM aba wumhAre Upara BarosA nahIM kara sakawA. <--BarosA_karanA <--nirBara_honA
;Don't depend on the bus leaving on time.
;basa ke samaya para calane para BarosA mawa raKo. <---Basa ke samaya para calane para nirBara nahIM raho.
;
;sUwra : nirBara_honA` 
