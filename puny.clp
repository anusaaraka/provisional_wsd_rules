;@@@ Added by 14anu-ban-09 on (07-02-2015)
;Two puny men armed only with swords against a twenty-ton killing machine. 	[COCA]
;दो शक्तिहीन आदमी एक बीस-टन घातक मशीन के विरुद्ध  सिर्फ़ तलवारों से सशस्त्र थे .  			[Manual]

(defrule puny1
(declare (salience 1000))
(id-root ?id puny)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-subject  ?id2 ?id1)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id2 arm)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SakwihIna)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  puny.clp 	puny1   "  ?id "  SakwihIna )" crlf))
)

;-----------------------------DEFAULT RULE--------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (07-02-2015)
;Her puny limps were not strong enough to carry the weight.     [Hinkhoj.com]
;उसकी अदना लंगड़ी चाल  भार उठाने के लिए मजबूत नहीं थी. 

(defrule puny0
(declare (salience 000))
(id-root ?id puny)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  puny.clp     puny0   "  ?id "  axanA )" crlf))
)


