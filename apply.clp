
;@@@ Added by Garima Singh(M.Tech-C.S) 4-dec-2013
;He wants a job in which he can apply his foreign languages.[cambridge]
;वह एक ऐसा काम चाहता है जिसमें वह उसकी विदेशी भाषाएँ उपयोग कर सके. 
(defrule apply2
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?obj ?id1)
(kriyA-object  ?id ?obj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply2   "  ?id "  upayoga_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 4-dec-2013
;If you apply pressure to a cut it's meant to stop the bleeding.[cambridge]
;अगर तुम घाव पर दबाब डालोगे तो यह खून रोकने का काम करेगा 
(defrule apply3
(declare (salience 5500))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-to_saMbanXI  ?id ?id2)
(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1))
(id-word ?id1 pressure)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAlo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply3   "  ?id "  dAlo )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 4-dec-2013
;You can solve any problem if you apply yourself.[cambridge]
;तुम कोई भी समस्या हल कर सकते हो अगर तुम पूरी मेहनत करोगे
(defrule apply4
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-word ?obj yourself)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrI_mehanawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply4   "  ?id "  pUrI_mehanawa_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 4-dec-2013
;Tim has applied to join the police.[cambridge]
;टिम ने पुलिस में भर्ती के लिये आवेदन किया है
;We've applied to a charitable organization for a grant for the project.[cambridge]
;हमने परियोजना के लिये आर्थिक मदद के लिये एक समाजसेवी संस्था से आवेदन किया है . 
;Note:That bit of the form is for UK citizens - it doesn't apply to you. [this sentence is a contradictory example for above rule, so I have added a new rule 'apply5']
(defrule apply5
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-kriyArWa_kriyA  ?id ?id1)(kriyA-to_saMbanXI  ?id ?id1))
(kriyA-subject ?id ?sub)
(id-cat_coarse ?sub PropN|pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply5   "  ?id "  Avexana_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 5-dec-2013
;That bit of the form is for UK citizens - it doesn't apply to you. 
;फार्म का यह भाग यू.के. के नागरिको के लिये है , यह आप पर लागू नहीं होता है
(defrule apply6
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN|pronoun)
;(id-word ?id1 you|him|her|us)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply6   "  ?id "  lAgU_ho )" crlf))
)

;$$$ Modified by 14anu-ban-02 (19-11-2014)
;Note that this term does not apply to the burning of crops which are or can be usefully harvested by this means, such as sugar cane.[agriculture]
;ध्यान दीजिए कि यह शब्द फसलों के दाहक  पर लागू  नहीं होता है जो इन साधनों के द्वारा उपयोगी ढंग से फसल हो सकती है जैसे कि गन्ना|[self]
;@@@ Added by Garima Singh(M.Tech-C.S) 9-jan-2014
;Special conditions apply if you are under 18.[oald]
;खास शर्ते लागू होती हैं यदी आप १८ वर्ष से कम आयु के है
(defrule apply7
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject  ?id ?sub)(kriyA-vAkya_subject  ?id ?sub))
(id-root ?sub it|rule|law|condition|say|term);term is added the list by 14anu-ban-02(19-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply7   "  ?id "  lAgU_ho )" crlf))
)

;$$$ Modified by Bhagyashri Kulkarni (29-10-2016)
;There is no need to stay at hospital for more days because no stitches are being applied.(health)
;अधिक दिनों से अस्पताल पर रहने के लिए कोई जरूरत नहीं है क्योंकि कोई टाङ्कें नहीं लगाए जा रहे हैं .
;$$$ Modified by 14anu-ban-01 on 04-08-2014  --- added 'paste' to the list
;@@@ Added by Garima Singh(M.Tech-C.S) 5-dec-2013
;The paint should be applied thinly and evenly.[cambridge]
;रंग को पतला और एकसा लगाना चाहिये
;We applied our minds to finding a solution to our problem.[cambridge]
;हमने अपनी समस्या के हल को ढूढंने के लिये दिमाग लगाया
;Then the brakes are applied and the car stops at t 20 s and x 296 m.[ncert]
;इस समय इसमें ब्रेक लगाया जाता है जिसके परिणामस्वरूप वह t = 20 s पर और x = 296 m पर रुक जाती है 
;281962:If there is fever , then apply the paste of henna in palms , fever will lower fast .[Karan Singla]
;यदि बुखार हो , तो हाथ में मेंहदी का पेस्ट लगाएँ , बुखार जल्द उतर जाएगा ।
(defrule apply8
(declare (salience 4000)) ;salience reduced from 5600 to 4000 by 14anu-ban-02(11-02-2015)
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject  ?id ?id1)(kriyA-object  ?id ?id1))
(id-root ?id1 cream|paint|glue|mind|brain|brake|paste|stitch);added brake in the list-by Garima Singh(18-march-2014) ;added 'stitch' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply8   "  ?id "  lagA )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;To apply for a position in a company.[cambridge]
;कम्पनी में पद के लिये आवेदन करना
(defrule apply9
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply9   "  ?id "  Avexana_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;The new technology was applied to farming.[oald]
;नयी तकनीक खेती में इस्तेमाल की गयी है
(defrule apply10
(declare (salience 4500))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(kriyA-subject  ?id ?sub)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iswemAla_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply10   "  ?id "  iswemAla_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;To apply to company.[oald]
;कम्पनी में आवेदन करना
(defrule apply11
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id1 ?id)
(kriyA-to_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply11   "  ?id "  Avexana_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;By the time I saw the job advertised it was already too late to apply. [oald]
;जिस समय मैनें नौकरी का विज्ञापन  देखा तब तक आवेदन करने के लिये देर हो चुकी थी
(defrule apply12
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply12   "  ?id "  Avexana_kara )" crlf))
)

;$$$Modified by 14anu-ban-02(11-02-2015) -- modified '?id' as 1
;Apply the cream sparingly to your face and neck.[oald]
;मलाई को आपने चेहचर और गर्दन पर किफायत से लगाओ.[manual] 
;@@@ Added by Garima Singh(M.Tech-C.S) 9-jan-2014
;$$$ Modified by 14anu18 (added make-up, medicine, balm, lipstick, deodorant
;Apply the cream sparingly to your face and neck.[oald]
;Apply the suntan cream liberally to exposed areas every three hours and after swimming.[cambridge]
(defrule apply13
(declare (salience 5600))
(id-root 1 apply)
?mng <-(meaning_to_be_decided 1)
;(or(kriyA-subject  ?kri ?id1)(kriyA-object  ?kri ?id1))	;commented by 14anu-ban-02(11-02-2015)
;(id-word ?id1 force|cream|paint|glue|brakes|make-up|lipstick|balm|medicine|deodorant)	;commented by 14anu-ban-02(11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng 1 lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply13   "  1 "  lagA )" crlf))
)


;@@@ Added by 14anu-ban-02(01-09-2014)
;Equation (7.49b) applies to any rolling body: a disc, a cylinder, a ring or a sphere.[ncert]
;समीकरण (7.49ब्) किसी भी आवर्ती पिन्ड:चकती ,बेलन ,वलय या गोले  पर लागू होता है .[ncert]
;The principle of superposition applies to both fields ; (In this connection, note that the magnetic field is linear in the source Idl just as the electrostatic field is linear in its source: the electric charge).[ncert]
;दोनों ही क्षेत्रों पर अध्यारोपण सिद्धान्त लागू होता है [इस सम्बन्ध में यह ध्यान दीजिए कि स्रोत Idl में चुम्बकीय क्षेत्र रैखिक है जैसे कि अपने स्रोत, विद्युत आवेश में स्थिर वैद्युत क्षेत्र रैखिक है].[ncert]
(defrule apply14
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-word ?id1 equation|principle)       ;'principle' is added by 14anu-ban-02 (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply14   "  ?id "  lAgU_ho )" crlf))
)

;@@@ Added by 14anu-ban-02(01-09-2014)
;In this section we shall apply these laws to a commonly encountered phenomena, namely collisions.[ncert 11_06]
;इस अनुभाग में, हम इन नियमों का बहुधा सामने आने वाली परिघटनाओं, जिन्हें सङ्घट्ट कहते हैं, में प्रयोग करेंगे.[ncert]
;Before we can apply Equation (8.5) to objects under consideration, we have to be careful since the law refers to point masses whereas we deal with extended objects which have finite size.[ncert]
;समीकरण (8.5) का अनुप्रयोग, अपने पास उपलब्ध पिण्डों पर कर सकने से पूर्व हमें सावधान रहना होगा, क्योङ्कि यह नियम बिन्दु द्रव्यमानों से सम्बन्धित है, जबकि हमें विस्तारित पिण्डों, जिनका परिमित आमाप होता है, पर विचार करना है.[ncert]
;Before we can apply Equation (8.5),we have to be careful .[ncert]
;समीकरण (8.5) का प्रयोग कर सकने से पूर्व हमें सावधान रहना होगा.[ncert]
;τ can be measured independently e.g. by applying a known torque and measuring the angle of twist.[ncert]
;τ की माप अलग प्रयोग द्वारा की जा सकती है, जैसे कि ज्ञात बल आघूर्ण का अनुप्रयोग करके तथा व्यावर्तन कोण मापकर.[ncert]
(defrule apply15
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 Equation|torque|law);added 'torque' by 14anu-ban-02(01-10-2014)	;added 'law' and 'id-word' changed into 'id-root' by 14anu-ban-02(06-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply15   "  ?id "  prayoga_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-10-2014)
;We applied the results of our study even to the motion of bodies of finite size, assuming that motion of such bodies can be described in terms of the motion of a particle.[NCERT corpus]
;फिर, यह मानते हुए कि परिमित आकार के पिंडों की गति को बिंदु कण की गति के पदों में व्यक्त किया जा सकता है, हमने उस अध्ययन के परिणामों को परिमित आकार के पिंडों पर भी लागू कर दिया था.[NCERT corpus]
(defrule apply16
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 result|rule|law)
(kriyA-subject ?id ?sub)
(id-cat_coarse ?sub PropN|pronoun)
(kriyA-to_saMbanXI  ?id ?id2)
(viSeRya-of_saMbanXI  ?id2 ?id3)
=>
(retract ?mng)	
(assert (id-wsd_root_mng ?id lAgU_kara_xe))
(assert (id-wsd_viBakwi ?id1 ko))
(assert (id-wsd_viBakwi ?id3 para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  apply.clp  	apply16  "  ?id1 " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  apply.clp  	apply16  "  ?id3 " para)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply16   "  ?id "  lAgU_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-02 (17-11-2014)
;The lighted matchstick, when applied to a firecracker, results in a spectacular display of sound and light. [ncert]
;जब सुलगाई गई माचिस की तीली पटाखे में लगाई जाती है तो उसके परिणामस्वरूप ध्वनि एवं प्रकाश ऊर्जाओं का भव्य प्रदर्शन होता है.[ncert]
(defrule apply17
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-word ?id1 firecracker)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  apply.clp  	apply17   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-02 (17-11-2014)
;The greater the change in the momentum in a given time, the greater is the force that needs to be applied.[ncert]
;यदि अधिक संवेग परिवर्तन की आवश्यकता है तो लगाने के लिए अधिक परिमाण के बल की आवश्यकता होगी.[ncert]
;The court heard how the driver had failed to apply his brakes in time.[cambridge]
;दरबार ने सुना कि कैसे चालक समय में उसके ब्रेक लगाने में असफल हुआ . 
(defrule apply18
(declare (salience 5000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(assert (make_verbal_noun ?id))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  apply.clp 	apply18   "  ?id "  lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  apply.clp 	apply18    "  ?id " )" crlf))
)

;@@@Added by 14anu-ban-02(11-02-2015)
;In Fig.9.2(a), a cylinder is stretched by two equal forces applied normal to its cross-sectional area.[ncert 11_09]
;चित्र 9.2(a) में, एक बेलन को उसके अनुप्रस्थ परिच्छेद की लम्बवत् दिशा में दो समान बल लगाकर तानित किया गया है.[ncert]
(defrule apply19
(declare (salience 1000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagAkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  apply.clp 	apply19  "  ?id "  lagAkara )" crlf))
)

;@@@Added by 14anu-ban-02(16-02-2015)
;We assume this general result (see Exercise 7.31), and apply it to the case of rolling motion.[ncert 11_07]
;हम इस व्यापक परिणाम को मान कर चलते हैं, (देखिये अभ्यास 7.31 ), और चकती जैसे दृढ पिंड की लोटनिक गति के विशिष्ट मामले में इसे लागू कर लेते हैं.[ncert]
(defrule apply20
(declare (salience 1000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ? ?id1 ?id)
(id-root ?id1 assume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  apply.clp 	apply20  "  ?id "  lAgU_kara )" crlf))
)

;@@@Added by 14anu-ban-02(16-02-2015)
;You can stop a ball rolling down an inclined plane by applying a force against the direction of its motion.[ncert 11_05]
;किसी आनत तल पर नीचे की ओर लुढकती किसी गेंद को उसकी गति की विपरीत दिशा में बल लगाकर रोका जा सकता है.[ncert]
(defrule apply21
(declare (salience 1000))
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-against_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagAkara))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) lagAkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  apply.clp 	apply21  "  ?id "  lagAkara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " apply.clp	apply21  "  ?id "  " (- ?id 1) "  lagAkara  )" crlf))
)


;************************DEFAULT RULES**************************************
(defrule apply0
(declare (salience 0));salience reduced by Garima Singh(M.Tech-C.S) 4-dec-2013
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id applied )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyAvahArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  apply.clp  	apply0   "  ?id "  vyAvahArika )" crlf))
)

;"applied","Adj","1.vyAvahArika"
;In every text book applied grammar is given at the end of the lessons
;for practice .
;
;$$$ modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 09-jan-2014

(defrule apply1
(declare (salience 0));salience reduced by Garima Singh(M.Tech-C.S) 4-dec-2013
(id-root ?id apply)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avexana_kara)); meaning changed from 'lagU_kara' to 'Avexana_kara'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  apply.clp 	apply1   "  ?id "  Avexana_kara )" crlf))
)

;"apply","VI","1.lAgU_karanA"
;"apply","V","1.lAgU_kara[ho]"
;Traffic rules must be applied strictly .
;--"2.lagAnA"
;Apply 'BARNOL' on the burns .
;--"3.prayoga_meM_lAnA"
;Apply the breaks wherever necessary .
;Unsocial elements apply political pressure && go unpunished .
;--"4.Avexana_pawra_xenA"
;Apply to the chairman for the post of cashier .
;
