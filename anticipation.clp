;@@@ Added by 14anu-ban-02(28-02-2015)
;A sure pair of hands , agility , anticipation , a clean pick-up and a good throw can be developed by hard practice .[karan singla]
;मज़बूत पकड़वाले हाथ , गेंद कब कहां गिरने वाली है इसका सही अनुमान , चुस़्ती और सतर्कता , इऩ्हें अभ़्यास द्वारा पाया जा सकता है .[karan singla]
;मज़बूत पकड़वाले हाथ , चुस़्ती  ,पूर्वानुमान,(गेंद को)फुर्ती से उठाकर अच्छी तरह फेंकना अभ़्यास द्वारा पाया जा सकता है .[modified]
(defrule anticipation0
(declare (salience 0))
(id-root ?id anticipation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvAnumAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipation.clp 	anticipation0   "  ?id " pUrvAnumAna)" crlf))
)

;@@@ Added by 14anu-ban-02(28-02-2015)
;The courtroom was filled with anticipation.[oald]
;अदालत प्रत्याशा से भर गयी थी|[self]
(defrule anticipation1
(declare (salience 100))
(id-root ?id anticipation)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-root ?id1 fill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyASA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipation.clp 	anticipation1   "  ?id " prawyASA )" crlf))
)

;@@@Added by 14anu-ban-02(28-02-2015)
;He was standing at the bus station in my anticipation .[karan singla]
;वह मेरी प्रतीक्षा में बस अड्डे पर खडा हुआ था . [self]
(defrule anticipation2
(declare (salience 100))
(id-root ?id anticipation)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id1 ?id)	;need sentences to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawIkRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipation.clp 	anticipation2   "  ?id " prawIkRA )" crlf))
)

;@@@Added by 14anu-ban-02(02-03-2015)
;The statement was met with silence containing equal parts awe and anticipation .[karan singla]
;मगर इस बयान पर चुप्पी छाई रही , जिसमें हैरानी और उमीदों का पुट भी था .[karan singla]
;इस बयान पर चुप्पी छाई रही , जिसमें विस्मयपूर्ण आदर और उमीदों का बराबर पुट भी था .[self]
(defrule anticipation3
(declare (salience 100))
(id-root ?id anticipation)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id2 ?id1)	;need sentences to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ummIxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   anticipation.clp 	anticipation3   "  ?id " ummIxa )" crlf))
)

