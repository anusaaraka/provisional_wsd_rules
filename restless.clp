;##############################################################################
;#  Copyright (C) 2014-201 Amit kumar jha(amitjha823@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02149, USA.
;
;##############################################################################



;@@@ Added by 14anu23 23/06/2014
;The animal is restless and frequently changes postures . 
;पशु बेचैन है और जल्दी-जल्दी मुद्राएँ  बदलता हैं . 
(defrule restless0
(declare (salience 4801))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa   ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becEna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp 	restless0   "  ?id "  becEna )" crlf))
)


;@@@ Added by 14anu23 23/06/2014
;A restless child.
;एक चञ्चल बच्चा . 
(defrule restless1
(declare (salience 4800))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa   ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caMcala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp 	restless1   "  ?id "  caMcala )" crlf))
)

;@@@ Added by 14anu23 23/06/2014
;A restless wind .
;एक निद्रा रहित हवा .  
(defrule restless2
(declare (salience 4801))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 wind|sea|river|lake|ocean) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nixrA_rahiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp      restless2   "  ?id "  nixrA_rahiwa )" crlf))
)

;@@@ Added by 14anu23 23/06/2014
;Slept a restless night .
;एक बेचैन रात सोया.  
(defrule restless3
(declare (salience 4801))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 night|day) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becEna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp      restless3   "  ?id "  becEna )" crlf))
)

;@@@Added by 14anu-ban-01 on 02-08-2014
;66204:He fell into a restless sleep in her arms .
;उसकी बाँहों में उसे नींद आ गाई - बेचैन , अशान्त नींद ।
;124694:The gleaming mirror reflected back into the half - light an unknown face , crumpled with restless sleep .
;कमरे की धुँधली रोशनी में झिलमिलाते आईने पर एक अपरिचित - सा चेहरा उसकी ओर झाँकने लगा - अशान्त निद्रा की सलवटों से भरा चेहरा ।

(defrule restless4
(declare (salience 3000))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 sleep)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp 	restless4 "  ?id "  aSAnwa)" crlf))
)

;@@@Added by 14anu-ban-01 on 02-08-2014
;282514:You move your legs only on occurrence of pain in legs at night which is called restless legs syndrome .
;पैरों में दर्द होने पर ही आप रात में पैर चलाते हैं , जिसे रेस्टलेस लेग्स सिंड्रोम कहा जाता है ।
(defrule restless5
(declare (salience 3000))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 syndrome)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  (+ ?id 1) ?id1 restalesa_legsa_siMdroma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " restless.clp	restless5  "  ?id "  " (+ ?id 1) " " ?id1 "  restalesa_legsa_siMdroma  )" crlf))
)
;----------------------------------DEFAULT RULE--------------------------------------------------------------------------
;@@@Added by 14anu-ban-01 on 02-08-2014
;246272:Patient stays restless and gets tired by just a little hard work .[Karan Singla]
;रोगी बेचैन रहता है और थोड़े ही श्रम से थक जाता है ।[Karan Singla]
;रोगी बेचैन रहता है और थोड़े ही श्रम से थक जाता है ।

(defrule restless6
(declare (salience 0))
(id-root ?id restless)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becEna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restless.clp 	restless6 "  ?id "  becEna )" crlf))
)

