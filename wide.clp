
;@@@ Added by 14anu04 on 16-June-2014
;The bowlers bowled a lot of wide deliveries.
;गेंदबाज ने बहुत सारा अतिरिक्त गेंद फेंका . 
(defrule wide_tmp
(declare (salience 5000))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ball|delivery)
(viSeRya-viSeRaNa ?id1 ?id) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id awirikwa_geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  wide.clp     wide_tmp   "  ?id1 "  " ?id "  awirikwa_geMxa  )" crlf))
)

(defrule wide0
(declare (salience 4700));salience reduced from 4800 to 4700 by 14anu-ban-01.
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective|adjective_comparitive|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide0   "  ?id "  vyApaka )" crlf))
)

(defrule wide1
(declare (salience 4900))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide1   "  ?id "  cOdZA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on 24-07-2014.
;Her wide eyes looked at him without anger,without surprise .[BOJAR]
; फैली - चौड़ी आँखें उसे देख रही थीं - बिना किसी नाराज़गी के , बिना किसी आश्चर्य के ।[BOJAR]
;She sat there in the darkness , although the blackout was down , and stared with wide eyes at the window which showed a shade lighter than the ebony darkness of the room .	[BOJAR]
;वह अँधेरे में बैठी थी , हालाँकि ब्लैक - आउट के परदे नीचे टर्ग थे , और फैली आँखों से खिड़की की ओर देख रही थी , जहाँ कमरे के आबनूसी अँधेरे की अपेक्षा अधिक रोशनी थी ।[BOJAR]

(defrule wide2
(declare (salience 4800))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)  ;added by 14anu-ban-01 on 24-07-14
(id-root ?id1 eye|ear|grin)  ;added by 14anu-ban-01 on 24-07-14
(id-cat_coarse ?id adjective|adjective_comparitive|adjective_superlative) ;Modified id-cat as id-cat_coarse by 14anu-ban-01 on 24-07-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide2   "  ?id "  cOdA )" crlf))
)

;"wide","Adj","1.cOdA"
;This is a wide road.
;--"2.viswqwa"
;A wide audience watched the programme.
;--"3.pahuzca_ke_bAhara_ho{geMxa}"
;'That was a wide ball' the umpire said.


;@@@ Added by 14anu-ban-11 on 3-09-2014
;Cave number two is 2.4 metres long and 1.86 metres wide .(tourism corpus)
;गुफा  नम्बर  दो  2.4  मीटर  लम्बी  और  1.86  मीटर  चौड़ी  है  ।
(defrule wide3
(declare (salience 5100))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide3   "  ?id "  cOdA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-03-2015)
;A wide audience watched the programme.(hinkhoj)
;एक बडी सङ्ख्या मे श्रोतागणो ने कार्यक्रम को देखा . (self)
(defrule wide4
(declare (salience 4701))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 audience) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZI_saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide4  "  ?id "  badZI_saMKyA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-03-2015)
;That was a wide ball the umpire said.(hinkhoj)
;वह एक पहुँच के बाहर गेंद थी  निर्णायक ने कहा . (self)
(defrule wide5
(declare (salience 4702))
(id-root ?id wide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 ball) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca_ke_bAhara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wide.clp 	wide5   "  ?id "  pahuzca_ke_bAhara)" crlf))
)


