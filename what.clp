
(defrule what0
(declare (salience 5000))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya      )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp 	what0   "  ?id "  kyA )" crlf))
)


;What a beautiful scene!
(defrule what1
(declare (salience 4900))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) a )
(id-cat_coarse =(+ ?id 2) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiwanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp 	what1   "  ?id "  kiwanA )" crlf))
)

;$$$ Modified by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14) -- added +2 category as verb
; I did not know what to do.
(defrule what2
(declare (salience 4800))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
(id-cat_coarse =(+ ?id 2) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp 	what2   "  ?id "  kyA )" crlf))
)

;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14)
;What are you reading? 
;But it does not tell us in what direction an object is moving. 
(defrule what3
(declare (salience 4700))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(or (id-cat_coarse =(+ ?id 1) verb)(id-root =(- ?id 1) in))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what3   "  ?id "  kyA )" crlf))
)

;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14)
;What?
(defrule what4 
(declare (salience 4600))
(id-root 1 what)
?mng <-(meaning_to_be_decided 1)
(id-right_punctuation  1 PUNCT-QuestionMark)
=>
(retract ?mng)
(assert (id-wsd_root_mng 1 kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what4   1    kyA )" crlf))
)

;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14)
;What time is it?
(defrule what5
(declare (salience 4600))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?vi ?id)
(id-cat_coarse =(+ ?vi 1) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what5  "?id "   kyA )" crlf))
)


;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14)
;What kind of music do you like? 
(defrule what6
(declare (salience 4600))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?vi ?id)
(viSeRya-of_saMbanXI  ?vi ?id1)
(id-cat_coarse =(+ ?id1 1) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what6  "?id "   kyA )" crlf))
)


;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (18-03-14)
;It does not matter what Ted does.
;yaha mahawwvapUrNa_nahIM hE ki tEda kyA karawA hE.
(defrule what7
(declare (salience 4500))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?k ?id)
(kriyA-vAkyakarma ?kri ?k)
(root-verbchunk-tam-chunkids ? ? does_not_0 $? ?kri)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what7   "  ?id "  kyA )" crlf))
)


;@@@ Added by Shirisha Manju ,Suggested by Chaitanya Sir (14-01-14)
;Let him tell what he can.
;I spent what little time I had with my family.
(defrule what8
(declare (salience 100))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what8   "  ?id "  jo )" crlf))
)
;@@@ Added by Anita--24.5.2014 [By mail] 
;What rotten luck! 
;कितनी बुरी किस्मत !
(defrule what9
(declare (salience 4550))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiwanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what9   "  ?id "  kiwanA )" crlf))
)

;"what","Conj","1.kyA"
;"what","Pron","1.kyA"
;What are you reading.
;

;@@@ Added by 14anu-ban-11 on (13-11-2014)
;Some reviews have expressed the opinion that more research is needed to determine what effect the phytoestrogens in soybeans may have on infants.(agriculture)
;कुछ समीक्षा में राय व्यक्त की है कि अधिक अनुसंधान की जरूरत है निर्धारित करने कि क्या प्रभाव phytoestrogens में सोयाबीन का पड़ सकता है  शिशुओं पर  (manual)
(defrule what10
(declare (salience 4000))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-root ?id1 effect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what10   "  ?id "  kyA )" crlf))
)

;@@@ Added by 14anu24
;What if the tribunal needs more information ?
;तो क्या यदि न्यायाधिकरण को अधिक सूचना की जरूरत होती है? 
(defrule what11
(declare (salience 5000))
(id-root ?id what)
(id-word =(+ ?id 1) if)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wo_kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what11   "  ?id " wo_kyA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (24-11-2014)
;For small and far objects that can not be resolved by telescopes, much of what we know comes from the study of their albedos.(wiki) 
;जो छोटी और दूर वस्तु दूरबीनों के द्वारा हल नहीं की जा सकती है, हम क्या जानते हैं  उनके albedos के अध्ययन से आता है .(manual) 
(defrule what12
(declare (salience 400))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 know)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what12   "  ?id "  kyA )" crlf))
)



;@@@ Added by 14anu-ban-11 on (23-03-2015)
;What a dope he is. [merriam-webster]
;वह  कितना  मूर्ख  है . (self)    
(defrule what14
(declare (salience 5001))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-root ?id1 dope)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiwanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what14   "  ?id "  kiwanA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (10-04-2015)
;What defence did you use in that last game? (cald)
;उस आखिरी खेल में आपने कौनसी चाल का  उपयोग किया ?    [self]
(defrule what15
(declare (salience 5002))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-root ?id1 defence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOnasI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what15   "  ?id "  kOnasI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;What were you consorting about? (oald)
;आप किस विषय के बारे में बातचीत कर रहे थे? [manual]
(defrule what16
(declare (salience 5003))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 consort)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp      what16   "  ?id "  kisa)" crlf))
)

;@@@ Added by Rajini (17-10-16)
;What about business? Have you given it a thought?(rapidex)
;vyApAra ke bAre meM kyA kahawe ho? isake bAre meM socA hE?
(defrule what17
(declare (salience 4000))
(id-root ?id what)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) about)
(id-cat_coarse =(+ ?id 2) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA_kahawe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  what.clp     what17   "  ?id "  kyA_kahawe_ho )" crlf))
)



;------------------------------ Removed Rules ---------------------
; what3
;	if -1 word is tell then kyA
;	Note : contradictory examples : You tell what to do.    -- kyA
;					Let him tell what he can. -- jo
; what5
;	if category is 'conjunction' then 'kyA'
;
; what6
;	if category is 'pronoun' then 'kyA'
;
; what4
;	if praSnAwmaka_vAkya and  first position then 'kyA'
;	Note : contradictory examples : What?   -- kyA
;					What he says is irrelevant.  -- jo
