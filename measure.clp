;$$$ Modified by 14anu-ban-08 on (05-09-2014)
;@@@ Added By Nandini (17-12-13)
;Measure out 250 grams of flour and sift it into a large mixing bowl.
(defrule measure0
(declare (salience 170))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)          ;Added Relation by 14anu-ban-08
(id-word ?id1 out)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mApa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " measure.clp	measure0  "  ?id "  " ?id1 "  mApa  )" crlf))
)

;@@@ Added By Nandini (17-12-13)
;His answers are always well measured.
(defrule measure1
(declare (salience 110))
(id-root ?id measure)
(id-root ?id1 answer)
(kriyA-subject ?id ?id1)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id napA_wulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure1   "  ?id "  napA_wulA )" crlf))
)

;@@@ Added By Nandini (17-12-13)
;She could never measure up to her mother's expectations.
(defrule measure2
(declare (salience 150))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-to_saMbanXI  ?id ?id2)
(id-root ?id2 expectation)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Azkalana_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " measure.clp	measure2  "  ?id "  " ?id1 "  Azkalana_kara  )" crlf))
)

;He measured the flour into the bowl.
;usane katore meM AtA nApA.
(defrule measure3
(declare (salience 100))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure3   "  ?id "  nApa )" crlf))
)

;"measure","N","1.mApa"
;It is advisable to ascertain the correct measure of every thing purchased.
(defrule measure4
(declare (salience 50))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure4   "  ?id "  nApa )" crlf))
)
;==========================default-rule===========
;@@@ Added by Nandini (17-12-13)
;She replied in a measured tone to his threat.
;usane usakI XamakI ko eka napA-wulA AvAja meM javAba xiyA.
(defrule measure5
(declare (salience 50))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure5   "  ?id "  nApa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (05-09-2014)
;To measure lengths beyond these ranges, we make use of some special indirect methods.  [NCERT]
;इन परिसरों से बाहर की लम्बाइयों को मापने के लिए हमें कुछ परोक्ष विधियों का सहारा लेना होता है.
(defrule measure6
(declare (salience 100))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 length|angle|diameter|interval)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure6   "  ?id "  mApa )" crlf))
)

;@@@ Added by 14anu21 on 23.06.2014
;These measures are aimed at closing the gap between rich and poor.
;ये नाप अमीर और दीन के बीच अन्तर कम करने में लक्षित किए गये हैं . (Translation before adding rule)
;ये उपाय अमीर और दीन के बीच अन्तर कम करने में लक्षित किए गये हैं .
(defrule measure06
(declare (salience 60))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id measures)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure06   "  ?id "  upAya )" crlf))
)


;@@@ Added by 14anu-ban-08 on (05-09-2014)
;If d is the diameter of the planet and α the angular size of the planet (the angle subtended by d at the earth), we have α = d/D (2.2) The angle can be measured from the same location on the earth. [NCERT]
;यदि d ग्रह का व्यास और α उसका कोणीय आमाप (d द्वारा पृथ्वी के किसी बिन्दु पर अन्तरित कोण) हो, तो α = d/D (2.2) कोण को, पृथ्वी की उसी अवस्थिति से मापा जा सकता है.
(defrule measure7
(declare (salience 110))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 distance|angle|mass|period|quantity)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure7   "  ?id "  mApa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (06-09-2014)
;The smallest value that can be measured by the measuring instrument is called its least count.   [NCERT]
;किसी मापक यन्त्र द्वारा मापा जा सकने वाला छोटे से छोटा मान उस मापक यन्त्र का अल्पतमाङ्क कहलाता है.
(defrule measure8
(declare (salience 112))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 instrument|method)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure8   "  ?id "  mApa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (06-09-2014)
;Note that the errors have the same units as the quantity to be measured.   [NCERT]
;ध्यान दीजिए, त्रुटियों के भी वही मात्रक हैं जो मापी जाने वाली राशियों के हैं.
(defrule measure9
(declare (salience 113))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 quantity)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure9   "  ?id "  mApa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (06-09-2014)
;The accuracy of a measurement is a measure of how close the measured value is to the true value of the quantity ; Precision tells us to what resolution or limit the quantity is measured.   [NCERT]
;किसी माप की यथार्थता वह मान है जो हमें यह बताता है कि किसी राशि का मापित मान, उसके वास्तविक मान के कितना निकट है जबकि परिशुद्धता यह बताती है कि वह राशि किस विभेदन या सीमा तक मापी गई है.
(defrule measure10
(declare (salience 114))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 accuracy)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure10   "  ?id "  mApa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-10-2014)
;This relationship allows a gas to be used to measure temperature in a constant volume gas thermometer. [NCERT CORPUS]
;yaha saMbaMXa kisI niyawa Ayawana gEsa wApamApI meM wApa mApana ke lie gEsa ke upayoga ko svIkAra karawe hEM. [NCERT CORPUS]
;yaha saMbaMXa kisI niyawa Ayawana gEsa wApamApI meM wApa mApane ke lie gEsa ke upayoga ko svIkAra karawe hEM. [Self]

(defrule measure11
(declare (salience 113))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive ?id1 ?id)
(kriyA-object  ?id ?id2)
(id-root ?id2 temperature)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure11   "  ?id "  mApa )" crlf))
)

;;@@@ Added by 14anu23 24/06/2014
;Preventive measures include segregation of healthy animals and keeping them out of infected fields .
;इस रोग से बचाव के उपाय हैं : स्वस्थ जानवरों को अलग कर देना चाहिए तथा उनको रोग प्रभावित क्षेत्रों से दूर रखना .
(defrule measure12
(declare (salience 150))
(id-root ?id measure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  measure.clp 	measure12   "  ?id "  upAya )" crlf))
)

;----------------------------- Removed rules ----------
;measure0
; 	if word  measured and categorty adjective  then napA

;measure1
; 	if word  measured and categorty adjective  then  napA-wulA
	;His answers are always well measured.

;------------previous-examples---------
;"measure","V","1.nApanA"
;He measured the lenght of the room.
;--"2.nirNaya_karanA/pawA_lagAnA"
;Can one really measure the ability of a candidate in a ten minute interview?
;--"3.nApa_kA_honA"
;The lenght of the trouser measures 41 inches.
;--"4.nApa_wola_karanA"
;She never measures the effect of what she is going to say.

;-----------------Additional examples-----------
;He walked down the corridor with measured steps.
;It is advisable to ascertain the correct measure of every thing purchased.
;She never measures the effect of what she is going to say.
;The lenght of the trouser measures 41 inches.
;Can one really measure the ability of a candidate in a ten minute interview?
;He measured the length of the room.
;His answers are always well measured.
;Measure out 250 grams of flour and sift it into a large mixing bowl.
;She measured the shoe against the footprint, but it was smaller.
;There is no way of measuring the damage done to morale.
;This machine measures your heart rate.
;Let us measure it.
