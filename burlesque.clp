;@@@Added by 14anu-ban-02(20-04-2015)
;The book is a burlesque of Victorian society.[mw]
;पुस्तक विक्टोरिया कालीन समाज का एक कारटून है . [self]
(defrule burlesque3
(declare (salience 4900))
(id-word ?id burlesque)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-viSeRaNa  ?id1 ?)	;need sentences to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kAratUna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  burlesque.clp 	burlesque3   "  ?id "  kAratUna )" crlf))
)
;-------------------------default_rules---------------------------------------
(defrule burlesque0
(declare (salience 0))	       ;salience reduce to 0 from 5000 by 14anu-ban-02(20-04-2015)
(id-root ?id burlesque)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parihAsya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burlesque.clp 	burlesque0   "  ?id "  parihAsya )" crlf))
)

;"burlesque","Adj","1.parihAsya/wamASe_kA"
;Burlesque theater
;
;$$$Modified by 14anu-ban-02(20-04-2015)
;root fact is changed into word fact and id-wsd_root_mng --> id-wsd_word_mng
;Burke was a burlesque writer.[burlesque1]
;बर्क परिहास लेखक थी . [self]
(defrule burlesque1
(declare (salience 0))	;	salience reduce to 0 from 4900 by 14anu-ban-02(20-04-2015)
(id-word ?id burlesque)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id parihAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  burlesque.clp 	burlesque1   "  ?id "  parihAsa )" crlf))
)

;"burlesque","N","1.parihAsa"
;Burke was a burlesque writer.
;
(defrule burlesque2
(declare (salience 0))	;	salience reduce to 0 from 4800 by 14anu-ban-02(20-04-2015)
(id-root ?id burlesque)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parihAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burlesque.clp 	burlesque2   "  ?id "  parihAsa_kara )" crlf))
)

;"burlesque","VT","1.parihAsa_karanA"
;Some folks burlesqued the Prime Minister's words && ridiculed him.
;
