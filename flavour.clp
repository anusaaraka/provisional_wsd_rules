
;"flavouring","N","1.svAxiRta banAne ke liye milAyA gayA paxArWa"
;Essences are added as flavouring in many sweets.
(defrule flavour0
(declare (salience 5000))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flavouring )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id svAxiRta_banAne_ke_liye_milAyA_gayA_paxArWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  flavour.clp  	flavour0   "  ?id "  svAxiRta_banAne_ke_liye_milAyA_gayA_paxArWa )" crlf))
)

;"flavoured","Adj","1.svAxiRta yA sugaMXiwa kiyA huA"
;This milk is flavoured with chocolate.
(defrule flavour1
(declare (salience 4900))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flavoured )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta_yA_sugaMXiwa_kiyA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour1   "  ?id "  svAxiRta_yA_sugaMXiwa_kiyA_huA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-03-2015)
;The film retains much of the book's exotic flavour. [OALD]
;सिनेमा पुस्तक के आकर्षक प्रभाव को लगभग बनाए रखता है . 		[manual]

(defrule flavour4
(declare (salience 4801))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 book)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(assert  (id-wsd_viBakwi   ?id  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour4   "  ?id "  praBAva)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  flavour.clp 	flavour4    "  ?id " ko )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-03-2015)
;Foreign visitors help to give a truly international flavour to the occasion.[OALD]
;विदेशी दर्शक इस अवसर को  सचमुच एक अन्तर्राष्ट्रीय रंग  देने की कोशिश करते हैं .	[manual] 

(defrule flavour5
(declare (salience 4802))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 international)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour5   "  ?id "  raMga )" crlf))
)


;@@@ Added by 14anu-ban-05 on (09-03-2015)
;The following extract gives a flavour of the poet’s later works.[OALD]
;निम्नलिखित पद्द कवि की  आनेवाली रचना की जानकारी देता है . 		[manual]

(defrule flavour6
(declare (salience 4803))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 works)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour6   "  ?id "  jAnakArI )" crlf))
)


;@@@ Added by 14anu-ban-05 on (09-03-2015)
;I have tried to convey something of the flavour of the argument. [OALD]
;मैंने इस तर्क के  भाव के बारे में कुछ संप्रेषित करने की कोशिश की है.		[manual]
(defrule flavour7
(declare (salience 4803))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 argument)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour7   "  ?id "  BAva )" crlf))
)

;Note-'jAyakA' is another suggestion for the sentence 'I have tried to convey something of the flavour of the argument.' 

;@@@ Added by 14anu-ban-05 on (09-03-2015)
;This yogurt comes in ten different flavours. [OALD]
;यह दही दस विभिन्न  प्रकार के जायकों में आता है. [manual]

(defrule flavour8
(declare (salience 4803))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 different|many|special|distinguish)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAyakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour8   "  ?id "  jAyakA )" crlf))
)	

;@@@ Added by 14anu-ban-05 on (09-03-2015)
;This city has a distinct flavour of times gone by.[hinkhoj]
;इस शहर में बीते हुये समय की एक अलग झलक है.	[manual]

(defrule flavour9
(declare (salience 4803))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 time)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour9   "  ?id "  Jalaka )" crlf))
)

;------------------------ Default Rules ----------------------

;"flavour","N","1.svAxa"
;Food in this dish has a superb flavour.
(defrule flavour2
(declare (salience 4800))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour2   "  ?id "  svAxa )" crlf))
)

(defrule flavour3
(declare (salience 4700))
(id-root ?id flavour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flavour.clp 	flavour3   "  ?id "  svAxiRta_banA )" crlf))
)

;default_sense && category=verb	KAsa_svAxa_banA	0
;"flavour","V","1.KAsa_svAxa_banAnA"
;She flavoured the rice-dish with mint.
;
;"flavour","N","1.svAxa"
;Food in this dish has a superb flavour.
;--"2.puta"
;This city has a distinct flavour of times gone by.
;

