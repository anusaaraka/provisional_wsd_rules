;@@@ Added by 14anu-ban-05 on (03-04-2015)
;Run this sentence on parser 2 of multiple parse
;Empty houses quickly take on a forlorn look.[OALD]
;खाली मकान  जल्द ही सूना दिखने लगता है. [MANUAL]

(defrule forlorn2
(declare (salience 5001))
(id-root ?id forlorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 look)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forlorn.clp 	forlorn2   "  ?id "  sUnA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (03-04-2015)
;Run this sentence on parser4 of multi-parse
;His father smiled weakly in a forlorn attempt to reassure him that everything was all right. 	[OALD]
;उसके पिता उसको आश्वासन देने के लिये  कि सब-कुछ ठीक है एक क्षीण प्रयास में  फीके ढंग से मुस्कराया . 		[MANUAL]

(defrule forlorn3
(declare (salience 5001))
(id-root ?id forlorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 attempt)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRINa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forlorn.clp 	forlorn3   "  ?id "  kRINa )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule forlorn0
(declare (salience 5000))
(id-root ?id forlorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAyUsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forlorn.clp 	forlorn0   "  ?id "  mAyUsa )" crlf))
)

(defrule forlorn1
(declare (salience 4900))
(id-root ?id forlorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAcAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forlorn.clp 	forlorn1   "  ?id "  lAcAra )" crlf))
)

;"forlorn","Adj","1.lAcAra"
;A forlorn orphan was begging on the street.
;--"2.EsI_ASA_jo_pUrI_na_ho_sake"
;I made a forlorn attempt to shed weight.
;--"3.akelA
;The hermits prefer to live in a forelorn place.
;
;
