;@@@Added by 14anu-ban-02(05-03-2015)
;Sentence: He was avid for more information.[oald]	;run sentence on parser no. 3
;Translation: वह अधिक सूचना के लिए उत्सुक था . [manual]
(defrule avid0 
(declare (salience 0)) 
(id-root ?id avid) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id uwsuka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  avid.clp  avid0  "  ?id "  uwsuka )" crlf)) 
) 
;@@@Added by 14anu-ban-02(05-03-2015)
;She has taken an avid interest in the project.[oald]
;उसने परियोजना में अधिक रूचि ली .[self] 
(defrule avid1 
(declare (salience 100)) 
(id-root ?id avid) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aXika)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  avid.clp  avid1  "  ?id "  aXika )" crlf)) 
) 
