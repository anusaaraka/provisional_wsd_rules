
(defrule melt0
(declare (salience 5000))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id molten )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id piGalA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  melt.clp  	melt0   "  ?id "  piGalA_huA )" crlf))
)

;"molten","Adj","1.piGalA huA"
;The molten lava flew down the ridges like water.
;
(defrule melt1
(declare (salience 4900))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id melting )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id piGalawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  melt.clp  	melt1   "  ?id "  piGalawA_huA )" crlf))
)

;"melting","Adj","1.piGalawA_huA"
;I could see the metal melting.
;
;
(defrule melt2
(declare (salience 4800))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piGalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  melt.clp 	melt2   "  ?id "  piGalA )" crlf))
)

(defrule melt3
(declare (salience 4700))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piGala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  melt.clp 	melt3   "  ?id "  piGala )" crlf))
)

;"melt","V","1.piGalAnA[piGalanA]"
;Eat the ice-cream, don't wait for it to melt.

;@@@ Added by 14anu20 on 05.07.2014
;She melted into the background.
;वह पृष्ठभूमि में धीरे धीरे लुप्त हो गई . 
(defrule melt4
(declare (salience 4800))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-into_saMbanXI  ?id ?id1)
(id-root =(+ ?id 1) into)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) meM_XIre_XIre_lupwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " melt.clp  melt4  "  ?id "  " (+ ?id 1) "   meM_XIre_XIre_lupwa_ho_jA)" crlf))
)


;@@@ Added by 14anu20 on 05.07.2014.
;The crowd melted away.[OLD].
;भीड गायब हो गई . 
(defrule melt5
(declare (salience 4800))
(id-root ?id melt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga  ?id ?id1)
(kriyA-subject  ?id ?id2)
(id-root ?id1 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gAyaba_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " melt.clp  melt5  "  ?id "  " ?id1 "   gAyaba_ho_jA)" crlf))
)

;
