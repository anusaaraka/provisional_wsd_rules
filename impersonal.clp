
(defrule impersonal0
(declare (salience 5000))
(id-root ?id impersonal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id a-vEyakwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impersonal.clp 	impersonal0   "  ?id "  a-vEyakwika )" crlf))
)

;$$$ Modified by 14anu-ban-06 (01-04-2015)
;He always tried to keep the discussions impersonal so that no one would be offended. (cambridge)
;उसने हमेशा चर्चाओं को निरपेक्ष रखने का प्रयास किया जिससे कि कोई भी अप्रसन्न न हो . (manual)
(defrule impersonal1
(declare (salience 5100))       ;salience changed from '4900' by 14anu-ban-06 (01-04-2015)
(id-root ?id impersonal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)  ;added by 14anu-ban-06 (01-04-2015)
(id-root ?id1 discussion)       ;added by 14anu-ban-06 (01-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirapekRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impersonal.clp 	impersonal1   "  ?id "  nirapekRa )" crlf))
)

;"impersonal","Adj","1.nirapekRa"
;Lets keep the discussion impersonal so as not to en bored] anyone.
;
;
