;$$$ Modified by 14anu-ban-02(01-08-2014)
;modified (viSeRya-viSeRaNa  ?id ?id1) to (viSeRya-viSeRaNa  ?id ? ) and added (viSeRya-of_saMbanXI ?id ? )
;The elastic behavior of materials plays an important role in engineering design.
;aBiyAMwrikI dijZAina meM xravyoM ke prawyAsWa vyavahAra kI ahama BUmikA howI hE.
;It is very important to know the behavior of the materials under various kinds of load from the context of engineering design.
;aBiyAMwrikI dijZAina ke saMxarBa meM viBinna prakAra ke lodoM ke lie xravyoM ke vyavahAra ko jAnanA bahuwa mahawwvapUrNa howA hE.
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 15/03/2014
;The recognition of concepts of dimensions which guide the description of physical behavior is of basic importance as only those physical quantities can be added or subtracted which have the same dimensions.[ncert]
;विमाओं की सङ्कल्पना की स्वीकृति, जो भौतिक व्यवहार के वर्णन में मार्गदर्शन करती है, अपना एक आधारिक महत्व रखती है क्योङ्कि इसके अनुसार केवल वही भौतिक राशियाँ सङ्कलित या व्यवकलित की जा सकती हैं जिनकी विमाएँ समान हैं.
(defrule behaviour2
(declare (salience 3000))
(id-root ?id behaviour)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa  ?id ? )(viSeRya-of_saMbanXI ?id ? ))
;(id-word ?id1 physical) ; commented by 14anu-ban-02(1.08.14)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  behaviour.clp 	behaviour2   "  ?id "  vyavahAra )" crlf))
)


;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_behaviour1
(declare (salience 3000))
(id-root ?id behaviour)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-word ?id1 physical)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " behaviour.clp   sub_samA_behaviour1   "   ?id " vyavahAra )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_behaviour1
(declare (salience 3000))
(id-root ?id behaviour)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id ?id1)
(id-word ?id1 physical)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " behaviour.clp   obj_samA_behaviour1   "   ?id " vyavahAra )" crlf))
)

;@@@Added by 14anu-ban-02(04-02-2015)
;We begin with a description of a bar magnet and its behavior in an external magnetic field.[NCERT 12_05]
;इस अध्याय में हम एक छड चुम्बक और एक बाह्य चुम्बकीय क्षेत्र में इसके व्यवहार के वर्णन से प्रारम्भ करेंगे.[NCERT]
(defrule behaviour3
(declare (salience 3000))
(id-root ?id behaviour)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 field)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(assert (id-wsd_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  behaviour.clp 	behaviour3   "  ?id "  vyavahAra )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  behaviour.clp 	behaviour3   "  ?id " kA)" crlf)
)
)

;----------------------- Default Rules -----------------

;(added by Darpan Baweja : NIT allahabad)
;(Most of us tend to blame others for our own bad behavior )
(defrule behaviour0
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id behaviour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barwAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  behaviour.clp 	behaviour0   "  ?id "  barwAva )" crlf))
)

