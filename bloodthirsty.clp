;@@@Added by 14anu-ban-02(31-03-2015)
;It’s a bloodthirsty tale of murder and revenge.[oald]
;यह खून और प्रतिशोध का खूँखार किस्सा है . [self]
(defrule bloodthirsty1 
(declare (salience 100)) 
(id-root ?id bloodthirsty) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 tale)  
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KUzKAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodthirsty.clp  bloodthirsty1  "  ?id "  KUzKAra )" crlf)) 
) 

;------------------default_rule-----------------------------------------

;@@@Added by 14anu-ban-02(31-03-2015)
;Sentence: We are not a bloodthirsty people.[oald]
;Translation: 	हम खून के प्यासे लोग नहीं हैं . [self]
(defrule bloodthirsty0 
(declare (salience 0)) 
(id-root ?id bloodthirsty) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KUna_kA_pyAsA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodthirsty.clp  bloodthirsty0  "  ?id "  KUna_kA_pyAsA )" crlf)) 
) 
