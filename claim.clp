;@@@ Added by 14anu-ban-03 (26-02-2015)   ;working on parser no.- 2
;The victim of the accident made claims for damages. [hinkhoj]
;दुर्घटना के शिकार ने क्षति के लिए मुआवजे की माङ्ग की. [self]
(defrule claim2
(declare (salience 100))  
(id-root ?id claim)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 damage)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muAvajZe_kI_mAMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  claim.clp 	claim2   "  ?id "  muAvajZe_kI_mAMga )" crlf))
)


;@@@ Added by 14anu-ban-03 (26-02-2015)   ;working on parser no.- 2
;There are some unimportant matters in the newspaper that doesn't claim attention. [hinkhoj]
;समाचारपत्र में कुछ महत्वहीन विषय हैं जो ध्यान के पात्र नहीं होते है . [self]
(defrule claim3
(declare (salience 200))  
(id-root ?id claim)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 attention)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAwra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  claim.clp 	claim3   "  ?id "  ke_pAwra_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (27-02-2015)
;The car crash claimed three lives. [oald]
;गाडी धमाका तीन जिंदगियो की मृत्यु का कारण बना . [manual]
(defrule claim4
(declare (salience 300))  
(id-root ?id claim)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 life)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mqwyu_kA_kAraNa_bana))
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  claim.clp 	claim4   "  ?id1 " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  claim.clp 	claim4   "  ?id "  mqwyu_kA_kAraNa_bana )" crlf))
)


;------------------------ Default Rules ----------------------
;"claim","N","1.xAvA"
;Despite her claims of innocence,she was found guilty.
(defrule claim0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (26-02-2015)
(id-root ?id claim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  claim.clp 	claim0   "  ?id "  xAvA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (26-02-2015)
;Today, religion is just one of many social affiliations that claim people's allegiance. [coca]
;आजकल ,धर्म बहुत से  सामाजिक सहबद्धताओं  में से एक है जो लोगो की निष्ठा का दावा करता है. [self]
(defrule claim1
(declare (salience 00))    ;salience reduced by 14anu-ban-03 (26-02-2015)
(id-root ?id claim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))  ;added by 14anu-ban-03 (26-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  claim.clp 	claim1  "  ?id " kA )" crlf) ;added by 14anu-ban-03 (26-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  claim.clp 	claim1   "  ?id "  xAvA_kara )" crlf))
)

;default_sense && category=noun	xAvA	0
;"claim","N","1.xAvA"
;Despite her claims of innocence,she was found guilty.
;--"2.muAvajZe_kI_mAMga"
;The victim of the accident made claims for damages.
;--"3.jZora_xekara_kahanA"
;She made no claims that she was the best cook.
;



