;@@@ Added by 14anu-ban-08 on (09-09-2014)
;This example justifies the idea to retain one more extra digit (than the number of digits in the least precise measurement) in intermediate steps of the complex multi-step calculations in order to avoid additional errors in the process of rounding off the numbers.  [NCERT]
;उपरोक्त उदाहरण, जटिल बहुपदी परिकलन के मध्यवर्ती पदों में (कम से कम परिशुद्ध माप में अङ्कों की सङ्ख्या की अपेक्षा) एक अतिरिक्त अङ्क रखने की धारणा को न्यायसङ्गत ठहराता है, जिससे कि सङ्ख्याओं की पूर्णाङ्कन प्रक्रिया में अतिरिक्त त्रुटि से बचा जा सके.
(defrule least0
(declare (salience 0))
(id-root ?id least)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_se_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  least.clp 	least0   "  ?id "  kama_se_kama )" crlf))
)

;@@@ Added by 14anu-ban-08 on (09-09-2014)
;From the results obtained it is clear that among the three bodies the sphere has the greatest and the ring has the least velocity of the center of mass at the bottom of the inclined plane.   [NCERT]
;प्राप्त परिणामों से यह स्पष्ट है कि नत तल की तली में पहुँचने पर तीनों पिण्डों में गोले के द्रव्यमान केन्द्र का वेग सबसे अधिक और वलय के द्रव्यमान केन्द्र का वेग सबसे कम होगा.
(defrule least1
(declare (salience 100))
(id-root ?id least)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 velocity|figure|area|precise)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  least.clp 	least1   "  ?id "  sabase_kama )" crlf))
)

;Added By 14anu-ban-08 on (09-09-2014)
;Thus solids are least compressible whereas gases are most compressible. [NCERT]
;इस प्रकार ठोस पदार्थ सबसे कम संपीड्य होते हैं जबकि गैसें सबसे अधिक संपीड्य होती हैं.
;(defrule least2
;(declare (salience 101))
;(id-root ?id least)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaKa ?id1 ?id)
;(id-word ?id1 compressible)
;(id-cat_coarse ?id adverb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sabase_kama))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  least.clp 	least2   "  ?id "  sabase_kama )" crlf))
;)
