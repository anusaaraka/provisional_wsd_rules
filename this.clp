


;commented by 14anu-ban-07, for isa root word is 'yaha' and with the addition of 'se' postposition yaha will become 'इन '
;Added by Meena(9.9.09)
;Does this shirt match these trousers ? 
;क्या यह कमीज इन पतलून से मेल खाती है?  
;(defrule these0
;(declare (salience 5000))
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id these)
;(id-root  ?id1  trousers|scissors)
;(viSeRya-det_viSeRaNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id isa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  this.clp      these0   "  ?id "  isa )" crlf))
;)

;Added by Meena(9.9.09)
;
(defrule these1
(declare (salience 5000))
;(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id these)
(id-root  ?id1  trousers|scissors)
(subject-subject_samAnAXikaraNa ?id ?id1)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  this.clp      these1   "  ?id "  yaha )" crlf))
)



(defrule this0
(declare (salience 5000))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) all)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this0   "  ?id "  yaha )" crlf))
)
;Modified yaga- as yaha -- by manju

;commented by 14anu-ban-07, for isa root word is 'yaha'
;In Physics too, the term 'energy' is related to work in this sense, but as said above the term 'work' itself is defined much more precisely. (ncert)
;भौतिकी में भी ऊर्जा कार्य से इसी प्रकार सम्बन्धित है परन्तु जैसा ऊपर बताया गया है शब्द कार्य को और अधिक परिशुद्ध रूप से परिभाषित करते हैं.(ncert)
;(defrule this1
;(declare (salience 4900))
;(id-root ?id this)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) very|sense) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id isI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this1   "  ?id "  isI )" crlf))
;)

; I met you in this very university

;commented by 14anu-ban-07, for isa root word is 'yaha'

;(defrule this2
;(declare (salience 4800))
;(id-root ?id this)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) time)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id isa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this2   "  ?id "  isa )" crlf))
;)



;Added by Meena(4.11.09) (salience should be higher than this4)
;We thank you all for coming this evening . 
(defrule this3
(declare (salience 4800))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 evening|morning)
(viSeRya-det_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp      this3   "  ?id "  Aja )" crlf))
)




; This time the crop is good.
(defrule this4
(declare (salience 4700))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this4   "  ?id "  yaha )" crlf))
)

; This book is good.
(defrule this5
(declare (salience 4600))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this5   "  ?id "  yaha )" crlf))
)

;This is a book.
(defrule this6
(declare (salience 4500))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this6   "  ?id "  yaha )" crlf))
)

(defrule this7
(declare (salience 4400))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iwanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this7   "  ?id "  iwanA )" crlf))
)

;"this","Adv","1.iwanA"
;The plant is about this high.
;
;
;"this","Adv","1.iwanA"
;The plant is about this high.
;
(defrule this8
(declare (salience 4300))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this8   "  ?id "  yaha )" crlf))
)

;"this","Det","1.yaha[isa`]"
;--"2.yaha"
;This book is for Ram.
;
(defrule this9
(declare (salience 4200))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this9   "  ?id "  yaha )" crlf))
)

;@@@ Added by 14anu26 [14-06-14]
;Do not eat like this.
;इस तरह खाना मत खाइए .
(defrule this10
(declare (salience 5000))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
(kriyA-like_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  isa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this10  "  ?id " isa   )" crlf))
)



;@@@ Added by 14anu-ban-07 (19-11-2014)
;Dick Morris and Karl Rove have said that this would be used in the general election campaign. (mail)
;डिक मॊरस और कॉर्ल रोव्व ने कहा कि आम चुनाव अभियान में  इसका उपयोग किया जाएगा . (manual)
(defrule this12
(declare (salience 5000))
(id-root ?id this)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id)
(id-tam_type ?kri passive)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaha))
(assert (id-wsd_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  this.clp     this12  "  ?id " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this12   "  ?id "  yaha )" crlf))
)

;$$$Modified by 14anu-ban-07,(31-01-2015)
;You can talk to your son ' s teacher about this as well .
;आप अपने बेटे के अध्यापक से भी इस बारे में बात कर सकते हैं .
;@@@ Added by avni(14anu11)
(defrule this13
(declare (salience 5000))
(id-root ?id this)
;(id-root ?id1 as)
;(id-root ?id2 well)
(id-root =(+ ?id 1) as)
(id-root =(+ ?id 2) well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 is_bAre_me))
(assert (id-wsd_root_mng ?id yaha)) ; meaning changed from is_bAre_me to yaha by 14anu-ban-07,(31-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  this.clp 	this13   "  ?id "  yaha )" crlf))
)

;"this","Pron","1.yaha"
;Out of the four books, give this to Ram.
;
