;@@@ Added by 14anu-ban-01 on (06-02-2015)
;It would be a useful exercise for you to say the speech aloud several times.[cald]  
;बार-बार जोर से भाषण बोलना आप के लिए एक उपयोगी अभ्यास  होगा .[self]
(defrule several2
(declare (salience 4900))
(id-root ?id several)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) time)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) bAra_bAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " several.clp 	several2  "  ?id "  "=(+ ?id 1)"      bAra_bAra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-02-2015)
;Several more people than usual came to the meeting.[oald]  
;सामान्य की अपेक्षा और अधिक लोग बैठक में आए .[self]
(defrule several3
(declare (salience 4900))
(id-root ?id several)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) more)
(viSeRya-viSeRaNa  ? =(+ ?id 1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  several.clp 	several3   "  ?id "  Ora )" crlf))
)

;----------------------- Default Rules -----------------------

;"several","Det","1.aneka"
;Several people were killed in bomb explosion.
(defrule several0
(declare (salience 5000))
(id-root ?id several)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aneka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  several.clp 	several0   "  ?id "  aneka )" crlf))
)

;"several","Pron","1.kaI"
;Several of the paintings were destroyed in the fire.
(defrule several1
(declare (salience 4900))
(id-root ?id several)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  several.clp 	several1   "  ?id "  kaI )" crlf))
)

