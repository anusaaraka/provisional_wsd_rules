;@@@ Added by 14anu-ban-05 on (21-02-2015)
;There are three archs on the two facades and entrance of the prayer hall which are jagged as per the style of Shahjahan . [tourism]
;prArWanA-kakRa meM xo aliMxa Ora muKAra para wIna meharAba hEM jo SAhajahAz kI SElI ke anurUpa xAzwexAra hEM .		[manual]

(defrule facade1
(declare (salience 101))
(id-root ?id facade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aliMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  facade.clp  	facade1   "  ?id "  aliMxa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-02-2015)
;He kept his hostility hidden behind a friendly facade.		[cald]
;उसने अपनी दुश्मनी एक स्नेहशील मुखौटे के पीछे छिपा रखा है. 	[manual]

(defrule facade2
(declare (salience 102))
(id-root ?id facade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id muKOtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  facade.clp  	facade2   "  ?id "  muKOtA )" crlf))
)

;------------------------ Default Rules ----------------------


;@@@ Added by 14anu-ban-05 on (21-02-2015)
;They seem happy together, but it’s all a facade. [OALD]
;वे एक साथ खुश लग रहे हैं, लेकिन यह सब एक दिखावा है.        [manual]

(defrule facade0
(declare (salience 100))
(id-root ?id facade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xiKAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  facade.clp    facade0   "  ?id "  xiKAvA )" crlf))
)


