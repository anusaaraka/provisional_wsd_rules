
;@@@ Added by Manasa-Gurukul-Arsha sodh sansthan 08-07-14
;Bethany dropped her chopsticks and opened the fortune cookies.(coca)
;beWeni ne apane Copasticks PeMka kar apni PorcyUna kukIs KolI.
(defrule fortune0
(declare (salience 500))
(id-root ?id fortune)
?mng<-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 cookie|biscuit|chocolate) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Porcyuna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  "  ?*prov_dir* " fortune.clp fortune0" ?id " Porcyuna) " crlf))
)

;@@@ Added by Manasa-Gurukul-Arsha sodh sansthan 08-07-14
;She can tell your fortune by looking at the lines on your hand.
;वह  आपके हाथ की रेखाओं को देखकर आपका भाग्य बता सकती है।
(defrule fortune1
(id-root ?id fortune)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAgya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  "  ?*prov_dir* " fortune.clp fortune1 " ?id " BAgya) " crlf))
)

;@@@ Added by 14anu-ban-05 on (12-02-2015)
;I have had the good fortune to work with some brilliant directors.[OALD]
;मुझे कुछ प्रतिभाशाली निर्देशकों के साथ काम करने का सौभाग्य मिला है.		[manual]

(defrule fortune2
(declare (salience 501))
(id-root ?id fortune)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) good)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) sOBAgya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fortune.clp  fortune2  "  ?id "  " (- ?id 1) "  sOBAgya  )" crlf))
)


;@@@ Added by 14anu-ban-05 on (12-02-2015)
;He made a fortune in real estate.[OALD]
;उन्होंने अचल संपत्ति से धन  कमाया . [manual]

(defrule fortune3
(declare (salience 502))
(id-root ?id fortune)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 make|cost|spend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  "  ?*prov_dir* " fortune.clp   fortune3 " ?id " Xana) " crlf))
)

;@@@ Added by 14anu-ban-05 on (12-02-2015)
;She is hoping her US debut will be the first step on the road to fame and fortune. [oald]
;वह उम्मीद कर रही है कि उसका यूएस में प्रथम प्रदर्शन प्रसिद्धि और  सफलता के मार्ग पर पहला कदम रहेगा .	[manual]

(defrule fortune5
(declare (salience 505))
(id-root ?id fortune)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPalawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  "  ?*prov_dir* " fortune.clp   fortune5 " ?id " saPalawA) " crlf))
)


