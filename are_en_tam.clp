;$$$ Modified by Soma (22-11-14)
; passive statement brought in its position from print(removed 'passive' fact from action part and added in rule part)
;They are born in Patna.
(defrule are_en_tam0
(declare (salience 5000))
(id-TAM ?id are_en)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id born) ; modified ?id1 to ?id by Soma
(id-tam_type ?id passive) ;added by Soma
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id are_en yA_hE))
(assert (root_id-TAM-vachan ?id are_en p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp  	are_en_tam0  "  ?id "  yA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam0  "  ?id " are_en p )" crlf)
)
)

;$$$ Modified by Soma (22-11-14)
; passive statement brought in its position from print (removed 'passive' fact from action part and added in rule part)
(defrule are_en_tam1
(declare (salience 4900))
(id-TAM ?id are_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id need) ; modified ?id1 to ?id by Soma
(id-tam_type ?id passive) ;added by Soma
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id are_en hE))
(assert (root_id-TAM-vachan ?id are_en p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp  	are_en_tam1  "  ?id "  hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam1  "  ?id " are_en p )" crlf)
))




;$$$ Modified by Soma (22-11-14) 
;$$$ Modified by 14anu-ban-02  (17-11-2014)
;Water proofing agents on the other hand are added to create a large angle of contact between the water and fibers.[ncert]
;पानी तथा रेशों के बीच सम्पर्क कोण बडा करने के लिए पानी में जल सहकारक को मिलाया जाता है.[ncert]
;Added by Meena(20.02.10)
;There must be another sort of rules according to which sentences are composed. 
(defrule are_en_tam_default ; the rule has been declared default by Soma
(declare (salience 4900))
(id-TAM ?id are_en)
?mng <-(meaning_to_be_decided ?id)
(id-tam_type ?id passive) ;added by Soma
;(id-root ?id1 sentence|music); Commented this fact and modified as below. 
;(id-root ?id compose|grow|add); $$$ Modified by Roja (25-12-13) Suggested by Chaitanya Sir.Ex: Potatoes are grown mainly in Uttar Pradesh.   ;'add' is added in the list by 14anu-ban-02(17-11-2014) ;commented by Soma
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id are_en yA_jAwA_hE))
(assert (root_id-TAM-vachan ?id are_en p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp    are_en_tam_default  "  ?id "  yA_jAwA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam_default  "  ?id " are_en p )" crlf)
)
)




;Commented out by Soma (22-11-14)
;-H_tam_mng changed from wA_hE to yA_jAwA_hE(Meena 17.4.10)
;Added by Meena(4.3.10)
;Such flowers are found mainly in Europe . 
;(defrule are_en_tam3
;(declare (salience 5000))
;(id-TAM ?id are_en)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id find)
;=>
;(retract ?mng)
;(assert (id-E_tam-H_tam_mng ?id are_en yA_jAwA_hE))
;(assert (root_id-TAM-vachan ?id are_en p))
;(assert (id-tam_type ?id passive))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp    are_en_tam3  "  ?id " yA_jAwA_hE )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam3  "  ?id " are_en p )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-tam_type  " ?*prov_dir* "  are_en_tam.clp    are_en_tam3  "  ?id " passive )" crlf))
;)

;$$$Modified by 14anu-ban-02(06-02-2016)
;Applicants for posts are called for interview.[sd_verified]
;paxoM ke lie prArWI sAkRAwkAra ke lie bulAe gaye hEM.[sd_verified]
;$$$ Modified by Soma (22-11-14)
; passive statement brought in its position from print (removed 'passive' fact from action part and added in rule part)
; Commented out till we come across any suitable example for the rule -- by Soma
(defrule are_en_tam4
(declare (salience 5000))	;salience increased to 5000 from 4800 by 14anu-ban-02(06-03-2016)
(id-TAM ?id are_en)
?mng <-(meaning_to_be_decided ?id)
(id-tam_type ?id passive) ;added by Soma
(id-root ?id call)	;Added by 14anu-ban-02(06-03-2016)
(kriyA-for_saMbanXI  ?id ?id2)	;Added by 14anu-ban-02(06-03-2016)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id are_en yA_gayA_hE))
(assert (root_id-TAM-vachan ?id are_en p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp  	are_en_tam4  "  ?id "  yA_gayA_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam4  "  ?id " are_en p )" crlf)
)
)


;$$$ Modified by 14anu-ban-03 (20-03-2015)
;In Rutherford's nuclear model of the atom, the entire positive charge and most of the mass of the atom are concentrated in the nucleus with the electrons some distance away. [ncert]
;raxaraPorda ke paramANu ke nABikIya moYdala meM, paramANu kA kula XanAveSa waWA isakA aXikAMSa xravyamAna paramANu ke bahuwa Cote se Ayawana meM safkeMxriwa howA hE jise nABika kahawe hEM waWA ilektroYna isase kuCa xUra howe hEM. [ncert]
; @@@ Added by Soma (22-11-14)
(defrule are_en_tam5
(declare (salience 5000))
(id-TAM ?id are_en)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id term|recover|concentrate)  ;added 'concentrate' by 14anu-ban-03 (20-03-2015)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id are_en wA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp    are_en_tam5  "  ?id "  wA_hE )" crlf))
)

;@@@ Added by Rajini(21-06-2016)
;We were in the old morgue, where the records are kept .
; In other words, he knows where the skeletons are kept .
;Next stop: the main library, where current phone books are kept .
;In Kaladhungi there is museum of Jim Corbett Sahib where some important things related to his private life are kept. 
(defrule are_en_tam6
 (declare (salience 5000))       
 (id-TAM ?id are_en)
 ?mng <-(meaning_to_be_decided ?id)
 (id-tam_type ?id passive) 
 (id-root ?id keep)      
 (kriyA-aXikaraNavAcI ?id ?)
 (id-right_punctuation ?id PUNCT-Dot)
  =>
 (retract ?mng)
 (assert (id-E_tam-H_tam_mng ?id are_en yA_gayA_hE))
 (assert (root_id-TAM-vachan ?id are_en p)) 
 (if ?*debug_flag* then
  (printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  are_en_tam.clp   are_en_tam6  "  ?id "  yA_gayA_hE )" crlf)
  (printout wsd_fp "(dir_name-file_name-rule_name-root_id-TAM-vachan  " ?*prov_dir* "  are_en_tam.clp    are_en_tam6  "  ?id " are_en p )" crlf)
 )
 )

