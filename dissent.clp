;@@@ Added by 14anu-ban-04 (28-02-2015)
;When the time came to approve the proposal, there were one or two voices of dissent.              [cald]
;जब प्रस्ताव को स्वीकृति देने का समय आया,तब वहाँ विरोध की एक या दो आवाजें थीं .                                          [self]
(defrule dissent2
(declare (salience 5010))
(id-root ?id dissent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissent.clp 	dissent2   "  ?id "  viroXa )" crlf))
)

;He expressed his dissent in a contrary opinion.                            [hinkhoj]

(defrule dissent0
(declare (salience 5000))
(id-root ?id dissent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asahamawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissent.clp 	dissent0   "  ?id "  asahamawi )" crlf))
)

;"dissent","N","1.asahamawi"
;The republics expressed their dissent from joining the Union.
;

;$$$ Modified by 14anu-ban-04 (28-02-2015)               -------changed meaning from 'mawaBexa_ho'  to 'asahamawa_ho'
;Only two ministers dissented from the official view.                      [oald]
;आधिकारिक विचार से सिर्फ दो मंत्री असहमत हुए .                                       [self]
(defrule dissent1
(declare (salience 4900))
(id-root ?id dissent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asahamawa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dissent.clp 	dissent1   "  ?id "  asahamawa_ho )" crlf))
)

;"dissent","VT","1.mawaBexa_honA"
;The Soviet republics dissented from the Union.
;
