;@@@ Added by 14anu-ban-03 (06-02-2015)
;The climax of his political career. [oald]
;उसके राजनैतिक कैरियर का शिखर . [manual]
(defrule climax0
(declare (salience 00))
(id-root ?id climax)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiKara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  climax.clp 	climax0   "  ?id "  SiKara )" crlf))
)


;@@@ Added by 14anu-ban-03 (06-02-2015)
;The climax of that movie was not good. [hinkhoj]
;उस फिल्म की चरमावस्था अच्छी नहीं थी . . [manual]
(defrule climax1
(declare (salience 500))
(id-root ?id climax)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 movie|play|novel)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caramAvasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  climax.clp 	climax1   "  ?id " caramAvasWA )" crlf))
)



