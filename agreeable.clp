;@@@Added by 14anu-ban-02(18-02-2015)
;Do you think they will be agreeable to our proposal ?[oald]
;क्या आपको लगता हैं कि वे हमारे प्रस्ताव से सहमत होंगे? [self]
(defrule agreeable1 
(declare (salience 100)) 
(id-root ?id agreeable) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 proposal|republican|american)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sahamawa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agreeable.clp  agreeable1  "  ?id "  sahamawa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(18-02-2015)
;The deal must be agreeable to both sides.[oald]
;सौदा दोनों पक्षों से स्वीकार्य होना चाहिये.[self]
(defrule agreeable2 
(declare (salience 100)) 
(id-root ?id agreeable) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 side)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id svIkArya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agreeable.clp  agreeable2  "  ?id "  svIkArya )" crlf)) 
)
 
;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(18-02-2015)
;Sentence: We spent a most agreeable day together.[oald]
;Translation: हमने साथ में एक सबसे अधिक सुखद दिन बिताया .[self] 
(defrule agreeable0
(declare (salience 0))
(id-root ?id agreeable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agreeable.clp  agreeable0  "  ?id "  suKaxa )" crlf))
)

