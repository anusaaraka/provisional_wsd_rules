;@@@ Added by  14anu-ban-01 on (10-04-2015)
;The stairs will collapse on removing this stanchion.[self: with reference to cald]
;सीढियाँ यह आधार हटा देने पर ढह जाएँगी . [self]
(defrule stanchion1
(declare (salience 1000))
(id-root ?id stanchion)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id2 collapse)
(kriyA-on_saMbanXI  ?id2 ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXAra/teka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stanchion.clp 	stanchion1   "  ?id "  AXAra/teka )" crlf))
)

;@@@ Added by  14anu-ban-01 on (10-04-2015)
;His 18-yard shot hit the stanchion.[self: with reference to cald]
;उसका 18-गज़ शॉट गोलपोस्ट तक पहुंचा .[self] 
(defrule stanchion2
(declare (salience 1000))
(id-root ?id stanchion)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id2 shot)
(kriyA-subject  ?id1 ?id2)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golaposta/gola_kA_KamBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stanchion.clp 	stanchion2   "  ?id "  golaposta/gola_kA_KamBA )" crlf))
)

;-------------------------------------- Default Rules -----------------------------------------

;@@@ Added by  14anu-ban-01 on (10-04-2015)
;He prised Simon's fingers loose from the stanchion.[collins dictionary]
;उसने सिमोन की उङ्गलियाँ खम्भे से  बलपूर्वक अलग कीं . [self]
(defrule stanchion0
(declare (salience 0))
(id-root ?id stanchion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KamBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stanchion.clp        stanchion0   "  ?id "  KamBA )" crlf))
)

