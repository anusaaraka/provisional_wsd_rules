;@@@Added by 14anu-ban-02(24-03-2015)
;The medical prognosis was bleak.[oald]
;चिकित्सीय अग्रसूचना निराशाजनक थी . [self]
(defrule bleak1 
(declare (salience 100)) 
(id-root ?id bleak) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)	;need sentences to restrict the rule
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nirASAjanaka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bleak.clp  bleak1  "  ?id "  nirASAjanaka )" crlf)) 
) 


;--------------------default_rule---------
;@@@Added by 14anu-ban-02(24-03-2015)
;Sentence: They faced a financially bleak Christmas.[oald]
;Translation:उन्होंने आर्थिक रूप से ठण्डे बडे दिन का सामना किया .[self] 
(defrule bleak0 
(declare (salience 0)) 
(id-root ?id bleak) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id TaNdA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bleak.clp  bleak0  "  ?id "  TaNdA )" crlf)) 
) 
