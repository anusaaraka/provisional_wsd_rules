;@@@Added by 14anu-ban-08 (18-02-2015)
;Table 2.6 gives the range and order of lengths and sizes of some of these objects.  [NCERT]
;सारणी 2.3 में इनमें से कुछ पिंडों की आमापों और दूरियों की कोटि और परास दिए गए हैं.  [NCERT]
;सारणी 2.6 में इनमें से कुछ पिंडों की आमापों और दूरियों की कोटि और परास दिए गए हैं. [self]
(defrule length1
(declare (salience 10))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 range)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  length.clp 	length1   "  ?id "  xUrI )" crlf))
)

;@@@Added by 14anu-ban-08 (18-02-2015)
;We also use certain special length units for short and large lengths.  [NCERT]
;अत्यन्त सूक्ष्म और बहुत बडी दूरियों के मापन के लिए हम लम्बाई के कुछ विशिष्ट मात्रक भी प्रयोग में लाते हैं.  [NCERT]
;अत्यन्त सूक्ष्म और लम्बी दूरियों के मापन के लिए हम लम्बाई के कुछ विशिष्ट मात्रक भी प्रयोग में लाते हैं.  [self]
(defrule length2
(declare (salience 20))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 large)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  length.clp 	length2   "  ?id "  xUrI )" crlf))
)

;@@@Added by 14anu-ban-08 (26-02-2015)
;- In view of the tremendous accuracy in time measurement, the SI unit of length has been expressed in terms the path length light travels in certain interval of time (1 / 299, 792, 458 of a second) (Table 2.1).  [NCERT]
;समय मापन की इस आश्चर्यजनक यथार्थता को ध्यान में रखकर ही लम्बाई के SI मात्रक को प्रकाश द्वारा (1/299, 792, 458) सेकण्ड में चलित दूरी के रूप में व्यक्त किया गया है (सारणी 2.1).  [NCERT]
(defrule length3
(declare (salience 200))
(Domain physics)
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id1 light)
(id-root ?id2 path)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 caliwa_xUrI_ke_rUpa_meM))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " length.clp	length3  "  ?id "  " ?id2 "  caliwa_xUrI_ke_rUpa_meM  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " length.clp	length3  "  ?id "  " ?id2 " physics  )" crlf))
)

;@@@Added by 14anu-ban-08 (05-03-2015)
;I stripped off bark to use for kindling before laying on the split lengths of cedar. [oald]
;मैंने देवदार की काटी हुई लकड़ियों पर रखने से पहले आग जलाने में प्रयोग करने के लिए वृक्ष की छाल को छील लिया .[self] 
(defrule length4
(declare (salience 120))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(viSeRya-of_saMbanXI ?id ?id2)
(id-root ?id1 split)
(id-root ?id2 cedar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  length.clp   length4   "  ?id "  lakadZI )" crlf))
)

;@@@Added by 14anu-ban-08 (05-03-2015)
;Each class is 45 minutes in length.  [oald]
;हर कक्षा 45 मिनट की अवधि पर हैं.  [self]
(defrule length5
(declare (salience 120))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id)(preposition ?id2))
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 minute|hour|second)
(id-root ?id2 in)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " length.clp	length5  "  ?id "  " ?id2 "  avaXi )" crlf))
)

;@@@Added by 14anu-ban-08 (05-03-2015)
;The horse won by two clear lengths.  [oald]
;घोड़ा दो घोड़ों की दूरी से साफ जीत गया.  [self]
(defrule length6
(declare (salience 20))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-by_saMbanXI ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id1 win)
(id-root ?id2 horse)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  length.clp 	length6   "  ?id "  xUrI )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-08 (18-02-2015)
;The length of the table was 5 feet.  [hindkhoj]
;मेज की लम्बाई 5 फुट थी.    [self]
(defrule length0
(declare (salience 0))
(id-root ?id length)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  length.clp   length0   "  ?id "  lambAI )" crlf))
)

