;@@@ Added by Anita-07-06-2014
;Their stampede shaped the ridges and valleys upon the mountain sides. [By mail]
;उनकी भगदड़ ने पर्वत के किनारों पर मेड़ और घाटियों का आकार बनाया । 
(defrule ridge02
(declare (salience 5100))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 valleys)
(viSeRya-upon_saMbanXI  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id medZa))
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge02   "  ?id "  medZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  ridge.clp     ridge02   "  ?id1 "  kA )" crlf))
)


;@@@ Added by 14anu-ban-10 on (06-02-2015)  
;The ridge of snow is that line where two sides of mountain meet .[tourism corpus]
;बर्फ़ की रिज वह लाईन होती है जहाँ पर्वत के दो बाजू मिलते हैं ।[tourism corpus]
;If at all one has to walk on the ridge due to some reason then he should walk from the side facing the slope .[tourism corpus]
;यदि किसी कारणवश रिज के ऊपर ही चलना पड़े तो ढलान की ओर मुँह करके बगल से चलना चाहिए ।[tourism corpus]
(defrule ridge2
(declare (salience 5100))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ? )(kriyA-on_saMbanXI  ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rija))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge2   "  ?id "  rija )" crlf))
)
;@@@ Added by 14anu-ban-10 on (06-02-2015)  
;The shape of ridge is very sharp on high mountains .[tourism corpus]
;ऊँचे पर्वतों पर रिज की आकृति बहुत तीखी होती है ।[tourism corpus]
;The slopes on the both sides of snow ridge can be up to 600 to 900 metres .[tourism corpus]
;बर्फ़ की रिज के दोनों ओर की ढलानें 600 से 900 मीटर तक की हो सकती हैं ।[tourism corpus]
(defrule ridge3
(declare (salience 5200))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?  ?id )(samAsa_viSeRya-samAsa_viSeRaNa ?id ? ))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rija))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge3  "  ?id "  rija )" crlf))
)

;@@@ Added by Anita-13-07-2014
;I have seen north-east ridge of Mount Everest.
;मैं माउन्ट एवरेस्ट की उत्तर-पूर्वी पर्वत श्रृंखला को देख चुका हूँ ।
(defrule ridge04
(declare (salience 5200))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parvawa_SranKalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge04   "  ?id "  parvawa_SranKalA )" crlf))
)

;@@@ Added by 14anu-ban-10 on  (12-02-2015)
;Himalaya mountains have many prominent ridges.[hinkhoj]
;हिमालय के पहाड़ों में कई प्रमुख  मेड़ है ।[self]
(defrule ridge4
(declare (salience 5300))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge4  "  ?id "  meDZa )" crlf))
)

;@@@ Added by Anita-13-07-2014
;The ridges on the soles of my boots stopped me from slipping. [oxford learner's dictionary]
;मेरे जूते के तल्ले के उभार मुझे फिसलने से बचाते हैं । 
(defrule ridge5
(declare (salience 5300))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-on_saMbanXI  ?id ?sam)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge5   "  ?id "  uBAra )" crlf))
)

;@@@ Added by Anita-13-07-2014
;We walked along the narrow mountain ridge. [cambridge dictionary]
;हम सकरी पर्वत पट्टी के साथ-साथ चले ।
(defrule ridge6
(declare (salience 5400))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 mountain)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge6   "  ?id "  pattI )" crlf))
)

;------------------------- Default Rules --------------------
;"ridge","N","1.DAlU_tIlA{_yA_pahAdZI}"
;Himalaya mountains have many prominent ridges.
(defrule ridge0
(declare (salience 5000))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU_tIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge0   "  ?id "  DAlU_tIlA )" crlf))
)

;"ridge","V","1.medZa_banAnA"
;A slightly ridged && ploughed field is good for a healthy crop.
(defrule ridge1
(declare (salience 4900))
(id-root ?id ridge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id medZa_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ridge.clp 	ridge1   "  ?id "  medZa_banA )" crlf))
)

