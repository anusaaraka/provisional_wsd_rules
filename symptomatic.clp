;----------------------------DEFAULT RULE------------------------------------
;@@@ Added by 14anu-ban-01 on (07-10-14)
;It is a symptomatic infection.[oald]
;यह एक लक्षणात्मक संक्रमण है.[self]
(defrule symptomatic0
(declare (salience 0))
(id-word ?id symptomatic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lakRaNAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  symptomatic.clp  	symptomatic0  "  ?id " lakRaNAwmaka)" crlf))
)

;----------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (07-10-14)
;These debates are probably symptomatic of cultural changes.[wikipedia-wiki]
;यह विवाद सम्भवतः सांस्कृतिक  परिवर्तन के सूचक हैं.[self]
(defrule symptomatic1
(declare (salience 3000))
(id-word ?id symptomatic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sUcaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  symptomatic.clp  	symptomatic1  "  ?id " sUcaka )" crlf))
)
