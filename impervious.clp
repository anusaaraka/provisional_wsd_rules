;@@@ Added by 14anu-ban-06 (01-04-2015)
;She was impervious to his charms. (OALD)
;वह उसके आकर्षण से अप्रभावित थी . (manual)
(defrule impervious1
(declare (salience 2000))
(id-word ?id impervious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1 charm|pain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apraBAviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impervious.clp 	impervious1   "  ?id "  apraBAviwa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (01-04-2015)
;An impervious rock. (OALD)
;अभेद्य चट्टान . (manual)
(defrule impervious0
(declare (salience 0))
(id-word ?id impervious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBexya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impervious.clp 	impervious0   "  ?id "  aBexya )" crlf))
)


