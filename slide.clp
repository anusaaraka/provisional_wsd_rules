;##############################################################################
;#  Copyright (C) 2014-2015 Akshay Singh (akubuzz@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;$$$ Modified by 14anu-ban-01 on (29-01-2015)
;Example changed:He slid on his back.[collins dictionary]
;वह पीठ के बल गिर/फिसल गया.[self]
;$$$ Modified by Shirisha Manju on 17-11-14 -- corrected meaning 'Pisala_rah' as 'Pisala_raha'
;@@@ Added by 14anu03 on 30-june-14
;He is sliding.
;वह फिसल रहा है.
(defrule slide1
(declare (salience 0))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)	;corrected 'course' to 'coarse'  by 14anu-ban-01 on (29-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Pisala/gira));changed "Pisala_raha" to "Pisala" and added "gira" by 14anu-ban-01 on (29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide1  " ?id "    Pisala/gira  )" crlf));changed "Pisala_raha" to "Pisala" added "gira" by 14anu-ban-01 on (29-01-2015)
)

;$$$ Modified by 14anu-ban-01 on (29-01-2015):Run on parse no.76
;@@@ Added by 14anu03 on 30-june-14
;Children were enjoying slides.
;बच्चे झूले का मजा ले रहे थे .
;बच्चे झूलों का मजा ले रहे थे .[Translation improved by 14anu-ban-01]
(defrule slide2
(declare (salience 1))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun);corrected 'course' to 'coarse'  by 14anu-ban-01 on (29-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUlA));changed "JUle" to "JUlA"  by 14anu-ban-01 on (29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide2  " ?id "   JUlA  )" crlf));changed "JUle" to "JUlA"  by 14anu-ban-01 on (29-01-2015)
)

;$$$ Modified by 14anu-ban-01 on (29-01-2015)
;@@@ Added by 14anu03 on 30-june-14
;Would she slide back into that lost mode?
;क्या वह वापिस उस खोया हुआ चलन के अन्दर जाएगी? 
;क्या वह वापिस उसी गुमसुम अवस्था में चली जाएगी?[Translation improved by 14anu-ban-01 on (29-01-2015)]
(defrule slide3
(declare (salience 5500))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) back);modified by 14anu-ban-01 on (29-01-2015)
;(test (=(+ ?id 1) ?id1))commented by 14anu-ban-01 on (29-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) vApisa_calA_jA));changed "vApisa_jA" to "vApisa_calA_jA" by 14anu-ban-01 on (29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  slide.clp     slide3   "  ?id "  " (+ ?id 1) " vApisa_calA_jA  )" crlf));changed "vApisa_jA" to "vApisa_calA_jA" by 14anu-ban-01 on (29-01-2015)
)

;$$$ Modified by 14anu-ban-01 on (29-01-2015)
;@@@ Added by 14anu03 on 30-june-14
;He gave presentations with the help of slides.
;उसने स्लाइड की मदद से प्रस्तुतियाँ दी.
(defrule slide4
(declare (salience 1000));salience increased from 1 to 1000 by 14anu-ban-01
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)	;corrected 'course' to 'coarse'  by 14anu-ban-01 on (29-01-2015)
(viSeRya-of_saMbanXI  ?id1 ?id)	;added by 14anu-ban-01 on (29-01-2015)
;(kriyA-with_saMbanXI ?id1 ?id)	;commented by 14anu-ban-01 on (29-01-2015)
;(id-word ?id1 gave|given)	;commented by 14anu-ban-01 on (29-01-2015)
(id-word ?id1 help|group|set|number|series|use|folder|couple|combination|kind|parts);added by 14anu-ban-01 on (29-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id slAida))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide4  " ?id "   slAida  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (29-01-2015)
;NOTE:later this rule can be restriced for fear|tension etc. when any counter example is found.
;@@@ Added by 14anu03 on 30-june-14
;He sensed her tension slide away.
;उसने उसके तनाव को दूर जाता हुअा अनुभव किया .
;उसने उसके तनाव को दूर होता हुअा महसूस किया .[Translation improved by 14anu-ban-01 on (29-01-2015)]
(defrule slide5
(declare (salience 5500))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) away);modified by 14anu-ban-01 (29-01-2015)
;(test (=(+ ?id 1) ?id1))commented by 14anu-ban-01 (29-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) xUra_howA_huA));changed meaning from "xUra_jAwA_huA" to "xUra_howA_huA"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  slide.clp     slide5   "  ?id "  " (+ ?id 1) "  xUra_howA_huA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (17-03-2015)
;Her car went into a slide. (oald)
;दराजें आसानी से अन्दर और बाहर सरकती हैं . (self)
(defrule slide6
(declare (salience 100))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 drawer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  saraka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide6  " ?id "   saraka)" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-03-2015)
;A stock market slide. (oald)
;स्टॉक बजार मे गिरावट  . (self)
(defrule slide7
(declare (salience 250))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 market)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  girAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide7  " ?id "   girAvata )" crlf))
)


;@@@ Added by 14anu-ban-11 on (17-03-2015)
;The drawers slide in and out easily.(oald)
;उसकी गाडी फिसलनी जगह में गई . (self)
(defrule slide8
(declare (salience 260))
(id-root ?id slide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id1 go)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PisalanI_jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " slide.clp  slide8  " ?id "  PisalanI_jagaha)" crlf))
)
