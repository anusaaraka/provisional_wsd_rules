(defrule divine0
(declare (salience 0));salience reduced by 14anu-ban-04 on 20-08-2014
(id-root ?id divine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ISvarIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divine.clp 	divine0   "  ?id "  ISvarIya )" crlf))
)

;"divine","Adj","1.ISvarIya/svargIya"
;Divine worship
;her pies were simply divine
;--"2.paviwra"
;The custom of killing the divine king upon any serious failure of his...powers
;--"3.SreRTa"
;Divine judgment
;The divine strength of Achilles
;
(defrule divine1
(declare (salience 0));salience reduced by 14anu-ban-04 on 20-08-2014
(id-root ?id divine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaviRyavANI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divine.clp 	divine1   "  ?id "  BaviRyavANI_kara )" crlf))
)

;"divine","V","1.BaviRyavANI_kara"

;@@@ Added by 14-anu-ban-04 on 20-08-2014
;It was in Hastinapur that Muni Suvrat Nath Swami , Bhagwan Parswanath and Bhagwan Mahavir Swami preached in their divine voice .   [tourism-corpus]
;हस्तिनापुर  में  ही  मुनि  सुव्रत  नाथ  स्वामी  ,  भगवान  पार्श्वनाथ  और  भगवान  महावीर  स्वामी  ने  अपनी  दिव्य  वाणी  में  प्रवचन  दिए  ।
;If u want to see the divine figure of Maa Tara then go to Tarapith and worship her with all devotion.        [tourism-corpus]
;माँ  तारा  का  दिव्य  रूप  देखना  हो  ,  तो  तारापीठ  जायें  और  मन  से  माँ  की  पूजा  करें  ।
(defrule divine2
(declare (salience 10))
(id-root ?id divine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xivya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divine.clp 	divine2  "  ?id "  xivya )" crlf))
)

