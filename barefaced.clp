
;@@@Added by 14anu-ban-02(20-03-2015)
;A barefaced challenge for a fight.[mw]
;लड़ाई के लिए एक खुली चुनौती . [self]
(defrule barefaced1 
(declare (salience 100)) 
(id-root ?id barefaced) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 challenge)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KulA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barefaced.clp  barefaced1  "  ?id "  KulA )" crlf)) 
) 

;------------------------ Default Rules ---------------------

;@@@Added by 14anu-ban-02(20-03-2015)
;Sentence: That's a barefaced lie![cald]
;Translation: वह एक सफेद झूठ है! [self]
(defrule barefaced0 
(declare (salience 0)) 
(id-root ?id barefaced) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id saPexa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barefaced.clp  barefaced0  "  ?id "  saPexa )" crlf)) 
) 



