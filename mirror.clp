;$$$Modified by 14anu-ban-08 (09-01-2015)        ;meaning changes from 'prawibiMbiwa' to 'prawixvaMxiwa '
;$$$ Modified by 14anu02 on 17.06.14
;@@@ Added by Nandini
;She saw herself mirrored in the window.
;उसने स्वयं को खिड़की मे प्रतिद्वंदित देखा.
;उसने स्वयं को खिड़की मे प्रतिबिंब देखा. [self]  ;added by 14anu-ban-08 (12-03-2015)
(defrule mirror1
(declare (salience 200))
(id-root ?id mirror)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma ? ?id)	;Added by 14anu02
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibiMba ))	;meaning changed from 'prawibiMbiwa_ho' to 'prawibiMbiwa' by 14anu02                   ;meaning changed from 'prawibiMbiwa' to 'prawixvaMxiwa ' by 14anu-ban-08        ;meaning changed from 'prawixvaMxiwa' to 'prawibiMba' by 14anu-ban-08 (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " mirror.clp 	mirror1   "  ?id "  prawibiMba)" crlf))                                           ;meaning changed from 'prawibiMbiwa' to 'prawixvaMxiwa ' by 14anu-ban-08   ;meaning changed from 'prawixvaMxiwa' to 'prawibiMba' by 14anu-ban-08 (12-03-2015)
)

;$$$Modified by 14anu-ban-08 (09-01-2015)     ;meaning changes from 'prawibiMbiwa_kara' to 'prawixvaMxiwa_kara
;@@@ Added by 14anu02 on 17.06.14
;She mirrored herself in the window.
;उसने स्वयं को खिड़की मे प्रतिद्वंदित किया.
;उसने स्वयं को खिड़की मे  प्रतिबिंब किया.  [self]       ;added by 14anu-ban-08 (12-03-2015)
(defrule mirror2
(declare (salience 100))
(id-root ?id mirror)
?mng <-(meaning_to_be_decided ?id)  
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibiMba_kara))        ;meaning changed from 'prawibiMbiwa_kara' to 'prawixvaMxiwa_kara ' by 14anu-ban-08    ;meaning changed from 'prawixvaMxiwa_kara' to 'prawibiMba_kara' by 14anu-ban-08 (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " mirror.clp 	mirror2   "  ?id "  prawibiMba_kara)" crlf))                                              ;meaning changed from 'prawibiMbiwa_kara' to 'prawixvaMxiwa_kara ' by 14anu-ban-08   ;meaning changed from 'prawixvaMxiwa_kara' to 'prawibiMba_kara' by 14anu-ban-08 (12-03-2015)
)

;===============default-rule==========
;@@@ Added by Nandini
;He looked at his reflection in the mirror.[Cambridge Advanced Learner’s Dictionary]
(defrule mirror0
(declare (salience 50))
(id-root ?id mirror)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarpaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " mirror.clp 	mirror0   "  ?id "  xarpaNa)" crlf))
)
