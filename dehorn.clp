
;@@@ Added by 14anu23 20/06/2014
;Dehorning is commonly practised in milch cattle , especially in large dairy herds .
;ढोरों के आमतौर पर सींग काट दिये जाते हैं , विशेष रूप से ऐसे जानवरों के जो दुग्धालयों में विशाल समूहों में रखे जाते हैं .
;सामान्यतः सींग काटना  दुधारू पशु में प्रचलित है ,विशेष रूप से बड़े दुग्धशाला  झुंडों में|      ;translation corrected by 14anu-ban-04 (08-01-2015)
(defrule dehorn0
(declare (salience 5000))
(id-root ?id dehorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIMga_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dehorn.clp 	dehorn0   "  ?id "  sIMga_kAta )" crlf))
)

;$$$ Modified by 14anu-ban-04 (08-01-2015)       ---correct the meaning from 'kate_huye_sIMga_vAle' to 'kate_hue_sIMga_vAlA'
;@@@ Added by 14anu23 20/06/2014
;Dehorned cattle are easier to handle than those with horns .
;जिन पशुओं के सींग उतार दिये गये होते हैं , उनकी देखभाल उन पशुओं की तुलना में सरल होती है जिनके सींग उतारे नहीं गये होते .
;कटे हुये सींग वाले पशु को सम्भालना अासान होता है सींग वाले पशुओं की अपेक्षा |               ;translation corrected by 14anu-ban-04 (08-01-2015)
(defrule dehorn1
(declare (salience 5000))
(id-word ?id dehorned)                         ;replaced 'id-root' with 'id-word' and 'dehorn' by 'dehorned' by 14anu-ban-04 (02-02-2015)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)                 ;commented by 14anu-ban-04 (02-02-2015)
(id-cat_coarse =(+ ?id 1) noun)                 ;added by 14anu-ban-04 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kate_hue_sIMga_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dehorn.clp 	dehorn1   "  ?id "  kate_hue_sIMga_vAlA )" crlf))
)
