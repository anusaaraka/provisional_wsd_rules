;@@@Added by 14anu-ban-02 (01-04-2015)
;The folds of the curtains were blackened with dirt.[cald]
;पर्दों की चुन्नटें धूल से  काली हो गयीं थीं . [self]
(defrule blacken2
(declare (salience 100))
(id-root ?id blacken)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 dirt)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blacken.clp 	blacken2   "  ?id "  kAlA_ho )" crlf))
)


;@@@Added by 14anu-ban-02 (01-04-2015)
;The financial crash blackened the image of bankers.[cald]
;आर्थिक गिरावट ने बैंक का सञ्चालकों की छवि खराब की . [self]
(defrule blacken3
(declare (salience 100))
(id-root ?id blacken)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blacken.clp 	blacken3   "  ?id "  KarAba_kara )" crlf))
)

;------------------------default_rule ------------------------------

;"blacking","N","1.syAhIr{camadZA_Axi_camakAne_kI_syAhI}"
(defrule blacken0
(declare (salience 5000))
(id-root ?id blacken)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id blacking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id syAhIr))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blacken.clp  	blacken0   "  ?id "  syAhIr )" crlf))
)

(defrule blacken1
(declare (salience 0))	;salience ruduce to 0 from 4900 by 14anu-ban-02 (01-04-2015)
(id-root ?id blacken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blacken.clp 	blacken1   "  ?id "  kAlA_kara )" crlf))
)

