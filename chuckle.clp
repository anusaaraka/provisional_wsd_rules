;@@@ Added by 14anu-ban-03 (19-02-2015)
;She was chuckling as she read the letter. [cald]
;जैसे ही उसने खत पढा वह मुस्कुराई थी. [manual]
(defrule chuckle2
(declare (salience 100))
(id-root ?id chuckle)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa ?id ?id1)
(id-root ?id1 read)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muskurA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chuckle.clp 	chuckle2   "  ?id "  muskurA )" crlf))
)

;------------------------ Default Rules ----------------------

;"chuckle","N","1.xabI_huI_hazsI"
;She gave a chuckle while reading the witty story.
(defrule chuckle0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (19-02-2015)
(id-root ?id chuckle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabI_huI_hazsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chuckle.clp 	chuckle0   "  ?id "  xabI_huI_hazsI )" crlf))
)

;"chuckle","VI","1.ruka_ke_hazsanA"
;She chuckled while reading the witty story.
(defrule chuckle1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (19-02-2015)
(id-root ?id chuckle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruka_ke_hazsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chuckle.clp 	chuckle1   "  ?id "  ruka_ke_hazsa )" crlf))
)

