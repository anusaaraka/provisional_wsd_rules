;@@@ Added by 14anu-ban-01 on (06-04-2015)
;She is never completely sincere in what she says about people. [oald]
;वह लोगों के बारे में जो कहती है उसमें  वह कभी भी पूरी तरह से ईमानदार नहीं  होती  . [self]
(defrule sincere1
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ImAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere1   "  ?id "  ImAnaxAra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;She made a sincere attempt to resolve the problem.[self: with reference to oald]
;उसने समस्या को हल करने का वास्तविक प्रयास किया .  [self]
(defrule sincere2
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 attempt|try|trial)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere2   "  ?id "  vAswavika )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;When they hugged at the first sight,it was sincere and seemly.[COCA]
;जब वे देखते ही गले लग गए, वह हार्दिक और खूबसूरत था . [self]
(defrule sincere3
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 and)
(id-root ?id2 seemly)
(conjunction-components ?id1 ?id ?id2)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hArxika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere3   "  ?id "  hArxika)" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;Please accept our sincere thanks.[oald]
;कृपया हमारा हार्दिक धन्यवाद स्वीकार कीजिए . [self]
(defrule sincere4
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 thanks|gratitude)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hArxika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere4  "  ?id "  hArxika )" crlf))
)


;@@@ Added by 14anu-ban-01 on (06-04-2015)
;The warm, deeply sincere note in her voice melted me.[self: with referece to oald]
;उसकी आवाज के  स्नेही, बेहद खरे भाव ने मुझे पिघला दिया .  [self]
(defrule sincere5
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 note|tone)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere5  "  ?id "   KarA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (06-04-2015)
;His refusal to be sincere about his feelings saddened me.[self:with reference to oald]
;उसके अपनी भावनाओं के बारे में ईमानदार होने के इनकार/अस्वीकार ने मुझे दुःखी कर दिया .  [self]
(defrule sincere6
(declare (salience 1000))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-about_saMbanXI ?id ?)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ImAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp 	sincere6   "  ?id "  ImAnaxAra )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (06-04-2015)
;He seemed sincere enough when he said he wanted to help. [oald]
;वह बिल्कुल निष्कपट प्रतीत हुआ जब उसने कहा कि वह सहायता करना  चाहता है .[self]
(defrule sincere0
(declare (salience 0))
(id-root ?id sincere)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRkapata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sincere.clp  sincere0   "  ?id "  niRkapata )" crlf))
)


