;@@@Added by 14anu-ban-07,(16-03-2015)
;Tributes have been pouring in from all over the world for the famous actor who died yesterday.(cambridge)
;श्रद्धाञ्जलि प्रसिद्ध अभिनेता के लिए पूरे  विश्व  भर  से अा रहीं हैं जो कल मर गये .  (manual)
(defrule tribute0
(id-root ?id tribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SraxXAFjali))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tribute.clp 	tribute0   "  ?id "  SraxXAFjali )" crlf))
)


;@@@Added by 14anu-ban-07,(16-03-2015)
;His recovery is a tribute to the doctors' skill.(oald)
;उसका स्वास्थ्यलाभ डाक्टरों की निपुणता के लिए पुरस्कार है . (manual)
(defrule tribute1
(declare (salience 1000))
(id-root ?id tribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 skill|determination)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id puraskAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tribute.clp 	tribute1   "  ?id "  puraskAra )" crlf))
)

