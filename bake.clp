;@@@ Added by 14anu-ban-02 (13-11-2014)
;Among many uses, soy flour thickens sauces, prevents staling in baked food, and reduces oil absorption during frying.[agriculture]
;बहुत उपयोगों के बीच में,सोये आटे की चटनियाँ गाढी होती है,पके हुए भोजन को बासी होने से रोकता है,और तलने के दौरान तेल के अवशोषण को कम कर देता।[manual]
(defrule bake2
(declare (salience 4900))
(id-root ?id bake)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bake.clp  	bake2   "  ?id " paka )" crlf))
)

;------------------------ Default rules ---------------------------
(defrule bake0
(declare (salience 5000))
(id-root ?id bake)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id baking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id seMkane_kI_kriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bake.clp  	bake0   "  ?id "  seMkane_kI_kriyA )" crlf))
)

;"baking","N","1.seMkane_kI_kriyA"
;Get me a baking tin.
;
(defrule bake1
(declare (salience 0));salience reduced from 4900 to 0 by 14anu-ban-02(13-11-2014)
(id-root ?id bake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sezka))
(assert (kriyA_id-object2_viBakwi ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bake.clp 	bake1   "  ?id "  sezka )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object2_viBakwi   " ?*prov_dir* "  bake.clp      bake1   "  ?id " ke_liye )" crlf)
)

;"bake","VT","1.sezkanA"
;Sita is baking  breads for breakfast.
;
;
