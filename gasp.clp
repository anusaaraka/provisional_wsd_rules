;@@@ Added by 14anu-ban-05 on (16-01-2015)
;When she saw the money hidden in the box she gasped in surprise.	[cambridge]
;जब उसने बक्सा में छिपे हुए पैसे को देखा तब वह आश्चर्य में धक से रह  गई.			[manual]
(defrule gasp1
(declare (salience 101))
(id-root ?id gasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xaka_se_raha_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gasp.clp 	gasp1   "  ?id "  Xaka_se_raha_jA )" crlf))
)

;---------------- Default Rules --------------
;@@@ Added by 14anu-ban-05 on (16-01-2015)
;He came to the surface of the water gasping for air.   [OALD]
;vaha havA ke lie hAzPawA_huA pAnI kI sawaha para AyA.  [MANUAL]
;there is a parser problem in 'gasping'. It runs on parser number3
(defrule gasp0
(declare (salience 100))
(id-root ?id gasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAzPawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gasp.clp     gasp0   "  ?id "  hAzPawA_huA )" crlf))
)

