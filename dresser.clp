;@@@ Added by 14anu-ban-04 (20-02-2015)
;She was always a very stylish dresser.                 [cald]
;वह हमेशा से अत्यन्त सुरुचिपूर्ण ड्रेसर थी .                           [self]
(defrule dresser1
(declare (salience 20))
(id-root ?id dresser)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dresara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dresser.clp 	dresser1   "  ?id "  dresara )" crlf))
)


;@@@ Added by 14anu-ban-04 (20-02-2015)
;There was a small dresser in the corner of the bedroom.                   [oald] 
;शयन कक्ष के कोने में एक छोटी लकड़ी की अल्मारी थी .                                       [self]
(defrule dresser0
(declare (salience 10))
(id-root ?id dresser)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakadI_kI_almArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dresser.clp 	dresser0   "  ?id "   lakadI_kI_almArI )" crlf))
)


