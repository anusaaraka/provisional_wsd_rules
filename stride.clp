
(defrule stride0
(declare (salience 0));salience reduced from 5000 to 0 by 14anu-ban-01 on (21-10-2014)
(id-root ?id stride)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laMbe_kaxama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stride.clp 	stride0   "  ?id "  laMbe_kaxama )" crlf))
)

;"stride","N","1.laMbe_kaxama"
;With each stride he came closer to success.
;Great strides have recently been made towards preserving the environment.
;--"2.laMbe_daga_lekara_calanA"
;A stride in the morning is good for health.
;
(defrule stride1
(declare (salience 4900))
(id-root ?id stride)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laMbe_kaxama_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stride.clp 	stride1   "  ?id "  laMbe_kaxama_raKa )" crlf))
)

;"stride","V","1.laMbe_kaxama_raKanA"
;Ram strode towards the class when he saw the principal coming.
;--"2.lAzGanA"
;Girls should not stride their limits.
;

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;From the sixteenth century onwards, great strides were made in science in Europe.[NCERT corpus]
;सोलहवीं शताब्दी से यूरोप में विज्ञान के क्षेत्र में अत्यधिक प्रगति हुई .[NCERT corpus]
(defrule stride2
(declare (salience 3000))
(id-root ?id stride)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(kriyA-in_saMbanXI ?id1 ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pragawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stride.clp 	stride2   "  ?id "  pragawi )" crlf))
)

;@@@ Added by 14anu-ban-11 on (10-03-2015)
;Girls should not stride their limits.(stride.clp)
;लडकियों को उनकी सीमाएँ नहीं लाङ्घनी चाहिए . (self)
(defrule stride3
(declare (salience 4901))
(id-root ?id stride)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 limit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAMGanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stride.clp 	stride3   "  ?id "  lAMGanA )" crlf))
)


