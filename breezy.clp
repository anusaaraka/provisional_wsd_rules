;@@@Added by 14anu-ban-02(08-04-2015)
;You're very bright and breezy today![oald]
;आज आप  अत्यन्त प्रसन्नचित और प्रफुल्लित हैं! [self]
(defrule breezy1 
(declare (salience 100)) 
(id-root ?id breezy) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id3)
(conjunction-components  ?id2 ?id3 ?id)
(id-root ?id2 and|or)
(id-root ?id3 bright)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prasannaciwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breezy.clp  breezy1  "  ?id "  prasannaciwa )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(08-04-2015)
;Sentence: The breezy east coast.[oald]
;Translation: हवादार पूर्वी समुद्र तट . [self]
(defrule breezy0 
(declare (salience 0)) 
(id-root ?id breezy) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id havAxAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breezy.clp  breezy0  "  ?id "  havAxAra )" crlf)) 
) 
