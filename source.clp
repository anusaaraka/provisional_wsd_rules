;@@@ Added by 14anu-ban-11 on (04-02-2015)
;I have got this information from a reliable source.(hinkhoj) 
;मुझे यह सूचना एक विश्वास योग्य स्रोत से मिली है . (self)
(defrule source0
(declare (salience 100))
(id-root ?id source)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id srowa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  source.clp 	source0   "  ?id "  srowa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-02-2015)
;The source of Ganga is in the Hmalayas.(hinkhoj)
;गङ्गा का उद्गम हिमालय में है . (self)
(defrule source1
(declare (salience 200))
(id-root ?id source)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxgama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  source.clp 	source1   "  ?id "  uxgama )" crlf))
)



