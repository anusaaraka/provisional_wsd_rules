;@@@ Added by 14anu-ban-10 on (14-10-2014)
;Thus, a vector is represented by a bold face, e.g. by A, a, p, q, r, ... x, y, with respective magnitudes denoted by light face A, a, p, q, r, ... x, y.  [ncert corpus]
;isa prakAra eka saxiSa ko hama mote akRara yaWA @A yA @a, @p, @q, @r, ... @x, @y, se vyakwa karawe hEM jabaki inake parimANoM ko kramaSaH hama @A yA @a, @p, @q, @r, ... @x, @y xvArA vyakwa karawe hEM .[ncert corpus]
(defrule respective0
(declare (salience 0000))
(id-root ?id respective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kramaSaH ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respective.clp 	respective0   "  ?id "  kramaSaH)" crlf))
)
;
;@@@ Added by 14anu-ban-10 on (14-10-2014)
;It is much easier to add vectors by combining their respective components. [ncert corpus]
;Binna - Binna saxiSoM ko unake safgawa GatakoM ko milAkara jodanA aXika AsAna howA hE.[ncert corpus]
(defrule respective1
(declare (salience 200))
(id-root ?id respective)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 component)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safgawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respective.clp 	respective1   "  ?id " safgawa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-11-2014)
;When two objects collide, the mutual impulsive forces acting over the collision time Δt cause a change in their respective momenta.[ncert corpus]
;jaba xo piNda safGatta karawe hEM wo safGatta samaya Δ@t meM kAryarawa paraspara AvegI bala, unake paraspara saMvegoM meM parivarwana lAne kA kAraNa howe hEM.[ncert corpus]
(defrule respective2
(declare (salience 300))
(id-root ?id respective)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 momentum)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paraspara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respective.clp 	respective2   "  ?id " paraspara )" crlf))
)
;
