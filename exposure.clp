
;Added by Meena(23.4.11)
;The silver was tarnished by the long exposure to the air. 
(defrule long_exposure_to
(declare (salience 4000))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id exposure)
(id-root =(- ?id 1) long)
(id-root =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(+ ?id 1) meM_bahuwa_samaya_pade_rahanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  exposure.clp  long_exposure_to  "  ?id "  "(- ?id 1) " "(+ ?id 1) "  meM_bahuwa_samaya_pade_rahanA  )" crlf))
)


;@@@ Added by 14anu-ban-04 (10-11-2014)
;Some people (0.6%[26] of the United States population) report that they experience mild to severe allergic reactions to peanut exposure; symptoms can range from watery eyes to anaphylactic shock, which can be fatal if untreated.      [agriculture]
;कुछ लोग (यूनाइटड स्टॆट्स की जनसङ्ख्या का 0.6 %) शिकायत करते हैं कि वे मूँगफली खाने पर मृदु से गंभीर प्रत्यूर्ज प्रतिक्रियाओं का अनुभव करते हैं;लक्षण आँखों से पानी बहने  से लेकर तीव्रगाहिता संबंधी आघात तक हो सकते हैं जो कि अनुपचारित रहें तो  घातक हो सकते हैं |                 [manual]
(defrule exposure1
(declare (salience 100))
(Domain agriculture)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id exposure)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 peanut) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KA))
(assert (make_verbal_noun ?id))
(assert (id-domain_type  ?id agriculture))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exposure.clp  exposure1   "  ?id "  KA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  exposure.clp 	exposure1    "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  exposure.clp 	exposure1   "  ?id "  agriculture )" crlf))
)

