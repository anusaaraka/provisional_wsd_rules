
;Added by sheetal(o9-o9-o9)
(defrule current1
(declare (salience 4950))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp   current1   "  ?id "  varwamAna )" crlf))
)
;To pretend that our program is usable in its current form would be silly .



;@@@ Added by 14anu-ban-03 (20-11-2014)
;An average lightning carries currents of the order of tens of thousands of amperes and at the other extreme, currents in our nerves are in microamperes.[ncert]
;जहाँ एक ओर किसी औसत तडित में हजारों ऐंपियर कोटि की धारा प्रवाहित हो जाती है, वहीं दूसरी ओर हमारी तन्त्रिकाओं से प्रवाहित होने वाली धाराएँ कुछ माइक्रोऐंपियर कोटि की होती हैं.[ncert]
;Just as static charges produce an electric field, the currents or moving charges produce (in addition) a magnetic field, denoted by B( r), again a vector field. [ncert]
;jisa prakAra sWira AveSa vixyuwa kRewra uwpanna karawe hEM, vixyuwa XArAez aWavA gawimAna AveSa (vixyuwa kRewra ke sAWa-sAWa) cumbakIya kRewra uwpanna karawe hEM jise @B(@r) xvArA nirxiRta kiyA jAwA hE waWA yaha BI eka saxiSa kRewra hE.[ncert]
(defrule current3
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 addition|nerve)    ;added 'nerve' by 14anu-ban-03 (04-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current3   "  ?id "  XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (20-11-2014)
;Reversing the direction of the current reverses the orientation of the needle. [ncert]
;yaxi wAra meM XArA kI xiSA viparIwa kara xI jAe wo cumbakIya suI BI GUma kara viparIwa xiSA meM saMreKiwa ho jAwI hE.[ncert]
(defrule current4
(declare (salience 5000))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current4   "  ?id " XArA  )" crlf))
)


;@@@ Added by 14anu-ban-03 (22-11-2014)
;For a steady current I in this conducting rod, we may assume that each mobile carrier has an average drift velocity vd (see Chapter 3).[ncert]
;isa cAlaka Cada meM aparivarwI vixyuwa XArA @I ke lie hama yaha mAna sakawe hEM ki prawyeka gawiSIla vAhaka kA apavAha vega @vd hE (aXyAya 3 xeKie).[ncert]
(defrule current5
(declare (salience 5000))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 steady)   ;added by 14anu-ban-03 (14-03-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current5   "  ?id " vixyuwa  )" crlf))
)

;@@@ Added by 14anu-ban-03 (26-11-2014)
;These currents do not change direction with time. [ncert]
;समय के साथ इन धाराओं की दिशा में परिवर्तन नहीं होता . [ncert]
;Such currents occur naturally in many situations.[ncert]
;ऐसी ही धारा प्रकृति में बहुत-सी स्थितियों में पाई जाती है.[ncert]
(defrule current6
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 change|occur)  ;added 'occur' by 14anu-ban-03 (03-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current6   "  ?id "  XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-12-2014)
;Charges in motion constitute an electric current.[ncert]
;गतिमान आवेश विद्युत धारा का निर्माण करते हैं.[ncert]
(defrule current7
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 electrical|steady)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current7   "  ?id "  vixyuwa_XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-12-2014)
;This is proportional to t for steady current and the quotient is defined to be the current across the area in the forward direction.[ncert]
;स्थायी धारा के लिए यह t के अनुक्रमानुपाती है और भागफल क्षेत्र से होकर अग्रगामी दिशा में प्रवाहित विद्युत धारा को परिभाषित करता है. [ncert]
(defrule current8
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(id-root ?id1 define)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current8   "  ?id "  vixyuwa_XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-12-2014)
;Currents are not always steady and hence more generally, we define the current as follows.[ncert]
;विद्युत धाराएँ सदैव अपरिवर्ती नहीं होतीं, इसलिए अधिक व्यापक रूप में हम विद्युत धारा को निम्न प्रकार से परिभाषित करते हैं.
(defrule current9
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 electrical|steady)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current9   "  ?id "  vixyuwa_XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-12-2014)
;An ampere is defined through magnetic effects of currents that we will study in the following chapter.[ncert]
;एक ऐंपियर को विद्युत धारा के चुम्बकीय प्रभाव द्वारा परिभाषित किया जाता है जिसका हम अगले अनुच्छेद में अध्ययन करेंगे. [ncert]
;An ampere is typically the order of magnitude of currents in domestic appliances. [ncert]
;घरेलू वैद्युत-साधित्रों में प्रवाहित होने वाली प्रतिरूपी विद्युत धारा के परिमाण की कोटि एक ऐंपियर होती है. [ncert]
(defrule current10
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 effect|magnitude)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current10   "  ?id "  vixyuwa_XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-12-2014)
;An average lightning carries currents of the order of tens of thousands of amperes and at the other extreme, currents in our nerves are in microamperes.[ncert]
;जहाँ एक ओर किसी औसत तडित में हजारों ऐंपियर कोटि की धारा प्रवाहित हो जाती है, वहीं दूसरी ओर हमारी तन्त्रिकाओं से प्रवाहित होने वाली धाराएँ कुछ माइक्रोऐंपियर कोटि की होती हैं.[ncert]
(defrule current11
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 order)    
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current11   "  ?id "  XArA )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-12-2014)
;If we consider solid conductors, then of course the atoms are tightly bound to each other so that the current is carried by the negatively charged electrons.[ncert]
;यदि हम ठोस चालक पर विचार करें तो वास्तव में इनमें परमाणु आपस में निकट रूप से, कस कर आबद्ध होते हैं जिसके कारण ऋण आवेशित इलेक्ट्रॉन विद्युत धारा का वहन करते हैं.[ncert]
(defrule current12
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 carry)  
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_XArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current12   "  ?id "  vixyuwa_XArA )" crlf))
)



;@@@Added by 14anu19(26-06-2014)
;He was almost carried away by the current .
;वह प्रवाह से लगभग बहाया गया था .  
(defrule current13
(declare (salience 5000))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-by_saMbanXI  ?id1 ?id)
(id-root ?id1 carry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp   current13   "  ?id "  pravAha )" crlf))
)


;@@@ Added by 14anu-ban-03 (14-03-2015)
;The dominance of PBS affects current developments in apparel manufacturing and employee management for two reasons. [report set 4]
;पीबीएस के प्रभुत्व दो कारणों के लिए पोशाक उत्पादन और कर्मचारी प्रबन्ध में वर्तमान विकासों को प्रभावित करता है . [manual]
(defrule current14
(declare (salience 5000))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current14   "  ?id "  varwamAna )" crlf))
)

;------------------------ Default Rules ----------------------

;"current","Adj","1.varwamAnakAlika"
;Get me a current magazine
(defrule current0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (14-03-2015)
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAnakAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current0   "  ?id "  varwamAnakAlika )" crlf))
)

;"current","N","1.pravAha"
;He had to swim against the current
(defrule current2
(declare (salience 4900))
(id-root ?id current)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  current.clp 	current2   "  ?id "  pravAha )" crlf))
)

;"current","N","1.pravAha"
;He had to swim against the current
;--"2.vixyuwa_pravAha"
;A sudden fluctuation in the current made the lights fuse
;

