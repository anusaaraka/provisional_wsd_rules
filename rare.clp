
;@@@ Added by 14anu-ban-10 on (07-02-2015)
;It is rare to get such a good luck in the final stage .[tourism corpus]
;अंतिम चरण में ऐसा सौभाग्य विरले ही मिलता है ।[tourism corpus]
(defrule rare2
(declare (salience 5100))
(id-root ?id rare)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(kriyA-object  ?id1 ?id2)
(id-root ?id2 luck)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rare.clp 	rare2   "  ?id "  virala)" crlf))
)

;------------------ Default Rules --------------------

(defrule rare0
(declare (salience 5000))
(id-root ?id rare)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurlaBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rare.clp 	rare0   "  ?id "  xurlaBa )" crlf))
)

;"rare","Adj","1.xurlaBa"
;He has a huge collection of rare books in his library. 
(defrule rare1
(declare (salience 4900))
(id-root ?id rare)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurlaBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rare.clp 	rare1   "  ?id "  xurlaBa )" crlf))
)

