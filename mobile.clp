
;@@@ Added by 14anu04 on 17-June-2014
;He bought a new mobile phone.
;उसने एक नया फोन खरीदा . 
(defrule mobile_tmp
(declare (salience 5550))             ;Salience is increased from 5000 to 5550 by 14anu-ban-08 (09-01-2015)       
(id-root ?id mobile)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 phone)
(test (= (+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Pona))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " mobile.clp 	mobile_tmp  "  ?id "  " ?id1 "  Pona )" crlf))
)

;@@@ Added by 14anu01
;Give me your mobile number.
;???? ???? ?????? ????? ????? . 
(defrule mobile2
(declare (salience 5500))
(id-root ?id mobile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) phone|number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mobAila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mobile.clp 	mobile2   "  ?id "  mobAila)" crlf))
)

;@@@ Added by 14anu-ban-08 (25-02-2015)
;We shall assume one kind of mobile carriers as in a conductor (here electrons).  [NCERT]
;We shall assume one kind of mobile carriers as in a metal conductor (here electrons are mobile carriers).  [self]
;हम किसी चालक (जिसमें इलेक्ट्रॉन गतिशील वाहक हैं) की भाँति एक ही प्रकार के गतिशील वाहक मानेंगे.  [NCERT]
;हम किसी धातु सुचालक(जिसमें इलेक्ट्रॉन गतिशील वाहक हैं) की भाँति एक ही प्रकार के गतिशील वाहक मानेंगे.  [self]
(defrule mobile3
(declare (salience 5500))
(id-root ?id mobile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 carrier)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawiSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mobile.clp 	mobile3   "  ?id "  gawiSIla)" crlf))
)


;---------------------- default rules --------------------
;$$$ Modified by 14anu23
;I bought a mobile.
;मैने एक मोबाइल खरीदा.       
(defrule mobile0
(declare (salience 5000))
(id-root ?id mobile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mobAila))         ;meaning changed from  calawA to mobAila on 04/07/14
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mobile.clp 	mobile0   "  ?id "  mobAila )" crlf))
)

(defrule mobile1
(declare (salience 4900))
(id-root ?id mobile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calawA_PirawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mobile.clp 	mobile1   "  ?id "  calawA_PirawA )" crlf))
)

;"mobile","Adj","1.calawA PirawA"
;He was glad to be mobile again.
;
;
