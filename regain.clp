
;@@@ Added by 14anu-ban-05 on (23-04-2015)
;I struggled to regain some dignity. [OALD]
;मैंने  गरिमा  फिर से हासिल करने के लिए संघर्ष किया. 	[MANUAL]
(defrule regain1
(declare (salience 101))
(id-root ?id regain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 dignity)		;more constraints can be added
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pira_se_hAsila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regain.clp 	regain1   "  ?id "  Pira_se_hAsila_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (23-04-2015)
;They finally managed to regain the beach.[OALD]
;वे अन्ततः समुद्रतट  पहुँचने में सफल हुए . [MANUAL]
(defrule regain2
(declare (salience 102))
(id-root ?id regain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regain.clp 	regain2   "  ?id "  pahuzca )" crlf))
)

;-------------------------------------- Default Rules --------------------------------------

;@@@ Added by 14anu-ban-05 on (23-04-2015)
;He was determined to regain what his father had lost.[OALD]
;उसके पिता ने जो खोया था उसे वह पुनः प्राप्त करने का दृढ निश्चय किया    .  [MANUAL]
(defrule regain0
(declare (salience 100))
(id-root ?id regain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id punaH_prApwa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regain.clp   regain0   "  ?id "  punaH_prApwa_kara  )" crlf))
)

