
(defrule ground0
(declare (salience 5000))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) play)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mExAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground0   "  ?id "  mExAna )" crlf))
)
;$$$ Modified by Gourav Sahni (MNNIT ALLAHABAD) on 16.06.2014 email-id:sahni.gourav0123@gmail.com
;We are going to play in the ground.
;हम मैदान में खेलना जा रहे हैं . 
(defrule ground1
(declare (salience 5000))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1  play)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mExAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground1   "  ?id "  mExAna )" crlf))
)
;$$$ Modified by Gourav Sahni (MNNIT ALLAHABAD) on 16.06.2014 email-id:sahni.gourav0123@gmail.com
;He lost his balance and fell to the ground. 
;उसने उसका सन्तुलन खोया और जमीन पर गिरा . 
(defrule ground2
(declare (salience 4800))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZamIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground2   "  ?id "  jZamIna )" crlf))
)

(defrule ground3
(declare (salience 5000));salience changed by Gourav Sahni from 4700 to 5000
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamIna_para_raKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground3   "  ?id "  jamIna_para_raKA )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (08-12-2014)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 16.06.2014 email-id:sahni.gourav0123@gmail.com
;The plane was grounded.
;समतल जमीन पर आ गया था . 
;प्लेन  जमीन पर आ गया था . 	;translation modified by 14anu-ban-05 on (08-12-2014)
(defrule ground4
(declare (salience 5000))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1  plane|aeroplane|aircraft|airbus|airship|crate|airliner|spacecraft)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)		;added by 14anu-ban-05 on (08-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamIna_para_A_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground4   "  ?id "  jamIna_para_A_gayA )" crlf))
)
;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 16.06.2014 email-id:sahni.gourav0123@gmail.com
;He does this on grounds of sex. 
;वह लिङ्ग के आधार पर यह करता है . 
(defrule ground5
(declare (salience 4800))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 age|race|caste|religion|safety|security|income|pay|gender|money|place|region|nation|notion)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  AXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground5   "  ?id "   AXAra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-12-2014)
;At room temperature, most of the hydrogen atoms are in ground state.[NCERT]
;kamare ke wApa para, aXikAMSa hAidrojana paramANu apanI nimnawama avasWA meM rahawe hEM.[NCERT]
(defrule ground6
(declare (salience 5000))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) state)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng  (+ ?id 1) ?id nimnawama_avasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " ground.clp ground6  "  (+ ?id 1) "  " ?id "  nimnawama_avasWA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-12-2014)
;It was ground, polished, set up, and is being used by the Indian Institute of Astrophysics, Bangalore.[NCERT]
;ise GarRiwa kiyA gayA, Pira poYliSa kI gaI Ora vyavasWiwa kiyA gayA waWA aba ise BArawIya Kagola BOwikI saMsWAna, bafgalurU xvArA prayoga kiyA jA rahA hE.[NCERT]
(defrule ground7
(declare (salience 5000))
(Domain physics)
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-subject  ?id ?id1)
(id-root ?id1 it)		
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id GarRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground7   "  ?id "  GarRiwa_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  ground.clp      ground7   "  ?id "  physics )" crlf))
)

;default_sense && category=verb	XarawI_para_raKa	0
;"ground","V","1.XarawI_para_raKanA"
;His bat was grounded
;He ground the stick to avoid falling
;
;LEVEL 
;Headword : ground
;
;Examples --
;
;--"1.jZamIna"
;Ravi lifted Manish off the 'ground'.
;ravi ne manIRa ko 'jZamIna' se uTA liyA.
;The contraption took a few erratic hops but never really got off the ground.
;vaha upakaraNa xo-cAra bAra kuCa uCalA para vAswava meM jZamIna se Upara nahIM uTA. (yAni udZA nahIM)
;--"2.sWAna"  <--jZamIna
;Lakes, rivers && oceans are the dumping ground for industrial wastes.
;wAlAba, naxiyAz Ora samuxra Oxyogika apaSiRta ko PeMkane kA sWAna bana gaye hEM.
;--"3.AXAra" <--warka kI jZamIna
;He won the case on this ground.
;usane isa AXAra para kesa jIwa liyA.
;
;"get off the ground","cAlU_honA"
;The project should get off the ground before the end of this month.
;isa mahIne ke anwa waka projEkta(para kAma) cAlU ho jAnA cAhiye
;
;"gain ground","PElanA{logoM_waka}"
;His views on free software are gaining ground.
;'PrI_sOYPtaveyara' para usake vicAra PEla rahe hEM.
;
;At last the sale 'ground to a halt'.    ???
;AKira sela 'rukI'.
;
;
;sUwra : jZamIna`



;@@@ Added by 14anu-ban-05 on (24-02-2015)
;He was acquitted on the grounds of insufficient evidence.[OALD]
;उसे अपर्याप्त  साक्ष्य के आधार पर बरी कर दिया गया था.			[manual]

(defrule ground8
(declare (salience 5001))
(id-word ?id grounds)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
(viSeRya-of_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert  (id-wsd_viBakwi   ?id1  ke))
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (+ ?id 1) AXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " ground.clp  ground8  "  ?id "  " (+ ?id 1) "  AXAra  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  ground.clp  ground8   "  ?id1 " ke )" crlf))
)



;@@@ Added by 14anu-ban-05 on (24-02-2015)
;We have grounds to believe that you have been lying to us. [cald]
;हमारे पास मानने के लिए आधार हैं कि आप हमें झूठ बोलते रहे हैं .	[manual] 

(defrule ground9
(declare (salience 5002))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)		
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground9   "  ?id "  AXAra)" crlf))
)



;@@@ Added by 14anu-ban-05 on (24-02-2015)
;When the conversation turns to politics he's on familiar ground .[CALD]
;जब वार्तालाप राजनीति की ओर मुखातिब होता है  तब वह  परिचित  क्षेत्र में  होता है .	[manual]

(defrule ground10
(declare (salience 5003))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground10   "  ?id "  kRewra)" crlf))
)

;@@@ Added by 14anu-ban-05 on (24-02-2015)
;I enjoyed her first novel, but I felt in the second she was going over the same ground.	[CALD]
;मैंने उसके पहले उपन्यास का आनंद लिया है, लेकिन  मैंने  दूसरे में महसूस किया कि वह एक ही विषय  पर जा रही थी .		[manual]
;Note-run the sentence on parser number 10 of multiparse.
(defrule ground11
(declare (salience 5004))
(id-root ?id ground)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 go)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ground.clp 	ground11   "  ?id "  viRaya)" crlf))
)





