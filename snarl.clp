
;@@@ Added by 14anu-ban-11 on (23-03-2015)
;She answered with an angry snarl.(oald)
;उसने क्रोधित कर्कश स्वर में  उत्तर दिया . (self)
(defrule snarl2
(declare (salience 5001))
(id-root ?id snarl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 angry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karkaSa_svara_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snarl.clp 	snarl2   "  ?id "  karkaSa_svara_meM)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-03-2015)
;The dog snarled at us.(self)
;कुत्ता हम पर गरजा . [manual]
(defrule snarl3
(declare (salience 4901))
(id-root ?id snarl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 us)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snarl.clp 	snarl3   "  ?id "  garaja)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-03-2015)
;The dog turned with a snarl to attack me. [oald]
;कुत्ता  मुझ पर हमला करने के लिये गुर्राहट के साथ मुडा . (self)
(defrule snarl4
(declare (salience 5002))
(id-root ?id snarl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-root ?id1 turn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gurrAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snarl.clp 	snarl4   "  ?id "  gurrAhata)" crlf))
)



(defrule snarl0
(declare (salience 5000))
(id-root ?id snarl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snarl.clp 	snarl0   "  ?id "  ulaJana )" crlf))
)

;"snarl","N","1.ulaJana/gurrAhata"
(defrule snarl1
(declare (salience 4900))
(id-root ?id snarl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snarl.clp 	snarl1   "  ?id "  ulaJA_xe )" crlf))
)

;"snarl","V","1.ulaJA_xenA/gurrAnA"
