;To be taken care of as mentioned in t-backup.clp
;to83, to82, to80, to75, to74, to72, to69, to61
;To be adde to other verb files:
; to57
; Needed or Not to64 - If go followed by object  , to becomes NULL
;____________________________________

; @@@Added by Soma (26-10-14)
;I want to go home
; मैं घर जाना चाहता हुँ
;Mohan began to feel useless. === -ना
;मोहन ने बेकार महसूस करना शुरु किया.
;She began to take off her clothes. === -ना 
;उसने अपने वस्त्र उतारना  शुरु किया.

(defrule to_default_kridant_nA_1
(declare (salience 1000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
;(kriyA-kriyArWa_kriyA ?mvid ?rid) ; This is not a proper relation - however kept for now (comment by soma(6-9-15))
(kriyA-kqxanwa_karma ?mvid ?rid)
(id-cat_coarse ?rid verb)
(id-root ?mvid begin|cease|choose|continue|dislike|forget|hate|learn|like|love|mean|need|prefer|remember|start|teach|want)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp 	to_default_kridant_nA_1   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_default_kridant_nA_1  "  ?id "  -)" crlf)
)
)

;$$$ Modified by Soma (26-10-14) -- verb list added; but commented out on (12-9-15)
;$$$ Modified by Sukhada (14-08-14) -- added below sentences with translations
;7000 people turned out to see him. === -ने के लिये #### Raising predicate
;7000 लोग उसे देखने के लिए  आये.
;Another patient was caused to hear her small son. === -ने के लिये #### ECM verb passive 
;अन्य मरीज को उसके छोटे बेटे से बात करने के लिए  राजी किया गया था.
;Chinese person shall be caused to be removed there-from to the country from whence he came. === -ने के लिये #### ECM verb passive ;
;चीनी व्यक्ति को  जहाँ से वह आया है उस देश को जाने के लिये मज़बूर किया जाना चाहिये.
;I should not have been caused to live so long. === -ने के लिये #### ECM verb passive 
;मुझे इतने लंबे समय जीवित रहने के लिए मज़बूर नहीं किया जाना चाहिये.
;National holidays were declared to mark the wedding. === -ने के लिये #### ECM verb passive ;
;विवाहोत्सव यादगार बनाने के लिए राष्ट्रिय अवकाश घोषित किए गये थे.
;xeSaBakwa avakASa_kA xina vivAhowsava ko cihniwa karane ke lie GoRiwa kie gaye We.
;The agent agreed to pay her $ 800 for a night. === -ने के लिये #### Subject control verb
;एजेंट उसे एक रात के लिये  $800 देने के लिये मान गया.
;The instructor persuaded Mary to take that course
(defrule V_to_nA_ke_liye_2
;(declare (salience 1000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
;(kriyA-kriyArWa_kriyA ?mvid ?rid)
(id-cat_coarse ?rid verb)
;(id-root ?mvid turn_out|declare|agree|apply|arrange|ask|will|come|discuss|elect|flock|force|jump|move|spend|negotiate|pose|pay|persuade|catch|require|heat|go|rise|sit|tell|call|use)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid ke_lie))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	V_to_nA_ke_liye_2   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     V_to_nA_ke_liye_2  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     V_to_nA_ke_liye_2 "  ?rid " ke_liye )" crlf)
)
)

; @@@Added by Soma (26-10-14)
;They failed to make remarkable discoveries. === -ने में
;वे उल्लेखनीय खोज करने में असफल रहे.
;The trial would not be delayed to accommodate it. === -ने में / -ने के लिये
;परीक्षण में इसे समायोजित करने में देरी नहीं की जाएगी.
;He managed to get home on Sunday. === -ने में
;वह सन्डे को घर आने में कामयाब रहा.
;She would managed to keep him at a safe distance. === -ने में
;वह उसको उचित दूरी पर रखने में कामयाब रहेगी.
;I do not mind to do it. === -ने में
;मुझे यह करने में आपत्ति नहीं है.
;Each listener minded to understand other traditions. === -ने में
;हर श्रोता ने अन्य परम्पराओं को समझने में आपत्ति जताई.

(defrule V_to_nA_meM_3
(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
(kriyA-kriyArWa_kriyA ?mvid ?rid)
(id-root ?mvid fail|delay|manage|mind)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid meM))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	V_to_nA_meM_3   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     V_to_nA_meM_3  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     V_to_nA_meM_3 "  ?rid " meM )" crlf)
)
)

; @@@Added by Soma (26-10-14)
;I hope to become president someday. === -ने का
;मैं एक दिन राष्ट्रपति बनने की आशा करता हूँ.
;I tried to write a letter. === -ने का
;मैंने पत्र लिखने का प्रयास किया।
;It aims to help customers. === -ने का
;यह ग्राहकों की सहायता करने का लक्ष्य रखता है.
;WHO aims to make insurance affordable and accessible to all. === -ने काi 
;WHO सबके द्वारा  बीमा वहन करने योग्य और सुलभ बनाने का लक्ष्य रखता है.
;He is presently attempting to do the translation work. === -ने का
;वह  अभी अनुवाद कार्य करने का प्रयास कर रहा है.
;She did not bother to look inside. === -ने का
;उसने अन्दर देखने की परवाह नहीं की.
;The woman did not care to be constantly connected. === -ने का
;स्त्री ने लगातार जुडे रहने की  परवाह नहीं की.
;She claimed to be the wife of the owner, but I know she is not. === -ने का
;उसने मालिक की पत्नी होने का दावा किया, परन्तु मैं जानता हूँ कि वह नहीं है.





;Others conspired to keep Thomas off the original Dream Team. === -ने का
;अन्यों ने मूल ड्रीम टीम से टॉमस को दूर रखने का / के लिए षडयन्त्र रचे.
;I decided to go ahead. === -ने का 

;मैंने आगे जाने का फैसला किया.
;My dad determined to be the next Consul of Avalon. === -ने का no copula drop
;मेरे पिता ने ऐवलॉन का अगला कॉन्सल होने का निर्णय किया.
;The readers would enjoy to read the story. === -ने का
;पाठक कहानी पढने का आनन्द उठाएँगे.
;I will enjoy to talk later. === -ने का
;मैं बाद में बातचीत करने का आनन्द उठाऊँगा.
;I did not intend to do anything. === -ने का
;मेरा कुछ भी करने का इरादा नहीं था.
;They opted to go home. === -ने का
;उन्होंने घर जाने का फैसला किया.


(defrule V_to_nA_kA_4
(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
(kriyA-kriyArWa_kriyA ?mvid ?rid)
(id-root ?mvid become|try|aim|attempt|bother|care|claim|conspire|decide|determine|enjoy|opt|intend|allow|hope|desire|wish|need|expect|promise)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid kA))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	V_to_nA_kA_4   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     V_to_nA_kA_4  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     V_to_nA_kA_4 "  ?rid " kA )" crlf)
)
)

; @@@Added by Soma (26-10-14)
;She declined to comment. === -ने से
;उसने टिप्पणी करने से इनकार किया.
;She declined to be interviewed. === -ने से
;उसने इंटरव्यू देने से इनकार किया.
(defrule V_to_nA_se_5
(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
(or (kriyA-kriyArWa_kriyA ?mvid ?rid)(kriyA-kqxanwa_karma ?mvid ?rid))
(id-root ?mvid decline|deny|fear)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid se))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	V_to_nA_se_5   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     V_to_nA_se_5  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     V_to_nA_se_5 "  ?rid " se )" crlf)
)
)

;@@@ Added by Sukhada (14-08-14)
;It appears to be moving toward a middle ground. 
;यह  बीच की जमीन की ओर जाता हुआ लगता है.
;All students appeared to take the assessments seriously. 
;सब विद्यार्थी राय को गम्भीरता से लेते हुए लगे.
;He appeared to take great satisfaction in connecting adoptive parents with disabled kids. 
;वह दत्तक माँ बाप को विकलाङ्ग बच्चों से मिलाने में बहुत सन्तोष अनुभव करता हुआ लगा.
;They appear to show genuine concern about your welfare. 
;;;वे आपके हित में वास्तविक दिलचस्पी दिखाते हुए लगते हैं.
;She appeared to enjoy it. 
;वह इसका आनन्द उठाती हुई लगी.
(defrule to_appear_seem_6
;(declare (salience 5000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-kqxanwa_karma ?kri ?inf) (kriyA-kriyArWa_kriyA  ?kri ?inf))
(id-root ?kri appear|seem)
(to-infinitive ?id  ?inf)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(assert (id-H_vib_mng ?inf wA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " to.clp    to_appear_seem_6  "  ?id "  -  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* " to.clp    to_appear_seem_6  "  ?inf "  wA_huA  )" crlf))
)

; @@@Added by Soma (1-2-15) ; to25 is taken care of by this rule
;But my efforts to win his heart have failed .
(defrule N_to_nA_ke_liye_7
;(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?toid)
(saMjFA-to_kqxanwa ?nid ?toid)
;(kriyA-subject ?vmid ?nid)
(id-cat_coarse ?nid noun)
(id-cat_coarse ?toid verb)
;(id-cat_coarse ?vmid verb) Commented out by Soma
;(id-root ?nid effort)  COmmented out by Soma
=>
(retract ?mng)
(assert (make_verbal_noun ?toid))
(assert (id-H_vib_mng ?toid ke_lie))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        N_to_nA_ke_liye_7   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     N_to_nA_ke_liye_7  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp      N_to_nA_ke_liye_7      "  ?toid " ke_liye )" crlf)
)
)

; rule to_53 is a more generic one and has taken care the following condition and therefore this rule has been commented out (20-8-16)
; @@@Added by Soma (26-10-14) ; to18 is taken care of by this rule
;It is hard to find a good man.
;अच्छा आदमी मिलना मुशकिल है.
; It is important to know the truth.
;(defrule ADJ_to_nA_8 
;(declare (salience 1900))
;(id-root ?id to)
;?mng <-(meaning_to_be_decided ?id)
;(to-infinitive  ?id ?rid)
;(saMjFA-to_kqxanwa ?jjid ?rid)
;(id-cat_coarse ?jjid adjective)
;(id-cat_coarse ?rid verb)
;(id-root ?jjid good|nice|hard|easy|difficult|tough|right|possible|important|manner|idea|wrong|impossible)
;=>
;(retract ?mng)
;(assert (make_verbal_noun ?rid))
;(assert (id-wsd_root_mng ?id  -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	ADJ_to_nA_8   " ?id "  )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     ADJ_to_nA_8  "  ?id "  -)" crlf)
;)
;)

; @@@Added by Soma (26-10-14) - THis rule shd be there in contrast to the previous rule ADJ_to_nA. Currently commented out because the syntactic relation is not being captured by relations.
;A good man is hard to find.
;अच्छा आदमी मिलना मुशकिल है.
;(defrule RAISED_ADJ_to_nA  ;to10 condition
;(declare (salience 1900))
;(id-root ?id to)
;?mng <-(meaning_to_be_decided ?id)
;(to-infinitive  ?id ?rid)
;(subject-subject_samAnAXikaraNa ?jjid ?rid) ; Not a correct relation - correct would be abhihit relation
;(id-cat_coarse ?rid verb)
;(id-root ?jjid hard)
;=>
;(retract ?mng)
;(assert (make_verbal_noun ?rid))
;(assert (id-wsd_root_mng ?id  -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	RAISED_ADJ_to_nA   " ?id "  )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     RAISED_ADJ_to_nA  "  ?id "  -)" crlf)
;)
;)

; @@@Added by Soma (26-10-14)
;Amit is ready to go.
;अमित जाने के लिये तैयार है.
(defrule ADJ_to_nA_ke_liye_9  ;to10 condition, merged with this rule to22 partially
;(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
;(subject-subject_samAnAXikaraNa ?jjid ?rid) correct relation
(saMjFA-to_kqxanwa ?jjid ?rid)  ; Not the appropriate condition for this rule  - although working
(id-cat_coarse ?rid verb)
(id-root ?jjid ready|free|anxious|willing|quick)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid ke_lie))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	ADJ_to_nA_ke_liye_9   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     ADJ_to_nA_ke_liye_9  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     ADJ_to_nA_ke_liye_9 "  ?rid " ke_liye )" crlf)
)
)

; @@@Added by Soma (9-11-14)
;I am glad to meet you here.
;mEM yahAz Apase mila kara prasanna hUz.

(defrule ADJ_to_kara_10 ; same as rule to30 
;(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
(saMjFA-to_kqxanwa ?jjid ?rid)  ; Not the appropriate condition for this rule  - although working
(id-cat_coarse ?jjid adjective)
(id-root ?jjid happy|glad|sad|shocked|surprised|amazed|pleased|disheartened|dismayed|disappointed|delighted)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     ADJ_to_kara_10 "  ?rid " kara )" crlf)
)
)

; @@@Added by Soma (9-11-14)
;I am able to do the work.
;mEM yaha kAma karane meM samarWa hUz.
(defrule ADJ_to_ne_meM_11 
;(declare (salience 1900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
;(subject-subject_samAnAXikaraNa ?jjid ?rid) correct relation
(saMjFA-to_kqxanwa ?jjid ?rid)  ; Not the appropriate condition for this rule  - although working
(id-cat_coarse ?rid verb)
(id-root ?jjid able)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid meM))
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	ADJ_to_ne_meM_11   " ?id "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     ADJ_to_ne_meM_11  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     ADJ_to_ne_meM_11 "  ?rid " meM )" crlf)
)
)

;$$$Modified by Soma (9-11-2014)
;???@@@ Added by Prachi Rathore 3-1-14
;Turning [to] her husband, she told him unquestioningly to imitate her actions.[gyan-nidhi]
;उसके पति की ओर मुडते हुए, उसने उसको बिना कुछ पूछे उसकी क्रियाएँ नकल करने के लिये बताया .
(defrule to_kI_ora_12
(declare (salience 2900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-to_saMbanXI  ?id1 ?id2) 
(id-root ?id1 turn)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp 	to_kI_ora_12   "  ?id "  ki_Ora )" crlf))
)

;$$$Modified by Soma (9-11-2014)
;Added by Meena(9.11.09)
;She awakened to the sound of birds' singing . 
(defrule to_se_13
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id1 awaken)
(id-root ?id2 sound|light|taste|smell|sight)	; added by Soma 'light|taste|smell|sight'
;(viSeRya-of_saMbanXI  ?id2 ?id3)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_se_13   "  ?id "  se )" crlf))
)

;Modified by Soma (9-11-14) Merged to45 and to65_samvandhi
;To whom were you speaking ? (Modified by Meena 27.3.10)
;;He is not related to me .(Modified by Meena 22.8.09)
;;I should have talked to you before inviting John . Added by sukhada
(defrule to_se_arg_of_to_not_specified_14
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id1 talk|relate|speak|lose|agree|whisper|connect)    ;Added speak in the list(Meena(27.3.10));ex. for lose: 'Losing to a younger player was a bitter pill to swallow.' added verbs of to45 list to this rule and remove 'think', Added 'connect' by Soma (15-11-14) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_se_arg_of_to_not_specified_14   "  ?id "  se )" crlf))
)

;$$$Modified by Soma (9-11-2014)
;Added by Meena(31.8.09)
;We discussed adding new features to the program . 
;Some people take a lot of time to acclimatize themselves to the new environment .
(defrule to_meM_15
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-to_saMbanXI  ?id1 ?id2) ; Modified any argument of 'to'
(id-root ?id1 acclimatize|add)	;Added by Soma verbs
;(id-root ?id2 program|piece|environment) ;piece added Meena in the list(28.01.10) ;environment added sheetal; Commented out by Soma
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_meM_15   "  ?id "  meM )" crlf))
)

;Where did they go to?
(defrule to_where_16
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-word ?id2 where)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_where_16   "  ?id "  - )" crlf))
)


;$$$ Modified by Shirisha Manju (19-03-14) Suggested by Sukhada
;added afford in the list
;I can not afford to buy a house.
;mEM Gara KarIxane kA Karca_nahIM_uTA sakawA hUz.
;Modified by sheetal(18-03-10)
;He has the opportunity to go to school.
(defrule to_opportunity_afford_17
(declare (salience 3700))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) opportunity|afford)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid kA))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	to_opportunity_afford_17   " ?rid " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_opportunity_afford_17  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_opportunity_afford_17  "  ?rid " kA )" crlf))
)

;$$$ Modified by Soma (17-8-16)
;His aim was to become president.
(defrule to_18
(declare (salience 3500))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 2) aim|purpose|plan|idea);changed =(- ?id 1) to =(- ?id 2) by sheetal(21-09-09) | commented by Soma (17-8-16) after adding the conditions below; counterexample: The purpose of the present paper is to present ...) 
;(id-root =(- ?id 1) is|are|be|was|were|been|am|become) ;Added "become" to the list by sheetal(21-09-09)| commented by Soma (17-8-16) after adding the conditions below; counterexample: The purpose of the present paper is to present ...) 
(to-infinitive  ?id ?rid)
(kriyA-kqxanwa_karma ?vid ?rid)
(kriyA-subject ?vid ?sub)
(id-word ?vid is|are|be|was|were|been|am|become|became)
(id-root ?sub aim|purpose|plan|idea|hope|demand|goal|role|aspect|fact|mission|vision|assignment|way|temptation|strategy|message|concept|challene|process|focus|point|dream|task|avenue|intent|intention|result|method)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid 0))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	to_18   " ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_18  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_18  "  ?rid " 0 )" crlf))
)
; Not a very tight rule
; I thought it right to resign.
(defrule to_19
(declare (salience 3300))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) think)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid 0))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	to_19   "  ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_19  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_19  "  ?rid "  0 )" crlf)
)
)

;He was to become president.
;Because of the recession the company is to axe 350 jobs .
; The subject of verb for this kind of sentence has or acquire an animacy (personified) feature.
(defrule to_ne_vAlA_20  ; This rule is merged with to4
(declare (salience 3400))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
;(id-root =(- ?id 1) is|are|be|was|were|been|am)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
(kriyA-kqxanwa_karma ?vid ?rid)
(kriyA-subject ?vid ?sub)
(id-word ?vid is|are|be|was|were|been|am|become|became)
(not(id-root ?sub aim|purpose|plan|idea|hope|demand|goal|role|aspect|fact|mission|vision|assignment|way|temptation|strategy|message|concept|challene|process|focus|point|dream|task|avenue|intent|intention|result|method))
;(not(id-root ?rid become|guide|give|create)) ;Added 'give' by Roja(15-12-10);The purpose of this note is to give simple sentences for translation. Added 'create' by Roja(19-11-13). Ex: The main purpose of industry is to create wealth. COPIED FROM to4 Not really the condition (Soma 17-8-16)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid vAlA))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	to_ne_vAlA_20   " ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_ne_vAlA_20  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_ne_vAlA_20  "  ?rid "  vAlA)" crlf))
)


; $$$ Modified by Soma (25-12-14) - RULE NEEDS TO BE WORKED OUT
;To stimulate is to cause to be alert.
; The rule says that if the subject of the verb is a S and is in samAnadhikaraNa relation with another S which is TO clause, the TO is NULL.
;(defrule to5
;(declare (salience 4700))
;(id-root ?id to)
;?mng <-(meaning_to_be_decided ?id)
;(id-id =(- ?id 1) ?id1)
;(id-word ?id1 is)
;(kriyA-subject ?id1 ?id2)
;(subject-subject_samAnAXikaraNa ?id2 ?)
;=>
;(retract ?mng)
;(assert (make_verbal_noun ?rid))
;(assert (id-H_vib_mng ?rid 0))
;(assert (id-wsd_root_mng ?id -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to5  "  ?rid " )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to5   "  ?id "  -)" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to5   "  ?rid " 0 )" crlf))
;)

; $$$Modified by Soma (9-11-14)
;???@@@   Added by Prachi Rathore				Already taken  - 'go/take sth to NP'
;So Sidey went [to] him.[gyan-nidhi]
;इसलिए दुमछल उसके पास गयी।
;So I have come [to] you.[gyan-nidhi]
;मैं इसीलिए आपके पास आई हूँ।
;I must take them[ to] the King, the farmer said [firmly].[GYANNIDHI]
;यही उचित है कि हम उन्हें राजा के पास ले जायें।
(defrule to_ke_pAsa_21
(declare (salience 2500))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id1 ?id2)
(id-word ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id1 go|come|take|bring) ; Added by Soma (9-11-14) bring
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_ke_pAsa_21  "  ?id "  ke_pAsa)" crlf)
)
)

; Added Soma (10-11-14)  - Modified to64, to86
;They went to Delhi.
;They went to the school.
;They brought the girl to my house.
;They took my car to Delhi.
(defrule to_NULL_22
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id1 ?id2)
;(id-word ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id1 go|come|take|bring) ; Added by Soma (10-11-14) bring
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_NULL_22  "  ?id "  -)" crlf)
)
)
; $$$ Modified by Soma (22-11-14)
;@@@ Added by Prachi Rathore 6-1-14
;In half an hour they had swept all the litter and were about to make a large heap outside the garden. 
(defrule to_23
(declare (salience 2900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) about )
;(id-root ?id1)	Commented out by Soma (22-11-14)
;(id-cat_coarse =(+ ?id 1) verb)
;(saMjFA-to_kqxanwa  ?id1 ?rid)
(to-infinitive ?id ?id1 )	; Added by Soma
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?id1))
(assert (id-wsd_root_mng ?id -))
(assert (id-H_vib_mng ?id1 vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp  	to_23   " ?id1 "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_23  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_23 "  ?id1 " vAlA )" crlf)
)
)

; @@@ Added by Soma (21-11-14)
;The efficiency of nutrient uptake by crops from fertilizers or residue release is generally thought to be similar.
;उत्पादक या बचे हुए  अवशेष से फसलों द्वारा पोषक उद्ग्रहण का दक्षता आम तौर पर समान माना जाता है ।
(defrule to_24
(declare (salience 2900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive ?id ?id1 )
(id-root ?id1 be)
(id-word =(- ?id 2) similar )
;(id-cat_coarse =(+ ?id 1) verb)
;(saMjFA-to_kqxanwa  ?id1 ?rid)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id1 -))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* "  to.clp        to_24   " ?id1 " -  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_24  "  ?id "  -)" crlf)
)
)

; $$$ Modified by Soma (13-12-14)
;Rule added by Meena(22.8.09)
;He left all his money to the orphanage .
(defrule to_25
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 money|property|thing|belonging)
(kriyA-object ?id1 ?id2)
(id-root ?id1 leave)  ; added by Soma
(kriyA-to_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_25   "  ?id "  ke_liye )" crlf))
)

(defrule to_26 ; This is the default rule
(declare (salience -100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_26   "  ?id "  - )" crlf))
)

; $$$ Modifed by SOma (20-08-16)
; He is a threat to the society.
; It is adequate to our need.
; It would be painful to her family.

(defrule to_27
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id1 ?)
(id-word ?id1 threat|adequate|advantageous|beneficial|painful|conducive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_27   "  ?id "  ke_liye )" crlf))
)

; The man sat next to me. Example Added by Soma (22-12-14)
(defrule to_28
(declare (salience 1100)) ; Salience modified by Soma from -1000 to 1100
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) next )
(id-cat_coarse =(+ ?id 1) ~verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_karIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_28   "  ?id "  ke_karIba )" crlf))
)


; $$$ Modified by Soma (22-12-14) Very specific rule for 'give birth to', 'give importance to', 'give weightage to'
;Modified by Meena(30.4.10)
;Added by Meena(28.4.10)
;She gave birth to twins .
;Some schools do not give weightage to extracurricular activities .  
(defrule to_29
(declare (salience 200))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 give) ; Added by Soma
;(id-word =(+ ?id 1) birth|importance|weightage) ; Added and commented out by Soma because of the data 'give a lot of importance', instead added ?id3
(kriyA-to_saMbanXI ?id1 ?id2)
(not(or(id-word ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-cat_coarse ?id2 PropN)))
(kriyA-object ?id1 ?id3)
(id-word ?id3 birth|importance|weightage) ;Added by SOma
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_29  "  ?id "  ko)" crlf)
)
)

;Added by Meena(28.4.10)
;To the people of India he is the symbol of mature wisdom .
(defrule to_30
(declare (salience 100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-to_saMbanXI ?id1 ?id2)(viSeRya-to_saMbanXI ?id1 ?id2));Added by Roja(17-12-10) ;this rule is modified to stop the rule firing in unneccesary cases.Eg: He made negative comments to the press . 
(viSeRya-of_saMbanXI  ?id2 ?id3)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_30  "  ?id "  ke_liye)" crlf)
)
)
;Added by Meena(10.11.09)       
;Failure to comply may result in dismissal.
(defrule to_31
(declare (salience 2800))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ?id ?rid)
(id-root ?id1 failure)
(saMjFA-to_kqxanwa  ?id1 ?rid);Renamed saMjFA-kqxanwa as saMjFA-to_kqxanwa by Manju (05-02-11)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid));added by sheetal
(assert (id-H_vib_mng ?rid kA))
(assert (id-wsd_root_mng ?id -));meaning is change by sheetal
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp         to_31   "  ?rid " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_31  "  ?id "  - )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_31  "  ?id " kA )" crlf)
)
)

;$$$ Modified by Soma (18-09-2016)
;$$$ Modified by Prachi Rathore;added tea in the list Ex: Uncle, all this applies to Green tea. - to be taken care of -
;Uncle, all this applies to Green tea.
;चाचा, सब यह हरियाली चाय पर लागू करता है . 
;Added apply in the list 10-1-14 
;Pressure applied to the wound will stop the bleeding.
;Modified by Meena(28.4.10); added top|station|place in the list and commented (id-root ?id1 heat),so that the rule works with other verbs.
;Added by sheetal(14-01-10).
(defrule to_32
(declare (salience 1150))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
;(id-root ?id1 heat)
(or(id-root ?id1 tune|fall|apply|heat)(id-root ?id2 temperature|top|place|river|station|tea)) ; remove write from the verb list because of the coutnerexample 'Have you written a letter to Radha'
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_32   "  ?id "  para )" crlf))
)
;Food must be heated to a high temperature to kill harmful bacteria .





; @@@ Added by Soma (13-12-14) to take care of verbs like say with which ko is added to the object
; I said to the boy that he must finish his work.
;मैंने लडके को कहा कि उसको उसका कार्य पूर्ण करना चाहिए . 
;I gave a book to the boy.
;I sent a letter to the headmaster.
;I asked a question to the man.
(defrule to_33
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 say|give|send|ask|write)
(kriyA-to_saMbanXI  ?id1 ?id2)
(or(id-word ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id2 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_33   "  ?id "  ko )" crlf))
)
;Modified by SOma (24-12-14) to0 - NOT WORKING
;Added by Meena(12.4.10)
;Everyone has a right to education.  (2nd linkage gives correct parse)
(defrule to_right-to_34
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) right)
;(id-word =(+ ?id 1) education)
;(id-cat_coarse =(+ ?id 1) noun)
(viSeRya-to_saMbanXI ?id1 ?id2)
(id-cat_coarse ?id2 noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " to.clp    to_right-to_34  "  ?id "   kA  )" crlf))
)

; $$$ Modified by Soma (24-12-14)
(defrule to_35
(declare (salience 4700))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 prefer)
(kriyA-to_saMbanXI ?id1 ?) ; Added by Soma
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_apekRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_35   "  ?id "  kI_apekRA )" crlf))
)


;$$$ Modified 'wsd_root_mng' to 'affecting_id-affected_ids-wsd_group_root_mng' by Roja(27-04-14). Suggested by Chaitanya Sir.
;He talked to him in order to secure the account.
(defrule to_36 ; in-order-to
(declare (salience 4400))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) order )
(id-word =(- ?id 2) in)
(to-infinitive  ?id ?rid)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid ke_liye))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_36   "  ?rid " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  to.clp     to_36  "  ?id "  "(- ?id 1)" "(- ?id 2)"   -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_36  "  ?rid " ke_liye )" crlf))
)

; $$$ Modified by Soma (25-12-14)
;He ran from point A to point B.
(defrule to_37
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 run) ;the meaning of 'to' is decided in the context of 'run'
(kriyA-to_saMbanXI ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_37   "  ?id "  waka )" crlf))
)

;This is the way to do the work.
;This is the way to go.

(defrule to_38
(declare (salience 2950));salience changed by sheetal
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) way|method|procedure|order)
(to-infinitive  ?id ?rid)
(not(id-root ?rid catch))  ;Added by Meena(22.3.11) so that we get "pakadane ke liye" for "to catch" in the ex.The old man had to walk a long way to catch the bus .
(id-cat_coarse ?rid verb)
(not (kriyA-kriyA_viSeRaNa  ?kriyA =(- ?id 1)));Added by sheetal 4:The old man had to walk a long way to catch the bus .
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid kA))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_38   "  ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_38  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_38  "  ?rid " kA )" crlf))
)

(defrule to_39                  
(declare (salience 3900))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) going )
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid 0))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_39   "  ?rid " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_39  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_39  "  ?rid " 0 )" crlf))
)

;Comment by Soma - Not a very robust rule - might need to be corrected/constrained properly later  (1-02-15)
;I am afraid to go there.
(defrule to_40
(declare (salience 2400))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) afraid|horrified|terrified)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-H_vib_mng ?rid se))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_40   " ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_40  "  ?id "  -)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  to.clp     to_40  "  ?rid " se )" crlf)
)
)




;He is going to start it.
;@@@ Added by Soma (8-1-15)
; Elastic forces bind the constituents to each other 
;प्रत्यास्थ बल माध्यम के अवयवों को एक-दूसरे से बाँध रखते हैं 
(defrule to_41
(declare (salience 1100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 bind)
(kriyA-to_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp    to_41   "  ?id "  se )" crlf))
)

(defrule to_42
(declare (salience 1800))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) as)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_42   "  ?id "  - )" crlf))
)


;$$$modified by Soma (20-8-16)
;The teacher reads to her students everyday. or The teacher reads once in a week to her students
(defrule to_43
(declare (salience 1700))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
;(id-root =(- ?id 1) read) [commented out because it is very specifice]
(kriyA-to_saMbanXI ?id1 ?)
(id-word ?id1 reads|read|reading)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp   to_43   "  ?id "  ke_liye )" crlf))
)


(defrule to_44 ;;; NOT A ROBUST RULE
(declare (salience 1600))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) cardinal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_44   "  ?id "  waka )" crlf))
)

;;; Modified by Soma (18-05-2015)
(defrule to_45
(declare (salience 1500))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2);;; added the relation by Soma
(id-root ?id1 follow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_45   "  ?id "  waka )" crlf))
)

;;; Modified by Soma (18-05-2015)
;Modified by Sukhada
;The girl running to the shop is my friend . 
;Indeed, the branch of optics in which one completely neglects the finiteness of the wavelength is called geometrical optics and a ray is defined as the path of energy propagation in the limit of wavelength [tending to] zero.
(defrule to_46
(declare (salience 1400))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2);;; added the relation by Soma
(id-root ?id1 walk|point|carry|run|cycle|tend) ;used root instead of word  ;Added tend in the list by Manju (23-08-13) Suggested by Chaitanya Sir 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_46   "  ?id "  kI_ora )" crlf))
)



;;; Modified by Soma (18-05-2015)
(defrule to_47
(declare (salience 400))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id2);;; added the relation by Soma
(id-root ?id1 belong|reply)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_47   "  ?id "  kA )" crlf))
)


;;; Modified by Soma (15-05-2015)
(defrule to_48
(declare (salience 100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 expose)
(kriyA-to_saMbanXI  ?id1 ?id2);;; added the relation by Soma
(id-root ?id2 air|water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_48   "  ?id "  meM )" crlf))
)


;;; Modified by Soma (15-05-2015)
(defrule to_49
(declare (salience -100))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 expose)
(kriyA-to_saMbanXI  ?id1 ?id2) ;;; added the relation by Soma
(id-root ?id2 public|audience|sun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to_49   "  ?id "  ke_sAmane )" crlf))
)

;;; Added by Soma (10-02-2016)
;;; I have an important announcement to make.[oald]
;;; मुझे एक महत्वपूर्ण घोषणा करनी है[ suggested by Shivani Pathak 10-2-16]

(defrule to_50
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 have)
(viSeRya-kqxanwa_viSeRaNa ?id2 ?rid)
(kriyA-object ?id1 ?id2)
(to-infinitive  ?id ?rid)
(id-cat_coarse ?rid verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?rid))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_50   " ?rid "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_50  "  ?id "  -)" crlf)
))

;;; @@@ Added by Soma (20-08-2016)
;;; I am very thankful to you.[oald]
;;; मैं आपके प्रति अत्यन्त कृतज्ञ हूँ 
;;; He was cruel to children
;;; vaha baccoM ke prawi nirxayI WA.
;;; He was answerable to the governor.

(defrule to_51
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id1 ?)
(id-word ?id1 thankful|cruel|answerable|alert)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_prawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_51  "  ?id "  ke_prawi)" crlf)
))


;;; @@@ Added by Soma (20-08-2016)
;;; It was not acceptable to us.[oald]
;;;  It is accessible to the disabled.
;;; It sounds pretty accurate to me.
;;; It is available to all.
(defrule to_52
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id1 ?)
(id-word ?id1 acceptable|accessible|accurate|available)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_52  "  ?id "  ko)" crlf)
))


;@@@ Added by Soma (20-08-2016)
;It is not a good manner to eat alone.
;viSrAma ke binA GaNtoM waka vAhana calAnA acCA vicAra nahIM hE.
;I do not consider it a good manner to eat alone.
;It was interesting to hear about school life in Britain.
;britana meM vixyAlaya jIvana ke bAre meM sunanA rucikara WA.
;It is not a good manner to eat alone.
;akelA KAnA acCA vyavahAra nahIM hE.
;akelA KAnA acCA AcaraNa nahIM hE.
;It was her custom to rise early. 
;jalxI uTanA usakI Axawa WI.

(defrule to_53
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive ?id ?id1)
(saMjFA-to_kqxanwa ?nid ?id1)
(or (subject-subject_samAnAXikaraNa ? ?nid)(object-object_samAnAXikaraNa ? ?nid))
=>
(retract ?mng)
(assert (make_verbal_noun ?id1))
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  to.clp        to_53   " ?id1 "  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_53  "  ?id "  -)" crlf)
))


;@@@ Added Soma (20-08-16) - Word specific rule
;They went to the party.
;They came to the school.
(defrule to_meM_54
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id1 ?id2)
(id-root ?id1 go|come)
(id-root ?id2 party)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_meM_54  "  ?id "  meM)" crlf)
)
)

; @@@ Added Soma (21-08-16) - Word specific rule
;It was known to all.
(defrule to_55
(declare (salience 2000))
(id-root ?id to)
?mng <-(meaning_to_be_decided ?id)                                                                                       
(kriyA-to_saMbanXI ?id1 ?)
(id-word ?id1 known)
(id-cat_coarse ?id preposition)
=>                                                                                                                       
(retract ?mng)                                                                                                           
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  to.clp     to_55  "  ?id "  ko)" crlf)
))

;Good to see you
; tumako dekhakara acchA lagA
;The role of a teacher is to guide students towards knowledge.
;(defrule to62  ;Translation of 'to' as 'ko'  Does not appear to be a default one, therefore commeneted out
;(declare (salience -100))
;(id-root ?id to)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id preposition)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ko))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  to.clp       to62   "  ?id "  ko )" crlf))
;)
;


; @@@Added by Soma (27-10-14)		TO BE DONE
;I want him to go home.
;I expect the girl to pass the examination.
;I intend him to finish his study fast.
;We are not allowed to go out of the house.
; I consider him to be intelligent.
; He is considered to be intelligent.
; I promised him to finish the work by today.


;TO BE FURTHER LOOKED INTO
; His effort to win the game failed.   - Worked out N_nA_ke_liye
; It is my duty to go.
; My duty to help my friends is never fulfilled.
