;$$$ modified by 14anu-ban-10 on (27-08-2014)
;@@@ Added by Anita  8.3.2014 
;Ready in debate, prompt and firm in giving decisions, he has, I believe, been a most expert chairman at meetings. --[By mail]
;मैंने सुना है कि विवाद में निपुण, फैसले करने में चुस्त और दृढ़ होने के कारण वे बैठकों में सबसे अधिक कुशल अध्यक्ष रहे हैं।
(defrule ready3
(declare (salience 5000))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 firm) 
(id-root ?id2 debate)
(or(viSeRya-in_saMbanXI  ?id ?id1) 
(viSeRya-in_saMbanXI ?id ?id2)(kriyA_vAkyakarma ?id ?)) ;added (kriyA_vAkyakarma' relation by 14anu-ban-10 on (27-8-10)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nipuNa)) ; meaning changed from wEyAra to 'nipuNa' by 14anu-ban-10 on (27-8-10)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready3   "  ?id "  nipuNa )" crlf))
)


;@@@ Added by Anita--14.3.2014
;The dimensional formulae of a large number and wide variety of physical quantities derived from the ;equations representing the relationships among other physical quantities and expressed in terms of ;base quantities are given in Appendix 9 for your guidance and ready reference.
(defrule ready2
(declare (salience 5100))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ? $? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAwkAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready2   "  ?id "  wAwkAlika )" crlf))
)
;------------------------ Default rules ---------------------
(defrule ready0
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready0   "  ?id "  wEyAra )" crlf))
)

(defrule ready1
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready1   "  ?id "  prApwa_Xana )" crlf))
)

;"ready","N","1.prApwa_Xana"
;She is not having enough of the ready.   
;--"2.kAma_me_lAne_ke_liye_wEyAra"    
;The watchman keeps his spear && torch ready to hand.

;These are given in Appendix A 6.2 and A 6.3 for your ready reference.
; ye Apake wEyAra pramANa pawra ke lie apendiksa e 6.2 meM Ora e 6.3 meM xie gaye hEM .

;@@@ Added by 14anu-ban-10 on (27-08-2014)
;The people of West Bengal are conscious towards their problems , are ready for struggle .
;पश्चिम  बंगाल  के  लोग  अपनी  समस्याओं  के  प्रति  जागरूक  व  संघर्ष  के  लिए  तत्पर  रहते  हैं  ।
(defrule ready4
(declare (salience 5500))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaka ?id ?id1) ;commented out by 14anu-ban-10 on (25-11-2014)
(viSeRya-for_saMbanXI  ?id ?id1) ;added by 14anu-ban-10 on (25-11-2014)
(id-root ?id1 struggle) ;added by 14anu-ban-10 on (25-11-2014)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wawpara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready4 "  ?id "  wAwpara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-08-2014)
;Police appear to be ready for the security of this place they also ignore the activities of these youths .
;पुलिस  यहाँ  सुरक्षा  के  लिए  तैनात  नजर  आती  है  लेकिन  वह  भी  इन  युवाओं  की  हरकतों  को  दरकिनारा  कर  देते  हैं  ।
(defrule ready5
(declare (salience 5600))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ?id ? )(kriyA-kriyArWa_kriyA ?  ?id))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEnAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready5 "  ?id "  wEnAwa)" crlf))
)
;@@@ Added by 14anu-ban-10 on (27-08-2014)
;General guidelines for using symbols for physical quantities, chemical elements and nuclides are given in Appendix A7 and those for SI units and some other units are given in Appendix A8 for your guidance and ready reference. 
;BOwika rASiyoM, rAsAyanika wawvoM Ora nABikoM ke safkewoM ke upayoga sambanXI sAmAnya nirxeSa pariSiRta @A7 meM xie gae hEM Ora Apake mArgaxarSana waWA wAwkAlika sanxarBa ke lie @SI mAwrakoM evaM anya mAwrakoM sambanXI nirxeSa pariSiRta @A8 meM xie gae hEM.
(defrule ready6
(declare (salience 6000))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 reference)      ;added by 14anu-ban-10 on (25-11-2014)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAwkAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready6   "  ?id "  wAwkAlika)" crlf))
)


;@@@ Added by 14anu-ban-10 on (27-08-2014)
;As the day dawns , bazaar gets ready in the small and big streets of Goa  .
;दिन  निकलते  ही  गोवा  की  छोटी-बड़ी  अलियों-गलियों  में  बाजार  लग  जाता  है  ।
(defrule ready7
(declare (salience 5800))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)  
;(viSeRya-after_saMbanXI ? ?id))
(id-root ?id1 get) ;added by 14anu-ban-10 on (25-11-2014)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready7   "  ?id "  laga)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-08-2014)
;Aadiguru Shankaracharya had also visited here and his meeting with Kumaril Bhatta ,  ready for self-immolation in the woodfire also occurred here .
;आदिगुरु  शंकराचार्य  भी  यहाँ  पधारे  थे  और  कार्षाग्नि  में  आत्मदाह  के  लिये  उद्धत  कुमारिल  भट्ट  से  उनकी  यहीं  भेंट  हुई  थी  ।
(defrule ready8
(declare (salience 5900))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
;(or(viSeRya-for_saMbanXI ?id ? )(viSeRya-in_saMbanXI ?id ? ))   ;commented out by 14anu-ban-10 on (25-11-2014)
(viSeRya-viSeRaNa ?id1 ?id)       ; added by 14anu-ban-10 on (25-11-2014)
(id-root ?id1 visit)           ;added by 14anu-ban-10 on (25-11-2014)
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxXw))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready8   "  ?id "  uxXw)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-11-2014)
;I am just about ready.[sugested by soma mam]
;मैं लगभग तैयार हूँ . [manual]
(defrule ready9
(declare (salience 6000))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ?id ? )
(id-cat ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready9   "  ?id "  wEyAra)" crlf))
)

;@@@ Added by 14anu13  23-06-2014 
; I'll fix my hair and then I'll be ready. 
;मैं अपने बाल ठीक कर लूँ ,तब मैं तैयार हो जाऊँगा |
(defrule ready10
(declare (salience 5000))
(id-root ?id ready)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa 	?id1 	 ?id)
(viSeRya-viSeRaka 	?id 	 ?id2)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ready.clp 	ready10   "  ?id "  wEyAra )" crlf))
)

