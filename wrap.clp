
(defrule wrap0
(declare (salience 5000))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id wrapping )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lapetana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  wrap.clp  	wrap0   "  ?id "  lapetana )" crlf))
)

;"wrapping","N","1.lapetana"
;Give a good wrapping for the porcelain.


;(defrule wrap1
;(declare (salience 4900))
;(id-root ?id wrap)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 up)
;(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by ;;Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id lapeta));Automatically modified 'affecting_id-affected_ids-;wsd_group_root_mng ?id ?id1' to 'id-;wsd_root_mng ?id ' by Sukhada's program 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " wrap.clp  wrap1 " ?id "  lapeta )" crlf)) 
;)

(defrule wrap2
(declare (salience 4800))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wrap.clp  wrap2 "  ?id "  " ?id1 "  lapeta )" crlf))
)
;$$$ Modified by Anita-4.6.2014 [Add sentence]
;Wrap the chicken in foil and cook it for two hours. [cambridge dictionary] 
;मुर्गी को पन्नी में लपेटिये और इसे दो घंटे तक पकाइये ।
;Wrap yourself in that. [oxford learner's dictionary]
;खुद को उसमें लपेटिये ।
(defrule wrap3
(declare (salience 4700))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb|adjective)
=>
(retract ?mng)
(assert (id-wsd_number ?id s))
(assert (id-wsd_root_mng ?id lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  wrap.clp    wrap3   "  ?id " s )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrap.clp 	wrap3   "  ?id "  lapeta )" crlf))
)


(defrule wrap4
(declare (salience 4700))
(id-word ?id wrapped)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrap.clp      wrap4   "  ?id "  lapeta )" crlf))
)

;"wrap","VT","1.lapetanA{kAgajZa_Axi_se}"
;The gifts were wrapped && kept on the table.

;@@@ Added by Anita-2.6.2014
;His arms were wrapped around her waist. [oxford learner's dictionary] [root prob.]
;उसकी बाहें उसकी कमर के चारों ओर लपटी हुईं थीं । 
(defrule wrap5
(declare (salience 5100))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 waist|shoulder)
(kriyA-around_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapata_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrap.clp 	wrap5   "  ?id " lapata_ho)" crlf))
)


;@@@ Added by Anita-2.6.2014
;The text wraps around if it is too long to fit the screen. [oxford learner's dictionary]
;विषय-वस्तु यदि पर्दे के लिए अधिक लम्बी है तो कैसे हम इसे मिला सकते हैं । 
(defrule wrap6
(declare (salience 5200))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vrepa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wrap.clp  wrap6 "  ?id "  " ?id1 "  vrepa_kara )" crlf))
)
;@@@ Added by Anita-2.6.2014--parse prob. - kriyA-upasarga relation should be there but it is not ;coming
;How can I wrap the text around? 
;मैं विषय-वस्तु को कैसे समेट सकता हूँ ?
(defrule wrap7
(declare (salience 5300))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id modZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrap.clp 	wrap7   "  ?id " modZa )" crlf))
)
;@@@ Added by Anita-3.6.2014
;I wrapped the baby up in a blanket. [oxford learner's dictionary] [ root prob.]
;मैंने बच्चे को कंबल में लपेटा । 
;He spent the evening wrapping up the Christmas presents. [oxford learner's dictionary]
;उन्होंने कहा कि क्रिसमस उपहार लपेटते हुए शाम बिताई ।
(defrule wrap8
(declare (salience 5400))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga  ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wrap.clp  wrap8   " ?id "  " ?id1 "  lapeta )" crlf))
)
;@@@ Added by Anita-4.6.2014
;She wrapped the present and tied it with ribbon. [cambridge dictionary] [root prob.]
;उसने उपहार को लपेटा और रिबन से बाँधा ।
;He wrapped a towel around his shoulders. [cambridge dictionary] [root prob.]
;उसने तौलिया अपने कंधों के चारों ओर लपेटा ।
;I wrapped a blanket around the baby. [oxford learner's dictionary] [root prob.]
;मैंने शिशु के चारों ओर कंबल लपेटा ।
;A scarf was wrapped around his neck. [oxford learner's dictionary] [root prob.]
;स्कार्फ उसकी गर्दन के चारों ओर लपेटा हुआ था ।
;The nurse wrapped a bandage tightly around my ankle. [oxford learner's dictionary] [root prob.]
; परिचारिका ने मेरे टखने के चारों ओर कसकर पट्टी लपेटी ।
(defrule wrap9
(declare (salience 4800))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(or(conjunction-components  ? ?id ?)(kriyA-around_saMbanXI  ?id ?sam))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wrap.clp 	wrap9   "  ?id " lapeta)" crlf))
)

;@@@ Added by Anita-10-06-2014
;Rajvir carefully wrapped the clay in a piece of paper and thrust it into his pocket. 
;राजवीर ने सावधानी से कागज के एक टुकड़े में मिट्टी लपेटी और इसको अपनी जेब में रख लिया ।
(defrule wrap10
(declare (salience 5500))
(id-root ?id wrap)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 clay )
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  wrap.clp  	wrap10   "  ?id "  lapeta )" crlf))
)



;Would you like the chocolates gift-wrapped? [oxford learner's dictionary]
;kyA Apa cAkaletoM ko gift-wrapped pasanxa karefge? [anusaaraka out-put]

;The store offers a gift-wrapping service. [oxford learner's dictionary]
;xukAna eka gift-wrapping sevA xewI hE. [anusaaraka out-put]

