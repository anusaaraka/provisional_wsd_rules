
(defrule finish0
(declare (salience 5000))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id finished )
;(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  finish.clp  	finish0   "  ?id "  pUrNa )" crlf))
)

;"finished","Adj","1.pUrNa"
;Finished job is always the most satisfying.
;--"2.samApwa"
;This is what the finished product looks like.
;
;
(defrule finish1
(declare (salience 4900))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mAra_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " finish.clp	finish1  "  ?id "  " ?id1 "  mAra_dAla  )" crlf))
)

;
;
(defrule finish2
(declare (salience 4800))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finish.clp 	finish2   "  ?id "  anwa )" crlf))
)

;"finish","N","1.anwa"
;This race will have an interesting finish.
;
(defrule finish3
(declare (salience 4700))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finish.clp 	finish3   "  ?id "  pUrNa_kara )" crlf))
)

;"finish","V","1.pUrNa_karanA"
;He has been asked to finish this work today.
;--"2.samApwa_karanA"
;Please finish the coffee.
;--"3.mAra_dAlanA"
;He has engaged some persons to finish Shyam.
;

;@@@ Added by 14anu-ban-05 on (24-03-2015)
;And when they died and the fleas had finished feasting on their blood, which was by then putrid and crawling with the bacilli, the fleas looked round for another host, and if they could not find another rat, or any small hairy creature to their liking, they settled for the next best thing, which was usually human.		[bnc-corpus]
;और जब वे मर गये और पिस्सुओं ने उनके खून पर भोजन करना समाप्त किया, जो कि तब तक सड़ चुका होता है और रेंगते हुये  bacilli से युक्त हो जाता है , तो  पिस्सु एक और  पोषक को आसपास  ढूँढते है,और अगर उन्हें अपनी पसंद के हिसाब से एक और चूहा, या कोई भी छोटे बालों वाले प्राणी नहीं मिलता है तब वह वे  अगली सबसे अच्छी चीज पर सन्तुष्ट हो जाते है,जो कि आम तौर पर मानव होते है.[manual]
(defrule finish4
(declare (salience 4701))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?id1)
(id-root ?id1 feast)			;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finish.clp 	finish4   "  ?id "  samApwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (24-03-2015)
;Please finish the coffee.[from finish.clp]
;कृप्या कॉफी समाप्त कीजिये.		[manual]
(defrule finish5
(declare (salience 4701))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 coffee)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finish.clp 	finish5   "  ?id "  samApwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (24-03-2015)
;He has engaged some persons to finish Shyam.[from finish.clp]
;उसने  श्याम को  मार डालने के लिए कुछ व्यक्तियों को लगाया  है.	[manual]
(defrule finish6
(declare (salience 4702))
(id-root ?id finish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(or (id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat ?id1 proper_noun))  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAra_dAla))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finish.clp 	finish6   "  ?id "  mAra_dAla )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  finish.clp 	finish6   "  ?id " ko )" crlf))
)
