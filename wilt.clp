;@@@ Added by 14anu-ban-11 on (16-04-2015)
;Cut flowers will soon wilt without water.(oald)
;टुटे हुए फूल पानी के बिना शीघ्र मुरझा जाएँगे . (self)
(defrule wilt1
(declare (salience 10))
(id-root ?id wilt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-without_saMbanXI  ?id ?id1)
(id-root ?id1 water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muraJA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wilt.clp 	wilt1   "  ?id "  muraJA_jA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (16-04-2015)
;After only an hour's walking they were beginning to wilt in the heat.(oald)
;सिर्फ एक घण्टे के टहलने के बाद उन्होने गरमी  में थकना शुरु कर दिया था. (self)
(defrule wilt2
(declare (salience 20))
(id-root ?id wilt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 heat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wilt.clp 	wilt2  "  ?id "  Waka)" crlf))
)

;@@@ Added by 14anu-ban-11 on (16-04-2015)  ;Note:- Working properly on parser no. 15.
;The passengers were visibly wilting with the heat and movement of the bus.(oald)
;यात्री बस की ऊष्मा और गति से परेशान होते हुए दिख रहे  थे . (self)
(defrule wilt3
(declare (salience 30))
(id-root ?id wilt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 heat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wilt.clp 	wilt3   "  ?id "  pareSAna_ho)" crlf))
)

;---------------------------- Default Rules ---------------------------

;@@@ Added by 14anu-ban-11 on (16-04-2015)
;He was wilting under the pressure of work.(oald)
;वह कार्य के दबाव के कारण शिथिल हो रहा था . (self)
(defrule wilt0
(declare (salience 00))
(id-root ?id wilt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiWila_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wilt.clp 	wilt0   "  ?id "  SiWila_ho)" crlf))
)


