;@@@ Added by 14anu-ban-03 (09-02-2015)
;It's only common courtesy to tell the neighbours that we'll be having a party. [oald]
;पड़ोसियों को बताना एक सामान्य शिष्टाचार है कि हम दावत दे रहे है. [manual]
(defrule courtesy0
(declare (salience 00))
(id-root ?id courtesy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRtAcAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  courtesy.clp 	courtesy0   "  ?id "  SiRtAcAra )" crlf))
)


;@@@ Added by 14anu-ban-03 (09-02-2015)
;The pictures have been reproduced by courtesy of the British Museum. [oald]
;तस्वीरें ब्रिटिश संग्रहालय के सौजन्य से उत्पन्न की गई है। [manual]
(defrule courtesy1
(declare (salience 500))
(id-root ?id courtesy)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id1 ?id)
(id-root ?id1 reproduce)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOjanya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  courtesy.clp 	courtesy1   "  ?id "  sOjanya )" crlf))
)
