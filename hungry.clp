
;##############################################################################
;#  Copyright (C) 2014 14anu21 
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu21 on 21.06.2014
;He is hungry about the results.
;वह परिणामों के बारे में भूखा है . (Translation before adding rule)
;वह परिणामों के बारे में उत्सुक है . 
(defrule hungry0
(declare (salience 2000));added by 14anu-ban-06 (08-12-2014) because of rule 'hungry1'
(id-root ?id hungry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse ?id1 noun)
(viSeRya-about_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungry.clp 	hungry0   "  ?id "  uwsuka )" crlf))
)

;@@@ Added by 14anu-ban-06 (29-01-2015)
;Digging the garden is hungry work. (cambridge)
;उद्यान खोदना थकाऊ कार्य है . (manual)
(defrule hungry2
(declare (salience 2100))
(id-root ?id hungry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 work)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakAU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungry.clp 	hungry2   "  ?id "  WakAU )" crlf))
)

;@@@ Added by 14anu-ban-06 (29-01-2015)
; She was so hungry for success that she'd do anything to achieve it. (cambridge)
;वह सफलता के लिए इतनी लालायित थी कि वह उसे हासिल करना कुछ भी करेगी .  (manual)
;The American people are extremely hungry for details.(COCA)
;अमरीका वासी लोग जानकारी के लिए अत्यधिक लालायित होते हैं . (manual)
(defrule hungry3
(declare (salience 2100))
(id-root ?id hungry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 success|detail|information)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAlAyiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungry.clp 	hungry3   "  ?id "  lAlAyiwa )" crlf))
)

;DEFAULT RULE
;$$$ Modified by 14anu-ban-06 (08-12-2014)
;@@@ Added by 14anu21 on 21.06.2014
;He is hungry.
;वह भूखा है.
(defrule hungry1
(id-root ?id hungry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUKA));modified meaning from 'BUKa' to 'BUKA' by 14anu-ban-06 (08-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungry.clp 	hungry1   "  ?id "  BUKA )" crlf))
)
