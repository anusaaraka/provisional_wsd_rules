
(defrule present0
(declare (salience 5000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id presents )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id upahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  present.clp  	present0   "  ?id "  upahAra )" crlf))
)


;past , present , future
;अतीत , व्रत्मान् ...
;@@@ Added by avni(14anu11)
(defrule present14
(declare (salience 5000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(or (viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id1))
(id-cat_coarse ?id noun)
(conjunction-components  ?id2 ?id3 ?id ?id4)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present14   "  ?id "  varwamAna )" crlf))
)

(defrule present1
(declare (salience 4900))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 buy)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeMta_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present1   "  ?id "  BeMta_vaswu )" crlf))
)

(defrule present2
(declare (salience 4800))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) a )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present2   "  ?id "  upahAra )" crlf))
)





;Modified by Meena(26.02.10); added (viSeRya-det_viSeRaNa ?id ?id1)) and commented (id-word ?id1 expensive)
;I gave her the present I bought for her .
(defrule present3
(declare (salience 4700))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 expensive)
(or (viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present3   "  ?id "  upahAra )" crlf))
)




;Salience reduced by Meena(30.8.10)
(defrule present4
(declare (salience 0))
;(declare (salience 4600))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present4   "  ?id "  upasWiwa )" crlf))
)

;"present","Adj","1.upasWiwa"
;I was not present when the telegram came.
;--"2.varwamAna"
;The present age is computer age.



;Added by Meena(30.8.10)
;Many sentences are required , in the present context , to test the rule for debugging . 
(defrule present05
(declare (salience 4000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?)
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)(viSeRya-viSeRaNa ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp   present05   "  ?id "  varwamAna )" crlf))
)




;
(defrule present5
(declare (salience 0))
;(declare (salience 4500))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAnakAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present5   "  ?id "  varwamAnakAla )" crlf))
)

;"present","N","1.varwamAnakAla"
;The present society is a sophisticated society.
;--"2.upahAra"
;This book was a present from my elder brother.
;


;Added by Sonam Gupta MTech IT Banasthali 2013
;May I present Professor Carter? [Veena mam Translation]
;क्या मैं प्रोफेसर कार्टर का परिचय करा दूँ ?
(defrule present6
(declare (salience 4400))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-kriyArWa_kriyA  ? ?id)(to-infinitive  ? ?id)(kriyA-to_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricaya_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present6   "  ?id "  paricaya_karA )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;The winners were presented with medals. [Veena mam Translation]
;विजेताओं को पदक प्रदान किए गए .
(defrule present7
(declare (salience 4300))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-subject  ?id ?)(kriyA-karma  ?id ?)(kriyA-with_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present7   "  ?id "  praxAna_kara )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;The documentary presented us with a balanced view of the issue. [Veena mam Translation]
;वृत्तचित्र में हमें समस्या के बारे में एक संतुलित विचार से अवगत कराया गया था .
(defrule present8
(declare (salience 4200))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-with_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avagawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present8   "  ?id "  avagawa_kara )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2013
;She presents the late-night news. [Veena mam Translation]
;वह देर रात के समाचार पेश करती है.
(defrule present9
(declare (salience 4100))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-kAlavAcI  ?id ?))(kriyA-object  ?id ?)(kriyA-subject  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present9   "  ?id "  peSa_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 6-1-2014
;Insisted on my being present. [Shikshyarthi kosh]
(defrule present10
(declare (salience 5000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upasWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present10   "  ?id "  upasWiwi )" crlf))
)


(defrule present11
(declare (salience 4000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present11   "  ?id "  xe )" crlf))
)

;"present","V","1.xe{upahAra}"
;--"2.xenA{upahAra}"
;My father presented me a bycycle on my birthday.
;--"3.xiKAnA"
;Present the entry-pass to go into the auditorium.
;--"4.praswuwa_karanA"
;The present cinema culture presents the abnormal development in the children.
;

;$$$ Modified by 14anu-ban-09 on 13-08-2014
;The system of units which is at present internationally accepted for measurement is the Système Internationale d' Unites (French for International System of Units), abbreviated as SI. [NCERT CORPUS]
;jo mApana ke lie anwarrARtrIyawA se svIkAra varwamAnakAla meM hE mAwraka kA nikAya esa AI ke jEsA safkRipwa PropN-système-PropN intarnESanoYlI P17 yUnAitsa yUnatsa ke intarnESanala nikAya ke lie PrAMsIsI, hE.
;jo mApana ke lie anwarrARtrIyawA se svIkAra varwamAna meM hE mAwraka kA nikAya esa AI ke jEsA safkRipwa PropN-système-PropN intarnESanoYlI P17 yUnAitsa yUnatsa ke intarnESanala nikAya ke lie PrAMsIsI, hE. [Own Manual]
;@@@ Added by 14anu13  on 02-07-14
;In former times there were pearl - banks in the bay of Sarandib ( Ceylon ) , but at present they have been abandoned. 
;प्राचीन काल में सरनदीव ( श्रीलंका ) की खाडी में मुक्ता - भंडार थे लेकिन वर्तमान में उनका परित्याग कर दिया गया है .
(defrule present12
(declare (salience 4500))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-at_saMbanXI ? ?id)(id-word =(- ?id 1) at))
(id-cat_coarse ?id adjective|noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varwamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp   present12   "  ?id "  varwamAna )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-10-2014)
;In 1676, he presented his law of elasticity, now called Hooke's law. [NCERT CORPUS]
;unhoMne san 1676 meM prawyAsWawA kA niyama praswuwa kiyA jo aba huka kA niyama kahalAwA hE. [NCERT CORPUS]

(defrule present13
(declare (salience 4000))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(kriyA-object  ?id ?id2)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-root ?id2 law)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present13   "  ?id "  praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;In this the Mahishmardini form as peaceful and the grandness of Varada form has been presented . [Tourism Corpus]
;इसमें दुर्गा के महिषमर्दिनी स्वरुप को शांत व वरद रूप की दिव्यता को प्रस्तुत किया गया है .  [Tourism Corpus]
;इसमें महिषमर्दिनी स्वरुप को शांत व वरद रूप की दिव्यता को प्रस्तुत किया गया है . [Self]
;Besides the statues of Brahma - Savitri , Laxmi - Narayan sitting on Barud , Uma - Maheshwar seated on Nandi etc . in the outer crevices of the temple , the scenes of contemporary social life have also been presented . [Tourism Corpus]
;मंदिर के बाह्य ताखों में ब्रम्हा-सावित्री , गरूड़ पर बैठे लक्ष्मी-नारायण , नंदी पर आसीन उमा-माहेश्वर आदि की प्रतिमाओं के अतिरिक्त मेवाड़ के तत्कालीन सामाजिक जीवन के दृश्यों को भी प्रस्तुत किया गया है . [Tourism Corpus]

(defrule present014
(declare (salience 4200))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 scene|form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present014   "  ?id "  praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;The speciality of the this world famous Shani Temple is that the stone statue of Shanidev located here is auspiciously present on a platform of a marble under the open sky without any umbrella or dome . [Tourism Corpus]
;विश़्व प्रसिद्ध इस शनि मंदिर की विशेषता यह है कि यहाँ स्थित शनिदेव की पाषाण प्रतिमा बगैर किसी छत्र या गुंबद के खुले आसमान के नीचे एक संगमरमर के चबूतरे पर विराजित है . [Tourism Corpus]

(defrule present15
(declare (salience 4200))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI  ?id ?id1)
(subject-subject_samAnAXikaraNa  ?id2 ?id)
(id-root ?id1 platform)
(id-root ?id2 statue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id virAjiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present15   "  ?id "  virAjiwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-03-2015)
;The windmill presented unexpected difficulties. 	[Report-Set 2]
;पवन चक्की ने अनपेक्षित कठिनाइयों का सामना किया . 		[Anusaaraka]

(defrule present16
(declare (salience 4200))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 windmill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp 	present16   "  ?id "  sAmanA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  present.clp      present16   "  ?id " kA )" crlf)
)
)

;@@@ Added by Manasa (Arsha sodha sansthan) 14-09-2015
;In such socities the analysis that we present in the book will not be apilicable.
;Ese samAjo me puswaka me praxarSiwa kiye gaye viSleRaNa lAgu nahIM howe heM. 
(defrule present17
(declare (salience 4200))
(id-root ?id present)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-subject  ?id ?id1)
(kriyA-in_saMbanXI ?id ?id2)
(id-root ?id2 book|concert|show|picture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  present.clp  present17   "  ?id "  praxarSiwa_kara )" crlf))
)

;LEVEL 
;
;
;"present"
;
;
;nIce xiye gae vAkyoM meM "present" Sabxa kA alaga sanxarBoM meM alaga arWa
;ho rahA hEM jEse :
;
;Examples :
;
;1.He presented her a gold watch.
;1.usane use sone kI GadZI xI{upahAra}     --- xenA
;2.We have to present our passport at the border.
;3.Army life presents many difficulties.
;3.sEnika jIvana aneka kaTinAIyAz upasWiwa karawA hE  --- upasWiwa_karanA
;4.The present situation in the city is bad.
;4.Sahara ke (mOjUxA) hAlAwa KarAba hEM                   --- Ajakala_ke
;5.She is preparing for the exams at present.
;5.vaha (varwamAnakAla) meM parIkRA kI wEyArI kara rahI hE         --- Ajakala
;
;agara hama "present" kA arWa "peSa_karanA" le, wo vaha (1-3) vAkyoM ke sanxarBoM
;meM uciwa ho sakawA hE. jEse :
;
;1.He presented her a gold watch.
;1.usane use sone kI GadZI peSa_kI
;2.We have to present our passport at the border.
;2.hameM sImA para apanA pAsaporta{pArapawra} peSa karanA hogA
;3.Army life presents many difficulties.
;3.sEnika jIvana aneka kaTinAIyAz peSa karawA hE
;
;lekina (4-5) vAkyoM ke sanxarBa meM "peSa_karanA" anuciwa howA hE. jEse :
;
;4.The present situation in the city is bad.
;4. * Sahara ke peSa_kie hAlAwa KarAba hEM
;5.She is preparing for the exams at present.
;5. * vaha peSakAla meM parIkRA kI wEyArI kara rahI hE
; 
;agara "present" kA arWa "praswuwa" xiyA jAe wo vaha saBI sanxarBoM meM uciwa
;howA hE.  jEse :
;
;1.He presented her a gold watch.
;1.usane use sone kI GadZI praswuwa kI
;2.We have to present our passport at the border.
;2.hameM sImA para apanA pAsaporta{pArapawra} praswuwa karanA hogA
;3.Army life presents many difficulties.
;
;3.sEnika jIvana aneka kaTinAIyAz praswuwa karawA hE
;4.She is preparing for the exams at present.
;4.vaha praswuwakAla meM parIkRA kI wEyArI kara rahI hE
;5.The present situation in the city is bad.
;5.Sahara ke praswuwa hAlAwa KarAba hEM
;
;
;aba hama "present" kA arWa `praswuwa' Ese aBivyakwa kara sakawe hEM :
;
;"present","V","1.praswuwa_kara"
;He presented her a gold watch.
;usane use sone kI GadZI praswuwa kI
;
;
;"present","N","1.praswuwa[kAla]"
;She is preparing for the exams at present.
;vaha praswuwakAla meM parIkRA kI wEyArI kara rahI hE
;
;"present","Adj","1.praswuwa"
;The present situation in the city is bad.
;Sahara ke praswuwa hAlAwa KarAba hEM
;
;sUwra : praswuwa`
;
;
;
