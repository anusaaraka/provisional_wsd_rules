;@@@ Added by 14anu-ban-06 (23-04-2015)
;No one should be immune from prosecution.(OALD)
;कोई भी  अभियोग से बचा हुआ नहीं रहना चाहिए . (manual)
(defrule immune1
(declare (salience 2000))
(id-root ?id immune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-from_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immune.clp 	immune1   "  ?id "  bacA_huA )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (23-04-2015)
;Adults are often immune to German measles. (OALD)
;अक्सर युवा जर्मनी के खसरे से प्रतिरक्षित हैं . (manual)
(defrule immune0
(declare (salience 0))
(id-root ?id immune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawirakRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immune.clp 	immune0   "  ?id "  prawirakRiwa )" crlf))
)
