
;@@@ Added by 14anu-ban-03 (15-04-2015)
;She was consoled by the news from home. [oald]
;वह घर के समाचार से प्रसन्न हुई गयी थी . [manual]
(defrule console2
(declare (salience 10))       
(id-root ?id console)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasanna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  console.clp 	console2   "  ?id "  prasanna_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (15-04-2015)
;An equipment console. [cald]  ;working on parser- 17
;उपकरण नियन्त्रण पट . [manual]  [suggested by chaitanya sir]
(defrule console3
(declare (salience 10))       
(id-root ?id console)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyanwraNa_pata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  console.clp 	console3   "  ?id "  niyanwraNa_pata )" crlf))
)

;------------------------------------------------Default rules--------------------------------------------------

;@@@ Added by 14anu-ban-03 (15-04-2015)
;He consoled her when her father passed away. [hinkhoj]
;जब उसके पिता गुजर  गए उसने उसको सान्त्वना दी.    [manual]
(defrule console0
(declare (salience 00))
(id-root ?id console)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAnwvanA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  console.clp  console0   "  ?id "  sAnwvanA_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (15-04-2015)
;The bust of Napoleon stood on a console. [hinkhoj]
;नेपोलियन की अर्धृप्रतिमा ढाँचे पर खडी हुई हैं . [manual]
(defrule console1
(declare (salience 00))
(id-root ?id console)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  console.clp  console1   "  ?id "  DAzcA )" crlf))
)


