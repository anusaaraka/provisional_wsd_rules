
;@@@ Added by 14anu-ban-11 on (22-04-2015)
;The wan sunlight of a winter’s morning.(oald)
;सरदी की सुबह की हल्की धूप .(self) 
(defrule wan1
(declare (salience 10))
(id-root ?id wan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 sunlight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wan.clp 	wan1   "  ?id "  halkA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (22-04-2015)
;She was looking a little wan.(coca)
;वह थोडी बीमार देख रही थी (self)
(defrule wan2
(declare (salience 20))
(id-root ?id wan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 little)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bImAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wan.clp 	wan2   "  ?id "  bImAra)" crlf))
)


;-------------------------------------- Default Rules --------------------------------

;@@@ Added by 14anu-ban-11 on (22-04-2015)
;She gave me a wan smile.(oald)
;उसने मुझे मुरझायी मुस्कराहट दी . (self)
(defrule wan0
(declare (salience 00))
(id-root ?id wan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muraJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wan.clp      wan0   "  ?id "  muraJA)" crlf))
)


