;@@@ Added by 14anu-ban-03 (03-04-2015)
;‘It's only a suspicion,’ she said, ‘nothing concrete.’ [oald]
;'यह सिर्फ शक  है,' उसने, 'कुछ भी  यथार्थपूर्ण नही कहा.' [manual]
(defrule concrete2
(declare (salience 5001))
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 nothing)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaWArWapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete2   "  ?id "  yaWArWapUrNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-04-2015)
;The pathway is formed from large pebbles set in concrete. [oald]
;मार्ग अधिक कंकडों से बनाया गया है जो कन्क्रीट से तैयार किया हैं. [manual]
(defrule concrete3
(declare (salience 5000))   
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kankrIta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete3   "  ?id "  kankrIta )" crlf))
)

;@@@ Added by 14anu-ban-03 (03-04-2015)
;Trees,books etc are concrete things. [hinkhoj]
;पेड, पुस्तक आदि भौतिक चीजें हैं . [manual]
(defrule concrete4
(declare (salience 5000))
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BOwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete4   "  ?id "  BOwika )" crlf))
)


;@@@ Added by 14anu-ban-03 (03-04-2015)
;A pile of concretes. [oald]
;पत्थरों का ढेर . [manual]
(defrule concrete5
(declare (salience 5000))   
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawWara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete5   "  ?id "  pawWara )" crlf))
)

;-----------------------------------------------Default rules----------------------------------

;"concrete","Adj","1.Tosa"
;Trees,books etc are concrete things.
;The police couldn't solve the case due to the lack of concrete evidence.
(defrule concrete0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (03-04-2015)
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Tosa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete0   "  ?id "  Tosa )" crlf))
)

;"concrete","N","1.rodZA"
;Modern buildings are mainly concrete.
(defrule concrete1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (03-04-2015)
(id-root ?id concrete)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rodZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concrete.clp 	concrete1   "  ?id "  rodZA )" crlf))
)

