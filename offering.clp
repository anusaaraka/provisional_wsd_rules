;@@@ Added by 14anu-ban-09 on (02-04-2015)
;Each household must make daily offerings to the gods. [oald]
;हर एक परिवार को देवताओं को रोज चढावे चढाने चाहिए .  [Manual]

(defrule offering1
(declare (salience 20))
(id-root ?id offering)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id2 god)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caDAvA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offering.clp 	offering1   "  ?id "  caDAvA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;The company is trying to generate interest in its new offerings. [oald]
;कम्पनी उनके नये प्रस्तावों में मुनाफ़ा कमाने का प्रयास कर रही है .  [Manual]

(defrule offering2
(declare (salience 20))
(id-root ?id offering)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 company|organization|organisation)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offering.clp 	offering2   "  ?id "  praswAva )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;NOTE-Example sentence need to be added.

(defrule offering0
(declare (salience 0000))
(id-root ?id offering)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offering.clp         offering0   "  ?id "  BeMta )" crlf))
)

