;@@@Added by 14anu-ban-02(23-04-2015)
;A police car drew abreast of us and signalled us to stop.[oald]	;run the sentence on parser no. 9
;पुलिस कार हमारे बराबर आई और हमें रुकने के लिए सङ्केत किया . [self]
(defrule abreast1
(declare (salience 100))	
(id-root ?id abreast)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 draw|come)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barAbara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abreast.clp 	abreast1 "  ?id "  barAbara )" crlf))
)

;@@@Added by 14anu-ban-02(23-04-2015)
;I try to keep abreast of any developments.[cald]	;run the sentence on parser no. 3
;मैं किसी भी गतिविधि के प्रति सचेत रहने का प्रयास करता हूँ.[self]
(defrule abreast2
(declare (salience 100))	
(id-root ?id abreast)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 keep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_prawi_sacewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abreast.clp 	abreast2  "  ?id "  ke_prawi_sacewa )" crlf))
)

;--------------------------------default_rules-------------------------------------

;@@@Added by 14anu-ban-02(23-04-2015)
;Cycling two abreast.[oald]	;run the sentence on parser no. 9
;साथ-साथ दो साइकिल चलाना  . [self]
(defrule abreast0
(declare (salience 0))	
(id-root ?id abreast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa-sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abreast.clp 	abreast0   "  ?id "  sAWa-sAWa )" crlf))
)
