;@@@ Added by 14anu-ban-10 on (18-11-2014)
;Reversing the direction of the current reverses the orientation of the needle.[ncert corpus]
;yaxi wAra meM XArA kI xiSA viparIwa kara xI jAe wo cumbakIya suI BI GUma kara viparIwa xiSA meM saMreKiwa ho jAwI hE.[ncert corpus]
(defrule reverse3
(declare (salience 5100))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  viparIwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse3   "  ?id "   viparIwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-02-2015)
;This is the reverse process of modulation.[ncert corpus]
;yaha moYdulana ke viparIwa prakriyA hE.[ncert corpus]
(defrule reverse4
(declare (salience 5200))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse4   "  ?id "   viparIwa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (07-03-2015)
;On the scale of a million years, the earth's magnetic fields has been found to reverse its direction.[NCERT CORPUS]
;yaha pAyA gayA hE ki 10 lAKa varRoM meM pqWvI ke cumbakIya kRewra kI xiSA ulata jAwI hE.[NCERT CORPUS]
(defrule reverse5
(declare (salience 5300))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 direction)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ulata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse5   "  ?id "   ulata_jA)" crlf))
)

;------------------------ Default Rules ----------------------

;"reverse","Adj","1.ultI"
;The three winners were announced in reverse order.
;She is going in the reverse direction.
(defrule reverse0
(declare (salience 5000))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ultI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse0   "  ?id "  ultI )" crlf))
)

;"reverse","N","1.viparIwa"
;He has done the reverse of what he was asked.
;He suffered some serious financial reverses.
;The reverse of a mountain is the area of rain shadow.
;The arms race has gone int
(defrule reverse1
(declare (salience 4900))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse1   "  ?id "  viparIwa )" crlf))
)

;"reverse","VT","1.ulatanA"
;The gate was open,so she reversed in.
;The Judge reversed the decision of a lower court.
;They are trying to reverse the decline of their father's company.
(defrule reverse2
(declare (salience 4800))
(id-root ?id reverse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reverse.clp 	reverse2   "  ?id "  ulata )" crlf))
)
