
;@@@Added by 14anu-ban-02(06-04-2015)
;It was just a bogus claim.[mw]
;यह सिर्फ एक झूठा दावा  था . [self]
(defrule bogus1
(declare (salience 100)) 
(id-root ?id bogus) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 claim)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id JUTA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bogus.clp  bogus1  "  ?id "  JUTA )" crlf)) 
) 

;-------------------------------------default_rules---------------------------------------
;@@@Added by 14anu-ban-02(06-04-2015)
;Sentence: The evidence was completely bogus.[mw]
;Translation: साक्ष्य पूरी तरह से नकली था . [self]
(defrule bogus0 
(declare (salience 0)) 
(id-root ?id bogus) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nakalI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bogus.clp  bogus0  "  ?id "  nakalI )" crlf)) 
) 
