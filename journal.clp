;@@@ Added by 14anu-ban-06 (05-02-2015)
;She kept a travel journal during her trip to South America.(cambridge)
;उसने अपनी दक्षिण अमेरिका की यात्रा के दौरान एक यात्रा की डायरी रखी .  (manual)
(defrule journal1
(declare (salience 2000))
(id-root ?id journal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 travel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAyarI))
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  journal.clp 	journal1   "  ?id "  dAyarI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  journal.clp      journal1   "  ?id1 " kA )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (06-02-2015)
;He kept a journal of his travels across Asia.(OALD)
;उसने अपनी एशिया की यात्रा के दौरान एक यात्रा की डायरी रखी .  (manual)
(defrule journal2
(declare (salience 2500))
(id-root ?id journal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 travel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAyarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  journal.clp 	journal2   "  ?id "  dAyarI )" crlf)
)
)

;---------------------- Default Rules -------------------
;@@@ Added by 14anu-ban-06 (05-02-2015)
;A trade journal.(cambridge)
;एक व्यापार पत्रिका . (manual)
(defrule journal0
(declare (salience 0))
(id-root ?id journal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawrikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  journal.clp 	journal0   "  ?id "  pawrikA )" crlf))
)

