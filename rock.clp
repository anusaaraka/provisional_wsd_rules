;@@@ Added by 14anu09 [01-07-14]
;She loves rock music.
;वह रॉक सङ्गीत को पसन्द करती है . (anusaaraka translation)
;उसे रॉक सङ्गीत पसन्द है.
(defrule rock2
(declare (salience 5000))
(id-root ?id rock)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
;(id-root ?id1 music|band|concert|show|world) remove the comment if overgeneralized
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roYka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rock.clp 	rock2   "  ?id "  roYka )" crlf))
)


;@@@ Added by 14anu-ban-10 on (06-02-2015)
;If the slope is of dense vegetation  , grass  , trees  , bushes and large rocks  .[tourism corpus]
;ढलान यदि घनी वनस्पति , घास , पेड़-पौधों , झाड़ियों और बड़े-बड़े पत्थरों वाली होती है ।[tourism corpus]
(defrule rock3
(declare (salience 5100))
(id-root ?id rock)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1 )
(id-root ?id1 large)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawWara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rock.clp 	rock3   "  ?id "  pawWara )" crlf))
)

;--------------------- Default Rules ---------------------
(defrule rock0
(declare (salience 5000))
(id-root ?id rock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cattAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rock.clp 	rock0   "  ?id "  cattAna )" crlf))
)

;"rock","VTI","1.hilanA"
;She sat rocking in her chair.
(defrule rock1
(declare (salience 4900))
(id-root ?id rock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rock.clp 	rock1   "  ?id "  hila )" crlf))
)

;"rock","VTI","1.hilanA"
;She sat rocking in her chair.
;--"2.hila_jAnA{xenA}"        
;The whole building rocked as the bomb exploded.
;The scandal about bribe rocked his life. 
;
;


