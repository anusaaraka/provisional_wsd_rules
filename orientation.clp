;@@@ Added by 14anu-ban-09 on (07-02-2015)
;Moral orientation was not related to moral stage. 	[COCA]
;नैतिक अनुकूलन नैतिक स्थिति से सम्बन्धित नहीं था .			[Manual]

(defrule orientation1
(declare (salience 1000))
(id-root ?id orientation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUlana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  orientation.clp 	orientation1   "  ?id "  anukUlana )" crlf))
)

;-----------------------------DEFAULT RULE--------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (07-02-2015)
;NOTE-Example sentence need to be added.


(defrule orientation0
(declare (salience 000))
(id-root ?id orientation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xigvinyAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  orientation.clp      orientation0   "  ?id "  xigvinyAsa )" crlf))
)


