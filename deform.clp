
(defrule deform0
(declare (salience 5000))
(id-root ?id deform)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id deformed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vikqwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  deform.clp  	deform0   "  ?id "  vikqwa )" crlf))
)


;"deformed","Adj","1.vikqwa"
;He has a deformed leg by birth.
;
;$$$ Modified by 14anu-ban-06 (21-10-2014)
;However, if you apply force to a lump of putty or mud, they have no gross tendency to regain their previous shape, and they get permanently deformed.(NCERT)
;जब एक पङ्क पिंड पर बल लगाते हैं तो पिंडक में अपना प्रारम्भिक आकार प्राप्त करने की प्रवृत्ति नहीं होती है और यह स्थायी रूप से विकृत हो जाता है.(NCERT)
;$$$ Modified by Pramila(BU) on 24-02-2014
(defrule deform1
(declare (salience 4900))
(id-root ?id deform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-after_saMbanXI  ?id ?id1)(and(kriyA-kriyA_viSeRaNa ?id ?id1)(id-root ?id1 permanently)))
;added (kriyA-kriyA_viSeRaNa ?id ?id1)(id-root ?id1 permanently) by 14anu-ban-06 (21-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikqwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deform.clp 	deform1   "  ?id "  vikqwa_ho )" crlf))
)

;"deform","V","1.vikqwa honA"
;The structure was deformed badly after the earthquake.
;
;

;@@@ Added by Pramila(Banasthali University) on 24-02-2014
;In our experience force is needed to push carry or throw objects deform or break them.
;हम सभी का यह अनुभव है कि वस्तुओं को धकेलने, ले जाने अथवा फेङ्कने, विकृत करने अथवा उन्हें तोडने के लिए बल की आवश्यकता होती है.
(defrule deform2
(declare (salience 100))
(id-root ?id deform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikqwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deform.clp 	deform2   "  ?id "  vikqwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (30-10-2014) 
;When a body is subjected to a deforming force, a restoring force is developed in the body.  [NCERT-CORPUS]
;जब किसी पिंड पर एक विरूपक बल लगाया जाता है तो उसमें एक प्रत्यानयन बल विकसित हो जाता है, जैसा कि पहले कहा जा चुका है.       [NCERT-CORPUS]

(defrule deform3
(declare (salience 100))
(id-root ?id deform)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  virUpaka))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deform.clp 	deform3   "  ?id "  virUpaka )" crlf))
)


;@@@ Added by 14anu-ban-04 (30-10-2014) 
;However, if two equal and opposite deforming forces are applied parallel to the cross-sectional area of the cylinder, as shown in Fig. 9.2(b), there is relative displacement between the opposite faces of the cylinder.  [NCERT-CORPUS]
; यदि दो बराबर और विरोधी विरूपक बल बेलन के अनुप्रस्थ परिच्छेद के समान्तर लगाए जाएँ, जैसा चित्र 9.2(b) में दिखाया गया है, तो बेलन के सम्मुख फलकों के बीच सापेक्ष विस्थापन हो जाता है.         [NCERT-CORPUS]

(defrule deform4
(declare (salience 150))
(id-root ?id deform)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  virUpaka))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deform.clp 	deform4  "  ?id "  virUpaka )" crlf))
)


