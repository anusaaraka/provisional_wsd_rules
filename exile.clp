;@@@ Added by 14anu-ban-04 (22-04-2015)
;He was publicly disgraced and sent into exile.                [oald]
;उसका सार्वजनिक रूप से  अपमान किया था  और  उसको निर्वासन में भेजा गया था .               [self]
(defrule exile2
(declare (salience 40))
(id-root ?id exile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI ?id1 ?id) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirvAsana))        
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exile.clp 	exile2   "  ?id "  nirvAsana )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-04-2015)
;Sita had gone to exile with Ram and Laxman.                [hinkhoj]
;सीता राम और लक्ष्मण के साथ प्रवास के लिए गई .                            [self]
(defrule exile3
(declare (salience 30))
(id-root ?id exile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI ?id1 ?id) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravAsa))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exile.clp 	exile3   "  ?id "  pravAsa )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-04-2015)
;Many Vietnamese exiles were living in France during this period.             [same clp file]
;isa xOrAna bahuwa se viyawanAmI nirvAsI PrAMsa meM raha rahe We.             [same clp file]
(defrule exile4
(declare (salience 30))
(id-root ?id exile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirvAsI))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exile.clp 	exile4  "  ?id "  nirvAsI )" crlf))
)

(defrule exile0
(declare (salience 10))                 ;salience reduced from '5000' to '10' by 14anu-ban-04 (22-04-2015)
(id-root ?id exile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeSa_nikAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exile.clp 	exile0   "  ?id "  xeSa_nikAlA )" crlf))
)

;"exile","N","1.xeSa_nikAlA"
;Ram was in exile for 10 years.
;
(defrule exile1
(declare (salience 4900))
(id-root ?id exile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirvAsiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exile.clp 	exile1   "  ?id "  nirvAsiwa_kara )" crlf))
)

;"exile","VT","1.nirvAsiwa_karanA"
;Ram was exiled for 10 years.
;
;LEVEL 
;Headword : exile
;
;Examples --
;
;"exile","N","1.nirvAsana" 
;After an exile of five years,he returned to India.
;vaha pAzca varRa nirvAsana ke paScAwa BArawa vApasa lOtA.
;--"2.nirvAsI"
;Many Vietnamese exiles were living in France during this period.
;isa xOrAna bahuwa se viyawanAmI nirvAsI PrAMsa mema raha rahe We.
;
;"exile","VT","1.xeSanikAlA_xenA" <--xeSa se nirvAsiwa kara xenA 
;He was exiled from his country because of his involvement in the security scam.
;surakRA GotAle meM usake SAmila hone ke kAraNa use apane xeSa se nirvAsiwa kara xiyA gayA.
;
;
;ukwa uxAharaNoM se lagawA hE ki 'exile' kA mUla arWa 'xeSa se bAhara' rahanA hE - yA apane mUla nivAsa se xUra rahanA. awaH hinxI meM isake liye 'nirvAsana' sahI prawIwa howA hE.
;
;anwarnihiwa sUwra ;
;
;                       nirvAsana {xUra rahanA}
;                         |
;             |-----------|----------|
;             |                      |
;           (saMjFA_prayoga)       (kriyA_prayoga)
;         jo xeSa se xUra rahe      kisI ko xeSa se xUra karanA
;              |                        |
;             nirvAsI                nirvAsiwa_karanA - xeSanikAlA_xenA
;
;
;             sUwra : xeSanikAlA_xenA^nirvAsI^nirvAsana  
;
