;@@@Added by 14anu-ban-02(26-02-2015)
;Sentence: Aggarwal , Khanni and Vaishyas have also remained merchants of this place .[total_tourism]
;Translation: कूर्मांचल के प्रसिद्ध कवि गुमानी की पैदाइश भी काशीपुर की ही है [total_tourism]
(defrule also0 
(declare (salience 0)) 
(id-root ?id also) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adverb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  also.clp  also0  "  ?id "  BI )" crlf)) 
) 
;@@@Added by 14anu-ban-02(26-02-2015)
;Also objects in motion generally experience friction, viscous drag, etc.[ncert 11_05]
;साथ ही, गतिशील वस्तुएं सदैव ही घर्षण बल, श्यान कर्षण आदि का अनुभव करती हैं .[ncert]
(defrule also1 
(declare (salience 10)) 
(id-root 1 also) 
?mng <-(meaning_to_be_decided 1) 
(id-cat_coarse 1 adverb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng 1 sAWa_hI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  also.clp  also1  "  1 "  sAWa_hI )" crlf)) 
) 

