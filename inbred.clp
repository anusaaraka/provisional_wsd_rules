;@@@ Added by 14anu-ban-06 (06-04-2015)
;An inbred family.(cambridge)
;सम्बन्धित परिवार . (manual) 
(defrule inbred1
(declare (salience 2000))
(id-root ?id inbred)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 family)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inbred.clp 	inbred1   "  ?id "  sambanXiwa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (06-04-2015)
;An inbred sense of right and wrong.(cambridge)
;सही और गलत की सहज संवेदना .(manual) 
(defrule inbred0
(declare (salience 0))
(id-root ?id inbred)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inbred.clp 	inbred0   "  ?id "  sahaja )" crlf))
)
