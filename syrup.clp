
;@@@ Added by 14anu-ban-11 on (05-02-2015)
;The tinned pineapple in syrup is a favourite food of small children.(hinkhoj) 
;चाशनी में टीन का अनन्नास छोटे बच्चों का एक प्रिय आहार है . (manual)
(defrule syrup0
(declare (salience 10))
(id-root ?id syrup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cASanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrup.clp 	syrup0   "  ?id "  cASanI )" crlf))
)

;@@@ Added by 14anu-ban-11 on (05-02-2015)
;For ginger syrup.(oald)
;अदरख के शर्बत के लिए .(manual) 
(defrule syrup1
(declare (salience 20))
(id-root ?id syrup)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 ginger)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarbawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrup.clp 	syrup1   "  ?id "  Sarbawa )" crlf))
)

