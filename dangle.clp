;@@@ Added by 14anu-ban-04 (12-03-2015)
;He dangled the puppet in front of the children.                 [cald]
;उसने बच्चों के सामने  कठपुतली  हिलाई .                                      [self]
;She dangled her car keys nervously as she spoke.               [oald]
;उसने  अपनी गाड़ी की चाबियाँ हिलाईं जब वह घबराते हुए बोली .                       [self]
(defrule dangle1
(declare (salience 20))
(id-root ?id dangle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dangle.clp 	dangle1   "  ?id "  hilA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (12-03-2015) 
;A single light bulb dangled from the ceiling.                 [oald]
;एक विद्युत बल्ब  भीतरी छत से लटका .                                       [self]
;Loose electric wires were dangling from the wall.              [cald]
;बिजली के ढीले  तार दीवार से लटक रहे थे .                                    [self]
(defrule dangle0
(declare (salience 10))
(id-root ?id dangle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dangle.clp 	dangle0   "  ?id "  lataka )" crlf))
)
