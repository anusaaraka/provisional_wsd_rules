;@@@Added by 14anu-ban-08 (09-03-2015)    ;Run on parser 4
;He made several mocking asides about the inadequacy of women.[oald]
;उसने स्त्रियों की अयोग्यता के बारे में कई  उपहासपूर्ण जनान्तिक बनाए . [self]
(defrule mock2
(declare (salience 5001))
(id-root ?id mock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 aside)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upahAsapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mock.clp 	mock2   "  ?id "  upahAsapUrNa )" crlf))
)

;@@@Added by 14anu-ban-08 (27-03-2015)
;The new exam mocked the needs of the majority of children.  [oald]
;नई परीक्षा ने अधिकांश बच्चो की जरूरतों का तिरस्कार किया.  [self]
(defrule mock3
(declare (salience 4908))
(id-root ?id mock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 exam)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wiraskAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mock.clp 	mock3   "  ?id "  wiraskAra_kara )" crlf))
)

;@@@Added by 14anu-ban-08 (27-03-2015)
;You can mock, but at least I’m willing to have a try!  [oald]
;तुम चाहे मेरा कितना भी मजाक उड़ा लो परन्तु मुझमें प्रयास करने की इतनी हिम्मत तो हैं.  [self]
(defrule mock4
(declare (salience 4908))
(id-root ?id mock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  majAka_udZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mock.clp 	mock4   "  ?id "   majAka_udZA )" crlf))
)

;------------------------ Default Rules ----------------------

;"mock","Adj","1.banAvatI"
;He laughed a mock laughter to irritate them further.
(defrule mock0
(declare (salience 5000))
(id-root ?id mock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banAvatI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mock.clp 	mock0   "  ?id "  banAvatI )" crlf))
)

;"mock","V","1.ciDZAnA"
;He would not give up a chance to mock ever.
(defrule mock1
(declare (salience 4900))
(id-root ?id mock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mock.clp 	mock1   "  ?id "  ciDZA )" crlf))
)
