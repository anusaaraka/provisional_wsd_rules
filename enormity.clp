;$$$ Modified by 14anu-ban-04 (16-04-2015)
;@@@ Added by 14anu03 on 17-june-14
;The enormity of Stalin's purges astounds me.
;स्तलिं के शुद्ध करों की महानता मुझे चकित करती है .
(defrule enormity2
(declare (salience 90))                             ;salience reduced from '7001' to '90' by 14anu-ban-04 on (16-04-2015)         
(id-root ?id enormity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)                              ;added by 14anu-ban-04 (16-04-2015)
(viSeRya-of_saMbanXI ?id ?id1)
(id-word ?id1 purges|army)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahAnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enormity.clp      enormity2   "  ?id "  mahAnawA)" crlf))
)

;@@@  Added by 14anu-ban-04 (16-04-2015)
;The enormities of the Hitler regime.              [oald]
;हिटलर दौर के अत्याचार .                                [self]
(defrule enormity3
(declare (salience 100))
(id-root ?id enormity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(viSeRya-of_saMbanXI ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id2 hitler|Hitler)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyAcAra))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enormity.clp      enormity3   "  ?id "   awyAcAra)" crlf))
)

;@@@  Added by 14anu-ban-04 (16-04-2015)
;It’s difficult to grasp the sheer enormity of the tragedy.                [oald]
;दुःखद घटना की पूर्णतया हकीकत  को समझना  मुश्किल है .                                   [self]
(defrule enormity4
(declare (salience 100))
(id-root ?id enormity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 attack|tragedy|crime|decision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hakIkawa))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enormity.clp      enormity4  "  ?id "   hakIkawa)" crlf))
)

;@@@  Added by 14anu-ban-04 (16-04-2015)
;In Jainism killing of animals is an enormity.                 [hinkhoj]
;जॆनिजम में पशुओं की हत्या एक महापाप है . 
(defrule enormity5
(declare (salience 100))
(id-root ?id enormity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 killing|crime|murder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahApApa))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enormity.clp      enormity5  "  ?id "   mahApApa)" crlf))
)

;$$$ Modified by 14anu-ban-04 (16-04-2015)
;@@@ Added by 14anu03 on 17-june-14
;The enormity of the elephant astounded me.
;हाथी की विशालता ने मुझे चकित किया .
(defrule enormity1
(declare (salience 10))                       ;salience reduced from '6001' to '10' by 14anu-ban-04 on (16-04-2015)
(id-root ?id enormity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)                          ;added by 14anu-ban-04 on (16-04-2015)
;(viSeRya-of_saMbanXI ?id ?id1)                   ;commented by 14anu-ban-04 on (16-04-2015)
;(id-word ?id1 elephant|giraffe|dynasore)         ;commented by 14anu-ban-04 on (16-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAlawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enormity.clp      enormity1   "  ?id "  viSAlawA)" crlf))
)
