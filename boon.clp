;@@@Added by 14anu-ban-02(06-04-2015)
;A softhearted man who finds it hard to deny any boon, whether it be for friend or stranger. [mw]
;एक कोमल हृदय का  आदमी जिसे  किसी भी अनुग्रह को  इनकार करना कठिन लगता है, चाहें वह  मित्र के लिए हो  या अजनबी के लिए .[self]
(defrule boon2
(declare (salience 100))
(id-root ?id boon)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 deny)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anugraha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boon.clp 	boon2   "  ?id "  anugraha )" crlf))
)


;-----------------------------default_rules---------------------------------
;"boon","Adj","1.KuSamijZAjZa"
;They are both boon couples.
(defrule boon0
(declare (salience 0))	;salience reduce to 0 from 5000 by 14anu-ban-02(06-04-2015)
(id-root ?id boon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuSamijZAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boon.clp 	boon0   "  ?id "  KuSamijZAjZa )" crlf))
)

;"boon","N","1.varaxAna"
;Parks are a great boon to the children in big cities. 
(defrule boon1
(declare (salience 0))	;salience reduce to 0 from 4900 by 14anu-ban-02(06-04-2015)
(id-root ?id boon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varaxAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boon.clp 	boon1   "  ?id "  varaxAna )" crlf))
)

