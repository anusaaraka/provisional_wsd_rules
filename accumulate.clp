;@@@ Added by 14anu-ban-02 (31-10-2014)
;Sentence: We have accumulated a great amount of evidence.[oald]
;Translation: हमने अधिक मात्रा में साक्ष्य एकत्रित कर लिये हैं.[manual]
(defrule accumulate0 
(declare (salience 0)) 
(id-root ?id accumulate) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ekawriwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  accumulate.clp  accumulate0  "  ?id "  ekawriwa_kara )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (31-10-2014)
;Sentence: By the nineteenth century, enough evidence had accumulated in favor of atomic hypothesis of matter.[ncert]
;Translation: उन्नीसवीं शताब्दी तक पदार्थ की परमाण्वीय परिकल्पना के समर्थन में काफी साक्ष्य एकत्रित हो गए थे.[ncert]
(defrule accumulate1 
(declare (salience 100)) 
(id-root ?id accumulate) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject ?id ?s)
(id-root ?s ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-cat_coarse ?id verb)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ekawriwa_ho)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  accumulate.clp  accumulate1  "  ?id "  ekawriwa_ho )" crlf)) 
) 
