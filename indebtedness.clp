;@@@ Added by 14anu-ban-06 (07-04-2015)
;Personal indebtedness. (OALD)
;वैयक्तिक ऋणग्रस्तता .(manual) 
;Corporate indebtedness. (OALD)
;निगमित ऋणग्रस्तता . (manual) 
(defrule indebtedness1
(declare (salience 2000))
(id-root ?id indebtedness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 private|personal|corporate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id qNagraswawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indebtedness.clp 	indebtedness1   "  ?id "  qNagraswawA )" crlf))
)

;@@@ Added by 14anu-ban-06 (07-04-2015)
;Today we are living in an era of indebtedness. (OALD)
;आज हम ऋणग्रस्तता के एक युग में रह रहे हैं . (manual)
(defrule indebtedness2
(declare (salience 2100))
(id-root ?id indebtedness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 era)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id qNagraswawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indebtedness.clp 	indebtedness2   "  ?id "  qNagraswawA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (07-04-2015)
;His deep indebtedness to Karen Burton is acknowledged in this book. (OALD)
;उसने इस पुस्तक में  केरन बर्टन के लिए गहरा आभार  स्वीकार किया है . (manual)
(defrule indebtedness0
(declare (salience 0))
(id-root ?id indebtedness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ABAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indebtedness.clp 	indebtedness0   "  ?id "  ABAra )" crlf))
)
