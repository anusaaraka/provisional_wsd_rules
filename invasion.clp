;@@@ Added by 14anu-ban-06 (13-03-2015)
;The actress described the photographs of her as an invasion of privacy.(OALD)
;अभिनेत्री ने उसके फोटो को गोपनीयता में हस्तक्षेप की तरह  बताया. (manual)
(defrule invasion1
(declare (salience 2000))
(id-root ?id invasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 privacy|secrecy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haswakRepa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invasion.clp 	invasion1   "  ?id "  haswakRepa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (13-03-2015)
;Farmers are struggling to cope with an invasion of slugs. (OALD)
;किसान घोंघों के हमले से मुक़ाबला करने के लिए जूझ रहे हैं . (manual)
(defrule invasion0
(declare (salience 0))
(id-root ?id invasion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hamalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invasion.clp         invasion0   "  ?id "  hamalA )" crlf))
)

