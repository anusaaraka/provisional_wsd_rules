
(defrule of0
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) because)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_vajaha_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of0   "  ?id "  kI_vajaha_se )" crlf))
)

;
(defrule of1
(declare (salience 4900))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAhara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of1   "  ?id "  ke_bAhara )" crlf))
)

(defrule of2
(declare (salience 4800))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) accept|acknowledge|add|admit|agree|allege|announce|answer|argue|arrange|assert|assume|assure|believe|boast|check|claim|comment|complain|concede|conclude|confirm|consider|contend|convince|decide|demonstrate|deny|determine|discover|dispute|doubt|dream|elicit|ensure|estimate|expect|explain|fear|feel|figure|find|foresee|forget|gather|guarantee|guess|hear|hold|hope|imagine|imply|indicate|inform|insist|judge|know|learn|maintain|mean|mention|note|notice|notify|object|observe|perceive|persuade|pledge|pray|predict|pretend|promise|prophesy|prove|read|realize|reason|reassure|recall|reckon|record|reflect|remark|remember|repeat|reply|report|require|resolve|reveal|say|see|sense|show|state|suggest|suppose|swear|teach|tell|think|threaten|understand|vow|warn|wish|worry|write)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_kA_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of2   "  ?id "  isa_kA_ki )" crlf))
)




;Modified by Meena(13.4.11)
;I can see I will have to revise my opinions of his abilities now.
;$$$ Modified by Shirisha Manju 17-4-14 -- added '|talk|hear|think|complain' to the list
;added 'speak' in the list by Shirisha Manju 19-4-14 suggested by Sukhada
;You spoke of pesticide.
(defrule of3
(declare (salience 4700))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) inform|remind|opinion|talk|hear|think|complain|speak)     
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of3   "  ?id "  ke_bAre_meM )" crlf))
)

;Removed by 14anu-ban-09 on (27-01-2015)
;NOTE-The meaning and constraints both are not matching with the example sentence.
;$$$ Modified by 14anu09[26-06-14]
;We can still inform the public of the value of silence.
;हम लोगों को शांति के बारे में बता सकते है.
;(defrule of4
;(declare (salience 4000));salience reduced because of rule of5 by 14anu09
;(id-root ?id of)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) what|when|where|why|how|who)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id usa_kA_ki))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of4   "  ?id "  usa_kA_ki )" crlf))
;)


;$$$ Modified by Bhagyashri Kulkarni (25-10-2016)
;Because she is interested in the progress of her students. (rapidex)
;क्योंकि वह उसके विद्यार्थियों की प्रगति में रुचि रखनेवाला है .
(defrule of5
(declare (salience 4500))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) whom|her) ; her is added in the word by sukhada for the sentence Of her childhood we know very little.
(not (id-word =(- ?id 1) both|use|progress)) ;Both of her parents are from India. ;added 'progress' by Bhagyashri Kulkarni (25.10.2016). 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of5   "  ?id "  ke_bAre_meM )" crlf))
)

;Of whom do you speak?
(defrule of6
(declare (salience 4400))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) hers)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of6   "  ?id "  - )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (10-11-2014)
;[Counter Example-NCERT CORPUS] ###########We know from experience that a glass of ice-cold water left on a table on a hot summer day eventually warms up whereas a cup of hot tea on the same table cools down.############  [NCERT CORPUS]
;apane anuBavoM se hama yaha jAnawe hEM ki kisI wapwa garmI ke xina eka meja para raKA barPa ke SIwala jala se BarA gilAsa aMwawogawvA garma ho jAwA hE jabaki wapwa cAya se BarA pyAlA usI meja para TaMdA ho jAwA hE. [NCERT CORPUS]
(defrule of7
(declare (salience 4300))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id =(- ?id 1))(preposition ?id)) ;added by 14anu-ban-09 on (10-11-2014)
(id-word =(- ?id 1) cup|bag|bottle|glass|box)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
;(assert (id-wsd_root_mng ?id Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of7   "  ?id "  kA )" crlf))
)

(defrule of8
(declare (salience 4200))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) majority|plenty|best|all|some|both|many|none|few|much|hundreds|thousands|kilos|liters)
(id-word =(+ ?id 1) them)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of8   "  ?id "  meM_se )" crlf))
)

(defrule of9
(declare (salience 4100))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) majority|plenty|best|all|some|both|many|none|few|much|hundreds|thousands|kilos|liters)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of9   "  ?id "  - )" crlf))
)

;$$$ Modified by Shirisha Manju 17-4-14 -- added 'ahead|aware|afraid' in the list and used 'root' fact instead 'word'
;added 'die|scare' in the list Suggested by Chaitanya Sir
;I am dying of stomach pain.  We are scared of death.
;It is full of advertisemnets.
;He is ahead of you
;She is full of bright ideas.  [Verified sentences]
;वह बढ़िया विचारों से भरी हुई है . 
;He was well aware of the problem. [Verified sentences]
;वह समस्या से अच्छी तरह अवगत है 
(defrule of10
(declare (salience 4000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) full|ahead|aware|afraid|die|scare|relieve|make|consist|frighten|beg|clear|empty|drain|deprive|cheat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of10   "  ?id "  se )" crlf))
)

(defrule of13
(declare (salience 3700))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) proud)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of13   "  ?id " para )" crlf))
)

;We are proud of your achievements
(defrule of14
(declare (salience 3600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) kind )
(id-word =(+ ?id 1) you)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of14   "  ?id "  kA )" crlf))
)

;$$$ Modified by Shirisha Manju 17-4-14 -- added 'fond' in the list and used root fact intead word 
;He is fond of sweets.
(defrule of16
(declare (salience 3400))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) kind|type|clever|unwise|fond)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of16   "  ?id "  kA )" crlf))
)

(defrule of17
(declare (salience 3300))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) millions|hundreds|thousands|tens|crores|lakhs|billions|million|billion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of17   "  ?id "  - )" crlf))
)

;It is made up of millions of cells.

;$$$ modified by 14anu26   [24-06-14] - added sentence with translation  
;Three of her novels have been adapted for television. 
;उसके उपन्यासों में से तीन दूरदर्शन के लिए ढाली गयी है .
;Note: Below rule should use 'part_of_rel' using mrs of Ace parser
;added 'most|many|few' in the list by Roja. Suggested by Chaitanya Sir and Soma Mam.
;added cat_coarse fact by Roja. 
(defrule of18
;(declare (salience 3200))
(declare (salience 5000))     ;salience is changed by 14anu26
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(or (id-word =(- ?id 1) some|none|more|most|many|few)
    (id-cat_coarse =(- ?id 1) number) 
)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of18   "  ?id "  meM_se )" crlf))
)

(defrule of19
(declare (salience 3100))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) integer|whole number|number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of19   "  ?id "  meM_se )" crlf))
)

(defrule of20
(declare (salience 3000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) all )
(id-word =(+ ?id 1) all)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of20   "  ?id "  - )" crlf))
)


;from_list(previous_word,pron_of_meM_se.dat)	meM_se	0
;One of those, etc.
;One friend of mine told me this.
;Some friends of mine
;Any friend of mine
;No friend of mine can do this mistake.
;This rule need to be modified according to above examples
;determiner presence is neccessary to modify this rule.
(defrule of22
(declare (salience 2800))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) friend )
(id-word =(+ ?id 1) mine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of22   "  ?id "  meM_se )" crlf))
)

(defrule of23
(declare (salience 2700))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) mine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of23   "  ?id "  - )" crlf))
)

;$$$ Modified by Shirisha Manju 19-4-14
;used 'id-root' fact instead 'id-word' fact
;She is full of bright ideas.  [Verified sentences]
;वह बढ़िया विचारों से भरी हुई है . 
;He was well aware of the problem. [Verified sentences]
;वह समस्या से अच्छी तरह अवगत है .
(defrule of25
(declare (salience 2500))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
;(or (kriyA-of_saMbanXI ?id1 ?)(viSeRya-of_saMbanXI ?id1 ?)) ;added by Shirisha Manju 19-4-14
(pada_info (group_head_id ?sam)(preposition  ?id)) ; added by Shirisha Manju 16-6-17
(kriyA-of_saMbanXI ?id1 ?sam) ;added by Shirisha Manju 19-4-14
(id-root ?id1 relieve|die|make|drain|deprive|cheat|frighten|full|aware|afraid|ahead|clear|empty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of25   "  ?id "  se )" crlf))
)

;Splitted above rule and below rule by Roja (13-06-14). Suggested by Chaitanya Sir.### Counter Ex: According to this figure, you need to configure this software. 
(defrule of25_1
(declare (salience 2500))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?) ;added by Shirisha Manju 19-4-14
;(id-root ?id1 cheat|frighten|full|aware|afraid|ahead|clear|empty)
(id-root ?id1 aware)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of25_1   "  ?id "  se )" crlf))
)

(defrule of26
(declare (salience 2400))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) make)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of26   "  ?id "  kA )" crlf))
)

(defrule of27
(declare (salience 2300))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) fool|idiot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of27   "  ?id "  - )" crlf))
)

(defrule of28
(declare (salience 2200))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) kilo|kilos|meters|meter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of28   "  ?id "  - )" crlf))
)

(defrule of29
(declare (salience 2100))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) all)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of29   "  ?id "  meM_se )" crlf))
)

(defrule of30
(declare (salience 2000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) a|an|the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of30   "  ?id "  kA )" crlf))
)

(defrule of31
(declare (salience 1900))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) which)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of31   "  ?id "  meM_se )" crlf))
)

(defrule of33
(declare (salience 1700))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) which)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of33   "  ?id "  se )" crlf))
)


;$$$ Modified by 14anu21 on 13.06.2014 by adding:
;(kriyA-object ?id3 ?)
;(not(id-root ?id3 sink|drink))
;### He sank three bottles of milk.
;### उसने दूध तीन बोतलें पीं . (Translation before modification)
;$$$ Modified by 14anu-ban-09 on (07-11-2014)
;##### [COUNTER EXAMPLE] ######### We also know that in the case of glass tumbler of ice cold water, heat flows from the environment to the glass tumbler, whereas in the case of hot tea, it flows from the cup of hot tea to the environment.[NCERT CORPUS]
;hama yaha BI jAnawe hEM ki gilAsa meM Bare SIwala jala ke prakaraNa meM URmA paryAvaraNa se gilAsa meM pravAhiwa howI hE, jabaki garma cAya ke prakaraNa meM URmA garma cAya ke pyAle se paryAvaraNa meM pravAhiwa howI hE. [NCERT CORPUS]
;Added by Meena(17.9.09)
;Would you like a cup of tea.
(defrule of34
(declare (salience 4600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id)) ;added by 14anu-ban-09 on (07-11-2014)
(id-root ?id2 tea|coffee|milk)
(viSeRya-of_saMbanXI ?id1 ?id2)
(kriyA-object  ?id3 ?id1) ;added by 14anu-ban-09 on (07-11-2014)
(id-root ?id3 like) ;added by 14anu-ban-09 on (07-11-2014)
(not(id-root ?id3 sink|drink))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp        of34   "  ?id "  - )" crlf))
)


(defrule sh-of0
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(id-root =(- ?id 1) invite)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp        sh-of0   "  ?id "  ko )" crlf))
)
;I should have talked to you before the inviting of John .
 

;Added by Meena(2.3.10)
;She said she did not approve of my behavior .
(defrule approve_of0
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) approve)
(kriyA-of_saMbanXI  =(- ?id 1) ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp        approve_of0   "  ?id "  ko )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (12-12-2014)
;Changed meaning from 'ke' to 'kA'
;@@@ Added by 14anu09[26-06-14]
; You can get an idea of how bad the situation is in the office.
;आप दफ्तर के माहौल का अंदाजा लगा सकते हैं
;आप दफ्तर की बुरी स्थिति का अंदाजा लगा सकते हैं. [Translation improved] ;by 14anu-ban-09 on (12-12-2014)
(defrule of036
(declare (salience 4900))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) idea)
(id-root =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of036   "  ?id "  kA )" crlf))
)
;

;$$$ Modified by 14anu-ban-01 on (13-04-2016): removed 'development' from the list
;###Counter Example### The parents documented every step of their child's development. [sd_verified]
;माँ बाप ने उनके बच्चे के विकास का प्रत्येक चरण लिखा. [sd_verified]
;@@@ Added by 14anu-ban-09 on (07-11-2014)
;He was the fourth player chosen in the 2007 draft, but the first of his class to reach the major leagues. [oald] ;added by 14anu-ban-09 on (31-03-2015)
;वह 2007 दल में चुना गया चौथा खिलाड़ी था, परन्तु  मुख्य संघ  तक  पहुँचने के लिए अपने वर्ग में प्रथम था  .                                               [self]	;added by 14anu-ban-09 on (31-03-2015)
;Considered a cradle of civilization, Ancient Egypt experienced some of the earliest developments of writing, agriculture, urbanisation, organised religion and central government in history.		[Report set 3]	;added by 14anu-ban-09 on (14-03-2015)
;समाज को विकास की भूमि मानना,  ऎन्चन्ट इजिप्ट कुछ  शुरू शुरू के विकासों में लेखन , कृषि वर्ग, नगरीकरण, संगठित धर्म और  इतिहास में केंद्रीय-सरकार का अनुभव किया . 	[self]	Tranlation needs futher improvement ;added by 14anu-ban-09 on (14-03-2015)
;सभ्यता के उद्गम पर गौर किया जाए तो वृत्तांत में ऎन्चन्ट इजिप्ट ने लेखन, कृषि वर्ग, नगरीकरण, संगठित धर्म और  केंद्रीय-सरकार के सबसे शुरुआती विकासों का अनुभव किया. 	Translation improved by 14anu-ban-01"	;###Counter Example###
;He is a master of disguise.                     [oald]	;added by 14anu-ban-09 on (03-03-2015)
;वह वेश परिवर्तन करने में  माहिर है .                        [self] ;added by 14anu-ban-09 on (03-03-2015)
;The commonly used property is variation of the volume of a liquid with temperature. [NCERT CORPUS]
;sAmAnya upayoga meM Ane vAlA guNa "wApa ke sAWa kisI xrava ke Ayawana meM parivarwana" howA hE. [NCERT CORPUS]
(defrule of37
(declare (salience 5001))	;salience increased '2000' to '5001' by 14anu-ban-09 on (14-03-2015)
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-of_saMbanXI  ? ?id1)
(id-root ?id1 volume|disguise|class)	;added 'disguise' by 14anu-ban-09 on (03-03-2015) ;added 'development' by 14anu-ban-09 on (14-03-2015) ;added 'class' by 14anu-ban-09 on (31-03-2015)	;removed 'development' by 14anu-ban-01 on (11-04-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of37   "  ?id "  meM )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-11-2014)
;I was disturbed by the news of her death. 	[disturb.clp]	;added by 14anu-ban-09 on (13-03-2015)
;उसकी मृत्यु के समाचार से मैं परेशान हो गया था . 		[manual]	;added by 14anu-ban-09 on (13-03-2015)	
;The units of all other physical quantities can be expressed as combinations of the base units. [NCERT CORPUS]
;inake awirikwa anya saBI BOwika rASiyoM ke mAwrakoM ko mUla mAwrakoM ke saMyojana xvArA vyakwa kiyA jA sakawA hE. [NCERT CORPUS]
(defrule of38
(declare (salience 4501))	;salience increased '2500' to '4501' by 14anu-ban-09 on (13-03-2015)
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-of_saMbanXI  ? ?id1)
(id-root ?id1 quantity|death)		;added 'death' by 14anu-ban-09 on (13-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of38   "  ?id "  kA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (28-02-2015)
;###[COUNTER EXAMPLE]### He dropped dead on the squash court at the age of 43.   [cald]
;###[COUNTER EXAMPLE]### ४३ वर्ष की आयु में स्क्वाश कोर्ट में उसकी  मृत्यु  अचानक  हो गयी .	 [Self]
;@@@ Added by 14anu01 on 26-06-2014
;Do you have change of rupees 1000.
;क्या आपके  1000 रुपए के खुले पैसे है . 
(defrule of39
(declare (salience 2000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))	;Added by 14anu-ban-09 on (28-02-2015)
;(id-cat_coarse =(+ ?id 1) number)	;commented by 14anu-ban-09 on (28-02-2015)
(viSeRya-of_saMbanXI  ? ?id1)		;Added by 14anu-ban-09 on (28-02-2015)
(id-root ?id1 rupee)			;Added by 14anu-ban-09 on (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of39   "  ?id "  ke )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-01-2015)
;Lisa has grown out of her old shoes. [grow.clp]
;लीसा अपने पुराने जूतों के लिए बहुत बड़ी हो गई है. [Self]

(defrule of40
(declare (salience 5600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI ?id1 ?)
(id-root ?id1 grow)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ke_lie))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  of.clp       of40   "  ?id "  ke_lie )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-01-2015)
;Please tell him I will call him on the dot of twelve.	[OALD]
;कृपया उसको बताइए कि मैं ठीक बारह बजे उसे  मिलने बुलाऊँगा.		[Manual]
(defrule of41
(declare (salience 5600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-of_saMbanXI  ?id2 ?id1)	;modified '?' to '?id2' by 14anu-ban-09 on (28-02-2015)
(id-cat_coarse ?id1 number)
(id-root ?id2 dot)			;added by 14anu-ban-09 on (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  of.clp       of41   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-03-2015)
;The coffee machine suddenly ejected a handful of coins.	[CALD]
;कॉफी मशीन ने अचानक मुट्ठी भर  सिक्के निकाले . 		[Manual]

(defrule of44
(declare (salience 5600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-of_saMbanXI  ?id2 ?id1)
(id-root ?id2 handful)
(id-root ?id1 coin)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  of.clp       of44   "  ?id "  - )" crlf))
)


;@@@ Added by 14anu-ban-09 on (04-02-2015)
;There were impressions around her ankles made by the tops of her socks. 	[Cambridge]
;उसके टखने के इधर उधर निशान उसके मोजों के ऊपरी सिरे से बन गए थे . 				[Manual]

(defrule of42
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(viSeRya-of_saMbanXI  ? ?id1)
(id-root ?id1 sock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of42   "  ?id "  kA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-02-2015)
;She gave up smoking for the sake of her health. 	[oald.com]
;अपने स्वास्थ्य के खातिर उसने  धूम्रपान छोड दिया . 			[Manual]

(defrule of43
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(id-cat_coarse ?id preposition)
(viSeRya-of_saMbanXI  ?id1 ?)
(id-root ?id1 sake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of43   "  ?id "  kA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-03-2015)
;He has a deep distrust of all modern technology.  	[oald.com]
;उसे सभी आधुनिक प्रौद्योगिकी पर गहरा अविश्वास है .  		[Manual]
(defrule of45
(declare (salience 5000))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(id-cat_coarse ?id preposition)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 distrust)
(id-root ?id2 technology)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp 	of45   "  ?id "  para )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;Peace and harmony should be maintained for the common good of all. [Hinkhoj]
;अमन और एकता सभी के सार्वजनिक हित के लिए बनाए जाने चाहिए.		         [Manual]

(defrule of46
(declare (salience 2500))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-of_saMbanXI  ?id2 ?id1)
(id-root ?id1 all)
(id-root ?id2 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of46   "  ?id "  kA )" crlf))
)

;@@@ Added by Rajini (15-07-2016)
;Out of the 24 Tirthankaras the 20th Tirthankar Shri Sant Suvrat Swami, 23rd Tirthankar Parsvanath Bhagwan and the last Tirthankar Mahavir Swami purified this place with their holy feet. (tourism)
;24 तिर्थानकरस के बाहर 20th तिर्थानकर श्री सन्त सुवरावत स्वामी ने, 23rd तिर्थानकर परस्वनाथ भगवान ने और आखिरी तिर्थानकर महावीर स्वामी ने उनके पवित्र पैर के_ससाथ यह स्थान पावन किया.
;Only one parent out of four puts forth efforts to develop their children's reading habit. (byu.corpus)
;cAra meM se kevala eka mAwA-piwA apane baccoM ke paDane kI AxAwa ko vikAsa karane ke lie prayawna karawe hEM.
;Seven out of 10 people who work at Google are men. (byu.corpus)
;sAwa meM se xasa loga jo gUgula meM kAma karawe hEM vo AxamI hEM.
(defrule of47
 (declare (salience 4900))
 (id-root ?id of)
 ?mng <-(meaning_to_be_decided ?id)
 (id-word ?id1 out)
 (or(viSeRya-out_of_saMbanXI  ? ?id3)(kriyA-out_of_saMbanXI  ? ?id3) (viSeRya-out_of_saMbanXI  ?id3 ?))
 (viSeRya-saMKyA_viSeRaNa  ?id3 ?)
 =>
 (retract ?mng)
 (assert (id-wsd_root_mng ?id meM_se))
 (if ?*debug_flag* then
  (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of47   "  ?id "  meM_se )" crlf))
 )



;------------------------------------ Default Rules --------------------
(defrule of35
(declare (salience 1600))
(id-root ?id of)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  of.clp       of35   "  ?id "  kA )" crlf))
)

;---------------------------- Removed rules------------------------------------------------
;removed of11 by shirisha Manju
;	if -1  ahead then 'se'   Note : added in of10
;removed of12 by shirisha Manju
;	if -1 fond then 'kA'	 Note : added in of16
;removed of15 by shirisha Manju
;	if -1 kinds|types then  'kA'   	Note : same as of16
;removed of21 by shirisha Manju	
;	if -1 aware|afraid then 'se'	Note : added in of10
;removed of24 by shirisha Manju
;	if -1 talk|hear|think|complain then  'ke_bAre_meM'   Note : added in of3
; removed of32 --- same as of29  by shirisha Manju

;LEVEL 
;Headword : of
;
;Examples --
;
;--"1.kA"
;
;Jungles of India are dense.
;jaMgala of BArawa hEM GanA
;BArawa ke jaMgala Gane hEM.                   Jungles of India
;                                           N1   of  N2 =>
;                                           N2   kA  N1 
;
;Open the first page of the book.
;Kolie the pahalA pqRTa of the kiwAba 
;kiwAba kA pahalA pqRTa Koliye.            the first page of the book
;                                      [the first page] of [the book]
;                                      [the pahalA pqRTa]  of [the kiwAba]
;				            NP1         of   NP2 =>
;                                            NP2         kA   NP1
;
;Open the lid of the box.
;Kolie the Dakkana of the dibbA
;dibbe kA Dakkana Kolo
;
;The role of a teacher is to guide a student towards knowledge.
;the BUmikA of a aXyApaka hE to mArga_xiKAnA a vixyArWI towards jFAna.
;aXyApaka kI BUmikA vixyArWI ko jFAna kA mArga xiKAnA hE.
;
;The inhabitants of the area are put under a lot of inconvenience.
;the  nivAsI of the kRewra hEM raKe under a bahuwa of asuviXA
;kRewra ke nivAsiyoM ke liye bahuwa asuviXA ho gayI hE.
;
;All the novels of Premchand are in the library.
;saba the upanyAsa of premacaMxa hEM in the puswakAlaya
;premacaMxa ke saBI upanyAsa puswakAlaya meM hEM.
;
;I bought a new cassette of songs of Lata Mangeshkar.
;mEM KarIxA a nayA kEseta of gAnA of lawA maMgeSakara.
;mEMne lawA maMgeSakara ke gAnoM kA eka nayA kEseta KarIxA.
;
;The book contains stories of crime.
;the puswaka yukwa_hE kahAniyAz of aparAXa.
;puswaka meM aparAXa kI kahAniyAz hEM.
;
;She was wearing a sari of gold zari.
;vaha{swrI.} WI pahane_hue a sAdZI of sunaharI jZarI.
;vaha sunaharI jZarI kI sAdZI pahane WI.
;
;A rupee can buy three sheets of paper.
;a rupayA KarIxa_sakawA_hE wIna panne of kAgajZa
;eka rupaye meM kAgajZa ke wIna panne KarIxe jA sakawe hEM.
;
;In these parts of the country it rains for six months of the year.
;in ye     ilAke of the xeSa vaha` barasawA_hE for CaH mahIne of the varRa.
;xeSa ke isa ilAke meM sAla ke CaH mahIne pAnI barasawA hE.
;xeSa ke isa ilAke meM sAla meM CaH mahIne pAnI barasawA hE.
;
;Ram accused Mohan of stealing his book
;rAma{0/ne} xoRa_lagAyA mohana{para} kA curAnA usakA-kiwAba (patient)
;rAma ne mohana para apanI kiwAba curAne kA xoRa lagAyA
;
;I have often dreamed of going to far off places
;mEM hE    aksara sapanA_xeKA of jAnA to xUra-xarAjZa sWAna
;mEM aksara xUra xarAjZa sWAnoM para jAne kA sapanA xeKA karawA WA.
;                                      
;--"2.se"
;Hari died of cancer  (kAraNa)
;hari marA   of kEnsara 
;hari kEnsara se marA.
;
;Ram is afraid of heights.
;rAma hE  darA   of UzcAI  (kAraNa)
;rAma UzcI jagahoM se darawA hE.
;
;--"3.meM_se"
;I know some of the people.   'Det/quantifier of det N'  
;mEM jAnawA_hUz kuCa of the loga
;mEM logoM meM se kuCa ko jAnawA hUz.  
;mEM kuCa logoM ko jAnawA hUz
;
;Two of the children came late.
;xo of the bacce Aye xera_se.
;baccoM meM se xo xera se Aye. 
;xo bacce xera se Aye.
;
;Most of the shops are closed today.
;jyAxAwara of the xukAneM hEM banxa_huyI Aja
;xukAne meM se jyAxAwara Aja banxa hEM.
;jyAxAwara xukAne Aja banxa hEM. 
;
;I bought six of the green apples.
;mEM KarIxA CaH of the harA seba.
;hare seboM meM se CaH mEMne KarIxe.
;mEMne CaH hare seba KarIxe.
;
;Which of your books is with me.
;kOna_sA[jo`] of wumhArI kawAbeM hE with mEM.
;wumhArI kiwAboM meM se kOna sI mere pAsa hE.
;wumhArI kOna se kiwAba mere pAsa hE.
;
;Neither    of these tables will do.
;nahIM_koI{xo_meM} of ina mejoM calegI.
;ina xono mejZoM meM se koI nahIM calegI.
;ye xonoM mejeM nahIM caleMgI.
;
;--"4.0"
;Buy two kilos of potatoes.
;KarIxiye xo kilo of AlU{ba.}
;* AluoM kA xo kilo KarIxiye.
;xo kilo AlU KarIxiye.
;
;He is very fond of his dog.
;vaha hE bahuwa pasanxa_karanA of  usakA kuwwA
;* use apane kuwwe se bahuwa pasanxa karanA hE 
;use apanA kuwwA bahuwa pasanxa hE.
;
;ukwa uxAharaNoM se yaha spaRta hE ki 'of' kA praXAna arWa 'kA' howA hE. 
;viSiRta saMxarBoM meM yaha 'kA' BAva hinxI ke 'se', 'meM_se' Axi ke arWa 
;meM prayoga howA hE. kinwu XyAna xene para lagawA hE ki ye xono arWa BI
;'kA' se hI judZe hEM. 'se' 'kA_kAraNa' ke liye Ora 'meM_se' 'kA_hissA'
;arWa ke liye.
;
;awaH isameM anwarnihiwa sUwra hogA -
;
;anwarnihiwa sUwra ;
;
;                kA
;                |
;            |--------|
;          (kA_kAraNa)  (kA_hissA)
;            |          |
;            se         meM_se
;
;isakA sUwra hogA
;
;sUwra : kA`
; 
