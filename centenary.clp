
;@@@ Added by 14anu-ban-03 (14-03-2015)   
;Next year is the centenary of her death.  [oald]
;अगले वर्ष उसकी मृत्यु शताब्दी है .  [manual]
(defrule centenary2
(declare (salience 10))
(id-root ?id centenary)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 death)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SawAbxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  centenary.clp  centenary2   "  ?id "  SawAbxI )" crlf))
)


;-------------------------Default rules---------------------------------------------------

;@@@ Added by 14anu-ban-03 (14-03-2015)
;As part of the centenary celebrations a chain of beacons was lit across the region. [cald]
;शतवर्षीय उत्सव के अन्तरगत आकाशदीप की लड़ी  क्षत्र में सर्वत्र प्रकाशित की गयी थी . [manual]
(defrule centenary0
(declare (salience 00))
(id-root ?id centenary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SawavarRIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  centenary.clp  centenary0   "  ?id "  SawavarRIya )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-03-2015)    ;working on parser no.- 4
;Centenary celebrations. [oald]
;शतवर्षीय उत्सव . [manual]
(defrule centenary1
(declare (salience 00))
(id-root ?id centenary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SawavarRIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  centenary.clp  centenary1   "  ?id "  SawavarRIya )" crlf))
)

