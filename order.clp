;NOTE:-The rule is wrong. Since, its completely wrong and not needed too.
;Commented by 14anu-ban-09 on 30-8-14 
;@@@ Added by 14anu03 on 25-june-14
;Everyone must follow the law and order. 
;हर किसीको नियम और कानून का अनुसरण करना चाहिए . 
;(defrule order102
;(declare (salience 5600))
;(id-root ?id order)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(id-word ?id2 law)
;(test (=(- ?id 2) ?id2))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kAnUna))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order102   "  ?id "  kAnUna )" crlf))
;)

;Changed meaning from "maMgavAyA" to "mazMgavA"
;$$$ Modified by 14anu-ban-09 on 30-8-14
;@@@ Added by 14anu03 on 25-june-14
;They ordered pizza.
;उन्होंने पिज्जा  मँगवाया .
(defrule order101
(declare (salience 5600))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 pizza|burger|tea|coffee|lunch|dinner|supper|breakfast|food)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mazMgavA)) ;modified by 14anu-ban-09
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order101   "  ?id "  mazMgavA )" crlf)
)
)

;@@@ Added by 14anu03 on 25-june-14
;He had to follow his order.
;उसको उसकी आदेश का अनुसरण करना पडेगा.
(defrule order100
(declare (salience 5600))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-word ?id1 follow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order100   "  ?id "  AxeSa )" crlf))
)

(defrule order0
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 see)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order0   "  ?id "  anukrama )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2103
;How long can the police maintain order? [Cambridge]
;पुलीस व्यवस्था कितना लम्बा बनाए रख सकती है? 
(defrule order1
(declare (salience 4900))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id1 maintain|social|established)
(or(kriyA-object  ?id1 ?id)(and(viSeRya-viSeRaNa  ?id ?)(viSeRya-viSeRaNa  ?id ?id1)(viSeRya-det_viSeRaNa  ?id ?)(viSeRya-to_saMbanXI  ? ?id))(and(viSeRya-viSeRaNa  ?id ?id1)(viSeRya-det_viSeRaNa  ?id ?)(kriyA-to_saMbanXI  ? ?id)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order1   "  ?id "  vyavasWA )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;In alphabetical order. [Cambridge]
;वर्णमालानुसार क्रम में . 
(defrule order2
(declare (salience 4800))
(id-root ?id order)
(id-word ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id krama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order2   "  ?id "  krama )" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;I would like to place an order for ten copies of this book. [OALD]
;मैं इस पुस्तक की दस प्रतियों के लिए एक ओर्डर रखना पसंद करूँगा . 
(defrule order3
(declare (salience 4700))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(and(viSeRya-det_viSeRaNa  ?id ?)(kriyA-object  ? ?id))(kriyA-to_saMbanXI  ? ?id)(and(kriyA-subject  ? ?id)(viSeRya-det_viSeRaNa  ?id ?)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?))(and(viSeRya-det_viSeRaNa  ?id ?)(viSeRya-for_saMbanXI  ?id ?)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ordara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order3   "  ?id "  ordara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 22-1-2014
;We make wedding cakes to order. [OALD]
;हम ओर्डर पर विवाह के केक बनाते हैं .
(defrule order6
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ordara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order6   "  ?id "  Ordara )" crlf)
)
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 22-1-2014
;As the demonstration began to turn violent, the police were called in to restore order.  [OALD]
;जैसे ही प्रदर्शन हिंसात्मक हुआ, व्यवस्था को वापस लाने के लिये पुलिस को बुलाया गय् .
(defrule order7
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 restore|reestablish|fix|reconstruct|reimpose|reinstall)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order7   "  ?id "  vyavasWA )" crlf)
)
)


;$$$ Modified by 14anu-ban-09 on 6-8-14
;NOTE:- Changed meaning from "parAsa" to "koti"
;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;Table 2.4 gives the range and order of the typical masses of various objects.  [physics ncert]
;सारणी 2.4 में विभिन्न द्रव्यमानों के कोटि और परास दिए गए हैं.
(defrule order8
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(conjunction-components  ? ?id1 ?id)
(id-root ?id1 range)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order8   "  ?id "  koti )" crlf)
)
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;Thus the diameter of the earth is 17 orders of magnitude larger than the hydrogen atom.  [physics ncert]
;इस प्रकार पृथ्वी का व्यास हाइड्रोजन अणु से १७ गुणा अधिक होता है .
(defrule order9
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order9   "  ?id "  guNA )" crlf)
)
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 24.06.14
;Everything was in order for the party.
;पार्टी के लिए सब कुछ क्रम में था .
(defrule order010
(declare (salience 5500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 in)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id krama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order010   "  ?id "  krama )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 24.06.14
;The General gave the order to open fire.
;जनरल ने गोली चलाने का आदेश दिया .
(defrule order011
(declare (salience 5500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order011   "  ?id "  AxeSa )" crlf))
)


;@@@ Added by 14anu-ban-09 on 30-08-2014
;Everyone must follow the law and order. [Same CLP file] 
;हर किसीको नियम और कानून का अनुसरण करना चाहिए . 
;हर किसीको कानून और नियम का अनुसरण करना चाहिए . [Own Manual] 

(defrule order10
(declare (salience 6500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id3 ?id)
(id-word =(- ?id 1) and)
(id-word =(- ?id 2) law)
(id-root ?id3 follow)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (- ?id 2) kAnUna_Ora_niyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " order.clp  order10  "  ?id "  " (- ?id 1) " " (- ?id 2) " kAnUna_Ora_niyama  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-02-2015)
;The Court of Appeal exercised its jurisdiction to order a review of the case. 	[OALD]
;कोर्ट ऑफ अपील ने मामले के पुनरावलोकन के लिए अपने न्याय अधिकार का प्रयोग करते हुए आदेश दिया . 	[manual]

(defrule order11
(declare (salience 4500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(saMjFA-to_kqxanwa  ?id1 ?id)
(to-infinitive  ?id2 ?id)
(id-root ?id1 jurisdiction)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 AxeSa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " order.clp  order11  "  ?id "  " ?id2 " AxeSa_xe  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-02-2015)
;The teacher ordered silence.	[Same clp file]
;शिक्षक ने विस्मृति का आदेश दिया.       [Self]

(defrule order12
(declare (salience 4500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 teacher)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa_xe))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order12   "  ?id "  AxeSa_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  order.clp      order12   "  ?id " kA )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (25-03-2015)
;She ordered me to leave.		[same clp file]
;उसने मुझे जाने का आदेश दिया.		[same clp file]
(defrule order13
(declare (salience 4600))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 leave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa_xe))		
(assert  (id-wsd_viBakwi   ?id1  kA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order13   "  ?id "  AxeSa_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  order.clp       order13   "  ?id1 " kA )" crlf)	
)
)


;@@@ Added by 14anu-ban-09 on (27-02-2015)
;The police have been ordered to pay substantial damages to the families of the two dead boys.  [cald]  
; पुलिस को दो मृत लड़कों के परिवारों को पर्याप्त हर्जाना देने का आदेश दिया गया  है. [self]
(defrule order18
(declare (salience 5000))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 police)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id AxeSa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi  " ?*prov_dir* "  order.clp       order18   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp    order18   "  ?id "  AxeSa_xe )" crlf)
)
)


;--------------------Default Rules -----------------------

(defrule order4
(declare (salience 4600))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AjFA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order4   "  ?id "  AjFA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (25-03-2015);Changed viBakwi 'ko' to 'kA'.
;###[Counter Example]### They waited until they found a fairly large gap between the rocks and ordered a full stop.[coca];added by 14anu-ban-09 on (25-03-2015)
;उन्होंने तब तक प्रतीक्षा की जब तक उनको चट्टानों के बीच एक पर्याप्त फ़ासला नहीं मिला और फिर ठहराव का आदेश दिया . [self];added by 14anu-ban-09 on (25-03-2015)
;$$$ Modified by 14anu18(02-07-14)
;;Meaning changed from AjFA_xe to Axesha_xe
;"order","VTI","1.AjFA_xenA"
;The teacher ordered silence.
(defrule order5
(declare (salience 4500))
(id-root ?id order)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxeSa_xe))		
(assert (kriyA_id-object_viBakwi ?id kA))	;changed viBakwi 'ko' to 'kA' by 14anu-ban-09 on (25-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  order.clp 	order5   "  ?id "  AxeSa_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  order.clp     order5   "  ?id " kA )" crlf)	;changed viBakwi 'ko' to 'kA' by 14anu-ban-09 on (25-03-2015)
)
)
;--"2.krama_meM_raKanA"
;I need time to order my thoughts.

