;$$$ Modified by 14anu-ban-04 on 03-09-2014
;### COUNTER EXAMPLE ### Drops and Bubbles One consequence of surface tension is that free liquid drops and bubbles are spherical if effects of gravity can be neglected.   [NCERT-CORPUS] 
;पृष्ठ तनाव का एक महत्त्व यह भी है कि यदि गुरुत्व बल के प्रभाव की उपेक्षा की जा सके तो द्रव की मुक्त बूँदें तथा बुलबुले गोलाकार होते हैं.    [NCERT-CORPUS]
;### COUNTER EXAMPLE ### The mass of a body is thus an important parameter that determines the effect of force on its motion.     [NCERT-CORPUS]
;इस प्रकार किसी पिण्ड की संहति एक महत्त्वपूर्ण प्राचल है जो गति पर बल के प्रभाव को निर्धारित करता है.                [NCERT-CORPUS]
;@@@ Added by Pramila(Banasthali University) on 21-01-2014
;What will be the effect of these reforms ?          ;shiksharthi
;इन सुधारों का क्या परिणाम होगा ?
(defrule effect2
(declare (salience 5000))
(id-root ?id effect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 reform)         ;added by 14anu-ban-04
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariNAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effect.clp   effect2   "  ?id "  pariNAma )" crlf))
)

;@@@ Added by Pramila(Banasthali University) on 21-01-2014
;I have also received notice to the same effect.         ;shiksharthi
;मुझे भी इसी आशय की सूचना मिली है.
(defrule effect3
(declare (salience 5000))
(id-root ?id effect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effect.clp   effect3   "  ?id "  ASaya )" crlf))
)

;@@@ Added by 14anu07 on 23/06/2014
(defrule effect4
(declare (salience 5000))
(id-root ?id effect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 morphine|medicine|injection)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effect.clp   effect4   "  ?id "  praBAva)" crlf))
)

;------------------ Default rules -----------------

(defrule effect0
(declare (salience 4000))
(id-root ?id effect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effect.clp 	effect0   "  ?id "  praBAva )" crlf))
)

(defrule effect1
(declare (salience 4900))
(id-root ?id effect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effect.clp 	effect1   "  ?id "  pUrA_kara )" crlf))
)

;"effect","VT","1.pUrA_karanA"
;The company's transition to automation was effected recently.
;
;
