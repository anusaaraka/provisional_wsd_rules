rm error
echo "(defglobal ?*debug_flag* = TRUE)" > check_error.clp
echo "(defglobal ?*prov_dir* = $HOME_anu_provisional_wsd_rules)"  >> check_error.clp
echo "(deftemplate pada_info (slot group_head_id (default 0))(slot group_cat (default 0))(multislot group_ids (default 0))(slot vibakthi (default 0))(slot gender (default 0))(slot number (default 0))(slot case (default 0))(slot person (default 0))(slot H_tam (default 0))(slot tam_source (default 0))(slot preceeding_part_of_verb (default 0)) (multislot preposition (default 0))(slot Hin_position (default 0))(slot pada_head (default 0)))" >> check_error.clp
echo "(load \"$1\")" >> check_error.clp
echo "(exit)" >>  check_error.clp
myclips -f  check_error.clp >> error
grep -E "%%%%|FALSE|Global variable|Expected|Syntax Error|Redefining defrule|WARNING" error
grep -E "kriyA-aXikaraNavAcI_avyaya|samAsa |kriyA-conjunction" $1 > err
if [ -s err ]; then 
	echo "Relations got replaced check your clp file"  
fi
python $HOME_anu_provisional_wsd_rules/check_error.py $1
python $HOME_anu_provisional_wsd_rules/get_default_rules_info.py $1
