
;$$$ Modified by 14anu-ban-09 Anshika Sharma (CS) 21-07-2014 Banasthali Vidyapeeth
;This festival running for five days is a unique programme of Indian classical music and dance. [Parallel Corpus]
;pAzca xina calane vAlA yaha uwsava BArawIya SAswrIya saMgIwa va narwya kA Ayojana howA hE.
;The greatest classical dancer of the country make enliven this craft with their art. [Parallel Corpus]
;xeSa ke sarvaSreRTa SAswrIya narwakaapanI kala se isa Silpa ko jiMxA kara xewe hEM.
;"classical","Adj","1.SAswrIya"
;Bharatnatyam is a classical dance.
(defrule classical1
(declare (salience 6000))
(id-root ?id classical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id) ; added by 14anu-ban-09 21-7-14
(id-root ?id1 music|dance|musician|dancer|singer)  ; added by 14anu-ban-09 21-7-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAswrIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  classical.clp 	classical1   "  ?id "  SAswrIya )" crlf))
)

;@@@ Added by 14anu-ban-03 (26-02-2015)
;In 1906, he proposed a classic experiment of scattering of these α-particles by atoms to investigate the atomic structure. [ncert]
;paramANu kI saMracanA kA anveRaNa karane ke lie unhoMne san 1906 meM paramANuoM xvArA ElPA-kaNoM ke prakIrNana se sambanXiwa eka klAsikI prayoga praswAviwa kiyA.[ncert]
;paramANu kI saMracanA kA anveRaNa karane ke lie unhoMne san 1906 meM paramANuoM xvArA ElPA-kaNoM ke prakIrNana se sambanXiwa eka acCA prayoga praswAviwa kiyA. [improved]
(defrule classical2
(declare (salience 5000))
(id-root ?id classical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 experiment)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  classical.clp 	classical2   "  ?id "  acCA )" crlf))
)

;------------------------ Default Rules ----------------------

;;"classical","Adj","2.prAcIna"


;$$$ Modified by 14anu-ban-03 (04-03-2015)
;In the classical picture of an atom, the electron revolves round the nucleus much like the way a planet revolves round the sun. [NCERT CORPUS]
;परमाणु के क्लासिकी चित्रण में, इलेक्ट्रॉन नाभिक के चारों ओर ठीक ऐसे ही परिक्रमा करता है जैसे कि सूर्य के चारों ओर ग्रह परिक्रमा करते हैं.		[NCERT CORPUS]
;परमाणु के प्राचीन-उत्कृष्ट चित्रण में, इलेक्ट्रॉन नाभिक के चारों ओर ठीक ऐसे ही परिक्रमा करता है जैसे कि सूर्य के चारों ओर ग्रह परिक्रमा करते हैं.  [self] ;suggested by chaitanya sir.
;Many of them criticised the classical Malthusian theory of Population.
(defrule classical0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (26-02-2015)
(id-root ?id classical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAcIna-uwkqRta))  ;meaning changed from 'prAcIna' to 'prAcIna-uwkqRta' by 14anu-ban-03 (04-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  classical.clp 	classical0   "  ?id "  prAcIna-uwkqRta )" crlf))
)

