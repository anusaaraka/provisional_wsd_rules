
;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################



;@@@ 
;Both women will be appearing before magistrates later this week.[cambridge]
;दोनों औरतें इस हफ्ते के अंत में  मजिस्ट्रेट के सामने पेश होंगी .
(defrule appear1
(declare(salience 3800))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or
(kriyA-as_saMbanXI ?id ?id1)
(kriyA-before_saMbanXI ?id ?id1)
(kriyA-on_saMbanXI ?id ?id1)
(kriyA-at_saMbanXI ?id ?id1)
(kriyA-in_saMbanXI ?id ?id1)
)
(id-root ?id1 court|charge|magistrate|witness)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear1   "  ?id "  peSa_ho )" crlf))
)

;@@@
; His new book will be appearing in the spring.[oald]
;उसकी नयी किताब स्प्रिगं में प्रकाशित होगी
(defrule appear2
(declare(salience 3600))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(kriyA-subject ?id ?id2)
(not(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear2   "  ?id "  prakASiwa_ho )" crlf))
)

;@@@
;He suddenly appeared in the doorway.[cambridge]
;वह अचानक दरवाजे के रास्ते मे दिखा.
;We'd been in the house a month when dark stains started appearing on the wall.[cambridge]
;हमे घर में एक महीने ही थे जब दीवार पर काले धब्बे दिखाई देने लगे थे .
(defrule appear3
(declare(salience 3400))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-in_saMbanXI  ?id ?id1)(kriyA-on_saMbanXI  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear3   "  ?id "  xiKa )" crlf))
)

;@@@
;If she hasn't appeared by ten o'clock I'm going without her.[cambridge]
;अगर वह दस बजे तक नहीं आयी तो मैं उसके बिना ही चला जाऊँगा.
(defrule appear4
(declare(salience 3000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear4   "  ?id "  A )" crlf))
)

;@@@
;There appears to be some mistake.[cambridge]
;कुछ गलती लग रहीं हैं
;Things aren't always what they appear to be .[cambridge]
(defrule appear5
(declare(salience 3000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or
(kriyA-kriyArWa_kriyA  ?id ?id1)
(kriyA-kqxanwa_karma  ?id ?id1)
)
(to-infinitive  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear5   "  ?id "  laga )" crlf))
)


;@@@
;It appears that she left the party alone.[cambridge]
;ऐसा लगता है कि वह अकेले ही पार्टी छोड़ कर चली गयी.
(defrule appear6
(declare(salience 3000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi  ?id1 ?id2)
(kriyA-vAkyakarma  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear6   "  ?id "  laga )" crlf))
)

;@@@
;Ms Hawley was appearing for the defence.[cambridge]
;श्रीमती हवेल्य बचाव पक्ष कीं तरफ से प्रतिनिधित्व करेंगी .
(defrule appear7
(declare(salience 3000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiniXiwva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear7   "  ?id "  prawiniXiwva_kara )" crlf))
)


;@@@
;Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10-dec-2013
;As you appeared on earth for our good, so let me always desire the good of others.[gyananidhi]
;जैसे आप दूसरों के हित के लिए पृथ्वी पर प्रकट हुयी हैं, वैसे मैं भी सदा दूसरों के हित को ध्यान में रखूं
(defrule appear8
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or
(and(kriyA-on_saMbanXI  ?id ?id1)(id-root ?id1 earth))
(and(kriyA-for_saMbanXI  ?id ?id1)(id-root ?id1 good))
)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear8   "  ?id "  prakata_ho )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 10-Jan-2013
;He applied to the University authorities next year for permission to appear again at the PRS Examination in these three Arts subjects.
;उन्होंने अगले वर्ष विश्वविद्यालय के अधिकारियों को कला संकाय के इस तीन विषयों में फिर से परीक्षा देने की अनुमति के लिए आवेदन दिया।
(defrule appear9
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or
(kriyA-at_saMbanXI  ?id ?id1)(kriyA-in_saMbanXI  ?id ?id1))
(id-root ?id1 examination)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bETa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear9   "  ?id "  bETa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 24/03/2014
;Of these the weak and strong forces appear in domains that do not concern us here.[ncert]
;इनमें दुर्बल तथा प्रबल बल ऐसे प्रभाव क्षेत्र में प्रकट होते हैं, जिनका यहां हमसे सम्बन्ध नहीं है.
(defrule appear10
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear10   "  ?id "  prakata_ho )" crlf))
)
;$$$ Modified by 14anu-ban-02(09-10-2014)
;Removed '(kriyA-vAkya_viSeRaNa ?id ?id1)' relation
;@@@ Added by 14anu-ban-07 on 14-7-14 
;It appears as if all these tourists understand the Rajasthani language.(Parallel corpus) 
;ऐसा  लगता  है  जैसे  इन  सभी  पर्यटकों  को  राजस्थानी  भाषा  समझ  आती  हो  ।
;EsA lagawA hE jEse saBI payrtakoM ko rAjaswAnI BARA samaJa AWI hE.
;The flow of Ganga from Nilkanth appear extremely charming .(Parallel corpus) 
;नीलकंठ से गंगा का प्रवाह अत्यंत मनोहारी लगता है ।
;nIlakMTo se gMgA kA PravAha awyMw manohara lagawA hE. 

(defrule appear11
(declare(salience 3500));(salience reduce by 14anu-ban-02 on 09-10-2014)
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject ?id ?id1) ; added by 14anu-ban-02(09-10-2014)
(id-root ?id1 it|flow) ; added by 14anu-ban-02(09-10-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp   appear11   "  ?id "  laga )" crlf))
)
;@@@ Added by 14anu-ban-02(06.09.14)
;You may notice that there is an interesting coincidence between the numbers appearing in Tables 2.3 and 2.5.  [ncert]
;सारणी 2.3 एवं 2.5 में दर्शायी गई सङ्ख्याओं में आश्चर्यजनक अनुरूपता है.[ncert]
(defrule appear12
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-word ?id1 tables|table)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear12   "  ?id "  xarSA )" crlf))
)

;@@@ Added by 14anu-ban-02(25-09-2014)
;Observations since early times recognized stars which appeared in the sky with positions unchanged year after year.  [ncert]
;आद्यकाल के प्रेक्षणों द्वारा पहचाने गये तारें जोकि आकाश में सालोंसाल अपरिवर्तित स्थिति में दिखाई दिये.[manual]
;;However if you see in the map Bahrain appears as a small dot .(Parallel corpus) 
;;वैसे  नक्शे  में  देखो  तो  बहरीन  एक  छोटा  सा  बिंदु  दिखाई देता है   ।[manually]
(defrule appear13
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-in_saMbanXI ?id ?id1)(kriyA-as_saMbanXI ?id ?id1)); added'(kriyA-as_saMbanXI ?id ?id1)' relation by 14anu-ban-02 on 09-10-14
(id-word ?id1 sky|dot) ; added 'dot' by 14anu-ban-02 on 09-10-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKAI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear13   "  ?id "  xiKAI_xe )" crlf))
)

;@@@ Added by 14anu-ban-02 (09-10-14)
;If the total binding energy of the reacting molecules is less than the total binding energy of the product molecules, the difference appears as heat and the reaction is exothermic.[ncert]
;यदि अभिकर्मक अणुओं की कुल बन्धन ऊर्जा उत्पादित अणुओं की कुल बन्धन ऊर्जा से कम होती है तो ऊर्जा का यह अन्तर ऊष्मा के रूप में प्रकट होता है और अभिक्रिया ऊष्माक्षेपी होती है.[ncert]
(defrule appear14
(declare(salience 4000))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-word ?id1 difference)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKAI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear14   "  ?id "  xiKAI_xe )" crlf))
)




;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;You must fill a form for appearing in exam. 
;आप परीक्षा में शामिल होने के लिए एक फार्म भरना होगा.
(defrule appear15
(declare(salience 4800))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 exam)
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear15   "  ?id "  SAmila_ho )" crlf))
)

;@@@Added by 14anu-ban-02(05-02-2015)
;If the circular orbit is in the equatorial plane of the earth, such a satellite, having the same period as the period of rotation of the earth about its own axis would appear stationery viewed from a point on earth.[ncert 11_08]
;यदि वृत्तीय कक्षा पृथ्वी के विषुवत वृत्त के तल में है, तो इस प्रकार का उपग्रह, जिसका आवर्तकाल पृथ्वी के अपने अक्ष पर घूर्णन करने के आवर्तकाल के बराबर हो, पृथ्वी के किसी बिंदु से देखने पर स्थिर प्रतीत होगा.[ncert]
(defrule appear16
(declare(salience 4800))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id2)
(id-root ?id2 view)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawIwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear16   "  ?id "  prawIwa_ho )" crlf))
)

;***************************************** DEFAULT RULE **********************************************************


;@@@
(defrule appear0
(declare(salience 0))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp 	appear0   "  ?id "  xiKa )" crlf))
)

;**************************************************************** EXAMPLES ******************************************************************

;His new book will be appearing in the spring.
;It was too late to prevent the story from appearing in the national newspapers.
;These allegations appear in a forthcoming documentary.
;He suddenly appeared in the doorway.
;We'd been in the house a month when dark stains started appearing on the wall.
;His name appears in the film credits for lighting.
;If she hasn't appeared by ten o'clock I'm going without her.
;The film, currently in the States, will be appearing on our screens later this year.
;I've noticed that smaller cars are starting to appear again.
;Both women will be appearing before magistrates later this week.
;You've got to appear calm in an interview even if you're terrified underneath.
;To people who don't know him he probably appears to be rather unfriendly.
;Things aren't always what they appear to be .
;She appears to actually like the man, which I find incredible.
;There appears to be some mistake.
;It appears that she left the party alone.
;It appears to me that we need to make some changes.
;Dave Gilmore is currently appearing as Widow Twanky in the Arts Theatre's production of "Puss in Boots".
;She appears briefly in the new Bond film.
;Ms Hawley was appearing for the defence.
;It was his first appearance on television/television appearance as president.
;She will be making a public appearance, signing copies of her latest novel.
;This was the defendant's third court appearance for the same offence.
;He made his first stage/TV appearance at the age of six.
;I didn't really want to go to the party, but I thought I'd better put in an appearance.


;@@@ Added by 14anu19 (20-06-2014)
;The civic authorities appear to have a distinct apathy towards our cemeteries and crematoria.
;नागरिक अधिकारियों में हमारे समाधि स्थलों और शवदाहगृह की ओर एक स्पष्ट उदासीनता दिखती है 
(defrule appear_tmp
(declare(salience 3550))
(id-root ?id appear)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ? ?id1)
(kriyA-object ?id1 ?id2)
(id-word ?id2 apathy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKawI))
(assert (kriyA_id-subject_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  appear.clp   appear_tmp   "  ?id "  xiKawI )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  appear.clp      appear_tmp  "  ?id " meM )" crlf)
)

