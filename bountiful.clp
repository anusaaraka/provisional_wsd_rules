;@@@Added by 14anu-ban-02(02-04-2015)
;Belief in a bountiful god.[oald]
;दयालु देवता में विश्वास रखो.[self]
(defrule bountiful1 
(declare (salience 100)) 
(id-root ?id bountiful) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 god|nature)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xayAlu)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bountiful.clp  bountiful1  "  ?id "  xayAlu )" crlf)) 
) 

;-------------default_rules---------------------------------
;@@@Added by 14anu-ban-02(02-04-2015)
;Sentence: A bountiful supply of food.[oald]
;Translation: आहार की प्रचुर सामग्री . [self]
(defrule bountiful0 
(declare (salience 0)) 
(id-root ?id bountiful) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pracura)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bountiful.clp  bountiful0  "  ?id "  pracura )" crlf)) 
) 
