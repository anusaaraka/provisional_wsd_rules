
(defrule put0
(declare (salience 5000))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put0  "  ?id "  " ?id1 "  PElA  )" crlf))
)

;They put about that she was married.
;unhoneM PElA xiyA ki usakI SAxI ho gaI
(defrule put1
(declare (salience 4900))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xiSA_baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put1  "  ?id "  " ?id1 "  xiSA_baxala  )" crlf))
)

;They put about && went for home.
;unhoneM xiSA baxalI Ora Gara cale gae
(defrule put2
(declare (salience 4800))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 aside)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put2  "  ?id "  " ?id1 "  CodZa_xe  )" crlf))
)

;She put aside her sketch for having her dinner.
;usane apanA nakSA Bojana lena hewu CodZa xiyA
(defrule put3
(declare (salience 4700))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vApisa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put3  "  ?id "  " ?id1 "  vApisa_raKa  )" crlf))
)

;Did you put my dress back in my cupboard?
;kyA wumane merI poSAka alamArI meM vApisa raKa xI?
(defrule put4
(declare (salience 4600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vApisa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put4  "  ?id "  " ?id1 "  vApisa_raKa  )" crlf))
)

;Did you put my dress back in my cupboard?
;kyA wumane merI poSAka alamArI meM vApisa raKa xI?
(defrule put5
(declare (salience 4500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 by)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bacA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put5  "  ?id "  " ?id1 "  bacA  )" crlf))
)

;She has put by some money for her daughter's marriage.
;usane apanI puwrI kI SAxI hewu kuCa rUpayA bacA kara raKA hE
(defrule put6
(declare (salience 4400))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put6  "  ?id "  " ?id1 "  xabA  )" crlf))
)

;Please put down this issue right now.
;kqpyA isa muxxe ko aBI xabA xo
(defrule put7
(declare (salience 4300))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nivexana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put7  "  ?id "  " ?id1 "  nivexana_kara  )" crlf))
)

;He's putting in for that grant.
;vaha grAMta ke lie nivexana kara rahA hE
(defrule put8
(declare (salience 4200))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put8  "  ?id "  " ?id1 "  baMxa_kara  )" crlf))
)

;Please put the fan off.
;kqpyA paMKA baMxa kara xo
(defrule put9
(declare (salience 4100))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put9  "  ?id "  " ?id1 "  baDZA  )" crlf))
)

;He put out his hand to hold her.
;usane apanA hAWa use pakadZane ke lie baDZA xiyA
(defrule put10
(declare (salience 4000))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 together)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put10  "  ?id "  " ?id1 "  jodZa  )" crlf))
)

;The kettle broke,but i managed to put it together again.
;kewalI tUta gaI WI paraMwu mEne use xobArA se jodZa xiyA
(defrule put11
(declare (salience 3900))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put11  "  ?id "  " ?id1 "  saha  )" crlf))
)

;I cannot put up with all the noise.
;mEM yaha Sora sahana nahIM kara sakawA
(defrule put12
(declare (salience 3800))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KadZA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put12  "  ?id "  " ?id1 "  KadZA_kara  )" crlf))
)

;He is putting up a new building.
;vaha eka naI imArawa KadZI kara rahA hE
(defrule put13
(declare (salience 3700))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put13  "  ?id "  " ?id1 "  baDZA  )" crlf))
)

;Don't put up the price again.
;kImawa ko xobArA mawa baDZAo
(defrule put14
(declare (salience 3600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put14  "  ?id "  " ?id1 "  xe  )" crlf))
)

;He promised to put up the money for charity.
;usane canxe ke lie pEse xene kA vAyaxA kiyA
(defrule put15
(declare (salience 3500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 question)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put15   "  ?id "  pUCa )" crlf))
)

(defrule put16
(declare (salience 3400))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 query)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put16   "  ?id "  pUCa )" crlf))
)



;Added by Meena(4.09.09)
;He took all her letters into the yard and put a match to them . 
(defrule put17
(declare (salience 3400))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 match)
(kriyA-object ?id ?id1)
(kriyA-to_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp    put17   "  ?id "  lagA_xe )" crlf))
)


;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;As Albert Einstein put it, The most incomprehensible thing about the Universe is that it is comprehensible. [Gyan nidhi]
;अल्बर्ट आइनस्टीन ने कहा था, ब्रह्मांड की सबसे अबोधगम्य चीज यह है कि यह बोधगम्य है।
(defrule put18
(declare (salience 3300))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 pronoun)
(kriyA-object  ?id ?id1)
(kriyA-subject  ?id ?)
(kriyA-vAkya_viBakwi  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahA_WA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put18   "  ?id "  kahA_WA )" crlf))
)

;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;The suspension bridges put up in these isolated places are still used, added Grandpa. [Gyannidhi]
;इन एकान्त स्थानों पर बनाए हुए झूलेदार पुल अाज भी उपयोग होते हैं, दादाजी ने कहा.
(defrule put19
(declare (salience 3200))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(viSeRya-kqxanwa_viSeRaNa  ? ?id)
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banAye_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put19  "  ?id "  " ?id1 "  banAye_hue )" crlf))
)

;Nobody could stop him from putting that question again && again.
(defrule put20
(declare (salience 3100))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put20   "  ?id "  raKa )" crlf))
)

;"put","V","1.raKanA"
;He put the magazine aside && turned to speak to me.
;--"2.mahawwA xenA"
;our company puts the emphasis on communication skills.
;--"3.vyakwa karanA"
;I put a clear statement on this issue.
;--"4.PeMkanA"
;My leg was fractured while I was putting the shot-put.
;
;


;@@@ Added by Sonam Gupta MTech IT Banasthali 3-1-2014
;The prisoners were put to death. 
;कैदियों को मृत्युदण्ड दिया गया.
(defrule put21
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 death)
(kriyA-to_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mqwyuxaNda_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put21  "  ?id "  " ?id1 "  mqwyuxaNda_xe )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 8-1-2014
;To put a scheme into action. [OALD]
;योजना को कार्यान्वित करना
(defrule put22
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put22   "  ?id "  kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 8-1-2014
;I'm tired of him putting me down all the time. [Cambridge]
;मैं हर समय उसके द्वारा मुझे नीचा दिखाने से थक चुका हूँ . 
(defrule put23
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-down_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIcA_xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put23   "  ?id "  nIcA_xiKA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 8-1-2014
;You'd better put your coat on, it's cold outside. [Cambridge]
;आप अपना कोट पहन लीजिये, बाहर ठण्ड है.
(defrule put24
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 on )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahana_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put24  "  ?id "  " ?id1 "  pahana_le )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 8-1-2014
;Can you put the light on? [Cambridge]
;क्या आप बिजली चालू कर सकते हैं?
(defrule put25
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(kriyA-object  ?id ?id2)
(id-root ?id2 light|switch|music)
(id-root ?id1 on )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cAlU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  put.clp 	put25   "  ?id "  " ?id1 " cAlU_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;He's not really upset, he's just putting it on. [Cambridge]
;वह सच में दुखी नहीं है, वह बस दिखावा कर रहा है. 
(defrule put26
(declare (salience 5500))
(id-root ?id put)
(id-word ?id putting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 on )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xiKAvA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put26  "  ?id "  " ?id1 "  xiKAvA_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;Did you put sugar in my coffee? [OALD]
;क्या आपने मेरी कोफी में चीनी डाली है? 
(defrule put27
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put27   "  ?id "  dAla )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;Put your hand up if you need more paper. [OALD]
;यदि आपको अौर कागज चाहिये तो अपना हाथ ऊपर उठाइए. 
(defrule put28
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put28  "  ?id "  " ?id1 "  uTa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;He put his fist through a glass door. [OALD]
;उसने अपनी मुट्ठी शीशे के दरवाजे पर मारी . 
(defrule put29
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put29   "  ?id "  mAra )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;It was the year the Americans put a man on the moon. [OALD]
;इस वर्ष में अमरीकियों ने मनुष्य को चाँद पर भेजा था . 
(defrule put30
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 man|staff|fellow|crew|guy)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BejA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put30   "  ?id "  BejA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (13-03-2015)
;It was his chance to put something in the history books, says Shields.	[Report set 4]	;Added by 14anu-ban-09 on (13-03-2015)
;यह उसका इतिहास पुस्तकों में कुछ लिखने का अवसर था, शील्डज कहता है  .		[Manual] 	;Added by 14anu-ban-09 on (13-03-2015)
;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;I'll put it in my diary. [OALD]
;मैं मेरी डायरी में इसको लिखूँगा . 
;I couldn't read what she had put. [OALD]
;मैं नहीं पढ सका उसने क्या लिखा था . 
(defrule put31
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 diary|paper|notebook|read|book)	;Added 'book' by 14anu-ban-09 on (13-03-2015)
(or(kriyA-in_saMbanXI  ?id ?id1)(kriyA-vAkyakarma  ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put31   "  ?id "  liKa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 9-1-2014
;Her new job has put a great strain on her. [OALD]
;उसके नये काम ने उसपर अत्यधिक तनाव डाला है . 
(defrule put32
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 strain|pressure)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put32   "  ?id "  dAla )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 11-1-2014
;Asutosh wanted that he should not be transferred from Calcutta as that would put an end to his research. [Gyannidhi]
;आसुतोष चाहता था कि उसको कलकता से स्थानान्तरित नहीं होना चाहिए क्योंकि वह उसके शोध को रोकेगा . 
(defrule put33
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 end|stop|halt)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put33  "  ?id "  " ?id1 "  roka )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;If you need somewhere to stay, we can put you up for the night. [Cambridge]
;यदि आपको रहने के लिये किसी जगह की जरूरत है, तो हम आपको रात के लिए रख सकते हैं . 
(defrule put34
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id1 pronoun)
(kriyA-upasarga  ?id ?id2)
(id-root ?id2 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put34  "  ?id "  " ?id2 "  raKa )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 15-1-2014
;He is so rude, I don't know how you put up with him. [Cambridge]
;वह इतना अभद्र है, मैं नहीं जानता हूँ कि आपने उसका साथ कैसे निभाया . 
(defrule put35
(declare (salience 5650))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id1 pronoun)
(kriyA-upasarga  ?id ?id2)
(id-root ?id2 up)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 niBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put35  "  ?id "  " ?id2 "  niBA )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 21-1-2014
;He finally put his ideas on paper. [OALD]
;अन्ततः उसने अपने विचार कागज पर लिखे . 
(defrule put36
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 paper|notebook|diary)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put36   "  ?id "  liKa )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (09-02-2015)
;Corrected spelling 'laga' to 'lagA' .
;@@@ Added by Sonam Gupta MTech IT Banasthali 21-1-2014
;They papered over their disagreements in order to convince the investors to put up the money. [MW]
;उन्होंने अपनी असहमति को छुपाया ताकि निवेशकों को पैसा लगाने के लिये मना सके .
(defrule put37
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
(kriyA-object  ?id ?id2)
(id-root ?id2 money|cash|effort)	
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lagA)) ;Corrected spelling 'laga' to 'lagA' by 14anu-ban-09 on (09-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp  put37  "  ?id "  " ?id1 "  lagA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 28-1-2014
;He put the finishing touches to his painting. [OALD]
;उसने अपनी कलाकृति को अन्तिम रूप दिया .
(defrule put38
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put38   "  ?id "  xe )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 12-3-2014
;Another way of putting it is that mass comes only in one variety there is no negative mass but charge comes in two varieties: 
;positive and negative charge. [physics ncert]
;इसे कहने का एक और मार्ग है कि द्रव्यमान केवल एक ही प्रकार (ऋणात्मक द्रव्यमान जैसा कुछ नहीं है ) का होता है, जबकि आवेश दो प्रकार के होते हैं: धनावेश तथा ऋणावेश.
(defrule put39
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 way)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put39   "  ?id "  kaha )" crlf))
)

;@@@ Added by 14anu01
;You can put the suitcases down in the bedroom.
;आप शयन कक्ष में सूटकेस नीचे रख सकते हैं 
(defrule put40
(declare (salience 6000))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 down)
(kriyA-upasarga  ?id ?id1)
(id-root ?id2 table|floor|bedroom|chair|desk|room)
(or(kriyA-in_saMbanXI  ?id ?id2)(kriyA-on_saMbanXI  ?id ?id2))
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  nIce_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put40   "  ?id "   nIce_raKa )" crlf))
)


;@@@ Added by 14anu17
;When Kamsa , Krishna ' s maternal uncle , heard that his sister Devaki ' s child will kill him , he put Devaki and her husband Vasudeva behind bars and killed each child that was born to the couple .
(defrule put41
(declare (salience 3200))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-behind_saMbanXI  ?id ?id1)
(id-root ?id1 bar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put41   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu01
;My husband and I are going to put down some money to buy that house in the centre of town.
;मेरे पति और मैं नगर के केंद्र में वह घर खरीदने के लिए थोडा पैसा रखने जा रहे हैं
(defrule put0041
(declare (salience 5500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 down)
(kriyA-upasarga  ?id ?id1)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put0041   "  ?id "   raKa )" crlf))
)


;@@@ Added by 14anu-ban-09 on (14-11-2014)
;In 1678, the Dutch physicist Christiaan Huygens put forward the wave theory of light — it is this wave model of light that we will discuss in this chapter. [NCERT CORPUS]
;san_ 1678 meM daca BOwikavixa kristiAna hAigeMsa ne prakASa ke warafga sixXAnwa ko praswuwa kiyA-isa aXyAya meM hama prakASa ke isI warafga sixXAnwa para vicAra kareMge. [NCERT CORPUS]

(defrule put041
(declare (salience 4000))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 forward)
(kriyA-upasarga ?id ?id1)
(kriyA-object  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put041  "  ?id "  " ?id1 "  praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;He sealed down the envelope and put a stamp on it.[cald]	;added by 14anu-ban-09 on (05-03-2015)
;उसने एनवलप बन्द किया और उसपर एक टिकट लगाया . [self]		;added by 14anu-ban-09 on (05-03-2015)
;We had to put new locks on all the doors.		[oald]	;added by 14anu-ban-09 on (09-02-2015)
;हमें सारे दरवाजो पर नए ताले लगाने पड़ेगे.				[Self]  ;added by 14anu-ban-09 on (09-02-2015)
;Definitely put labels of name - address on your every suitcase , attaché , bag etc . [Tourism Corpus]
;अपने हर सूटकेस , अटैची , बैग आदि पर नाम-पते का लेबल अवश्य लगाएँ . [Tourism Corpus]

(defrule put42
(declare (salience 3500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 label|lock|stamp)	;added 'lock' by 14anu-ban-09 on (09-02-2015)	;added 'stamp' by 14anu-ban-09 on (05-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put42   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;Damascus puts the tourists coming in into a confusing state for once if they are really moving around in a desert area . [Tourism Corpus]
;दमिश़्क आने वाले पर्यटकों को एक बार तो संशय की स्थिति में डाल ही देता है कि क्या वे वास्तव में किसी रेगिस्तानी इलाके में घूम रहे हैं ? [Tourism Corpus]
;दमिश़्क आने वाले पर्यटकों को एक बार तो संशय की स्थिति में डाल देता है यदि वे वास्तव में किसी रेगिस्तानी इलाके में घूम रहे हैं ? [Self]

(defrule put43
(declare (salience 3500))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id2)
(kriyA-into_saMbanXI  ?id2 ?id3)
(id-root ?id1 tourist)
(id-root ?id3 state)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put43   "  ?id "  dAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-01-2015)
;Who's been putting such weird ideas into your head. [OALD]
;आपके दिमाग में ऐसे विचित्र विचार कौन डाल रहा है . [Self]

(defrule put44
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 head)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put44   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-02-2015)
;If you put wee bit of water it gradually blossoms.		(coca)
;यदि आपने थोडा सा पानी  डाला तो यह धीरे धीरे खिलता है . 			(manual)

(defrule put45
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put45   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-02-2015)
;I put a clear statement on this issue.		[Hinkhoj.com]
;मैने इस विषय पर स्पष्ट कथन व्यक्त कर दिया.		[self]

(defrule put46
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 statement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put46   "  ?id "  vyakwa_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-02-2015)
;NOTE- There is a root problem in below sentence. When the root problem solved then uncomment (id-root ?id1 shot-put) and comment (id-word ?id1 shot-put) 
;My leg was fractured while I was putting the shot-put.		[Same clp file]
;मेरी टाँग टूट गई जब मैं गोला फेक रहा था. 					

(defrule put47
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 shot-put)
;(id-root ?id1 shot-put)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Peka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put47   "  ?id "  Peka )" crlf))
)


;@@@ Added by 14anu-ban-09 on (09-02-2015)
;Put your name here.				[oald.com]
;यहाँ आपका नाम लिखिए.				[self]

(defrule put047
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 name|diary|notebook)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put047   "  ?id "  liKa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-02-2015)
;The incident put her in a bad mood. 		[oald.com]
;घटना ने उसको खराब मनोदशा में डाल दिया.   		[manual]

(defrule put48
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 mood)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put48   "  ?id "  dAla_xe )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (09-02-2015)
;Put yourself in my position. What would you have done?		[oald.com]
;खुदको मेरी स्थिति में रख कर देखो. तुमने क्या किया हैं/होता ?	   		[manual]

(defrule put49
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 position)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa_kara_xeKa))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put49   "  ?id "  raKa_kara_xeKa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  put.clp   put49   "  ?id " ko )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (09-02-2015)
;I was put in charge of the office.		[oald.com] 
;मुझे दफ्तर की जिम्मेदारी दी गई थी.			[manual]

(defrule put50
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 charge)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xI_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put50   "  ?id "  xI_jA )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (09-02-2015)
;Our company puts the emphasis on quality.	[oald.com]
;हमारी कम्पनी गुणवत्ता पर जोर डालती हैं.			[manual]

(defrule put51
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 emphasis)
(viSeRya-on_saMbanXI  ?id1 ?id2)
(id-root ?id2 quality|cost)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put51   "  ?id "  dAla )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;We’re not allowed to put posters on the walls.		[oald]	;added by 14anu-ban-09 on (31-03-2015)
;हमे दीवारों पर विज्ञापन लगाने की इजाजत नहीं है . 			[Manual]	;added by 14anu-ban-09 on (31-03-2015)
;Nurse was told to put oxyzen pipe on patient's nose. 			[oald.com]
;परिचारिका को मरीज की नाक में oxyzen पाइप लगाने को कहा गया था . 			[manual]

(defrule put52
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 nose|wall)	;added 'wall' by 14anu-ban-09 on (31-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put52   "  ?id "  lagA )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;I put my objections bluntly.		[freedictionary.com]
;मैंने मेरी आपत्तियाँ स्पष्टतया से प्रकट कीं . 		[Manual]

(defrule put53
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 objection)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put53   "  ?id "  prakata_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;Her family put her into a nursing home. 	[oald.com]
;उसका परिवार उसको एक उपचार गृह में भेजा .  		[Manual]

(defrule put54
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 home|hospital|park)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put54   "  ?id "  Beja )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;It's time you put a stop to this childish behaviour.   	[oald.com]
;समय है कि आप इस लडकपन के व्यवहार को बन्द कर दे . 		[Manual]

(defrule put55
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 stop)
(kriyA-to_saMbanXI  ?id ?id2)
(id-root ?id2 behaviour)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put55  "  ?id "  " ?id1 "  baMxa_kara_xe  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;He put a limit on the amount we could spend. 	[oald.com]
;उसने रकम पर सीमा लगाई जिसे हम खर्च कर सके .  		[Manual]

(defrule put56
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 limit)
(kriyA-on_saMbanXI  ?id ?id2)
(id-root ?id2 price|amount)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put56   "  ?id "  lagA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;I'd put her in the top rank of modern novelists. 	[oald.com]
;मैंने उसको आधुनिक उपन्यासकार की ऊपरी श्रेणी में रखा .   		[Manual]

(defrule put57
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 rank)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 novelist)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put57   "  ?id "  raKa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;She put it very tactfully.	[oald]
;उसने यह अत्यन्त व्यवहार-कुशलता के साथ व्यक्त किया .  [Anusaaraka]

(defrule put58
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id2 tactfully)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put58   "  ?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-03-2015)
;She had never tried to put this feeling into words.	[oald.com]
;उसने कभी भावनाओ को शब्दों में व्यक्त करने का प्रयास नहीं किया था .  		[Manual]

(defrule put59
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 word)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put59   "  ?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-03-2015)
;Don’t go putting yourself at risk.	[oald.com]
;तुम खुद को खतरे में मत डालो  .   		[Manual]

(defrule put60
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 risk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put60   "  ?id "  dAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-03-2015)
;I tried to put the matter into perspective. [oald]
;मैंने मामले को अपने दृष्टिकोण में व्यक्त करने का प्रयास किया .                     [Manual]

(defrule put61
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 matter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put61   "  ?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-03-2015)
;It was time to put their suggestion into practice. [oald]
;यह उनके सुझाव को व्यवहार में लाने का समय था  . 		         [Manual]

(defrule put62
(declare (salience 5501))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 practice)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put62   "  ?id "  lA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (01-04-2015)
;I asked him in the nicest possible way to put his cigarette out.  [oald]
;मैंने उसको सिगरेट बुझाने को अच्छे तरीके में कहा . 
(defrule put63
(declare (salience 5000))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 out)
(kriyA-object ?id ?id2)
(id-root ?id2 cigarette)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 buJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " put.clp	put63  "  ?id "  " ?id1 "  buJA  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;Every year the school puts on a musical production. 	[oald]
;प्रत्येक वर्ष विद्यालय में एक संगीत प्रस्तुति रखी जाती है .	[Manual] 

(defrule put64
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 production)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKI_jA))
(assert (kriyA_id-subject_viBakwi ?id meM)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put64   "  ?id "  raKI_jA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  put.clp put64      "  ?id " meM )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;She put the baby to her breast.	[oald]
;उसने शिशु को अपने स्तन से लगाया .	[self]

(defrule put65
(declare (salience 5600))
(id-root ?id put)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 breast|chest)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(assert (kriyA_id-object_viBakwi ?id ko)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  put.clp 	put65   "  ?id "  lagA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  put.clp put65      "  ?id " ko )" crlf))
)





