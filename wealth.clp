
;@@@ Added by Anita-12.12.2013
;She has risen from humble origins to immense wealth.
;वह नम्र मूल से बहुत अधिक संपन्नता को पहुँच चुकी है ।
(defrule wealth1
(declare (salience 5000))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 immense)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMpannawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth1   "  ?id " saMpannawA )" crlf))
)   


;@@@ Added by Pramila(BU) on 04-03-2014
;He gave a wealth of examples.   ;shiksharthi
;उसने प्रचुर उदाहरण दिए.
(defrule wealth2
(declare (salience 5000))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pracura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wealth.clp	wealth2 "  ?id "  " (+ ?id 1) "  pracura  )" crlf))
)

;@@@ Added by 14anu-ban-11 on 3.9.2014.
;Kohima , the capital of Nagaland , is rich with forest wealth and natural beauty.(Tourism corpus)
;नागालैंड  राज्य  की  राजधानी  कोहिमा  जंगली  संपदा  और  नैसर्गिक  सौंदर्य  से  संपन्न  है 
(defrule wealth3
(declare (salience 4300))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  sampawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth3   "  ?id "  sampawwi )" crlf))
)

;@@@Added by Manasa ( 29-02-2016 )
;It is not that countries which are endowed with a bounty of natural wealth — minerals or forests or the most fertile lands — are naturally the richest countries.
;यह नहीं है जो प्राकृतिक सम्पत्ति — खनिज या जङ्गल या सबसे अधिक उर्वर भूमि — की एक उदारता प्रदान किए जाते हैं कि जो देश स्वभाविक रूप से सबसे अमीर देश हैं . 
(defrule wealth5
(declare (salience 4300))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  sampawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth5   "  ?id "  sampawwi )" crlf))
) 

;@@@added by Manasa ( 29-02-2016)
;In fact the resource rich Africa and Latin America have some of the poorest countries in the world, whereas many prosperous countries have scarcely any natural wealth 
;वास्तव में संसाधन धनवान व्यक्ति अफ्रीका और लैटिन अमेरिका सबसे अधिक दीन देशों में से विश्व में कुछ, जबकि बहुत समृद्ध देशों में मुशकिल से कोई प्राकृतिक सम्पत्ति है . 
(defrule wealth6
(declare (salience 5000))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth6   "  ?id " sampawwi )" crlf))
)
  

;####################################default-rule###############################

;@@@ Added by Anita-14.12.2013
;During a successful business career, she accumulated a great amount of wealth.
;सफल व्यापारिक पेशे में उसने बहुत सारा धन इकट्ठा किया ।
(defrule wealth0
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth0   "  ?id "  Xana )" crlf))
)   

;@@@ Added by 14anu-ban-11 on 3.9.2014.
;Kohima , the capital of Nagaland , is rich with forest wealth and natural beauty.(Tourism corpus)
;नागालैंड  राज्य  की  राजधानी  कोहिमा  जंगली  संपदा  और  नैसर्गिक  सौंदर्य  से  संपन्न  है 
(defrule wealth4
(declare (salience 4300))
(id-root ?id wealth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  sampawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wealth.clp    wealth4   "  ?id "  sampawwi )" crlf))
)
