;@@@ Added by 14anu-ban-11 on (17-04-2015)
;A watery smile .(oald)
;फीकी मुस्कराहट . (self)
(defrule watery1
(declare (salience 10))
(id-root ?id watery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 smile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watery.clp 	watery1   "  ?id "  PIkA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-04-2015)
;The vegetables were watery and tasteless. (oald)
;सब्जियाँ रसीली और बेस्वाद थीं . (self)
(defrule watery2
(declare (salience 20))
(id-root ?id watery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 vegetable)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasIlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watery.clp 	watery2   "  ?id "  rasIlI)" crlf))
)

;--------------------------------------- Default Rules --------------------------------

;@@@ Added by 14anu-ban-11 on (17-04-2015)
;His eyes were red and watery.(oald)
;उसकी आँखें लाल और अश्रुपूर्ण थीं . (self)
(defrule watery0
(declare (salience 00))
(id-root ?id watery)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSrupUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watery.clp   watery0   "  ?id "  aSrupUrNa)" crlf))
)

