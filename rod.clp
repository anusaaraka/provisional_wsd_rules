;a long, thin pole made of wood or metal: 
;----------------------------------
;He was given a fishing rod for his birthday. [cambridge dictionary]
;उन्हें उनके जन्मदिन के लिए एक मछली पकड़ने वाली छड़ी दी गयी थी ।

;The concrete is strengthened with steel rods. [cambridge dictionary]
;लोहे की छड़ के साथ कंकड़ी से मजबूत बनाया गया है ।

;Fishing with rod and line. [oxford learner's dictionary] 
;डंडे और लाइन से मछ्ली पकड़ना ।
;The teacher has a rod in his hand. [Advanced Learner's English-Hindi Dictionary]
;शिक्षक अपने हाथ में एक बेंत लिये है.
;There used to be a saying 'Spare the rod and spoil the child.'[oxford learner's dictionary] 
;एक कहावत है 'छड़ी छोड़ देना और बच्चे को खराब करना है ।
;The rod is not allowed in this school . [Advanced Learner's English-Hindi Dictionary]
;दण्ड की इस स्कूल में अनुमति नहीं है।



;@@@ Added by Anita - 9.5.2014
;He was given a fishing rod for his birthday. [cambridge dictionary]
;उन्हें उनके जन्मदिन के लिए एक मछली पकड़ने वाली छड़ी दी गयी थी ।
(defrule rod0
(declare (salience 1000))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 birthday)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod0  "  ?id " CadZI )" crlf))
)

;@@@ Added by Anita - 9.5.2014;
;The teacher has a wooden rod in his hand . [Advanced Learner's English-Hindi Dictionary-Dr. Hardev Bahri]
;शिक्षक के हाथ में एक लकड़ी का डंडा था ।
(defrule rod1
(declare (salience 5000))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 wooden) ;added by Anita 27.6.2014 
(viSeRya-viSeRaNa  ?id ?id1) ;added by Anita 27.6.2014
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id daMdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod1  "  ?id " daMdA )" crlf))
)
;@@@ Added by Anita - 9.5.2014
;The teacher has a rod in his hand. [Advanced Learner's English-Hindi Dictionary-Dr. Hardev Bahri]
;शिक्षक अपने हाथ में एक बेंत लिये है.
(defrule rod2
(declare (salience 3000))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 hand)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id beMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod2  "  ?id " beMwa )" crlf))
)

;@@@ Added by Anita - 9.5.2014
;The rod is not allowed in this school . [Advanced Learner's English-Hindi Dictionary-Dr. Hardev Bahri]
;दण्ड की इस स्कूल में अनुमति नहीं है।
(defrule rod3
(declare (salience 4000))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pitAI))
(assert (id-wsd_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod3  "  ?id " pitAI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  rod.clp      rod3   "  ?id " kI )" crlf))
)
;@@@ Added by Anita - 24.5.2014 [By mail]
;He was fishing with rod and line. 
;मच्छली की बंसी और तार से मच्छली पकड़ना ।
(defrule rod4
(declare (salience 2500))
(id-root ?id rod)
(id-word ?id1 fishing)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod4  "  ?id " baMsI )" crlf))
)
;@@@ Added by Anita - 30.7.2014
;Whatever is he doing with that rod ? [cambridge dictionary]
;वह डंडे के साथ क्या कर रहा है ? 
(defrule rod5
(declare (salience 4500))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 do)
(kriyA-with_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id daMdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod5  "  ?id " daMdA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-8-14)
;Taking the origin to be at the geometric center of the rod and x-axis to be along the length of the rod, we can say that on account of reflection symmetry, for every element dm of the rod at x, there is an element of the same mass dm located at — x (Fig. 7.8).  
;Cada kI lambAI @x - akRa ke anuxiSa raKeM Ora mUla binxu isake jyAmiwIya kenxra para le leM wo parAvarwana samamiwi kI xqRti se hama kaha sakawe hEM ki prawyeka @x para sWiwa prawyeka @dm Gataka ke samAna @dm kA Gataka – @x para BI sWiwa hogA (ciwra 7.8).
(defrule rod6
(declare (salience 5000))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod6  "  ?id " Cada )" crlf))
)

;@@@ Added by 14anu-ban-10 on (8-9-14)
;A single wire of this radius would practically be a rigid rod.  
;isa wrijyA kA ekala wAra vyAvahArika rUpa se eka xqDZa CadZa ho jAegA.
(defrule rod7
(declare (salience 5100))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id ? )(viSeRya-det_viSeRaNa ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod7  "  ?id " CadZI )" crlf))
)

;#################################default-rule############################
;@@@ Added by Anita - 9.5.2014
;The concrete is strengthened with steel rods. [cambridge dictionary]
;लोहे की छड़ के साथ कंकड़ी से मजबूत बनाया गया है ।
(defrule rod_default-rule
(declare (salience 0))
(id-root ?id rod)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rod.clp     rod_default-rule  "  ?id " CadZa )" crlf))
)
