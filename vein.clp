
(defrule vein0
(declare (salience 5000))
(id-root ?id vein)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id veined )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nAdImaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  vein.clp  	vein0   "  ?id "  nAdImaya )" crlf))
)

;"veined","Adj","1.nAdImaya"
;The doctor took blood from the veined hand
;
(defrule vein1
(declare (salience 4900))
(id-root ?id vein)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SirA)); Replaced 'SIrA' with 'SirA' by Shirisha Manju Suggested by Chaitanya Sir (14-10-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vein.clp 	vein1   "  ?id "  SirA )" crlf))
)

;default_sense && category=noun	SirA	0
;"vein","N","1.SirA"
;She had an injury in the vein
;The picture on the veins of the pipal leaf is wonderful 
;--"2.vyApwa_guNa"
;A vein of melancholy was noted in her song
;--"3.manoxaSA"
;The king && the vidushak had discussion in a lighter vein
;
;

;@@@ Added by 14anu-ban-07,(27-03-2015)
;Other speakers tackled the same problem in a lighter vein. (oadl)
;दूसरे वक्ताओं ने एक अधिक हलकी मनोदशा में वही समस्या का सामना किया . (manual)
(defrule vein2
(declare (salience 5000))
(id-root ?id vein)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 angry|light)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoxaSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vein.clp 	vein2   "  ?id "  manoxaSA )" crlf))
)

;@@@ Added by 14anu-ban-07,(27-03-2015)
;A rich vein of iron ore was found in the hillside. (cambridge)
;लोहे की खनिज मिट्टी की एक धनी परत पहाडी की ढाल में पाई गयी थी .  (manual)
(defrule vein3
(declare (salience 5100))
(id-root ?id vein)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 ore|mineral)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vein.clp 	vein3   "  ?id "  parawa )" crlf))
)
