
;$$$ Modified by 14anu-ban-01 on 28-07-14
(defrule objective0
(declare (salience 4800));salience reduced to 4800 by 14anu-ban-01
(id-root ?id objective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya));meaning changed from 'lakRya' to 'uxxeSya'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  objective.clp 	objective0   "  ?id "  uxxeSya )" crlf))
)



(defrule objective1
(declare (salience 4800));salience reduced from 4900 to 4800 by 14anu-ban-01
(id-root ?id objective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRayaniRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  objective.clp 	objective1   "  ?id "  viRayaniRTa )" crlf))
)

;@@@ Added by 14anu-ban-01 on 28-07-14.
;Such an arrangement is called an oil immersion objective. 
;इस व्यवस्था को एक तैल निमज्जन अभिदृश्यक कहते हैं.
;But here, the objective has a large focal length and a much larger aperture than the eyepiece.
;परंतु यहाँ पर, नेत्रिका की अपेक्षा अभिदृश्यक की फ़ोकस दूरी अधिक तथा इसका द्वारक भी काफ़ी अधिक होता है. 
;Light from a distant object enters the objective and a real image is formed in the tube at its second focal point. 
;अभिदृश्यक में प्रवेश करता है तथा ट्यूब के अंदर इसके द्वितीय फ़ोकस पर वास्तविक प्रतिबिंब बनता है.
;For these reasons, modern telescopes use a concave mirror rather than a lens for the objective.
;यही कारण है कि आधुनिक दूरदर्शकों में अभिदृश्यक के रूप में लेंस के स्थान पर अवतल दर्पण का उपयोग किया जाता है.
(defrule objective2
(declare (salience 4900))
(id-root ?id objective)
?mng <-(meaning_to_be_decided ?id)
(id-word ?wrd focal|focus|eyepiece|concave|convex|immersion)
(test (or(< ?id ?wrd)(> ?id ?wrd)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBixqSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  objective.clp 	objective2   "  ?id "  aBixqSyaka )" crlf))
)

;@@@ Added by Ayushi agrawal 14anu-ban-01 on 28-07-14.
;We can apply a similar argument to the objective lens of a microscope. [NCERT-physics corpus]
;इस पट्टी के बाईं ओर की सभी हम एक सूक्ष्मदर्शी के अभिदृश्यक लेंस (objective lens) के लिए समान तर्क का उपयोग कर सकते हैं.[NCERT-physics corpus]
;One obvious problem with a reflecting telescope is that the objective mirror focusses light inside the telescope tube.[NCERT-physics corpus]
;परावर्ती दूरबीन की एक सुस्पष्ट समस्या यह होती है कि अभिदृश्यक दर्पण दूरदर्शक की नली के भीतर प्रकाश को फ़ोकसित करता है.[NCERT-physics corpus]
;Usually an oil having a refractive index close to that of the objective glass is used. [NCERT-physics corpus] 	
;प्रायः एक तेल जिसका अपवर्तनाङ्क लेंस के काँच के अपवर्तनाङ्क के समीप है, का उपयोग किया जाता है.[NCERT-physics corpus]
(defrule objective3
(declare (salience 4900))
(id-root ?id objective)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) lens|glass|mirror)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBixqSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  objective.clp 	objective3   "  ?id "  aBixqSyaka )" crlf))
)

;@@@ Added by 14anu-ban-01 on 28-07-14.
;The former clearly depends on the area of the objective. [NCERT-physics corpus] 
;प्रकाश संग्रहण क्षमता स्पष्ट रूप से दूरदर्शक के अभिदृश्यक के क्षेत्रफल पर निर्भर करती है.[NCERT-physics corpus]
;The resolving power, or the ability to observe two objects distinctly, which are in very nearly the same direction, also depends on the diameter of the objective. [NCERT-physics corpus]
;विभेदन क्षमता अथवा एक ही दिशा में दो अत्यधिक निकट की वस्तुओं को सुस्पष्टतः भिन्न प्रेक्षित करने की योग्यता भी अभिदृश्यक के व्यास पर निर्भर करती है.[NCERT-physics corpus]

(defrule objective4
(declare (salience 4900))
(id-root ?id objective)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id1 ?id))
(id-root ?id1 area|diameter|length) 
(id-cat_coarse ?id adjective|noun);adjective is taken for the cases like area of thr objective lens'.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBixqSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  objective.clp 	objective4   "  ?id "  aBixqSyaka )" crlf))
)

;"objective","Adj","1.viRayaniRTa"
;Locke believed in the objective perception of reality.
;--"2.niRpakRa"
;The objective report was not appreciated.
;
;
