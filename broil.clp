;@@@Added by 14anu-ban-02(07-04-2015)
;Broil the fish for 8 minutes.[cald]
;8 मिनटों के लिए मछलियों को सेंकिए . [self]
(defrule broil2
(declare (salience 100))
(id-root ?id broil)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(viSeRya-for_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id seMka))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  broil.clp 	broil2   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  broil.clp 	broil2   "  ?id "  seMka )" crlf))
)


;------------------------------------ Default Rules -------------------------------
(defrule broil0
(declare (salience 0))	;salience reduced to 0 from 5000 by 14anu-ban-02(07-04-2015)
(id-root ?id broil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Soragula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  broil.clp 	broil0   "  ?id "  Soragula )" crlf))
)

;"broil","N","1.Soragula"
(defrule broil1
(declare (salience 0))	;salience reduced to 0 from 4900 by 14anu-ban-02(07-04-2015)
(id-root ?id broil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  broil.clp 	broil1   "  ?id "  garma_kara )" crlf))
)

;"broil","V","1.garma_karanA"
;The travellers sat broiling in the sun, for their next bus.
;--"2.Aga_yA_SIKa_para_pakAnA"
;They broiled some chicken for dinner.
;--"3.seMkanA"
;Broil fish
;The sun broils the valley in the summer
;
