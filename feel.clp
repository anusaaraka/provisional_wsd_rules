
;FILE MODIFIED BY MEENA (26.8.09)




(defrule feel0
(declare (salience 5000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id feeling )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id anuBUwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  feel.clp  	feel0   "  ?id "  anuBUwi )" crlf))
)

;given_word=feeling && word_category=noun	$BAvanA)

;"feeling","N","1.BAvanA"
;The speaker appealed to the feelings of the audiance
;--"2.boXa"
;I have a bad feeling in this matter.
;--"3.cewanA"
;I have lost all feelings in my leg.
;
;Added by Meena(26.8.09)
;Biking feels good . 
(defrule feel2
(declare (salience 4900))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id1 verbal_noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahasUsa_ho))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp    feel2   "  ?id "  mahasUsa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  feel.clp    feel2   "  ?id " se )" crlf)
)
)


;$$$ Modified by 14anu-ban-09 on 01-08-2014
;I felt so sad. [OALD]
;muJe bahuwa nurA mahasUsa huA. [Own Manual]
;The sun felt so good on my face. [OALD]
;mere cahare para padane vAlI sUraja kI kiraNoM se moJe acCA mahasUsa huA. [Own Manual]

(defrule feel3
(declare (salience 4800))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1)) ; added (kriyA-object ?id ?id1) by 14anu-ban-09 on 01-08-2014
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))) ;added by 14anu-ban-09 on 01-08-2014
(id-cat_coarse ?id verb) ; uncommented by 14anu-ban-09 on 01-08-2014
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahasUsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp 	feel3   "  ?id "  mahasUsa_kara )" crlf))
)

;@@@Added by 14anu20 on 26.06.2014
;I am not feeling myself.
;मैं खुद को स्वस्थ महसूस नहीं कर रहा हूँ .
(defrule feel23_5
(declare (salience 5800))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id =(+ ?id 1))
(id-cat_coarse =(+ ?id 1) pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svasWa_mahasUsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp 	feel23_5   "  ?id "  svasWa_mahasUsa_kara )" crlf))
)

;@@@Added by 14anu20 on 26/06/2014.
;He felt his mother's death.
;वह उसकी माँ की मृत्यु से काफी प्रभावित हुआ . 
(defrule feel23_9
(declare (salience 5800))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 heat|cold|death|force|lectures)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_kAPI_praBAviwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp 	feel23_9   "  ?id "  se_kAPI_praBAviwa_ho )" crlf))
)



;@@@Added by 14anu20 on 26/06/2014
;I feel like celebrating.
;मैं उत्सव मनाना चाहता हूँ . 
(defrule feel23_2
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) like)
(id-cat_coarse =(+ ?id 2) verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) cAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_2  "  ?id "  " (+ ?id 1) "  cAha  )" crlf))
)

;@@@Added by 14anu20 on 26/06/2014
;I feel like a drink.
;मैं पेय या शरबत पीना चाहता हूँ .
(defrule feel23_3
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) like)
(kriyA-like_saMbanXI  ?id ?id1)
(id-root ?id1 drink|tea|coffee|milk|water|beer|juice|buttermilk)

=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) pInA_cAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_3  "  ?id "  " (+ ?id 1) "  pInA_cAha  )" crlf))
)

;@@@Added by 14anu20 on 26/06/2014
;I feel like breakfast.
;मैं जलपान करना चाहता हूँ . 
(defrule feel23_4
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) like)
(kriyA-like_saMbanXI  ?id ?id1)
(id-root ?id1 breakfast|lunch|dinner)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) karanA_cAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_4  "  ?id "  " (+ ?id 1) "  karanA_cAha  )" crlf))
)

;$$$ Modified by Shirisha manju (20-05-2016)
;Changed meaning from "mahasUsa_kara" to "mahasUsa_karA"
;The heat made him feel faint.
;@@@Added by 14anu20 on 26/06/2014
(defrule feel23_6
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-preraka_kriyA  ?id ?id1)
(id-cat_coarse ?id1 verb)
(id-cat_coarse ?id verb)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) ;added by Shirisha manju (20-05-2016)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mahasUsa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_6  "  ?id "  " ?id1 "  mahasUsa_karA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  feel.clp    feel23_6   "  ?id " ko )" crlf)
)
)

;@@@Added by 14anu20 on 26.06.2014.
;I feel flattered by her.
;मैं उसके द्वारा खुअ किया गया हूँ . 
(defrule feel23_7
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) flattered)
(subject-subject_samAnAXikaraNa  ?id1 =(+ ?id 1))
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) KuSa_kiyA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_7  "  ?id "  " (+ ?id 1) "  kuSa_kiyA_gayA  )" crlf))
)

;@@@Added by 14anu20 on 26.06.2014.
;I feel my way in the new job.
;मैं नये काम में सावधानी से काम करता हूँ . 
(defrule feel23_8
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 2) way)
(kriyA-object  ?id =(+ ?id 2))
(id-cat_coarse =(+ ?id 1) pronoun)
(viSeRya-RaRTI_viSeRaNa  =(+ ?id 2) =(+ ?id 1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) sAvaXAnI_se_kAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel23_8  "  ?id "  " (+ ?id 1) "  " (+ ?id 2) "  sAvaXAnI_se_kAma_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-10-2014)
;If the green coconut on tree grows more in size it will taste like coca-cola, it also felt so on drinking. [Tourism-corpuss]
;पेड़ पर लगा हरा नारियल बड़े आकार का हो जाए तो स्वाद कोका-कोला जैसा हो जाता है , पीकर ऐसा लगा भी ।

(defrule feel04
(declare (salience 6000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ?id2)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 so)
(id-root ?id2 drink)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 EsA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feel.clp  feel04  "  ?id "  " ?id1 "  EsA_lagA  )" crlf))
)


;"feel","V","1.mahasUsa_karanA"
;Can you feel the tension in this room ?
;You will feel better after taking this medicine.
;We all felt that the boss is a good person.
;The boss feels that he is suitable for this work.
;--"2.sparSa_se_anuBava_karanA"
;Doctors feel the pulse of the patient to sense his fever.
;3.tatolanA"
;He can feel his way in the dark.

;@@@ Added by 14anu09[17-6-14]
(defrule feel4
(declare (salience 5000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id feeling|feelings )
(kriyA-object ?id1 ?id)
;(id-root ?id1 express) remove comments if overgeneralized 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  feel.clp  	feel4   "  ?id "  BAvanA )" crlf))
)

;@@@Added by 14anu20 on 25.06.2014.
;The water feels warm.
;पानी गर्म लगता है . 
(defrule feel23_1
(declare (salience 5500))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(kriyA-subject  ?id ?id1)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " feel.clp   feel23_1   "   ?id " laga )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-02-2015)
;Areas feels like a smudge.[COCA]
;क्षेत्र गन्दा जैसा महसूस हो रहा है . [SELF]

(defrule feel5
(declare (salience 5000))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-like_saMbanXI  ?id  ? )
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mahasUsa_ho_rahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  feel.clp  	feel5   "  ?id "  mahasUsa_ho_rahA)" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-02-2015)
;She has a real feel for language. [CALD]
;उसे भाषा की  वास्तविक जानकारी है. [manual]

(defrule feel6
(declare (salience 5001))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 language|fabric|music)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAnakArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  feel.clp  	feel6   "  ?id "  jAnakArI)" crlf))
)

;@@@Added by 14anu-ban-02(07-01-2016)
;But now all at once he felt a desire to be with other people. [Crime and Punishment]
; लेकिन इस वक्त अचानक उसे दूसरे लोगों की संगत की इच्छा हुई[Crime and Punishment]
(defrule feel7
(declare (salience 4800))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 desire)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahasUsa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp 	feel7   "  ?id "  mahasUsa_ho )" crlf))
)

;------------------------ Default Rules ----------------------

;"feel","N","1.anuBava"
;Feel of this place is very depressing.
(defrule feel1
(declare (salience 4900))
(id-root ?id feel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feel.clp 	feel1   "  ?id "  anuBava )" crlf))
)

