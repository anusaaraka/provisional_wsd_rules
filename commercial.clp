;@@@ Added by 14anu-ban-03 (20-03-2015)
;Commercial baby foods.  [oald]
;व्यावसायिक शिशु आहार . [manual]
;A commercial radio station. [oald]
;एक व्यावसायिक रेडियो स्टेशन . [manual]
(defrule commercial2
(declare (salience 4900))
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAvasAyika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial2   "  ?id "  vyAvasAyika )" crlf))
)


;@@@ Added by 14anu-ban-03 (20-03-2015)
;The movie was not a commercial success. [oald]
;फिल्म व्यावसायिक रूप से सफल नहीं थी .  [manual]
(defrule commercial3
(declare (salience 4910))
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 success)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAvasAyika_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial3  "  ?id "  vyAvasAyika_rUpa_se )" crlf))
)


;@@@ Added by 14anu-ban-03 (20-03-2015)
;The show was commercial, with little artistic merit. [oald]
;शो कलात्मक योग्यता के साथ, थोड़ा मामूली था ।  [manual]
(defrule commercial4
(declare (salience 4900))
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 show)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAmUlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial4  "  ?id "  mAmUlI )" crlf))
)


;@@@ Added by 14anu-ban-03 (20-03-2015)
;A commercial break.  [oald]
;एक विज्ञापन अन्तराल. [manual]
(defrule commercial5
(declare (salience 5000))  
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 break)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vijFApana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial5  "  ?id "  vijFApana )" crlf))
)
;-----------------------------------------Default rules-------------------------------------------

(defrule commercial0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (20-03-2015)
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vANijyika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial0   "  ?id "  vANijyika )" crlf))
)

(defrule commercial1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (20-03-2015)
(id-root ?id commercial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commercial.clp 	commercial1   "  ?id "  vyApArika )" crlf))
)

;"commercial","Adj","1.vyApArika"
;Mohan is a commercial pilot in a private airline.
;
;
