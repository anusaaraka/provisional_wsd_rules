;Modified by Meena(9.3.10);deleted (id-word =(+ ?id 1) pl) added (viSeRya-wulanAwmaka.......)
;Thomas Edison tried two thousand different materials in search of a filament for the light bulb .(9th parse) 
(defrule different0
(declare (salience 5000))
(id-root ?id different)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-wulanAwmaka_viSeRaNa  ?id1 ?id)
;(id-word =(+ ?id 1) pl)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  different.clp 	different0   "  ?id "  alaga_alaga )" crlf))
)


(defrule different1
(declare (salience 4900))
(id-root ?id different)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) sg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  different.clp 	different1   "  ?id "  alaga )" crlf))
)

;Added by Pramila(Banasthali University)
;It was concluded by different scientists.
;It is found by different studies that medical science has a large space.
(defrule different3
(declare (salience 4850))
(id-root ?id different)
?mng <-(meaning_to_be_decided ?id)
(id-root-category-suffix-number =(+ ?id 1)  ? noun ? p)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBinna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  different.clp 	different3   "  ?id "  viBinna )" crlf))
)

;@@@Added by Manasa ( 23-02-2016 )
;We can broadly describe physics as a study of the basic laws of nature and their manifestation in different natural phenomena.
;हम प्रकृति के मूल नियम और अलग प्-अलग राकृतिक परिघटनाओं में उनके आविर्भाव का एक अध्ययन के जैसा भौतिक विज्ञान मोटे तौर पर व्यक्त_कर सकते हैं.
(defrule different4
(declare (salience 5000))
(id-root ?id different)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  different.clp        different4   "  ?id "  alaga_alaga )" crlf))
)


;"different","Adj","1.alaga"
;Both of them took different approaches to the problem.
(defrule different2
(declare (salience 100))
(id-root ?id different)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  different.clp 	different2   "  ?id "  alaga )" crlf))
)

