;@@@ Added by 14anu-ban-06 (24-03-2015)
;Anna took the old sword and hefted it in her hands.(OALD)
;अन्ना ने पुरानी तलवार ली और अपना हाथों में उसको तौला . (manual)
(defrule heft1
(declare (salience 2000))
(id-root ?id heft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 hand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wOla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heft.clp 	heft1   "  ?id "  wOla )" crlf))
)

;xxxxxxxxxxxx Default Rules xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (24-03-2015)
;The two men hefted the box into the car.(OALD)
;दो आदमियों ने गाडी में बक्सा उठाकर रखा . (manual)
(defrule heft0
(declare (salience 0))
(id-root ?id heft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTAkara_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heft.clp 	heft0   "  ?id "  uTAkara_raKa )" crlf))
)

;@@@ Added by 14anu-ban-06 (24-03-2015)
;She was surprised by the sheer heft of the package.(OALD)
;वह पार्सल के असली वजन से आश्चर्यचकित हो गयी थी .  (manual)
(defrule heft2
(declare (salience 0))
(id-root ?id heft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vajana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heft.clp 	heft2   "  ?id "  vajana )" crlf))
)
