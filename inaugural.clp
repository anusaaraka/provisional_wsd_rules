;@@@ Added by 14anu-ban-06  (01-09-14)
;It is based on the eighteen principles accepted by the RDP at its inaugural conference and popularised by it since then .(Parallel corpus)
;यह उन अठारह सिद्धांतो पर आधारित है जिऩ्हें रेडिकल डेमोक्रेटिक पार्टी ने अपने प्रारंभिक अधिवेशन में स़्वीकार किया था तब से इसको प्रचलित किया गया .
;It held its inaugural conference in Poona in June 1938 .(Parallel corpus)
;इसका प्रारंभिक सम़्मेलन जून 1938 में पूना में हुआ .
(defrule inaugural0
(declare (salience 0))
(id-root ?id inaugural)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inaugural.clp 	inaugural0   "  ?id "  prAramBika )" crlf))
)

;@@@ Added by 14anu-ban-06  (01-09-14)
;From Egypt besides the craftsmen a team of folk dancers are also coming who will give their daily show besides the inaugural function .(Parallel corpus)
;मिस्र से शिल्पकारों के अलावा लोकनर्तकों का एक दल भी आ रहा है जो उद्‍घाटन समारोह के अलावा रोजाना अपना कार्यक्रम देगा ।
;He also took a leading part in the formation of the Czechoslovak - Indian Association and addressed its inaugural ceremony in 1934 .(Parallel corpus)
;चेकोस़्लोवाकिया - भारत समिति ? के गठन में भी उऩ्होंने प्रमुख भूमिका निभाई तथा 1934 में उसके उद्घाटन - समारोह में भाषण भी दिया .
(defrule inaugural1
(declare (salience 2000))
(id-root ?id inaugural)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 ceremony|function)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxGAtana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inaugural.clp 	inaugural1   "  ?id "  uxGAtana )" crlf))
)
