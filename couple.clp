
;"coupling","N","1.yugmana/saMyojana"
;The concert was a rare coupling of Indian && Western classical music.
(defrule couple0
(declare (salience 5000))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id coupling )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id yugmana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  couple.clp  	couple0   "  ?id "  yugmana )" crlf))
)

;$$$ Modified by 14anu-ban-03 (03-04-2015)
;@@@ Added by 14anu-ban-09 on 05-08-2014
;Molecules in gases are very poorly coupled to their neighbors. 
;gEsoM ke aNu apane pAsa ke aNuoM se bahuwa halake se yugmiwa howe hEM.
(defrule couple3
(declare (salience 4800))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 molecule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yugmiwa_ho))  ;meaning changed from 'yugmiwa' to 'yugmiwa_ho'by 14anu-ban-03 (03-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  couple.clp 	couple3   "  ?id "  yugmiwa_ho )" crlf))
)


;@@@ Added by 14anu-ban-03 (03-04-2015)
;The doctor said my leg should be better in couple of days. [oald]
;डाक्टर ने कहा कि मेरी टाँग कुछ दिनों में अधिक बेहतर हो जाएगी. [manual]
(defrule couple4
(declare (salience 5000))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 day|year|month)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  couple.clp 	couple4   "  ?id "   kuCa )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-04-2015)
;They met couple of times but they didn't really connect. [oald]
;वे  बहुत बार मिले परन्तु वास्तव में उनमे कोई मेल नहीं बना. [manual]
(defrule couple5
(declare (salience 5000))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  couple.clp 	couple5   "  ?id "   bahuwa )" crlf))
)


;------------------------ Default Rules ----------------------
;"couple","N","1.yugala"
;A young married couple from Chicago are staying in that house.
;??He's coming for a couple of days.
(defrule couple1
(declare (salience 4900))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yugala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  couple.clp 	couple1   "  ?id "  yugala )" crlf))
)

;"couple","V","1.milAnA/jodZanA"
;The bad light, coupled with ugly sound of the mike,spoiled the show.
(defrule couple2
(declare (salience 4800))
(id-root ?id couple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  couple.clp 	couple2   "  ?id "  milA )" crlf))
)

