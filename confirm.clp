;@@@ Added by 14anu-ban-03 (01-04-2015)
;He was confirmed as captain for the rest of the season. [oald]
;वह  शेष  वर्ष के लिए कप्तान के रूप में पक्का किया गया था . [manual]
(defrule confirm1
(declare (salience 10))
(id-root ?id confirm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id ?id1) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakkA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confirm.clp 	confirm1  "  ?id " pakkA_kara)" crlf))
)


;@@@ Added by 14anu-ban-03 (01-04-2015)  
;I'm very happy to confirm you in your post. [oald]
;मैं  आपको आपकी जगह पर पक्का करके अत्यन्त खुश हूँ .   [manual]
(defrule confirm2
(declare (salience 10))
(id-root ?id confirm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakkA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confirm.clp 	confirm2  "  ?id " pakkA_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (01-04-2015)
;She was baptized when she was a month old and confirmed when she was thirteen. [oald]
;उसको बप्तिस्मा दिया गया था जब वह एक महीने की थी और जब वह तेरह की हो गयी थी उसे ईसाई समाज का हिस्सा बनाया  . [manual]
(defrule confirm3
(declare (salience 10))
(id-root ?id confirm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa ?id1 ?id)
(id-root ?id1 baptize)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IsAI_samAja_kA_hissA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confirm.clp 	confirm3  "  ?id " IsAI_samAja_kA_hissA_banA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (01-04-2015)
;His guilty expression confirmed my suspicions. [oald]
;उसके अपराधी हाव-भावों ने मेरे शक की पुष्टि की . [manual]
(defrule confirm0
(declare (salience 00))
(id-root ?id confirm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id puRti_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  confirm.clp  confirm0  "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confirm.clp  confirm0   "  ?id " puRti_kara)" crlf))
)

