;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu06(Vivek Agarwal) on 17/6/2014*******
;Beseech for help.
;मदद के लिए हाथ जोड़ना.

(defrule beseech0
(declare (salience 4800))
(id-root ?id beseech)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAWa_jodZanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beseech.clp     beseech0   "  ?id "  hAWa_jodZanA )" crlf))
)

;@@@ Added by 14anu06(Vivek Agarwal) on 17/6/2014*******
;He beseeched for their help.
;उसने उनकी मदद के लिए  अनुनय किया.

(defrule beseech1
(declare (salience 5000))
(id-root ?id beseech)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anunaya_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beseech.clp     beseech1   "  ?id "  anunaya_karanA )" crlf))
)

;@@@ Added by 14anu06(Vivek Agarwal) on 17/6/2014*******
;His beseeching eyes.
;उसकी अनुनयपूर्ण आंखें.

(defrule beseech2
(declare (salience 5100))
(id-root ?id beseech)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anunayapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  beseech.clp     beseech2   "  ?id "  anunayapUrNa )" crlf))
)
