
(defrule transfer0
(declare (salience 5000))
(id-root ?id transfer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnAnwaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  transfer.clp 	transfer0   "  ?id "  sWAnAnwaraNa )" crlf))
)

;"transfer","N","1.sWAnAnwaraNa"
;Uma's transfer was a surprise to her friends.
;
(defrule transfer1
(declare (salience 4900))
(id-root ?id transfer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwariwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  transfer.clp 	transfer1   "  ?id "  anwariwa_kara )" crlf))
)

;"transfer","VI","1.anwariwa_karanA"
;He got the money transferred from the bank.
;--"2.sWAnAnwariwa_karanA"
;Ajay was transferred from Delhi to Bhilai.
;--"3.sOMpanA"
;He transferred his property to his brother.
;--"4.anuciwriwa_karanA"
;Her novels were transferred into cinema.
;


;$$$ Modified by 14anu-ban-07 (27-09-2014)
;added or-condition and relation (kriyA-between_saMbanXI  ?id ?)
;In Section 11.8, we have learned that certain amount of heat energy is transferred between a substance and its surroundings when it undergoes a change of state.  (ncert corpus)
;अनुभाग 11.8 में हमने यह सीखा है कि जब कोई पदार्थ अवस्था परिवर्तन की स्थिति में होता है तो पदार्थ तथा उसके परिवेश के बीच ऊष्मा की एक निश्चित मात्रा स्थानांतरित होती है.
;@@@ Added by Prachi Rathore[29-3-14]
; He was transferred from Nehru Nagar Hariparvat to Bamrauli Katara just 3 days ago.[news]
;तीन दिन पहले ही हरीपर्वत की नेहरू नगर चौकी से बमरौली कटारा स्थानांतरण हुआ था।
(defrule transfer3
(declare (salience 5000))
(id-root ?id transfer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-from_saMbanXI ?id ?)(kriyA-to_saMbanXI  ?id ?)(kriyA-between_saMbanXI  ?id ?))
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnAnwariwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  transfer.clp 	transfer3   "  ?id "  sWAnAnwariwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-07 (08-11-2014)
;If a similar experiment is repeated with a nylon thread or a rubber band, no transfer of charge will take place from the plastic rod to the pith ball.(ncert)
;यदि इसी प्रयोग को नॉयलोन के धागे अथवा रबर के छल्ले के साथ दोहराएँ तो प्लास्टिक-छड से सरकण्डे की गोली में आवेश का स्थानान्तरण नहीं होता.(manual)
(defrule transfer4
(declare (salience 5100))
(Domain physics)
(id-root ?id transfer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) of)
(id-root =(+ ?id 2) charge)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) AveSa_kA_sWAnAnwaraNa))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " transfer.clp	 transfer4  "  ?id "  " =(+ ?id 1) =(+ ?id 2) " AveSa_kA_sWAnAnwaraNa  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  transfer.clp 	transfer4   "  ?id "  physics )" crlf))
)

