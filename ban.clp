;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 04-March-2014
;A campaign to ban smoking in public places.[oald]
;सार्वजनिक स्थानों में धूम्रपान पर प्रतिबंध लगाने के लिए एक अभियान.
(defrule ban2
(declare (salience 3000))
(id-root ?id ban)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibaMXa_lagA))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ban.clp 	ban2   "  ?id "  prawibaMXa_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " ban.clp 	ban2   "  ?id " para )" crlf)
)
)

;commented by 14anu-ban-02(10-03-2015)
;same rule as ban2
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 04-March-2014
;A campaign to ban smoking in public places.[oald]
;सार्वजनिक स्थानों में धूम्रपान पर प्रतिबंध लगाने के लिए एक अभियान.
;(defrule ban3
;(declare (salience 3000))
;(id-root ?id ban)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id prawibaMXa_lagA))
;(assert (kriyA_id-subject_viBakwi ?id para))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ban.clp 	ban3   "  ?id "  prawibaMXa_lagA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* " ban.clp 	ban3   "  ?id " para )" crlf)
;)
;)

;@@@Added by 14anu-ban-02(10-03-2015)
;Chemical weapons are banned internationally.[oald]
;रासायनिक शस्त्र अन्तर्राष्ट्रीय रूप से अवैध घोषित हैं . [self]
(defrule ban4
(declare (salience 3000))
(id-root ?id ban)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 weapon)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avEXa_GoRiwa))	;meaning suggested by chaitanay sir.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ban.clp 	ban4   "  ?id "  avEXa_GoRiwa )" crlf))
)

;***********************DEFAULT RULES****************************

;"ban","N","1.prawibaMXa"
;The government should put a ban on smoking in public places.
(defrule ban0
(declare (salience 0))
(id-root ?id ban)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ban.clp 	ban0   "  ?id "  prawibaMXa )" crlf))
)


;$$$Modified by 14anu-ban-02(10-03-2015)
;He was banned from the meeting.[oald]
;उस पर  मीटिंग के लिये प्रतिबन्ध लगाया गया था . [self]
;"ban","VT","1.prawibaMXa_lagAnA"
;The government has banned the use of chemical weapons.[old example]
(defrule ban1
(declare (salience 0))
(id-root ?id ban)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibaMXa_lagA))
(assert (kriyA_id-subject_viBakwi ?id para))	;added by 14anu-ban-02(10-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ban.clp 	ban1   "  ?id "  prawibaMXa_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " ban.clp 	ban1   "  ?id " para )" crlf)	
)	;added by 14anu-ban-02(10-03-2015)
)	



