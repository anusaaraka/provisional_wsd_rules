
;Added by Meena(19.02.10)
;There must be another sort of rules according to which sentences are composed .
;(defrule another00
;(declare (salience 5000))
;(id-root ?id another)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-of_saMbanXI  ?id1 ?id2)
;;(id-root ?id1 sort|room|matter)
;(viSeRya-det_viSeRaNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id xUsarA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another00   "  ?id "  xUsarA )" crlf))
;)

;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 03-jan-2014
;Would you like another drink?[oald]
;क्या आप एक और पेय या शरबत लेना पसंद करेंगे
;Added by Meena(19.02.10)
;There must be another sort of rules according to which sentences are composed .
(defrule another0
(declare (salience 4000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_Ora)); modified by Garima Singh.meaning changed from 'xUsarA' to 'eka_Ora'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another0   "  ?id "  eka_Ora )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 03-jan-2014
;After fasting and praying for another three days, the King went to his court again.[gyananidhi]
;और तीन दिन का उपवास और प्रार्थना करने के बाद राजा सिंहासन की ओर बढ़ा।
(defrule another3
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another3   "  ?id "  Ora )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 03-jan-2014
;Let's do it another time.[oald]
;चलिये हम इसको किसी और समय करते है . 
(defrule another4
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another4   "  ?id "  kisI_Ora )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 03-jan-2014
;I'm going to ask for another.[oald]
;मैं दूसरे के लिये पूछने जा रहा हूँ 
(defrule another5
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another5   "  ?id "  xUsarA )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 22/03/2014
;Although the number of physical quantities appears to be very large we need only a limited number of units for expressing all the physical quantities since they are inter-related with one another.[oald]
;यद्यपि हमारे द्वारा मापी जाने वाली भौतिक राशियों की सङ्ख्या बहुत अधिक है, फिर भी, हमें इन सब भौतिक राशियों को व्यक्त करने के लिए, मात्रकों की सीमित सङ्ख्या की ही आवश्यकता होती है, क्योङ्कि, ये राशियाँ एक दूसरे से परस्पर सम्बन्धित हैं.
(defrule another6
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?kri ?id1)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?kri inter-related)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id ?id1 eka_xUsare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " another.clp   another6  "  ?id "  " ?id1 "  eka_xUsare )" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-10-2014)
;In this chapter, you will learn what heat is and how it is measured, and study the various proceses by which heat flows from one body to another.[NCERT corpus]
;इस अध्याय में आप यह जानेंगे कि ऊष्मा क्या है और इसे कैसे मापते हैं, तथा एक वस्तु से दूसरी वस्तु में ऊष्मा प्रवाह की विभिन्न प्रक्रियाओं का अध्ययन करेंगे.[NCERT corpus]
(defrule another7
(declare (salience 3000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another7   "  ?id "  xUsarA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-12-2014)
;It consists of an evacuated glass/quartz tube having a photosensitive plate C and another metal plate A.(Ncert) 
;इसमें एक निर्वातित काँच/क्वार्टज की नली है जिसमें एक प्रकाश-संवेदी पट्टिका C और दूसरी धातु पट्टिका A है.(Ncert)
(defrule another8
(declare (salience 4500))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-root ?id2 plate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another8   "  ?id "  xUsarA )" crlf))
)

;@@@Added by 14anu-ban-02(09-02-2015)
;Energy comes in many a forms which transform into one another in ways which may not often be clear to us.[ncert]
;ऊर्जा बहुत से रूपों में प्राप्त होती है जिनको एक रूप से दूसरे रूप में कई विधियों द्वारा रूपान्तरित किया जाता है जो प्रायः हमें भी कभी-कभी स्पष्ट नहीं होते.[ncert]
(defrule another9
(declare (salience 1000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 one)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another9   "  ?id "  xUsarA )" crlf))
)

;********************DEFAULT RULES**********************************
;Salience reduced by Meena(19.02.10)
(defrule another1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp   another1   "  ?id "  eka_Ora )" crlf))
)




;"another","Adj","1.eka_Ora/xUsarA"
;Would you like to have another cup of tea?
;Take this book && bring another one.
;


;Salience reduced by Meena(19.02.10)
(defrule another2
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_Ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  another.clp 	another2   "  ?id "  eka_Ora )" crlf))
)

;"another","Det","1.eka_Ora"

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_another6
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?kri ?id1)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?kri inter-related)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id ?id1 eka_xUsare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng " ?*prov_dir* " another.clp   sub_samA_another6   "   ?id " " ?id1 " eka_xUsare )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_another6
(declare (salience 5000))
(id-root ?id another)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?kri ?id1)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-word ?kri inter-related)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id ?id1 eka_xUsare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng " ?*prov_dir* " another.clp   obj_samA_another6   "   ?id " " ?id1 " eka_xUsare )" crlf))
)
