

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;NOTE-Example sentence need to be added.
(defrule peaceful0
(id-root ?id peaceful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peaceful.clp 	peaceful0  "  ?id "  SAMwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;The riot began as a peaceful protest. [oald]
;दङ्गे की एक शान्तिपूर्ण विरोध से शुरुआत हुई .	              [Manual] 
(defrule peaceful1
(declare (salience 1000))
(id-root ?id peaceful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 protest)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwipUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  peaceful.clp 	peaceful1  "  ?id "  SAMwipUrNa )" crlf))
)

