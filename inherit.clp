;@@@ Added by 14anu-ban-06 (14-03-2015)
;Policies inherited from the previous administration.(OALD)
;पिछले प्रशासन से प्राप्त की गई नीतिया . (manual)
(defrule inherit1
(declare (salience 2000))
(id-root ?id inherit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
(id-root ?id1 administration)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " inherit.clp   inherit1   "   ?id " prApwa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (14-03-2015)
;He has inherited his mother's patience. (OALD)
;उसने उसकी माँ का धीरज उत्तराधिकार में प्राप्त किया है . (manual)
(defrule inherit0
(declare (salience 0))
(id-root ?id inherit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwarAXikAra_meM_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " inherit.clp   inherit0   "   ?id " uwwarAXikAra_meM_prApwa_kara )" crlf))
)

