;@@@ Added by 14anu-ban-06 (17-11-2014)
;After the independence , Indian government appointed two committees twice in 1956 and 1977 to investigate about the accident.(parallel corpus)
;स्वतंत्रता के पश्चात भारत सरकार ने इस घटना की जाँच करने के लिए 1956 और 1977 में दो बार एक आयोग को नियुक्त किया ।(parallel corpus)
(defrule investigate0
(declare (salience 0))
(id-root ?id investigate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  investigate.clp 	investigate0   "  ?id "  jAzca_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-11-2014)
;Wilhelm Hallwachs and Philipp Lenard investigated the phenomenon of photoelectric emission in detail during 1886-1902.(NCERT)
;विलहेल्म हालवॉक्स तथा फिलिप लीनार्ड ने 1886-1902 के बीच प्रकाशविद्युत उत्सर्जन की परिघटना का अन्वेषण किया.(NCERT)
;In 1906, he proposed a classic experiment of scattering of these α-particles by atoms to investigate the atomic structure.(NCERT)
;परमाणु की संरचना का अन्वेषण करने के लिए उन्होंने सन् 1906 में परमाणुओं द्वारा ऐल्फा-कणों के प्रकीर्णन से सम्बन्धित एक क्लासिकी प्रयोग प्रस्तावित किया.
;(NCERT)
;He investigated this phenomenon.(NCERT)
;उन्होंने इस परिघटना का अन्वेषण किया.(NCERT-improvised)
(defrule investigate1
(declare (salience 2000))
(id-root ?id investigate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 structure|phenomenon)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anveRaNa_kara))
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  investigate.clp 	investigate1   "  ?id "  anveRaNa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  investigate.clp      investigate1   "  ?id1 " kA )" crlf)
)
)

