;@@@ Added by 14anu-ban-05 on (08-04-2015)
;They communicated entirely by gesture.	[OALD]
;वे इशारों द्वारा पूरी बातचीत की.		[MANUAL]

(defrule gesture1
(declare (salience 101))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-by_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id iSArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp  	gesture1   "  ?id "  iSArA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-04-2015)
;They sent some flowers as a gesture of sympathy to the parents of the child. [OALD]
;वे बच्चे के माता-पिता के लिए सहानुभूति के भाव के रूप में कुछ फूल भेजे.		[MANUAL]

(defrule gesture2
(declare (salience 102))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp  	gesture2   "  ?id "  BAva )" crlf))
)


;@@@ Added by 14anu-ban-05 on (08-04-2015)
;It was a nice gesture to invite his wife too.[OALD]
;उसकी पत्नी को भी आमन्त्रित करना एक अच्छा व्यवहार था .	[MANUAL]

(defrule gesture3
(declare (salience 103))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)
(id-root ?id1 invite)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp  	gesture3   "  ?id "  vyavahAra )" crlf))
)


;@@@ Added by 14anu-ban-05 on (08-04-2015)
;His speech was at least a gesture towards improving relations between the two countries. [OALD]
;उनका भाषण दोनों देशों के बीच संबंधों में सुधार की ओर कम से कम एक संकेत था. 	[MANUAL]
;The government has made a gesture towards public opinion. 		[OALD]
;सरकार ने जनता की राय की ओर एक संकेत किया है.			[MANUAL]

(defrule gesture4
(declare (salience 104))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-towards_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMkewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp  	gesture4   "  ?id "  saMkewa )" crlf))
)


;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (08-04-2015)
;He made a rude gesture at the driver of the other car. [OALD]
;उसने दूसरी गाडी के चालक पर एक अभद्र मुद्रा बनाई . [MANUAL]

(defrule gesture0
(declare (salience 100))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id muxrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp   gesture0   "  ?id "  muxrA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-04-2015)
;He gestured to the guards and they withdrew.	[OALD]
;उसने सुरक्षाकर्मियों को इशारा किया और वे वापिस हो गये . 	[MANUAL]

(defrule gesture5
(declare (salience 100))
(id-root ?id gesture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id iSArA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gesture.clp  	gesture5   "  ?id "  iSArA_kara )" crlf))
)


