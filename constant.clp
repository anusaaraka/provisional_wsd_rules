
;@@@Added by Preeti(6-12-13)
;A small baby requires constant care. 
;eka Cote SiSu ko niraMwara xeKaBAla kI jarUrawa howI hE.
(defrule constant2
(declare (salience 5050))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niraMwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant2   "  ?id "  niraMwara )" crlf))
)

;@@@ Added by 14anu01 on 28-06-2014
;Planck constant is used in calculation of energy.
;प्लाङ्क स्थिराङ्क ऊर्जा की गणना में उपयोग किया जाता है . 
(defrule constant3
(declare (salience 5100))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse =(- ?id 1) PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWirAMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant3   "  ?id "  sWirAMka )" crlf))
)


;@@@ Added by 14anu-ban-03 (13-10-2014)
;Note that value of constant k can not be obtained by the method of dimensions.[ncert]
;ध्यान दीजिए, यहाँ स्थिराङ्क k का मान विमीय विधि से ज्ञात नहीं किया जा सकता है. [ncert]
;Where k is dimensionless constant and x, y and z are the exponents. [ncert]
;जहाँ, k एक विमाहीन स्थिराङ्क है, एवं x, y, z घाताङ्क हैं.[ncert]
(defrule constant4
(declare (salience 5050))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id) (subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-word ?id1 k)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWirAMka ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant4   "  ?id "  sWirAMka )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-10-2014)
;However, dimensionless constants can not be obtained by this method.[ncert]
;तथापि विमाहीन स्थिराङ्कों के मान इस विधि द्वारा ज्ञात नहीं किए जा सकते. [ncert]
(defrule constant5
(declare (salience 5100))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id  ?id1)
(id-root ?id1 dimensionless)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWirAMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant5   "  ?id "  sWirAMka )" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-10-2014)
;When the pressure is held constant, the volume of a quantity of the gas is related to the temperature as V/T = constant. [ncert corpus]
;jaba xAba ko niyawa raKawe hEM, wo kisI niSciwa parimANa kI gEsa kA Ayawana usake wApa se isa prakAra saMbaMXiwa hE: @V/@T = niyawAMka. [ncert corpus]
(defrule constant6
(declare (salience 5500))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id adjective)
(id-root ?id1  pressure)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant6   "  ?id "  niyawa )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-11-2014)
;When the car moves with constant velocity, there is no net external force. [ncert]
;जब कार एक समान वेग से गति करती है तब उस पर कोई नेट बाह्य बल नहीं होता . [ncert]
(defrule constant7
(declare (salience 5050))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 velocity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant7   "  ?id " samAna  )" crlf))
)

;@@@ Added by 14anu-ban-03 (24-11-2014)
;We write it as F = Gm1 m2/r2, where G is the universal constant of gravitation.[ncert]
;ise hama isa prakAra @F = @Gm1 @m2/@r2 vyakwa karawe hEM, yahAz @G guruwvAkarRaNa kA sArvawrika niyawAfka hE.[ncert]
(defrule constant8
(declare (salience 5000))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 gravitation)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyawAfka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant8   "  ?id "  niyawAfka )" crlf))
)

;@@@ Added by 14anu-ban-03 (29-11-2014)
;Whenever this constant is an integral multiple of λ, the fringe will be bright and whenever it is an odd integral multiple of λ / 2 it will be a dark fringe.[ncert]
;jaba BI yaha niyawAfka λ kA samAkala guNaka hE, PriMja xIpwa hogA waWA jaba yaha λ/2 kA viRama samAkala guNaka hE, PriMja axIpwa hogA.[ncert]
(defrule constant9
(declare (salience 5050))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(id-root ?id1 multiple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyawAfka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant9   "  ?id "  niyawAfka )" crlf))
)

;------------------------ Default rules -------------------------

;$$$ Modified by 14anu-ban-03(05-08-2014)
;Meaning changed from 'adiga' to 'sWira'.
;From Fig. 3.7, we note that during the period t = 10 s to 18 s the velocity is constant. 
;ciwra 3.7 se yaha spaRta hE ki @t = 10 @s se 18 @s ke maXya vega sWira rahawA hE .
(defrule constant0
(declare (salience 5000))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant0   "  ?id "  sWira )" crlf))
)

(defrule constant1
(declare (salience 4900))
(id-root ?id constant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  constant.clp 	constant1   "  ?id "  sWAyI )" crlf))
)

;"constant","N","1.sWAyI"
;Pressure in the container remains constant.
;--"2.niwya"
;This route is in constant use.
;--"3.vaPAxAra"
;My pet dog has been my constant companion.
;
;

