
;@@@ Added by 14anu-ban-02(06.09.14)
;this accidental discovery of capt . john smith was none other than the famous ajanta caves .[karan singla]
;यह कैपटन जान स्र्रिमथ की आकस्मिक् खोज , और कहीं की नहीं बल्र्रिक प्रसिद्ध अजंता की गुफाओं की थी .[karan singla]
(defrule accidental0
(id-root ?id accidental)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accidental.clp 	accidental0   "  ?id "  Akasmika )" crlf))
)

;@@@ Added by 14anu-ban-02(06.09.14)
;Is this a curious coincidence between these large numbers purely accidental?  [ncert]
;क्या इन विशाल सङ्ख्याओं की यह आश्चर्यजनक, अनुरूपता मात्र संयोग है?[ncert]
(defrule accidental1
(declare(salience 100))
(id-root ?id accidental)
?mng <-(meaning_to_be_decided ?id)
(viSeRaNa-viSeRaka  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accidental.clp 	accidental1   "  ?id "  saMyoga )" crlf))
)
