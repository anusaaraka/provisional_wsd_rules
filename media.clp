;@@@Added by 14anu-ban-08 (11-02-2015)
;The media plays a big role in creating doubts in peoples minds.  [hindkhoj]
;संचार माध्यम लोगों के दिमागों में संदेह उत्पन्न करने में एक महत्वपूर्ण भूमिका निभाता हैं.   [self]
(defrule media0
(declare (salience 0))
(id-root ?id media)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcAra_mAXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  media.clp 	media0   "  ?id "  saMcAra_mAXyama)" crlf))
)

;@@@Added by 14anu-ban-08 (11-02-2015)
;They have different values for different media.  [NCERT]
;इनके विभिन्न माध्यमों के लिए भिन्न-भिन्न मान हैं.   [NCERT]
(defrule media1
(declare (salience 10))
(id-root ?id media)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 different)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  media.clp 	media1   "  ?id "  mAXyama)" crlf))
)


