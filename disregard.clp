;@@@ Added by 14anu-ban-04  (01-04-2015)
;Please disregard what I said before.                 [merriam webster]	
;कृपया ध्यान मत दीजिए मैंने जो पहले कहा .                           [self]
(defrule disregard2
(declare (salience 20))            
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma ?id ?id1)
(kriyA-subject ?id1 ?id2)
(id-root ?id1 say)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_mawa_xe))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard2   "  ?id "  XyAna_mawa_xe )" crlf))
)


;@@@ Added by 14anu-ban-04  (01-04-2015)
;You cannot disregard the fact that heart disease is the biggest killer in the western world.               [oald]
;आप इस तथ्य की उपेक्षा नहीं कर सकते हैं कि पश्चिमी विश्व में  हृदय रोग सबसे  बड़ा घातक रोग है .                                 [self]
(defrule disregard3
(declare (salience 20))             
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 fact)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))   
(assert (id-wsd_root_mng ?id upekRA_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disregard.clp     disregard3   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard3  "  ?id "  upekRA_kara )" crlf))
)

;@@@ Added by 14anu-ban-04  (01-04-2015)
;Safety rules were disregarded.                          [oald]            ;run this sentence on parse no. 2
;सुरक्षा नियमों पर ध्यान नहीं दिया गया था .                             [self]
(defrule disregard4
(declare (salience 30))            
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 rule)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id para))   
(assert (id-wsd_root_mng ?id XyAna_nahIM_xe))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  disregard.clp     disregard4   "  ?id "  para  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard4   "  ?id "  XyAna_nahIM_xe )" crlf))
)

;@@@ Added by 14anu-ban-04  (01-04-2015)
;The company showed a disregard for the safety of the environment.             [oald]
;कम्पनी ने पर्यावरण की सुरक्षा के लिए लापरवाही दिखाई .                                  [self]
(defrule disregard5
(declare (salience 20))               
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id)
(kriyA-for_saMbanXI  ?kri ?id1)
(id-root ?id1 safety)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAparavAhI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard5   "  ?id "  lAparavAhI )" crlf))
)

;------------------------ Default Rules ---------------------

;"disregard","N","1.anAxara"
;He shows a total disregard for his wife's feelings.
(defrule disregard0
(declare (salience 10))              ;decreased salience from '5000' to '10'  by 14anu-ban-04 on  (01-04-2015) 
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anAxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard0   "  ?id "  anAxara )" crlf))
)

;$$$ Modified by 14anu-ban-04 (01-04-2015)         -------changed meaning from 'wucCa_jAna' to 'wiraskAra_kara'
;He totally disregarded all the advice that he was given.                   [oald]
;उसने पूर्ण रूप से सभी सलाह का तिरस्कार किया जो उसको दी गयी थी.                                [self]    
(defrule disregard1
(declare (salience 10))            ;decreased salience from '4900' to '10'  by 14anu-ban-04 on  (01-04-2015)
(id-root ?id disregard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))                ;added by 14anu-ban-04 on  (01-04-2015)
(assert (id-wsd_root_mng ?id wiraskAra_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disregard.clp     disregard1   "  ?id " kA  )" crlf)                    ;added by 14anu-ban-04 on  (01-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disregard.clp 	disregard1   "  ?id "  wiraskAra_kara )" crlf))
)

;"disregard","VT","1.wucCa_jAnanA"
;Her husbanded disregarded her feelings.
;
