;@@@ Added by 14anu-ban-06 (24-02-2015)
;In the Caribbean waters there are fish of every hue.(cambridge)
;कैरिबियन पानी में प्रत्येक रंग की मछलियाँ हैं . (manual)
(defrule hue0
(declare (salience 0))
(id-root ?id hue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hue.clp 	hue0   "  ?id "  raMga )" crlf))
)


;@@@ Added by 14anu-ban-06 (24-02-2015)
;Supporters of every political hue.(OALD)
;प्रत्येक राजनैतिक मत के समर्थक . (manual)
;Influential sectors of the public, and certainly governments of whatever political hue, are likely to want more spent on nonmilitary security goods. (COCA)
;जनता के प्रभावशाली सेक्टर, और निश्चित रूप से किसी भी राजनैतिक मत की सरकारें, असैनिक सुरक्षा हित पर अधिक खर्च करना चाहती हैं .(manual)
(defrule hue1
(declare (salience 2000))
(id-root ?id hue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 political)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hue.clp 	hue1   "  ?id "  mawa )" crlf))
)
