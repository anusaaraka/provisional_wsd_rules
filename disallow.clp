;@@@ Added by 14anu-ban-04 (02-04-2015)
;The second goal was disallowed.             [oald]            ;run this sentence on parse no. 2
;दूसरे  गोल को रद्द कर दिया गया था .                    [self]
;The England team had two goals disallowed.    [cald]          ;run this sentence on parse no. 3
;इंग्लैंड दल के दो गोल रद्द कर दिए   थे .                 [self]
(defrule disallow1
(declare (salience 30))
(id-root ?id disallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id1)(viSeRya-kqxanwa_viSeRaNa ?id1 ?id))
(id-root ?id1 goal)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id raxxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  disallow.clp     disallow1  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disallow.clp    disallow1  "  ?id " raxxa_kara_xe)" crlf))
)

;@@@ Added by 14anu-ban-04 (02-04-2015)
;The court will disallow evidence obtained through torture.           [oald]
;अदालत यातना के माध्यम से प्राप्त  किया हुआ साक्ष्य मानने से इन्कार करेगी .                    [self]
(defrule disallow2
(declare (salience 20))
(id-root ?id disallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnane_se_inkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disallow.clp    disallow2 "  ?id " mAnane_se_inkAra_kara)" crlf))
)


;-----------------------------------------DEFAULT RULE ---------------------------------------------

;@@@ Added by 14anu-ban-04 (02-04-2015)
;All protests have been disallowed in the city.                [oald]        
;शहर में  सभी विरोधों को निषेध किया गया है .                                 [self]
(defrule disallow0
(declare (salience 10))
(id-root ?id disallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id niReXa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  disallow.clp     disallow0  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disallow.clp    disallow0 "  ?id " niReXa_kara)" crlf))
)
