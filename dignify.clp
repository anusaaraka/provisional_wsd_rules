;@@@ Added by 14anu-ban-04 (07-04-2015)
;The mayor was there to dignify the celebrations.               [oald]
;मेयर वहाँ पर उत्सव की गौरवान्विता बढ़ाने के लिए  था .                             [self]
(defrule dignify1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignify)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 celebration|occasion)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI)) 
(assert (id-wsd_root_mng ?id gOravAnviwA_baDA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   dignify.clp   dignify1   "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignify.clp       dignify1  "  ?id "  gOravAnviwA_baDA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (07-04-2015)
;I'm not going to dignify his comments by reacting to them.                [oald]
;मैं उसकी टिप्पणियों की प्रतिष्ठा नहीं करने जा रहा हूँ  उन के विषय में  पर प्रतिक्रिया देकर .  [self]
(defrule dignify0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignify)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id prawiRTA_kara/sammAna_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   dignify.clp   dignify0   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignify.clp       dignify0  "  ?id "  prawiRTA_kara/sammAna_kara )" crlf))
)

