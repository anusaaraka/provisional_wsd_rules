;$$$ Modified by 14anu-ban-06 (05-11-2014)
;###[COUNTER EXAMPLE]### Gases, on the other hand exhibit a large variation in densities with pressure.(NCERT);added by 14anu-ban-06 (05-11-2014)
;###[COUNTER EXAMPLE]### इसके विपरित, गैसें दाब में परिवर्तन के साथ घनत्व में अत्यधिक परिवर्तन दर्शाती हैं.(NCERT)
;$$$ Modified by Pramila(Banasthali university) on 22-02-2014
;They will be exhibiting their new designs at the trade fairs.   ;oald
;वे व्यापार मेलों में अपने नए डिजाइनों का प्रदर्शन करेंगे.
;He exhibits regularly in local art galleries.   ;oald
;वह स्थानीय कला चित्रशालाओं में नियमित रूप से प्रदर्शन करता है.
(defrule exhibit1
(declare (salience 4900))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-at_saMbanXI  ?id ?id1)(kriyA-in_saMbanXI  ?id ?id1))
(id-root ?id1 fair|gallery|museum);added by 14anu-ban-06 (05-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit1   "  ?id "  praxarSana_kara )" crlf))
)

;"exhibit","VT","1.praxarSana_karanA"
;He exhibits a great talent
;


;@@@ Added by 14anu-ban-06 (05-11-2014)
;The different arts of India are exhibited here .(parallel corpus)
;यहाँ भारत की विभिन्न कलाओं को प्रदर्शित किया गया है ।(parallel corpus)
;Very beautiful crafts and pictures have been exhibited in them from 9th century till 18th century .(parallel corpus)
;इसमें 9 वीं शताब्दी से लेकर 18 वीं शताब्दी तक के अत्यंत सुंदर शिल्प एवं चित्र प्रदर्शित किये गये हैं ।(parallel corpus)
(defrule exhibit3
(declare (salience 5000))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 art|craft|picture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit3   "  ?id "  praxarSiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (07-02-2015)
;The first exhibit was a knife which the prosecution claimed was the murder weapon.             [oald]
;पहला सबूत एक चाकू था जिसे  अभियोग पक्ष ने  कत्ल  का  हथियार दावा किया  था .                      [manual]
(defrule exhibit4
(declare (salience 5010))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(viSeRya-jo_samAnAXikaraNa ?id1 ?id3)             ;added by 14anu-ban-04 on (23-03-2015)
(kriyA-subject ?kri ?id2)                         ;added by 14anu-ban-04 on (23-03-2015)
(kriyA-object ?kri ?id3)                          ;added by 14anu-ban-04 on (23-03-2015)
(id-root ?id1 knife) 
(id-root ?id2 prosecution)                        ;added by 14anu-ban-04 on (23-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabUwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit4   "  ?id "  sabUwa )" crlf))
)


;@@@ Added by 14anu-ban-04 (07-02-2015)
;The museum has a fascinating collection of exhibits ranging from Iron Age pottery to Inuit clothing.              [cald]
;संग्रहालय में प्रदर्शित वस्तुओं का लौह युग के बर्तनों से लेकर इनुइट कपड़ों तक एक आकर्षक संग्रह है।             [self]
;The museum had rare exhibits collected from China. 
;The museum contains some interesting exhibits on Spanish rural life.
(defrule exhibit5
(declare (salience 5010))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?kri ?id1)                                         ;added by 14anu-ban-04 (17-02-2015)
(id-root ?id1 museum)                                             ;added by 14anu-ban-04 (17-02-2015)  
(or(viSeRya-of_saMbanXI ?id2 ?id)(kriyA-object ?kri ?id))         ;added 'kriyA-object' by 14anu-ban-04 (17-02-2015)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSiwa_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit5   "  ?id "  praxarSiwa_vaswu )" crlf))
)

;------------------------- Default Rules -----------------------
;"exhibit","N","1.praxarSana"
;The museum had many exhibits of oriental art
(defrule exhibit0
(declare (salience 5000))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit0   "  ?id "  praxarSana )" crlf))
)


;$$$ Modified by 14anu-ban-06 (05-11-2014)
;Gases, on the other hand exhibit a large variation in densities with pressure.(NCERT);added by 14anu-ban-06 (05-11-2014)
;इसके विपरित, गैसें दाब में परिवर्तन के साथ घनत्व में अत्यधिक परिवर्तन दर्शाती हैं.(NCERT)
;@@@ Added by Pramila(Banasthali university) on 22-02-2014
;He exhibited great self-control considering her rudeness.   ;cald
;उसने उसकी अशिष्टता का लिहाज करते हुए बहुत आत्म - नियंत्रण दिखाया.
(defrule exhibit2
(declare (salience 100))
(id-root ?id exhibit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKA/xarSA));added xarSA by 14anu-ban-06 (05-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exhibit.clp 	exhibit2   "  ?id "  xiKA/xarSA )" crlf))
)

