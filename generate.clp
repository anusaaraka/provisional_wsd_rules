;@@@ Added by 14anu-ban-05 on (21-10-2014)
;We need someone to generate new ideas.[OALD]
;हमें नए विचारों को उत्पन्न करने के लिए किसी की जरूरत है.[MANUAL]
(defrule generate0
(declare (salience 1000))
(id-root ?id generate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  generate.clp  	generate0   "  ?id "   uwpanna_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-10-2014)
;Sometimes technology gives rise to new physics; at other times physics generates new technology.[NCERT]
;kaBI prOxyogikI navIna BOwikI ko janma xewI hE, wo kaBI BOwikI navIna prOxyogikI uwpanna karawI hEM.[NCERT]
;In winter, in order to feel warm, we generate heat by vigorously rubbing our palms together.[NCERT]
;SIwakAla meM hama apanI haWeliyoM ko Apasa meM jora se ragadakara URmA uwpanna karawe hEM.[NCERT]
;The impact and deformation during collision may generate heat and sound.[NCERT]
;safGatta ke xOrAna takkara Ora vikqwi, URmA Ora Xvani uwpanna karawe hEM.[NCERT]
(defrule generate1
(declare (salience 5000))
(Domain physics)
(id-root ?id generate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 technology|heat|sound)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_kara))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generate.clp 	generate1  "  ?id "   uwpanna_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generate.clp       generate1   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-10-2014)
;When a spring is compressed or extended by an external force, a restoring force is generated.[NCERT]
;jaba kisI kamAnI ko kisI bAhya bala xvArA sampIdiwa aWavA viswAriwa kiyA jAwA hE, waba eka prawyAnayana bala uwpanna howA hE.[NCERT]
;Some circular motions called eddies are also generated.[NCERT]
;kuCa vqwwIya gawiyAz jinheM Bazvara kahawe hEM, BI uwpanna howI hEM.[NCERT]
;He observed that boring of a brass cannon generated a lot of heat, indeed enough to boil water.[NCERT]
;inhoMne pAyA ki pIwala kI wopa meM Cexa karawe samaya iwanI aXika URmA uwpanna howI hE ki usase pAnI ubala sakawA hE .[NCERT]
(defrule generate2
(declare (salience 5000))
(Domain physics)
(id-root ?id generate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 force|eddy|boring)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_ho))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generate.clp 	generate2  "  ?id "   uwpanna_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generate.clp       generate2   "  ?id "  physics )" crlf))
)
