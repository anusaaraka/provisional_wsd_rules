;@@@ Added by 14anu21 on 20.06.2014
;This is an unhappy situation.
;यह एक अप्रसन्न हालत है.
;यह एक दु:खद स्थिति है.
(defrule unhappy0
(declare (salience 1000)) ;added by 14anu-ban-07 (27-01-2015)
(id-root ?id unhappy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuHKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unhappy.clp     unhappy0   "  ?id "  xuHKaxa )" crlf))
)

;@@@ Added by 14anu-ban-07 (27-01-2015)
;They were unhappy with their accommodation. (oald)
;वे उनके रुकने की जगह से अप्रसन्न थे . 
(defrule unhappy1
(id-root ?id unhappy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aprasanna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unhappy.clp     unhappy1   "  ?id "  aprasanna )" crlf))
)


