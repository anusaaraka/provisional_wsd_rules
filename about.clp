;;FILE MODIFIED BY Meena(20.5.10); DELETED THE REDUNDANT RULES 


;@@@ Added by Garima Singh(M.Tech-C.S) 31-dec-2013
;We can ask the gardeners to gather the buds just as they are about to open every evening.[gyananidhi]
;हम माली को कलियाँ इकठ्ठा करने के लिये कह सकते हैं जैसे ही वे प्रत्येक सन्ध्या खिलने वाली हो 
(defrule about10
(declare (salience 5000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(kriyA-vAkya_viBakwi  ?id ?id2)
(id-word ?id2 as)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " about.clp	about10  "  ?id "  " (+ ?id 1) "  vAlA  )" crlf))
)


;Commented by 14anu-ban-02 (28-08-2014)
;@@@ Added by Garima Singh(M.Tech-C.S) 11-dec-2013
;Grandpa, you said you would tell us about Bhagirath.
;दादाजी, आपने कहा था कि आप हमें भागीरथ के बारे में बताएँगे
;(defrule about9
;(declare (salience 4000))
;(id-root ?id about)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-about_saMbanXI  ?kri ?id1)
;(id-cat_coarse ?id1 ?cat&~number)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_bAre_meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about9   "  ?id "  ke_bAre_meM )" crlf))
;)


;@@@ Added by Garima Singh(M.Tech-C.S) 6-dec-2013
;I was about to tell you, he said.
;मैं तुमको बताने ही वाला था, उसने कहा
(defrule about8
(declare (salience 5000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(kriyA-object  ?id1 ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) hI_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " about.clp	about8  "  ?id "  " (+ ?id 1) "  hI_vAlA  )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S) 4-dec-2013
;The interview lasted about an hour.
;उसका इण्टरव्यू लगभग एक घन्टा चला
(defrule about7
(declare (salience 5000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?kri ?id1)
(id-word ?kri lasted)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about7   "  ?id "  lagaBaga )" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (20-09-2016)
;How about going for a walk?(oald)
;सैर के बारे में क्या ख्याल है ? 
;Added by Meena(13.5.11)
(defrule what_about
(declare (salience 4600))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) how|what)
(id-cat_coarse =(+ ?id 1) verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(+ ?id 1) ke_bAre_meM_kyA_KyAla_hE)) ;changed from '0_ne_ke_bAre_meM_kya_KyAla_hE' to 'ke_bAre_meM_kyA_KyAla_hE' by Bhagyashri Kulkarni (20.09.2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " about.clp   what_about  "  ?id "  " (- ?id 1) " "(+ ?id 1)"  ke_bAre_meM_kyA_KyAla_hE )" crlf))
)





(defrule about0
(declare (salience 5000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) have|has|had)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about0   "  ?id "  lagaBaga )" crlf))
)

; I have about 100 Rs.





(defrule about1
(declare (salience 4700))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) accept|acknowledge|add|admit|agree|allege|announce|answer|argue|arrange|assert|assume|assure|believe|boast|check|claim|comment|complain|concede|conclude|confirm|consider|contend|convince|decide|demonstrate|deny|determine|discover|dispute|doubt|dream|elicit|ensure|estimate|expect|explain|fear|feel|figure|find|foresee|forget|gather|guarantee|guess|hear|hold|hope|imagine|imply|indicate|inform|insist|judge|know|learn|maintain|mean|mention|note|notice|notify|object|observe|perceive|persuade|pledge|pray|predict|pretend|promise|prophesy|prove|read|realize|reason|reassure|recall|reckon|record|reflect|remark|remember|repeat|reply|report|require|resolve|reveal|say|see|sense|show|state|suggest|suppose|swear|teach|tell|think|threaten|understand|vow|warn|wish|worry|write)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_ke_bAre_meM_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about1   "  ?id "  isa_ke_bAre_meM_ki )" crlf))
)

; He told us about who killed whom.




(defrule about2
(declare (salience 4600))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usa_ke_bAre_meM_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about2   "  ?id "  usa_ke_bAre_meM_ki )" crlf))
)

; He was thinking about who got killed in the accident.


;$$$ Modified by 14anu02 on 20.6.14
;counter ex-The suspect was about 2 metres tall. 
;$$$ modified by Garima Singh(M.Tech-C.S) 4-dec-2013
;Modified by Meena(27.5.11); added "half" ,"an" in the list Ex; The interview lasted about an hour.
;Added (or(id-cat =(+ ?id 1) <num>)(id-cat_coarse =(+ ?id 1) number)(......) for {"The suspect was about 2 metres tall."} Meena (11.5.11) 
;Modified by Meena(20.5.10)
;Typically , a doctor will take about thirty patients a day .
(defrule about3
(declare (salience 5000))	;salience increased by 14anu02
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ? =(+ ?id 1));added by Garima Singh(M.Tech-C.S) 4-dec-2013
; changed from '(viSeRya-saMKyA_viSeRaNa  =(+ ?id 2) =(+ ?id 1))' to '(viSeRya-saMKyA_viSeRaNa  ? =(+ ?id 1))' by 14anu02
;(or(id-cat =(+ ?id 1) <num> )(id-cat_coarse =(+ ?id 1) number)(id-word =(+ ?id 1) half|an|one|two|three|four|five|six|seven|eight|nine|ten|twenty|thirty|forty|fifty|sixty|seventy|eighty|ninety|hundred|thousand))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about3   "  ?id "  lagaBaga )" crlf))
)





;$$$Modified by 14aanu-ban-02(07-02-2015)
;###[COUNTER EXAMPLE]### She told us a killing story about her wedding day.###[COUNTER EXAMPLE]### (cambridge)(parser no. 12)
;###[COUNTER EXAMPLE]### उसने उपने विवाहोत्सव दिन के बारे में हमें  एक बेहद मजेदार किस्सा सुनाया.###[COUNTER EXAMPLE]### (manual)
;Modified by Meena(3.6.11);(not(id-cat_coarse =(+ ?id 1) verb|.. ..))
;Is John about?
;There's a lot of flu about.
(defrule about04
(declare (salience  100))
;(declare (salience 4200))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(- ?id 1) noun|PropN|adverb)	;commented by 14anu-ban-02(07-02-2015)
(id-root =(- ?id 1) flu )			;added by 14anu-ban-02(07-02-2015)
(not(id-cat_coarse =(+ ?id 1) verb|verbal_noun|determiner))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsapAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp     about04   "  ?id "  AsapAsa )" crlf))
)





;Salience changed by Meena(12.5.11)
(defrule about4
(declare (salience 0))
;(declare (salience 4200))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAro_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about4   "  ?id "  cAro_ora )" crlf))
)

;"about","Adv","1.cAro_ora"
;The boys were climbing about on the rocks.
;--"2.iXara_uXara"
;Boys were sitting about in the park.
;--"3.lagaBaga"
;She is about to finish her homework.
;--"4.bahuwa_ho_gayA"
;I've had about enough of your complaining.
;


;Modified by Meena(8.8.11)
;Added by Meena(12.5.11)
;Their belongings were flung about the room.
;I have been running about all morning trying to find you.
;She always leaves her clothes lying about on the floor. 
(defrule about5
(declare (salience 4100))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-about_saMbanXI  ?id1 ?saMb)(kriyA-kriyA_viSeRaNa ?id1 ?id))
(id-root ?id1 fling|throw|lie|run|jog|walk|move|stroll)
(id-cat_coarse =(+ ?id 1) ?cat)
(id-word =(+ ?id 1) ?wrd)
=>
(if (and (neq ?cat preposition)(neq ?wrd all)) then
    (assert (id-wsd_root_mng ?id meM_iXara-uXara))
    (retract ?mng)
    (if ?*debug_flag* then
        (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp      about5   "  ?id "  meM_iXara-uXara )" crlf))
    else
    (assert (id-wsd_root_mng ?id iXara-uXara))
    (retract ?mng)
    (if ?*debug_flag* then
        (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp     about5   "  ?id "  iXara-uXara )" crlf))
))



;Salience increased to 200 by Aditya and Hardik(23-06-13), IIT(BHU)
;Salience reduced by Meena(12.5.11)
;;We have very different ideas about disciplining children. 
(defrule about6
;(declare (salience 200))
(declare (salience 0));salience decreased to 0 by 14anu-ban-02(28.08.14)
;(declare (salience 4100))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about6   "  ?id "  ke_bAre_meM )" crlf))
)

;"about","Prep","1.ke_bAre_meM"
;I read a book about cricket.
;--"2.Asa_pAsa"
;I dropped the pen somewhere about here.
;

;$$$Modified by 14anu-ban-02(07-02-2015)
;###[COUNTER STATEMENT] There's a lot of flu about.[about04]
;###[COUNTER STATEMENT] वहाँ  आसपास बहुत फ्लू  है.[manual]
;@@@ Added by 14anu17
;Reduce the number of people smoking by about a third.
;लगबग एक तिहाई से धूम्रपान कर रहे लोगों की संख्या कम करें.
;धूम्रपान करने वाले लोगों की संख्या लगबग एक तिहाई से कम करें.[manual 14anu-ban-02][12-12-2014]
(defrule about11
(declare (salience 4600))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 third)	;added by 14anu-ban-02(07-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about11   "  ?id "  lagaBaga )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (20-10-2014)
;If you look around, you will come across many examples of rotation about an axis, a ceiling fan, a potter's wheel, a giant wheel in a fair, a merry-go-round and so on (Fig 7.3 (a) and (b)).[NCERT corpus]
;यदि आप अपने चारों ओर देखें तो आपको छत का पङ्खा, कुम्हार का चाक (चित्र 7.3(a) एवं (b)) , विशाल चक्री-झूला (जॉयन्ट व्हील), मेरी-गो-राउण्ड जैसे अनेक ऐसे उदाहरण मिल जायेंगे जहाँ किसी अक्ष के परितः घूर्णन हो रहा हो.[NCERT corpus]
;@@@ Added by 14anu-ban-02(26.08.14)
;The theorem may be stated as follows: The moment of inertia of a body about any axis is equal to the sum of the moment of inertia of the body about a parallel axis passing through its center of mass and the product of its mass and the square of the distance between the two parallel axes.
;प्रमेय का कथन इस प्रकार है: किसी पिण्ड का, किसी अक्ष के परितः जडत्व आघूर्ण, उस योग के बराबर है जो पिण्ड के द्रव्यमान केन्द्र से गुजरने वाली सामानान्तर अक्ष के परितः लिए गए जडत्व आघूर्ण और पिण्ड के द्रव्यमान तथा दोनों अक्षों के बीच की दूरी के वर्ग के गुणनफल को जोडने से प्राप्त होता है.

(defrule about12
(declare (salience 3000));salience reduced from 7000 to 3000 by 14anu-ban-01 on (20-10-2014)
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) body); commented by 14anu-ban-01 on (20-10-2014)
(viSeRya-about_saMbanXI  ?id2 ?id1);added by 14anu-ban-01 on (20-10-2014)
(id-root ?id1 axis);added by 14anu-ban-01 on (20-10-2014)
(id-cat_coarse ?id2 noun);added by 14anu-ban-01 on (20-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariwaH))
(assert (id-wsd_viBakwi ?id1 ke));added by 14anu-ban-01 on (20-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  about.clp 	about12   "  ?id1 " ke)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about12   "  ?id " pariwaH  )" crlf))
)

;@@@ Added by 14anu-ban-02 (30-08-2014)
;Thus a nucleus in an atom is as small in size as the tip of a sharp pin placed at the center of a sphere of radius about a metre long. 
;अतः किसी परमाणु में नाभिक आमाप में उतना ही छोटा है जितनी छोटी लगभग 1m व्यास के गोले के केन्द्र पर रखे गए तीक्ष्ण पिन की नोक होती है.
(defrule about13
(declare (salience 10))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?kri ?id1)
(id-word ?id1 metre|week)	;'week' is added in the list by 14anu-ban-02(09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about13   "  ?id "  lagaBaga )" crlf))
)

;@@@ Added by 14anu-ban-02 (12-11-2014)
;We shall, in the present chapter, consider rotational motion about a fixed axis only.[ncert]
;इस अध्याय में हम एक स्थिर अक्ष के परितः होने वाली घूर्णी गति का ही अध्ययन करेंगे.[ncert]
(defrule about14
(declare (salience 10))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ? ?id2)
(id-word ?id2 axis)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pariwaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about14   "  ?id "  ke_pariwaH )" crlf))
)

;Commented by 14anu-ban-02(09-02-2015)
;correct meaning is coming from about13.
;@@@ Added by 14anu17
;About a week to 10 days after the MMR some children become feverish.
;MMR के बाद लगभग  हफ्ते से १० दिन तक कुछ बच्चों को बुखार आया था.
;(defrule about15
;(declare (salience 4500))	;salience reduce by 14anu-ban-02(07-02-2015)
;(id-root ?id about)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adverb)
;(id-cat_coarse =(+ ?id 1) determiner)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id lagaBaga))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about15  "  ?id "  lagaBaga )" crlf))
;)

;$$$Modified by 14anu-ban-02(15-01-2015)
;@@@Added by 14anu18
;He is about to vomit.
;वह उल्टी करने वाला है . 
(defrule about16
(declare (salience 5000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
;(id-word =(+ ?id 1) to)	;commented by14anu-ban-02(15-01-2015)
(id-root ?id1 vomit)  		;added by14anu-ban-02(15-01-2015)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) vAlA))	;commented by14anu-ban-02(15-01-2015)
(assert (id-wsd_root_mng ?id vAlA))	;added by14anu-ban-02(15-01-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " about.clp	about16  " ?id "  " =(+ ?id 1) "  vAlA  )" crlf))	;commented by14anu-ban-02(15-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about16  "  ?id "  vAlA )" crlf))
)	;added by14anu-ban-02(15-01-2015)

;$$$Modified by 14anu-ban-02(15-01-2015)
;They are about to start this work.
;@@@ Added by 14anu01
;They are about to start this work. 
(defrule about012
(declare (salience 5000))	;salience increased from 4900 to 5000 by 14anu-ban-02(15-01-2015)
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)	;added by 14anu-ban-02(15-01-2015)
(id-root ?id1 start)		;added by 14anu-ban-02(15-01-2015)
(id-cat_coarse ?id adverb)
;(id-word =(+ ?id 1) to)	;commented by 14anu-ban-02(15-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about012   "  ?id "  aBI )" crlf))
)

;@@@Added by 14anu-ban-02(07-02-2015)
;She wore a shawl about her shoulders.[oald]
;उसने अपने कन्धों के चारो ओर शाल लपेटी.[manual]
(defrule about17
(declare (salience 1000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-about_saMbanXI  ?id1 ?id2)
(id-root ?id2 shoulder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_cAroM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about17   "  ?id "  ke_cAroM_ora )" crlf))
)

;@@@Added by 14an-ban-02(07-02-2015)
;We tried to raise eighty thousand pounds, but unfortunately we fall short by about ten thousand. [wordrefernce forum]
 ;हमने अस्सी हजार पाउन्ड ऊपर उठाने का प्रयास किया, परन्तु दुर्भाग्य से हम लगभग दस हजार तक ठिगना गिरते हैं . [manual]
(defrule about18
(declare (salience 1000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka  ?id1 ?id)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagaBaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about18   "  ?id "  lagaBaga )" crlf))
)

;@@@Added by 14anu-ban-02(24-03-2015)
;The electrons would be moving in orbits about the nucleus just as the planets do around the sun.[ncert 12_12]
;इलेक्ट्रॉन, नाभिक के चारों ओर कक्षा में चक्कर लगाते हैं, ठीक ऐसे ही जैसे सूर्य के चारों ओर ग्रह चक्कर लगाते हैं.[ncert]
(defrule about19
(declare (salience 1000))
(id-root ?id about)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-about_saMbanXI  ?id1 ?id2)
(id-root ?id2 nucleus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_cAroM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  about.clp 	about19   "  ?id "  ke_cAroM_ora )" crlf))
)


