
;@@@ Added by 14anu-ban-11 on (10-04-2015)
;Naples surpassed Rome as a centre/center of artistic production in the 18th century.(oald)
;नॆपल्ज 18th शताब्दी में  रोम को कलात्मक प्रस्तुतिकरण का केंद्र बना दिया .        [Manual] 
(defrule surpass2
(declare (salience 4901))
(id-root ?id surpass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 centre)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surpass.clp 	surpass2   "  ?id "  banA_xe)" crlf))
)

;"surpassing","Adj","1.SreRTa"
;There is a scenery of surpassing beauty.
(defrule surpass0
(declare (salience 5000))
(id-root ?id surpass)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id surpassing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SreRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  surpass.clp  	surpass0   "  ?id "  SreRTa )" crlf))
)

;----------------------------------- Default Rule ------------------------------------

;"surpass","V","1.Age nikala jZAnA"
;Geetika worked so hard that she surpassed Neha.
(defrule surpass1
(declare (salience 4900))
(id-root ?id surpass)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_nikala_jZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surpass.clp 	surpass1   "  ?id "  Age_nikala_jZA )" crlf))
)

