;@@@ Added by 14anu-ban-11 on (02-04-2015)
;A sober grey suit.(oald)
;एक सादा ग्रे सूट . (self)
(defrule sober4
(declare (salience 4901))
(id-root ?id sober)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-viSeRaNa  ?id2 ?id1)
(id-root ?id1 grey)
(id-root ?id2 suit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sober.clp 	sober4   "  ?id "  sAxA )" crlf))
)



;"sobering","Adj","1.gamBIrawA_kI_ora"
;The incident had a sobering effect on all.
(defrule sober0
(declare (salience 5000))
(id-root ?id sober)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id sobering )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gamBIrawA_kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sober.clp  	sober0   "  ?id "  gamBIrawA_kI_ora )" crlf))
)

;@@@ Added by 14anu-ban-11 on (09-02-2015)
;A sobering thought.(hinkhoj)
;सादा विचार. (self)
(defrule sober3
(declare (salience 5001))
(id-root ?id sober)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-subject ?id1 ?id)
(id-root ?id1 think)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sober.clp 	sober3   "  ?id "  sAxA )" crlf))
)

;------------------------ Default Rules ---------------------
(defrule sober1
(declare (salience 4900))
(id-root ?id sober)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwuliwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sober.clp 	sober1   "  ?id "  saMwuliwa )" crlf))
)

;"sober","Adj","1.amawwa"
;Were you completely sober when you said that?
(defrule sober2
(declare (salience 4800))
(id-root ?id sober)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amawwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sober.clp 	sober2   "  ?id "  amawwa )" crlf))
)

;"sober","Adj","1.amawwa"
;Were you completely sober when you said that?
;--"2.saMyamI/samaJaxAra" A boy of your age should be sober"
;
;
