
(defrule decrease0
(declare (salience 5000))
(id-root ?id decrease)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decrease.clp 	decrease0   "  ?id "  kamI )" crlf))
)

;"decrease","N","1.kamI"
;There was a decrease in the speed of train near the station.
;--"2.GatawI"
;A decrease was noted in the rate of crime in that area.
;



;Thus, as the excitation of hydrogen atom increases (that is as n increases) the value of minimum energy required to 
;free the electron from the excited atom decreases.
;इस प्रकार, हाइड्रोजन परमाणु की उत्तेजित अवस्था बढाने पर (अर्थात n के बढने पर) उत्तेजित परमाणु से इलेक्ट्रॉन को स्वतन्त्र करने के लिए आवश्यक न्यूनतम ऊर्जा घटती है. 

(defrule decrease1
(declare (salience 4900))
(id-root ?id decrease)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gata))	;Modified by Pramila(Banasthali University) on 19-10-2013
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decrease.clp 	decrease1   "  ?id "  Gata )" crlf))
)

;"decrease","V","1.GatanA_yA_GatAnA"
;The intensity of rain has decreased now.
;


;@@@ Added by 14anu-ban-04 (19-11-2014)
;Soil microorganisms also improve nutrient availability and decrease pathogen and pest activity through competition.[agriculture]
;प्रतिस्पर्धा के माध्यम से मिट्टी के सूक्ष्मजीव पोषक तत्वों की उपलब्धता को बढ़ाते है और रोगाणु और हानिकारक कीट गतिविधि को घटाते है . [manual]
(defrule decrease2
(declare (salience 4910))
(id-root ?id decrease)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 microorganism) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatA))
(assert (kriyA_id-object_viBakwi ?id ko))                    ;added by 14anu-ban-04 (21-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  decrease.clp      decrease2   "  ?id " ko )" crlf)                                 ;added by 14anu-ban-04 (21-11-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decrease.clp 	decrease2   "  ?id "  GatA )" crlf))
)

