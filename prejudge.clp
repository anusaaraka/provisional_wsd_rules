;@@@ Added by 14anu-ban-09 on (23-02-2015)
;They took care not to prejudge the issue. [oald]
;उन्होंने समस्या का पूर्वनिर्णय नहीं लेने का ख्याल रखा.	   [manual]

(defrule prejudge1
(declare (salience 10))
(id-root ?id prejudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 issue|situation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvanirNaya_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudge.clp 	prejudge1   "  ?id "  pUrvanirNaya_le )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-02-2015)
;Officials complain that some reporters have prejudged the outcome of the investigation. [merrian-webster]
;अधिकारी शिकायत करते हैं कि कुछ संवाददाता जाँच के परिणाम का पूर्वनिर्णय निकाल चुके हैं .    [manual]

(defrule prejudge2
(declare (salience 10))
(id-root ?id prejudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 outcome)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvanirNaya_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudge.clp 	prejudge2   "  ?id "  pUrvanirNaya_nikAla )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-09 on (23-02-2015)
;NOTE:- Example sentence need to be added.

(defrule prejudge0
(declare (salience 0))
(id-root ?id prejudge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvanirNaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudge.clp         prejudge0   "  ?id "  pUrvanirNaya )" crlf))
)

