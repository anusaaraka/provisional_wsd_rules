;$$$ Modified by 14anu26 -- Added sentence and translation by 14anu26
;The taste of this ice-cream is good.
;इस मलाई बरफ का स्वाद अच्छा है . 
(defrule taste0
;(declare (salience 5000))
(declare (salience 0)) ;salience changed by 14anu26
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste0   "  ?id "  svAxa )" crlf))
)

(defrule taste1
(declare (salience 4900))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste1   "  ?id "  svAxa_le )" crlf))
)

;"taste","VTI","1.svAxa_le"
;Taste the curry .
;
;

;@@@ Added by 14anu26  [24-06-14]
;These styles can be adapted to suit individual tastes.
;ये शैलियाँ अलग स्वादानुसार सुविधाजनक होने के लिए ढली जा सकतीं हैं .
(defrule taste2
(declare (salience 5500))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id  ?id1)
(id-root ?id1 individual)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxAnusAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste2   "  ?id "  svAxAnusAra )" crlf))
)

;$$$ Modified by 14anu-ban-07 (16-12-2014)
;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;He tasted the life of the very rich.(Source: thefreedictionary.com )
;उसने अत्यन्त अमीर जीवन का अनुभव किया. 
(defrule taste02
(declare (salience 5100))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 life|success|defeat)   ;more can words can be added here
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava_kara)) ;meaning changed from anuBava to anuBava_kara by 14anu-ban-07 (16-12-2014)
(assert  (id-wsd_viBakwi   ?id1  kA))  ;added by 14anu-ban-07 (16-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  taste.clp      taste02   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste02   "  ?id "  anuBava_kara )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;A room furnished with superb taste. (Source: thefreedictionary.com )
;कमरा शानदार शैली में सुसज्जित है.
(defrule taste3
(declare (salience 5100))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SElI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste3   "  ?id "  SElI )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, and 14anu24 MNNIT Allahabad on 1/7/2014*****
;He has a taste for adventure. (Source: thefreedictionary.com )
;उसको साहसिक कार्य पसन्द है .
;A source close to Krishna says "he has a taste for the elegant things in life" . (ex. and translation by 14anu24 )
;कृष्ण के नजदीकी सूत्र का कहना है कि  उन्हें  नफीस चीजें पसंद हैं .
(defrule taste4
(declare (salience 5200))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasaMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste4   "  ?id "  pasaMxa )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;He has a good taste of art. (Source: thefreedictionary.com )
;उसे कला की अच्छी समझ है 
(defrule taste5
(declare (salience 5200))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 music|art|sport|literature|theatre)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste5   "  ?id "  samaJa )" crlf))
)


;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;This was my first taste of live theatre. (Source: http://www.oxfordlearnersdictionaries.com/definition/english/taste_1)
;यह जीवित रन्गमन्च का मेरा प्रथम अनुभव था 
(defrule taste6
(declare (salience 5200))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 first|last)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste6   "  ?id "  anuBava )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 1/7/2014*****
;He has very good taste in music. (Source: http://www.oxfordlearnersdictionaries.com/definition/english/taste_1)
;उसे सङ्गीत कि बहुत अच्छी समझ है . 
(defrule taste7
(declare (salience 5200))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 music|art|sport|literature|theatre|acting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste7   "  ?id "  samaJa )" crlf))
)

;@@@Added by 14anu-ban-07,(27-02-2015)
;I had a taste of office work during the summer and that was quite enough.(cambridge)
;मेरे पास ग्रीष्म के दौरान कार्यालय कार्य का अनुभव था और वह  पर्याप्त था . (manual)
(defrule taste8
(declare (salience 5300))
(id-root ?id taste)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste8   "  ?id "  anuBava )" crlf))
)

;;Added by 14anu-ban-07,(27-02-2015)
;;I'm afraid I have expensive tastes.(cambridge)
;;मुझ खेद है (कहते हुए ) कि  मेरी  पसन्द  महंगी है  . (manual)
;(defrule taste9
;;(declare (salience 5400))
;(id-root ?id taste)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id ?id1)
;(id-root ?id1 expensive)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id pasaMxa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  taste.clp 	taste9   "  ?id "  pasaMxa )" crlf))
;)
