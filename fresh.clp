
;@@@ Added by 14anu04 on 26-June-2014
;She had a fresh complexion.
;उसका रंग गोरा था . 
(defrule fresh_tmp4
(declare (salience 5100))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) complexion|skin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gorA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh_tmp4   "  ?id "  gorA )" crlf))
)


;@@@ Added by 14anu04 on 26-June-2014
;He is a fresher in the college.
;वह कालेज में नया है . 
(defrule fresh_tmp
(declare (salience 5100))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id fresher)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh_tmp   "  ?id "  nayA )" crlf))
)

;@@@ Added by 14anu04 on 26-June-2014
;We are fresh out of milk.
;हमारे पास दूध अभी खत्म हुअा है. 
(defrule fresh_tmp2
(declare (salience 5100))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-out_of_saMbanXI  ?id ?)
(id-word ?id1 out)
(id-word ?id2 of)
(test (=(+ ?id 1) ?id1))
(test (=(+ ?id 2) ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 aBI_Kawma_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fresh.clp 	fresh_tmp2  "  ?id "  " ?id1 ?id2 " aBI_Kawma_ho )" crlf))
)

;@@@ Added by 14anu04 on 26-June-2014
;The employees are fresh from college.
;कर्मचारी कालेज से नए हैं. 
(defrule fresh_tmp3
(declare (salience 5100))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nae))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh_tmp3   "  ?id "  nae )" crlf))
)

;@@@ Added by 14anu-ban-01 on 2-08-14.
;91973:Even as he was writing this novel and probing the spiritual hollowness of mere success , he was himself getting restless for fresh fields to conquer .[Karan Singla]
;रवीन्द्रनाथ जब यह उपन्यास लिख रहे थे और सफलता में आध्यात्मिक रिक्तता ढूंढ रहे थे तब वे खुद ही नए क्षेत्र जीतने के लिए बैचेन हो रहे थे .[Karan Singla]
;104707:Yet enough is never enough ; every capitulation inspires a fresh abduction , preferably one with a political gloss .[Karan Singla]
;हर बार घुटने टेकने पर नए , खासकर राजनैतिक चरित्र वाले अपहरण को प्रेरणा मिलती है .[Karan Singla]
;3570:When the Governor dissolved the Council and ordered fresh elections , the Congress party returned with added strength .[Karan Singla]
;गवर्नर ने जब कौंसिल भंग करके नये चुनाव घेषित किये तो कांग्रेस अधिक शक़्तिशाली होकर असेंबली में लौटी थी .[Karan Singla]
;4831:look at the situation through fresh eyes.
;और एक नये नज़रिये से स्थितियों पर गौर करें.

(defrule fresh2
(declare (salience 4900))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 field|area|abduction|lease|code|stimulus|containers|steel|section|capacity)
(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh2   "  ?id "  nayA )" crlf))
)

;@@@ Added by 14anu-ban-01 on 2-08-14.
;290535:Prevent rubbing eyes to keep the eyes fresh .[Karan Singla]
;आँखों को तरोताजा बनाए रखने के लिए आँखों को मसलने से बचें ।[Karan Singla]
;289684:Under this new skin emerges very soon , whereas before this when the ablative lasers were used , then it took time for the fresh skin to emerge .[Karan Singla]
;इसके अंतर्गत जल्द ही तरोताजा त्वचा निकलती है , जबकि इससे पहले जब अब्लेटिव लेजर्स का इस्तेमाल किया जाता था , तब त्वचा के तरोताजा निकलने में वक्‍त लगता था ।[Karan Singla]

(defrule fresh3
(declare (salience 4900))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(or (viSeRya-viSeRaNa ?id1 ?id)(kriyA-object ? ?id1))
(id-root ?id1 skin|eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warowAjZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh3   "  ?id "  warowAjZA )" crlf))
)


;@@@ Added by 14anu-ban-01 on 2-08-14
;35016:There were new worlds to explore , fresh minds to meet .[Karan Singla]
;ऐसे भी कई नए विश्व थे - जिनका अन्वेषण करना था - और अभिनव मानस थे , जिनसे मिलना था .[Karan Singla]
(defrule fresh4
(declare (salience 4900))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 mind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh4   "  ?id "  aBinava )" crlf))
)

;@@@ Added by 14anu-ban-01 on 2-08-14
;4831:look at the situation through fresh eyes.
;और एक नये नज़रिये से स्थितियों पर गौर करें.
(defrule fresh5
(declare (salience 4900))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh5   "  ?id "  nayA )" crlf))
)

;----------------------------------------Default rules------------------------------------------------------------------------
(defrule fresh0
(declare (salience 900));salience reduced from 5000 to 900 by 14anu-ban-01.
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh0   "  ?id "  wAjA )" crlf))
)

(defrule fresh1
(declare (salience 4900))
(id-root ?id fresh)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fresh.clp 	fresh1   "  ?id "  wAjA )" crlf))
)

;"fresh","Adj","1.wAjA"
;Have only fresh bread for breakfast.

