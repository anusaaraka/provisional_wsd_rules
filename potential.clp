
(defrule potential0
(declare (salience 5000))
(id-root ?id potential)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmarWya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  potential.clp 	potential0   "  ?id "  sAmarWya )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning from 'aXika_SakwiSAlI_Kaware' to 'SakwiSAlI'. 
;Here we have to point out that another front of cultural tension ( perhaps with more potential danger than the linguistic one )
; is where the liberal and conservative elements are facing each other .
;@@@ Added by 14anu11
;यहां हमें यह बताना पडेगा कि सांस्कृतिक तनाव का एक दूसरा मोर्चा ( शायद भाषा की अपेक्षा अधिक शक्तिशाली खतरे के साथ ) वह है जहां उदारवादी और रूढिवादी तत्व एक दूसरे के सामने आते हैं .
(defrule potential2
(declare (salience 6000))
(id-root ?id potential)
;(id-word =(+ ?id 1) danger) 	;commented by 14anu-ban-09 on (01-01-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 danger) 		;modified ?id2 as ?id1 by 14anu-ban-09 on (01-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SakwiSAlI)) ;Added by 14anu-ban-09 on (01-01-2015)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 aXika_SakwiSAlI_Kaware)) ;commented by 14anu-ban-09 on (01-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  potential.clp 	potential2   "  ?id "  SakwiSAlI )" crlf)) ;Added by 14anu-ban-09 on (01-01-2015)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  potential.clp 	potential2   "  ?id aXika_SakwiSAlI_Kaware ")" crlf)) ;commented by 14anu-ban-09 on (01-01-2015)
)


;@@@ Added by 14anu-ban-09 on (03-03-2015)
;The sum of kinetic and potential energies is thus conserved. [NCERT CORPUS]
;इस प्रकार वस्तु की स्थितिज ऊर्जा तथा गतिज ऊर्जा का योग संरक्षित रहता है.		[NCERT CORPUS]
(defrule potential3
(declare (salience 5000))
(id-root ?id potential)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 energy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwija))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  potential.clp 	potential3   "  ?id "  sWiwija )" crlf))
)

;"potential","Adj","1.sAmarWya"
;Students having potential should be encouraged to work harder.
;

;$$$ Modified by 14anu-ban-09 on (27-03-2015)  ;Changed meaning from 'anwarnihiwa_Sakwi' to 'sAmaWrya'. 
;Students having potential should be encouraged to work harder. [same clp file]
;विद्यार्थी जिनके पास सामर्थ्य हैं उनहे कठोर परिश्रम करने के लिए प्रोत्साहित किया जाना चाहिए .	[Manual] 

(defrule potential1
(declare (salience 4900))
(id-root ?id potential)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmarWya))	;changed meaning from 'anwarnihiwa_Sakwi' to 'sAmarWya' by 14anu-ban-09 on (27-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  potential.clp 	potential1   "  ?id "  sAmarWyas )" crlf)	;changed meaning from 'anwarnihiwa_Sakwi' to 'sAmarWya' by 14anu-ban-09 on (27-03-2015)
)
)

;"potential","N","1.anwarnihiwa_Sakwi"
;She recognised the potential for error in the method being used.
;
