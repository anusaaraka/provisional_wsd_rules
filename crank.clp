;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu06 Vivek Agarwal,MNNIT Allahabad on 3/6/2014
;Default Rule.....
(defrule crank0
(declare (salience 4700))
(id-root ?id crank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XurI_kI_moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crank.clp 	crank0   "  ?id " XurI_kI_moda )" crlf))
)

;$$$ Modified by 14anu-ban-03 (15-12-2014)
;@@@ Added by 14anu06 Vivek Agarwal BTech in CSE, MNNIT Allahabd 
;vivek17.agarwal@gmail.com		
;Please crank up the volume.
;कृपया आवाज़ बढाना

(defrule crank1
(declare (salience 5000))
(id-root ?id crank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1 )
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDAnA ))  ;meaning changed from 'bDAnA' to 'baDAnA' by 14anu-ban-03 (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crank.clp     crank1   "  ?id "  baDAnA )" crlf))
)
