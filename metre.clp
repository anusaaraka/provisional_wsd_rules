;$$$ Modified by 14anu-ban-08 (20-02-2015)   ;modify 'root_mng'
;The city is at an elevation of 2000 metres.     [olad]
;शहर 2000 मीटर की ऊँचाई पर है .                      [self]
;@@@ Added by 14anu13   on 24-06-2014
;Temple is situated at distance of 100 metres from my home.
;मेरे घर से मंदिर 100 मीटर की दूरी पर स्थित है |
(defrule metre0
(declare (salience 5000))
(id-root ?id metre)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id   mItara))  ;modify 'root_mng' to 'word_mng' by 14anu-ban-08 (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  metre.clp 	metre0    "  ?id "   mItara )" crlf))                   ;modify 'root_mng' to 'word_mng' by 14anu-ban-08 (20-02-2015)
)

;@@@ Added by 14anu13   on 24-06-2014
; Besides , the scientific books of the Hindus are composed in various favourite metres , by which they intend , considering that the books soon become corrupted by additions and omissions , to preserve them exactly as they are , in order to facilitate their being learned by heart , because they consider as canonical only , that which is known by heart , not that which exists in writing . 
;साथ ही , हिन्दुओं की शास्त्र - संबंधी पुस्तकों की रचना विभिन्न छंदों में की गई है जिसका अभिप्राय यह रहा होगा कि छंदोबद्ध रचना कंठस्थ करने में आसानी होती है , अन्यथा पुस्तकों में लोग कहीं प्रक्षेप और कहीं विलोपन करके उसे विकृत कर देते हैं . साथ ही , वे उसी को प्रामाणिक मानते हैं जो कंठस्थ हो सके , जो कुछ लिखित रूप में विद्यमान है उसका उनकी दृष्टि में उतना महत्व नहीं .

(defrule metre1
(declare (salience 5500))
(id-root ?id metre)
?mng <-(meaning_to_be_decided ?id)
(or(and(id-root ?id2 favourite|various)(viSeRya-viSeRaNa ?id  ?id2))
   (and(id-root ?id1 compose)(kriyA-in_saMbanXI  ?id1  ?id)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   Canxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  metre.clp 	metre1    "  ?id "   Canxa )" crlf))
)
