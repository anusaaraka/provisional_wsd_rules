;$$$ Modified by 14anu-ban-06 (31-01-2015)
;### [COUNTER EXAMPLE] ### There was an intense dislike of fascism and Nazism and no desire to see them win .(parallel corpus)
;### [COUNTER EXAMPLE] ### वहां फासिज़्म और नाजिज़्म के लिए बेहद नफरत थी और उनकी जीत के लिए कोई तमऩ्ना नहीं थी .(parallel corpus)
;$$$ MODIFIED BY GOURAV SAHNI (MNNIT ALLAHABAD) ON 13/06/2014. 
;Changed meaning from 'wIkRNa' to 'wIvra'
;Try to avoid intense light bulbs . 
;तीव्र प्रकाश बल्बों को टालने के लिए प्रयास कीजिए . 
(defrule intense1
(declare (salience 5000))
(id-root ?id intense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id) ; added by GOURAV SAHNI (MNNIT ALLAHABAD) ON 13/06/2014.
(id-root ?id1 bulb);added by 14anu-ban-06 (31-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIvra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intense.clp 	intense1   "  ?id "  wIvra )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-08-14)
;There is a hearsay regarding this that to get rid of killing of the same gotra the five Pandavas had done an intense penance for twelve years here and went for the ascension of the heaven from here itself . (tourism corpus)
;इसके  विषय  में  जनश्रुति  है  कि  गोत्र  हत्या  के  पाप  से  छुटकारा  पाने  के  लिए  यहाँ  पाँच  भाई  पाण्डवों  ने  बारह  वर्ष  शिव  की  घोर  तपस्या  की  तथा  यहीं  से  स्वर्गारोहण  को  चले  गए  थे  ।
(defrule intense2
(declare (salience 5100))
(id-root ?id intense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 penance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intense.clp 	intense2   "  ?id "  Gora )" crlf))
)

;@@@ Added by 14anu-ban-06 (31-01-2015)
;I have come from a place of intense darkness .(parallel corpus)
;मै गहन अँधेरे की दुनिया से आया हूँ ।(parallel corpus)
;This dualism in him , of intense subjectivity and a manly concern with the objective world , was a basic characteristic of his personality , from his childhood to death .(parallel corpus)
;जीवन के आरंभ से लेकर अंत तक उनके अंदर की विविधता - गहन आत्मनिष्ठा और वस्तु - जगत के प्रति पुरुषोचित प्रतिबद्धता - उनके व्यक्तित्व की आधारभूत विशिष्टता रही है .(parallel corpus)
(defrule intense3
(declare (salience 5100))
(id-root ?id intense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 darkness|subjectivity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intense.clp 	intense3   "  ?id "  gahana )" crlf))
)

;@@@ Added by 14anu-ban-06 (31-01-2015)
;There was an intense dislike of fascism and Nazism and no desire to see them win .(parallel corpus)
;वहां फासिज़्म और नाजिज़्म के लिए अत्यधिक नफरत थी और उनकी जीत के लिए कोई तमऩ्ना नहीं थी .(parallel corpus)
;Stories like the above were perhaps later creations to show the intense love Basava had for his men .
;(parallel corpus)
;इस तरह की कहानियां शायद बाद में जोड़ी गई हैं . इनका मकसद यह दिखलाना है कि बसव को अपने लोगों से अत्यधिक प़्यार था .(parallel corpus)
(defrule intense4
(declare (salience 5100))
(id-root ?id intense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 dislike|love|pleasure)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AwyaXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intense.clp 	intense4   "  ?id "  AwyaXika )" crlf))
)

;---------------------- Default Rules -------------------

(defrule intense0
(declare (salience 5000))
(id-root ?id intense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMBIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intense.clp 	intense0   "  ?id "  gaMBIra )" crlf))
)

;"intense","Adj","1.wIkRNa/uwkata/awimAwra"
;Tom has intense pain in his ankle.
;--"2.weja"
;Don't go out in intense heat.
;--"3.gamBIra"
;He gets very intense when he talks about politics.
;
;

