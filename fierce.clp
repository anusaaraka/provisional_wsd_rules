;@@@ Added by 14anu-ban-05 on (28-01-2015)
;Two fierce eyes glared at them.[OALD]
;दो भयंकर आँखें उनको घूरीं .[manual]
(defrule fierce0
(declare (salience 100))
(id-root ?id fierce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BayaMkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fierce.clp  	fierce0   "  ?id "  BayaMkara )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (28-01-2015)
;In the history of this tiny country of Central Europe many fierce attacks are registered . From 1795 to 1918 the name of Poland kept under going change on the world map .[tourism]
;maXya yUropa ke isa nanheM se xeSa ke iwihAsa meM aneka BIRaNa AkramaNa xarja hEM . 1795 se 1918 waka viSva ke mAnaciwra meM polEMda kA nAma banawA-bigadawA hI rahA .[tourism]
(defrule fierce1
(declare (salience 101))
(id-root ?id fierce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 attack|torture|wind)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BIRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fierce.clp  	fierce1   "  ?id "  BIRaNa )" crlf))
) 
