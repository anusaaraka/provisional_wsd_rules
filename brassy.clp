;@@@Added by 14anu-ban-02(07-04-2015)
;Her brassy voice rang out from the next room.[cald]
;अगले कमरे से उसकी कर्कश आवाज  गूंजी . [self]
(defrule brassy1 
(declare (salience 100)) 
(id-root ?id brassy) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 voice)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id karkaSa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brassy.clp  brassy1  "  ?id "  karkaSa )" crlf)) 
)


;@@@Added by 14anu-ban-02(08-04-2015)
;The brassy blonde behind the bar.[oald]
;शराबघर के पीछे बेहया गोरे शरीर व नीली आँख वाली स्त्री . [self]
(defrule brassy2 
(declare (salience 100)) 
(id-root ?id brassy) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 blonde)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id behayA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brassy.clp  brassy2  "  ?id "  behayA )" crlf)) 
) 


;-----------------------default_rules---------------------------------

;@@@Added by 14anu-ban-02(07-04-2015)
;Sentence: A brassy yellow.[cald]
;Translation: पीतल के रङ्ग का पीला.[self]
(defrule brassy0 
(declare (salience 0)) 
(id-root ?id brassy) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pIwala_ke_rafga_kA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brassy.clp  brassy0  "  ?id "  pIwala_ke_rafga_kA )" crlf)) 
) 
