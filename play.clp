

;@@@ Added by 14anu04 on 24-June-2014
;They were out played by their opponents. 
;उन्हें उनके प्रतिपक्षियों ने हरा दिया था. 
(defrule play_tmp
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(test (=(- ?id 1) ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 harA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play_tmp  "  ?id "  " ?id1 "  harA_xe  )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
(defrule play00
(declare (salience 4900))  
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 noun)
(id-cat_coarse ?id2 noun)
(id-word ?id1 film|hero|actor|actress|theatre|character)
(and(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?)(kriyA-in_saMbanXI  ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " play.clp play00 " ?id "  aBinaya_kara )" crlf)) 
)

;Commented by Shirisha Manju (17-03-14) :conflict sentence   'On Saturdays I play games at school.'
;(defrule play0
;(declare (salience 4000))  ;Salience changed from 5000 to 4000 by Roja 07-11-13. Ex: On Saturdays I play games at school. (Note: This rule need to be improved.) 
;(id-root ?id play)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 at)
;(kriyA-at_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(kriyA-object ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id aBinaya_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " play.clp play0 " ?id "  aBinaya_kara )" crlf)) 
;)

;$$$ Modified by 14anu-ban-09 on (21-03-2015)
;NOTE:- Parser problem because parser is treating at as "preposition" but for upasarga we want "particle". Otherwise the rule is ok!  And no parser is taking 'at' as particle.
;He just plays at being a writer.
;vaha leKaka hone kA aBinaya karawA hE. 
;As kids we used to play at being doctors && patients. [Same CLP file]	;added by 14anu-ban-09 on (21-03-2015)
;bacapana meM hama dAktara-marIjZa kA xiKAvA/aBinaya kara rahe We. [Manual]	;added by 14anu-ban-09 on (21-03-2015)
(defrule play1
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 at)
(kriyA-upasarga ?id ?id1)
(id-word =(+ ?id 2) being)	;added by 14anu-ban-09 on (21-03-2015)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aBinaya_kara))	;commented by 14anu-ban-09 on (21-03-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 (+ ?id 2) aBinaya_kara))	;added by 14anu-ban-09 on (21-03-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play1  "  ?id "  " ?id1 "  aBinaya_kara  )" crlf)	;commented by 14anu-ban-09 on (21-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play1  "  ?id "  " ?id1 "  " (+ ?id 2) " aBinaya_kara  )" crlf)	;added by 14anu-ban-09 on (21-03-2015)
)
)


;As kids we used to play at being doctors && patients.
;bacapana meM hama dAktara-marIjZa kA aBinaya karawe We
(defrule play2
(declare (salience 4800))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)  
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nIcA_xiKA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play2  "  ?id "  " ?id1 "  nIcA_xiKA )" crlf))		
)

;You cannot play me down like this!
;wuma muJe isa waraha nIcA nahIM xiKA sakawe

;$$$ Modified by 14anu-ban-09 on (21-03-2015)
;Change meaning from "lABa" to "lABa_uTA"
;Advertisements often play on people's fears. [OALD] ;added by 14anu-ban-09 [NOTE- Parser problem. Run on parser no. 2]
;vigyApana logo ke dara/BAvanAo kA PAyaxA uTAwe hE. [Own Manual]
;She always play on his feelings. [Same CLP file] [NOTE- Parser probelm. Run on parser no. 4 or 5]
;vaha hameMSAM usakI BAvanAoM kA PAyaxA uTAwI hE.
(defrule play3
(declare (salience 4700))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lABa_uTA))	;changed meaning from 'lABa' to 'lABa_uTA' by 14anu-ban-09 on (21-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play3  "  ?id "  " ?id1 "  lABa_uTA  )" crlf)	;changed meaning from 'lABa' to 'lABa_uTA' by 14anu-ban-09 on (21-03-2015)
)
)

;She always plays on his feelings. ;corrected from 'play' to 'plays' by Bhagyashri Kulkarni (8.11.2016)
;vaha hameMSAM usakI BAvanAoM kA PAyaxA uTAwI hE
(defrule play4
(declare (salience 4600))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cApalUsI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play4  "  ?id "  " ?id1 "  cApalUsI_kara  )" crlf))
)

;She is always playing up to her boss.
;vaha hameMSAM apane bAsa kI cApalUsI karawI hE

;$$$ Modified by 14anu-ban-09 on (21-03-2015)
;NOTE-Already send the mail for entry modification for 'parents'. After modification,comment '(id-root ?id2 parent)' and uncomment '(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))' .
;He is playing up towards his parents.
;vaha apane mAwA-piwA kI  avajFA  karawA hE.
(defrule play5
(declare (salience 4700))	;Salience increased '4500' to '4700' by 14anu-ban-09 on (21-03-2015)
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-towards_saMbanXI  ?id ?id2) 	;added by 14anu-ban-09 on (21-03-2015)
(id-root ?id2 parent) 			;added by 14anu-ban-09 on (21-03-2015)
;(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))     ;added by 14anu-ban-09 on (21-03-2015)		
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 avajFA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play5  "  ?id "  " ?id1 "  avajFA_kara  )" crlf))
)


(defrule play6
(declare (salience 4400))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 jolt)
(viSeRya-by_saMbanXI ?id1 ?id) ;Replaced viSeRya-by_viSeRaNa as viSeRya-by_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play6   "  ?id "  nAtaka )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (27-03-2015)
;You can see the play this evening. [COCA]	;added by 14anu-ban-09 on (27-03-2015)
;esa SAma meM wuma yaha nAtaka xeKa sakawe ho. [Manual]	;added by 14anu-ban-09 on (27-03-2015)
(defrule play7
(declare (salience 4300))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 see)	;changed '(id-word)' to '(id-root)' by 14anu-ban-09 on (27-03-2015)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play7   "  ?id "  nAtaka )" crlf))
)

(defrule play8
(declare (salience 4200))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play8   "  ?id "  Kela )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
(defrule play9
(declare (salience 4100))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 trick)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SarArawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play9  "  ?id "  " ?id1 "  SarArawa_kara )" crlf))
)


;$$$ Modified by 14anu-ban-09 on 02-09-2014
;Changed meaning from "niBAI" to "niBA"
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Scientists also believe that magnetic effects played an important role during the contraction.
;वैज्ञानिकों का यह भी विश्वास है कि सिकुड़न के दौरान चुंबकीय प्रभावों ने भी महत्वपूर्ण भूमिका निभायी।
(defrule play10
(declare (salience 4000))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 role|responsibility|part)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play10   "  ?id "  niBA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Aditya saw a flock of sheep and ran off to play with them.  [Gyannidhi]
;आदित्य ने भेड़ों का झ़ुंड देखा तो साथ खेलने के लिये उनके बीच भागा।
(defrule play11
(declare (salience 3900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(id-cat_coarse ?id1 preposition) 	;commened by 14anu-ban-09 on (01-01-2015)
(kriyA-with_saMbanXI  ?id ?id1) 	;added '?id1' by 14anu-ban-09 on (01-01-2015)
(id-cat_coarse ?id1 pronoun) 		;Added by 14anu-ban-09 on (01-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play11  "  ?id "  " ?id1 "  Kela )" crlf))
)


;particle_with_- && category=verb	soca	100
;PP_null_with && category=verb	soca	100
;Example is needed. Default sense is still KelanA as in
;The children played with the toys.
(defrule play12
(declare (salience 3800))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play12   "  ?id "  Kela )" crlf))
)

;default_sense && category=noun	Kela	0
;"play","N","1.Kela"
;Our aim is learn through play.
;--"2.nAtaka"
;That radio play was very good.
;--"3.guMjAiSa"
;The cliff hanger got a play on the rope && climbed up.
;--"4.gawiviXi"
;New budget boons changed the market plays.
;
;


;$$$ Modified by Sonam Gupta MTech IT Banasthali 25-1-2014
;Added by sheetal(25-02-10)
;The playing of the piano really bothers John .
(defrule play13
(declare (salience 4250))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id playing|play)
(or (kqxanwa-of_saMbanXI  ?id ?id1)(kriyA-of_saMbanXI ?id ?id1)(kriyA-object ?id ?id1)(viSeRya-of_saMbanXI ?id ?id1));Modified by Roja (09-08-10)
(id-root ?id1 piano|composition|music)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp      play13   "  ?id "  baja )" crlf))
)
;kriyA-of_saMbanXI added by Manju (16-03-11)


;$$$ Modified by Bhagyashri Kulkarni (8-11-2016)
;Can you play sitar? (rapidex)
;क्या तुम सितार बजा सकती हो ?
;$$$ Modified by 14anu-ban-09 on (23-03-2015)
;He played a tune on his harmonica. 	[oald]	;added by 14anu-ban-09 on (23-03-2015)
;उसने माउथ ऑर्गन पर एक धुन बजाई.		[Manual] 	;added by 14anu-ban-09 on (23-03-2015)
;Lord Krishna plays pipe very well. [Pipe CLP file]
;BagavAna kqaRNa bahuwa acCI bAsurI bajAwe hE. [Own Manual] 
;$$$ Modified by Madhavi Banaras Hindu University 08-07-2014
;Added 'recording|song|flute' to the list
;@@@ Added by Sonam Gupta MTech IT Banasthali 28-1-2014
;She played the piano with a light touch. [OALD]
;उसने हल्के स्पर्श से पियानो बजाया .
(defrule play14
(declare (salience 5550))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 piano|composition|music|recording|song|flute|tune|piece|CD|pipe|sitar)	;added 'tune|piece' by 14anu-ban-09 on (23-03-2015)	;added 'CD|pipe' by 14anu-ban-09 on (27-03-2015) ;Added 'sitar' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp      play14   "  ?id "  bajA )" crlf))
)

;@@@ Added by Madhavi Banaras Hindu University 08-07-2014 (WSD participant)
;Play the movie.
;calaciwra calAo.
(defrule play15
(declare (salience 7700))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 movie|radio)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp      play15   "  ?id "  calA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (21-03-2015)          ;Corrected 'bajawe' to 'baja'.
;In the distance a band was playing. [oald]	;added by 14anu-ban-09 on (23-03-2015)
;दूर में, बैंड बज रहा था . 		           [Manual]	;added by 14anu-ban-09 on (23-03-2015)
;@@@ Added by 14anu-ban-07 Soshya Joshi MTech IT Banasthali 11-07-2014
;On the other side also slogans are raised , songs are played and the soldiers of Pakistani army keep busy in the formalities .(PARALLEL CORPORA)
;उधर भी नारेबाजी होती है , गाने बजते हैं तथा पाकिस्तानी सेना के जवान कवायद में लगे रहते हैं ।
;uaGara BI nArebAjI howI hE, gane bajawe hE waWA pakiswAnI senA ke javAna niyama pAlana meM vyaswa rahawe hE.
(defrule play16
(declare (salience 6000))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 song|music|piano|composition|band) ;added 'band' by 14anu-ban-09 on (23-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp      play16   "  ?id "  baja )" crlf))	;changed 'bajawe' to 'baja' by 14anu-ban-09 on (21-03-2015)
)
 
;@@@ Added by 14anu-ban-09 on (15-10-2014)
;In rotation (about a fixed axis), the moment of inertia plays a similar role as mass does in linear motion. [NCERT CORPUS]
;(sWira akRa ke pariwaH) GUrNana meM jadawva AGUrNa vahI BUmikA axA karawA hE jo reKIya gawi meM xravyamAna. [NCERT CORPUS]

(defrule play17
(declare (salience 3800))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 moment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play17   "  ?id "  axA_kara )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;महाराष्ट्र के लोकगीतों - खासकर तमाशा और पौवाडा में तुनतुने की संगीतात्मक क्षमता का सही रूप देखने को मिलता है .
;A very common musical situation in which the tuntune is found is the tarnasha play and povada ballad singing of Maharashtra
;@@@ Added by 14anu11
(defrule play18
(declare (salience 4200))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-det_viSeRaNa  ?id ?id1) ;commented by 14anu-ban-09 on (01-01-2015)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wmASA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play18  "  ?id "   wmASA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-03-2015)
;The kids have been playing up all day. [OALD]
;baccoM ne pUre xina se pareSAna kara xiyA hE. [Own Manual]
;बच्चे पूरे दिन परेशान करते रहे हैं .  [Anusaaraka output]

(defrule play19
(declare (salience 4700))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up) 
(kriyA-subject ?id ?id2)
(id-root ?id2 kid)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pareSAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play19  "  ?id "  " ?id1 "  pareSAna_kara  )" crlf))
)

 
;@@@ Added by 14anu-ban-09 on (21-03-2015)
;NOTE:- Parser problem because parser is treating at as "preposition" but for upasarga we want "particle". Otherwise the rule is ok! And no parser is taking 'at' as particle.
;The children were playing at Batman and Robin. [Cambridge Advanced Learner's Dictionary]
;bacce bEtamEna Ora robina kA aBinaya kara rahe We. [Own Manual]
(defrule play20
(declare (salience 5000))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 at) 
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aBinaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play20  "  ?id "  " ?id1 "  aBinaya_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-03-2015)
;Out of sheer envy, she would always play down her sister's accomplishments. 	[merrian-webster]
;पूर्णतया ईर्ष्या के बहाने , वह हमेशा उसकी बहन की योग्यता को कम महत्तव देगी . 	[Manual]
;If you play down your part in the project, you won't get the credit you deserve.	[merrian-webster]
;यदि आप परियोजना में आपके हिस्से को कम महत्तव देते हैं, तो आप वह श्रेय प्राप्त नहीं करेंगे जिसे आप योग्य हैं .	[Manual] 

(defrule play21
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 down) 
(kriyA-object ?id ?id2)
(id-root ?id2 part|accomplishment)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kama_mahawwava_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play21  "  ?id "  " ?id1 "  kama_mahawwava_xe  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-03-2015)
;This guitar is a delight to play. 	[oald]
;इस गिटार को बजाने में एक खुशी है . 	[Manual]

(defrule play22
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(id-root ?id2  guitar|triangle|drum|flute|piano|saxophone|trombone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play22  "  ?id "   bajA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (23-03-2015)
;Play us that new piece.  	[oald]
;हमारे लिए वो नई धुन बजाइए . 	[Manual]


(defrule play23
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id2  guitar|triangle|drum|flute|piano|saxophone|trombone|piece)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play23  "  ?id "   bajA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;A good play ruined by putrid acting.  	[freedictionary.com] 
;एक अच्छा नाटक बुरे अभिनय से बर्बाद हो गया . 	`	[Manual]


(defrule play24
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(kriyA-by_saMbanXI  ?id1 ?id2)
(id-root ?id2  acting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play24  "  ?id "   nAtaka)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;There should be sad violin music playing at that point. 	[Boy's Life]
;वहाँ पर उदास सारंगी संगीत बजता हुआ होना चाहिए. 	[Manual]

(defrule play25
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1  music)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play25  "  ?id "   baja)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;Play me their new album, please.	[oald]
;कृपया मेरे लिए उनका नया एलबम बजाइए.	  	[Manual]

(defrule play26
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id2  music|CD|album)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play26  "  ?id "   bajA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;Who played Darth Vader in Star Wars? 	[cald]
;स्टार वॊर्ज में डॉर्थ वॆडर का अभिनय किसने किया?  		[Manual]

(defrule play27
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play27  "  ?id "   aBinaya_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;Who played Darth Vader in Star Wars? 	[Merrian Webster]
;स्टार वॊर्ज में डॉर्थ वॆडर का अभिनय किसने किया?  		[Manual]

(defrule play28
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 base)
(kriyA-on_saMbanXI  ?id1 ?id2)
(id-root ?id2 event)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play28  "  ?id "   nAtaka)" crlf))
)


;@@@ Added by 14anu-ban-09 on (27-03-2015)
;He wrote, directed, and starred in the play. 	[Merrian Webster]
;उसने लिखा, निर्देशित किया , और नाटक में अभिनय किया .   	[Manual]

(defrule play29
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 star|direct|write)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play29  "  ?id "   nAtaka)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;His fifth grade class is putting on a play about the first Thanksgiving. [Merrian Webster]
;उसकी पाँचवी प्राथमिक कक्षा पहले थैंक्सगिविंग के बारे  में नाटक रख रही हैं . 	[Manual]

(defrule play30
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-about_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play30  "  ?id "   nAtaka)" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-03-2015)
;The actors played the scene with great skill.	[freedictionary.com]
;अभिनेताओं ने बडी प्रवीणता से दृश्य में अभिनय किया .  		[Manual]

(defrule play31
(declare (salience 4900))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 actor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBinaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  play.clp 	play31  "  ?id "   aBinaya_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on 05-09-2014
;Patricia and I were playing with the idea of moving to Glosgrow. [CALD] (NOTE- Run on Parser no. 29)
;peTrIsiyA Ora mE glAsago me gara baxalane kA vicAra soca rahE We. [Own Manual]
(defrule play017
(declare (salience 5000))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 with) 
(kriyA-object  ?id ?id2)
(id-root ?id2 idea|possibility)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 soca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play017  "  ?id "  " ?id1 "  soca  )" crlf))
)

;@@@ Added by 14anu-ban-09 on 05-09-2014
;She was playing with her hair. [OALD]
;vaha apane bAlo se Kela rahI WI. [Own Manual]
;Stop playing with your food! [OALD]
;apane KAne mawa Kelo. [Own Manual]
(defrule play018
(declare (salience 5000))
(id-root ?id play)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 with) 
(kriyA-object  ?id ?id2)
(id-root ?id2 hair|food)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " play.clp	play018  "  ?id "  " ?id1 "  Kela  )" crlf))
)

