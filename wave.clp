
(defrule wave0
(declare (salience 5000))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hata_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " wave.clp wave0 " ?id "  hata_jA )" crlf)) 
)

(defrule wave1
(declare (salience 4900))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wave.clp	wave1  "  ?id "  " ?id1 "  hata_jA  )" crlf))
)

(defrule wave2
(declare (salience 4800))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vixA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wave.clp	wave2  "  ?id "  " ?id1 "  vixA_kara  )" crlf))
)

(defrule wave3
(declare (salience 4700))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave3   "  ?id "  laharA )" crlf))
)

(defrule wave4
(declare (salience 4600))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave4   "  ?id "  lahara )" crlf))
)

(defrule wave5
(declare (salience 4500))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave5   "  ?id "  lahara )" crlf))
)

;$$$ Modified by Anita 29.1.2014
;"wave","VTI","1.laharAnA"
;He waved his hands for help. [old clp sentence]
(defrule wave6
(declare (salience 4400))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?) ; added relation by Anita
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave6   "  ?id "  laharA )" crlf))
)

;@@@ Added by Anita-23.1.2014
;We waved as the train pulled out of the station. [by mail sentence]
;जैसे ही रेलगाड़ी स्टेशन से चली हमने हाथ हिलाया ।
(defrule wave7
(declare (salience 5100))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-samakAlika_kriyA  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAWa_hilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave7   "  ?id "  hAWa_hilA )" crlf))
)


(defrule wave8
(declare (salience 4000))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave8   "  ?id " warMga  )" crlf))
)


;@@@ Added by 14anu-ban-11 on 3.9.14
;With the charm of this scene heart starts waving.(tourism corpus)
;इस  दृश्य  की  रमणीयता  से  हृदय  हिलोरे  लेने  लगता  है  ।
(defrule wave9
(declare (salience 5100))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 heart)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilore ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave9   "  ?id " hilore  )" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-10-2014)
; Similarly, the then accepted wave picture of light failed to explain the photoelectric effect properly.(ncert)
;isI prakAra usa samaya waka mAnya "prakASa kA warafga sixXAnwa" BI prakASa vixyuwa praBAva ko spaRta karane meM asaPala rahA. (ncert)
(defrule wave10
(declare (salience 4700))
(id-root ?id wave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warafga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wave.clp 	wave10   "  ?id " warafga  )" crlf))
)

