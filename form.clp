
(defrule form0
(declare (salience 5000))
(id-root ?id form)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  form.clp 	form0   "  ?id "  rUpa )" crlf))
)

(defrule form1
(declare (salience 4900))
(id-root ?id form)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  form.clp 	form1   "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-11-2014)
;He also gave an explicit form for the force for gravitational attraction between two bodies.[NCERT]
;unhoMne xo piMdoM ke bIca guruwvAkarRaNa bala ke lie suspaRta sUwra BI xiyA.[NCERT]
(defrule form2
(declare (salience 5000))
(Domain physics)
(id-root ?id form)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 give)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUwra))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  form.clp 	form2  "  ?id "   sUwra)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  form.clp       form2   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-11-2014)
;Waves used in television broadcast or other forms of communication have much higher frequencies and thus can not be received beyond the line of sight.[NCERT]
;xUraxarSana-prasAraNa aWavA anya prakAra ke saFcAra meM upayoga hone vAlI warafgoM kI AvqwwiyAz awyaXika ucca howI hEM, awaH inheM sIXe hI xqRti-reKA se bAhara grahaNa nahIM kiyA jA sakawA.[NCERT]
(defrule form3
(declare (salience 5000))
(Domain physics)
(id-root ?id form)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 communication)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakAra))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  form.clp 	form3  "  ?id "   prakAra)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  form.clp       form3   "  ?id "  physics )" crlf))
)

;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;You must fill a form for appearing in exam. 
;आप परीक्षा में शामिल होने के लिए एक फार्म भरना होगा.
;आपको परीक्षा में शामिल होने के लिए एक फार्म भरना होगा. ;translation modified by 14anu-ban-05 on (15-01-2015)
(defrule form4
(declare (salience 5000))
(id-root ?id form)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 fill)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PArma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  form.clp 	form4   "  ?id "  PArma )" crlf))
)


;"form","V","1.banAnA"
;He formed a union in his organisation.
;The Prime Minister formed his cabinet within a week of his election.
;
;LEVEL 
;
;
; Headword : form
;
;Examples --
;
;`form" Sabxa ke viviXa prayoga--
; 
;"form","N","1.Akqwi"
;A dark form could be seen in the distance.
;--"2.Sabxa_rUpa"
;          ---- < Sabxa kI viviXa AkqwiyAz < Akqwi
;The past form of `write' is `wrote'.
;--"3.prArUpa/CapA_huA_PArma"
;          ---- < Xvani kA Akqwi-rUpa < Akqwi
;I got an application form.
;--"4.avasWA"
;          ---- < kisI kA BI Akqwi-viSeRa < Akqwi
;Water in the form of solid is ice.
;--"5.kAryaviXi"
;          ---- < kiye jAnevAle kArya kA (Akqwi) rUpa < Akqwi
;The assignments should be done in a proper form.
;--"6.prakAra
;          ---- < viBinna Akqwi-rUpoM meM se eka < Akqwi
;The modern form of the painting is often obscure.
;   
;"form","V","1.banAnA"
;          ---- < kisI ko Akqwi-rUpa xenA < Akqwi
;He formed a union in his organisation.
;The Prime Minister formed his cabinet within a week of his election.
;----------------------------------------------------------------------
;
;sUwra : prArUpa`[<Akqwi`]
;-------------
;
;  `form' Sabxa ke Upara xiye gaye viviXa prayoga vAswava meM Akqwi Sabxa ke 
;avasWA-viSeRa hEM . una avasWA-viSeRoM ke lie alaga-alaga Sabxa loka-prayoga
;meM hEM . veM hI Upara ke viBinna Sabxa hEM . Akqwi Sabxa se ye saBI samaJe jA 
;sakawe hEM . ye saBI Sabxa Akqwi Sabxa ke avasWA-viSeRa kEse hEM, inakI vyAKyA  
;xeKane se yaha spaRta ho jAyegA . 
;
;-- SabxarUpa vAswava meM kaI viBinna SabxoM kI AkqwiyoM ke mela se banA huA hE . 
;yA eka Sabxa kI viBinna AkqwiyAz hI SabxarUpa kahalAwI hEM . 
;
;-- prArUpa/CapA huA PArma . XvaniyoM(SabxoM Axi) ke prawyakRa AkqwiyoM(lipi) kA  
;Cape hue rUpa meM PArma ko xeKA jA sakawA hE . yaha XyAwavya hE ki prArUpa Sabxa meM  
;`rUpa' Akqwi kA hI vAcaka hE . 
;
;-- avasWA . prakAra . kisI BI Akqwi-viSeRa kI sWiwi avasWA kahalAwI hE . 
;viBinna Akqwi-viSeRoM meM se ciwa eka viSiRta Akqwi kI avasWA ko prakAra 
;kahawe hEM . 
;
;-- kArya-viXi . kiye jAnevAle kArya kA Akqwi-rUpa hI kArya-viXi hE . 
;
;-- banAnA . kisI ko Akqwi-rUpa xenA usakA banAnA kahalAwA hE . 
;
; 
;
