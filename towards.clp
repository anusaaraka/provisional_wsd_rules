;@@@ Added by 14anu-ban-07,(26-08-2014)
;These people themselves have much devotion towards this reservoir.(tourism corpus)
;इन  लोगों  को  स्वयं  भी  इस  कुंड  के  प्रति  बड़ी  श्रद्धा  है  ।
(defrule towards3
(declare (salience 4900))
(id-root ?id towards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-towards_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_prawi ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  towards.clp 	towards3   "  ?id "  ke_prawi  )" crlf))
)

;@@@ Added by 14anu-ban-07,(14-03-2015)
;The course had been geared towards the specific needs of its members.(oald)
;पाठ्यक्रम को उसके सदस्यों की विशिष्ट आवश्यकताओं के लिये तैयार किया गया था.(manual)
(defrule towards4
(declare (salience 5000))
(id-root ?id towards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-towards_saMbanXI  ?id1 ?id2)
(id-root ?id1  gear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  towards.clp 	towards4   "  ?id "  ke_liye )" crlf))
)
;----------------- Default rules ---------------------------
(defrule towards0
(declare (salience 5000))
(id-root ?id towards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samIpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  towards.clp 	towards0   "  ?id "  samIpa )" crlf))
)

;"toward","Adv","1.samIpa/uxyawa/lage/banawA_huA"
(defrule towards1
(declare (salience 4900))
(id-root ?id towards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  towards.clp 	towards1   "  ?id "  kI_ora )" crlf))
)

(defrule towards2
(declare (salience 4800))
(id-root ?id towards)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  towards.clp 	towards2   "  ?id "  kI_ora )" crlf))
)

;"toward","Prep","1.kI_ora"
;We walked towards the park.
;--"2.ke_viRaya_meM"
;The concert was held towards the spirit of unity.
;--"3.ke_lie"
;We collected money towards orphanage.
;--"4.ke_nikata"
;Computer will dominate towards the end of the century.
;
