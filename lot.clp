
(defrule lot0
(declare (salience 5000))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 money )
(viSeRya-of_saMbanXI ?id1 ?id) ;Replaced viSeRya-of_viSeRaNa as viSeRya-of_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dera))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lot.clp 	lot0   "  ?id "  Dera )" crlf))
)

;Modified by sheetal
(defrule lot1
(declare (salience 4900))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id =(+ ?id 2))
(id-word =(+ ?id 1) of)
(id-word =(- ?id 1) a)
(id-word =(+ ?id 2) time)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) bahuwa_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  lot.clp         lot1   "  ?id (+ ?id 1) (+ ?id 2) "  bahuwa_samaya )" crlf))
)

;$$$ Modified by 14anu-ban-09 on 24-07-2014 
;Changed meaning "bahuwa_badZA" to "bahuwa_sArA"
;Some people take a lot of time to acclimatize themselves to the new environment.
;kuCa loga naye vAwAvaraNa ke anukUla hone meM bahuwa sArA samaya lewe hE. [Own Manual]
;There is a lot of different types of fish in the river. [Parallel Corpus]
;नदी  में  विभिन्न  प्रकार  की बहुत  साली  मछलियाँ  हैं  ।

(defrule lot2
(declare (salience 4900))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_sArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lot.clp 	lot2   "  ?id "  bahuwa_sArA)" crlf))
)

;$$$ Modified by 14anu-ban-09 on 24-07-2014
;Changed meaning "bahuwa_badI_safKya" to "bahuwa"
;It is said that when Buddha used to be in Rajgrih then he liked to stay at this place a lot . [Parallel Corpus]
;कहा  जाता  है  कि  बुद्ध  जब  राजगृह  में  होते  तब  उन्हें  इस  जगह  ठहरना  बहुत  पसन्द  था  ।
;For the people interested in fun  , food items and shopping there is a lot in Goa. [Parallel Corpus]
;मौज-मस्ती  ,  खाने-पीने  और  खरीदारी  के  शौकीन  लोगों  के  लिए  गोवा  में  बहुत  कुछ  है  ।

(defrule lot3
(declare (salience 4800))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lot.clp 	lot3   "  ?id "  bahuwa)" crlf))
)

;default_sense && category=noun	Dera	0
;"lot","N","1.Dera"
;This lot is meant for him.
;--"2.bahuwa badI saMKyA"
;Lots of people come to this temple every day.
;--"3.sWAna viSeRa"
;Parking lot.
;--"4.BAgya"
;I pity her lot.
;
;

;$$$ Modified by 14anu-ban-08 (16-01-2015)           ;changed meaning from 'bahuwa_sare' to 'bahuwa_sarA
;@@@ Added by 14anu17
;Lots of people have tried several times before .
;बहुत सारे लोग पहले कई बार का प्रयास कर चुके हैं . 
(defrule lot4
(declare (salience 4900))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 2) people|peoples|teacher|teachers|student|students)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_sArA))     ;changed meaning from 'bahuwa_sare' to 'bahuwa_sarA' by 14anu-ban-08 (16-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lot.clp 	lot4  "  ?id "  bahuwa_sArA )" crlf))                           ;changed meaning from 'bahuwa_sare' to 'bahuwa_sarA' by 14anu-ban-08 (16-01-2015)
)

;@@@ Added by 14anu17
;She still have an awful lot.
;उसका अभी भी बुरा भाग्य है .
(defrule lot5
(declare (salience 4900))
(id-root ?id lot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse =(- ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAgya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lot.clp 	lot5   "  ?id "  BAgya )" crlf))
)
