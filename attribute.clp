
(defrule attribute0
(declare (salience 5000))
(id-root ?id attribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attribute.clp 	attribute0   "  ?id "  guNa )" crlf))
)

;"attribute","N","1.guNa/BAva"
;His greatest attribute was his helpful nature.
;

(defrule attribute1
(declare (salience 4900))
(id-root ?id attribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXa_TaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attribute.clp 	attribute1   "  ?id "  sambanXa_TaharA )" crlf))
)

;"attribute","VT","1.sambanXa_TaharAnA"
;Attribute their illness to their poor diets.
;

;@@@ Added by 14anu11
;Social evils , most of which are certainly capable of removal , are attributed to original sin , to the unalterableness of human nature, or the social structure , or ( in India ) to the inevitable legacy of previous births .
;इन बुराइयों को किसी पुराने पाप का परिणाम बताया जाता है या इनके बारे में यह कहा जाता है कि इंसान की प्रकृति या सामाजिक गठन ही कुछ ऐसा है कि उसे बदला नहीं जा सकता या 
;( हिंदुस्तान में ) इन्हें पुराने जन्म का फल बताया जाता
(defrule attribute02
(declare (salience 5000))
(id-root ?id attribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariNAma_bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attribute.clp 	attribute02  "  ?id "  pariNAma_bawA )" crlf))
)


;@@@ Added by 14anu-ban-02 (20-11-2014)
;Arthropod abundance and biomass was also higher in the clover cover cropped fields throughout much of the songbird breeding season, which was attributed to an increased supply of flower nectar from the clover.[agriculture]
;गानेवाले पक्षी के प्रजनन ऋतु  में तिपतिया फसलो के क्षत्रों में संधिपाद प्राणी की बहुलता और जैव ईधन ज्यादा थी, जो  तिपतिया  में फूल के पराग की बढी हुई सप्लाई  का कारण बताया गया था.[manual]
(defrule attribute2
(declare (salience 4900))
(id-root ?id attribute)
?mng <-(meaning_to_be_decided ?id)
(Domain agriculture)
(kriyA-to_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-domain_type  ?id agriculture))
(assert (id-wsd_root_mng ?id kAraNa_bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  attribute.clp       attribute2   "  ?id "  kAraNa_bawA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attribute.clp 	attribute2   "  ?id "  kAraNa_bawA )" crlf))
)

;$$$Modified by 14anu-ban-02(10-01-2015)
;Be sure to attribute authorship of the posting to the posting party.  [sentence from http://sentence.yourdictionary.com/attribute] 
;पोस्टिंग पार्टी को पोस्टिंग के लेखकत्व का श्रेय देना सुनिश्चित करें.
;@@@ Added by 14anu13 on 28-06-14
;Be sure to attribute authorship of the posting to the posting party.  [sentence from http://sentence.yourdictionary.com/attribute] 
;पोस्टिंग पार्टी को पोस्टिंग के लेखकत्व का श्रेय देना सुनिश्चित करें.
(defrule attribute3
(declare (salience 5000))
(id-root ?id attribute)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object 	?id 	 ?);commented by 14anu-ban-02(10-01-2015)
(to-infinitive  ? ?id);added by 14anu-ban-02(10-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sreya_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attribute.clp 	attribute3   "  ?id "  Sreya_xe )" crlf))
)


