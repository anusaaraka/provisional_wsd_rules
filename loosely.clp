;@@@Added by 14anu-ban-08 (31-03-2015)
;She draped the scarf loosely around her shoulders.   [cald]
;उसने अपने कन्धों के चारों ओर स्कार्फ को ढीला  ओढ़ा .  [self]
(defrule loosely0
(declare (salience 0))
(id-root ?id loosely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loosely.clp 	loosely0   "  ?id "  DIlA )" crlf))
)


;@@@Added by 14anu-ban-08 (31-03-2015)
;Economic growth can be loosely defined as an increase in GDP.  [oald]
;आर्थिक विकास को मोटे तौर पर जीडीपी में बढोतरी से स्पष्ट किया जा सकता हैं. [self]
(defrule loosely1
(declare (salience 100))
(id-root ?id loosely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 define)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mote_wOra_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loosely.clp 	loosely1   "  ?id "  mote_wOra_para )" crlf))
)

