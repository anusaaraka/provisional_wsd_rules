;@@@ Added by 14anu-ban-04 (23-03-2015)
;I didn't have to spend a lot of dough for a new stereo.             [merriam-webster] 
;मुझे एक नये स्टीरियो के लिए बहुत सारा पैसा खर्च  नहीं करना  पड़ा .                           [self]
(defrule dough1
(declare (salience 20))
(id-root ?id dough)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id1) 
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?kri spend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dough.clp    dough1  "  ?id "  pEsA)" crlf))
)


;@@@ Added by 14anu-ban-04 (23-03-2015)
;Leave the dough to rise.                 [oald]
;सने हुए आटे को फूलने के लिए छोड़ दीजिए .             [self]
(defrule dough0
(declare (salience 10))
(id-root ?id dough)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanA_huA_AtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dough.clp    dough0  "  ?id " sanA_huA_AtA)" crlf))
)



