
;@@@ Added by 14anu-ban-09 on (28-01-2015)
;Rainwater permeating through the ground.	[http://www.oxfordlearnersdictionaries.com/definition/english/permeate]
;बारिश का पानी ज़मीन पर फ़ैल रहा हैं.			[Self]

(defrule permeate2
(declare (salience 4900))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?id1)
(id-root ?id1 ground)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZEla_raha))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate2   "  ?id "  PZEla_raha )" crlf))		
)

;@@@ Added by 14anu-ban-09 on (28-01-2015)
;Vast, distant galaxies, the tiny invisible atoms, men and beasts all are permeated through and through with a host of magnetic fields from a variety of sources.   [http://aura.edu.in/hindilang/class-12/physics-part1/0177.html]
;विशाल दूरस्थ मंदाकिनी , अतिसूक्ष्म अदृश्य परमाणु , आदमी और जानवर सभी पूरी तरह से विभिन्न प्रकार के स्रोतो के चुम्बकीय क्षेत्रो से युक्त सरोबार हैं.		[Self] 

(defrule permeate3
(declare (salience 4000))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_with_saMbanXI  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 field)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarobAra))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate3   "  ?id "  sarobAra )" crlf))		
)

;@@@ Added by 14anu-ban-09 on (28-01-2015)
;The smell of leather permeated the room.	[http://www.oxfordlearnersdictionaries.com/definition/english/permeate]
;चमड़े की सुगंध कमरे में फ़ैल गई. 			[Self]

(defrule permeate4
(declare (salience 4900))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-through_saMbanXI  ?id ?))
(kriyA-object ?id ?id1)
(id-root ?id1 room)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZEla_jA))	
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate4   "  ?id "  PZEla_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  permeate.clp	permeate4  "  ?id " meM)" crlf)
)		
)

;@@@ Added by 14anu-ban-09 on (28-01-2015)
;The fragrance of flowers permeated through the drawing room.		[Hinkhoj.com]	;source added  by 14anu-ban-09 on (28-01-2015)
;फूलो की सुगंध बैठक में फ़ैल गई.						[Self] 		;Added by 14anu-ban-09 on (28-01-2015)
(defrule permeate5
(declare (salience 4900))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?id1)	
(id-root ?id1 room)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZEla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate5   "  ?id "  PZEla_jA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (28-01-2015)
;A room permeated with tobacco smoke.		[http://www.merriam-webster.com/dictionary/permeate]
;तंबाकू के धुएँ से भरा हुआ कमरा.				[Self]

(defrule permeate6
(declare (salience 200))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_ho))	;changed meaning 'Bara_jA' to 'BarA_ho' by 14anu-ban-09 on (02-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate6   "  ?id "  Bara_ho )" crlf))		
)
;------------------- Default Rules ----------------------------
(defrule permeate0
(declare (salience 5000))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAragamya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate0   "  ?id "  pAragamya )" crlf))
)

;"permeate","Adj","1.pAragamya"
;Fertilizer in liquid form is permeable in the soil.
;

;$$$ Modified by 14anu-ban-09 on (28-01-2015)
;### [Counter Example] ### Vast, distant galaxies, the tiny invisible atoms, men and beasts all are permeated through and through with a host of magnetic fields from a variety of sources.   [http://aura.edu.in/hindilang/class-12/physics-part1/0177.html]
;### [Counter Example] ### विशाल दूरस्थ मंदाकिनी , अतिसूक्ष्म अदृश्य परमाणु , आदमी और जानवर सभी पूरी तरह से विभिन्न प्रकार के स्रोतो के चुम्बकीय क्षेत्रो से युक्त सरोबार हैं.		[Self]  
;Liquid permeating a membrane.		[http://www.thefreedictionary.com/permeate]
;Liquid which is permeating a membrane is viscous.	[Modified sentence]
;झिल्ली में रिसता हुआ द्रव गाढा था.			[Self]

;"permeate","V","1.meM_PZEla_jAnA"

(defrule permeate1
(declare (salience 200))
(id-root ?id permeate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id risawA_ho))	; Modified 'meM_PZEla_jA' to 'Gusa_jA' by 14anu-ban-09 on (28-01-2015) 
						; Modified 'Gusa_jA' to 'risawA_ho' by 14anu-ban-09 on (28-01-2015) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  permeate.clp 	permeate1   "  ?id "  risawA_ho )" crlf))		
)

