;@@@ Added by 14anu-ban-06 (27-11-2014)
;Being ignorant is not so much a shame , as being unwilling to learn .(parallel corpus)
;अज्ञानी होना उतनी शर्म की बात नहीं है जितना कि सीखने की इच्छा न रखना .(parallel corpus)
;Ignorant men raise questions that wise men answered a thousand years ago .(parallel corpus)
;अज्ञानी व्यक्ति वह प्रश्न पूछते हैं जिनका उत्तर समझदार व्यक्तियों द्वारा एक हजार पहले दे दिया गया होता है.(parallel corpus)
(defrule ignorant0
(declare (salience 0))
(id-root ?id ignorant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ajFAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ignorant.clp 	ignorant0   "  ?id "  ajFAnI )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-11-2014)
;They were totally ignorant of the most elementary scientific knowledge and whilk they believed blindly and abjectly in every kind of religious hocus - pocus they were incredulous of the real miracles of science .(parallel corpus)
;एक तरफ तो वे आधारभूत वैज्ञानिक जानकारियों से पूरी तरह अनजान थे तो दूसरी तरफ हर तरह के धार्मिक पाखंड पर आंख मूंद कर विश्वास करते हुए विज्ञान के वास्तविक चमत्कारों को संदेह की दृष्टि से देखते थे .(parallel corpus)
(defrule ignorant1
(declare (salience 2000))
(id-root ?id ignorant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anajAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ignorant.clp 	ignorant1   "  ?id "  anajAna )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-11-2014)
;I'm totally ignorant about design.(COCA)
;मैं योजना के बारे में पूर्ण रूप से अपरिचित हूँ . (manual)
;But in Britain, people are extremely ignorant about the history of the Middle East.(COCA)
;परन्तु ब्रिटन में, लोग मध्य पूर्व के इतिहास के बारे में अत्यधिक अपरिचित हैं . (manual)
;He's ignorant about modern technology.(OALD)
;वह आधुनिक प्रौद्योगिकी के बारे में अपरिचित है . (manaul)
(defrule ignorant2
(declare (salience 2500))
(id-root ?id ignorant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-about_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apariciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ignorant.clp 	ignorant2   "  ?id "  apariciwa )" crlf))
)
