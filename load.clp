;Commented by Nandini(8-5-14) ;root word is loaded so the clp will be different.
;(defrule load0
;(declare (salience 5000))
;(id-root ?id load)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id loaded )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id BAriwa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  load.clp  	;load0   "  ?id "  BAriwa )" crlf))
;)

;"loaded","Adj","1.BAriwa"
;A loaded dice.
;--"2.BarA_huA"
;He carries a loaded gun.
;--"3.nihiwa arWa honA"
;A loaded statement.
;A loaded question.
;
(defrule load1
(declare (salience 3000));salience change by Nandini 
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load1   "  ?id "  boJA )" crlf))
)

(defrule load2
(declare (salience 4800))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load2   "  ?id "  lAxa )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;The available load is not enough to meet the requirements of the city.[hinkoja]
;upalabXa vixyuwa BAra Sahara kI AvaSyakwAoM ko paryApwa nahIM hE.
(defrule load3
(declare (salience 4950))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(saMjFA-to_kqxanwa  ?id2 ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyuwa_BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load3   "  ?id "  vixyuwa_BAra )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;We saw a load of houses before we bought this one. [OALD]
;yaha Gara KarIxane ke pahale hamane bahuwa Gara xeKe. 
(defrule load4
(declare (salience 4950))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 of)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load4   "  ?id "  bahuwa )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;There is loads to do today. [OALD]
;Aja karane ke liye bahuwa hE.
(defrule load5
(declare (salience 4950))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load5   "  ?id "  bahuwa )" crlf))
)


;@@@--- Added by Nandini(8-5-14)
;Teaching loads have increased in all types of school.[OALD]
; saba prakAra ke vixyAlayoM  meM siKAne kA BAra baDa cukA hE.
(defrule load6
(declare (salience 4950))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 increase)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load6   "  ?id "  BAra )" crlf))
) 

;$$$ Modified by 14anu-ban-08 (14-10-2014)     
;A pillar with rounded ends as shown in Fig. 9.10(a) supports less load than that with a distributed shape at the ends (Fig. 9.10(b)).   [NCERT]
;गोल सिरों वाले खम्भे जैसा चित्र 9.10(a) में दिखाये गये हैं, फैलावदार आकृति चित्र 9.10(b) वाले खम्भों की अपेक्षा कम भार वहन कर सकते हैं.     [NCERT]
;@@@--- Added by Nandini(8-5-14)
;Extra warmth from sunlight can put an additional load on the air-conditioning system.[OALD]
(defrule load7
(declare (salience 4950))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 put|support|withstand) ;added 'support|withstand' by 14anu-ban-08 (14-10-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load7   "  ?id "  BAra )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;She was loaded down with bags of groceries.[OALD]
;vaha kirAnoM ke WEloM ke sAWa KUba_laxI huI WI.
(defrule load8
(declare (salience 4850))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KUba_laxA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " load.clp	load8  "  ?id "  " ?id1 "  KUba_laxA_ho )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;Have you loaded the software?[OALD]
;kyA Apane soYPtaveyara loda kara cuke hEM?
(defrule load9
(declare (salience 4850))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loda_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load9   "  ?id "  loda_kara )" crlf))
)

;@@@--- Added by Nandini(8-5-14)
;She loaded film into the camera.[OALD]
;usane kEmare ke aMxara sinemA Bara xiyA.
(defrule load10
(declare (salience 4850))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Bara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load10   "  ?id "  Bara_xe )" crlf))
)


;@@@ Added by 14anu-ban-08 (14-10-2014)
;Nevertheless, the body still returns to its original dimension when the load is removed.        [NCERT]
;फिर भी भार हटाने पर पिंड अभी भी अपनी प्रारम्भिक विमाओं पर वापस आ जाता है.       [NCERT]
(defrule load11
(declare (salience 4900))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " load.clp	load11  "  ?id "  BAra )" crlf))
)


;@@@ Added by 14anu-ban-08 (14-10-2014)
;It is very important to know the behavior of the materials under various kinds of load from the context of engineering design.   [NCERT]
;अभियान्त्रिकी डिजाइन के सन्दर्भ में विभिन्न प्रकार के भार के लिए द्रव्यों के व्यवहार को जानना बहुत महत्त्वपूर्ण होता है.     [NCERT]
(defrule load12
(declare (salience 4902))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 kind)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " load.clp	load12  "  ?id "  BAra )" crlf))
)


;@@@ Added by 14anu-ban-08 (14-10-2014)
;Robert Hooke, an English physicist (1635-1703 A.D) performed experiments on springs and found that the elongation (change in the length) produced in a body is proportional to the applied force or load.     [NCERT]
;एक अङ्ग्रेज भौतिक शास्त्री राबर्ट हुक (सन् 1635-1703) ने स्प्रिंगों पर प्रयोग किए और यह पाया कि किसी पिंड में उत्पन्न विस्तार (लम्बाई में वृद्धि) प्रत्यारोपित  भार के अनुक्रमानुपाती होता है.   [NCERT]
(defrule load13
(declare (salience 4903))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id1 ?id)
(id-root ?id1 proportional)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  BAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " load.clp	load13  "  ?id "  BAra )" crlf))
)

;$$$Modified  by 14anu-ban-08 (07-04-2015)   ;changed meaning
;@@@ Added by 14anu11
;He was surprised to listen to an oath of this kind and he decided to test Basava by asking for cart - loads of gold , pearls and rubies .
;वह इस प्रकार की शपथ सुनकर हैरान हुआ और निश्चय किया कि छकडा भर सोना , मोती और मूंगा मांगकर बसव की परीक्षा ली जाय .
(defrule load14
(declare (salience 5000))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-viSeRaNa  ?id2 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))   ;changed meaning from 'Bre' to 'Bara' by 14anu-ban-08 (07-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load14   "  ?id " Bara )" crlf))   ;changed meaning from 'Bre' to 'Bara' by 14anu-ban-08 (07-04-2015)
)


;$$$ Modified by 14anu-ban-08 (16-01-2015)        ;changed meaning
;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
;Is the gun loaded? 
;क्या बंदूक भरी हुई है?
(defrule load15
(declare (salience 4800))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 gun|pistol|cannon|handgun|revolver|shotgun)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarI_ho))        ;changed meaning from 'BarI_huI' to 'BarI_ho' by 14anu-ban-08 (16-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load15   "  ?id "  BarI_ho )" crlf))                                        ;changed meaning from 'BarI_huI' to 'BarI_ho' by 14anu-ban-08 (16-01-2015)
)


;"load","V","1.lAxanA"
;Load the truck.
;--"2.BaranA"
;Load the software.
;
;

;@@@Added by 14anu-ban-08 (07-04-2015)
;She thought she would not be able to bear the load of bringing up her family alone.  [oald]
;उसने सोचा वह अकले अपने परिवार का बोझ नहीं उठा पा रही हैं.  [self]
(defrule load16
(declare (salience 5000))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 bring)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boJa))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load16   "  ?id " boJa )" crlf))   
)

;@@@Added by 14anu-ban-08 (07-04-2015)
;Can you help me load the dishwasher?  [oald]
;क्या तुम मेरी मदद कर सकते हो बर्तन धोने की मशीन रखने में.  [self]
(defrule load17
(declare (salience 5000))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 dishwasher)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load17   "  ?id " raKa )" crlf))   
)

;@@@Added by 14anu-ban-08 (07-04-2015)
;They loaded her with gifts.  [oald]
;उसने बहुत सारे उपहार दिये.  [self]
(defrule load18
(declare (salience 5000))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 gift)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load18   "  ?id " xe )" crlf))   
)

;@@@Added by 14anu-ban-08 (07-04-2015)
;Wait for the game to load.  [oald]
;खेल के लोड होने का इन्तजार करो.  [self]
(defrule load19
(declare (salience 5000))
(id-root ?id load)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id1 ?id2)
(kriyA-to_saMbanXI ?id1 ?id)
(id-root ?id1 wait)
(id-root ?id2 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loda_ho))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  load.clp 	load19   "  ?id " loda_ho )" crlf))   
)
