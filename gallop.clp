
;He galloped through his work so that he could go home early.
;usane apanA kAma jalxI se kiyA wAki vaha jalxI Gara pahuzca sake
(defrule gallop0
(declare (salience 5000))
(id-root ?id gallop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalxI_se_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gallop.clp	gallop0  "  ?id "  " ?id1 "  jalxI_se_kara  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (15-01-2015)
;We galloped through the woods.[cambridge]
;हम जंगल में से तेजी से निकले .   [manual]
(defrule gallop3
(declare (salience 4801))
(id-root ?id gallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gallop.clp 	gallop3   "  ?id "  wejI_se_nikala )" crlf))
)

;------------------------ Default rules-----------------
(defrule gallop1
(declare (salience 4900))
(id-root ?id gallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarapata_xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gallop.clp 	gallop1   "  ?id "  sarapata_xOdZa )" crlf))
)

;"gallop","N","1.sarapata_xOdZa"
;I saw the gallop of the horse.
;
(defrule gallop2
(declare (salience 4800))
(id-root ?id gallop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarapata_xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gallop.clp 	gallop2   "  ?id "  sarapata_xOdZa )" crlf))
)

;"gallop","V","1.sarapata_xOdZanA"
;The horse was galloping.
;
