;@@@ Added by 14anu-ban-03 (30-03-2015)
;Obsessions and compulsions often develop in people who live stressful lives. [oald]
;जुनून और लालसाएँ अक्सर उन लोगों में विकसित होती  हैं जो तनावपूर्ण जीवन  जीते हैं . [manual]
(defrule compulsion1
(declare (salience 10)) 
(id-root ?id compulsion)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 develop)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAlasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compulsion.clp 	compulsion1  "  ?id "  lAlasA )" crlf))
)

;@@@ Added by 14anu-ban-03 (30-03-2015)
;For many people, dieting is a compulsion. [cald]
;बहुत सारे लोगों के लिए, आहार आवश्यकता है .  [manual]
(defrule compulsion2
(declare (salience 10)) 
(id-root ?id compulsion)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyakawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compulsion.clp 	compulsion2  "  ?id "  AvaSyakawA )" crlf))
)

;---------------------------- Default rules -----------------------------

;@@@ Added by 14anu-ban-03 (30-03-2015)
;The legal system is based on compulsion. [oald]
;कानूनी प्रणाली दबाव पर आधारित है . [manual]
(defrule compulsion0
(declare (salience 00))
(id-root ?id compulsion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compulsion.clp       compulsion0   "  ?id "  xabAva )" crlf))
)


