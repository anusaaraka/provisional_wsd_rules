
(defrule formal0
(declare (salience 5000))
(id-root ?id formal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id OpacArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formal.clp 	formal0   "  ?id "  OpacArika )" crlf))
)

(defrule formal1
(declare (salience 4900))
(id-root ?id formal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id OpacArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formal.clp 	formal1   "  ?id "  OpacArika )" crlf))
)

;"formal","Adj","1.OpacArika"
;For formal functions formal dress is more appropriate.
;A formal students'union meeting was held yesterday.
;
;
;@@@ Added by 14anu-ban-05 on (02-03-2015)
;Ashmol liked his proposal and in 1667 the museum started in a formal way .[tourism]
;ESamola ko unakA praswAva pasaMxa AyA Ora viXivawa rUpa se 1677 meM isa saMgrahAlaya kI SurUAwa ho gaI .[tourism]
(defrule formal2
(declare (salience 5001))
(id-root ?id formal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 way)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXivawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formal.clp 	formal2   "  ?id "  viXivawa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (02-03-2015)
;Critics have concentrated too much on the formal elements of her poetry, without really looking at what it is saying.[tourism]
;आलोचकों ने  वास्तव में कविता क्या कह रही है को देखे बगैर, उसकी कविता  के  रूपात्मक तत्वों पर बहुत अधिक ध्यान केंद्रित किया है.[manual]
(defrule formal3
(declare (salience 5001))
(id-root ?id formal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 element)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formal.clp 	formal3   "  ?id "  rUpAwmaka )" crlf))
)


;@@@ Added by 14anu-ban-05 on (02-03-2015)
;A formal garden is carefully designed and kept according to a plan, and it is not allowed to grow naturally.[cald]
;एक सुव्यवस्थित उद्यान सावधानी से बनाया गया है और रखा एक योजना के अनुसार रखा गया है, और इसे स्वाभाविक रूप से विकसित करने के लिए अनुमति नहीं है.	[manual]
(defrule formal4
(declare (salience 5001))
(id-root ?id formal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 garden)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formal.clp 	formal4   "  ?id "  suvyavasWiwa )" crlf))
)
