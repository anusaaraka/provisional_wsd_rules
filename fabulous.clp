
;@@@ Added by 14anu-ban-05 on (05-12-2014)
;The reason is that because of this many people are rushing there due to which a threat has developed to this fabulous natural monument.[TOURISM]
;kAraNa hE ki isase vahAz logoM kI kAPI AvAjAhI ho rahI hE jisase isa SAnaxAra necurala smAraka para KawarA uwpanna ho gayA hE .[TOURISM]
(defrule fabulous0
(declare (salience 1000))
(id-root ?id fabulous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fabulous.clp 	fabulous0   "  ?id "  SAnaxAra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-12-2014)
;If you are a fancier of eating various varieties of food then understand that you will be made available fabulous tasting sea food .[TOURISM]
;agara Apa viBinna vErAyatI kA KAnA KAne ke SOkIna hEM wo mAna lIjie ki Apako sI PUda lAjavAba svAxa upalabXa karAezge .[TOURISM]
(defrule fabulous1
(declare (salience 1000))
(id-root ?id fabulous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 taste|cook|food|meal)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAjavAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fabulous.clp 	fabulous1   "  ?id "  lAjavAba )" crlf))
)
