

(defrule trim1
(declare (salience 4900))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka-TAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim1   "  ?id "  TIka-TAka )" crlf))
)

;"trim","Adj","1.TIka-TAka"
;She has a trim figure.
;
(defrule trim2
(declare (salience 4800))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim2   "  ?id "  suvyavasWiwa )" crlf))
)

;"trim","N","1.suvyavasWiwa"
;The garden was trim && nice.
;
(defrule trim3
(declare (salience 4700))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta_CAzta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim3   "  ?id "  kAta_CAzta_kara )" crlf))
)

;"trim","VT","1.kAta_CAzta_karanA"
;He trimmed the hedges recently.
;

;@@@ Added by Prachi Rathore[10-2-14]
;Using the diet he's trimmed down from 90 kilos to 70.[oald]
;आहार का उपयोग करते हुए उसने 90 किलो से 70 वजन कम किया  है . 
(defrule trim4
(declare (salience 5000))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vajana_kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " trim.clp 	trim4  "  ?id "  " ?id1 "  vajana_kama_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(17-03-2015)
;Gloves trimmed with fur.(oald)
;दस्तानों को फर के साथ सजाया . (manual)
(defrule trim5
(declare (salience 4800))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 fur)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim5   "  ?id "  sajA )" crlf))
)


;@@@ Added by 14anu-ban-07,(17-03-2015)
;They're trying to trim their costs, so staff who leave are not being replaced.(cambridge)
; वे अपना खर्चा कम करने का प्रयास कर रहे हैं, ताकि जो कर्मचारी छोड कर जा रहे हैं (उनके) बदले में  कोई ओर रखना नहीं पडे. (manual)
(defrule trim6
(declare (salience 4900))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 cost)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim6   "  ?id "  kama_kara )" crlf))
)

;@@@ Added by 14anu-ban-07,(17-03-2015)
;A trim garden.(oald)
;एक साफ सुथरा उद्यान . (manual)
(defrule trim7
(declare (salience 5000))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAPa_suWarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim7   "  ?id "  sAPa_suWarA)" crlf))
)


;@@@ Added by 14anu-ban-07,(17-03-2015)
;A blue jacket with a white trim.(oald)
;एक नीला जैकेट  सफेद किनारे के साथ  . (manual)
(defrule trim8
(declare (salience 5100))
(id-root ?id trim)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 white|grey|black)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trim.clp 	trim8   "  ?id "  kinArA)" crlf))
)


