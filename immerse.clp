;@@@ Added by 14anu-ban-06 (21-02-2015)
;The shells should be immersed in boiling water for two minutes.(OALD)
;छिलके दो मिनटों पानी में उबालने के लिए डुबाए जाने चाहिए . (manual)
;The seeds will swell when immersed in water. (cambridge)
;बीज फूलेगा जब पानी में डुबाया जाता है .(manual) 
(defrule immerse0
(id-root ?id immerse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dubA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immerse.clp 	immerse0   "  ?id "  dubA )" crlf))
)

;@@@ Added by 14anu-ban-06 (21-02-2015)
;Clare and Phil were immersed in conversation in the corner. (OALD)
;क्लेर और फिल कोने में वार्तालाप में तल्लीन हो गये थे . (manual)
(defrule immerse1
(declare (salience 2000))
(id-root ?id immerse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 conversation|work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wallIna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immerse.clp 	immerse1   "  ?id "  wallIna_ho )" crlf))
)
