;@@@ Added by 14anu-ban-11 on (28-01-2015)
;Palmer tries to quash such talk.(coca)
;पॉमर ऐसी बातचीत समाप्त करने का प्रयास करता है . (anusaaraka)
(defrule quash1
(declare (salience 20))
(id-root ?id quash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 talk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quash.clp 	quash1   "  ?id "  samApwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-11 on (28-01-2015)
;Shamus denied Wizard's goal was to quash Drum's party.(coca)
;शॆमस ने इनकार किया विजर्ड का लक्ष्य ड्रम की पार्टी रछ करने वाला था .(anusaaraka) 
(defrule quash2
(declare (salience 30))
(id-root ?id quash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 party)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id raCa_kara))   ;commented by 14anu-ban-11 (02-02-2015)
(assert (id-wsd_root_mng ?id raxxa_kara))    ;Added by 14anu-ban-11 (02-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quash.clp 	quash2   "  ?id "  raCa_kara )" crlf))   ;commented by 14anu-ban-11
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quash.clp 	quash2   "  ?id "  raxxa_kara )" crlf))   ;Added by 14anu-ban-11
)


;@@@ Added by 14anu-ban-11 on (28-01-2015)
;Even cold weather won't necessarily quash the weed.(coca)
;यहाँ तक कि ठण्डा मौसम भी घास फूस अनिवार्य रूप से नष्ट नहीं करेगा .(manual)
(defrule quash3
(declare (salience 40))
(id-root ?id quash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 weed)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quash.clp 	quash3   "  ?id "  naRta_kara )" crlf))
)

;------------------- Default Rules --------------------
;@@@ Added by 14anu-ban-11 on (28-01-2015)
;The king quashed the attempt of an uprising.(hinkhoj)
;राजा ने बगावत के प्रयास का दमन किया .(manual) 
(defrule quash0
(declare (salience 10))
(id-root ?id quash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xamana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quash.clp    quash0   "  ?id "  xamana_kara )" crlf))
)


