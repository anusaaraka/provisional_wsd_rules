;@@@ Added by 14anu-ban-04 (18-03-2015)
;Financial matters are her domain.               [oald]
;आर्थिक विषय उसका कार्यक्षेत्र हैं .                         [self]
;I don't do any gardening - that's my wife's domain.                    [cald]
;मैं कुछ बागबानी नहीं करता हूँ- वह मेरी पत्नी का कार्यक्षेत्र है .                               self]
(defrule domain1
(declare (salience 20))
(id-root ?id domain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(subject-subject_samAnAXikaraNa ?id1 ?id)(viSeRya-RaRTI_viSeRaNa ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryakRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " domain.clp 	domain1   "  ?id "  kAryakRewra )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (18-03-2015)    
;Are you still looking for some kind of job in the political domain.      [cald]
;क्या आप अभी भी राजनैतिक क्षेत्र में   किसी न किसी प्रकार का काम  ढूँढ़ रहे हैं?                     [self]        
(defrule domain0
(declare (salience 10))
(id-root ?id domain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " domain.clp 	domain0   "  ?id "  kRewra )" crlf))
)
