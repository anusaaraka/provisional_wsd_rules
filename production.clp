;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################



;@@@Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;In the Science department out of 82 published papers, based on original investigation, 
;60 were the production of the teachers, 14 were the result of joint efforts of professors and 
;Hundred Years of the University of Calcutta, p, 194-195. students, and 8 were the result of independent 
;research by students.[gyannidhi]
;विज्ञान विभाग में ऐसे 82 प्रकाशित शोद-पत्रों में से जो कि मौलिक खोज पर आधारित थे, 60 अध्यापकों के परिश्रम का फल थे, 
;14 प्रोफेसरों और छात्रों के संयुक्त प्रयासों का परिणाम थे और 8 छात्रों के स्वतंत्र शोध का नतीजा थे। 
(defrule production0
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 teacher|employee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SarmaPala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production0  "  ?id "  SarmaPala )" crlf))
)


;@@@Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;A school production of 'Romeo and Juliet'. [Cambridge]
;रोमियो जूलियट का स्कूल प्रदर्शन . 
(defrule production1
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production1  "  ?id "  praxarSana )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;The breakneck production of naval vessels during World War II.	[mw]
; विश्व युद्ध II के दौरान नौसैनिक जहाजो का अन्धाधुन्ध निर्माण.	[self]

(defrule production3
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 vessel)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id2 naval)
=>	
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production3  "  ?id "  nirmANa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;He wants a career in film production. [oald]
;वह फिल्म के निर्माण में कैरियर बनाना चाहता है . 	[Manual]

(defrule production4
(declare (salience 5001))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 film)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production4  "  ?id "  nirmANa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;She’s currently performing in a new production of ‘King Lear’. [oald]
;वह 'किङ लिर' के एक नये निर्माण में आज-कल कार्य कर रही है . 		[Manual]

(defrule production5
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production5  "  ?id "  nirmANa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;Every year the school puts on a musical production. 	[oald]
;प्रत्येक वर्ष विद्यालय में एक संगीत  प्रस्तुति रखी जाती है .	[Manual] 

(defrule production6
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 musical)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production6  "  ?id "  praswuwi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;These substances are not allowed in organic production.  	[oald]
;इन पदार्थों ने कार्बनिक की पैदावार होने नहीं दी हैं . 	[Manual] 

(defrule production7
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 organic)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExAvAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production7  "  ?id "  pExAvAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;This is not a big studio production.  [oald]
;यह एक बीग स्टुडियो का निर्माण नहीं है .		[Manual]

(defrule production8
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id2)
(id-root ?id2 big)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 studio)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production8 "  ?id "  nirmANa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-04-2015)
;Naples surpassed Rome as a centre/center of artistic production in the 18th century. 	[oald]
;नॆपल्ज 18th शताब्दी में  रोम को कलात्मक प्रस्तुतिकरण का केंद्र बना दिया .		[Manual] 

(defrule production9
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 artistic)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwikaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production9 "  ?id "  praswuwikaraNaa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-04-2015)
;Sand is used in the production of glass.	[oald]
;रेत काँच के निर्माण में उपयोग की जाती है . 	[Manual]
(defrule production10
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
(id-root ?id1 glass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production10  "  ?id "  nirmANa )" crlf))
)


;@@@ Added by 14anu-ban-09 on (11-04-2015)
;We saw a stage production of the novel. 	[mw]
;हमने उपन्यास की एक मञ्च प्रस्तुति को देखा . 	[Manual]
(defrule production11
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 stage)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production11  "  ?id "  praswuwi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-04-2015)
;The director wants her in his next production.  [mw]
;निदेशक उसे अगले निर्माण में लेना चाहता है . 	[Manual]
(defrule production12
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 director)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production12  "  ?id "  nirmANa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-04-2015)
;A movie going into production. [freedictionary.com]
;चलचित्र का निर्माण प्रारंभ कर दिया .  [Manual]
(defrule production13
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI ?id1  ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id2 ?id1)
(id-root ?id2 movie)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production13  "  ?id "  nirmANa )" crlf))
)

  
;@@@ Added by 14anu-ban-09 on (11-04-2015)
;Shakespeare's production of poetry was enormous. [vocabulary.com]
;शेक्सपियर की काव्य की रचना महान थी .	[Manual]
(defrule production14
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
(id-root ?id1 poetry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id racanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production14  "  ?id "  racanA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-04-2015)
;The production of crop was up in the second quarter. [vocabulary.com-modified]
;फसल की पैदावार दूसरे चौथाई भाग में बढ़ गई थी . 	[Manual]
(defrule production15
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
(id-root ?id1 crop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExAvAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production15  "  ?id "  pExAvAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-04-2015)
;The body’s production of hormones changed according to the age. [http://dictionary.reference.com-modified]
;शरीर के हारमोन की उत्पत्ति उम्र के अनुसार बदलाती रहती हैं . 	[Manual]
(defrule production16
(declare (salience 5000))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI   ?id ?id1)
(id-root ?id1 hormone)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production16  "  ?id "  uwpawwi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;Demonstrated ability to collaborate successfully as a member of a production team and as a member of an academic program. [http://dictionary.reference.com] 
;निर्माण दल और शैक्षिक कार्यक्रम के एक सदस्य के जैसा सफलतापूर्वक सहयोग करने की योग्यता दर्शाइए .	 [Manual]
(defrule production17
(declare (salience 5001))
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 team)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production17  "  ?id "  nirmANa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by Sonam Gupta MTech IT Banasthali 4-2-2014
;We need to increase production by 20%. [OALD]
;हमें उत्पादन२०% से बढाने की आवश्यकता है . 
(defrule production2
(id-root ?id production)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpAxana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  production.clp 	production2  "  ?id "  uwpAxana )" crlf))
)

