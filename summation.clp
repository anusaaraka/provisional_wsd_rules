;@@@ Added by 14anu-ban-01 on (05-12-2014)
;This is a fair summation of the discussion.[self:with reference to oald]
;यह परिचर्चा का एक स्पष्ट अन्तिम अभिवचन है. [self]
(defrule summation0
(declare (salience 0))
(id-root ?id summation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwima_aBivacana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  summation.clp 	summation0   "  ?id "  anwima_aBivacana )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-12-2014)
;This summation can be converted to an integral in most cases.[NCERT corpus]
;अधिकांश प्रकरणों में सङ्कलन को समाकलन में परिवर्तित कर लेते हैं. [NCERT corpus]
;अधिकांश प्रकरणों में यह सङ्कलन समाकलन में परिवर्तित किया जा सकता है.[NCERT corpus:improvised]
(defrule summation1
(declare (salience 0))
(id-root ?id summation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 convert)
(kriyA-subject ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkalana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  summation.clp 	summation1   "  ?id "  saMkalana )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-12-2014)
;Sigma,which represents summation,looks like a sideways M.[self:with reference to COCA]
;सिग्मा जो कि संकलन को दर्शाता है,तिरछे M की तरह दिखता है.[self]
(defrule summation2
(declare (salience 0))
(id-root ?id summation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 represent)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkalana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  summation.clp 	summation2   "  ?id "  saMkalana )" crlf))
)
