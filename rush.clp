
(defrule rush0
(declare (salience 5000))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush0   "  ?id "  BIdZa )" crlf))
)

;"rush","N","1.BIdZa"
;We left in the afternoon to avoid the evening rush.
;--"2.hadZabadZI/jalxI_meM"
;She made a rush for the shop.
;Don't work in a rush though there is a pile of files.
;She is in a mad rush so I can't stop her.
;--"3.baDZawI_mAzga"
;The unexpected heavy monsoon caused a rush on raincoats.
;
(defrule rush1
(declare (salience 4000))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxabAjZI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush1   "  ?id "  jalxabAjZI_kara )" crlf))
)

;"rush","VI","1.jalxabAjZI_karanA"
;Don't rush, take your own time to get ready.
;I'm afraid without a single thought she rushed into marriage.
;

;@@@ Added by Anita-9.1.2014
;The action of the rushing water cleans the gully. [By mail]
;तेज़ी से बहते हुये पानी की क्रिया नाली साफ करती है ।
(defrule rush2
(declare (salience 5000))
(id-root ?id rush)
(id-word ?id rushing)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 water)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wejZI_se_bahawe_hue_pAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush2   " ?id "  " ?id1 " wejZI_se_bahawe_hue_pAnI )" crlf))
)

;@@@ Added by Anita-9.1.2014
;I stretched out and listened to the sound of the rushing stream. [Cambridge-dictionary]
;मैं बाहर निकला और तेज़ी से बहते हुए झरने की आवाज़ सुनी ।
(defrule rush3
(declare (salience 5010))
(id-root ?id rush)
(id-word ?id rushing)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 stream)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wejZI_se_bahawI_huI_Jarane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush3   " ?id "   " ?id1 " wejZI_se_bahawe_hue_Jarane )" crlf))
)


;@@@ Added by Anita-7.7.2014
;The people of the community rushed to the spot.
;मुहल्ले वाले घटना स्थल की ओर तेजी से भागे ।
(defrule rush4
(declare (salience 5050))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 spot)
(kriyA-to_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZI_se_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush4   "  ?id "  wejZI_se_BAga )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-03-2015)
;She is in a mad rush so I cant stop her.[hinkhoj]
;वह उत्तेजित जल्दी में है इसलिए मैं उसको रोक नही साकत हूँ  . [manual]
(defrule rush5
(declare (salience 6000))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush5   "  ?id "   jalxI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-03-2015)
;She made a rush for the shop.[hinkhoj]
;उसने दुकान के लिए  झटपट दौड़  लागाई . [manual]
(defrule rush6
(declare (salience 6100))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 make)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Jatapata_xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush6   "  ?id "   Jatapata_xOdZa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-03-2015)
;The unexpected heavy monsoon caused a rush on raincoats.[hinkhoj]
;नपेक्षित भारी मानसूनी बरसाती के बढ़ती माँग का कारण बनी . [manual]
(defrule rush7 
(declare (salience 6200))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 cause)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  baDZawI_mAzga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush7   "  ?id "  baDZawI_mAzga )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-03-2015)
;He rushed into the room.[hinkhoj]
;उसने कमरे में दौडकर प्रवेश करा  . [manual]
(defrule rush8 
(declare (salience 6300))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  xOdakara_praveSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush8   "  ?id "   xOdakara_praveSa_kara)" crlf))
)


;$$$ Modified by 14anu-ban-11 on (30-04-2015)
;@@@ Added by 14anu-ban-10 on (27-03-2015)
;The product was rushed through without adequate safety testing.[oald]
;बिना उत्पाद  पर्याप्त सुरक्षा परीक्षण मे से शीघ्रता से पारित किया गया था . [manual]
(defrule rush9 
(declare (salience 6400))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 through)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id   se_SIGrawA_se_pAriwa_kara))  ;Commented by 14anu-ban-11 on (30-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng  ?id  ?id1  se_SIGrawA_se_pAriwa_kara))   ;Added by 14anu-ban-11 on (30-04-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush9   "  ?id "   se_SIGrawA_se_pAriwa_kara)" crlf))       ;Commented by 14anu-ban-11 on (30-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rush.clp  rush9  "  ?id "  " ?id1 "  SAhI_aMxAja  )" crlf));Added by 14anu-ban-11 on (30-04-2015)
)





;@@@ Added by 14anu-ban-10 on (27-03-2015)
;The editors rushed out an item on the crash for the late news.[oald]
;सम्पादकों ने देरे से आनेवाला समाचार के लिए धमाके के विषय पर  छापा . [manual]
(defrule rush10 
(declare (salience 6500))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id    CApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush10   "  ?id "    CApa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-03-2015)
;Don’t travel at rush hour.[oald]
;व्यस्त समय में यात्रा मत कीजिए.[manual] 
(defrule rush11 
(declare (salience 6600))
(id-root ?id rush)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 hour)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   vyaswa_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rush.clp 	rush11   "  ?id "   vyaswa_samaya)" crlf))
)
