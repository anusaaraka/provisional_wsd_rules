;@@@ Added by 14anu-ban-02 (03-12-2014)
;Sentence: We make an assumption .[karan singla]
;Translation: हम एक धारणा बना लेते हैं .[manual]
(defrule assumption0 
(declare (salience 0)) 
(id-root ?id assumption) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id XAraNA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  assumption.clp  assumption0  "  ?id "  XAraNA )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (03-12-2014)
;sentence:This fact is based on assumption .[karan singla]
;translation:यह तथ्य अनुमान पर आधारित है ।[karan singla]
(defrule assumption1 
(declare (salience 100)) 
(id-root ?id assumption) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-on_saMbanXI  ?id1 ?id)
(id-root ?id1 base)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anumAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  assumption.clp  assumption1  "  ?id "  anumAna )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (03-12-2014)
;By making this adhoc assumption, Huygens could explain the absence of the backwave.[ncert]
;इस तदर्थ कल्पना से हाइगेंस पश्च तरङ्गों की अनुपस्थिति को समझा पाए.[ncert]
(defrule assumption2 
(declare (salience 100)) 
(id-root ?id assumption) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id ?id1)
(id-word ?id1 adhoc)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kalpanA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  assumption.clp  assumption2  "  ?id "  kalpanA )" crlf)) 
)
 
;@@@Added by 14anu-ban-02(19-02-2015)
;If we consider points at arbitrary distance from the surface of the earth, the result just derived is not valid since the assumption that the gravitational force mg is a constant is no longer valid.[ncert 11_01]
;यदि हम पृथ्वी के पृष्ठ से यादृच्छिक दूरियों के बिंदुओं पर विचार करें तो उपरोक्त परिणाम प्रामाणिक नहीं होते क्योंकि तब यह मान्यता कि गुरुत्वाकर्षण बल mg अपरिवर्तित रहता है वैध नहीं है.[ncert]
(defrule assumption3 
(declare (salience 100)) 
(id-root ?id assumption) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-since_saMbanXI  ?id1 ?id)
(id-root ?id1 valid)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id mAnyawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  assumption.clp  assumption3  "  ?id "  mAnyawA )" crlf)) 
) 
