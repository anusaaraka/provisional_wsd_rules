
;@@@ Added by 14anu-ban-09 on (03-02-2015)
;Manipur achieved the pride being an independent state in January 1972 .	[Total_tourism]
;मणिपुर को स्वतंत्र राज्य होने का गौरव जनवरी 1972 में हासिल हुआ .				[Total_tourism]

(defrule pride2
(declare (salience 5000))
(id-root ?id pride)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
;(id-root ?id2 Manipur) 		;commented by 14anu-ban-09 on (06-02-2015)
(id-cat_coarse ?id2 PropN)		;added by 14anu-ban-09 on (06-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gOrava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pride.clp 	pride2   "  ?id "  gOrava )" crlf))
)

;---------------------- Default Rules -------------------------

;"pride","N","1.aBimAna"
;The pride of our nation was Bapuji.
(defrule pride0
(declare (salience 5000))
(id-root ?id pride)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pride.clp 	pride0   "  ?id "  aBimAna )" crlf))
)

;"pride","V","1.garva_karanA"
;Pride yourself on your achievements.
(defrule pride1
(declare (salience 4900))
(id-root ?id pride)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pride.clp 	pride1   "  ?id "  garva_kara )" crlf))
)

;"pride","V","1.garva_karanA"
;Pride yourself on your achievements.
;"pride","N","1.aBimAna"
;The pride of our nation was Bapuji.
;--"2.AwmABimAna"
;Don't hurt one's pride.
;--"3.SeroM_kA_samUha"
;There is a pride in the nearby  forest.
;
