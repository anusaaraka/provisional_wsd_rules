;@@@ Added by 14anu-ban-06 (25-02-2015)
;I feel horny after I have been drinking. (COCA)
;मैं कामोत्तेजित महसूस करता हूँ जब मैं शराब पीता रहा होता हूँ . (manual)
(defrule horny0
(declare (salience 0))
(id-root ?id horny)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAmowwejiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  horny.clp   horny0   "  ?id "  kAmowwejiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-02-2015)
;Birds have horny beaks. (cambridge)
;पक्षियों की चोंच सख्त होती हैं . (manual)
(defrule horny1
(declare (salience 2000))
(id-root ?id horny)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 beak)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saKwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  horny.clp   horny1   "  ?id "  saKwa )" crlf))
)
