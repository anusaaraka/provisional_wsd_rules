;@@@ Added by 14anu-ban-01 on (03-02-2015)
;The ball should be able to swivel freely in the socket.[oald]
;गेंद सॉकेट में आसानी से घूमनी चाहिये.[self]
(defrule swivel1
(declare (salience 1000))
(id-root ?id swivel)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swivel.clp 	swivel1   "  ?id "  GUma )" crlf))
)

;@@@ Added by 14anu-ban-01 on (03-02-2015)
;The figure swiveled around so that the finger of the statuette on it always pointed south.[NCERT corpus]
;रथ पर लगी हुई प्रतिमा इस तरह घूम जाती थी कि उसकी अँगुली हमेशा दक्षिण की ओर सङ्केत करे.[NCERT corpus]
(defrule swivel2
(declare (salience 1000))
(id-root ?id swivel)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 around)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GUma_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  swivel.clp 	swivel2   "  ?id "  " ?id1 "  GUma_jA )" crlf))
)

;----------------------------- Default Rules ---------------------
;@@@ Added by 14anu-ban-01 on (03-02-2015)
;I swiveled/swivelled my head to peer at him.[oald]
;मैंने उसे देखने के लिए अपना सिर घुमाया.[self]
(defrule swivel0
(declare (salience 0))
(id-root ?id swivel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swivel.clp   swivel0   "  ?id "  GumA )" crlf))
)

