
(defrule estimate0
(declare (salience 5000))
(id-root ?id estimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  estimate.clp 	estimate0   "  ?id "  anumAna )" crlf))
)

;default_sense && category=noun	mUlya_kA_anumAna	0
;"estimate","N","1.mUlya_kA_anumAna"
;An estimate of what it would cost
;Many factors are involved in any estimate of human life
;He got an estimate from the car repair shop
;
(defrule estimate1
(declare (salience 0000))  ;salience reduced by 14anu-ban-04 
(id-root ?id estimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxAjZa_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  estimate.clp 	estimate1   "  ?id "  aMxAjZa_lagA )" crlf))
)

;"estimate","VT","1.aMxAjZa_lagAnA"
;I estimate this chicken to weigh at three pounds
;

;@@@ Added by 14anu-ban-04 on 27-08-2014
;It is possible to estimate the sizes of molecules.   [NCERT-CORPUS]
;अणुओं की आमाप का आकलन करना सम्भव है.
(defrule estimate2
(declare (salience 10))
(id-root ?id estimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akalana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  estimate.clp 	estimate2   "  ?id "  Akalana_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 on 27-08-2014
;For a given set-up, these errors may be estimated to a certain extent and the necessary corrections may be applied to the readings.  [NCERT-CORPUS]
;किसी भी दी गई व्यवस्था के लिए, इन त्रुटियों का कुछ निश्चित सीमाओं तक आकलन किया जा सकता है और पाठ्याङ्कों को तदनुसार संशोधित किया जा सकता है. [NCERT-CORPUS]
(defrule estimate3
(declare (salience 20))
(id-root ?id estimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-to_saMbanXI ?id ?id1)(kriyA-for_saMbanXI ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akalana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  estimate.clp 	estimate3  "  ?id "  Akalana_kara )" crlf))
)
