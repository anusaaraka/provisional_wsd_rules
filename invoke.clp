;@@@ Added by 14anu-ban-06  (06-10-2014)
;His name was invoked as a symbol of the revolution.(OALD)
;उसका नाम क्रांति के प्रतीक के रूप में  स्मरण किया गया था .(manual)
(defrule invoke1
(declare (salience 2000))
(id-root ?id invoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI ?id ?id1)
(id-root ?id1 symbol)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invoke.clp 	invoke1   "  ?id "  smaraNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06  (06-10-2014)
;Generally , the protection afforded by these rights is invoked to challenge the validity of externment or deportation orders which go to curtail the two freedoms .(Parallel corpus)
;आमतौर पर , इन अधिकारों द्वारा दिए गए संरक्षण का बाह्यकरण1 या देशनिकालेऑ के उन आदेशों को चुनौती देने के लिए उपयोग  किया जाता है जिनसे दोनों स्वतंत्रताओं में कटौती हो जाती है .(Parallel corpus)
;These clauses have not been invoked to protect civilians in the DRC.(COCA)
;ये उपवाक्य डी आर सी में असैनिक कर्मचारियो की रक्षा करने में  उपयोग किये जाते हैं.(manual)
(defrule invoke2
(declare (salience 2100))
(id-root ?id invoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 challenge|deal|protect|avoid|explain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invoke.clp 	invoke2   "  ?id "  upayoga_kara )" crlf))
)


;@@@ Added by 14anu-ban-06  (06-10-2014)
;In 1910 , District Collector of Hamirpur invoked him because of his creation Soje Vatan Lament of Nation , and blamed him for instigating to the people .(Parallel corpus)
;१९१० में उनकी रचना सोजे - वतन राष्ट्र का विलाप के लिए हमीरपुर के जिला कलेक्टर ने तलब किया और उन पर जनता को भड़काने का आरोप लगाया । (Parallel corpus)
(defrule invoke3
(declare (salience 2100))
(id-root ?id invoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 him|her|them)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walaba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invoke.clp 	invoke3   "  ?id "  walaba_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06  (06-10-2014)
;The Spirit of Evening which he invoked in the first poem , vague and mysterious , has now assumed a concrete and familiar image .(Parallel corpus)
;इस कविता में संध्या का  जिस मनोदशा में आह्वान किया गया था , वह अस्पष्ट और रहस्यमय था लेकिन धीरे धीरे स्पष्ट और सुपरिचित बिंबों में ढलने लगता है .(Parallel corpus)
;Humiliated and wounded in the core of her being , Chitra broke her bow into two and invoked by her penance Madana , the god of love. (parallel corpus)
;अपनी अहंमन्यता के बोध से अपमानित और आहत होकर चित्रा ने अपना धनुष दो टुकड़ों में तोड़कर फेंक दिया और अपनी तपस्या द्वारा मदन का आह्वान किया .(Parallel corpus)
(defrule invoke0
(declare (salience 0))
(id-root ?id invoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AhvAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invoke.clp   invoke0   "  ?id "  AhvAna_kara )" crlf))
)

