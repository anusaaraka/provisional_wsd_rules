
;@@@Added by 14anu-ban-07,(09-04-2015)
;There is often a kernel of truth in what they say.(cambridge)
; वे जो कहते हैं  अक्सर उसमे सच का एक मुख्य भाग  होता है . (manual)
(defrule kernel1
(declare (salience 1000))
(id-root ?id kernel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 truth|argument)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kernel.clp 	kernel1   "  ?id "  muKya_BAga )" crlf))
)

;@@@Added by 14anu-ban-07,(09-04-2015)
;A typical OS kernel already provides a wide range of protection mechanisms.(coca)
;एक विशिष्ट ओ एस कर्नल पहले से ही रक्षा यन्त्रविन्यास के  विभिन्न प्रकार  देता है . (manual)
(defrule kernel2
(declare (salience 1100))
(id-root ?id kernel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 OS)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karnala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kernel.clp 	kernel2   "  ?id "  karnala )" crlf))
)

;---------------------------------- Default Rules -----------------------------

;@@@Added by 14anu-ban-07,(09-04-2015)
;Apricot kernels.(oald)
;खूबानी की गुठली.(manual)
(defrule kernel0
(id-root ?id kernel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAnA/guTalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kernel.clp   kernel0   "  ?id "  xAnA/guTalI )" crlf))
)

