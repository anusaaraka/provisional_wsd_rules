;@@@ Added by 14anu-ban-11 on (05-03-2015)
;Her hair was styled to look like a 50s movie star.(cald)
;उसके केश एक 50s फिल्म सितरे की तरह दिखाने के लिये बनाए गये थे . (self)
(defrule style2
(declare (salience 20))
(id-root ?id style)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  style.clp 	style2   "  ?id "  banA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (07-03-2015)
;A fine example of Gothic style.(oald)
;एक सुन्दर उदाहरण  गथिक भाषा सम्बन्धी शैली का . (self)
(defrule style3
(declare (salience 30))
(id-root ?id style)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 example)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SElI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  style.clp 	style3   "  ?id "  SElI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (07-03-2015)
;He wouldn't try to mislead you-it's not his style.(cald)
;वह आपको पथभ्रष्ट करने का प्रयास नहीं करेगा- यह उसका तरीका नहीं है . (self)
(defrule style4
(declare (salience 40))
(id-root ?id style)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-vAkyakarma  ?id ?id1)
(kriyA-kriyArWa_kriyA  ?id1 ?id2)
(id-root ?id1 try)
(id-root ?id2 mislead)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  style.clp 	style4   "  ?id "  warIkA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (05-03-2015)
;I like your style. (oald)
;मुझे आपका अन्दाज  पसन्द है .  (self)
(defrule style0
(declare (salience 00))
(id-root ?id style)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  style.clp    style0   "  ?id "  aMxAjZa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (05-03-2015)
;He styled himself Major Carter. (oald)
;उसने स्वयं को मेजर कॉर्टर नाम दिया .  (self)
(defrule style1
(declare (salience 10))
(id-root ?id style)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAma_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  style.clp    style1  "  ?id "  nAma_xe)" crlf))
)

