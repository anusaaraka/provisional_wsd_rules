



(defrule pluck0
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAhasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck0   "  ?id "  sAhasa )" crlf))
)

;"pluck","N","1.sAhasa"
;The boy showed his pluck in capturing the decoits.
;



;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;Police plucked a drowning girl from the river yesterday.  [OALD]
;पुलीस ने कल नदी में एक डूबती हुई लडकी को बचाया . 
(defrule pluck1
(declare (salience 4900))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kAlavAcI  ?id ?)
(kriyA-object  ?id ?)
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacAyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck1   "  ?id "  bacAyA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (20-02-2015)
;We should not pluck the flowers from the public park. 	[same clp file]
;हमें सार्वजनिक उद्यान से फूल नहीं तोडने चाहिए . 			[Anusaaraka]
;Changed meaning from 'wofA' to 'woda'.
;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;I plucked an orange from the tree. [OALD]
;मैंने पेड से एक नारङ्गी तोङा . 
(defrule pluck2
(declare (salience 4800))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 tree|plant|flower|park)		;added 'park' by 14anu-ban-09 on (20-02-2015)
(kriyA-from_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id woda))	;changed meaning from 'wofA' to 'woda' by 14anu-ban-09 on (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck2   "  ?id "  woda )" crlf))	;changed meaning from 'wofA' to 'woda' by 14anu-ban-09 on (20-02-2015)
)



;$$$Modified by Sonam Gupta MTech IT Banasthali 2013
;A helicopter plucked him from the sea. [Cambridge]
;हेलिकोप्टर ने समुद्र से उसको खीँचा . 
(defrule pluck4
(declare (salience 4600))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KIzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck4   "  ?id "  KIzcA )" crlf))
)


;"pluck","V","1.wodanA"
;We should not pluck the flowers from the public park.
;--"2.ocanA"
;The cheff plucked && prepared the chicken to cook.
;--"3.KIMcanA"
;I plucked && showed the licence to the harassing traffic constable.
;--"4.bacAnA"
;the fisherman who were being drowned in the sea were plucked by airforce.
;

;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;Plucking can be done from the first year. [Gyannidhi]
;पत्ती तोङना प्रथम वर्ष से किया जा सकता है . 
(defrule pluck5
(declare (salience 5500))
(id-word ?id plucking)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawwI_wofanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck5   "  ?id "  pawwI_wofanA )" crlf))
)

;@@@Added by Sonam Gupta MTech IT Banasthali 25-1-2014
;I have not yet plucked up the courage to ask her. [Cambridge]
;मैं अब तक उससे पूछने का साहस नहीं जुटा पाया हूँ . 
(defrule pluck6
(declare (salience 5500))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 courage)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jutA_pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck6   "  ?id "  jutA_pA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning from 'sAhasa_juta' to 'jutA_le'
;@@@ Added by 14anu20 ON 05.07.2014
;I finally have plucked the courage to answer. ;wrong example sentence.So, removed it. By 14anu-ban-09 on (01-01-2015)
;मैं अन्ततः उत्तर देने के लिए साहस जुटा चुका हूँ . 
;I finally plucked up the courage to ask her for a date. [OALD] ;added by 14anu-ban-09 on (01-01-2015)
;मैंने अन्ततः उससे  मुलाक़ात के विषय में पुछने के लिये साहस जुटा लिया.

(defrule pluck7
(declare (salience 5550)) ;salience increased '5000' to '5550'
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(id-root =(+ ?id 1) up)	;commented by 14anu-ban-09 
(kriyA-upasarga  ?id ?id2) 	;added by 14anu-ban-09 
(id-root ?id2 up) 		;added by 14anu-ban-09 
(kriyA-object  ?id ?id1)
(id-root ?id1 courage)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) ?id1  sAhasa_juta)) ;commented by 14anu-ban-09 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 jutA_le)) ;added by 14anu-ban-09  
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck7  "  ?id "  " (+ ?id 1) " " ?id1 "   sAhasa_juta  )" crlf)) ;commented by 14anu-ban-09 
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck7  "  ?id "  " ?id2 "   jutA_le  )" crlf)) ;added by 14anu-ban-09 
)

;@@@ Added by 14anu20 ON 05.07.2014
;Police plucked a drowning girl from the river yesterday.
;पुलीस ने कल नदी से एक डूबती हुई लडकी को निकाला . 
(defrule pluck8
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-from_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck8   "  ?id "  nikAla )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning 'bajA' to 'Ceda'
;@@@ Added by 14anu20 ON 05.07.2014
;He took the guitar and plucked at the strings.
;उसने गिटार लिया और बजाया 
;उसने गिटार लिया और उसके तार छेडे. [Self] ;Added by 14anu-ban-09 

(defrule pluck9
(declare (salience 5050)) ;salience increased '5000' to '5050' by 14anu-ban-09 on (01-01-2015)
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 string)
(id-root =(+ ?id 1) at)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) ?id1  bajA)) ;commented by 14anu-ban-09 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  Ceda)) ;Added by 14anu-ban-09 
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck9  "  ?id "  " (+ ?id 1) " " ?id1 "   bajA  )" crlf)) ;commented by 14anu-ban-09 
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck9  "  ?id "  " (+ ?id 1) "   Ceda  )" crlf)) ;Added by 14anu-ban-09 
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning from 'hAWoM_se_noca' to 'Kica_kara'.
;@@@ Added by 14anu20 ON 05.07.2014
;She plucked out a hair.
;उसने एक  कैश हाथों से नोचा 
;उसने एक बाल खिच कर निकाला. [Self] ;Added by 14anu-ban-09 on (01-01-2015)

(defrule pluck10
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-out_saMbanXI  ?id ?id1)
;(id-root =(+ ?id 1) out) ;commented by 14anu-ban-09 on (01-01-2015)
(id-root ?id1 hair|eyebrow)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  hAWoM_se_noca)) ;commented by 14anu-ban-09 on (01-01-2015)
(assert (id-wsd_root_mng ?id   Kica_kara)) ;Added by 14anu-ban-09 on (01-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " pluck.clp  pluck10  "  ?id "      Kica_kara  )" crlf)) ;Added by 14anu-ban-09 on (01-01-2015)
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck10  "  ?id "  " (+ ?id 1) "    hAWoM_se_noca  )" crlf)) ;commented by 14anu-ban-09 on (01-01-2015)
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning 'Jatake_se_KIMca' to 'kaI_bAra_KIMca'.
;@@@ Added by 14anu20 ON 05.07.2014
;The wind plucked at my jacket.
;हवा ने मेरा धातु आवरण झटके से खींचा . 
;हवा ने मेरा धातु आवरण कई बार खींचा . [Self] ;added by 14anu-ban-09

(defrule pluck11
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root =(+ ?id 1) at)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) kaI_bAra_KIMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pluck.clp  pluck11  "  ?id "  " (+ ?id 1) "  kaI_bAra_kIMca  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-02-2015)
;She was plucked from obscurity to instant stardom. 	[oald]
;उसे शीग्र सितारापन के अंधकार से निकाला गया था.			[self]
(defrule pluck12
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ? ?id1)
(id-root ?id1 obscurity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAlA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck12  "  ?id "  nikAlA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-02-2015)
;The chef plucked & prepared the chicken to cook. 	[same clp file]
;प्रधान रसोइये ने ओचा और मुर्गी को पकाने के लिय तैयार किया . 		[self]
(defrule pluck13
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 chef)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ocA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck13  "  ?id "  ocA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-02-2015)
;The fisherman who were being drowned in the sea were plucked by airforce.  	[same clp file]
;वह मछुआरे जो समुद्र में डूबे रहे थे वे वायु सेना के द्वारा बचाए गये थे . 		[self]
(defrule pluck14
(declare (salience 5000))
(id-root ?id pluck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-word ?id1 airforce)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacAe_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pluck.clp 	pluck14  "  ?id "  bacAe_jA )" crlf))
)

