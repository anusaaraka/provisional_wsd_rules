;@@@ Added by Anita--25-07-2014
;I question the wisdom of giving a child so much money. [oxford learner's dictionary]
;मैंने बच्चे को इतना पैसा देने की अक्लमंदी  पर सवाल किया ।
(defrule wisdom0
(declare (salience 5000))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aklamaMxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom0   "  ?id "  aklamaMxI )" crlf))
)

;@@@ Added by Anita--25-07-2014
;The collective wisdom of the Native American people, is very vast. [oxford learner's dictionary]
;अमेरिका के निवासी लोगों का ज्ञान सामूहिकरूप से बहुत विशाल है ।
;He's got a weekly radio programme in which he dispenses wisdom on a variety of subjects. [cambridge ;dictionary]
;वह रेडियो पर साप्ताहिक कार्यक्रम करता है जिसमें वह विभिन्न विषयों पर ज्ञान प्रदान करता है ।
(defrule wisdom1
(declare (salience 4900))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id ?)(kriyA-object  ?kri ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jFAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom1   "  ?id "  jFAna )" crlf))
)

;@@@ Added by Anita--25-07-2014
;One certainly hopes to gain a little wisdom as one grows older. [cambridge dictionary]
;जैसे-जैसे  किसी की उम्र  बढ़ती है तो आशा की जाती है कि थोड़ी अक्ल भी बढ़ेगी ।
(defrule wisdom2
(declare (salience 4800))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 little)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id akla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom2   "  ?id "  akla )" crlf))
)

;She's having her wisdom teeth out. [cambridge dictionary]
;वह अपनी अक्ल दाढ़ निकलवा रही है ।

;@@@ Added by Anita--26-07-2014
;I tend to doubt the wisdom of separating a child from its family whatever the circumstances . [cambridge dictionary]
;कोई भी परिस्थितियाँ हों किसी भी बच्चे को उसके परिवार से अलग करने के विवेक पर मुझे शंका है ।
(defrule wisdom3
(declare (salience 5100))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 separating)
(kriyA-object  ?kri ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viveka))
(assert  (id-wsd_viBakwi   ?id  para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom3   "  ?id "  viveka )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  wisdom.clp     wisdom3   "  ?id "  para )" crlf))
)

;@@@ Added by Anita--26-07-2014
;Did we ever stop to question the wisdom of going to war ? [cambridge dictionary]
;क्या हमने कभी भी युद्ध में जाने की समझदारी के प्रश्न को रोका है ?
(defrule wisdom4
(declare (salience 5200))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 going)
(id-word ?id2 war)
(and(viSeRya-of_saMbanXI  ?id ?id1)(kriyA-to_saMbanXI  ?id1 ?id2))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJaxArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom4   "  ?id "  samaJaxArI )" crlf))
)

;@@@ Added by Anita--26-07-2014
;Before I went off to university my father gave me a few words of wisdom. [cambridge dictionary]
; विश्वविद्यालय के लिए मेरे रवाना होने से पहले मेरे पिता ने मुझसे व्यावहारिक ज्ञान के कुछ शब्द कहे ।
(defrule wisdom5
(declare (salience 5300))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 word)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAvahArika_jFAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom5  "  ?id "  vyAvahArika_jFAna )" crlf))
)

;@@@ Added by Anita--26-07-2014
;Popular wisdom has it that women are more emotional than men, but in my experience it often is not the case. [cambridge ;dictionary]
;सामान्य मान्यता यह है कि पुरुषों की तुलना में महिलाएं  अधिक भावुक होती हैं लेकिन मेरे अनुभव में अक्सर असलियत यह नहीं है ।
(defrule wisdom6
(declare (salience 5400))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 popular)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 sAmAnya_mAnyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wisdom.clp  wisdom6 "  ?id "  " ?id1 "  sAmAnya_mAnyawA )" crlf))
)

;##################################default-rule##############################
;@@@ Added by Anita--04-08-2014
;My friend's wisdom is very sharp . [self]
;मेरे दोस्त की बुद्धि बहुत तेज़ है ।
(defrule wisdom_default-rule
(declare (salience 0))
(id-root ?id wisdom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buxXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisdom.clp 	wisdom_default-rule  "  ?id "  buxXi )" crlf))
)
