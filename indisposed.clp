;@@@ Added by 14anu-ban-06 (08-04-2015)
;Large sections of the potential audience seemed indisposed to attend. (OALD)
;सामर्थ्य श्रोतागणो का अधिकतर भाग उपस्थित होने के लिए अनिच्छुक प्रतीत होता हैं. (manual)
(defrule indisposed1
(declare (salience 2000))
(id-root ?id indisposed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(saMjFA-to_kqxanwa ?id ?id1)
(id-root ?id1 attend|help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anicCuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indisposed.clp 	indisposed1   "  ?id "  anicCuka )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (08-04-2015)
;Sheila Jones is indisposed, so the part of the Countess will be sung tonight by Della Drake.(cambridge)
;शील जोन अस्वस्थ है, इसलिए आज रात काउंटेस का भाग डेल ड्रॆक के द्वारा गाया जाएगा . (manual)
(defrule indisposed0
(declare (salience 0))
(id-root ?id indisposed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asvasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indisposed.clp 	indisposed0   "  ?id "  asvasWa )" crlf))
)
