
(defrule stage0
(declare (salience 5000))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id staging )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aBinaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stage.clp  	stage0   "  ?id "  aBinaya )" crlf))
)

;"staging","N","1.aBinaya"
;An imaginative new staging of Macbeth.
;$$$ Modified by 14anu19
;stage2,3,4,5 can be combined into stage1.
;early stage
;प्रारंभिक अवस्था
(defrule stage1
(declare (salience 4900))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 different|early|earliest|earlier|various)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage1   "  ?id "  avasWA )" crlf))
)

;(defrule stage2
;(declare (salience 4800))
;(id-root ?id stage)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 early)
;(viSeRya-viSeRaNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id avasWA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage2   "  ?id "  avasWA )" crlf))
;)

;(defrule stage3
;(declare (salience 4700))
;(id-root ?id stage)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 earliest)
;(viSeRya-viSeRaNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id avasWA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage3   "  ?id "  avasWA )" crlf))
;)

;(defrule stage4
;(declare (salience 4600))
;(id-root ?id stage)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 earlier)
;(viSeRya-viSeRaNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id avasWA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage4   "  ?id "  avasWA )" crlf))
;)

;(defrule stage5
;(declare (salience 4500))
;(id-root ?id stage)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 various)
;(viSeRya-viSeRaNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id avasWA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage5   "  ?id "  avasWA )" crlf))
;)

;$$$ Modified by 14anu-ban-01 on (07-10-2014)
;@@@ Added by jagriti(4.3.2014)
;At the present stage of our understanding we know of four fundamental forces in nature which are described in brief here.[gyanidhi]
;अपनी समझ के वर्तमान चरण पर हम प्रकृति के चार मूल बलों को जानते हैं, जिनका यहाँ सङ्क्षेप में वर्णन किया गया है.
;Communication pervades all stages of life of all living creatures.[NCERT corpus(chp-15) added by 14anu-ban-01]
;सामाजिक व्यवहार/ संप्रेषण सभी सजीव प्राणियों के जीवन के सभी चरणों में व्याप्त होता है.[self]

(defrule stage6
(declare (salience 4400))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1);added by 14anu-ban-01 on (07-10-14)
(id-root ?id1 life|understanding);added by 14anu-ban-01 on (07-10-14)
;(id-root ?id1 present) ;commented by 14anu-ban-01 on (07-10-14)
;(viSeRya-viSeRaNa ?id ?id1) ;commented by 14anu-ban-01 on (07-10-14)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caraNa)); corrected meaning 'caraN' as 'caraNa' by 14anu-ban-01 on (07-10-14)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage6   "  ?id "  caraNa )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-11-2014)
;At some stage, for a certain positive potential of plate A, all the emitted electrons are collected by the plate A and the photoelectric current becomes maximum or saturates.(Ncert)
;पट्टिका A के एक निश्चित धन विभव के लिए एक ऐसी स्थिति आ जाती है जिस पर सभी उत्सर्जित इलेक्ट्रॉन पट्टिका A पर सङ्ग्रहीत हो जाते हैं तथा प्रकाश-विद्युत धारा उच्चतम हो जाती है अर्थात सन्तृप्त हो जाती है(Ncert)
(defrule stage8
(declare (salience 400))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id)
(kriyA-for_saMbanXI  ?id1 ?id2)
(id-root ?id2 potential)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage8   "  ?id "  sWiwi )" crlf))
)

;@@@ Added by14anu17
;The rash can appear anywhere on the body and in the early stages may be a little more than a small red spots in the skin .
;फुन्सी शरीर पर और प्रारम्भिक दौडों में कहीं भी दिख सकता है त्वचा में एक छोटे लाल धब्बों अधिक छोटा हो सकता है . 
(defrule stage9
(declare (salience 101))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-jo_samAnAXikaraNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage9   "  ?id "  xOdZa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-02-2015)
;Xi said that since his visit to India in September last year, relations between the two countries have entered into a new stage. [Mailed by Soma Mam]
;Xi ने कहा कि  उनके पिछले वर्ष के सितम्बर में  भारत के दौरे के बाद से ,दोनों  देशों के बीच सम्बन्ध एक नये स्तर पर पहुँचे है.[self: 14anu-ban-01 and 14anu-ban-07]
(defrule stage10
(declare (salience 100))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 enter|reach)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 relation|alliance|association)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage10   "  ?id "  swara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-02-2015)
;Moral orientation was not related to moral stage.[COCA]
;नैतिक अनुकूलन नैतिक स्थिति से सम्बन्धित नहीं था . [self]
(defrule stage11
(declare (salience 100))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 moral|ethical|embryonic)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage11  "  ?id "  sWiwi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-02-2015)
;We have seen that the interaction between two charges can be considered in two stages.[NCERT corpus]
;हमने यह देखा है कि दो आवेशों के बीच अन्योन्य क्रिया पर दो चरणों में विचार किया जा सकता है .[NCERT corpus]
(defrule stage12
(declare (salience 1000))	;salience increased from 100 to 1000 by 14anu-ban-01 on (17-02-2015)
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ? ?id)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage12   "  ?id "  caraNa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (07-09-2015)
;Before 1921, India was in the first stage of demographic transition. [SOCIAL SCIENCE]
;1921 से पहले, भारत जनसाङ्ख्यिकीय परिवर्तन के पहले  चरण में था . [MANUAL]
;The second stage of transition began after 1921 .[social]
;परिवर्तन का दूसरा चरण 1921 के बाद शुरु हुआ. [MANUAL]

(defrule stage13
(declare (salience 1001))	
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 first|second)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage13   "  ?id "  caraNa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (07-09-2015)
;However, neither the total population of India nor the rate of population growth at this stage was very high . [social science]
;हालाँकि, ना तो भारत की कुल जनसङ्ख्या ना ही इस समय तक जनसङ्ख्या वृद्धि की दर बहुत ज्यादा था . [manual]
(defrule stage14
(declare (salience 1002))	
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI  ? ?id)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage14   "  ?id "  samaya )" crlf))
)

;....default Rule...
(defrule stage7
(declare (salience 100))
(id-root ?id stage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stage.clp 	stage7   "  ?id "  maMca )" crlf))
)

;default_sense && category=noun	raMgamaMca	
;"stage","N","1.raMgamaMca"
;He is arranging the stage for the play.
;
;
