;@@@ Added by 14anu-ban-06 (22-04-2015)
; Illuminated manuscripts.(OALD)[parser no.- 13]
;सुसज्जित पाण्डुलिपियाँ . (manual)
(defrule illuminated1
(declare (salience 2000))
(id-word ?id illuminated)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 manuscript)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id susajjiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illuminated.clp 	illuminated1   "  ?id "  susajjiwa )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (22-04-2015)
;The doctor examined his x-ray on an illuminated screen. (OALD)
;डाक्टर ने प्रकाशित परदे पर अपने एक्स रे ले कर जाँच की . (manual)
(defrule illuminated0
(declare (salience 0))
(id-word ?id illuminated)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illuminated.clp 	illuminated0   "  ?id "  prakASiwa )" crlf))
)
