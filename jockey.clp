
(defrule jockey0
(declare (salience 5000))
(id-root ?id jockey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GudZa_xOdZa_kA_peSevara_GudasavAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jockey.clp 	jockey0   "  ?id "  GudZa_xOdZa_kA_peSevara_GudasavAra )" crlf))
)

;"jockey","N","1.GudZa_xOdZa_kA_peSevara_GudasavAra"
;Pesi Shroff is the most famous jockey in India.
;
(defrule jockey1
(declare (salience 4900))
(id-root ?id jockey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Taga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jockey.clp 	jockey1   "  ?id "  Taga )" crlf))
)

;"jockey","VT","1.TaganA"
;Some shopkeepers jockey foreign tourists to make money.
;

;@@@ Added by 14anu-ban-06 (27-02-2015)
;The bosses were eventually jockeyed into signing the union agreement.(cambridge)[parse no. 54]
;अन्त में बॉस से सङ्घ अनुबन्ध पर हस्ताक्षर चालाकी से कराए गये थे . (manual)
(defrule jockey2
(declare (salience 5300))
(id-root ?id jockey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cAlAkI_se_karA))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " jockey.clp	jockey2  "  ?id "  " ?id1 "  cAlAkI_se_karA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  jockey.clp       jockey2   "  ?id " se )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-02-2015)
;Since the death of the president, opposition parties and the army have been jockeying for power. (cambridge)
;अध्यक्ष की मृत्यु के उपरान्त, विपक्ष पार्टियाँ और सेना शक्ति को हथियाने की कोशिश कर रहे हैं . (manual)
(defrule jockey3
(declare (salience 5400))
(id-root ?id jockey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAne_kI_koSiSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jockey.clp 	jockey3   "  ?id "  haWiyAne_kI_koSiSa_kara )" crlf))
)
