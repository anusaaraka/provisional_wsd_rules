(defrule rocket0
(declare (salience 5000))
(id-root ?id rocket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roYketa)) ; modified 'bANa-havAI' as 'roYketa'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rocket.clp 	rocket0   "  ?id "  roYketa )" crlf))
)

;"rocket","N","1.bANa-havAI"
;Rockets are used to shoot missile or launch spacecraft. 
;
(defrule rocket1
(declare (salience 4900))
(id-root ?id rocket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acAnaka_bahuwa_baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rocket.clp 	rocket1   "  ?id "  acAnaka_bahuwa_baDa )" crlf))
)

;"rocket","V","1.acAnaka_bahuwa_baDanA"
;Nowadays land prices are rocketing.      

;@@@ Added by 14anu-ban-10 on (16-10-2014)
;Such a situation occurs in rocket propulsion.[ncert corpus]  
;EsI hI sWiwi roYketa - noxana meM howI hE.[ncert corpus]
;For example, a spaceship out in interstellar space, far from all other objects and with all its rockets turned off, has no net external force acting on it.  [ncert corpus]
;uxAharaNa ke lie, anwarA wArakIya AkASa meM saBI guruwvIya vaswuoM se bahuwa xUra kisI anwarikRayAna, jisake saBI roYketa banxa kie jA cuke hoM, para koI neta bAhya bala kAryarawa nahIM howA.[ncert corpus]
(defrule rocket2
(declare (salience 5100))
(id-root ?id rocket)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-det_viSeRaNa ?id ? )(samAsa_viSeRya-samAsa_viSeRaNa ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roYketa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rocket.clp 	rocket2   "  ?id "roYketa)" crlf))
)
