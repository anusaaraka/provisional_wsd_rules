;@@@ Added by 14anu-ban-01 on (11-03-2015)
;A savage dog. [cald]
;एक जङ्गली कुत्ता.  [self]
(defrule savage0
(declare (salience 0))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jafgalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage0   "  ?id "  jafgalI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-03-2015)
;A work full of savage satire. [cald]
;एक तीखा व्यङ्ग्य से परिपूर्ण कार्य .  [self]
(defrule savage1
(declare (salience 100))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 satire|criticism)
(viSeRya-viSeRaNa  ?id1 ?id)			;added by 14anu-ban-01 on (13-03-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIKA/katu))	;added 'katu' by 14anu-ban-01 on (13-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage1  "  ?id "  wIKA/katu)" crlf))	;added 'katu' by 14anu-ban-01 on (13-03-2015)
)


;@@@ Added by 14anu-ban-01 on (13-03-2015)
;Savage cuts in education spending are not appreciable. [Self: with respect to cald]
;शिक्षा खर्च  में बहुत ज्यादा कटौती प्रशंसनीय नहीं हैं . [self]
(defrule savage2
(declare (salience 100))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 spend)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-in_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_jZyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage2   "  ?id "  bahuwa_jZyAxA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;Her latest novel has been savaged by the critics. [oald]
;उसके नवीनतम उपन्यास की आलोचकों के द्वारा तीखी आलोचना की गयी है .  [self]
(defrule savage3
(declare (salience 0))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIKI_AlocanA_kara))
(assert (kriyA_id-subject_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  savage.clp 	savage3   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage3   "  ?id "  wIKI_AlocanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;The child was savaged by a dog. [oald]
;बच्चे पर कुत्ते के द्वारा खूङ्खार रूप से आक्रमण किया गया था .  [self]
(defrule savage4
(declare (salience 200))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 dog|bear)
(kriyA-by_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUMKAra_rUpa_se_AkramaNa_kara))
(assert (kriyA_id-subject_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  savage.clp 	savage4   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage4   "  ?id "  KUMKAra_rUpa_se_AkramaNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;She was savaged to death by a bear. [cald] 
;वह भालू के द्वारा आक्रमण कर मार दी गयी थी . [self]
(defrule savage5
(declare (salience 200))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 death)
(kriyA-to_saMbanXI  ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?id2)) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ?id1 AkramaNa_kara_mAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " savage.clp 	savage5   "  ?id "  " ?id2 "  " ?id1 "  AkramaNa_kara_mAra_xe )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;The article was a savage attack on the government's record.[oald]
;लेख सरकार के इतिहास पर एक कटु आलोचना था . [self]
(defrule savage6
(declare (salience 200))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ? ?id1)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 katu_AlocanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " savage.clp 	savage6   "  ?id "  " ?id1 "    katu_AlocanA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;He turned out to be the savage killer of two young boys.[self: with respect to oald]
;वह दो छोटे बच्वों का खूङ्खार हत्यारा निकला .   [self]
(defrule savage7
(declare (salience 100))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 killer|criminal|assassin|gunman|terrorist)
(viSeRya-viSeRaNa  ?id1 ?id)			
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUfKAra/barbara/nirxayI))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage7  "  ?id " KUfKAra/barbara/nirxayI)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (13-03-2015)
;Twelve thousand years ago, our ancestors were primitive savages living in caves.[cald]
;बारह हजार वर्ष पहले, हमारे पूर्वज गुफाओं में रहने वाले प्राचीन असभ्य आदिम जाति का सदस्य थे . [self]
(defrule savage8
(declare (salience 0))
(id-root ?id savage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asaBya_Axima_jAwi_kA_saxasya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savage.clp 	savage8   "  ?id "  asaBya_Axima_jAwi_kA_saxasya )" crlf))
)
