;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-02(22-08-2014)
;see and found are added in the list .
;My article has been accepted for publication.[oald]
;मेरी लेख प्रकाशन के लिए स्वीकार किया गया है . [self]
;I found the article while I was browsing through some old magazines.[oald]
;मुझे कुछ पुरानी पत्रिकाओं  के पृष्ठ पलटते समय यह लेख मिला.
;John happened to see the article .[MTES]
;जॉन को लेख दिखा .।[manual]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)
;Added 'kriyA-subject' relation and word 'advance' in the list by Garima Singh 1-feb-2014
; She wrote an article attacking the judges and their conduct of the trial.
;उसने जजों कीं पैरवी के ढ़ंग कीं आलोचना करते हुए एक लेख लिखा
;The article advances a new theory to explain changes in the climate.
;लेख जलवायु में परिवर्तन समझाने के लिये एक नया सिद्धान्त प्रस्तुत करता है . 
(defrule article0
(declare (salience 3500))
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ?id1 ?id)(kriyA-subject ?id1 ?id)(kriyA-in_saMbanXI  ?id1 ?id));added '(kriyA-in_saMbanXI  ?id1 ?id)' by Garima Singh
(id-root ?id1 write|read|discuss|publish|describe|advance|see|found|accept)	;'accept' is added in the list by 14anu-ban-02(26-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article0   "  ?id "  leKa )" crlf))
)

;Commented by 14anu-ban-02 (22-08-2014)
;@@@ Added by Garima Singh(M.Tech-C.S,Banasthali Vidyapith)17-Feb-2014
;I found the article while I was browsing through some old magazines.[oald]
;मुझे कुछ पुरानी पत्रिकाओं  के पृष्ठ पलटते समय यह लेख मिला. 
;(defrule article2
;(declare (salience 3500))
;(id-root ?id article)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?kri1 ?id)
;(kriyA-samakAlika_kriyA  ?kri1 ?kri2)
;(kriyA-through_saMbanXI  ?kri2 ?id1)
;(id-root ?id1 book|magazine|newspaper)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id leKa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article2   "  ?id "  leKa )" crlf))
;)

;@@@ Added by 14anu-ban-02 (19-11-2014)
;The strategy of destroying the food supply of the civilian population in an area of conflict has been banned under Article 54 of Protocol I of the 1977 Geneva Conventions.[agriculture]
;The strategy of destroying the food supply of the civilian population in an area of conflict has been banned under article 54 of Protocol I of the 1977 Geneva Conventions.[modified]
;संघर्ष के एक क्षेत्र में नागरिक आबादी की खाद्य आपूर्ति को नष्ट करने की रणनीति पर 1977 में जेनेवा कन्वेन्शन्ज के प्रोटोकॉल आई के अनुच्छेद 54 के तहत प्रतिबंध लगाया गया है .[manual]
(defrule article3
(declare (salience 5000))
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) number)
;(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anucCexa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article3   "  ?id "  anucCexa )" crlf))
)

;@@@Added by 14anu-ban-02(25-03-2015)
;The newspaper received a deluge of complaints about the article.[cald]
;समाचारपत्र ने लेख के बारे में शिकायतों का भरमार प्राप्त किया . [self]
(defrule article4
(declare (salience 1000))
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?id1 ?id)
(kriyA-object  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article4   "  ?id "  leKa )" crlf))
)



;*******************DEFAULT RULES**************************************
;$$$Modified by 14anu-ban-02(25-03-2015)
;The articles found in the car helped the police to identify the body.[oald]
;गाड़ी में पाई गई वस्तुओं ने लाश की पहचान करने में  पुलिस की सहायता की . [self]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)
(defrule article1
(declare (salience 0))	;salience reduced to 0 from 3500 by 14anu-ban-02(25-03-2015)
(id-root ?id article)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id1 ?id)	;commented by 14anu-ban-02(25-03-2015)
;(id-root ?id1 write|read|discuss|publish|described)	;commented by 14anu-ban-02(25-03-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  article.clp 	article1   "  ?id "  vaswu )" crlf))
)

;********************************************************* EXAMPLES **************************************************************************
; She wrote an article attacking the judges and their conduct of the trial.

