;##############################################################################
;#  Copyright (C) 2014-2015 Shalu Sneha (shalusneha1994@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu01 on 27-06-2014
;Crickets were chirping in the night.
;झींगुर रात में चूँ-चूँ कर रहे थे . 
(defrule cricket0
(declare (salience 4900))
(id-root ?id cricket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 verb)
(id-root ?id1 chirp|sing|sound|peep|babble|blab|tattle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  JIMgura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cricket.clp 	cricket0   "  ?id "   JIMgura)" crlf))
)


;@@@ Added by 14anu01 on 27-06-2014
;He was playing cricket.
;वह क्रिकेट खेल रहा था . 
(defrule cricket1
(declare (salience 4000))
(id-root ?id cricket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   kriketa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cricket.clp 	cricket1   "  ?id "    kriketa)" crlf))
)

