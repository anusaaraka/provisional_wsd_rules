
;Added by Amba
(defrule perform0
(declare (salience 5000))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 process)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform0   "  ?id "  kara )" crlf))
)

(defrule perform1
(declare (salience 4900))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 photosynthesis)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform1   "  ?id "  kara )" crlf))
)

(defrule perform2
(declare (salience 4800))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform2   "  ?id "  kArya_kara )" crlf))
)

;default_sense && category=verb	kara	0
;"perform","V","1.karanA"
;By giving donation he has performed a great social service.
;That magician performs miracles.
;--"2.praxarSana_karanA"
;The play was first performed in the late 80's.
;*--"3.kArya meM AnA/kriyASIla honA"
;Your new technique has performed very well in the tests.
;
;

;@@@ Added by 14anu-ban-09 on (18-10-2014)
;Robert Hooke, an English physicist (1635 - 1703 A.D) performed experiments on springs and found that the elongation (change in the length) produced in a body is proportional to the applied force or load. [NCERT CORPUS] 
;eka aMgrejZa BOwika SAswrI rAbarta huka (san 1635 - 1703) ne spriMgoM para prayoga kie Ora yaha pAyA ki kisI piNda meM uwpanna viswAra (laMbAI meM vqxXi) prawyAropiwa bala yA loda ke anukramAnupAwI howA hE. [NCERT CORPUS]

(defrule perform3
(declare (salience 4800))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-object  ?id ?id1) 
(id-root ?id1 experiment) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform3   "  ?id "  kara )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (15-12-2014)
;Changed meaning from 'praxarSana_karA' to 'praxarSana_kara'
;@@@ Added by 14anu13
;Singer performed well in ceremony.
;समारोह मे़ गायक ने अच्छा प्रदर्शन किया |
;गायक ने समारोह में अच्छा प्रदर्शन किया . [Anusaaraka] ;Added by 14anu-ban-09 on (15-12-2014)
(defrule perform4
(declare (salience 5000))
(id-root ?id perform)
(id-root ?sub singer|dancer|actor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?sub)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform4   "  ?id "  praxarSana_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (29-01-2015)
;The younger brother of Bahubali Shreyansh Kumar had performed the feet worship of Adinath Bhagwan .	[Total_tourism]
;बाहुबली के छोटे भाई श्रेयंषकुमार ने आदिनाथ भगवान का चरणाभिषेक किया था | 						[Total_tourism]

(defrule perform5
(declare (salience 5000))
(id-root ?id perform) 
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-root ?id1 worship)
(id-root ?id2 foot)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 caraNABiReka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  perform.clp  perform5  "  ?id "  " ?id1 "  " ?id2 "  caraNABiReka_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (26-02-2015)
;This experiment was later performed around 1911 by Hans Geiger (1882 — 1945) and Ernst Marsden (1889 — 1970, who was 20 year-old student and had not yet earned his bachelor's degree).	[NCERT-CORPUS]
;यह प्रयोग कुछ समय पश्चात सन् 1911 में हैंस गाइगर (1882 – 1945) तथा अर्नेस्ट मार्सडन (1889 – 1970, जो 20 वर्षीय छात्र थे तथा जिन्होंने अभी स्नातक की उपाधि भी ग्रहण नहीं की थी) ने किया.		[NCERT-CORPUS]
(defrule perform6
(declare (salience 5000))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 experiment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform6   "  ?id "  kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-03-2015)
;In some cases, the entire history and examination was performed together. [Report set 5]
;कुछ मामलों में, सम्पूर्ण इतिहास और परीक्षा एक साथ पूरी की गयी थी .			   [Manual] 

(defrule perform7
(declare (salience 5000))
(id-root ?id perform)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 examination)
(conjunction-components  ?id2 ?id3 ?id1)
(id-root ?id2 and)
(id-root ?id3 history)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perform.clp 	perform7   "  ?id "  pUrA_kara)" crlf))
)


