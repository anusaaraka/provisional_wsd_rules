;@@@Added by 14anu-ban-02(04-03-2015)
;Sentence: They are trying to protect their child's anonymity.[mw]
;Translation: वे उनके बच्चे का अनामिकता बनाये रखने के लिये प्रयास कर रहे हैं . [self]
(defrule anonymity0 
(declare (salience 0)) 
(id-root ?id anonymity) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anAmikawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anonymity.clp  anonymity0  "  ?id "  anAmikawA )" crlf)) 
) 


;@@@Added by 14anu-ban-02(04-03-2015)
;She enjoyed the anonymity of life in a large city.[mw]
;उसने बड़े शहर में जीवन की गुमनामी का आनन्द उठाया . [self]	;suggested by chaitanaya sir.
(defrule anonymity1 
(declare (salience 100)) 
(id-root ?id anonymity) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)	;need sentences to restrict the rule
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id gumanAmI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anonymity.clp  anonymity1  "  ?id "  gumanAmI )" crlf)) 
)
