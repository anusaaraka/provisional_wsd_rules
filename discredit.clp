
(defrule discredit0
(declare (salience 5000))
(id-root ?id discredit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxanAmI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discredit.clp 	discredit0   "  ?id "  baxanAmI )" crlf))
)

;"discredit","N","1.baxanAmI"
;Your actions will bring discredit to your name.
;
(defrule discredit1
(declare (salience 4900))
(id-root ?id discredit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxanAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discredit.clp 	discredit1   "  ?id "  baxanAma_kara )" crlf))
)

;"discredit","VT","1.baxanAma_karanA"
;This newspaper story discredits politicians.
;

;@@@ Added by 14anu-ban-04 on 04-09-2014
;His theory was discredited by the church, but notable amongst its supporters was Galileo who had to face prosecution from the state for his beliefs.
;गिरजाघर ने इस सिद्धान्त पर सन्देह  किया ; परन्तु इस सिद्धान्त के लब्ध प्रतिष्ठित समर्थकों में एक गैलीलियो थे, जिनपर शासन के द्वारा, आस्था के विरुद्ध होने के कारण, मुकदमा चलाया गया.
(defrule discredit2
(declare (salience 4900))
(id-root ?id discredit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxeha_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  discredit.clp 	discredit2   "  ?id "  sanxeha_kara )" crlf))
)
