
(defrule hitch0
(declare (salience 5000))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hitch.clp 	hitch0   "  ?id "  Jataka )" crlf))
)

(defrule hitch1
(declare (salience 4900))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JatakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hitch.clp 	hitch1   "  ?id "  JatakA )" crlf))
)

;"hitch","N","1.JatakA/XakkA/atakAva"
;usane jIwU ko 'hitch'(XakkA mAra) kara girA xiyA.

;$$$ Modified by 14anu-ban-06 (28-11-2014)
;### [COUNTER EXAMPLE] ### They hitched a ride in a truck.(OALD);added by 14anu-ban-06 (28-11-2014)
;### [COUNTER EXAMPLE] ### They hitched across the States.(OALD);added by 14anu-ban-06 (28-11-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 23/6/2014*******
;They got hitched last month.
;वे पिछले महीने विवाहित हुए .
(defrule hitch2
(declare (salience 5200))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(- ?id 1) get);added by 14anu-ban-06 (28-11-2014)
;(kriyA-subject  ?id ?id1);commented by 14anu-ban-06 (28-11-2014)
;(id-word ?id1 they);commented by 14anu-ban-06 (28-11-2014)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id vivAhiwa)) ;commented by 14anu-ban-06
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1)  vivAhiwa_ho));added by 14anu-ban-06 (28-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hitch.clp	 hitch2  "  ?id "  " =(- ?id 1)  " vivAhiwa_ho  )" crlf))
)
;
;
;@@@ Added by 14anu-ban-06 (28-11-2014)
;We spent the summer hitching around Europe.(OALD)
;हमने यूरोप के आसपास घूमते हुए ग्रीष्म बिताया . (manual)
(defrule hitch3
(declare (salience 5200))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmawe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hitch.clp 	hitch3   "  ?id "  GUmawe_ho )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-11-2014)
;The horses were hitched to a shiny black carriage.(cambridge)
;घोडे एक चमकदार काली गाडी से बाँधे गये थे . (manual)
;She hitched the pony to the gate. (OALD)
;उसने टट्टू  को दरवाज़े से बाँधा .(manual)
(defrule hitch4
(declare (salience 5200))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hitch.clp 	hitch4   "  ?id "  bAzXa )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-11-2014)
;The only hitch , as the embarrassed organisers discovered later , was the presence of at least five child couples .(parallel corpus)
;एकमात्र अड़चन , जो बाद में आयोजकों को पता चली , पांच बाल जोड़ें की उपस्थिति थी .(parallel corpus)
;The only hitch was the extraordinarily low salary. (COCA)
;असामान्य रूप से कम वेतन एकमात्र अड़चन थी . (manual)
(defrule hitch5
(declare (salience 5200))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 only)
(viSeRya-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id adZacana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hitch.clp 	hitch5   "  ?id "  adZacana )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-02-2015)
;She hitched up her skirt and waded into the river. (OALD)
;उसने अपनी स्कर्ट ऊपर उठायी और नदी में कठिनाई से चली . (manual)
(defrule hitch6
(declare (salience 5300))
(id-root ?id hitch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Upara_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hitch.clp	hitch6  "  ?id "  " ?id1 "  Upara_uTA  )" crlf))
)
