
(defrule fold0
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-in_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMta));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fold.clp fold0 " ?id "  PeMta )" crlf)) 
)

(defrule fold1
(declare (salience 4900))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fold.clp	fold1  "  ?id "  " ?id1 "  PeMta  )" crlf))
)

(defrule fold2
(declare (salience 4800))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-into_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMta));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fold.clp fold2 " ?id "  PeMta )" crlf)) 
)

(defrule fold3
(declare (salience 4700))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fold.clp	fold3  "  ?id "  " ?id1 "  PeMta  )" crlf))
)

(defrule fold4
(declare (salience 4600))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-in_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMta));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fold.clp fold4 " ?id "  PeMta )" crlf)) 
)

(defrule fold5
(declare (salience 4500))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PeMta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fold.clp	fold5  "  ?id "  " ?id1 "  PeMta  )" crlf))
)

(defrule fold6
(declare (salience 4400))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waha_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold6   "  ?id "  waha_kara )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 24/6/2014
;They had to fold the company.
;उनको कंपनी बंद करनी पडी.
(defrule fold08
(declare (salience 4600))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 company|industry|enterprise|business)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold08   "  ?id "  baMxa )" crlf))
)


;Commented this rule by Roja(27-12-13). File never loads with Suffix. Ex: He has multiplied his money tenfold by investing judiciously. Here 'fold' is the suffix of the word 'ten'. File loads only when 'tenfold.clp' is present.
;"fold","Suffix","1.gunA"
;(defrule fold7
;(declare (salience 4300))
;(id-root ?id fold)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id Suffix)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id gunA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold7   "  ?id "  gunA )" crlf))
;)

;@@@ Added by 14anu02 on 5.7.14
;She folded a blanket around the baby.
;उसने शिशु के चारों ओर कम्बल लपेटा . 
(defrule fold7
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold7   "  ?id "  lapeta )" crlf))
)

;@@@ Added by 14anu02 on 5.7.14
;She gently folded the baby in a blanket.
;उसने नरमी से शिशु को कम्बल में लपेटा . 
(defrule fold8
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 blanket)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapeta))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold8   "  ?id "  lapeta )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  fold.clp     fold8   "  ?id "  ko )" crlf))
)

;@@@ Added by 14anu02 on 5.7.14
;The blankets had been folded down.
;कम्बल तह किए गये थे. 
;The bed can be folded away during the day.
;बिस्तर दिन के दौरान तह किया जा सकता है . 
(defrule fold9
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 back|down|over|away|down|up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 waha_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fold.clp	fold9  "  ?id "  " ?id1 "  waha_kara  )" crlf))
)


;@@@ Added by 14anu-ban-05 on 05-09-2014
;In the second condition by lying on the left side the right leg should be folded from the knee.[KARAN SINGLA]
;दूसरी अवस्था में बाँए करवट लेकर दाहिने पैर को घुटने पर से मोड़ते हुए रखना चाहिए ।
;In one condition after lying flat and leaving the abdomen region loose both the knees are folded upwards .
;एक अवस्था में पीठ के बल लेटकर पेडू के हिस्से को ढीला करके दोनों घुटनों को ऊपर की ओर मोड़ लेते हैं ।
(defrule fold008
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 leg|knee|arm)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold008   "  ?id "  moda )" crlf))
)

;@@@ Added by 14anu-ban-05 on 05-09-2014
;This aluminium ladder is of the type which can be kept folded .
;एलुमिनियम की यह सीढ़ी मोड़ कर रखी जा सकने वाली होती है ।
(defrule fold09
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(- ?id 1) keep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold09   "  ?id "  moda_kara )" crlf))
)


;
;LEVEL 
;Headword : fold
;
;Examples --
;
;"Fold","V"
;       
;--"1.wahAnA"
;When I saw Sita she was folding her clothes. 
;jaba mEne sIwA ko xeKA vaha apane kapadZe wahA rahI WI.  
;--"2.mudZanA"
;After the party Ravi folded the chairs && kept them in a corner.  
;pArtI ke bAxa ravi ne kursiyo ko modZA Ora eka kone meM raKa xiyA.
;--"3.jodZanA"
;She folded her hands in a gesture of greeting.
;usane vanxana karane ke liye apane hAWa jodZa liye.
;--"4.sametanA"     
;Ravi folded the baby in his arms. 
;ravi ne bacce ko apanI bAhoM meM sameta liyA. 
;
;uparyukwa vAkyo ko XyAna se paraKane ke bAxa yaha spaRta ho jAwA hE ki eka
;Sabxa ke aneka arWa howe hue BI vaha eka xUsare se baMXe howe hEM.  
;
;anwarnihiwa sUwra ;
;
;     1.jaba mEne sIwA ko xeKA vaha apane kapadZe waha rahI WI. -- wahanA
;                                                   |                   
;                                                   | 
;     2.ravi ke pAsa mudZane vAlI kursI hEM.       modZanA
;                                                   |
;                                                   |
;                                                jodZanA
;                                                   |
;                                                   |
;    3.ravi ne bacce ko apanI bAhoM meM sameta liyA.  sametanA
;
;
;spaRta hE ki waha karane kI prakriyA meM modZanA SAmila, jaba kuCa xoharA mudZawA hE wo judZa BI jAwA hE Ora simata BI jAwA hE. awaH isakA mUlArWa 'waha' hI hogA. isakA
;sUwra banegA -
;
;sUwra : waha`

;@@@ Added by 14anu-ban-05 on (27-11-2014)
;Fold a small, thin aluminium foil (about 6 cm in length) in the middle and attach it to the flattened end of the rod by cellulose tape.[NCERT]
;lambA pawalA Eluminiyama pawra (lagaBaga 6 @cm ) lekara ise bIca meM modie Ora ise Cada ke capate sire para selyulosa-tepa ke sAWa joda xIjie.[NCERT]
(defrule fold10
(declare (salience 5000))
(id-root ?id fold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 foil)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fold.clp 	fold10   "  ?id "  moda )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  fold.clp     fold10   "  ?id "  ko )" crlf))
)






