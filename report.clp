;@@@ Added by Anita--21.3.2014
;To remove such ambiguities in determining the number of significant figures the best way is to report ;every measurement in scientific notation in the power of 10. [ncert]
;सार्थक अङ्कों के निर्धारण में इस प्रकार की सन्दिग्धता को दूर करने के लिए सर्वोत्तम उपाय यह है कि प्रत्येक माप को वैज्ञानिक सङ्केत (10 ;की घातों के रूप में) में प्रस्तुत किया जाए.
;Thus the result of measurement should be reported in a way that indicates the precision of measurement.
;अतः मापन के परिणामों को इस प्रकार प्रस्तुत किया जाना चाहिए कि मापन की परिशुद्धता स्पष्ट हो जाए..
;Clearly reporting the result of measurement that includes more digits than the significant digits is ;superfluous and also misleading since it would give a wrong idea about the precision of measurement.
;अतः मापन के परिणामों को इस प्रकार प्रस्तुत किया जाना चाहिए कि मापन की परिशुद्धता स्पष्ट हो जाए
(defrule report2
(declare (salience 5100))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-in_saMbanXI  ?id ?)(kriyA-kriyA_viSeRaNa  ?id ?)) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report2   "  ?id "  praswuwa_kara )" crlf))
)

;@@@ Added by Anita--21.3.2014
;Suppose a length is reported to be 4.700 m.. [ncert]
;मान लीजिए कि लंबाई 4.700 मीटर होने का वर्णन है ।
(defrule report3
(declare (salience 5200))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varNana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report3   "  ?id "  varNana_kara )" crlf))
)

;@@@ Added by Anita--21.3.2014
;Normally the reported result of measurement is a number that includes all digits in the number that ;are known reliably plus the first digit that is uncertain. [ncert]
;अभिलम्बवत् मापन का अच्छे नाम का परिणाम एक ऐसा अंक है कि जो उस अंक में सब अङ्क सम्मिलित होता है कि जो मिलाकर प्रथम ;अङ्क विश्वसनीय ढङ्ग से जाना गया है कि वह अनिश्चित है।
;सामान्यतः नाप का सूचित परिणाम एक ऐसा अङ्क है कि जो उस अङ्क में सब अङ्क सम्मिलित होता है कि जो मिलाकर प्रथम अङ्क ;विश्वसनीय ढङ्ग से जाना गया है कि वह अनिश्चित है। [anusaaraka out-put] 
(defrule report4
(declare (salience 5300))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report4   "  ?id "  sUciwa )" crlf))
)

;@@@ Added by Anita--22.3.2014
;The length of an object reported after measurement to be 287.5 cm has four significant figures the digits 2 8 7 are certain ;while the digit 5 is uncertain. [ncert ]
;माप करने के बाद किसी वस्तु की लम्बाई  चार महत्वपूर्ण आँकड़ों में २८७.५ सेमी बताई गई  है  ,अंकों में २८७ निश्चित है , जबकि अंक ५ अनिश्चित आंकड़ा है । 
(defrule report5
(declare (salience 5400))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-after_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawAyA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report5   "  ?id "  bawAyA_gayA )" crlf))
)

;@@@ Added by Anita--29.3.2014
;Thus in the example above density should be reported to three significant figures. [ncert]
;अतः उपरोक्त उदाहरण में घनत्व को तीन सार्थक अङ्कों तक ही लिखा जाना चाहिए.
(defrule report6
(declare (salience 5500))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report6   "  ?id "  liKa )" crlf))
)
;@@@ Added by Anita--17.6.2014
;Inspector Tajganj Hari Mohan reported that a case has been filed against the truck driver. 
;निरीक्षक ताजगञ्ज हरि मोहन ने बताया गया कि मामला ट्रक ड्राइवर के विरुद्ध दर्ज किया गया है ।
;The General Manager of NHPC, Arvind Batt, reported that staff kept on repairing the power line inspite of rain. [news-dev]
;एनएचपीसी के महाप्रबंधक अरविंद बट ने बताया कि बारिश के बावजूद कर्मचारी बिजली की मरम्मत में जुटे रहे।
(defrule report7
(declare (salience 5600))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?)
(kriyA-subject  ?id ?sub)
(or (id-root ?sub  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-cat_coarse ?sub PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report7   "  ?id "  bawA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* " report.clp  report7   "  ?id " ne )" crlf)
)
)

;@@@ Added by Anita--7.7.2014
;Thus the result of measurement should be reported in a way that indicates the precision of measurement. 
;इस प्रकार नाप का परिणाम इस प्रकार से सूचित किया जाना चाहिए जो नाप की परिशुद्धता दर्शाताए ।
(defrule report8
(declare (salience 5700))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 way)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report8   "  ?id "  sUciwa_kara )" crlf))
)
;@@@ Added by Anita--12.8.2014
;People around the house immediately reported the matter to the police. [The news-dev]
;तत्काल आसपास के लोगों ने पुलिस को इस विषय को सूचित कर  दिया ।
(defrule report9
(declare (salience 5800))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 police)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report9   "  ?id "  sUciwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-9-2014)
;The length of an object reported after measurement to be 287.5 cm has four significant figures, the digits 2, 8, 7 are certain while the digit 5 is uncertain. [ncert corpus] 
;yaxi mApana ke bAxa kisI vaswu kI lambAI, 287.5 @cm vyakwa kI jAe wo isameM cAra sArWaka afka hEM, jinameM 2, 8, 7 wo niSciwa hEM paranwu afka 5 aniSciwa hE.  [ncert corpus]
;यदि मापन के बाद किसी वस्तु की लम्बाई, 287.5 cm व्यक्त की जाए तो इसमें चार सार्थक अङ्क हैं, जिनमें 2, 8, 7 तो निश्चित हैं परन्तु अङ्क 5 अनिश्चित है.
(defrule report10
(declare (salience 5600))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-after_saMbanXI ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report10   "  ?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-9-2014)
;The scientific notation is ideal for reporting measurement.[ncert corpus]  
;kisI BI mApana ke praswuwikaraNa kI vEjFAnika safkewa viXi eka AxarSa viXi hE.[ncert corpus] 
;किसी भी मापन के प्रस्तुतिकरण की वैज्ञानिक सङ्केत विधि एक आदर्श विधि है.
(defrule report11
(declare (salience 5700))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1 )
(id-root ?id1 measurement)
(id-cat_coarse ?id verb)
(id-word ?id reporting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwikaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report11   "  ?id "  praswuwikaraNa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-9-2014)
;Clearly, reporting the result of measurement that includes more digits than the significant digits is superfluous and also misleading since it would give a wrong idea about the precision of measurement.  [ncert corpus]
;awaH rASi ke mApana ke pariNAma meM sArWaka afkoM se aXika afka liKanA anAvaSyaka evaM BrAmaka hogA, kyofki, yaha mApa kI pariSuxXawA ke viRaya meM galawa XAraNA xegA.[ncert corpus]
;अतः राशि के मापन के परिणाम में सार्थक अङ्कों से अधिक अङ्क लिखना अनावश्यक एवं भ्रामक होगा, क्योङ्कि, यह माप की परिशुद्धता के विषय में गलत धारणा देगा.
;Suppose a length is reported to be 4.700 m.[ncert corpus]  
;mAna lIjie kisI vaswu kI lambAI 4.700 @m liKI gaI hE.
;मान लीजिए किसी वस्तु की लम्बाई 4.700 m लिखी गई है.
(defrule report12
(declare (salience 5800))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1);added by 14anu-ban-10 on (12-11-2014)
(id-root ?id1 result);added by 14anu-ban-10 on (12-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report12   "  ?id "  liKa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-11-2014)
;Some people (0.6%[26] of the United States population) report that they experience mild to severe allergic reactions to peanut exposure; symptoms can range from watery eyes to anaphylactic shock, which can be fatal if untreated.[agriculture domain]
;कुछ लोग (यूनाइटड स्टॆट्स की जनसङ्ख्या का 0.6 %) शिकायत करते हैं कि वे मूँगफली खाने पर मृदु से गंभीर प्रत्यूर्ज प्रतिक्रियाओं का अनुभव करते हैं;लक्षण आँखों से पानी बहने  से लेकर तीव्रगाहिता संबंधी आघात तक हो सकते हैं जो कि अनुपचारित रहें तो घातक हो सकते हैं.[manual]
(defrule report13
(declare (salience 6000))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)
(id-root ?id1 experience)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAyawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report13   "  ?id " SikAyawa_kara)" crlf))
)

;@@@ Added by 14anu24
;Foot and mouth disease has been reported , among elephants .
;पांव और मुख की बीमारी यह भी हाथियों में पाया गया .
(defrule report14
(declare (salience 5800))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAyA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report14   "  ?id "  pAyA_gayA )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;@@@ Added by 14anu17
;Each year there are around 1,800 reported cases of bacterial meningitis in England and Wales.
;हर एक वर्ष इंग्लैंड और वेल्स में जीवाण्विक गर्दन तोड बुखार के 1,800 रिपोर्ट मामलों आसपास हैं
(defrule report15
(declare (salience 4901))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 case)              ; added by 14anu-ban-10 on (13-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id riporta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report15   "  ?id "  riporta)" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-01-2015)
;Normally the reported result of measurement is a number that includes all digits in the number that are known reliably plus the first digit that is uncertain. [ncert corpus]
;sAXAraNawaH, mApana ke pariNAmoM ko eka safKyA ke rUpa meM praswuwa karawe hEM jisameM vaha saBI afka sammiliwa howe hEM jo viSvasanIya hEM, waWA vaha praWama afka BI sammiliwa kiyA jAwA hE jo aniSciwa hE.[ncert corpus]
;साधारणतः, मापन के परिणामों को एक सङ्ख्या के रूप में प्रस्तुत करते हैं जिसमें वह सभी अङ्क सम्मिलित होते हैं जो विश्वसनीय हैं, तथा वह प्रथम अङ्क भी सम्मिलित किया जाता है जो अनिश्चित है.[ncert corpus]
(defrule report16
(declare (salience 6100))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 result)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report16   "  ?id "  praswuwa_kara) " crlf))
)

;@@@ Added by 14anu-ban-10 on (09-02-2015)
;Upon your arrival , please report to the reception desk. (cambridge)
;आपके आगमन  पर, कृपया स्वागत पूछ-ताछ में सूचना दे. (manual)
(defrule report17
(declare (salience 6200))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1 )
(id-root ?id1 desk)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUcanA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report17   "  ?id "  sUcana_xe )" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-03-2015)
;The soldier was reported to his superior officer for failing in his duties.[cald]
;अपने कार्यों में असफल होने के लिए सैनिक उसके उच्च अधिकारी के समक्ष हाज़िर किया गया था . [self]
(defrule report18
(declare (salience 6300))
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) kiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " report.clp  report18  "  ?id "  " (+ ?id 1) "   kiyA)" crlf))
)

;#####################################default-rule################################
;$$$ Modified by Anita-12.8.2014 [made 0 no . rule to default-rule-by Anita]
(defrule report_default-rule
;(declare (salience 5000)) ;commented by Anita-22.3.2014
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id riporta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report_default-rule   "  ?id "  riporta )" crlf))
)

(defrule report1
;(declare (salience 4900)) ;commented by Anita-12.8.2014
(id-root ?id report)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAla_liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  report.clp 	report1   "  ?id "  ;hAla_liKa )" crlf))
)
;"report","VTI","1.hAla_liKanA"
;The discovery of a new medicine of Cancer has been reported.
;--"2.kArya_pragawi_sUcanA"
;The doctor reported the patient is now fit && well. 
;--"3.SikAyawa_karanA"
;I intend to report him to the secretary for cheating.  
;--"4.liKanA"   
;The stenographer has reported the speech of the president. 
;--"5.jAnakArI_xenA"
;Please report to the receptionist on arrival at 10.1.tomorrow with your
;certificates && testimonials. 
;--"6.varNana_karanA"
;He has reported a deficit.  
;The company reports pre-tax profits of over 1000d crore.
;
;


