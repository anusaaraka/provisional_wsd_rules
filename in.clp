;$$$ Modified by 14anu-ban-06 (11-12-2014) 
;@@@ Added by 14anu03 on 19-june-14.
;The person who was going Lucknow died in car accident.
;जो लखनऊ जा रहा था उस व्यक्ति ने गाडी दुर्घटना मे मारा गया .
;जो व्यक्ति लखनऊ जा रहा था वह गाडी दुर्घटना में मारा गया .(manual);added by 14anu-ban-06 (11-12-2014) 
(defrule in100
(declare (salience 5500))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 died)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 me_mArA_gayA)) ;commented by 14anu-ban-06 (11-12-2014) 
(assert (id-wsd_root_mng ?id meM));added by by 14anu-ban-06 (11-12-2014) 
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  in.clp     in100   "  ?id "  " ?id1 "  ;me_mArA_gayA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in100   "  ?id "  meM )" crlf))
)

;I have faith in him.
(defrule in0
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) faith)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in0   "  ?id "  para )" crlf))
)

(defrule in1
(declare (salience 4900))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in1   "  ?id "  - )" crlf))
)




;Modified by Meena(29.10.09)
;I will have dinner at seven o'clock in the evening . 
(defrule in2
(declare (salience 4800))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) evening|noon|afternoon)
(id-word =(+ ?id 2) evening|noon|afternoon)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in2   "  ?id "  ko )" crlf))
)




(defrule in3
(declare (salience 4700))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) chair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in3   "  ?id "  para )" crlf))
)

(defrule in4
(declare (salience 4600))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) go|slip|drag|pull|push)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in4   "  ?id "  aMxara )" crlf))
)

(defrule in5
(declare (salience 4500))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in5   "  ?id "  iMca )" crlf))
)

;"in","Abbr:inch","1.iMca"
;He is 4 ft 2 inches tall.
;
(defrule in6
(declare (salience 4400))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(not (kriyA-in_saMbanXI ?kri ?id1));Added by sheetal
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in6   "  ?id "  anxara )" crlf))
)

;"in","Adv","1.anxara"
;He walked in at the right moment.




;Added by Meena(29.10.09)
;I will have tea in the morning. 
(defrule in7
(declare (salience 1100))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 2) morning)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in7   "  ?id "  - )" crlf))
)



;;Modified by Meena(4.8.11) ; added (id-root =(- ?id 1) arrive|reach|come) for the following example.
;Reached in Honolulu, Mr. Shidler said that he believes the various Hooker malls can become profitable with new management. 
;Added by Meena(12.5.10)
;It was in Paris that Debussy first heard Balinese music . 
(defrule in8
(declare (salience 2000))
;(declare (salience 1000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-dummy_subject  =(- ?id 1)  =(- ?id 2))(id-root =(- ?id 1) arrive|reach|come))
;(or(kriyA-dummy_subject  =(- ?id 1)  =(- ?id 2))(id-cat_coarse =(+ ?id 1) PropN))
(kriyA-in_saMbanXI =(- ?id 1) ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in8   "  ?id "  - )" crlf))
)





;$$$ Modified by 14anu-ban-06 (07-03-2015)
;changed meaning from 'meM' to 'se' in print statement by 14anu-ban-06 (07-03-2015)
;Added by Meena(19.5.10)
;Paul, in typically rude fashion, told him he was talking rubbish.
;पॉल ने,विशिष्ट रूप से अभद्र तरीके से, उसको बताया कि वह बकवास कर रहा था . (manual);added by 14anu-ban-06 (07-03-2015)
(defrule in9
;(declare (salience 4300))
(declare (salience 1000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(or(id-root =(+ ?id 2) fashion)(id-root =(+ ?id 3) fashion))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in9   "  ?id "  se )" crlf)) 
)





;Salience reduced by Meena(29.10.09)
(defrule in10
;(declare (salience 4300))
(declare (salience 0))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 ?wrd)
(or (id-cat_coarse ?id preposition)(kriyA-in_saMbanXI  ?kri ?id1));"kriyA-in_saMbanXI" added by sheetal
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in10   "  ?id "  meM )" crlf))
)





;$$$ Modified by 14anu-ban-06 (15-12-2014)
;@@@ Added by Prachi Rathore[6-3-14]
;Applications must be in by April 30.[oald]
;प्रार्थना पत्र ३० अप्रेल तक पहुँच जाना चाहिये
(defrule in13
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id2 ?id1)
(kriyA-in_by_saMbanXI  ?id2 ?)
(id-root ?id1 application|letter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca_jA));meaning changed from 'pahuca_jA' to 'pahuzca_jA' by 14anu-ban-06 (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in13   "  ?id "  pahuzca_jA)" crlf)))


;@@@ Added by Prachi Rathore[6-3-14]
;Will you keep a tally of the number of customers going in and out?[cambridge]
;क्या आप  अंदर और बाहर जाते हुए ग्राहकों की संख्या की गिनती रखेंगे? 
(defrule in14
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in14   "  ?id "  aMxara)" crlf)))


;@@@ Added by Prachi Rathore[6-3-14]
;Exotic pets are the in thing right now.[oald]
;विदेशी पालतू जानवर आजकल प्रचलित  हैं. 
(defrule in15
(declare (salience 5050))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) thing)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pracaliwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " in.clp        in15  "  ?id "  " (+ ?id 1) "  pracaliwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_in14
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " in.clp   sub_samA_in14   "   ?id " aMxara)" crlf)))

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_in14
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " in.clp   obj_samA_in14   "   ?id " aMxara)" crlf)))
;

;$$$ Modified by 14anu-ban-06 (11-12-2014)
;@@@ Added by 14anu02 on 23.06.14
;And finally, there are changes in how the news is done.(corpus)
;और अन्ततः, इसमे बदलाव हैं कि समाचार कैसे किया गया है .  
;और अन्ततः, इसमे बदलाव हैं कि खबर कैसे तैयार की जाती है .  (manual);added by 14anu-ban-06 (11-12-2014)
(defrule in016
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) how)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isameM));meaning changed from 'isame' to 'isameM' by 14anu-ban-06 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " in.clp   in016   "   ?id " isameM)" crlf))
)


;@@@ Added by 14anu26     [30-06-14]
;It was in a sense , Pakistan's truth commission . 
;वह एक तरह से , पाकिस्तान का सच आयोग था . 
(defrule in16
(declare (salience 4300))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-in_saMbanXI  ?kri ?id1)
(id-root ?id1 sense)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in16   "  ?id "  se )" crlf))
)

;@@@ Added by 14anu-ban-06   (7-08-14)
;In Fig. 9.2(d), a solid sphere placed in the fluid under high pressure is compressed uniformly on all sides.  (NCERT)
;ciwra 9.2(@d) meM, aXika xAba ke kisI xrava ke aMxara raKA eka Tosa golA saBI ora se samAna rUpa se saMpIdiwa ho jAwA hE.
(defrule in17
(declare (salience 4400))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?kri ?id1)
(id-root ?id1 fluid|cupboard|box|locker)
(id-word ?kri placed)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?kri ?id ke_aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  in.clp  in17   "  ?id "    " ?kri " ke_aMxara)" crlf))
)

;$$$Modified by 14anu-ban-02(31-03-2016)
;###[COUNTER STATEMENT]In ancient times there were many kingdoms.[sd_verified]
;###[COUNTER STATEMENT]prAcIna kAla meM bahuwa rAjya We.[sd_verified]
;@@@ Added by 14anu-ban-06   (10-10-2014)
;A remarkable fact is that some special physical quantities, however, remain constant in time.  (NCERT)
;viBinna baloM xvArA niyanwriwa kisI BI BOwika pariGatanA meM kaI rASiyAz samaya ke sAWa parivarwiwa ho sakawI hEM.(NCERT)
(defrule in18
(declare (salience 4450))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?kri ?id1)
;(id-root ?id1 time)	;commented by 14anu-ban-02(31-02-2016)
(id-root ?kri remain)	;Added by 14anu-ban-02(31-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in18   "  ?id "  ke_sAWa )" crlf))
)

;@@@ Added by 14anu-ban-06   (13-10-2014)
;In principle, this means that the laws for 'derived' forces (such as spring force, friction) are not independent of the laws of fundamental forces in nature.  (NCERT)
;sixXAnwa rUpa meM isakA wAwparya yaha hE ki vyuwpanna baloM (jEse kamAnI bala, GarRaNa) ke niyama prakqwi ke mUla baloM ke niyamoM se svawanwra nahIM hE.(NCERT)
(defrule in19
(declare (salience 4450))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 nature)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in19   "  ?id "  ke_sAWa )" crlf))
)

;@@@ Added by 14anu-ban-06   (15-10-2014)
;A number of exercises at the end of this chapter will help you develop skill in dimensional analysis.  (NCERT)
;इस अध्याय के अन्त में दिए गए कई अभ्यास प्रश्न, आपकी विमीय विश्लेषण की कुशलता विकसित करने में सहायक होङ्गे.(NCERT)
(defrule in20
(declare (salience 4500))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id2)
(id-root ?id2 analysis)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in20   "  ?id "  kI )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-02-2015)
;The novel is good in parts.(oald)
;उपन्यास का अंश अच्छा है . (manual)
(defrule in21
(declare (salience 4600))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(viSeRya-in_saMbanXI ?id2 ?id3)
(id-root ?id1 novel|book)
(id-root ?id3 part)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in21   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-02-2015)
;The pilgrims knelt in self abasement. (OALD)
;तीर्थयात्री अपनी दशा पर झुकते हैं.(manual)
(defrule in22
(declare (salience 4600))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id2)
(kriyA-in_saMbanXI ?id1 ?id3)
(id-word ?id3 abasement|plight)
(id-root ?id2 pilgrim)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in22   "  ?id "  para )" crlf))
)

;@@@ Added by 14anu-ban-06 (07-03-2015)
;The meeting was cloaked in mystery. (OALD)
;बैठक रहस्य से आच्छादित थी . (manual)
(defrule in23
(declare (salience 4700))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-in_saMbanXI  ?kri ?id1)
(id-root ?kri cloak)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in23   "  ?id "  se )" crlf))
)

;@@@ Added by 14anu-ban-06 (07-03-2015)
;He is blind in one eye. (in.clp)
;वह एक आँख से अन्धा है . (manual)
(defrule in24
(declare (salience 4700))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-in_saMbanXI  ?kri ?id1)
(id-root ?kri blind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp 	in24   "  ?id "  se )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Chaitanya sir (16-02-2017)
;I know whom to visit in such occasions.
(defrule in25
(declare (salience 4700))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-in_saMbanXI  ?kri ?id1)
(id-root ?id1 occasion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp       in25   "  ?id "  para )" crlf))
)

;------------------------ Default Rules ----------------------
;"in","Prep","1.meM"
;They were born in the same year 1984.
;Salience reduced by Meena(29.10.09)
(defrule in11
;(declare (salience 4300))
(declare (salience 0))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in11   "  ?id "  meM )" crlf))
)

;@@@ Added by Prachi Rathore[6-3-14]
;Short skirts are in again.[oald]
;छोटी स्कर्ट फिर से प्रचलन में है
(defrule in12
(declare (salience 5000))
(id-root ?id in)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracalana_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  in.clp        in12   "  ?id "  pracalana_meM)" crlf)))



;"in","Prep","1.meM"
;They were born in the same year 1984.
;--"2.se"
;He is blind in one eye.
;--"3.kA"
;He is the most famous person in town.
;
;"Prep","1.meM"
;They were born in the year 1984.
;ve 1984 meM janme.
;--"2. meM_kA"
;He is the most famous person in town.
;vaha isa Sahra kA sabase prasixXa  vyakwi hE.
;
;"Adv","1.anxara"
;He walked in at the right moment.
;vaha sahI samaya meM aMxara AyA.
;
;"Abbr:inch","1.iMca"
;He is 4 ft 2 inches tall.
;vaha 4 PIta 2 incesa uzcA hE.


