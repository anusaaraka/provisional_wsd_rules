
(defrule slur0
(declare (salience 5000))
(id-root ?id slur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta_AvAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slur.clp 	slur0   "  ?id "  aspaRta_AvAjZa )" crlf))
)

;"slur","N","1.aspaRta_AvAjZa"
;He continued to speak in a slur.
;
(defrule slur1
(declare (salience 4900))
(id-root ?id slur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalaMka_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slur.clp 	slur1   "  ?id "  kalaMka_laga )" crlf))
)

;"slur","V","1.kalaMka_laganA"
;Traitor quisling slurred the image of his country poland.
;--"2.aspaRta_uccAraNa"
;He continued to slur his words.
;

;@@@ Added by 14anu-ban-11 on (28-02-2015)
;She had dared to cast a slur on his character.(oald) 
;उसने  साहस किया उसके चरित्र  पर मिथ्या आरोप डालना  का . (self)
(defrule slur2
(declare (salience 5001))
(id-root ?id slur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-on_saMbanXI  ?id1 ?id2)
(id-root ?id1 cast)
(id-root ?id2 character)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miWyA_Aropa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slur.clp 	slur2   "  ?id "  miWyA_Aropa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (28-02-2015)
;He continued to slur his words. (slur.clp)
;उसने  उसके शब्दो को लगातार अस्पष्ट उच्चारण किया. (self)
(defrule slur3
(declare (salience 4901))
(id-root ?id slur)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 word)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta_uccAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slur.clp 	slur3   "  ?id "  aspaRta_uccAraNa)" crlf))
)


