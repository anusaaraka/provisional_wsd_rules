
;@@@ Added by 14anu-ban-09 on (10-01-2015)
;The culprit started to run and the cop took off in pursuit. [Hinkhoj]
;अपराधी ने भागना शुरू किया और  पुलिस अधिकारी तलाश में निकल गया. [Self]

(defrule pursuit0
(declare (salience 0000))
(id-root ?id pursuit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pursuit.clp 	pursuit0   "  ?id "  walASa )" crlf))
)


;@@@ Added by 14anu-ban-09 on (10-01-2015)
;It carries with it the seductive appeal of contentment , which is the enemy of all sporting pursuit . [sport.clp] 
;इसमें संतोष का वह चित्ताकर्षक भाव छिपा है जो सभी खेल संबंधी लक्ष्य का शत्रु है . [Manual]


(defrule pursuit1
(declare (salience 4300))
(id-root ?id pursuit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 sport)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pursuit.clp 	pursuit1   "  ?id "  lakRya )" crlf))
)


