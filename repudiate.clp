;@@@ Added by 14anu-ban-10 on (27-02-2015)
;He has repudiated his old friend.[oald]
;उसने उसके पुराने मित्र को नामंजूर करना है .[manual] 
(defrule repudiate1
(declare (salience 200))	
(id-root ?id repudiate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 friend)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAmaZjUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repudiate.clp 	repudiate1  "  ?id "  nAmaZjUra_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-02-2015)
;To repudiate a report.[oald]
;रिपोर्ट को अस्वीकार करना.[manual]
(defrule repudiate2
(declare (salience 300))	
(id-root ?id repudiate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 report)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asvIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repudiate.clp 	repudiate2  "  ?id "  asvIkAra_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (27-02-2015)
;The woman repudiated the divorce settlement.[hinkhoj]
;पत्नी तलाक समझौता से  मुकर गई. [manual]
(defrule repudiate0
(declare (salience 100))
(id-root ?id repudiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  repudiate.clp        repudiate0  "  ?id "  mukara_jA )" crlf))
)

