
(defrule smooth0
(declare (salience 5000))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth0   "  ?id "  cikanA )" crlf))
)

(defrule smooth1
(declare (salience 4900))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sapAta_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir*  " smooth.clp smooth1 " ?id "  sapAta_kara )" crlf)) 
)

(defrule smooth2
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sapAta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir*  " smooth.clp	smooth2  "  ?id "  " ?id1 "  sapAta_kara  )" crlf))
)

(defrule smooth3
(declare (salience 4700))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sapAta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth3   "  ?id "  sapAta_kara )" crlf))
)

(defrule smooth4
(declare (salience 4600))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth4   "  ?id "  cikanA )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (08-04-2015)
;In the background is the sound of smooth jazz.[coca]
;वातावरण में  मधुर जाज की ध्वनि है. [self]
;@@@ Added by jagriti(13.12.2013)
;She has a smooth voice.
;उसकी आवाज मधुर है . 
(defrule smooth5
(declare (salience 4650))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 voice|jazz)	;added 'jazz' by 14anu-ban-01 on (08-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth5   "  ?id "  maXura )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;A smooth road. 
;The path was smooth.
;पथ सपाट था . 
(defrule smooth6
(declare (salience 4655))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 road|path)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sapAta ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth6   "  ?id "  sapAta )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;She has a smooth skin. 
;उसकी त्वचा मुलायम है.	
(defrule smooth7
(declare (salience 4655))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 skin )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mulAyama ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth7   "  ?id "  mulAyama )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;The receptionist was smooth.
;स्वागती शान्त था . 
(defrule smooth8
(declare (salience 4652))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 receptionist|person )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth8   "  ?id "  SAnwa )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;The journey was smooth.
;यात्रा निर्विघ्न थी .
(defrule smooth9
(declare (salience 4640))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 journey)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirviGna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth9   "  ?id "  nirviGna )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;Regular servicing guarantees the smooth operation of the engine.
;नियमित सर्विसिंग इंजन के आसान संचालन की गारंटी देता है.
(defrule smooth10
(declare (salience 4644))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa  ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 operation|mission|work )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsAna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth10   "  ?id "  AsAna  )" crlf))
)
;@@@ Added by jagriti(13.12.2013)
;The engine is running smooth.
;इंजन आसानी से चल रहा है.
(defrule smooth11
(declare (salience 4000))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma_se ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth11   "  ?id "  ArAma_se )" crlf)))

;@@@ Added by jagriti(28.12.2013)
;From then on the plan they had formulated was carried out with smooth precision. [gyannidhi-corpus]
(defrule smooth12
(declare (salience 4600))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 precision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth12   "  ?id "  sahaja )" crlf))
)


;@@@ Added by 14anu-ban-01 on (08-04-2015) 
;A medic smoothed a wet salve on the sunburned face, neck, and hands.[coca]
;डाक्टर ने धूप-ताम्र चेहरे पर, गरदन, और हाथों पर एक गीला लेप लगाया . [self]
(defrule smooth14
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 salve)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth14   "  ?id "  lagA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (08-04-2015) 
;The man had lived his whole life dealing with smooth talk.[COCA]
;उस आदमी ने अपना पूरा जीवन भुलावा देने वाली खुशामद/बहकाने वाली खुशामद का सामना करते हुए जिया. 
(defrule smooth15
(declare (salience 4655))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 talk)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BulAvA_xene_vAlI_KuSAmaxa/bahakAne_vAlI_KuSAmaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth15  "  ?id "  " ?id1 "   BulAvA_xene_vAlI_KuSAmaxa/bahakAne_vAlI_KuSAmaxa  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (08-04-2015) 
;She was smoothed out by his smooth talk. [coca]
;वह उसकी भुलावा देने वाली खुशामद से बहक गयी . [self]
(defrule smooth16
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-karma ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth16  "  ?id "  " ?id1 "   bahaka_jA  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-04-2015) 
;She spoke to both sides in the dispute in an attempt to smooth things over.[oald]
;उसने चीजों को ठीक करने के प्रयास में  विवाद में [डूबे] दोनों पक्षों से बात की . [self]
(defrule smooth17
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smooth.clp 	smooth17  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth17  "  ?id "  " ?id1 "   TIka_kara  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-04-2015) 
;He smoothed his hair back. [oald]
;उसने अपने केश ठीक किए . [self]
(defrule smooth18
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth18  "  ?id "  " ?id1 "   TIka_kara  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-04-2015) 
;Smooth the icing over the top of the cake.[oald]
;केक के ऊपर सजावट_के_लिए_तैयार_किया_मिश्रण(आइसिंग) फैलाइए .  [self]
(defrule smooth19
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-root ?id1 top)
(kriyA-over_saMbanXI  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth19   "  ?id "  PElA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-04-2015) 
;He will be okay as things will smooth down.[self: with reference to COCA]
;जैसे जैसे चीजें ठीक होंगी वह ठीक हो जायेगा.  [self]
(defrule smooth20
(declare (salience 5000))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(not(kriyA-object ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth20  "  ?id "  " ?id1 "   TIka_ho  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;She was trying to smooth down her hair .[COCA]
;वह अपने केश ठीक करने का प्रयास कर रही थी .  [self]
(defrule smooth21
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth21  "  ?id "  " ?id1 "   TIka_kara  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;Once nails are dry, smooth down ragged cuticles with oil.[COCA]
;जब नाखून सूखे हों,खुरखुरी उपत्वचा को तेल से चिकनी कीजिए .  [self]
(defrule smooth22
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object  ?id ?id2)
(viSeRya-with_saMbanXI  ?id2 ?id3)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cikanA_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  smooth.clp 	smooth22  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth22  "  ?id "  " ?id1 "   cikanA_kara  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;She was smoothing out the creases in her skirt. [oald]
;वह अपने घाघरे की/में पड़ी हुई सिकुडनें हटा रही थी .  [self]
(defrule smooth23
(declare (salience 4800))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-root ?id2 crease|wrinkle)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smooth.clp 	smooth23  "  ?id "  " ?id1 "   hatA )" crlf))
)



;"smooth","Adj","1.cikanA"
;She had a smooth face.
;--"2.maXura"   
;The Ice cream was smooth.
;She has a smooth voice.
;--"3.barAbara"
;The path was smooth.
;--"4.nirviGna"
;The journey was smooth.
;--"5.SAnwa"
;The receptionist was smooth.
;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_smooth5
(declare (salience 4650))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 voice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " smooth.clp   sub_samA_smooth5   "   ?id " maXura )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_smooth5
(declare (salience 4650))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 voice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " smooth.clp   obj_samA_smooth5   "   ?id " maXura )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_smooth12
(declare (salience 4600))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 precision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " smooth.clp   sub_samA_smooth12	" ?id " sahaja )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_smooth12
(declare (salience 4600))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 precision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " smooth.clp  obj_samA_smooth12	" ?id " sahaja )" crlf))
)

;@@@ Added by Anita - 14.5.2014
;Rub the surface smooth. [oxford learner's dictionary]
;सतह को चिकनी रगड़िये ।
(defrule smooth13
(declare (salience 4700))
(id-root ?id smooth)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir*  "  smooth.clp 	smooth13   "  ?id "  cikanA )" crlf))
)
