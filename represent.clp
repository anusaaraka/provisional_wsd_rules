;@@@ Added by Anita 29-11-13
;The crosses on the map represent churches.   [cambridge.org/dictionary/learner-english]
;मानचित्र पर क्रॉस गिरिजाघर के प्रतीक हैं ।
(defrule represent1
(declare (salience 50))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(viSeRya-on_saMbanXI  ?id1 ?id2)
(id-root ?id2 map|road|paper)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent1   "?id"  prawIka )" crlf))
)

;(defrule represent2
;(declare (salience 55))
;(id-root ?id represent)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?id ?)
;(kriyA-subject  ?id ?)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id prawiniXi))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent2   "?id "  prawiniXi )" crlf))
;)

;@@@ Added by Anita 29-11-13
;We represented our grievances/demands to the boss.  [cambridge.;org/dictionary/learnerenglish]
; हमने अपनी समस्याओं को बाँस के सामने पेश किया ।
;He represents himself as an expert, but he knows nothing.
;वह अपने को बहुत निपुण की तरह पेश करता है ,लेकिन उसे कुछ नहीं आता है ।
(defrule represent2
(declare (salience 60))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-to_saMbanXI  ?id ?)(kriyA-as_saMbanXI  ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent2   "?id "  peSa_kara )" crlf))
)

;@@@ Added by Anita 29-11-13
;They chose a famous barrister to represent them in court. [cambridge.org/dictionary/learner-english]
;कोर्ट में उनका प्रतिनिधित्व करने के लिए उन्होंने मशहूर वकील को चुना ।
;Union officials representing the teachers met the government today.  [cambridge.org/dictionary/learner-english]
;अध्यापकों का प्रतिनिधित्व करने वाले यूनियन अधिकारी ने आज सरकार से भेंट की ।
;I sent my husband to represent me at the funeral.        [cambridge.org/dictionary/learner-english]
;अंतिम संस्कार पर अपना प्रतिनिधित्व करने के लिए मैंने अपने पति को भेजा ।
;Women were well/poorly represented at the conference .   [cambridge.org/dictionary/learner-english]
;कांफ्रेंस में बड़ी संख्या में महिलाओं ने प्रतिनिधित्व किया ।
;Mr Smythe represents Barnet.                             [cambridge.org/dictionary/learner-english]
;श्री स्मिथ बर्नेट का प्रतिनिधित्व करते हैं ।
;She will be remembered for her courage.
;उसे उसके साहस के लिए याद किया जायेगा .
(defrule represent3
(declare (salience 50))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-subject ?id ?sub)(saMjFA-to_kqxanwa  ?sub ?id))
(or (id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-cat_coarse ?sub PropN) (add_organizations_list.gdbm))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiniXiwva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent3   "?id "  prawiniXiwva_kara )" crlf))
)
;@@@ Added by 14anu-ban-10 on (13-08-2014)
;This line OQ represents a vector R, that is, the sum of the vectors A and B. [ncert corpus]
;yaha reKA @OQ pariNAmI saxiSa @R ko vyakwa karawI hE jo saxiSoM @A waWA @B kA yoga hE.[ncert corpus]
(defrule represent4
(declare (salience 300))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
;(kriyA-subject  ?id ?id1) ;commented by 14anu-ban-10 on (08-11-2014)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2);commented by 14anu-ban-10 on (08-11-2014)
;(id-root ?id2 line);commented by 14anu-ban-10 on (08-11-2014)
(id-cat_coarse ?id verb)
;(id-cat_coarse ?id1 PropN);commented by 14anu-ban-10 on (08-11-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyakwa_kara));meaning changed from (vyakwa) to (vyakwa_kara) by 14anu-ban-10 on (08-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent4   "?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-08-2014) 
;Since the magnitudes of the two vectors are the same, but the directions are opposite, the resultant vector has zero magnitude and is represented by 0 called a null vector or a zero vector.  [ncert corpus]
;kyofki xo saxiSoM kA parimANa vahI hE kinwu xiSA viparIwa hE, isalie pariNAmI saxiSa kA parimANa SUnya hogA Ora ise @O se vyakwa karawe hEM.[ncert corpus]
;The lengths of the line segments representing these vectors are proportional to the magnitude of the vectors.[ncert]
;ina saxiSoM ko vyakwa karane vAlI reKA - KaNdoM kI lambAiyAz saxiSoM ke parimANa ke samAnupAwI hEM . [ncert]
(defrule represent5
(declare (salience 400))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-by_saMbanXI ?id ? )(viSeRya-kqxanwa_viSeRaNa ? ?id)) ; added 'viSeRya-kqxanwa_viSeRaNa' by 14anu-ban-10 (13-10-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vyakwa_kara)) ;meaning modified from vyakwa to vyakwa_kara by 14anu-ban-10 (13-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent5   "?id "  vyakwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-11-2014) 
;The darkened ends of the needle represent north poles.[ncert corpus]
;suiyoM ke kAle sire uwwarI Xruva praxarSiwa karawe hEM.[ncert corpus]
(defrule represent6
(declare (salience 500))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1 )
(id-root ?id1 pole)  ;added by 14anu-ban-10 on (19-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praxarSiwa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent6   "?id "  praxarSiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-11-2014) 
;Let CE represent a tangent plane drawn from the point C on to the sphere.[ncert corpus]
;mAna lIjie @CE biMxu @C se gole para KIzce gae sparSI wala ko nirUpiwa karawA hE.[ncert corpus]
;Now, if c represents the speed of light in vacuum, then, are known as the refractive indices of medium 1 and medium 2, respectively.[ncert corpus]
;aba yaxi nirvAwa meM prakASa kI cAla ko nirUpiwa karawI hE, waba, kramaSaH mAXyama 1 waWA mAXyama 2 ke apavarwanAfka hEM.[ncert corpus]
(defrule represent7
(declare (salience 600))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1 )
(id-root ?id1  plane|speed)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nirUpiwa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent7   "?id "  nirUpiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-12-2014) 
;The following species are some examples of families representing the most devastating members of Coleoptera in Persia.[agriculture domain]
;निम्नलिखित प्रजातियाँ फारस में चोलेओप्तेराके के सबसे विनाशकारी सदस्यों का प्रतिनिधित्व करने वाले कुलों के कुछ उदाहरण हैं।[manual]
(defrule represent8
(declare (salience 700))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 family)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prawiniXiwva)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent8   "?id "  prawiniXiwva)" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-12-2014) 
;Sigma,which represents summation,looks like a sideways M.[self:with reference to COCA]
;सिग्मा जो कि संकलन  को दर्शाता है,तिरछे M की तरह दिखता है.[self]
(defrule represent9
(declare (salience 800))
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 summation)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xarSA)) 
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent9  "?id "  xarSA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  represent.clp   represent9   "  ?id " ko )" crlf))
)

;------------------------------- Default Rules -----------------------------------

;The cancellation of the new road project represents a victory for protesters. [cambridge.org/dictionary/learner-english]
;नई सड़क परियोजना का रद्द होना प्रदर्शनकारियों की जीत को दर्शाती है ।
;The union represents over 200 employees.                  [cambridge.org/dictionary/learner-english]
; संघ 200 से अधिक कर्मचारियों को दर्शाती  है ।
;The statue represents St George killing the dragon.  [cambridge learner's ;dictionary]
;मूर्ति को  संत जॉर्ज के द्वारा ड्रेगन (सर्प )को मारते हुए दर्शाया गया  है ।
;This book represents ten years of thought and research. [cambridge learner's ;dictionary]
;यह किताब दस वर्ष की सोच और शोध को दर्शाती है ।
;The new offer represented an increase of 10% on the previous one. [cambridge learner's ;dictionary]
;नयी क़ीमत पिछली से  १०% की बढ़ोतरी को दर्शाती है ।
;@@@ Added by Anita 29-11-13
(defrule represent_default
(id-root ?id represent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  represent.clp 	represent_default   "?id "  xarSA )" crlf))
)

