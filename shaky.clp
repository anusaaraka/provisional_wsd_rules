
(defrule shaky0
(declare (salience 5000))
(id-root ?id shaky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kampAyamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shaky.clp 	shaky0   "  ?id "  kampAyamAna )" crlf))
)

(defrule shaky1
(declare (salience 4900))
(id-root ?id shaky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kampAyamAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shaky.clp 	shaky1   "  ?id "  kampAyamAna )" crlf))
)

;"shaky","Adj","1.kampAyamAna"
;Her hands are shaky because she's nervous.
;--"2.asWira"
;The government is looking rather shaky at the moment.
;


;@@@ Added by 14anu-ban-11 on (21-03-2015)
;Business is looking shaky at the moment.(oald)
;उद्योग इस समय कमजोर दिख रहा है . (self)
(defrule shaky2
(declare (salience 5001))
(id-root ?id shaky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 business)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shaky.clp 	shaky2   "  ?id "  asWira)" crlf))
)



;@@@ Added by 14anu-ban-11 on (21-03-2015)
;The child wrote her name in large shaky letters.(cambridge.dic)
;बच्चे ने बडे तेढे मेढे अक्षरो में अपना नाम लिखा .  (self)
(defrule shaky3
(declare (salience 5003))
(id-root ?id shaky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 letter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weDZe_meDZe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shaky.clp 	shaky3   "  ?id "  weDZe_meDZe)" crlf))
)

;
