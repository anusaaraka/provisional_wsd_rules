;@@@ Added by 14anu-ban-06 (4-08-14)
;However, the total energy of an isolated system does not change, as long as one accounts for all forms of energy. (NCERT)
;waWApi UrjA ke saBI rUpoM kA XyAna raKane para hama pAwe hEM ki vilagiwa nikAya kI kula UrjA parivarwiwa nahIM howI.
;Energy may be transformed from one form to another but the total energy of an isolated system remains constant. (NCERT)
;UrjA eka rUpa se xUsare rUpa meM rUpAnwariwa ho sakawI hE paranwu kisI vilagiwa nikAya kI kula UrjA niyawa rahawI hE .
;Since the universe as a whole may be viewed as an isolated system, the total energy of the universe is constant. (NCERT)
;cUfki sampUrNa viSva ko eka vilagiwa nikAya ke rUpa meM xeKA jA sakawA hE awaH viSva kI kula UrjA acara hE .
(defrule isolate2
(declare (salience 5100))
(id-root ?id isolate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) system)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vilagiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  isolate.clp 	isolate2   "  ?id "  vilagiwa )" crlf))
)
;----------------- Default rules ------------------------

(defrule isolate0
(declare (salience 5000))
(id-root ?id isolate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pqWaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  isolate.clp 	isolate0   "  ?id "  pqWaka )" crlf))
)

(defrule isolate1
(declare (salience 4900))
(id-root ?id isolate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  isolate.clp 	isolate1   "  ?id "  alaga_raKa )" crlf))
)

;"isolate","VT","1.alaga_raKanA_"
;They isolated the political prisoners from the other inmates.
;--"pqWaka_karanA"
;Scientists have isolated the virus from the gene.
;
;
