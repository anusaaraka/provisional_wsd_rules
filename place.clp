;"placing","N","1.sWAna"  
;Placings are offered on compleating the training.  
(defrule place0
(declare (salience 5000))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id placing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  place.clp  	place0   "  ?id "  sWAna )" crlf))
)

;$$$ Modified by 14anu-ban-09 on 06-08-2014
;Changed meaning from "sWAna_para_raKa" to "raKa"
;In Fig. 9.2(d), a solid sphere placed in the fluid under high pressure is compressed uniformly on all sides. [NCERT CORPUS]
;चित्र 9.2(d) में, अधिक दाब के किसी द्रव के अंदर रखा एक ठोस गोला सभी ओर से समान रूप से संपीडित हो जाता है.
(defrule place1
(declare (salience 6000))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1) ;Added by 14anu-ban-09
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 noun) ;Added by 14anu-ban-09
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))	;modified 'id-wsd_word_mng' to 'id-wsd_root_mng' by 14anu-ban-09 on (05-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place1   "  ?id "  raKa )" crlf))	;modified 'dir_name-file_name-rule_name-id-wsd_word_mng' to 'dir_name-file_name-rule_name-id-wsd_root_mng' by 14anu-ban-09 on (05-03-2015)
)


;"placed","Adj","1.pAyA huA"
;The placed horce was second in the race.
(defrule place2
(declare (salience 4800))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id placed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pAyA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  place.clp  	place2   "  ?id "  pAyA_huA )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;More than 5000 troops have been placed on alert. [OALD]
;5000 से अधिक दल सचेत स्थिती में तैनात किए गये हैं . 
(defrule place4
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 alert|alarm)
(kriyA-on_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEnAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place4   "  ?id "  wEnAwa_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 21-1-2014
;Ian placed a hand on her shoulder. [OALD]
;इयान ने उसके कन्धे पर हाथ रखा .
(defrule place5
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place5   "  ?id "  raKa )" crlf))
)



;@@@ Added by 14anu-ban-09 on (16-10-2014)
;(b) To determine the temperature of a human body, a thermometer placed under the armpit will always give a temperature lower than the actual value of the body temperature. [NCERT CORPUS]
;(@b) mAnava SarIra kA wApa jFAwa karane ke lie yaxi Apa wApamApI ko bagala meM lagA kara wApa jFAwa kareMge wo yaha wApa SarIra ke vAswavika wApa se saxEva hI kuCa kama AegA. [NCERT CORPUS]

(defrule place6
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 thermometer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place6   "  ?id "  lagA_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-12-2014)
;Let the object be placed at a point O beyond the focus of the first lens A (Fig. 9.21). [NCERT CORPUS]
;mAna lIjie koI biMba pahale leMsa @A ke Pokasa se xUra kisI biMxu @O para sWiwa hE ( ciwra 9.21) . [NCERT CORPUS]

(defrule place7
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place7   "  ?id "  sWiwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-02-2015)
;The company places and removes many labourers a year.		[Hinkhoj]
;कम्पनी हर साल कामगारो को नौकरी देती और हटाती हैं .			[Self] 

(defrule place8
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 company)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place8   "  ?id "  nOkarI_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-02-2015)
;I didnt placed him as I was getting old.	[Hinkhoj]
;मैने उसे/उसको नहीं पहचाना क्योंकि मैं बूढ़ा हो गया था.		[Self]

(defrule place9
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa  ?id ?id1)
(id-root ?id1 old)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place9   "  ?id "  pahacAnA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-02-2015)
;The agency placed about 2 000 secretaries last year. [oald]
;अभिकरण ने पिछले वर्ष करीब 2000 सचिव को नौकरी दी .	      [self] 

(defrule place10
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 secretary)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place10   "  ?id "  nOkarI_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-02-2015)
;We placed an advertisement for a cleaner in the local paper. 	[oald]
;हमने स्थानीय समाचार पत्र में सफाई करने वाले के लिए विज्ञापन दिया.		[Self] 

(defrule place11
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 advertisement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place11   "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-02-2015)
;She was placed in the care of an uncle.		[oald]
;उसे  एक अंगकल की देख-रेख में छोड गये थे. 			[Self]

(defrule place12
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 care)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place12   "  ?id "  Coda )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-03-2015)
;Figure 5.12(b) shows a bar of paramagnetic material placed in an external field.	[NCERT CORPUS]
;चित्र 5.12(b) बाह्य चुम्बकीय क्षेत्र में रखी हुई अनुचुम्बकीय पदार्थ की एक छड प्रदर्शित करता है.	[NCERT CORPUS]
(defrule place13
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id2 ?id)
(id-root ?id2 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 field)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place13   "  ?id "  raKa)" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-03-2015)
;She placed the emphasis on the word 'soon'. [cald]
;उसने 'शीघ्र' शब्द पर  जोर दिया. 	[Manual]
(defrule place14
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 word)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place14   "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-04-2015)
;His resignation placed us in a difficult position.		[oald]
;उसकी रजिस्ट्री ने हमें कठिन परिस्थिति में डाल दिया था. 		[Manual]
(defrule place15
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 position)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place15   "  ?id "  dAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-04-2015)
;The job places great demands on me.  [oald]
;काम मुझपर बडी जिम्मेदारी डाल देता है .	[Manual] 
(defrule place16
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place16   "  ?id "  dAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-04-2015)
;I’ve heard his name before, but I can’t quite place him. [oald]
;मैं पहले उसका नाम सुन चुका हूँ, परन्तु मैं उसको पहचान नहीं सका .	[Manual]
(defrule place17
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id1 ?id2 ?id)
(id-root ?id1 but) 
(id-root ?id2 hear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place17   "  ?id "  pahacAna)" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-04-2015)
;She tried to place the faint West Country burr in his voice.  [oald]
;उसने उसकी आवाज में हल्की वेस्ट कन्ट्री का लहज़े की नकल करने का प्रयास किया .	[Manual]
(defrule place18
(declare (salience 6100))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 burr)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakZala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place18   "  ?id "  nakZala_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-04-2015)
;The blame was placed squarely on the doctor.  [oald]
;दोष सीधा डाक्टर पर लगाया गया था . 		[Manual]
(defrule place19
(declare (salience 5500))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 blame)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place19   "  ?id "  lagA )" crlf))
)


;@@@ Added by Bhagyashri Kulkarni (20-09-2016)
;Did you go to his place?(rapidex)
;क्या आप उसका घर गये?
;What about diner at my place?(oald)
;मेरे घर पर भोजन के बारे में क्या ख्याल है? 
(defrule place20
(declare (salience 5000))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id place )
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Gara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  place.clp  	place20   "  ?id "  Gara )" crlf))
)

;@@@ Added by Bhagyashri Kulkarni (20-09-2016)
;This is my place of work. (oald)
;यह कार्य का मेरा स्थान है . 
(defrule place21
(declare (salience 5000))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id place )
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-root ?id2 work )
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  place.clp  	place21   "  ?id "  sWAna )" crlf))
)


;------------------- Default Rules ----------------
;default_sense && category=verb	raKa xe	0
;"place","V","1.raKa xenA"
;The knife has been placed under the pillow.
(defrule place3
(declare (salience 4700))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place3   "  ?id "  raKa )" crlf))
)

;@@@ Added by Bhagyashri Kulkarni (22-09-2016)
(defrule place22
(declare (salience 4700))
(id-root ?id place)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  place.clp 	place22   "  ?id "  sWAna )" crlf))
)

;default_sense && category=verb	raKa xe	0
;"place","V","1.raKa xenA"
;The knife has been placed under the pillow.
;--"2.pahacAnanA"
;I didn't placed him as I was getting old.
;--"3.jArI raKanA"
;place a order for these pens to the stationer.
;--"4.nOkarI xenA"
;The company places && removes many labourers a year.

