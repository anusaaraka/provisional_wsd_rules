
(defrule amount0
(declare (salience 5000))
(id-root ?id amount)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  amount.clp 	amount0   "  ?id "  mAwrA )" crlf))
)

;@@@ Added by 14anu-ban-02 (15-11-2014)
;Soy milk does not naturally contain significant amounts of digestible calcium.[agriculture]
;सॉय दूध में स्वभाविक रूप से सुपाच्य कैल्शिअम की  महत्त्वपूर्ण मात्रा  नहीं होती हैं . [manual]
(defrule amount3
(declare (salience 5000))
(id-root ?id amount)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1) 	;added by 14anu-ban-02(17-02-2015)
(not(id-cat_coarse ?id1 number))	;added by 14anu-ban-02(17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAwrA))
(assert (id-wsd_number ?id s))	;added by 14anu-ban-02(15-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  amount.clp 	amount3   "  ?id "  mAwrA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number  " ?*prov_dir* "  amount.clp     amount3   "  ?id " s )" crlf)	;added by 14anu-ban-02(15-01-2015)
)
)

;@@@ Added by 14anu-ban-02(17-02-2015)
;Great advances in physics often amount to unification of different theories and domains.[ncert 11_01]
;भौतिकी की महत्वपूर्ण उन्नति प्रायः विभिन्न सिद्धान्तों तथा प्रभाव क्षेत्रों के एकीकरण की ओर ले जाती है.[ncert]
(defrule amount4
(declare (salience 4900))
(id-root ?id amount)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  amount.clp 	amount4   "  ?id "  le_jA )" crlf))
)


;-------------------------------------------Default_Rules---------------------------------------------------------------------------------
;"amount","N","1.rakZama"
;Indians waste a large amount of money on wedding ceremony.
(defrule amount1
(declare (salience 4900))
(id-root ?id amount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakZama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  amount.clp 	amount1   "  ?id "  rakZama )" crlf))
)

;"amount","V","1.ke_barAbara_honA"
;The cost amounted to Rs.250.
(defrule amount2
(declare (salience 4800))
(id-root ?id amount)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_barAbara_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  amount.clp 	amount2   "  ?id "  ke_barAbara_ho )" crlf))
)




;"amount","V","1.ke_barAbara_honA"
;The cost amounted to Rs.250.
;"amount","N","1.rakZama"
;Indians waste a large amount of money on wedding ceremony.
;--"2.kuCa_haxa_waka"
;You can expect a certain amount of confusion in the meeting.
;
;LEVEL 
;Headword : amount
;
;Examples --
;
;"amount","V","1.[kula_rakama]_kA_honA"
;Her debts amounted to Rs.10000.
;usakA uXAra kula kara xasa hajZAra rupaye kA ho gayA.
;--"2.ke_barAbara_honA"
;Her promises amount to nothing.
;usake vAyaxe na ke barAbara hEM.
;
;"amount","N","1.rakama"
;She has paid excess amount for this set.
;usane isa seta ke liye jZyAxA rakama xI hE.
;--"2.mAwrA" <---rakama kI mAwrA
;She can really afford any amount of money.
;vaha vAswava meM kiwanI BI mAwrA meM pEse xe sakawI hE.
;No amount of opposition will deter me.
;viroXI wAkawa jZarA BI nahIM muJe hilA pAyeMgI
;
;
;  nota:--yaxi'amount'Sabxa ke saBI vAkyoM 'kriyA'Ora'saMjFA'ke XyAna xeM wo yaha      niRkarRa nikAla sakawe hE ki saBI arWoM ko mUla arWa 'rakama'evaM'ke samAna'honA          se xiyA jA sakawA hE                                             
;
;             sUwra : rakama[>ke_barAbara]honA
;
;
; 
