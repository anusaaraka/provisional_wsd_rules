;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;It saddened her that people could be so cruel.[oald]
;इस [बात] ने उसको दुखी कर दिया कि लोग इतने निर्दयी भी हो सकते थे .  [self]
;It saddens me to think that we'll never see her again.[cald]
;मुझे यह सोचना दुखी कर देता है कि हम फिर उससे कभी नहीं मिलेंगे . [self]
(defrule sadden1
(declare (salience 1000))
(id-root ?id sadden)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 it)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuHKI_kara_xe/uxAsa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sadden.clp 	sadden1   "  ?id "  xuHKI_kara_xe/uxAsa_kara_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;This confirmation does not sadden him as much as he expected.[coca]
;इस पुष्टिकरण ने उसको उतना उदास नहीं किया जितनी अधिक उसने आशा की थी. [self]
(defrule sadden3
(declare (salience 1000))
(id-root ?id sadden)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAsa_kara/xuHKI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sadden.clp 	sadden3   "  ?id "  uxAsa_kara/xuHKI_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;We were deeply saddened by the news of her death.[oald]
;हम उसकी मृत्यु के समाचार से बहुत अधिक दुःखी थे . [self]
(defrule sadden2
(declare (salience 0))
(id-root ?id sadden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuHKI/uxAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sadden.clp   sadden2   "  ?id "  xuHKI/uxAsa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-04-2015) 
;Fans were saddened to see the former champion play so badly.[oald]
;प्रशंसक भूतपूर्व विजेता को इतनी बुरी तरह से  खेलता हुआ देखकर दुखी हो गये थे .[self]
(defrule sadden0
(declare (salience 0))
(id-root ?id sadden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuHKI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sadden.clp   sadden0   "  ?id "  xuHKI_ho)" crlf))
)

