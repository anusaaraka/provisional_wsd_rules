;----default-rule----
;They learn Russian at school.
;ve vixyAlaya para rUsI sIKawe hEM.
(defrule learn0
(declare (salience 50))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  learn.clp 	learn0   "  ?id "  sIKa )" crlf))
)

;$$$Modified by 14anu-ban-08 (02-04-2015)  ;commented relation and constraint,added relation and constraint
;Modified by Nandini 28-10-13
;We only learned who the new teacher was a few days ago.[advanced oxford leraner dictionary]
;hamane sirPa kuCa xina pahale jAnA nayA SikRaka  kOna WA.
;हमने कुछ दिन पहले जाना नया शिक्षक कौन था.  [self]
(defrule learn1
(declare (salience 5662))     ;salience increased by 14anu-ban-08 (02-04-2015)
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-about_saMbanXI  ?id ?id1)  ;commented by 14anu-ban-08 (02-04-2015)
;(id-word ?id1 learned)
;(id-cat_coarse ?id1 adverb)      ;commented by 14anu-ban-08 (02-04-2015)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma ?id ?id1)       ;added by 14anu-ban-08 (02-04-2015)
(kriyA-subject ?id1 ?id2)         ;added by 14anu-ban-08 (02-04-2015)
(id-root ?id1 be)                 ;added by 14anu-ban-08 (02-04-2015)
(id-root ?id2 teacher)            ;added by 14anu-ban-08 (02-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn1   "  ?id "  jAna )" crlf))
)


;I later learnt that the message had never arrived.
;mEM bAxa meM jAnI ki sanxeSa kaBI nahIM pahuzca gayA.
(defrule learn2
(declare (salience 5660))      ;salience increased by 14anu-ban-08 (02-04-2015)
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-aXikaraNavAcI  ?id ?id1) (kriyA-kriyA_viSeRaNa  ?id ?id1))
(id-cat_coarse ?id1 adverb)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn2   "  ?id "  jAna )" crlf))
)

;We were all shocked to learn of his death.[advanced oxford leraner dictionary]
;usakI mqwyu kI Kabar jAnakar hama sabako  saxamA pahuzcA WA.
(defrule learn3
(declare (salience 5660))          ;salience increased by 14anu-ban-08 (02-04-2015)
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(or(saMjFA-to_kqxanwa ?id1 ?id)(kriyA-kriyArWa_kriyA  ? ?id))
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn3   "  ?id "  jAna )" crlf))
)

;How did they react when they learned the news?[advanced oxford leraner dictionary]
;unakI  kyA prawikriyA WI jaba unhoMne samAcAra sunA?
(defrule learn4
(declare (salience 5660))          ;salience increased by 14anu-ban-08 (02-04-2015)
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 news)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn4   "  ?id "  jAna )" crlf))
)

;It has been learned that 500 jobs are to be lost at the factory.[advanced oxford leraner dict]
;yaha mAluma padA gayA hE ki 500 kAma PEktarI meM Koe jAne hEM.
(defrule learn5
(declare (salience 70))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 it)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAluma_pada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn5   "  ?id "  mAluma_pada )" crlf))
)


;They learned very well.
;unhoMne bahuwa acCA jAnA.
;(defrule learn2
;(declare (salience 70))
;(id-root ?id learn)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-kriyA_viSeRaNa  ?id ?id1)
;(id-cat_coarse ?id verb)
;(id-cat_coarse ?id1 adverb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id siKa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	;learn2   "  ?id "  siKa )" crlf))
;)

;==========Additional-examples==========
;I learnt of her arrival from a close friend.
;I only learnt about the accident later.


;$$$ Modified by 14anu-ban-08 (13-01-2015)       ;changed meaning from 'sIKe' to 'sIKa'
;@@@ Added by 14anu01
;If you write, learn to take photos.
;यदि आप लिखते हैं, तो तस्वीरें लेना सीखे है . 
(defrule learn6
(declare (salience 5000))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) to)
(id-cat_coarse =(+ ?id 2) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIKa))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  learn.clp 	learn6   "  ?id "  sIKa )" crlf))                                   
)


;@@@ Added by 14anu-ban-08 (11-10-2014)
;You will learn more about the significant figures in section 2.7.      [NCERT]
;नुभाग 2.7 में आप सार्थक अङ्कों के विषय में और विस्तार से सीखेंगे.     [NCERT]
(defrule learn7
(declare (salience 5001))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn7   "  ?id "  sIKa )" crlf))
)

;@@@ Added by 14anu-ban-08 (11-10-2014)
;Earlier you learned that motion is change in position of an object with time.     [NCERT]
;पहले आपने पढा है कि किसी वस्तु की स्थिति में परिवर्तन को गति कहते हैं.     [NCERT]
(defrule learn8
(declare (salience 5002))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(kriyA-kriyA_viSeRaNa ?id ?id2)              ;Added Relation by 14anu-ban-08 on (19-11-2014)
(id-root ?id2 earlier)                        ;Added by 14anu-ban-08 on (19-11-2014)
(id-cat_coarse ?id verb)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn8   "  ?id "  paDZa )" crlf))
)

;@@@ Added by 14anu-ban-08 (11-10-2014)
;Therefore, it is first necessary to learn the language of vectors.     [NCERT]
;अतएव सर्वप्रथम हम सदिशों की भाषा (अर्थात सदिशों के गुणों एवं उन्हें उपयोग में लाने की विधियाँ) सीखेंगे .    [NCERT]
(defrule learn9
(declare (salience 5000))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 language)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn9   "  ?id "  sIKa )" crlf))
)

;@@@Added by 14anu-ban-08 (07-02-2015)
;One could add to the first sentence in this box: It learns how to bend as it grows up!   [NCERT]
;इस बॉक्स के पहले वाक्य में आप जोड सकते हैं: बडे होने पर यह सीखता है कि मुडा कैसे जाए!  [NCERT]
(defrule learn10
(declare (salience 5000))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma ?id ?id1)
(kriyA-vAkya_viSeRaNa ?id1 ?id2)
(id-root ?id2 grow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn10   "  ?id "  sIKa)" crlf))
)

;@@@Added by 14anu-ban-08 (13-03-2015)
;We have to learn one of Hamlet's speeches for school tomorrow. [oald]
;कल स्कूल के लिए हमनें हैम्लट की एक पंक्ति याद करी.  [self]
(defrule learn11
(declare (salience 5002))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(viSeRya-of_saMbanXI ?id1 ?id2)
(id-root ?id1 one)
(id-root ?id2 speech)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn11   "  ?id "  yAxa_kara)" crlf))
)

;@@@Added by 14anu-ban-08 (13-03-2015)
;I learnt of her arrival from a close friend. [oald]
;करीबी दोस्त ने मुझे उसके आने की खबर दी. [self]
(defrule learn12
(declare (salience 5002))
(id-root ?id learn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI ?id ?id1)
(id-root ?id1 arrival)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kabara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  learn.clp  	learn12   "  ?id "  Kabara_xe)" crlf))
)


