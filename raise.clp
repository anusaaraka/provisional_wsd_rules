;@@@ Added by Anita-3.12.2013
;Would all those in favour please raise their hands? [cambridge Dictionary]
;जो पक्ष में हों कृपया अपने हाथ उठाएं ।
;He raised the window and leaned out. [cambridge Dictionary]
;उसने खड़की उठाई और बाहर झुका ।
(defrule raise1
(declare (salience 4975))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object  ?id ?)(kriyA-kriyArWa_kriyA  ? ?id)) ; relation added by Anita-31.5.2014
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise1   "  ?id "  Upara_uTA )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (12-12-2014)
;@@@ Added by Anita-3.12.2013
;Mary Quant was the first fashion designer to raise hemlines. [cambridge Dictionary]
;मैरी क्वॉन्ट पहली ऐसी फैशन डिज़ाइनर थीं जिन्होंने पोशाक के निचले किनारों को ऊँचा किया ।
(defrule raise2
(declare (salience 5000))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id1 ?id) ;added ?id1 by 14anu-ban-10 on (12-12-2014)
(id-root ?id1 designer) ;added by 14anu-ban-10 on (12-12-2014)
;(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UZcA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise2   "  ?id "  UZcA_kara )" crlf))
)
;@@@ Added by Anita-3.12.2013
;Her answers raised doubts in my mind. [cambridge Dictionary]
;उसके जवाबों ने मेरे दिमाग में कई शंकाएं पैदा कीं ।
(defrule raise3
(declare (salience 4985))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 mind)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise3   "  ?id " pExA_kara )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (02-08-2014)
;The vessel is raised slightly till the liquid just touches the glass plate and pulls it down a little because of surface tension.  
;xrava se Bare pAwra ko WodZA Upara uTAwe hEM wAki yaha kAzca kI pleta ke kREwija kinAroM ko CUne Bara lage Ora pqRTa wanAva ke kAraNa pleta ko nIce kI ora KIMcane lage.
;@@@ Added by Anita-3.12.2013
;The announcement raised a cheer. 
;इस चर्चा से हर्ष की लहर उठी ।
;Discussion has raised many important issues.  
;इस चर्चा से कई महत्वपूर्ण मुद्दे उठते हैं ।
(defrule raise4
(declare (salience 4985))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 question|cheer|issue|vessel) ;added vessel by 14anu-ban-10
(or(kriyA-object  ?id ?id1)(kriyA-kriyA_viSeRaNa  ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise4   "  ?id "  uTa )" crlf))
)

;+++ Re-modified by Sukhada 2/4/14
;@@@ Added by Anita-3.12.2013
;I want to start my own business if I can raise the money. [cambridge Dictionary]
;अगर मैं धन इकट्ठा कर सकूँ तो मैं अपना निजी धंधा शुरू करना चाहूँगा ।
(defrule raise5
(declare (salience 4980))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj money|fund) ;added by Sukhada 2/4/14
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikatTA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise5   "  ?id "  ikatTA_kara )" crlf))
)
;@@@ Added by Anita-3.12.2013
;The chapel was raised as a memorial to her son. [cambridge Dictionary]
;यह गिरजाघर उसने अपने लड़के की यादगार के रूप में बनवाया ।
(defrule raise6
(declare (salience 4999))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 son|daughter|wife|husband|father|mother)
(or(kriyA-as_saMbanXI  ?id ?)(kriyA-to_saMbanXI  ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise6   "  ?id "  banavA )" crlf))
)
;@@@ Added by Anita-3.12.2013
;I've been trying to raise Jack all day. [cambridge Dictionary]
;मैं दिन भर से जैक से संपर्क करने की कोशिश कर रहा हूँ ।
(defrule raise7
(declare (salience 4999))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-kriyArWa_kriyA  ? ?id)(to-infinitive  ? ?id))
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id1 PropN|pronoun)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samparka_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise7   "  ?id "  saMparka_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  raise.clp      raise7   "  ?id " ko )" crlf))
)
;$$$ modifed by Anita-31.5.2014
;@@@ Added by Anita-3.12.2013
;They agreed to raise the trade embargo if three conditions were met. [cambridge Dictionary]
;वे व्यापार प्रतिरोघ समाप्त करने के लिए तैयार हो गए यदि तीन शर्तें मान ली जाती हैं तो  ।
(defrule raise8
(declare (salience 4995))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 embargo|blockade|ban|siege) ; added by Anita-31.5.2014
(kriyA-object  ?id ?id1); condition changed by Anita-31.5.2014 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise8   "  ?id "  samApwa_kara )" crlf))
)
;@@@ Added by Anita-3.12.2013
;After three weeks the siege was raised. [cambridge Dictionary]
;तीन हफ्तों के बाद घेराबंदी हटा ली गई ।
;$$$ modified by 14anu26 for the above sentence   [23-06-14] -- removed (kriyA-karma  ?id ?)
(defrule raise9
(declare (salience 4998))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-after_saMbanXI  ?id ?id1)  ;added by 14anu26
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise9   "  ?id "  hatA_le )" crlf))
)
;@@@ Added by Anita-3.12.2013
;I want to raise two problems with you. [cambridge Dictionary]
;मैं आपके सामने दो समस्याएं रखना चाहता हूँ ।
(defrule raise10
(declare (salience 4999))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise10   "  ?id "  raKa )" crlf))
)
;$$$ Modified by 14anu-ban-10 on (12-12-2014)
;@@@ Added by Anita-13.3.2014
;The dimensions of a physical quantity are the powers or exponents to which the base quantities are raised to represent that quantity. [ncert]
;किसी भौतिक राशि की विमाएँ उन घातों (या घाताङ्कों) को कहते हैं, जिन्हें उस राशि को व्यक्त करने के लिए मूल राशियों पर चढ़ाना ;पड़ता है ।
(defrule raise11
(declare (salience 5100))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1) ;added ?id1 by 14anu-ban-10 on (12-12-2014)
(id-root ?id1 which) ;added by 14anu-ban-10 on (12-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise11   "  ?id "  caDZA)" crlf))
)

;@@@ Added by Anita-29.3.2014
;The rule by convention is that the preceding digit is raised by 1 if the insignificant digit to be dropped the underlined digit in ;this case is more than 5 and is left unchanged if the latter is less than 5. [ncert]
;परिपाटी के अनुसार नियम यह है कि यदि उपेक्षणीय अङ्क (पूर्वोक्त सङ्ख्या में अधोरेखाङ्कित अङ्क) 5 से अधिक है तो पूर्ववर्ती अङ्क में एक की वृद्धि कर दी जाती है, और यदि यह उपेक्षणीय अङ्क 5 से कम होता है, तो पूर्ववर्ती अङ्क अपरिवर्तित रखा जाता है।
(defrule raise12
(declare (salience 5200))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise12   "  ?id "  vqxXi_kara)" crlf))
)

;@@@ Added by 14anu26  [23-06-14] 
;Eyebrows were raised when they refused to negotiate.
;भौं ताने गये थे जब उन्होंने पहले बातचीत करने के लिए मना किया .
(defrule raise13
(declare (salience 4998))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?sub) 
(id-root ?sub eyebrow)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise13   "  ?id "  wAna )" crlf))
)

;@@@ Added by Anita-27-06-2014
;Animal welfare campaigners raised £ 70000 for their cause last year. [By mail] 
;पशु देखभाल अभियान प्रचारकों ने पिछले वर्ष उनके उद्देश्य के लिए £ 70000 इकट्ठा किए ।
(defrule raise013
(declare (salience 4980))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 campaigner)
(kriyA-for_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikatTA_kara))
(assert (id-wsd_viBakwi ?id1 ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise013   "  ?id "  ikatTA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  raise.clp 	raise013   "  ?id1 "  ne )" crlf)
)
)

;The udgata seated himself on a high seal , asandi ( a raised seat ) , and the others on grass mats during the yajna ritual and
; chanted the mantras 
;उद्गाता ऊंचे आसन असंधि पर बैठता था और यज्ञ के दौरान शेष घास की चटाई पर बैठते थे और मंत्र पाठ करते थे .
;@@@ Added by 14anu11
(defrule raise15
(declare (salience 4975))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-viSeRaNa  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Uve))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise15   "  ?id "  Uve )" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-08-2014)
; In order to answer this question in the first step, heat a given quantity of water to raise its temperature by, say 20 ° C and note the time taken.[ncert corpus]
; praWama kaxama meM isa praSna ke uwwara xene ke lie, ke xvArA usakA wApa UcA karane ke lie eka xiyA huA parimANa pAnI kA garama karawA hE, 20 ° C kahawA hE Ora liyA huA samaya XyAna se xeKawA hE . [ncert corpus]
(defrule raise14
(declare (salience 5200))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id1 ?id);relation modified from (kriyA-object) to (saMjFA-to_kqxanwa) by 14anu-ban-10 on (05-11-2014)
(id-root ?id1 quantity) ;added by 14anu-ban-10 on (12-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise14   "  ?id "  UzcA_kara )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (13-04-2015)   ;changed meaning from 'jutAne' to 'jutA' and added words 'loan|money|fund'
;He needed to raise a loan in order to set up in business.[OALD]
;उसे  व्यापार को स्थापित करने के लिए एक ऋण जुटाने की जरूरत है.   [MANUAL]
;@@@ Added by 14anu-ban-10 on (04-02-2015)
;We tried to raise eighty thousand pounds, but unfortunately we fall short by about ten thousand.[wordreference forum]
;हमने अस्सी हजार पाउन्ड जुटाने का प्रयास किया, परन्तु दुर्भाग्य से हमे लगभग दस हजार की कम रह गयी हैं .[self];Translation modified by 14anu-ban-10 on (12-02-2015)
(defrule raise16
(declare (salience 5300))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 pound|loan|money|fund) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jutA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise16   "  ?id "  jutA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;The opposition raised its voice against the womens bill.[hinkhoj]
;विपक्ष ने महिला बिल  के खिलाफ विरोध कर उनकी आवाज ऊपर उठाई . [manual]
(defrule raise17
(declare (salience 5400))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 opposition)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise17   "  ?id "  viroXa_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;There were many raised eyebrows when people saw him handcuffed.[hinkhoj]
;वहाँ पर बहुत से  घृणा प्रकट करते भौंह थे  जब लोगो ने उसके  हथकड़ी देखी.[manual]
(defrule raise18
(declare (salience 5500))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 eyebrow)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GqNA_prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise18   "  ?id "  GqNA_prakata_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;She raised her offer to Rs.3000.[hinkhoj]
;उसने वृद्धि करी उसके प्रस्ताव मे Rs.3000 तक.[manual]
(defrule raise19
(declare (salience 5600))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 offer)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise19   "  ?id " vqxXi_kara ) " crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;Why dont you raise funds for charity.[hinkhoj]
;अाप धन एकत्र करते क्यों नही भिक्षा के लिये.[manual]
(defrule raise20
(declare (salience 5700))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
 (kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 charity)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ekawra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise20   "  ?id "   ekawra_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;My victory in the final raised my spirits.[hinkhoj]
;मेरी  विजय ने अंतिम खेल मे मेरे उत्साह को प्रसन्न किया.[manual]
(defrule raise21
(declare (salience 5800))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 spirit)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasanna_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise21   "  ?id "  prasanna_kara ) " crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;She told us all her jokes,but she couldnt even raise a smile.[hinkhoj]
;उसने हमें उसके सब चुटकुले बताये, परन्तु वह एक भी मुस्कराहट उत्पन्न करना कर पायी.[manual]
(defrule raise22
(declare (salience 5900))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 smile)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise22   "  ?id "  uwpanna_kara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-03-2015)
;My parents died when I was young so I was raised by my aunt.[hinkhoj]
;मेरे माँ बाप मर गये जब मैं छोटा था इसलिए मैं मेरी चाची ने मुझे बड़ा किया . [manual]
(defrule raise23
(declare (salience 6000))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
 (kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 aunt)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  badZA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise23   "  ?id "  badZA_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-03-2015)
;Please dont raise a cloud of dust.
;धूल का गुबार कृपया मत ऊपर उठाना .
(defrule raise24
(declare (salience 6100))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 cloud)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Upara_uTAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise24   "  ?id " Upara_uTAnA ) " crlf))
)

;@@@ Added by 14anu-ban-05 on (11-04-2015)
;The campaign aims to raise awareness of the risks of illegal drugs. [OALD]
;अभियान अवैध दवाओं के खतरों के बारे में जागरूकता बढ़ाने के लिए करना है.		[MANUAL]
;The government has promised not to raise taxes.[OALD]
;सरकार ने  कर नहीं बढ़ाने का वादा किया है .			[MANUAL] 
(defrule raise25
(declare (salience 6101))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 awareness|tax|price)	;added 'tax|price' by 14anu-ban-05 on (13-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise25   "  ?id " baDA ) " crlf))
)


;@@@ Added by 14anu-ban-05 on (13-04-2015)
;Farmers cleared the land in order to raise cattle. [OALD]
;किसानों ने  पशु  पालने के लिए भूमि साफ की. 	[MANUAL]
(defrule raise26
(declare (salience 6101))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 cattle)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise26   "  ?id " pAla ) " crlf))
)

;@@@ Added by 14anu-ban-05 on (13-04-2015)
;I’ve never heard him even raise his voice. [OALD]
;मैंने  कभी भी  उसको अपनी आवाज ऊँची करते नहीं सुना  हूँ . 	[MANUAL]
(defrule raise27
(declare (salience 6100))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 voice)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise27   "  ?id "  UzcA_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (13-04-2015)
;The sale raised over £3 000 for charity. [OALD]
;बिक्री ने दान के लिए 3000 पाउंड के ऊपर  एकत्र किया . [MANUAL]
(defrule raise28
(declare (salience 6100))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(kriyA-over_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise28   "  ?id "  ekawra_kara )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (13-04-2015)
;They raise corn, soybeans and alfalfa on 460 acres. [OALD]
;वे 460 एकड पर मकई का दाना, सोयाबीन और चारा उपजाते हैं . 		[MANUAL]
(defrule raise29
(declare (salience 6101))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 corn|soyabean|alfalfa|wheat|rice)		;more constraints can be added	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise29   "  ?id "  upajA )" crlf))
) 


;@@@ Added by 14anu-ban-05 on (13-04-2015)
;They raised her as a Catholic.	[OALD]
;उन्होंने एक कैथोलिक के रूप में उसे  विकसित किया. [MANUAL]
(defrule raise30
(declare (salience 6101))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikasiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise30   "  ?id "  vikasiwa_kara )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (13-04-2015)
;Somehow we managed to raise him to his feet. [OALD]
;किसी तरह हमने उसको अपने पैरों पर खड़ा करने में कामयाब रहे.	[MANUAL]
(defrule raise31
(declare (salience 6101))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 foot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise31   "  ?id "  KadA_kara )" crlf))
) 


;##########################################default-rule############################################

;$$$ Modified by Anita-3.12.2013 changed mng from "vqxXi" to "vewana_vqxXi" 
;She asked the boss for a raise. [cambridge Dictionary]
;उसने अपने प्रमुख से वेतन वृद्धि के लिए कहा ।
(defrule raise0
;(declare (salience 5000))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vewana_vqxXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp 	raise0   "  ?id "  vewana_vqxXi )" crlf))
)

;Replaced 'Upara_uTA' with 'uTA' by Shirisha Manju Suggested by Chaitanya Sir (05-09-13)
;This naturally raises the questions like: Is the converse effect possible?
(defrule raise_default_vrule
;(declare (salience 4900))
(id-root ?id raise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  raise.clp     raise_default_vrule   "  ?id "  uTA )" crlf))
)

;"raise","VT","1.pExA_kara"

;Please don't raise a cloud of dust.  
;--"2.KadZA_karanA"    
;Go && raise a fallen child.
;--"3.vqxXi_karanA_/baDZAnA"   
;She raised her offer to Rs.3000.   
;--"4.ekawra_karanA"  
;Why don't you raise funds for charity.  
;--"5.uwpanna_karanA"   
;She told us all her jokes,but she couldn't even raise a smile. 
;--"6.badZA_karanA"   
;My parents died when I was young so I was raised by my aunt. 
;--"7.GqNA_prakata_karanA"   
;There were many raised eyebrows when people saw him handcuffed. 
;--"8.prasanna_honA"    
;My victory in the final raised my spirits.
;--"9.viroXa_karanA"   
;The opposition raised its voice against the women's bill. 
;
;LEVEL 
;
;
;                `raise' sUwra (nibanXa)
;                -------           
;
;`raise' Sabxa ke viviXa prayoga--
;-------------------------   
; 
;"raise","VT","1.Upara_uTAnA"    
;                  ---- < Upara uTAnA`
;Please don't raise a cloud of dust.
;--"2.KadZA_karanA"    
;                  ---- < Upara uTAnA`
;Go && raise a fallen child.
;--"3.vqxXi_karanA_/baDZAnA"
;                  ---- < Upara uTAnA`
;She raised her offer to Rs.3000.
;--"4.ekawra_karanA" 
;                  ---- < eka se aXika karanA < -- vqxXi karanA < -- Upara uTAnA`
;Why don't you raise funds for charity.
;--"5.uwpanna_karanA"
;                  ---- < vqxXi karanA <-- Upara uTAnA`
;She told us all her jokes,but she couldn't even raise a smile.
;--"6.badZA_karanA"   
;                  ---- < Upara uTAnA`
;My parents died when I was young so I was raised by my aunt.
;--"7.GqNA_prakata_karanA" 
;                  ---- < kisI cIja kA Upara uTanA <-- Upara uTAnA`
;There were many raised eyebrows when people saw him handcuffed.
;--"8.uwsAha_baDAnA" 
;                  ---- < uwsAha kI vqxXi karanA <-- Upara uTAnA` 
;My victory in the final raised my spirits.
;--"9.viroXa_karanA"   
;                  ---- < AvAja ko (Upara)uTAnA <-- Upara uTAnA`
;The opposition raised its voice against the women's bill.
;
;"raise (US=rise)","N","1.baDZowarI"
;                  ---- < vqxXi karanA <-- Upara uTAnA`

;Should I ask my boss for a rise/raise?,a five per cent pay rise/raise.
;-------------------------------------------------------------------------
;
;
;sUwra : vqxXi[<Upara_uTAnA`]
;
;----------------
;
;   aMgrejI ke isa Sabxa ke viBinna prayoga hinxI meM xeKe jA sakawe hEM, Upara xiye
;gaye alaga-alaga vAkyoM xvArA . ina saBI vAkyoM meM eka `Upara uTAne kA BAva'
;sAmAnya hEM . hinxI meM BI isa `Upara uTane ke BAva' ke viviXa sanxarBa prayukwa
;hEM . Upara ke vAkyoM xvArA isa waWya ko samaJA jA sakawA hE . tippaNiyoM se
;yaha Ora BI spaRta hE . kuCa ke vivaraNa yahAz xeKawe hEM--
;
;-- `ekawra karanA' . isa waraha ke BAvArWavAle vAkya ke mUla meM Upara uTAne kA BAva hE . 
;Upara uTAnA arWAw vqxXi karanA . vqxXi hone kI samBAvanA ekawriwa karane ke kArya 
;meM nihiwa hE . isa waraha isakA arWa-viswAra samaJA jA sakawA hE . 
;
;-- `GqNA prakata karanA' . isakA BAva kisI SArIrika aBivyakwi ke Upara uTAne se 
;AyA huA hE . jaba koI vyakwi GqNA ke BAva ko prakata karanA cAhawA hE wo vaha BOMhoM
;kA caDAnA avaSya karawA hE . isase yaha arWa-viswAra samaJA jA sakawA hE . 
;
;-- `viroXa karanA' . hinxI meM BI isa prakAra kA prayoga xeKA jA sakawA hE . bahuwa 
;prasaMgoM meM jaba viroXa prakata karane ke BAva ko kahanA howA hE wo `AvAja uTAne'
;yA `AvAja UzcA karane' ke kriyArUpa kA prayoga kiyA jAwA hE .
;
;-- anya prayoga tippaNiyoM se saralawayA sajaJe jA sakawe hEM .  

