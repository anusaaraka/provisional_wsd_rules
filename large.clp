
(defrule large0
(declare (salience 5000))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id larger)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXika_viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  large.clp  	large0   "  ?id "  aXika_viSAla )" crlf))
)

;$$$ Modified by 14anu-ban-08 (28-02-2015)
;A large fort.  [same clp file]
;विशाल किला.   [self]
(defrule large1
(declare (salience 4900))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
(viSeRya-viSeRaNa ?id1 ?id)    ;Added by 14anu-ban-08 (28-02-2015)
(id-root ?id1 fort|crowd|view) ;Added by 14anu-ban-08 (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp 	large1   "  ?id "  viSAla )" crlf))
)

;"large","Adj","1.viSAla"
;A large crowd. A large fort
;--"2.vyApaka"
;A large view. A large && knotty problem.
;
;


;$$$ Modified by 14anu-ban-08 (24-02-2015)   ;added constraint
;@@@ Added by 14anu17
;There is a large range of drugs.
;दवाओं की एक बहुत  श्रेणी  है.
(defrule large3
(declare (salience 6503))            ;Salience is increased from 6500 to 6503 by 14anu-ban-08 (12-01-2015)
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 range)              ;added by 14anu-ban-08 (24-02-2015)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp    large3   " ?id " bahuwa)" crlf))
) 

;$$$Modified by 14anu-ban-02(05-02-2016)
;He ladles it with a large wooden spoon.[sd_verified]
;वह बड़ी लकड़ी के चम्मच से इसको परसता है .[self] 
;@@@ Added by 14anu-ban-08 (07-11-2014)
;She enjoyed the anonymity of life in a large city.  [mw]
;उसने बड़े शहर में जीवन की गुमनामी का आनन्द उठाया . [self]    ;suggested by chaitanaya sir.
;Water proofing agents on the other hand are added to create a large angle of contact between the water and fibers.  [NCERT]
;पानी तथा रेशों के बीच सम्पर्क कोण बडा करने के लिए पानी में जल सहकारक को मिलाया जाता है.  [NCERT]
;Next we lightly sprinkle some lycopodium powder on the surface of water in a large trough and we put one drop of this solution in the water.    [NCERT]                     
;इसके बाद एक बडे नान्द में पानी लेकर, उसके ऊपर लायकोपोडियम पाउडर छिडक कर, लाइकोपोडियम पाउडर की एक पतली फिल्म जल के पृष्ठ के ऊपर बनाते हैं ; फिर ओलीक अम्ल के पहले बनाए गए घोल की एक बून्द इसके ऊपर रखते हैं.    [NCERT]
;The larger ones acquire negative charge.   [NCERT]
;बडे वाले ऋणात्मक आवेश प्राप्त करते हैं.   [NCERT]
(defrule large4
(declare (salience 6505))           ;salience increased by 14anu-ban-08 (06-02-2015)
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 one|trough|number|herd|angle|child|city|spoon) ;added 'trough|number' by 14anu-ban-08 (08-12-2014)  ;added 'herd' by 14anu-ban-08 (06-02-2015)  ;added 'angle' by 14anu-ban-08 (16-02-2015)	;spoon is added by 14anu-ban-02(05-02-2016)
 ;added 'child' by 14anu-ban-08 (24-02-2015) ;added 'city' by 14anu-ban-08 (03-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp    large4   " ?id " badA)" crlf))
)

;@@@ Added by 14anu20 on 19/06/2014
;His fortune is not large.
;उसका भाग्य अच्छा नहीं हैं .
(defrule large5
(declare (salience 6600))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 fortune)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp    large5   " ?id " acCA)" crlf))
)  


;@@@ Added by 14anu-ban-08 (06-12-2014)
;Is this a curious coincidence between these large numbers purely accidental? [NCERT]
;क्या इन विशाल सङ्ख्याओं की यह आश्चर्यजनक, अनुरूपता मात्र संयोग है?  [NCERT]
;To some people the excitement comes from the elegance and universality of its basic theories, from the fact that a few basic concepts and laws can explain phenomena covering a large range of magnitude of physical quantities.     [NCERT]
;कुछ व्यक्ति इसके मूल सिद्धान्तों के लालित्य तथा व्यापकता से इस तथ्य को लेकर उत्तेजित हो जाते हैं कि भौतिकी की कुछ मूल सङ्कल्पनाओं तथा नियमों द्वारा भौतिक राशियों के विशाल परिसर को प्रतिपादित करने वाली परिघटनाओं की व्याख्या की जा सकती है.    [NCERT]
(defrule large6
(declare (salience 6506))  ;salience increased from 6501 to 6506 by 14anu-ban-08 (28-02-2015)
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 range|mass|number)  ;added 'number' by 14anu-ban-08 (09-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp 	large6   "  ?id "  viSAla )" crlf))
)


;@@@ Added by 14anu-ban-08 (06-12-2014)
;Like the gravitational force, electromagnetic force acts over large distances and does not need any intervening medium.    [NCERT]
;गुरुत्वाकर्षण बल की भान्ति विद्युत चुम्बकीय बल भी काफी लम्बी दूरियों तक कार्यरत रहता है तथा इसे किसी मध्यवर्ती माध्यम की भी आवश्यकता नहीं होती.     [NCERT]
(defrule large7
(declare (salience 6506))          ;salience increased from 6501 to 6506 by 14anu-ban-08 (12-02-2015)
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id3 ?id)
(id-root ?id3 distance)     
(kriyA-over_saMbanXI ?id1 ?id3)      ;added by 14anu-ban-08 (12-02-2015)
(kriyA-subject ?id1 ?id2)            ;added by 14anu-ban-08 (12-02-2015)
(id-root ?id2 force)                ;added by 14anu-ban-08 (12-02-2015)
(id-root ?id1 act)                  ;added by 14anu-ban-08 (12-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp 	large7   "  ?id "  lambA )" crlf))
)

;$$$ Modified by Roja , Suggested by Chaitanya Sir (21-11-19)
;###Counter Example###: The tables may become [very large].
;To stop firing this rule from above example added relation subject-subject_samAnAXikaraNa and root fact
;@@@ Added by 14anu-ban-08 (06-12-2014)
;Although the number of physical quantities appears to be very large, we need only a limited number of units for expressing all the physical quantities, since they are inter-related with one another.    [NCERT]
;यद्यपि हमारे द्वारा मापी जाने वाली भौतिक राशियों की सङ्ख्या बहुत अधिक है, फिर भी, हमें इन सब भौतिक राशियों को व्यक्त करने के लिए, मात्रकों की सीमित सङ्ख्या की ही आवश्यकता होती है, क्योंकि, ये राशियाँ एक दूसरे से परस्पर सम्बन्धित हैं.     [NCERT]
(defrule large8
(declare (salience 6502))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) very)   
(subject-subject_samAnAXikaraNa  ?sub ?id) 
(id-root ?sub number)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) bahuwa_aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " large.clp  large8  "  ?id "  " (- ?id 1) "  bahuwa_aXika )" crlf))
)

;@@@ Added by 14anu-ban-08 (08-12-2014)
;The dimensional formulae of a large number and wide variety of physical quantities, derived from the equations representing the relationships among other physical quantities and expressed in terms of base quantities are given in Appendix 9 for your guidance and ready reference.     [NCERT]
;विविध प्रकार की बहुत सी भौतिक राशियों के विमीय सूत्र, जिन्हें अन्य भौतिक राशियों के मध्य सम्बन्धों को निरूपित करने वाले समीकरणों से व्युत्पन्न तथा मूल राशियों के पदों में व्यक्त किया गया है, आपके मार्गदर्शन एवं तात्कालिक सन्दर्भ के लिए परिशिष्ट-9 में दिए गए हैं.     [NCERT]
(defrule large9
(declare (salience 6502))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) number)     
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) bahuwa_sI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " large.clp  large9  "  ?id "  " (+ ?id 1) "  bahuwa_sI )" crlf))
)

;@@@ Added by 14anu-ban-08 (09-02-2015)
;Her killer is still at large.  [oald]
;उसका क़ातिल अभी भी गायब हैं.    [self]
(defrule large10
(declare (salience 6501))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id)(preposition ?id3))
(id-cat_coarse ?id adjective)
(kriyA-subject ?id2 ?id1)
(kriyA-at_saMbanXI ?id2 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))   ;modify 'killer' to 'animate file' by 14anu-ban-08 (20-02-2015)
(id-root ?id3 at)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id3 gAyaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " large.clp	large10  "  ?id "  " ?id3 "  gAyaba )" crlf))
)

;@@@ Added by 14anu-ban-08 (09-02-2015)
;We shall demonstrate that at large distances this axial field resembles that of a bar magnet. [NCERT]
;हम यह प्रदर्शित करेंगे कि बहुत अधिक दूरी पर यह अक्षीय क्षेत्र छड चुम्बक के अक्षीय क्षेत्र जैसा ही है.  [self]
(defrule large11
(declare (salience 6504))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(- ?id 1) at)         ;added by 14anu-ban-08 (21-02-2015)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 distance)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) bahuwa_aXika))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " large.clp  large11  "  ?id "  " (- ?id 1) "  bahuwa_aXika )" crlf)) 
)

;@@@Added by 14anu-ban-08 (18-02-2015)
;We also use certain special length units for short and large lengths.  [NCERT]
;अत्यन्त सूक्ष्म और बहुत बडी दूरियों के मापन के लिए हम लम्बाई के कुछ विशिष्ट मात्रक भी प्रयोग में लाते हैं.  [NCERT]
;अत्यन्त सूक्ष्म और लम्बी दूरियों के मापन के लिए हम लम्बाई के कुछ विशिष्ट मात्रक भी प्रयोग में लाते हैं.  [self]
(defrule large12
(declare (salience 6505))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)    
(id-root ?id1 length)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp 	large12   "  ?id "  lambA )" crlf))
)

;@@@ Added by 14anu-ban-08 (23-02-2015)
;The reason is that Jharkhand is such a state which is untouched to a large extent by the side effects of urbanization.  [Tourism]
;कारण  यह  है  कि  ,  झारखंड  एक  ऐसा  राज्य  है  जो  शहरीकरण  के  दुष्प्रभाव  से  काफी  हद  तक  अछूता  है  ।  [Tourism]
(defrule large13
(declare (salience 6505))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 extent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp 	large13   "  ?id "  kAPI )" crlf))
)


;@@@Added by 14anu-ban-08 (25-02-2015)
;The larger the momentum, the larger is the radius and bigger the circle described. [NCERT]
;जितना अधिक संवेग होगा उतनी ही अधिक निर्मित वृत्त की त्रिज्या होगी तथा निर्मित वृत्त भी बडा होगा.  [NCERT]
(defrule large14
(declare (salience 6505))
(id-word ?id larger)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id ?id1)(kriyA-vAkyakarma ?id ?id2)) 
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  large.clp 	large14   "  ?id "  aXika )" crlf))
)

;@@@ Added by 14anu-ban-08 (18-04-2015)
;It was necessary to strengthen the building with large external buttresses.   [oald]
;इमारत  को चौड़ी बाहरी टेक से मजबूत बनाना आवश्यक था .  [self] 
(defrule large16
(declare (salience 6505))
(id-word ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 buttress)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cOdZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  large.clp 	large16   "  ?id "  cOdZI )" crlf))
)

;------------------------Default Rule--------------------------------
;$$$ Modified by Roja(21-11-19) Suggested by Chaitanya Sir
;### Counter Example ### The tables may become very large. 
;For above purpose added id-root fact 'too' . 
;@@@ Added by 14anu-ban-08 (28-02-2015)
;If the speed is too large, or if the turn is too sharp (i.e. of too small a radius) or both, the frictional force is not sufficient to provide the necessary centripetal force, and the cyclist slips.  [NCERT]
;If the speed is too large, or if the turn is too sharp (i.e. of too small a radius) or both, the frictional force in this situation is not sufficient to provide the necessary centripetal force, and the cyclist slips.  [self]
;यदि चाल बहुत अधिक है, तथा/अथवा मोड अत्यधिक तीव्र है (अर्थात् त्रिज्या बहुत कम है), तब घर्षण बल इन स्थितियों में आवश्यक अभिकेंद्र बल प्रदान करने के लिए पर्याप्त नहीं होता और साइकिल सवार मोड लेते समय फिसल कर गिर जाता है. [NCERT]
;यदि गति बहुत अधिक है, तथा/अथवा मोड अत्यधिक तीव्र है (अर्थात् त्रिज्या बहुत कम है), तब घर्षण बल इन स्थितियों में आवश्यक अभिकेंद्र बल प्रदान करने के लिए पर्याप्त नहीं होता और साइकिल सवार मोड लेते समय फिसल कर गिर जाता है. [self]
(defrule large15
(declare (salience 0))
(id-root ?id large)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(- ?id 1) too)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  large.clp  	large15   "  ?id "  aXika )" crlf))
)


;Added by Veena Bagga (06-01-2010)
(defrule large_default 
(declare (salience -1))
(id-root ?id large)
;(Any)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  large.clp    large_default   " ?id " badA)" crlf))
)
;He is a large child for his age . 
