;@@@ Added by 14anu-ban-05 on (04-12-2014)
;The prisoner claimed the police had fabricated his confession.[OALD]
;कैदी ने दावा किया पुलीस ने उसकी स्वीकारोक्ति  को बनाया  था.  [MANUAL]
(defrule fabricate0
(declare (salience 1000))
(id-root ?id fabricate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fabricate.clp 	fabricate0   "  ?id "  banA )" crlf))
)	

;@@@ Added by 14anu-ban-05 on (04-12-2014)
;A large Shivalinga is situated in the fabricated Gauri seat of the height of 7 . 90 metres in the temple .[TOURISM]
;maMxira meM eka viSAla SivaliMga 7.90 mItara kI UzcAI ke gaDZe huye gOrIpatZta para sWiwa hE .[TOURISM]
(defrule fabricate1
(declare (salience 1000))
(id-root ?id fabricate)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id verb_past_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 seat|story|evidence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fabricate.clp 	fabricate1   "  ?id "  gaDZA )" crlf))
)
