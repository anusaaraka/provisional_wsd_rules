;----------------------DEFAULT RULE-----------------------------------------------------
;@@@ Added by 14anu-ban-09 on (02-12-2014)
;We should purify the water before drinking it. [Hinkhoj]
;हमें यह पानी पीने से पहले शुद्ध करना चाहिए. [Self]

(defrule purify0
;(declare (salience 0))
(id-root ?id purify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuxXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  purify.clp 	purify0   "  ?id "  SuxXa_kara )" crlf))
)

;----------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (02-12-2014)
;Out of the 24 Tirthankaras the 20th Tirthankar Shri Sant Suvrat Swami , 23rd Tirthankar Parsvanath Bhagwan and the last Tirthankar Mahavir Swami purified this place with their holy feet . [Tourism Corpus]
;24 तीर्थंकरो में से 20वें तीर्थंकर श्री मुनि सुव्रत स्वामी , 23वें तीर्थंकर पार्श्वनाथ भगवान और अंतिम तीर्थंकर श्री महावीर स्वामी ने भी इस स्थान को अपने चरणों से पावन किया . [Tourism Corpus]

(defrule purify1
(declare (salience 1000))
(id-root ?id purify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 foot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAvana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  purify.clp 	purify1   "  ?id "  pAvana_kara )" crlf))
)



