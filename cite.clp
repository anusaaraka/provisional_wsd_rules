;@@@ Added by 14anu-ban-03 (25-02-2015)
;He was cited for bravery. [oald]
;उसको बहादुरी के लिए प्रमाण दिया गया था . [manual]
(defrule cite1
(declare (salience 10))
(id-root ?id cite)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 bravery)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramANa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cite.clp 	cite1   "  ?id "  pramANa_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-02-2015)
;She cited a passage from the President’s speech. [oald]
;उसने राष्ट्रपति के भाषण से एक अनुच्छेद का उल्लेख किया . [manual]
(defrule cite2
(declare (salience 20))
(id-root ?id cite)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 passage|book|author)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  cite.clp 	cite2  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cite.clp 	cite2   "  ?id "  ulleKa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (25-02-2015)
;Many people were cited to witness the divorce proceedings. [hinkhoj]
;बहुत सारे लोग तलाक कार्रवाई देखने के लिए तलब किए गये थे . [manual]
(defrule cite0
(declare (salience 00))
(id-root ?id cite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walaba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cite.clp     cite0   "  ?id "  walaba_kara )" crlf))
)

