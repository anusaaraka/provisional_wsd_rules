;@@@ Added by 14anu-ban-09 on (21-01-2015)
;I photographed profusely. 					[Tourism Corpus]
;मैनें जमकर फ़ोटोग्राफी की.						[Tourism Corpus]

(defrule photograph2
(declare (salience 5000))
(id-root ?id photograph)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 profusely)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZotogrAPI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  photograph.clp 	photograph2   "  ?id "  PZotogrAPI_kara )" crlf))
)

;---------------------- Default rules ----------------

(defrule photograph0
(declare (salience 5000))
(id-root ?id photograph)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZoto))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  photograph.clp 	photograph0   "  ?id "  PZoto )" crlf))
)

(defrule photograph1
(declare (salience 4900))
(id-root ?id photograph)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZoto_KIMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  photograph.clp 	photograph1   "  ?id "  PZoto_KIMca )" crlf))
)

;"photograph","V","1.PZoto_KIMcanA"
;I am photographing a rare plant.
;
;
