;@@@ Added by 14anu-ban-11 on (26-02-2015)
;The fever slowly sapped her strength.(oald)
;बुखार ने धीरे धीरे उसकी शक्ति को कमजोर कर दिया . (self)
(defrule sap2
(declare (salience 4901))
(id-root ?id sap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 strength)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sap.clp 	sap2   "  ?id "  kamajora)" crlf))
)


;;@@@ Added by 14anu-ban-11 on (26-02-2015)
;The poor sap never knew what was going on behind his back.(oald)
;दीन मूर्ख आदमी  नहीं जानता था कि उसकी पीठ के पीछे क्या हो रहा था . (self)
(defrule sap3
(declare (salience 5002))
(id-root ?id sap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 poor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrKa_AxamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sap.clp 	sap3   "  ?id "  mUrKa_AxamI)" crlf))
)

;------------------------ Default Rules ----------------------

;"sap","N","1.rasa/sAra"
;That medicine is made from sap of Neem tree.
(defrule sap0
(declare (salience 5000))
(id-root ?id sap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sap.clp 	sap0   "  ?id "  rasa )" crlf))
)

;"sap","V","1.rasa_[nikAlanA]"
(defrule sap1
(declare (salience 4900))
(id-root ?id sap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rasa_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sap.clp 	sap1   "  ?id "  rasa_nikAla )" crlf))
)

;"sap","V","1.rasa_[nikAlanA]"
;--"2.aSakwa_yA_xurbala_honA"
;As he grew older && older his working power sapped.
;
;"sap","N","1.rasa/sAra"
;That medicine is made from sap of Neem tree.
;--"2.mUrKa"
;The poor sap never knew that his wife was cheating him.

