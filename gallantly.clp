;@@@ Added by 14anu-ban-05 on (06-04-2015)
;She gallantly battled on alone. 	[OALD]
;वह   बहादुरी से अकेले  लड़ाई लड़ी.		[MANUAL]

(defrule gallantly0
(declare (salience 100))
(id-root ?id gallantly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahAxurI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " gallantly.clp     gallantly0  "  ?id " bahAxurI_se )" crlf))
)

;@@@ Added by 14anu-ban-05 on (06-04-2015)
;He bowed and gallantly kissed my hand.	[OALD]
;वह झुके और  शिष्टतापूर्वक मेरे हाथ को चूमा		[MANUAL]

(defrule gallantly1
(declare (salience 101))
(id-root ?id gallantly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 kiss|speak)		;more contraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  SiRtawApUrvaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " gallantly.clp     gallantly1  "  ?id "  SiRtawApUrvaka )" crlf))
)
