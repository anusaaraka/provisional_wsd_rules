;@@@ Added by 14anu23 on 18-June-2014
;A scientific discovery that won critical applause.
;एक वैज्ञानिक- खोज  जिसने समीक्षात्मक वाहवाही जीती हो . 
(defrule applause0
(declare (salience 4800))
(id-root ?id applause)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAhavAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  applause.clp 	applause0   "  ?id "  vAhavAhI )" crlf))
)


;;@@@ Added by 14anu23 on 18-June-2014
;They gave him a round of applause. 
;उन्होंने उसकी ताली बजाकर  प्रशंसा  की  . 
(defrule applause1
(declare (salience 4900))
(id-root ?id applause)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  wAlI_bajA_kara_praSaMsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  applause.clp 	applause1   "  ?id "  wAlI_bajA_kara_praSaMsA_kara )" crlf))
)

