
;@@@ Added by 14anu-ban-05 on (23-04-2015)
;He could not reconcile himself to the prospect of losing her.[OALD]
;वह उसको खोने के आसार से  स्वयं को स्वीकार कर नहीं करा सका .	[MANUAL]
(defrule reconcile1
(declare (salience 101))
(id-root ?id reconcile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reconcile.clp 	reconcile1   "  ?id "  svIkAra_kara  )" crlf))
)

;---------------------------------------- Default Rules -----------------------------------------

;@@@ Added by 14anu-ban-05 on (23-04-2015)
;He has recently been reconciled with his wife.[OALD]
;उन्होंने हाल ही में अपनी पत्नी के साथ सामंजस्य स्थापित किया  है.     [MANUAL]
(defrule reconcile0
(declare (salience 100))
(id-root ?id reconcile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmaMjasya_sWApiwa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reconcile.clp        reconcile0   "  ?id "  sAmaMjasya_sWApiwa_kara  )" crlf))
)

