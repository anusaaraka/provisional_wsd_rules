;Added by Meena(20.7.10)
;This book is designed to be used in either an algebra based physics course or a calculus based physics course that has calculus as a corequisite.
(defrule to_be_use00
(declare (salience 5000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id used)
(id-root =(- ?id 2) to)
(id-root =(- ?id 1) be)
(id-root ?id1 ?)
(kriyA-kqxanwa_karma  ?id1 =(- ?id 1))
(kriyA-in_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2)  prayoga_hone_ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " use.clp  to_be_use00  "  ?id "  " (- ?id 1) "   " (- ?id 2) "  prayoga_hone_ke_liye  )" crlf))
)

;Added by Manju Suggested by Sukhada (24-09-11)
;He used to work at night.
(defrule be_used_to
(declare (salience 5000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id used)
(id-root =(- ?id 1) be)
(or (to-infinitive  =(+ ?id 1) ?id1)(kriyA-to_saMbanXI ?id ?id2)) ;Added by Aditya and Hardik(20-06-2013),IIT(BHU) batch 2012-2017.
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  =(+ ?id 1)  AxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " use.clp  be_used_to  "  ?id "  " (+ ?id 1) "  AxI )" crlf))
)



;used to do this 
;aadat 
;@@@ Added by avni(14anu11)
(defrule use8
(declare (salience 5000))
(id-root ?id use)
(id-root =(+ ?id 1) to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  aAxawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use8   "  ?id "  aAxawa )" crlf))
)

;Added by Meena(19.11.10)
;In physics, the term “fluid” is used to mean either a gas or a liquid.
(defrule used_to
(declare (salience 5000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id used)
(id-root =(+ ?id 1) to|for)
(id-root =(+ ?id 2) mean)
(kriyA-kqxanwa_karma  ?id =(+ ?id 2))
;(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)  arWa_hone_ke_liye_prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " use.clp  used_to  "  ?id "  " (+ ?id 1) "   " (+ ?id 2) "  prayoga_kiyA_jA  )" crlf))
)




;Added by Meena(12.1.11)
;She hurt her arm in the fall and lost the use of her fingers temporarily.
(defrule use00
(declare (salience 5000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 finger|mind|leg|arm|hand|eye|brain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayogitA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp       use00   "  ?id "  upayogitA )" crlf))
)





;Salience reduced by Meena(12.1.11)
(defrule use0
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use0   "  ?id "  upayoga )" crlf))
)

;Added by Sukhada(13-03-10)
;We now know that there are two thousand elements which we can not use to make a good light bulb .
(defrule use_which
(declare (salience 4900))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 which)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp       use_which   "  ?id "  upayoga_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  use.clp       use_which   "  ?id "  ko )" crlf)
)
)



;"use","N","1.upayoga"
;--"2.prayoga/Boga/sevana"
;the steps were worn from years of use
;--"3.AvaSyakawA"
; he put his knowledge to good use
;--"4.lABa/prayojana"
; we were given the use of his boat
;

(defrule use1
(declare (salience 4800))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use1   "  ?id "  upayoga_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  use.clp       use1   "  ?id "  kA )" crlf)
)
)

;"use","V","1.upayoga_kara"

;(defrule use2
;(declare (salience 4900))
;(id-root ?id use)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(id-root =(- ?id 1) to)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id upayoga_karane_ke_liye))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp      use2   "  ?id "  upayoga_karane_ke_liye )" crlf))
;(assert (kriyA_id-object_viBakwi ?id kA))
;)


;
(defrule use3
(declare (salience 0))
;(declare (salience 4700))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use3   "  ?id "  upayoga_kara )" crlf))
)

;"use","VTI","1.prayoga_kara"
; She uses drugs rarely.
;

;@@@ Added by 14anu-ban-07 (07-08-2014)
;Three such systems, the CGS, the FPS (or British) system and the MKS system were in use extensively till recently.(ncert corpus)
;aba se kuCa samaya - pUrva waka EsI wIna praNAliyAz - @CGS praNAlI, @FPS (yA britiSa) praNAlI evaM @MKS praNAlI, pramuKawA se prayoga meM lAI jAwI WIM.
(defrule use4
(declare (salience 4000))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use4   "  ?id "  prayoga )" crlf))
)

;@@@ Added by 14anu-ban-07 (13-08-2014)
;xvArA  :
;We shall now describe this law of addition using the graphical method.(ncert)
;aba hama grAPI viXi xvArA yoga ke isa niyama ko samaJAefge .
(defrule use5
(declare (salience 5100))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kqxanwa_karma  ?id1 ?id)
(id-root ?id1 describe)  ; added condition by 14anu-ban-07 (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use5   "  ?id "   xvArA )" crlf))
)


;@@@ Added by 14anu-ban-07 (27-09-2014)
;To represent a vector, we use a bold face type in this book.(ncert corpus)
;सदिश को व्यक्त करने के लिए इस पुस्तक में हम मोटे अक्षरों का प्रयोग करेङ्गे .
(defrule use6
(declare (salience 4800))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use6   "  ?id "  prayoga_kara )" crlf))
)

;@@@ Added by 14anu-ban-07 (06-11-2014)
;The commonly used property is variation of the volume of a liquid with temperature.(ncert)
;सामान्य उपयोग होने वाला गुण "ताप के साथ किसी द्रव के आयतन में परिवर्तन" होता है.(manually)
(defrule use7
(declare (salience 4900))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 commonly)  ; added by 14anu-ban-07 (24-11-2014)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp 	use7   "  ?id "  upayoga_ho )" crlf))
)

;@@@ Added by 14anu21 on 19.06.2014
;This tool is used for shaping wood.
;यह औजार लकडी को आकार देने के लिए प्रयोग किया जाता है . 
(defrule use_for
(declare (salience 4900))
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp       use_for   "  ?id "  prayoga_kara )" crlf)
)
)

;@@@ Added by 14anu-ban-07 (03-12-2014)
;Although the pesticides and particularly insecticides used in organic farming and organic gardening are generally safer than synthetic pesticides, they are not always more safe or environmentally friendly than synthetic pesticides and can cause harm.(agriculture)
;यद्यपि जैविक खेती और जैविक उद्यान में उपयोग होने वाले कीटनाशक और विशेषतः कीटाणुनाशक सिंथेटिक कीटनाशकों से आम तौर पर सुरक्षित होते हैं, वे हमेशा संश्लेषित कीटनाशक की अपेक्षा अधिक सुरक्षित या पर्यावरण की दृष्टि से अनुकूल नहीं होते हैं और हानी का कारण बन सकते हैं . (agriculture)
(defrule use08
(declare (salience 4800))
(Domain agriculture) 
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_ho))
(assert (id-domain_type  ?id agriculture))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp       use08   "  ?id "  upayoga_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  use.clp 	use08   "  ?id "  agriculture )" crlf)
)
)

;@@@ Added by 14anu-ban-07 (04-12-2014)
;These larvae, which are known as white grubs, are easily recognized by their broad and fleshy appearance, white or grayish white color together with the C-shaped body, having well-developed thoracic legs rarely used for locomotion. (agriculture)
;ये लार्वा, जो सफेद grubs के रूप में जाना जाता है, आसानी से अपनी व्यापक और मांसल  रूप द्वारा पहचाना जा सकता हैं ,इसका शरीर सफेद या भूरे सफेद रंग का सी आकार का होता है ,इसके पास अच्छी तरह से विकसित थोरैसिक पैर होते है जो कभी कभार  संचलन के लिए उपयोग किये जाते हैं . (agriculture)
(defrule use9
(declare (salience 5000))
(Domain agriculture) 
(id-root ?id use)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kiye_jA))
(assert (id-domain_type  ?id agriculture))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  use.clp       use9   "  ?id "  upayoga_kiye_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  use.clp 	use9   "  ?id "  agriculture )" crlf)
)
)
