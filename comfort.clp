;@@@ Added by 14anu-ban-03 (16-03-2015)
;I tried to offer a few words of comfort.  [oald]
;मैंने सान्त्वना के कुछ शब्दों का प्रस्ताव रखने का प्रयास किया . [manual]
(defrule comfort2
(declare (salience 5000))
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAnwvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort2   "  ?id "  sAnwvanA )" crlf))
)


;@@@ Added by 14anu-ban-03 (16-03-2015)
;It's a comfort to know that she is safe. [oald]
;यह जानकर तसल्ली है कि वह सुरक्षित है . [manual]
(defrule comfort3
(declare (salience 5000))
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wasallI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort3   "  ?id "  wasallI )" crlf))
)



;@@@ Added by 14anu-ban-03 (16-03-2015)
;She comforted herself with the thought that it would soon be spring. [oald]
;उसने विचार के साथ स्वयं को दिलासा दिया कि वसंत शीघ्र होगी . [manual]
(defrule comfort4
(declare (salience 5000))  
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xilAsA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort4   "  ?id "  xilAsA_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (16-03-2015)
;Stepping outside your comfort zone and trying new things can be a great experience. [oald]
;अपने आरामदायक क्षेत्र के बाहर निकलना और नयी चीजों का प्रयास करना एक अच्छा अनुभव हो सकता है . [manual]
(defrule comfort5
(declare (salience 5000))   
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 zone)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAmaxAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort5   "  ?id "  ArAmaxAyaka )" crlf))
)


;-----------------------------Default rules---------------------

(defrule comfort0
(declare (salience 00))   ;salience reduced by 14anu-ban03 (16-03-2015)
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort0   "  ?id "  ArAma )" crlf))
)

;"comfort","VT","1.ArAma_xenA"
;This pillow doesn't give me any comfort.
(defrule comfort1
(declare (salience 00))  ;salience reduced by 14anu-ban03 (16-03-2015)
(id-root ?id comfort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfort.clp 	comfort1   "  ?id "  ArAma_xe )" crlf))
)

