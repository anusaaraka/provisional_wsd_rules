
;"gathering","N","1.BIdZa"
;There was a large gathering of devotees in the temple.
(defrule gather0
(declare (salience 5000))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id gathering )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BIdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gather.clp  	gather0   "  ?id "  BIdZa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (15-10-2014)
;Information gathered from such satellites is extremely useful for remote sensing, meterology as well as for environmental studies of the earth.[NCERT]
;isa prakAra ke upagrahoM xvArA ekawra sUcanAez suxUra saMvexana, mOsama vijFAna ke sAWa pqWvI ke paryAvaraNIya aXyayanoM ke lie BI awyanwa upayogI hEM.
(defrule gather4
(declare (salience 5000))
(Domain physics)
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id ekawra))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gather.clp 	gather4   "  ?id "  ekawra )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  gather.clp       gather4   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-01-2015)
;She gathered the children up and hurried into the house.[cambridge]
;उसने  बच्चों को इकठ्ठा किया और घर में जल्दी से गई . 			 [manual]
(defrule gather5
(declare (salience 5000))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikaTTA_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gather.clp 	gather5   "  ?id "  ikaTTA_kara )" crlf))
)	

;@@@ Added by Shruti Singh M.TEch(CS) Banasthali (23-08-2016)
;the truck gathered the speed
;truck n apni speed badhayi
(defrule gather6
(declare (salience 5000))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 speed|velocity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " gather.clp   gather6 " ?id " baDZA )" crlf))
)

;--------------------------- Default rules ----------------------
;"gather","N","1.kapadZe_kI_waha"
;I like two gathers in my  trousers.
(defrule gather1
(declare (salience 4900))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kapadZe_kI_waha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gather.clp 	gather1   "  ?id "  kapadZe_kI_waha )" crlf))
)

;"gather","V","1.ikaTTA_kara"
;--"2.ikaTTA_karanA"
;He gathered as many flowers as he could.
;--"3.pariNAma_nikAlanA"
;I gather from the recent reports that external forces are trying to create 
;trouble in the country.
(defrule gather2
(declare (salience 4800))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikaTTA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gather.clp 	gather2   "  ?id "  ikaTTA_kara )" crlf))
)

;
;@@@ Added by 14anu-ban-06 (30-07-2014)
;At night in Banni Mandap the torch parade and the attractive rhythms of the police band charm the minds of the gathered crowd of people  .   (Parallel Corpus)
;बन्नी मंडप में रात्रि में ’टॉर्च परेड’ और ’पुलिस बैंड’ की आर्कषक धुनें एकत्रित जनसमूह का मन मोह लेती हैं ।
(defrule gather3
(declare (salience 4900))
(id-root ?id gather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gather.clp 	gather3   "  ?id "  ekawriwa)" crlf))
)

