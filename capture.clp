;--------------------Default Rule----------------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on 28-7-14
;The animal are captured in nets and sold in local zoos. [OALD]
;jAnavara jAla meM pakade Ora sWAnIya cidiyAgara meM beca die jAwe hE.
(defrule capture0
(declare (salience 4000))
(id-root ?id capture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capture.clp 	capture0   "  ?id "  pakada )" crlf))
)

;-------------------------------------------------------------------------------------------------------

;NOTE- Still working on this rule.
;@@@ Added by 14anu-ban-09 on 28-7-14
;And along with it take one binoculars and if possible a camera with good zoom to capture pictures. [Parallel Corpus]
;और साथ ही एक दूरबीन और संभव हो तो अच्छे जूम वाला कैमरा , तस्वीरें कैद करने के लिए ।
;Thekkady is the most appropriate place to capture the pictures of wild elephants. [Parallel Corpus]
;जँगली  हाथियों  का  फोटो  कैद करने के लिए  तेक्कडि  सबसे  उपयुक्त  स्थान  है  ।

(defrule capture1
(declare (salience 5000))
(id-root ?id capture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 picture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kExa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capture.clp 	capture1   "  ?id "  kExa )" crlf))
)

;NOTE- When we talk about "Person or animate" we use it as a "aXIna" and the meaning come from the Parallel Corpus.
;@@@ Added by 14anu-ban-09 on 28-7-14
;The British army chief Arthur Miller, who had come to capture the Pazhassi king , stayed in Thalassery for a long time. [Parallel Corpus]
;ब्रिटिश  सेनापति  आर्थर  मिल्लर  जो  पष़श्शि  राजा  को  अधीन  करने  आया  था  ,  तलश्शेरी  में  लम्बे  समय  तक  रहा  ।
;Capture the queen in chess. [FD]
;SawaraMja meM rAnI ko aXIna karanA. [Own Manual]

(defrule capture2
(declare (salience 5010))
(id-root ?id capture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capture.clp 	capture2   "  ?id "  aXIna)" crlf))
)

;@@@ Added by 14anu-ban-09 on 28-7-14
;The Dutch had captured the Saint Angelo Fort in 1663 and sold to Ali King of Kannur. [Parallel Corpus]
;सन्  1663  में  सैंट  आंचलो  किले  पर  डचों  ने  कब्जा  कर  लिया  था  और  कण्णूर  के  अली  राजा  को  बेच  दिया  था  ।
;After the revolt of 1857, British captured this building and made it their home leading to its nomenclature as Residency. [parallel Corpus]
;1857  में  आजादी  की  लड़ाई  के  बाद  अंग्रेजों  ने  इस  पर  कब्जा  कर  लिया  और  इस  में  अपना  निवास  बनाया  तो  इस  का  नाम  रेजीडेंसी  पड़  गया  ।
;Like the other estates  of the country the Britishers had captured Almora in 1815. [Parallel Corpus]
;देश  की  अन्य  रियासतों  की  तरह  अंग्रेजो  ,  ने  1815  में  अल्मोड़ा  पर  कब्जा  किया  था  ।
;When China had captured Tibet then the chief of the Karmagupa community of the Buddhists and the ninth incarnate Honourable Gyalwa Karmapa of the original Gyalwa Karmapa of Tibet had taken shelter here. [Parallel Corpus]
;जब  चीन  ने  तिब्बत  पर  कब्ज़ा  किया  था  तब  बौद्ध  के  कर्मागुपा  समुदाय  के  प्रमुख  और  तिब्बत  के  मूल  ग्यालवा  कर्मापा  के  नवें  इन्कारनेट  माननीय  ग्यालवा  कर्मापा  ने  यहाँ  शरण  ली  थी  ।

(defrule capture3
(declare (salience 5000))
(id-root ?id capture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kabjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capture.clp 	capture3   "  ?id "  kabjA )" crlf))
)

;NOTE- "capture the heart" is working like a Phrase.
;@@@ Added by 14anu-ban-09 on 30-7-14
;Ekadash Rudra pilgrimage is a charming place , which captures the heart of viewers. [Parallel Corpus]
;एकादश  रुद्र  तीर्थ  एक  रमणीय  स्थान  है  ,  जो  दर्शकों  का  मन  मोह  लेता  है  ।
;20 kilometres away from Nainital Bhimtal large lake captures the heart of tourists. [Parallel Corpus]
;नैनीताल  से  20  किलोमीटर  दूर  भीमताल  विशाल  झील  पर्यटकों  का  मन  मोह  लेती  है  ।
;Colourful birds capture the heart of tourists by their sweet songs.
;रंग-बिरंगे पक्षी अपने मधुर गीत - संगीत से भ्रमणकारियों का मन मोह लेते हैं ।

(defrule capture4
(declare (salience 5000))
(id-root ?id capture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 heart)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capture.clp 	capture4   "  ?id "  moha )" crlf))
)
