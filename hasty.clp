;@@@ Added by 14anu-ban-06 (27-01-2015)
;Let's not make any hasty decisions.(OALD)
;हमें कोई द्रुत निर्णय नहीं लेने चाहिए . (manual)
(defrule hasty0
(declare (salience 0))
(id-root ?id hasty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xruwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hasty.clp 	hasty0   "  ?id "  xruwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-01-2015)
;Application of ubtan is reducing in hasty life of today and women have started taking shelter of beauty parlors every other day .(parallel corpus)
;आज की भागदौड़ भरी जिंदगी में उबटनों का प्रयोग कम होता जा रहा है तथा महिलाएँ आए दिन ब्यूटी पार्लरों की शरण लेने लगी हैं ।(parallel corpus)
;When people come here terrified by the hasty life of metropolitan cities then the meaning of love and peace of is understood by them .(parallel corpus)
;महानगरों  की  भागदौड़ भरी  जिन्दगी  से  त्रस्त  लोग  जब  यहाँ  आते  हैं  तो  उन्हें  प्रेम  और  शांति  के  माने  भी  समझ  आते  हैं  ।(parallel corpus)
;Each one has become so busy in hasty life of today that he she does not have time for himself herself .(parallel corpus)
;आज की भागदौड़ भरी जिंदगी में हर कोई इतना व्यस्त हो गया है कि उसके पास अपने लिए ही समय नहीं है ।(parallel corpus)
(defrule hasty1
(declare (salience 2000))
(id-root ?id hasty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAgaxOdZa_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hasty.clp 	hasty1   "  ?id "  BAgaxOdZa_BarA )" crlf))
)

