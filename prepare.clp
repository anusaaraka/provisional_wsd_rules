;##############################################################################
;#  Copyright (C) 2002-2005 Shweta Vikram (shwetavikram.2009@rediffmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;Added by Sonam Gupta MTech IT Banasthali 2013
;It's best to be prepared. 
;तैयार रहना सर्वोत्तम है.
(defrule prepare0
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(or(and(to-infinitive  ? ?id)(kriyA-subject  ?id ?)(saMjFA-to_kqxanwa  ? ?id))(and(kriyA-kriyArWa_kriyA  ?id ?)(kriyA-subject  ?id ?))(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-kriyArWa_kriyA  ? ?id)(to-infinitive  ? ?id)(kriyA-for_saMbanXI  ?id ?)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare0  "  ?id " wEyAra_raha)" crlf))
)


;Added by Sonam Gupta MTech IT Banasthali 2013
;We don't have much more time to prepare.
;हमारे पास तैयारी करने के लिए ज्यादा समय नहीं है.
(defrule prepare1
(declare (salience 1800))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(and(saMjFA-to_kqxanwa  ? ?id)(to-infinitive  ? ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare1  "  ?id " wEyArI_kara)" crlf))
)


;Added by Shweta Vikram(07/09/2013)
;To prepare lunch.
;दोपहर का खाना बनाना.
(defrule prepare2
(declare (salience 1700))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(or(to-infinitive  ? ?id)(kriyA-in_saMbanXI  ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare2  "  ?id " banA)" crlf))
)



;Added by Shweta Vikram(11/09/2013)Default
(defrule prepare3
(declare (salience 1600))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse ?id1 verb)
(id-cat_coarse ?id2 adjective)
(or(and(kriyA-kriyArWa_kriyA  ?id ?id1)(viSeRya-viSeRaNa  ?id2 ?id))(and(kriyA-object ?id ?)(viSeRya-viSeRaNa  ? ?id)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare3  "  ?id "  wEyAra_kara )" crlf))
)



;Added by Shweta Vikram(11/09/2013)
;Hipparchus of Nicaea , a Greek mathematician ( 150 B.C. ) , was possibly the first to prepare these Tables . 
;nAikiyA nivAsI hippArkasa ( 150 IsA pUrva . ) , jo eka yUnAnI gaNiwajFa WA , saMBavawaH pahalA vyakwi WA jisane ina sAraNiyoM kI racanA kI.
;नाइकिया निवासी हिप्पार्कस ( 150 ईसा पूर्व . ) , जो एक यूनानी गणितज्ञ था , संभवतः पहला व्यक्ति था जिसने इन सारणियों की रचना की. 
(defrule prepare4
(declare (salience 1500))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id racanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare4   "  ?id "  racanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (08-11-2014)
;Because of their widespread use in prepared and packaged foods, the avoidance of peanuts is difficult. [Agriculture]
;क्योंकि तैयार और पैक खाद्य पदार्थों में उनके व्यापक उपयोग में, मूँगफली का परिहार कठिन है . [Self]

(defrule prepare6
(declare (salience 1500))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 food)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare6  "  ?id "  wEyAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;You can prepare the program according to that . [Tourism Corpus]
;उसी हिसाब से आप कार्यक्रम तय कर सकें . [Tourism Corpus]

(defrule prepare7
(declare (salience 1500))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 program)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare7  "  ?id "  waya_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-02-2015)
;She'll prepare the food ahead of time and freeze it. 	[same clp file]
;वह समय से आहार बनाएगी और उसे रख देगी.			[manual]
;The court empowered Sir Asutosh to prepare a scheme for utilizing the gift . [same clp file]	;added by 14anu-ban-09 on (16-02-2015)
;न्यायालय ने सर आशुतोष को ईस उपहार के उपयोग की योजना बनाने का अधिकार दिया.		[same clp file]	;added by 14anu-ban-09 on (16-02-2015)
;He was in the kitchen preparing lunch. 	[oald]
;वह रसोई में दोपहर का खाना बना रही थी.                  [self]

(defrule prepare8
(declare (salience 2000))		;salience increased '1500' to '2000' by 14anu-ban-09 on (16-02-2015)
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 lunch|breakfast|dinner|scheme|food)   ;added 'scheme' by 14anu-ban-09 on (16-02-2015)	;added 'food' by 14anu-ban-09 on (17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare8  "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-02-2015)
;Thus , the larvae prepare their own abode of safety and plenty .		[same clp file]	;added by 14anu-ban-09 on (17-02-2015)
;इस प्रकार लार्वा अपनी सुरक्षा और प्रचुरता के लिए अपना आवास स्वयं बनाते हैं . 			[manual]	;added by 14anu-ban-09 on (17-02-2015)
;Remedies prepared from herbal extracts.	[oald]
;जडी-बूटी के रस से बने उपचार.                 	 [self]

(defrule prepare09
(declare (salience 1500))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 remedy|lunch|breakfast|dinner|larva)	;added 'larva' by 14anu-ban-09 on (17-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare09  "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-02-2015)
;LSD is very easy to prepare in the laboratory . 	[same clp file]
;एल . एस . डी . को प्रयोगशाला में बनाना आसान है.	 	[manual]

(defrule prepare9
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 laboratory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare9  "  ?id " banA)" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-02-2015)
;The artistes themselves prepare the puppets required for the plays they produce .  	[same clp file]
;कलाकार खुद ही कठपुतलियाँ तैयार करते हैं जो वो नाटक में प्रदर्शित करने के लिये जरूरत होती है.     	 	[manual]

(defrule prepare10
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 puppet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare10  "  ?id " wEyAra_kara)" crlf))
)

;Removed by 14anu-ban-09 on (17-02-2015)
;NOTE-This case is already handled in prepare09.
;@@@ Added by 14anu-ban-09 on (12-02-2015)
;Thus , the larvae prepare their own abode of safety and plenty .		[same clp file]
;इस प्रकार लार्वा अपनी सुरक्षा और प्रचुरता के लिए अपना आवास स्वयं बनाते हैं . 			[manual]

;(defrule prepare11
;(declare (salience 1900))
;(id-root ?id prepare)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-subject ?id ?id1)
;(id-root ?id1 larva)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id banA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare11  "  ?id " banA)" crlf))
;)

;@@@ Added by 14anu-ban-09 on (16-02-2015)
;She helped Dr. Bose to prepare his new book .   	[same clp file]
;कलाकार खुद ही कठपुतलियाँ तैयार करते हैं जो वो नाटक में प्रदर्शित करने के लिये जरूरत होती है.     	 	[manual]

(defrule prepare12
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 book)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare12  "  ?id " praswuwa_kara)" crlf))
)

;$$$ Modified by Bhagyashri Kulkarni (20-09-2016)
;@@@ Added by 14anu-ban-09 on (16-02-2015)
;Have you prepared for your interview? 		[same clp file]
;क्या आप साक्षात्कार के लिए तैयार कर चुके हैं? 		[manual]
;Is your brother preparing for some examination? (rapidex)
;क्या आपका भाई किसी परीक्षा के लिए तैयारी कर रहा है? 
(defrule prepare13
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 interview|examination) ;added examination by Bhagyashri Kulkarni (20.09.2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare13  "  ?id " wEyArI_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-02-2015)
;Hipparchus of Nicaea , a Greek mathematician ( 150 B.C. ) , was possibly the first to prepare these Tables .  		[same clp file]
; नाइकिया निवासी हिप्पार्कस ( 150 ईसा पूर्व . ) , जो एक यूनानी का गणितज्ञ था , संभवतः पहला व्यक्ति था जिसने इन सारणियों की रचना की.		[manual]

(defrule prepare14
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 Tables)
(viSeRya-viSeRaNa  ?id2 ?id)
(id-root ?id2 first)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id racanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare14  "  ?id " racanA_kara)" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-02-2015)
;This course aims to prepare students for middle and senior managerial positions.  [same clp file]
;इस पाठ्यक्रम का लक्ष्य मध्यस्थल और वरिष्ठ प्रबन्धकीय पदो के लिए विद्यार्थियों को तैयारी करना है . 		[manual]

(defrule prepare15
(declare (salience 1900))
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 student)
(viSeRya-for_saMbanXI  ?id1 ?id2)
(id-root ?id2 position)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare15  "  ?id " wEyArI_kara)" crlf))
)


;####################################################Default Rule###########################################################################

(defrule prepare5
(id-root ?id prepare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prepare.clp 	prepare5  "  ?id "  wEyAra_kara )" crlf))
)




;meaning
;==1.wEyAra_kara
;==2.banA
;==3.racanA_kara
;==4.savaXAna_kara
;==5.pakA
;==6.sajA
;==7.praswuwa_kara
;==8.TIka_kara

;http://10.2.56.85:8084/Gyan_Search_80M/
;The first task was to prepare an , election manifesto .
;सबसे पहले काम था एक ऐसा घोषणा - पत्र तैयार करना . 
;LSD is very easy to prepare in the laboratory . 
;एल . एस . डी . को प्रयोगशाला में बनाना आसान है.
;The artistes themselves prepare the puppets required for the plays they produce . 
;स्वयं प्रहसन का पाठ तैयार करके वे उसे अपने भावी ग्राहकों के सामने प्रदर्शित करती हैं .
;Thus , the larvae prepare their own abode of safety and plenty .
;इस प्रकार लार्वा अपनी सुरक्षा और प्रचुरता के लिए अपना आवास स् वयं बनाते हैं . 
;It is necessary to prepare and plan for the distant future too . 
;दूर - गामी भविष्य के लिए भी तैयारी करना और योजना बनाना जरुरी है.
;She helped DrBose to prepare his new book . 
;उसने डा . बोस की नयी पुस्तक तैयार करने में सहायता की .
;Stop a little ,I said to the two of them ,let me prepare Lola . 
;जरा ठहरिये , मैंने दोनों से कहा , मैं जरा जाकर लोला को सावधान कर आऊं . ???(Discuss)
;They help one another in this search for new words and prepare the notes .
;वे एक - दूसरे को शब्दों की याद भी करा देते हैं और कोई - कोई तो शब्द लिख तक लेते है . 
;The Constituent Assembly set to work to prepare a new constitution for India . 
;विधान परिषद देश के लिए नया विधान बना रही थी.
;The court empowered Sir Asutosh to prepare a scheme for utilizing the gift . 
;न्यायालय ने सर आशुतोष को ईस उपहार के उपयोग की योजना बनाने का अधिकार दिया.
;Hipparchus of Nicaea , a Greek mathematician ( 150 B.C. ) , was possibly the first to prepare these Tables . 
;नाइकिया निवासी हिप्पार्कस ( 150 ईसा पूर्व . ) , जो एक यूनानी गणितज्ञ था , संभवतः पहला व्यक्ति था जिसने इन सारणियों की रचना की.
;Then I would arrive in the second car and prepare for the meeting , when the first car would move ahead to the next meeting place . 
;जब सब लोग अपने - अपने स्थान पर बैठ जाते , मैं दूसरी मोटर में पहुंच जाता .



;dictionary.cambridge.org
;Have you prepared for your interview? She'll prepare the food ahead of time and freeze it. 
;This course aims to prepare students for middle and senior managerial positions. 
;Are the players mentally and physically prepared to play a tough game? The meal took two hours to prepare.
;It almost seems as if she is preparing to die. You need to prepare your self for a long wait.






