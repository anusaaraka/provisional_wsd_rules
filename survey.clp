
(defrule survey0
(declare (salience 5000))
(id-root ?id survey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paryavalokana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  survey.clp 	survey0   "  ?id "  paryavalokana )" crlf))
)

;"survey","N","1.paryavalokana"
;A quick survey of the street showed that no one was about.
;
(defrule survey1
(declare (salience 4900))
(id-root ?id survey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  survey.clp 	survey1   "  ?id "  jAzca )" crlf))
)

;"survey","V","1.jAzcanA/nirIkRaNa_karanA"
;Police had surveyed the person whom she thinks theif.
;--"2.sarvekRaNa_karanA"
;The minister did an areal survey of the city.
;

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;The Al Qaida man had even surveyed the hotels and guest houses frequented by American and Israeli tourists , but Ayoob ' s 
;arrest scuttled the plot .
;कायदा का यह शस अमेरिकी और इज्राएली पर्यटकों के टिकने वाले होटलं , गेस्ट हाउसों को देख - स्रवेकष्ण् भी चुका था लेकिन अयूब की गिरतारी से सारी योजना धरी रह गई .
;@@@ Added by 14anu11
(defrule survey2
(declare (salience 5000))
(id-root ?id survey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(kriyA-subject  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvekRaNa_kara))          ;changed "sarvekRaNa" to "sarvekRaNa_kara" by 14anu-ban-01 on (12-01-2015)
(assert (kriyA_id-object_viBakwi ?id kA))             ;added by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  survey.clp 	survey2    "  ?id " kA )" crlf)                               ;added by 14anu-ban-01 on (12-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  survey.clp 	survey2   "  ?id "  sarvekRaNa_kara )" crlf))                        ;changed "sarvekRaNa" to "sarvekRaNa_kara" by 14anu-ban-01 on (12-01-2015)
)


;@@@ Added by 14anu-ban-11 on (09-03-2015)
;A detailed structural survey. (oald)
;एक व्यौरेवार संरचनात्मक सर्वेक्षण . (self)
(defrule survey3
(declare (salience 5001))
(id-root ?id survey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 structural)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvekRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  survey.clp 	survey3   "  ?id "  sarvekRaNa )" crlf))
)


