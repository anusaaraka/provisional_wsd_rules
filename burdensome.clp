;@@@Added by 14anu-ban-02(18-04-2015)
;Sentence: The burdensome living conditions that the early settlers had to endure.[mw]
;Translation: प्रारम्भिक अधिवासियों जिन्हें कष्टमय  रहन सहन की  स्थितियों में रहना पडा .  [self]
(defrule burdensome1
(declare (salience 100)) 
(id-root ?id burdensome) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 conditions)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kaRtamaya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  burdensome.clp  burdensome1  "  ?id "  kaRtamaya )" crlf)) 
) 


;@@@Added by 14anu-ban-02(18-04-2015)
;Sentence: A burdensome task .[cald]
;Translation: एक कठिन काम .  [self]
(defrule burdensome2
(declare (salience 100)) 
(id-root ?id burdensome) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 task)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kaTina)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  burdensome.clp  burdensome2  "  ?id "  kaTina )" crlf)) 
) 


;--------------------default_rules--------------------------------------


;@@@Added by 14anu-ban-02(18-04-2015)
;Sentence: The responsibility has become burdensome.[mw]
;Translation: उत्तरदायित्व भार हो गया है .  [self]
(defrule burdensome0 
(declare (salience 0)) 
(id-root ?id burdensome) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  burdensome.clp  burdensome0  "  ?id "  BAra )" crlf)) 
) 


