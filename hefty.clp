;@@@ Added by 14anu-ban-06 (04-04-2015)
;He gave the door a hefty kick.(OALD)
;उसने दरवाजे पर एक जोरदार लात मारी. (manual)
(defrule hefty1
(declare (salience 2000))
(id-root ?id hefty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 kick)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joraxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hefty.clp 	hefty1   "  ?id "  joraxAra )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (04-04-2015)
;They sold it easily and made a hefty profit.(OALD)
;उन्होंने आसानी से यह बेचा और एक भारी लाभ कमाया . (manual)
(defrule hefty0
(declare (salience 0))
(id-root ?id hefty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hefty.clp 	hefty0   "  ?id "  BArI )" crlf))
)

