;########################################################################
;#  Copyright (C) 05.07.2014 14anu21 
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`

;@@@ Added by 14anu21 on 05.07.2014
;Mercury is a poisonous silver liquid metal, used in thermometers.[oxford] 
;पारा थर्मामीटरों में उपयोग की हुई एक विषैली रजत द्रव धातु, है . 
;;Mercury is the smallest planet of our solar system .[oxford]
;पारा हमारे सौर परिवार का सबसे अधिक छोटा ग्रह है .[Translation before adding the rule] 
;बुध ग्रह हमारे सौर परिवार का सबसे  छोटा ग्रह है .
;
(defrule mercury0
(id-root ?id mercury)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(id-root ?id1 planet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buXa_graha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mercury.clp 	mercury0   "  ?id "  buXa_graha )" crlf))
)


;@@@ Added by 14anu21 on 05.07.2014
;Mercury is very close to Sun .[self]
;पारा सूर्य के निकट अत्यन्त है .[Translation before adding the rule] 
;बुध ग्रह  सूर्य के अत्यत निकट  है . 
;
(defrule mercury1
(id-root ?id mercury)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(viSeRya-to_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buXa_graha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mercury.clp 	mercury1   "  ?id "  buXa_graha )" crlf))
)

