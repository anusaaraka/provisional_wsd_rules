;commented by 14anu-ban-03 (02-02-2015) as meaning coming from default rule.
;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 28/6/2014*****
;Considered the rare Turkish stamp the crown of their collection.
;दुर्लभ तुर्की स्टाम्प उनके संग्रह का ताज माना जाता है.
;(defrule crown3
;(declare (salience 5000))
;(id-root ?id crown)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(object-object_samAnAXikaraNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id wAja))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown3   "  ?id "  wAja )" crlf))
;)

;@@@ Added by 14anu-ban-03 (03-12-2014)
;While the larvae feed on the petioles in the crown and on sap from the base of the fronds. [agriculture]
;जब कि डिंभक डंठल के  ऊपरी सिरे से और लम्बे बडे पत्तें के निचले भाग के रस से  भोजन  ग्रहण करते हैं.  [manual]
(defrule crown4
(declare (salience 4900))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(Domain agriculture)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-domain_type  ?id agriculture))
(assert (id-wsd_root_mng ?id sirA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  crown.clp 	crown4  "  ?id "  agriculture )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown4   "  ?id "  sirA )" crlf))
)

;commented by 14anu-ban-03 (02-02-2015)
;@@@ Added by 14anu-ban-03 (28-01-2015)
;If the shores here are the crown then the Kohinoor of this crown is Karbar shore .[tourism]
;यहाँ के तट अगर ताज हैं तो इस ताज का कोहिनूर कारबार तट है . [tourism]
;(defrule crown5
;(declare (salience 5000))
;(id-root ?id crown)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(or(viSeRya-of_saMbanXI ?id1 ?id) (subject-subject_samAnAXikaraNa  ?id1 ?id))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id wAja))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown5   "  ?id "  wAja)" crlf))
;)

;@@@ Added by 14anu-ban-03 (28-01-2015)
;Inside Cave - 19 of Ajanta the statues of Buddha and Bodhisattva , Nagaraj on the crown of seven hooded Gekhura and one hooded Nagarani outside etc . are specially worth a visit . [tourism]
;अजंता की गुफा-19 में भीतर बुद्ध और बोधिसत्त्व की मूर्तियाँ , बाहर सात फन वाले गेखुरा के मुकुट पर नागराज व एक फन वाले नागरानी आदि विशेष रूप से दर्शनीय हैं . [tourism]
(defrule crown6
(declare (salience 5000))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 seven)  ;added by 14anu-ban-03 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukuta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown6   "  ?id "  mukuta)" crlf))
)

;@@@ Added by 14anu-ban-03 (28-01-2015)
;All these statues are 67 feet high which appear to be sitting on the throne and are wearing double crown .[tourism]
;ये सभी मूर्तियाँ 67 फुट ऊँची हैं , जो एक सिंहासन पर बैठी प्रतीत होती हैं और दोहरे मुकुट पहने हुए हैं .[tourism]
(defrule crown7
(declare (salience 5000))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 double)  ;added by 14anu-ban-03 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukuta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown7   "  ?id "  mukuta)" crlf))
)

;@@@ Added by 14anu-ban-03 (28-01-2015)
;On her head is a hat like crown decorated by the circular design of rope like lines .[tourism]
;उनके शीश पर रज्जुनुमा रेखीय वृत्तों के अभिप्राय से अलंकृत टोपीनुमा मुकुट है .[tourism]
(defrule crown8
(declare (salience 5000))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(id-root ?id1 decorate)  ;added by 14anu-ban-03 (02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukuta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown8   "  ?id "  mukuta)" crlf))
)

;@@@ Added by 14anu-ban-03 (03-02-2015)
;The crown of king Chamarajaudayar is kept in the Mysore museum. [same clp file]
;राजा चमाराजउदयर का राजमुकुट मैसुर संग्रहालय  में रखा हुआ है . [manual] 
(defrule crown9
(declare (salience 4950))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id2)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id2 ?id1) 
(id-root ?id1 king)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjamukuta))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown9   "  ?id "  rAjamukuta )" crlf))
)


;---------------------------- Default rules ---------------------
(defrule crown0
(declare (salience 5000))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id crowning )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id samApana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  crown.clp  	crown0   "  ?id "  samApana )" crlf))
)

;"crowning","Adj","1.samApana"
;The performance provided the crowning touch to the entertainments
;

;$$$ Modified by 14anu-ban-03 (02-02-2015)
;Considered the rare Turkish stamp the crown of their collection.
;दुर्लभ तुर्की स्टाम्प उनके संग्रह का ताज माना जाता है.
(defrule crown1
(declare (salience 4900))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAja))   ;meaning changed from 'rAjamukuta' to 'wAja' by 14anu-ban-03 (02-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown1   "  ?id "  wAja )" crlf))
)


;"crown","N","1.rAjamukuta"
;The crown of king Chamarajaudayar is kept in the Mysore museum
;--"2.mukuta"
;Aishwarya Rai got the crown in the beauty contest
;--"3.SiKara"
;Poet Kalidasa gives a vivid description of the crown of Himalayas
;
(defrule crown2
(declare (salience 4800))
(id-root ?id crown)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjyABiReka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crown.clp 	crown2   "  ?id "  rAjyABiReka_kara )" crlf))
)


;"crown","VT","1.rAjyABiReka_karanA"
;Queen Elizabeth was crowned when George V died
;--"2.sammAniwa_karanA"
;Kasturirangan was crowned with Padma Vibhushan for his contribution in the
;field of space technology
;--"3.SiKara_para_honA"
;Mountaineer Bachendra Pal crowned the Himalayas with Indian flag
;--"4.xAzwa_para_Kola_caDAnA"
;The dentist put a crown on his wisdom tooth
;
;LEVEL 
;Headword : crown
;
;Examples --
;
;1. The crown of Emperor Akbar is kept in a museum at  Delhi.
;SAhaMSAha akabara kA wAja xillI ke eka saMgrahAlaya meM raKA gayA hE.
;2. His hair is quite thin on the crown.
;SIrRa ke BAga meM usake bAla bahuwa pawale hEM.
;3. Poet Kalidasa gives a vivid description of the crown of Himalayas.
;kavi kAlixAsa himAlaya ke SIrRa kA camakIlA varNana xewA hE.
;4. The dentist put a crown on his wisdom tooth
;xAzwa ke dAktara ne usakI akala kI xADZa para sirA lagAyA.
;5. Shahjahan was crowned when Akbar died.
;akabara kI mOwa para SAhajahAna ko wAja pahanAyA gayA WA.
;
;
;uparaliKiwa vAkyoM meM "crown" ke Binna laganevAle arWoM meM saMbaMXa vAkya 1. ke arWa
; "wAja" ke xvArA, pAyA jA sakawA hE.
;
;vAkya 1. meM "crown" kA arWa "wAja/mukuta" EsA A rahA hE.
;
;kyoMki wAja ko SIRa para pahanA jAwA hE wo vAkya 2. meM "crown" kA arWa "SIrRa kA BAga" EsA A rahA hE.
;
;yahI arWa vAkya 3. Ora vAkya 4. meM BI A rahA hE. vAkya 3. meM himAlaya parvawa ke 
;SIrRa BAga kI bAwa ho rahI hE. vAkya 4. meM akala kI xADZa para sirA lagAne kI bAwa hE,
;wo jEse mukuta sira para [jo ki SarIra kA SIrRa BAga he] pahanAyA jAwA hE. TIka vEse hI 
;sirA BI mukuta kI waraha xAzwa ke SIrRa BAga para pahanAyA jAwA hE. 
;
;vAkya 5. meM mukuta/wAja pahanAne kI kriyA ko varNiwa karawA hE.
;
;anwarnihiwa sUwra ;
;
; wAja - jahAz wAja pahanA jAwA hE > SIrRa BAga
;  |
;  |--wAja_pahanAnA-rAjyABiSeka karanA
;
;sUwra : SIrRa_BAga
;
;"crown","N","SIrRa_BAga"
;"crown","V","rAjyABiReka_karanA"

